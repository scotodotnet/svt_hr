﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Data.SqlClient;

public partial class Default2 : System.Web.UI.Page
{


    static int CurrentYear;
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode; string SessionUser;
    string SessionDivision; string SessionRights;
    BALDataAccess objdata = new BALDataAccess();
    string ss1 = "";
    string SSQL = "";

    string SessionUserType;
    string Date1_str;
    string Date2_str;
    string Wages_Type;
    bool Errflag = false;
    bool Errflag1 = false;
    string TotalDays;
    string PerDay;
    string GrandTotal;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionRights = Session["Rights"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report- Late IN Between Dates";
                LoadGroup();
              

            }

        }
        Load_Data();
    }

    private void LoadGroup()
    {

        {

            SSQL = "Select * from MstHindiCat where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
            ddl_GroupName.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
            ddl_GroupName.DataTextField = "CatName";
            ddl_GroupName.DataValueField = "CatCode";
            ddl_GroupName.DataBind();
            ddl_GroupName.Items.Insert(0, new ListItem("Select", "0"));
        }


    }

    protected void ddl_GroupName_SelectedIndexChanged(object sender, EventArgs e)
    {
        {
            if (ddl_GroupName.SelectedItem.Text != "Select")
            {
                SSQL = "";
                SSQL = "Select * from MstCommissionHindi where GroupCD='" + ddl_GroupName.SelectedItem.Value + "'and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                DataTable dt = new DataTable();

                dt = objdata.RptEmployeeMultipleDetails(SSQL);
                //if (dt.Rows.Count > 0)
                //{
                //    if (dt.Rows[0]["Amt"].ToString() != "")
                //    {
                //        txt_Grand_Total.Text = dt.Rows[0]["Amt"].ToString();
                //    }
                //    else
                //    {
                //        txt_Grand_Total.Text = "0";
                //    }
                //}
                //else
                //{
                //    txt_Grand_Total.Text = "0";
                //}
            }
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        bool ErrFlg = false;

        if (ddl_GroupName.SelectedItem.Text == "-Select-")
        {
            ddl_GroupName.Focus();
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please Select the group');", true);
            return;
        }
        if (!ErrFlg)
        {
            string Query = "";
            //Query = "Delete from SP_hindiBoysCommission where GroupName='" + ddl_GroupName.SelectedItem.Value + "' and Ccode='" + txt_Commision_Days.Text + "' and Lcode='" + SessionLcode + "'";

            // objdata.RptEmployeeMultipleDetails(Query);

            Query = "Insert into HindiGroupCommission values('" + ddl_GroupName.SelectedItem.Text + "','" + txt_Commision_Days.Text + "','" + txt_Commision_Perday.Text + "','" + txt_Grand_Total.Text + "')";
            objdata.RptEmployeeMultipleDetails(Query);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Data Saved Successfully!!!');", true);
            txt_Commision_Days.Text = ""; txt_Commision_Perday.Text = ""; txt_Grand_Total.Text = ""; ddl_GroupName.SelectedIndex = 0;
            Load_Data();

        }
        //  if(e.CommandArgument == "edit")
        //{
        //    string Query = "";
        //    Query = "Update HindiGroupCommission set GroupName='" + ddl_GroupName.SelectedItem.Text + "',TotalDays='" + txt_Commision_Days.Text + "',PerDay='" + txt_Commision_Perday.Text + "',GrandTotal='" + txt_Grand_Total.Text + "')";
        //    objdata.RptEmployeeMultipleDetails(Query);
        //}      



    }

    protected void txt_Commision_Perday_TextChanged(object sender, EventArgs e)
    {
        txt_Grand_Total.Text = (Convert.ToInt32(txt_Commision_Days.Text) * Convert.ToInt32(txt_Commision_Perday.Text)).ToString();
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        txt_Commision_Days.Text = ""; txt_Commision_Perday.Text = ""; txt_Grand_Total.Text = ""; ddl_GroupName.SelectedIndex = 0;
    }
    private void Load_Data()
    {
        SSQL = "";
        SSQL = "Select * from  HindiGroupCommission";

        DataTable dt = new DataTable();
        // dt = (DataTable)ViewState["ItemTable"];
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater1.DataSource = dt;
        Repeater1.DataBind();


    }


    protected void GridEditEnquiryClick(object sender, CommandEventArgs e)
    {

        DataTable DT = new DataTable();
        DataTable DT_Check = new DataTable();

        SSQL = "Select * from HindiGroupCommission";
        SSQL = SSQL + " where Sno ='" + e.CommandName.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT.Rows.Count != 0)
        {
            ddl_GroupName.SelectedItem.Text = DT.Rows[0]["GroupName"].ToString();
            txt_Commision_Days.Text = DT.Rows[0]["TotalDays"].ToString();
            txt_Commision_Perday.Text = DT.Rows[0]["PerDay"].ToString();
            txt_Grand_Total.Text = DT.Rows[0]["GrandTotal"].ToString();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Group Details not found.!');", true);
        }
        Load_Data();
    }
    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {
        DataTable DT = new DataTable();
        DataTable DT_Check = new DataTable();

        SSQL = "Select * from HindiGroupCommission";
        SSQL = SSQL + " where Sno ='" + e.CommandName.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT.Rows.Count != 0)
        {
            SSQL = "Delete from HindiGroupCommission where Sno ='" + e.CommandName.ToString() + "'";
            //SSQL = SSQL + " Sno ='" + e.CommandName.ToString() + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Group Details Deleted Succesfully..!');", true);
            Load_Data();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Group Details not found.!');", true);
        }
    }

    //protected void btnUpdate_Click(object sender, EventArgs e)
    //{
        
    //    {
    //        string Query = "";
    //        Query = "Update HindiGroupCommission set GroupName='" + ddl_GroupName.SelectedItem.Text + "',TotalDays='" + txt_Commision_Days.Text + "',PerDay='" + txt_Commision_Perday.Text + "',GrandTotal='" + txt_Grand_Total.Text + "'";
    //        objdata.RptEmployeeMultipleDetails(Query);
    //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Data Saved Successfully!!!');", true);
    //        txt_Commision_Days.Text = ""; txt_Commision_Perday.Text = ""; txt_Grand_Total.Text = ""; ddl_GroupName.SelectedIndex = 0;
    //        Load_Data();
    //    }    
    //}
}

