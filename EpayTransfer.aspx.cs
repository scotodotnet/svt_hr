﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Text;
using System.Security.Cryptography;
using System.IO;

public partial class EpayTransfer : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionUserType;
    
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SSQL = "";
    string[] Time_Minus_Value_Check;
    string SessionPayroll;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        Load_DB();
        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Manual Attendance";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");
            Fin_Year_Add();
            Load_WagesType();
            Load_Data_EmpDet();
        }

        Check_AdminRights();
    }
    public void Load_DB()
    {
        //Get Database Name
        string query = "";
        DataTable dt_DB = new DataTable();
        query = "select CMS,Rights,Sales,Stores,Weaving,Maintaince,Production,Payroll from [SVT_Rights]..MstDBname";
        dt_DB = objdata.RptEmployeeMultipleDetails(query);
        if (dt_DB.Rows.Count > 0)
        {
            SessionPayroll = dt_DB.Rows[0]["Payroll"].ToString();
            //SessionRights = dt_DB.Rows[0]["Rights"].ToString();
        }
    }

    public void Check_AdminRights()
    {
        DataTable dt = new DataTable();
        string query = "";

        query = "Select *from MstAdminRights where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        query = query + " And UserCode='" + SessionUserType + "'";
        dt = objdata.RptEmployeeMultipleDetails(query);

        if (dt.Rows.Count != 0)
        {
            if (dt.Rows[0]["Salary"].ToString() == "Yes")
            {
                btnConform.Enabled = false;
            }
        }
        else
        {
            btnConform.Enabled = true;
        }


    }


    public void Fin_Year_Add()
    {
        int CurrentYear;
        int i;
        //Financial Year Add
        CurrentYear = DateTime.Now.Year;
        if (DateTime.Now.Month >= 1 && DateTime.Now.Month <= 3)
        {
            CurrentYear = CurrentYear - 1;
        }
        else
        {
            CurrentYear = CurrentYear;
        }

        ddlFinyear.Items.Clear();
        ddlFinyear.Items.Add("-Select-");

        for (i = 0; i < 11; i++)
        {
            ddlFinyear.Items.Add(Convert.ToString(CurrentYear) + "-" + Convert.ToString(CurrentYear + 1));
            CurrentYear = CurrentYear - 1;
        }
    }

    private void Load_WagesType()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlWages.Items.Clear();
        query = "Select *from MstEmployeeType";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlWages.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpTypeCd"] = "0";
        dr["EmpType"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlWages.DataTextField = "EmpType";
        ddlWages.DataValueField = "EmpTypeCd";
        ddlWages.DataBind();
    }

    private void Load_Data_EmpDet()
    {
        string query = "";
        DataTable DT = new DataTable();

        //Repeater1.DataSource = DT;
        //Repeater1.DataBind();

        ddlTokenNo.Items.Clear();
        query = "Select ExistingCode from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        query = query + " And IsActive='Yes' And Wages='" + ddlWages.SelectedItem.Text + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        ddlTokenNo.DataSource = DT;
        DataRow dr = DT.NewRow();
        dr["ExistingCode"] = "-Select-";
        dr["ExistingCode"] = "-Select-";
        DT.Rows.InsertAt(dr, 0);
        ddlTokenNo.DataTextField = "ExistingCode";
        ddlTokenNo.DataValueField = "ExistingCode";
        ddlTokenNo.DataBind();
    }

    protected void ddlWages_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Data_EmpDet();
        Load_TwoDates();
    }

    protected void ddlTokenNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        string query;
        DataTable DT = new DataTable();

        query = "Select * from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        query = query + " And ExistingCode='" + ddlTokenNo.SelectedItem.Text + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            ddlTokenNo.Text = DT.Rows[0]["ExistingCode"].ToString();
            txtEmpName.Text = DT.Rows[0]["FirstName"].ToString();
           
        }
        else
        {
            ddlTokenNo.SelectedValue = "-Select-";
            txtEmpName.Text = "";
           
        }
    }
    protected void ddlMonth_SelectedIndexChanged(object sender, EventArgs e)
    {
      
        Load_TwoDates();
    }
    protected void ddlFinyear_SelectedIndexChanged(object sender, EventArgs e)
    {
      
        Load_TwoDates();
    }
   
    public void Load_TwoDates()
    {
        if (ddlMonth.SelectedValue != "-Select-" && ddlFinyear.SelectedValue != "-Select-" && ddlWages.SelectedValue != "0")
        {
            if (ddlWages.SelectedValue != "2" && ddlWages.SelectedValue != "5" && ddlWages.SelectedValue != "10")
            {
                decimal Month_Total_days = 0;
                string Month_Last = "";
                string Year_Last = "0";
                string Temp_Years = "";
                string[] Years;
                string FromDate = "";
                string ToDate = "";

                Temp_Years = ddlFinyear.SelectedValue;
                Years = Temp_Years.Split('-');
                if ((ddlMonth.SelectedValue == "January") || (ddlMonth.SelectedValue == "March") || (ddlMonth.SelectedValue == "May") || (ddlMonth.SelectedValue == "July") || (ddlMonth.SelectedValue == "August") || (ddlMonth.SelectedValue == "October") || (ddlMonth.SelectedValue == "December"))
                {
                    Month_Total_days = 31;
                }
                else if ((ddlMonth.SelectedValue == "April") || (ddlMonth.SelectedValue == "June") || (ddlMonth.SelectedValue == "September") || (ddlMonth.SelectedValue == "November"))
                {
                    Month_Total_days = 30;
                }

                else if (ddlMonth.SelectedValue == "February")
                {
                    int yrs = (Convert.ToInt32(Years[0]) + 1);
                    if ((yrs % 4) == 0)
                    {
                        Month_Total_days = 29;
                    }
                    else
                    {
                        Month_Total_days = 28;
                    }
                }
                switch (ddlMonth.SelectedItem.Text)
                {
                    case "January": Month_Last = "01";
                        break;
                    case "February": Month_Last = "02";
                        break;
                    case "March": Month_Last = "03";
                        break;
                    case "April": Month_Last = "04";
                        break;
                    case "May": Month_Last = "05";
                        break;
                    case "June": Month_Last = "06";
                        break;
                    case "July": Month_Last = "07";
                        break;
                    case "August": Month_Last = "08";
                        break;
                    case "September": Month_Last = "09";
                        break;
                    case "October": Month_Last = "10";
                        break;
                    case "November": Month_Last = "11";
                        break;
                    case "December": Month_Last = "12";
                        break;
                    default:
                        break;
                }

                if ((ddlMonth.SelectedValue == "January") || (ddlMonth.SelectedValue == "February") || (ddlMonth.SelectedValue == "March"))
                {
                    Year_Last = Years[1];
                }
                else
                {
                    Year_Last = Years[0];
                }
                FromDate = "01" + "/" + Month_Last + "/" + Year_Last;
                ToDate = Month_Total_days + "/" + Month_Last + "/" + Year_Last;

                txtFromDate.Text = FromDate.ToString();
                txtToDate.Text = ToDate.ToString();

            }
            else
            {
                txtToDate.Text = "";
                txtFromDate.Text = "";
            }

        }
    }
    protected void btnTransfer_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        DataTable DT = new DataTable();
        string Month_Name_Str = "";

        //FromDate Check
        DateTime FrmDate = Convert.ToDateTime(txtFromDate.Text);
        //Month_Name_Str = FrmDate.Month.ToString();

        if (FrmDate.Month.ToString() == "1")
        {
            Month_Name_Str = "January";
        }
        else if (FrmDate.Month.ToString() == "2")
        {
            Month_Name_Str = "February";
        }
        else if (FrmDate.Month.ToString() == "3")
        {
            Month_Name_Str = "March";
        }
        else if (FrmDate.Month.ToString() == "4")
        {
            Month_Name_Str = "April";
        }
        else if (FrmDate.Month.ToString() == "5")
        {
            Month_Name_Str = "May";
        }
        else if (FrmDate.Month.ToString() == "6")
        {
            Month_Name_Str = "June";
        }
        else if (FrmDate.Month.ToString() == "7")
        {
            Month_Name_Str = "July";
        }
        else if (FrmDate.Month.ToString() == "8")
        {
            Month_Name_Str = "August";
        }
        else if (FrmDate.Month.ToString() == "9")
        {
            Month_Name_Str = "September";
        }
        else if (FrmDate.Month.ToString() == "10")
        {
            Month_Name_Str = "October";
        }
        else if (FrmDate.Month.ToString() == "11")
        {
            Month_Name_Str = "November";
        }
        else if (FrmDate.Month.ToString() == "12")
        {
            Month_Name_Str = "December";
        }
        if (ddlWages.SelectedValue != "5")
        {
            if ((Month_Name_Str).ToUpper() != ddlMonth.SelectedItem.Text.ToUpper())
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with MONTH NAME AND FROM DATE.!');", true);
                txtFromDate.Focus();
            }
        }

        //ToDate Month Name Check
        DateTime ToDate = Convert.ToDateTime(txtToDate.Text);
        //Month_Name_Str = FrmDate.Month.ToString();

        if (ToDate.Month.ToString() == "1")
        {
            Month_Name_Str = "January";
        }
        else if (ToDate.Month.ToString() == "2")
        {
            Month_Name_Str = "February";
        }
        else if (ToDate.Month.ToString() == "3")
        {
            Month_Name_Str = "March";
        }
        else if (ToDate.Month.ToString() == "4")
        {
            Month_Name_Str = "April";
        }
        else if (ToDate.Month.ToString() == "5")
        {
            Month_Name_Str = "May";
        }
        else if (ToDate.Month.ToString() == "6")
        {
            Month_Name_Str = "June";
        }
        else if (ToDate.Month.ToString() == "7")
        {
            Month_Name_Str = "July";
        }
        else if (ToDate.Month.ToString() == "8")
        {
            Month_Name_Str = "August";
        }
        else if (ToDate.Month.ToString() == "9")
        {
            Month_Name_Str = "September";
        }
        else if (ToDate.Month.ToString() == "10")
        {
            Month_Name_Str = "October";
        }
        else if (ToDate.Month.ToString() == "11")
        {
            Month_Name_Str = "November";
        }
        else if (ToDate.Month.ToString() == "12")
        {
            Month_Name_Str = "December";
        }

        if (ddlWages.SelectedValue != "5")
        {
            if ((Month_Name_Str).ToUpper() != ddlMonth.SelectedItem.Text.ToUpper())
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with MONTH NAME AND TO DATE.!');", true);
                txtToDate.Focus();
            }
        }


        //Check Financial Year
        int CurrentYear;
        string Fin_Year_Str;
        //Financial Year Add
        CurrentYear = Convert.ToDateTime(txtFromDate.Text).Year;
        if (Convert.ToDateTime(txtFromDate.Text).Month >= 1 && Convert.ToDateTime(txtFromDate.Text).Month <= 3)
        {
            CurrentYear = CurrentYear - 1;
        }
        else
        {
            CurrentYear = CurrentYear;
        }
        Fin_Year_Str = Convert.ToString(CurrentYear) + "-" + Convert.ToString(CurrentYear + 1);
        if (Fin_Year_Str != ddlFinyear.SelectedItem.Text)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check With Financial Year...!');", true);
        }

        if (!ErrFlag)
        {
            Days_Transfer_To_EPay_New();
        }
    }

    public void Days_Transfer_To_EPay_New()
    {
        try{
         
            int intI  = 1;
            int intK = 1;
            int intCol;

            //iStr1 = Split(txtCompCode_Epay.Text, " | ")
            //iStr2 = Split(txtLocCode_Epay.Text, " | ")

            DataTable Log_DS=new DataTable();
            SSQL = "Select LTD.MachineID,LTD.ExistingCode,EM.FirstName,sum(LTD.Present) as Days,EM.DeptCode, ";
            SSQL = SSQL + " EM.DOJ,EM.Weekoff from LogTime_Days LTD inner join Employee_Mst EM on";
            SSQL = SSQL + " EM.CompCode=LTD.CompCode And EM.LocCode=LTD.LocCode And LTD.MachineID=EM.MachineID";
            SSQL = SSQL + " where LTD.CompCode='" + SessionCcode + "' And LTD.LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And EM.CompCode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And EM.Wages='" + ddlWages.SelectedItem.Text + "' And (EM.IsActive='Yes' OR CONVERT(DATETIME,EM.DOR,103)>=CONVERT(DATETIME,'" + txtFromDate.Text + "',103))";
            SSQL = SSQL + " And LTD.Attn_Date >= '" + Convert.ToDateTime(txtFromDate.Text).ToString("yyyy/MM/dd") + "'";
            SSQL = SSQL + " And LTD.Attn_Date <= '" + Convert.ToDateTime(txtToDate.Text).ToString("yyyy/MM/dd") + "'";

            if(ddlTokenNo.SelectedItem.Text!="-Select-")
            {
                SSQL = SSQL + " And EM.ExistingCode='" + ddlTokenNo.SelectedItem.Text + "'";
            }

            SSQL = SSQL + " Group by LTD.MachineID,LTD.ExistingCode,EM.FirstName,EM.DOJ,EM.Weekoff,EM.DeptCode";
            SSQL = SSQL + " Order by LTD.ExistingCode Asc";

            Log_DS = objdata.RptEmployeeMultipleDetails(SSQL);
            intI = 2;
            DataTable mDataSet=new DataTable();
            bool Transfer_Check_blb = false;
            //PBar_EPay.Visible = True
            //PBar_EPay.Value = 0
            //PBar_EPay.Maximum = Log_DS.Tables(0).Rows.Count
            //PBar_EPay.Show()
            //PBar_EPay.Refresh()
            //For i = 0 To Log_DS.Tables(0).Rows.Count - 1
            for (int i = 0; i < Log_DS.Rows.Count; i++)
            {

                if (Log_DS.Rows[i]["ExistingCode"].ToString() == "W0970") 
                {
                    string Check = "";
                }

                //Final Output Variable Declaration
                decimal NFH_Days_Count = 0;
                decimal AEH_NFH_Days_Count = 0;
                decimal LBH_NFH_Days_Count = 0;
                decimal NFH_Days_Present_Count = 0;
                decimal WH_Count = 0;
                decimal WH_Present_Count = 0;
                decimal Present_Days_Count = Convert.ToDecimal(Log_DS.Rows[i]["Days"]);
                int Fixed_Work_Days = 0;
                decimal Spinning_Incentive_Days = 0;


                string Emp_WH_Day = "";
                string DOJ_Date_Str = "";
                if (Log_DS.Rows[i]["DOJ"].ToString() != "")
                {
                    DOJ_Date_Str = Log_DS.Rows[i]["DOJ"].ToString();
                }

                Emp_WH_Day = Log_DS.Rows[i]["WeekOff"].ToString();

                //NFH Check Start
                DataTable NFH_DS = new DataTable();
                DateTime NFH_Date;
                DateTime DOJ_Date_Format;
                string qry_nfh = "";
                double NFH_Present_Check = 0;

                //NFH New Rule 29/10/2015
                decimal NFH_Double_Wages_Checklist = 0;
                decimal NFH_Double_Wages_Statutory = 0;
                decimal NFH_Double_Wages_Manual = 0;
                decimal NFH_WH_Days_Mins = 0;
                decimal NFH_Single_Wages = 0;

                DataTable NFH_Type_Ds = new DataTable();
                string NFH_Type_Str = "";
                string NFH_Name_Get_Str = "";
                string NFH_Dbl_Wages_Statutory_Check = "";

                qry_nfh = "Select NFHDate from NFH_Mst where NFHDate >= '" + Convert.ToDateTime(txtFromDate.Text).ToString("yyyy/MM/dd") + "'";
                qry_nfh = qry_nfh + " And NFHDate <= '" + Convert.ToDateTime(txtToDate.Text).ToString("yyyy/MM/dd") + "'";
                NFH_DS = objdata.RptEmployeeMultipleDetails(qry_nfh);
                if (NFH_DS.Rows.Count > 0)
                {
                    //Date OF Joining Check Start
                    for (int K = 0; K < NFH_DS.Rows.Count; K++)
                    {

                        NFH_Date = Convert.ToDateTime(NFH_DS.Rows[K]["NFHDate"]);
                        //NFH Day Present Check
                        string NFH_Date_P_Date = Convert.ToDateTime(NFH_Date).AddDays(0).ToString("yyyy/MM/dd");
                        SSQL = "Select * from LogTime_Days where Attn_Date='" + NFH_Date_P_Date + "' And MachineID='" + Log_DS.Rows[i]["MachineID"].ToString() + "' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                        mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);

                        if (mDataSet.Rows.Count != 0)
                        {
                            NFH_Present_Check = Convert.ToDouble(mDataSet.Rows[0]["Present"]);
                        }
                        else
                        {
                            NFH_Present_Check = 0;
                        }

                        //Get NFh Type
                        SSQL = "Select * from NFH_Mst where NFHDate=convert(varchar,'" + NFH_Date_P_Date + "',103)";
                        NFH_Type_Ds = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (NFH_Type_Ds.Rows.Count != 0)
                        {
                            NFH_Type_Str = NFH_Type_Ds.Rows[0]["NFH_Type"].ToString();
                            NFH_Dbl_Wages_Statutory_Check = NFH_Type_Ds.Rows[0]["Dbl_Wages_Statutory"].ToString();
                            NFH_Name_Get_Str = NFH_Type_Ds.Rows[0]["Form25DisplayText"].ToString();
                        }
                        else
                        {
                            NFH_Type_Str = "";
                            NFH_Dbl_Wages_Statutory_Check = "";
                            NFH_Name_Get_Str = "";
                        }

                        if (DOJ_Date_Str != "")
                        {
                            DOJ_Date_Format = Convert.ToDateTime(DOJ_Date_Str);
                            if (DOJ_Date_Format < NFH_Date)
                            {
                                if (NFH_Type_Str.ToUpper() == "WH Minus".ToUpper())
                                {
                                    NFH_WH_Days_Mins = NFH_WH_Days_Mins + 1;
                                }
                                else
                                {

                                    NFH_Days_Present_Count = Convert.ToDecimal(NFH_Days_Present_Count) + Convert.ToDecimal(NFH_Present_Check);
                                    if (NFH_Type_Str.ToUpper() == "Double Wages Checklist".ToUpper())
                                    {
                                        NFH_Double_Wages_Checklist = Convert.ToDecimal(NFH_Double_Wages_Checklist) + Convert.ToDecimal(NFH_Present_Check);
                                        if (NFH_Dbl_Wages_Statutory_Check.ToUpper() == "Yes".ToUpper())
                                        {
                                            NFH_Double_Wages_Statutory = Convert.ToDecimal(NFH_Double_Wages_Statutory) + Convert.ToDecimal(NFH_Present_Check);
                                        }
                                    }
                                    if (NFH_Type_Str.ToUpper() == "Double Wages Manual".ToUpper())
                                    {
                                        NFH_Double_Wages_Manual = Convert.ToDecimal(NFH_Double_Wages_Manual) + Convert.ToDecimal(NFH_Present_Check);
                                    }

                                }
                                NFH_Days_Count = NFH_Days_Count + 1;

                                if (NFH_Name_Get_Str.ToUpper() == "AEH".ToUpper())
                                {
                                    AEH_NFH_Days_Count = AEH_NFH_Days_Count + 1;
                                }
                                if (NFH_Name_Get_Str.ToUpper() == "LBH".ToUpper())
                                {
                                    LBH_NFH_Days_Count = LBH_NFH_Days_Count + 1;
                                }

                                //NFH_Days_Present_Count = NFH_Days_Present_Count + NFH_Present_Check
                            }
                            else
                            {
                            }
                        }
                        else
                        {
                            if (NFH_Type_Str.ToUpper() == "WH Minus".ToUpper())
                            {
                                NFH_WH_Days_Mins = NFH_WH_Days_Mins + 1;
                            }
                            else
                            {
                                NFH_Days_Present_Count = Convert.ToDecimal(NFH_Days_Present_Count) + Convert.ToDecimal(NFH_Present_Check);
                                if (NFH_Type_Str.ToUpper() == "Double Wages Checklist".ToUpper())
                                {
                                    NFH_Double_Wages_Checklist = Convert.ToDecimal(NFH_Double_Wages_Checklist) + Convert.ToDecimal(NFH_Present_Check);
                                    if (NFH_Dbl_Wages_Statutory_Check.ToUpper() == "Yes".ToUpper())
                                    {
                                        NFH_Double_Wages_Statutory = Convert.ToDecimal(NFH_Double_Wages_Statutory) + Convert.ToDecimal(NFH_Present_Check);
                                    }
                                }
                                if (NFH_Type_Str.ToUpper() == "Double Wages Manual".ToUpper())
                                {
                                    NFH_Double_Wages_Manual = Convert.ToDecimal(NFH_Double_Wages_Manual) + Convert.ToDecimal(NFH_Present_Check);
                                }


                            }
                            NFH_Days_Count = NFH_Days_Count + 1;

                            if (NFH_Name_Get_Str.ToUpper() == "AEH".ToUpper())
                            {
                                AEH_NFH_Days_Count = AEH_NFH_Days_Count + 1;
                            }

                            if (NFH_Name_Get_Str.ToUpper() == "LBH".ToUpper())
                            {
                                LBH_NFH_Days_Count = LBH_NFH_Days_Count + 1;
                            }
                            // NFH_Days_Present_Count = NFH_Days_Present_Count + NFH_Present_Check
                        }
                        //Next K
                    }
                }
                //Date OF Joining Check End
                else
                {
                    //Skip
                }
                //NFH Check END

               // Week of Check
                DataTable WH_DS = new DataTable();
                DateTime Week_Off_Date;
                DateTime WH_DOJ_Date_Format;
                bool Check_Week_Off_DOJ = false;

                SSQL = "Select Present,Attn_Date from LogTime_Days where MachineID='" + Log_DS.Rows[i]["MachineID"] + "' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                SSQL = SSQL + " And upper(DATENAME(weekday,Attn_Date))=upper('" + Emp_WH_Day + "')";
                SSQL = SSQL + " And Attn_Date >= '" + Convert.ToDateTime(txtFromDate.Text).ToString("yyyy/MM/dd") + "'";
                SSQL = SSQL + " And Attn_Date <= '" + Convert.ToDateTime(txtToDate.Text).ToString("yyyy/MM/dd") +"'";
                WH_DS = objdata.RptEmployeeMultipleDetails(SSQL);
                for (int j = 0; j < WH_DS.Rows.Count; j++)
                {
                    if (WH_DS.Rows[j]["Attn_Date"].ToString() != "")
                    {
                        Week_Off_Date = Convert.ToDateTime(WH_DS.Rows[j]["Attn_Date"]);
                    }
                    else
                    {
                        Week_Off_Date = new DateTime();
                    }
                    if (ddlWages.SelectedItem.Text.ToUpper() == "STAFF".ToUpper() || ddlWages.SelectedItem.Text.ToUpper() == "Watch & Ward".ToUpper() || ddlWages.SelectedItem.Text == "Manager".ToUpper())
                    {
                        if (DOJ_Date_Str != "")
                        {
                            WH_DOJ_Date_Format = Convert.ToDateTime(DOJ_Date_Str);
                            if (WH_DOJ_Date_Format <= Week_Off_Date)
                            {
                                Check_Week_Off_DOJ = true;
                            }
                            else
                            {
                                Check_Week_Off_DOJ = false;
                            }
                        }
                        else
                        {
                            Check_Week_Off_DOJ = true;
                        }
                    }
                    else
                    {
                        Check_Week_Off_DOJ = true;
                    }

                    if (Check_Week_Off_DOJ == true)
                    {
                        WH_Count = WH_Count + 1;
                        //NFH Day Check
                        string NFH_Date_WH_Date = Convert.ToDateTime(Week_Off_Date).AddDays(0).ToString("yyyy/MM/dd");

                        SSQL = "Select * from NFH_Mst where NFHDate=convert(varchar,'" + NFH_Date_WH_Date + "',103)";
                        mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);

                        if (mDataSet.Rows.Count > 0)
                        {
                            //Skip
                            if (mDataSet.Rows.Count != 0)
                            {
                                NFH_Type_Str = mDataSet.Rows[0]["NFH_Type"].ToString();
                                NFH_Dbl_Wages_Statutory_Check = mDataSet.Rows[0]["Dbl_Wages_Statutory"].ToString();
                            }
                            else
                            {
                                NFH_Type_Str = "";
                                NFH_Dbl_Wages_Statutory_Check = "";
                            }

                            if (NFH_Type_Str.ToUpper() == "WH Minus".ToUpper())
                            {
                                WH_Present_Count = WH_Present_Count + Convert.ToDecimal(WH_DS.Rows[j]["Present"]);
                            }
                            else
                            {
                            }

                        }
                        else
                        {
                            WH_Present_Count = WH_Present_Count + Convert.ToDecimal(WH_DS.Rows[j]["Present"]);
                        }
                    }
                }

            //Spinning Incentive Check
                if (ddlWages.SelectedItem.Text.ToUpper() == "REGULAR".ToUpper() || ddlWages.SelectedItem.Text.ToUpper() == "HOSTEL".ToUpper())
                {
                    //Check Spinning Days Start
                    bool Check_Spinning_Eligible = false;
                    DataTable DS_Spinning_Incv = new DataTable();
                    //        'Spinning Wages Check
                    int Month_Int = 1;
                    string Months_Full_Str = "";

                    string Spin_Wages = ddlWages.SelectedItem.Text;
                    string Spin_Machine_ID_Str = Log_DS.Rows[i]["MachineID"].ToString();
                    string Fin_Year = "";
                    DateTime FrmDate = Convert.ToDateTime(txtFromDate.Text);

                    Month_Int = Convert.ToDateTime(txtFromDate.Text).Month;
                    if (Month_Int >= 4)
                    {
                        Fin_Year = Convert.ToDateTime(txtFromDate.Text).Year + "-" + (Convert.ToDateTime(txtFromDate.Text).Year + 1);
                    }
                    else
                    {
                        Fin_Year = (Convert.ToDateTime(txtFromDate.Text).Year - 1) + "-" + (Convert.ToDateTime(txtFromDate.Text).Year);
                    }

                    if (Month_Int.ToString() == "1")
                    {
                        Months_Full_Str = "January";
                    }
                    else if (Month_Int.ToString() == "2")
                    {
                        Months_Full_Str = "February";
                    }
                    else if (Month_Int.ToString() == "3")
                    {
                        Months_Full_Str = "March";
                    }
                    else if (Month_Int.ToString() == "4")
                    {
                        Months_Full_Str = "April";
                    }
                    else if (Month_Int.ToString() == "5")
                    {
                        Months_Full_Str = "May";
                    }
                    else if (Month_Int.ToString() == "6")
                    {
                        Months_Full_Str = "June";
                    }
                    else if (Month_Int.ToString() == "7")
                    {
                        Months_Full_Str = "July";
                    }
                    else if (Month_Int.ToString() == "8")
                    {
                        Months_Full_Str = "August";
                    }
                    else if (Month_Int.ToString() == "9")
                    {
                        Months_Full_Str = "September";
                    }
                    else if (Month_Int.ToString() == "10")
                    {
                        Months_Full_Str = "October";
                    }
                    else if (Month_Int.ToString() == "11")
                    {
                        Months_Full_Str = "November";
                    }
                    else if (Month_Int.ToString() == "12")
                    {
                        Months_Full_Str = "December";
                    }


                    Months_Full_Str = Left_Val(Months_Full_Str, 3);
                    SSQL = "Select * from SpinIncentiveDetPart where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And Months='" + Months_Full_Str + "' And FinYear='" + Fin_Year + "' And Wages='" + Spin_Wages + "' And MachineID='" + Spin_Machine_ID_Str + "'";
                    DS_Spinning_Incv = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (DS_Spinning_Incv.Rows.Count != 0)
                    {
                        Check_Spinning_Eligible = true;
                    }
                    else
                    {
                        Check_Spinning_Eligible = false;
                    }

                    if (Check_Spinning_Eligible == true)
                    {
                        if (ddlWages.SelectedItem.Text.ToUpper() == "HOSTEL".ToUpper())
                        {
                            Spinning_Incentive_Days = Convert.ToDecimal(Log_DS.Rows[i]["Days"].ToString());
                        }
                        else
                        {
                            if (ddlWages.SelectedItem.Text.ToUpper() == "REGULAR".ToUpper())
                            {
                                string Minimum_Days="0";
                                string Shift3_Days = "0";
                                SSQL = "Select * from Department_Inc_Mst where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And Wages='REGULAR'";
                                DataTable DT_SH = new DataTable();
                                DT_SH = objdata.RptEmployeeMultipleDetails(SSQL);
                                if (DT_SH.Rows.Count != 0) { Minimum_Days = DT_SH.Rows[0]["Min_Days"].ToString(); }
                                SSQL = "Select isnull(sum(Present),0) as Days from LogTime_Days where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                SSQL = SSQL + " And MachineID='" + Log_DS.Rows[i]["MachineID"] + "'";
                                SSQL = SSQL + " And Attn_Date >= '" + Convert.ToDateTime(txtFromDate.Text).ToString("yyyy/MMM/dd") + "'";
                                SSQL = SSQL + " And Attn_Date <= '" + Convert.ToDateTime(txtToDate.Text).ToString("yyyy/MM/dd") + "'";
                                SSQL = SSQL + " And (Shift='SHIFT3')";
                                mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);
                                if (mDataSet.Rows.Count != 0)
                                {
                                    Shift3_Days = mDataSet.Rows[0]["Days"].ToString();
                                }
                                if (Convert.ToDecimal(Shift3_Days) != 0)
                                {
                                    if (Convert.ToDecimal(Minimum_Days) <= Convert.ToDecimal(Shift3_Days))
                                    {
                                        SSQL = "Select isnull(sum(Present),0) as Days from LogTime_Days where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                        SSQL = SSQL + " And MachineID='" + Log_DS.Rows[i]["MachineID"] + "'";
                                        SSQL = SSQL + " And Attn_Date >= '" + Convert.ToDateTime(txtFromDate.Text).ToString("yyyy/MMM/dd") + "'";
                                        SSQL = SSQL + " And Attn_Date <= '" + Convert.ToDateTime(txtToDate.Text).ToString("yyyy/MM/dd") + "'";
                                        SSQL = SSQL + " And (Shift='SHIFT2' or Shift='SHIFT3')";
                                        mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);
                                        if (mDataSet.Rows.Count != 0)
                                        {
                                            Spinning_Incentive_Days = Convert.ToDecimal(mDataSet.Rows[0]["Days"]);
                                        }
                                        else
                                        {
                                            Spinning_Incentive_Days = 0;
                                        }
                                    }
                                    else
                                    {
                                        //Check DOJ Current Month
                                        string query_doj = "";
                                        bool DOJ_Check_True_or_False = false;
                                        DataTable DT_Q_DOJ = new DataTable();
                                        query_doj = "Select Convert(varchar(20),DOJ,103) as DOJ from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And MachineID='" + Spin_Machine_ID_Str + "'";
                                        query_doj = query_doj + " And DATENAME(MM,DOJ)='" + ddlMonth.SelectedItem.Text + "' And YEAR(DOJ)='" + FrmDate.Year + "'";
                                        DT_Q_DOJ = objdata.RptEmployeeMultipleDetails(query_doj);
                                        if (DT_Q_DOJ.Rows.Count != 0)
                                        {
                                            string DOJ_Date_Str_Checking = DT_Q_DOJ.Rows[0]["DOJ"].ToString();
                                            string[] DOJ_Sshift_Inc = DOJ_Date_Str_Checking.Split('/');
                                            if (DOJ_Sshift_Inc[0].ToString() == "01")
                                            {
                                                DOJ_Check_True_or_False = false;
                                            }
                                            else
                                            {
                                                DOJ_Check_True_or_False = true;
                                            }
                                        }
                                        else
                                        {
                                            DOJ_Check_True_or_False = false;
                                        }

                                        if (DOJ_Check_True_or_False == true)
                                        {
                                            SSQL = "Select isnull(sum(Present),0) as Days from LogTime_Days where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                            SSQL = SSQL + " And MachineID='" + Log_DS.Rows[i]["MachineID"] + "'";
                                            SSQL = SSQL + " And Attn_Date >= '" + Convert.ToDateTime(txtFromDate.Text).ToString("yyyy/MMM/dd") + "'";
                                            SSQL = SSQL + " And Attn_Date <= '" + Convert.ToDateTime(txtToDate.Text).ToString("yyyy/MM/dd") + "'";
                                            SSQL = SSQL + " And (Shift='SHIFT3')";
                                            mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);
                                            if (Convert.ToDecimal(mDataSet.Rows[0]["Days"].ToString()) != 0)
                                            {
                                                SSQL = "Select isnull(sum(Present),0) as Days from LogTime_Days where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                                SSQL = SSQL + " And MachineID='" + Log_DS.Rows[i]["MachineID"] + "'";
                                                SSQL = SSQL + " And Attn_Date >= '" + Convert.ToDateTime(txtFromDate.Text).ToString("yyyy/MMM/dd") + "'";
                                                SSQL = SSQL + " And Attn_Date <= '" + Convert.ToDateTime(txtToDate.Text).ToString("yyyy/MM/dd") + "'";
                                                SSQL = SSQL + " And (Shift='SHIFT2' or Shift='SHIFT3')";
                                                mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);
                                                if (mDataSet.Rows.Count != 0)
                                                {
                                                    Spinning_Incentive_Days = Convert.ToDecimal(mDataSet.Rows[0]["Days"]);
                                                }
                                                else
                                                {
                                                    Spinning_Incentive_Days = 0;
                                                }
                                            }
                                            else
                                            {
                                                Spinning_Incentive_Days = 0;
                                            }
                                        }
                                        else
                                        {
                                            Spinning_Incentive_Days = 0;
                                        }


                                    }
                                }
                                else
                                {
                                    Spinning_Incentive_Days = 0;
                                }
                                //Here Close

                            }
                            else
                            {
                                Spinning_Incentive_Days = 0;
                            }

                        }
                    }
                }
                else
                {
                    Spinning_Incentive_Days = 0;
                }
            

            //Total Days Get
                DateTime date1 = Convert.ToDateTime(txtFromDate.Text);
                DateTime date2 = Convert.ToDateTime(txtToDate.Text);
                int Total_Days_Count = (int)((date2 - date1).TotalDays);
                Total_Days_Count = Total_Days_Count + 1;


                int Month_Mid_Total_Days_Count = 0;
            //Check DOJ Date to Report Date
                DateTime Report_Date;
                DateTime DOJ_Date_Format_Check;
                if (ddlWages.SelectedItem.Text.ToUpper() == "STAFF".ToUpper() || ddlWages.SelectedItem.Text.ToUpper() == "Watch & Ward".ToUpper() || ddlWages.SelectedItem.Text.ToUpper() == "Manager".ToUpper())
                {
                    if (DOJ_Date_Str != "")
                    {
                        Report_Date = Convert.ToDateTime(txtFromDate.Text);  //Report Date
                        DOJ_Date_Format_Check = Convert.ToDateTime(DOJ_Date_Str);
                        if (DOJ_Date_Format_Check <= Report_Date)
                        {
                            Month_Mid_Total_Days_Count = Total_Days_Count;
                        }
                        else
                        {
                            Month_Mid_Total_Days_Count = (int)((Convert.ToDateTime(txtToDate.Text) - DOJ_Date_Format_Check).TotalDays);
                            Month_Mid_Total_Days_Count = Month_Mid_Total_Days_Count + 1;
                        }
                    }
                    else
                    {
                        Month_Mid_Total_Days_Count = Total_Days_Count;
                    }
                }
                else
                {
                    //'Skip
                }
                if (ddlWages.SelectedItem.Text.ToUpper() == "STAFF".ToUpper() || ddlWages.SelectedItem.Text.ToUpper() == "Watch & Ward".ToUpper() || ddlWages.SelectedItem.Text.ToUpper() == "Manager".ToUpper())
                {
                    Fixed_Work_Days = Convert.ToInt32(Month_Mid_Total_Days_Count) - Convert.ToInt32(WH_Count);
                    Present_Days_Count = Present_Days_Count - WH_Present_Count;
                    Present_Days_Count = Present_Days_Count - NFH_Days_Present_Count;
                    WH_Present_Count = WH_Present_Count + NFH_Days_Present_Count;

                }
                else
                {
                    Fixed_Work_Days = 0;
                    Present_Days_Count = Present_Days_Count - WH_Present_Count;
                    Present_Days_Count = Present_Days_Count - NFH_Days_Present_Count;
                    WH_Present_Count = WH_Present_Count;
                }

            //    'Get NFH Rule Start
            //    'Dim NFH_Year_Str As String = "0"
            //    'Dim NFH_Month_Str As String = ""
            //    'Dim NFH_Rule_OLD As String = ""
            //    'Dim NFH_Display_Days As Int32 = 0
            //    'Dim NFH_Double_Wages_Rule As String = ""
            //    'NFH_Year_Str = txtFromDate_Epay.Value.Year : NFH_Month_Str = txtFromDate_Epay.Value.ToString("MMMM")
            //    'SSQL = "Select * from NFH_Rule_Det where NFHYear='" & NFH_Year_Str & "' And Months='" & NFH_Month_Str & "'"
            //    'mDataSet = ReturnMultipleValue(SSQL)
            //    'If mDataSet.Tables(0).Rows.Count <> 0 Then
            //    '    If UCase(mDataSet.Tables(0).Rows(0)("OLD_Rule").ToString) = UCase("True") Then
            //    '        NFH_Rule_OLD = "True"
            //    '    Else
            //    '        NFH_Rule_OLD = "False"
            //    '    End If
            //    '    NFH_Display_Days = IIf(mDataSet.Tables(0).Rows(0)("NFH_Disp_Days").ToString <> "", mDataSet.Tables(0).Rows(0)("NFH_Disp_Days"), 0)
            //    '    NFH_Double_Wages_Rule = mDataSet.Tables(0).Rows(0)("NFH_Wages_Rule").ToString()
            //    'End If

            //    'If Present_Days_Count = 0 Then
            //    '    NFH_Days_Count = 0
            //    'End If
            //    'If NFH_Rule_OLD = "True" And Present_Days_Count <> 0 And NFH_Days_Count <> 0 And NFH_Display_Days <> 0 Then
            //    '    'NFH_Days_Present_Count = 0

            //    '    If NFH_Display_Days = 1 And NFH_Days_Count = 1 Then
            //    '        NFH_Days_Count = 0
            //    '    End If

            //    '    Dim NFH_Week_Off_Minus_Days As Int32 = 0
            //    '    Dim NFH_Final_Disp_Days As Double = 0

            //    '    If NFH_Display_Days <> 0 Then NFH_Week_Off_Minus_Days = NFH_Display_Days - NFH_Days_Count
            //    '    NFH_Final_Disp_Days = NFH_Week_Off_Minus_Days + NFH_Days_Count
            //    '    If NFH_Week_Off_Minus_Days <= WH_Present_Count Then
            //    '        WH_Present_Count = WH_Present_Count - NFH_Week_Off_Minus_Days
            //    '    Else
            //    '        'NFH Days Minus for Working Days
            //    '        Dim Balance_NFH_Days_Minus_IN_WorkDays As Double = 0
            //    '        Balance_NFH_Days_Minus_IN_WorkDays = NFH_Week_Off_Minus_Days - WH_Present_Count
            //    '        Present_Days_Count = Present_Days_Count - Balance_NFH_Days_Minus_IN_WorkDays
            //    '        WH_Present_Count = 0
            //    '    End If
            //    '    NFH_Days_Count = NFH_Final_Disp_Days
            //    'Else
            //    '    'Skip
            //    'End If
            //    'If UCase(NFH_Double_Wages_Rule) = UCase("True") Then
            //    '    NFH_Days_Present_Count = NFH_Days_Present_Count
            //    'Else
            //    '    NFH_Days_Present_Count = 0
            //    'End If

                if (Present_Days_Count != 0 && NFH_Days_Count != 0 && NFH_WH_Days_Mins != 0)
                {
                    double NFH_Week_Off_Minus_Days = Convert.ToDouble(NFH_WH_Days_Mins);
                    double NFH_Final_Disp_Days = 0;
                    if (NFH_Week_Off_Minus_Days <= Convert.ToDouble(WH_Present_Count))
                    {
                        WH_Present_Count = WH_Present_Count - Convert.ToDecimal(NFH_Week_Off_Minus_Days);
                    }
                    else
                    {
                        //NFH Days Minus for Working Days
                        double Balance_NFH_Days_Minus_IN_WorkDays = 0;
                        Balance_NFH_Days_Minus_IN_WorkDays = NFH_Week_Off_Minus_Days - Convert.ToDouble(WH_Present_Count);
                        Present_Days_Count = Present_Days_Count - Convert.ToDecimal(Balance_NFH_Days_Minus_IN_WorkDays);
                        WH_Present_Count = 0;

                    }
                }
            //''Get NFH Rule End


            //    'Epay Transfer Var Declaration
                string Machine_ID_EPay = "";
                string Token_No_EPay = "";
                string EmpName_EPay = "";
                string Final_Work_Days_EPay = "0";
                string H_Allowed_EPay = "0";
                string NFH_EPay = "0";
                string OTDays_EPay = "0";
                string Spinning_Days_EPay = "0";
                string CanteenDays_Minus_EPay = "0";
                string OTHours_EPay = "0";
                string WeekOff_Work_Days_EPay = "0";
                string Fixed_Work_Days_EPay = "0";
                string EPay_Total_Days_Get = Total_Days_Count.ToString();

            // ayroll EmpDet Get
                string EPay_EmpNo_Get = Log_DS.Rows[i]["MachineID"].ToString();
                string EPay_Department_Get = Log_DS.Rows[i]["DeptCode"].ToString();
                string EPay_TokenNo_Get = Log_DS.Rows[i]["ExistingCode"].ToString();
               
                //SSQL = "Select * from [" + SessionPayroll + "]..EmployeeDetails where BiometricID='" + Log_DS.Rows[i]["MachineID"] + "' And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                //mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);

                //if (mDataSet.Rows.Count != 0)
                //{
                //    EPay_EmpNo_Get = mDataSet.Rows[0]["EmpNo"].ToString();
                //    EPay_Department_Get = mDataSet.Rows[0]["Department"].ToString();
                //    EPay_TokenNo_Get = mDataSet.Rows[0]["ExisistingCode"].ToString();
                //}

           

            //Get Allowence_Deduction Details
                DataTable AllDed_Ds = new DataTable();
                string[] Fin_Year_Spilit;
                bool Days_Update_Check_blb = false;
                Fin_Year_Spilit = ddlFinyear.SelectedItem.Text.Split('-');


                SSQL = "Select * from [" + SessionPayroll + "]..eAlert_Deduction_Det Where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And Month='" + ddlMonth.SelectedItem.Text + "' And FinancialYear='" + Fin_Year_Spilit[0] + "'";
                SSQL = SSQL + " And Wages='" + ddlWages.SelectedItem.Text + "' And MachineID='" + Log_DS.Rows[i]["MachineID"].ToString() + "'";
                SSQL = SSQL + " And ExisistingCode='" + Log_DS.Rows[i]["ExistingCode"].ToString() + "'";

                if (((int)((date2 - date1).TotalDays)) >= 0)
                {
                    SSQL = SSQL + " and FromDate = '" + Convert.ToDateTime(txtFromDate.Text).ToString("yyyy/MM/dd") + "' and ToDate = '" + Convert.ToDateTime(txtToDate.Text).ToString("yyyy/MM/dd") + "'";
                }
                AllDed_Ds = objdata.RptEmployeeMultipleDetails(SSQL);

                if (AllDed_Ds.Rows.Count != 0)
                {
                    H_Allowed_EPay = AllDed_Ds.Rows[0]["HAllowed"].ToString();
                    CanteenDays_Minus_EPay = AllDed_Ds.Rows[0]["CanteenDaysMinus"].ToString();
                    OTHours_EPay = AllDed_Ds.Rows[0]["OTHours"].ToString();
                }
                else
                {
                    H_Allowed_EPay = "0";
                    CanteenDays_Minus_EPay = "0";
                    OTHours_EPay = "0";
                }
                if (Present_Days_Count != 0)
                {
                    Days_Update_Check_blb = true;
                    Machine_ID_EPay = Log_DS.Rows[i]["MachineID"].ToString();
                    Token_No_EPay = Log_DS.Rows[i]["ExistingCode"].ToString();
                    EmpName_EPay = Log_DS.Rows[i]["FirstName"].ToString();
                    Final_Work_Days_EPay = Present_Days_Count.ToString();
                    H_Allowed_EPay = H_Allowed_EPay;
                    NFH_EPay = NFH_Days_Count.ToString();

                    if (ddlWages.SelectedItem.Text.ToUpper() == "STAFF".ToUpper() || ddlWages.SelectedItem.Text.ToUpper() == "Watch & Ward".ToUpper() || ddlWages.SelectedItem.Text.ToUpper() == "Manager".ToUpper())
                    {
                        OTDays_EPay = "0";
                        WeekOff_Work_Days_EPay = WH_Present_Count.ToString();
                        Fixed_Work_Days_EPay = Fixed_Work_Days.ToString();
                    }
                    else
                    {
                        OTDays_EPay = WH_Present_Count.ToString();
                        WeekOff_Work_Days_EPay = "0";
                        Fixed_Work_Days_EPay = "0";
                    }
                    Spinning_Days_EPay = Spinning_Incentive_Days.ToString();
                    CanteenDays_Minus_EPay = CanteenDays_Minus_EPay;
                }
                else
                {
                    //Zero Days Update Skip
                    Days_Update_Check_blb = false;
                }


            //Check Salary Conformation
                SSQL = "Select * from [" + SessionPayroll + "]..SalaryDetails where EmpNo='" + EPay_EmpNo_Get + "' And Ccode='" + SessionCcode + "'";
                SSQL = SSQL + " And Lcode='" + SessionLcode + "' And Month='" + ddlMonth.SelectedItem.Text + "' And FinancialYear='" + Fin_Year_Spilit[0] + "'";
                SSQL = SSQL + " And FromDate = '" + Convert.ToDateTime(txtFromDate.Text).ToString("yyyy/MM/dd") + "' and ToDate = '" + Convert.ToDateTime(txtToDate.Text).ToString("yyyy/MM/dd") + "'";
                SSQL = SSQL + " And Salary_Conform='1'";
                mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);

                if (mDataSet.Rows.Count != 0)
                {
                    Days_Update_Check_blb = false;
                }


                string NFH_Doubale_Wages_Attn_Inct = (NFH_Double_Wages_Checklist + NFH_Double_Wages_Manual + NFH_WH_Days_Mins).ToString();
            //EPay Update
                if (Days_Update_Check_blb == true)
                {
                    if (EPay_EmpNo_Get != "")
                    {
                        Transfer_Check_blb = true;
                        //Epay Attendance Det Update And Insert
                        //Check Already Insert Or Not
                        SSQL = "Select * from [" + SessionPayroll + "]..AttenanceDetails where EmpNo='" + EPay_EmpNo_Get + "' And Ccode='" + SessionCcode + "'";
                        SSQL = SSQL + " And Lcode='" + SessionLcode + "' And Months='" + ddlMonth.SelectedItem.Text + "' And FinancialYear='" + Fin_Year_Spilit[0] + "'";
                        SSQL = SSQL + " and FromDate = '" + Convert.ToDateTime(txtFromDate.Text).ToString("yyyy/MM/dd") + "' and ToDate = '" + Convert.ToDateTime(txtToDate.Text).ToString("yyyy/MM/dd") + "'";
                        mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (mDataSet.Rows.Count != 0)
                        {
                            //Delete
                            SSQL = "Delete from [" + SessionPayroll + "]..AttenanceDetails where EmpNo='" + EPay_EmpNo_Get + "' And Ccode='" + SessionCcode + "'";
                            SSQL = SSQL + " And Lcode='" + SessionLcode + "' And Months='" + ddlMonth.SelectedItem.Text + "' And FinancialYear='" + Fin_Year_Spilit[0] + "'";
                            SSQL = SSQL + " and FromDate = '" + Convert.ToDateTime(txtFromDate.Text).ToString("yyyy/MM/dd") + "' and ToDate = '" + Convert.ToDateTime(txtToDate.Text).ToString("yyyy/MM/dd") + "'";
                            objdata.RptEmployeeMultipleDetails(SSQL);
                        }

                        //Insert Attn. Details
                        SSQL = "Insert Into [" + SessionPayroll + "]..AttenanceDetails(DeptName,EmpNo,ExistingCode,Days,Months,FinancialYear,CreatedDate,Years,";
                        SSQL = SSQL + "TotalDays,Ccode,Lcode,NFh,WorkingDays,CL,AbsentDays,weekoff,FromDate,ToDate,Modeval,home,";
                        SSQL = SSQL + "FullNight,ThreeSided,OTHoursNew,Fixed_Work_Days,WH_Work_Days,NFH_Work_Days,NFH_Work_Days_Incentive,NFH_Work_Days_Statutory,AEH,LBH) Values('" + EPay_Department_Get + "',";
                        SSQL = SSQL + "'" + EPay_EmpNo_Get + "','" + EPay_TokenNo_Get + "','" + Final_Work_Days_EPay + "','" + ddlMonth.SelectedItem.Text + "',";
                        SSQL = SSQL + "'" + Fin_Year_Spilit[0] + "',GetDate(),'" + Fin_Year_Spilit[0] + "',";
                        SSQL = SSQL + "'" + EPay_Total_Days_Get + "','" + SessionCcode + "','" + SessionLcode + "','" + NFH_EPay + "','" + EPay_Total_Days_Get + "',";
                        SSQL = SSQL + "'" + H_Allowed_EPay + "','0.0','0.0','" + Convert.ToDateTime(txtFromDate.Text).ToString("yyyy/MM/dd") + "',";
                        SSQL = SSQL + "'" + Convert.ToDateTime(txtToDate.Text).ToString("yyyy/MM/dd") + "','1','" + CanteenDays_Minus_EPay + "',";
                        SSQL = SSQL + "'" + Spinning_Days_EPay + "','" + OTDays_EPay + "','" + OTHours_EPay + "','" + Fixed_Work_Days_EPay + "',";
                        SSQL = SSQL + "'" + WeekOff_Work_Days_EPay + "','" + NFH_Double_Wages_Checklist + "','" + NFH_Doubale_Wages_Attn_Inct + "',";
                        SSQL = SSQL + "'" + NFH_Double_Wages_Statutory + "','" + AEH_NFH_Days_Count + "','" + LBH_NFH_Days_Count + "')";
                        objdata.RptEmployeeMultipleDetails(SSQL);
                    }
                }

                NFH_Days_Count = 0;
                NFH_Doubale_Wages_Attn_Inct = "0";
                NFH_Double_Wages_Checklist = 0;
                NFH_Double_Wages_Statutory = 0;
                NFH_Double_Wages_Manual = 0;
                NFH_WH_Days_Mins = 0;
                AEH_NFH_Days_Count = 0;
                LBH_NFH_Days_Count = 0;

           
                }

            if (Transfer_Check_blb == true)
            {
                ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "ProgressBarHide();", true);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('All Days Transfer Successfully.!');", true);
                //Clear_All_Field();
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "ProgressBarHide();", true);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Record Transfer!');", true);
            }

           
        }
        catch(Exception e)
        {

        }
        
    }

    public string Left_Val(string Value, int Length)
    {

        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }

    public string Right_Val(string Value, int Length)
    {

        int i = 0;
        i = 0;
        if (Value.Length >= Length)
        {
            //i = Value.Length - Length
            return Value.Substring(Value.Length - Length, Length);
        }
        else
        {
            return Value;
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    private void Clear_All_Field()
    {
       // ddlMonth.SelectedValue = "-Select-";
       // ddlFinyear.SelectedValue = "-Select-";
      //  ddlPayperiod.SelectedValue = "-Select-";
      //  ddlWages.SelectedValue = "0";
        ddlTokenNo.SelectedValue = "-Select-";
        txtEmpName.Text = "";
      //  txtFromDate.Text = "";
      //  txtToDate.Text = "";
        Check_AdminRights();
    }

    protected void btnIncentive_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        DataTable DT = new DataTable();
        string Month_Name_Str = "";

        //Total Days Get
        DateTime date1 = Convert.ToDateTime(txtFromDate.Text);
        DateTime date2 = Convert.ToDateTime(txtToDate.Text);
        int Total_Days_Count = (int)((date2 - date1).TotalDays);

        if (Total_Days_Count >= 0)
        {
        }
        else
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select Proper From Date And ToDate.!');", true);
        }

        if (ddlWages.SelectedItem.Text.ToUpper() != "CIVIL".ToUpper())
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Wages.!');", true);
        }

        //Check Financial Year
        int CurrentYear;
        string Fin_Year_Str;
        //Financial Year Add
        CurrentYear = Convert.ToDateTime(txtFromDate.Text).Year;
        if (Convert.ToDateTime(txtFromDate.Text).Month >= 1 && Convert.ToDateTime(txtFromDate.Text).Month <= 3)
        {
            CurrentYear = CurrentYear - 1;
        }
        else
        {
            CurrentYear = CurrentYear;
        }
        Fin_Year_Str = Convert.ToString(CurrentYear) + "-" + Convert.ToString(CurrentYear + 1);
        if (Fin_Year_Str != ddlFinyear.SelectedItem.Text)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check With Financial Year...!');", true);
        }

        if (!ErrFlag)
        {
            Civil_Incentive_Days_Transfer_To_EPay();
        }
    }

    public void Civil_Incentive_Days_Transfer_To_EPay()
    {
        try
        {

            int intI = 1;
            int intK = 1;
            int intCol;

            //Spinning Incentive Var
            string Fin_Year = "";
            string Months_Full_Str = "";
            string Date_Col_Str = "";
            int Month_Int = 1;
            string Spin_Wages = ddlWages.SelectedItem.Text;
            string Spin_Machine_ID_Str = "";


            //'Attn_Flex Col Add Var
            string Att_Already_Date_Check;
            string Att_Year_Check;
            int Month_Name_Change;
            string halfPresent = "0";
            Att_Year_Check = "";
            int EPay_Total_Days_Get = 0;
            DataTable mDataSet = new DataTable();


            intCol = 4; Month_Name_Change = 1;
            DateTime date1 = Convert.ToDateTime(txtFromDate.Text);
            DateTime date2 = Convert.ToDateTime(txtToDate.Text);
            int dayCount = (int)((date2 - date1).TotalDays);

            int daysAdded = 0;
            Att_Already_Date_Check = "";

            while (dayCount >= 0)
            {
                dayCount -= 1;
                daysAdded += 1;
            }

            EPay_Total_Days_Get = daysAdded;

            intI = 2;
            intK = 1;


            //get Employee Details
            DataTable Emp_DS = new DataTable();
            SSQL = "Select * from Employee_MST where Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And Wages='" + ddlWages.SelectedItem.Text + "' And IsActive='YES'";
            if (ddlTokenNo.SelectedItem.Text == "-Select-")
            {
                //Skip
            }
            else
            {
                SSQL = SSQL + " And ExistingCode='" + ddlTokenNo.SelectedItem.Text + "'";
            }
            SSQL = SSQL + " Order by ExistingCode Asc";
            Emp_DS = objdata.RptEmployeeMultipleDetails(SSQL);
            //If Emp_DS.Tables(0).Rows.Count = 0 Then MsgBox("No Record Transfer", MsgBoxStyle.Critical) : Exit Sub

            bool Transfer_Check_blb = false;
            //PBar_EPay.Visible = True
            //PBar_EPay.Value = 0
            //PBar_EPay.Maximum = Emp_DS.Tables(0).Rows.Count
            //PBar_EPay.Show()
            //PBar_EPay.Refresh()
            for (int intRow = 0; intRow < Emp_DS.Rows.Count; intRow++)
            {

                intK = 1;

                //Payroll EmpDet Get
                string EPay_EmpNo_Get = "";
                string EPay_Department_Get = "";
                string EPay_TokenNo_Get = "";
                string EPay_EmpName_Get = "";
                SSQL = "Select * from Employee_MST where MachineID='" + Emp_DS.Rows[intRow]["MachineID"].ToString() + "' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);
                if (mDataSet.Rows.Count != 0)
                {
                    EPay_EmpNo_Get = mDataSet.Rows[0]["EmpNo"].ToString();
                    EPay_Department_Get = mDataSet.Rows[0]["DeptName"].ToString();
                    EPay_TokenNo_Get = mDataSet.Rows[0]["ExistingCode"].ToString();
                    EPay_EmpName_Get = mDataSet.Rows[0]["FirstName"].ToString();
                }

                //Get Employee Week OF DAY
                DataTable DS_WH = new DataTable();
                string Emp_WH_Day = "";
                SSQL = "Select * from Employee_MST where MachineID='" + Emp_DS.Rows[intRow]["MachineID"].ToString() + "' And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                DS_WH = objdata.RptEmployeeMultipleDetails(SSQL);
                if (DS_WH.Rows.Count != 0)
                {
                    Emp_WH_Day = DS_WH.Rows[0]["WeekOff"].ToString();
                }
                else
                {
                    Emp_WH_Day = "";
                }

                int colIndex = intK;
                bool isPresent = false;
                decimal Present_Count = 0;
                decimal Month_WH_Count = 0;
                decimal Present_WH_Count = 0;
                int Appsent_Count = 0;
                decimal Final_Count = 0;
                decimal Total_Days_Count = 0;

                string Already_Date_Check;
                string Year_Check;
                int Days_Insert_RowVal = 0;

                decimal FullNight_Shift_Count = 0;
                int NFH_Days_Count = 0;
                Already_Date_Check = ""; Year_Check = "";

                for (intCol = 0; intCol < daysAdded; intCol++)
                {
                    isPresent = false;
                    string Emp_Total_Work_Time_1 = "00:00";
                    string Machine_ID_Str = "";
                    string OT_Week_OFF_Machine_No;
                    string Date_Value_Str = "";
                    string Total_Time_get = "";
                    string Final_OT_Work_Time_1 = "00:00";
                    double Calculate_Attd_Work_Time = 0;
                    double Calculate_Attd_Work_Time_half = 0;
                    DataTable mLocalDS = new DataTable();
                    string Time_IN_Str = "";
                    string Time_Out_Str = "";
                    //string Total_Time_get = "";
                    int j = 0;
                    double time_Check_dbl = 0;
                    string Date_Value_Str1 = "";
                    isPresent = false;

                    //Shift Change Check Variable Declaration Start
                    DateTime InTime_Check;
                    DateTime InToTime_Check;
                    TimeSpan InTime_TimeSpan;
                    string From_Time_Str = "";
                    string To_Time_Str = "";
                    DataTable DS_Time = new DataTable();
                    DataTable DS_InTime = new DataTable();
                    string Final_InTime = "";
                    string Final_OutTime = "";
                    string Final_Shift = "";
                    DataTable Shift_DS_Change = new DataTable();
                    int K = 0;
                    bool Shift_Check_blb = false;
                    string Shift_Start_Time_Change;
                    string Shift_End_Time_Change;
                    string Employee_Time_Change = "";
                    DateTime ShiftdateStartIN_Change;
                    DateTime ShiftdateEndIN_Change;
                    DateTime EmpdateIN_Change;
                    //Shift Change Check Variable Declaration End


                    Spin_Machine_ID_Str = Emp_DS.Rows[intRow]["MachineID"].ToString();

                    Machine_ID_Str = UTF8Encryption(Emp_DS.Rows[intRow]["MachineID"].ToString());
                    OT_Week_OFF_Machine_No = Emp_DS.Rows[intRow]["MachineID"].ToString();
                    Date_Value_Str = Convert.ToDateTime(txtFromDate.Text).AddDays(intCol).ToString("yyyy/MM/dd");
                    Date_Value_Str1 = Convert.ToDateTime(Date_Value_Str).AddDays(1).ToString("yyyy/MM/dd");

                    //TimeIN Get
                    // DataTable mLocalDS1 = new DataTable();
                    // SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                    // SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    // SSQL = SSQL + " And TimeIN >='" + Date_Value_Str + " " + "02:00' And TimeIN <='" + Date_Value_Str1 + " " + "02:00' Order by TimeIN ASC";
                    // mLocalDS = objdata.RptEmployeeMultipleDetails(SSQL);
                    // if (mLocalDS.Rows.Count <= 0)
                    // {
                    //     Time_IN_Str = "";
                    // }
                    // else
                    // {
                    //     //Time_IN_Str = mLocalDS.Tables(0).Rows(0)(0)
                    // }

                    // //TimeOUT Get
                    // SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                    // SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    // SSQL = SSQL + " And TimeOUT >='" + Date_Value_Str + " " + "10:00' And TimeOUT <='" + Date_Value_Str1 + " " + "10:00' Order by TimeOUT Asc";
                    // mLocalDS1 = objdata.RptEmployeeMultipleDetails(SSQL);
                    // if (mLocalDS.Rows.Count <= 0)
                    // {
                    //     Time_Out_Str = "";
                    // }
                    // else
                    // {
                    //     //Time_Out_Str = mLocalDS.Tables(0).Rows(0)(0)
                    // }

                    // //Shift Change Check Start
                    // if(mLocalDS.Rows.Count !=0)
                    // {
                    //  InTime_Check = Convert.ToDateTime(mLocalDS.Rows[0]["TimeIN"].ToString());
                    //  InToTime_Check = InTime_Check.AddHours(2);

                    //  string s1 = InTime_Check.ToString("HH:mm:ss");
                    //  string s2 = InToTime_Check.ToString("HH:mm:ss");


                    //  string InTime_Check_str = string.Format("{0:hh\\:mm\\:ss}", InTime_Check);
                    //  string InToTime_Check_str = string.Format("{0:hh\\:mm\\:ss}", InToTime_Check);
                    //  InTime_TimeSpan = TimeSpan.Parse(InTime_Check_str);
                    //  From_Time_Str = InTime_TimeSpan.Hours + ":" + InTime_TimeSpan.Minutes;
                    //  InTime_TimeSpan = TimeSpan.Parse(InToTime_Check_str);
                    //  To_Time_Str = InTime_TimeSpan.Hours + ":" + InTime_TimeSpan.Minutes;


                    // //Two Hours OutTime Check
                    // SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                    // SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    // SSQL = SSQL + " And TimeOUT >='" + Date_Value_Str + " " + From_Time_Str + "' And TimeOUT <='" + Date_Value_Str + " " + To_Time_Str + "' Order by TimeOUT Asc";
                    // DS_Time = objdata.RptEmployeeMultipleDetails(SSQL);
                    // if(DS_Time.Rows.Count != 0)
                    // {
                    //       SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                    //       SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    //       SSQL = SSQL + " And TimeIN >='" + Date_Value_Str + " " + To_Time_Str + "' And TimeIN <='" + Date_Value_Str1 + " " + "02:00' Order by TimeIN ASC";
                    //       DS_InTime = objdata.RptEmployeeMultipleDetails(SSQL);
                    // if(DS_InTime.Rows.Count!= 0)
                    // {
                    //      Final_InTime = DS_InTime.Rows[0][0].ToString();
                    //      //Check With IN Time Shift
                    //      SSQL = "Select * from Shift_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And ShiftDesc like '%SHIFT%'";
                    //      Shift_DS_Change = objdata.RptEmployeeMultipleDetails(SSQL);
                    //      Shift_Check_blb = false;
                    // for(K = 0;K < Shift_DS_Change.Rows.Count;K++)
                    // {
                    //     Shift_Start_Time_Change = Date_Value_Str + " " + Shift_DS_Change.Rows[K]["StartIN"].ToString();
                    //     if(Shift_DS_Change.Rows[K]["EndIN_Days"].ToString() == "1")
                    //     {
                    //     Shift_End_Time_Change = Date_Value_Str1 + " " + Shift_DS_Change.Rows[K]["EndIN"].ToString();
                    //     }
                    //     else
                    //     {
                    //     Shift_End_Time_Change = Date_Value_Str + " " + Shift_DS_Change.Rows[K]["EndIN"].ToString();
                    //     }

                    //    ShiftdateStartIN_Change = System.Convert.ToDateTime(Shift_Start_Time_Change);
                    //    ShiftdateEndIN_Change = System.Convert.ToDateTime(Shift_End_Time_Change);
                    //    EmpdateIN_Change = System.Convert.ToDateTime(Final_InTime);
                    //     if(EmpdateIN_Change >= ShiftdateStartIN_Change && EmpdateIN_Change <= ShiftdateEndIN_Change)
                    //     {
                    //     Final_Shift = Shift_DS_Change.Rows[K]["ShiftDesc"].ToString();
                    //     Shift_Check_blb = true;
                    //     }
                    // }

                    // if(Shift_Check_blb == true)
                    // {
                    // //IN Time Query Update
                    //     SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                    //     SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    //     SSQL = SSQL + " And TimeIN >='" + Date_Value_Str + " " + To_Time_Str + "' And TimeIN <='" + Date_Value_Str1 + " " + "02:00' Order by TimeIN ASC";
                    //     mLocalDS = objdata.RptEmployeeMultipleDetails(SSQL);
                    // if((Final_Shift).ToUpper() == ("SHIFT2").ToUpper())
                    // {
                    //     SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                    //     SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    //     SSQL = SSQL + " And TimeOUT >='" + Date_Value_Str + " " + "13:00' And TimeOUT <='" + Date_Value_Str1 + " " + "03:00' Order by TimeOUT Asc";
                    // }
                    // else
                    // {
                    //    SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                    //    SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    //    SSQL = SSQL + " And TimeOUT >='" + Date_Value_Str + " " + "17:40' And TimeOUT <='" + Date_Value_Str1 + " " + "10:00' Order by TimeOUT Asc";
                    // }
                    // mLocalDS1 = objdata.RptEmployeeMultipleDetails(SSQL);
                    // }
                    // else
                    // {
                    //   //Get Employee In Time
                    //     SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                    //     SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    //     SSQL = SSQL + " And TimeIN >='" + Date_Value_Str + " " + "02:00' And TimeIN <='" + Date_Value_Str1 + " " + "02:00' Order by TimeIN ASC";
                    //     mLocalDS = objdata.RptEmployeeMultipleDetails(SSQL);
                    //     //TimeOUT Get
                    //     SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                    //     SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    //     SSQL = SSQL + " And TimeOUT >='" + Date_Value_Str + " " + "10:00' And TimeOUT <='" + Date_Value_Str1 + " " + "10:00' Order by TimeOUT Asc";
                    //     mLocalDS1 =objdata.RptEmployeeMultipleDetails(SSQL);
                    // }
                    // }
                    // else
                    //     {
                    //         //Get Employee In Time
                    //         SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                    //         SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    //         SSQL = SSQL + " And TimeIN >='" + Date_Value_Str + " " + "02:00' And TimeIN <='" + Date_Value_Str1 + " " + "02:00' Order by TimeIN ASC";
                    //         mLocalDS = objdata.RptEmployeeMultipleDetails(SSQL);

                    //         //TimeOUT Get
                    //         SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                    //         SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    //         SSQL = SSQL + " And TimeOUT >='" + Date_Value_Str + " " + "10:00' And TimeOUT <='" + Date_Value_Str1 + " " + "10:00' Order by TimeOUT Asc";
                    //         mLocalDS1 = objdata.RptEmployeeMultipleDetails(SSQL);
                    // }
                    // }
                    //else
                    // {
                    //     //Get Employee In Time
                    //     SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                    //     SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    //     SSQL = SSQL + " And TimeIN >='" + Date_Value_Str + " " + "02:00' And TimeIN <='" + Date_Value_Str1 + " " + "02:00' Order by TimeIN ASC";
                    //     mLocalDS = objdata.RptEmployeeMultipleDetails(SSQL);

                    //     //TimeOUT Get
                    //     SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                    //     SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    //     SSQL = SSQL + " And TimeOUT >='" + Date_Value_Str + " " + "10:00' And TimeOUT <='" + Date_Value_Str1 + " " + "10:00' Order by TimeOUT Asc";
                    //     mLocalDS1 = objdata.RptEmployeeMultipleDetails(SSQL);
                    // }
                    // }
                    //     //Shift Change Check End

                    // if (mLocalDS.Rows.Count > 1)
                    // {
                    //     for (int tin = 0; tin < mLocalDS.Rows.Count; tin++)
                    //     {
                    //         Time_IN_Str = mLocalDS.Rows[tin][0].ToString();

                    //         if (mLocalDS1.Rows.Count > tin)
                    //         {
                    //             Time_Out_Str = mLocalDS1.Rows[tin][0].ToString();
                    //         }
                    //         else if (mLocalDS1.Rows.Count > mLocalDS.Rows.Count)
                    //         {
                    //             Time_Out_Str = mLocalDS1.Rows[mLocalDS1.Rows.Count - 1][0].ToString();
                    //         }
                    //         else
                    //         {
                    //             Time_Out_Str = "";
                    //         }
                    //         TimeSpan ts4;
                    //         ts4 = Convert.ToDateTime(String.Format("{0:hh:mm tt}", Emp_Total_Work_Time_1)).TimeOfDay;
                    //         if (mLocalDS.Rows.Count <= 0)
                    //         {
                    //             Time_IN_Str = "";
                    //         }
                    //         else
                    //         {
                    //             Time_IN_Str = mLocalDS.Rows[tin][0].ToString();
                    //         }
                    //         if (Time_IN_Str == "" || Time_Out_Str == "")
                    //         {
                    //             time_Check_dbl = time_Check_dbl;
                    //         }
                    //         else
                    //         {
                    //             date1 = System.Convert.ToDateTime(Time_IN_Str);
                    //             date2 = System.Convert.ToDateTime(Time_Out_Str);

                    //             TimeSpan ts;
                    //             ts = date2.Subtract(date1);
                    //             ts = date2.Subtract(date1);
                    //             Total_Time_get = (ts.Hours).ToString().Trim();
                    //             ts4 = ts4.Add(ts);
                    //             //OT Time Get
                    //             Emp_Total_Work_Time_1 = (ts4.Hours).ToString().Trim() + ":" + (ts4.Minutes).ToString().Trim();
                    //             if (Left_Val((ts4.Minutes).ToString().Trim(), 1) == "-" | Left_Val((ts4.Hours).ToString().Trim(), 1) == "-")
                    //             {
                    //                 Emp_Total_Work_Time_1 = "00:00";
                    //             }

                    //             if (Left_Val(Total_Time_get, 1) == "-")
                    //             {
                    //                 date2 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                    //                 ts = date2.Subtract(date1);
                    //                 ts = date2.Subtract(date1);
                    //                 Total_Time_get = (ts.Hours).ToString().Trim(); //& ":" & Trim(ts.Minutes)
                    //                 time_Check_dbl = Convert.ToDouble(Total_Time_get);
                    //                 Emp_Total_Work_Time_1 = (ts.Hours).ToString().Trim() + ":" + (ts.Minutes).ToString().Trim();
                    //                 Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                    //                 if ((Left_Val(Time_Minus_Value_Check[0], 1) == "-") || (Left_Val(Time_Minus_Value_Check[1], 1) == "-"))
                    //                 {
                    //                     Emp_Total_Work_Time_1 = "00:00";
                    //                 }
                    //                 if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                    //                 {
                    //                     Emp_Total_Work_Time_1 = "00:00";
                    //                 }
                    //             }
                    //             else
                    //             {
                    //                 time_Check_dbl = time_Check_dbl + Convert.ToDouble(Total_Time_get);
                    //             }
                    //         }
                    //     }
                    // }
                    // else
                    // {
                    //     TimeSpan ts4;
                    //     ts4 = Convert.ToDateTime(String.Format("{0:hh:mm tt}", Emp_Total_Work_Time_1)).TimeOfDay;

                    //     if (mLocalDS.Rows.Count <= 0)
                    //     {
                    //         Time_IN_Str = "";
                    //     }
                    //     else
                    //     {
                    //         Time_IN_Str = mLocalDS.Rows[0][0].ToString();
                    //     }
                    //     for (int tout = 0; tout < mLocalDS1.Rows.Count; tout++)
                    //     {
                    //         if (mLocalDS1.Rows.Count <= 0)
                    //         {
                    //             Time_Out_Str = "";
                    //         }
                    //         else
                    //         {
                    //             Time_Out_Str = mLocalDS1.Rows[0][0].ToString();
                    //         }

                    //     }
                    //     //Emp_Total_Work_Time
                    //     if (Time_IN_Str == "" || Time_Out_Str == "")
                    //     {
                    //         time_Check_dbl = 0;
                    //     }
                    //     else
                    //     {
                    //         date1 = System.Convert.ToDateTime(Time_IN_Str);
                    //         date2 = System.Convert.ToDateTime(Time_Out_Str);
                    //         TimeSpan ts;
                    //         ts = date2.Subtract(date1);
                    //         ts = date2.Subtract(date1);
                    //         Total_Time_get = (ts.Hours).ToString().Trim(); //'& ":" & Trim(ts.Minutes)
                    //         ts4 = ts4.Add(ts);
                    //         //OT Time Get
                    //         Emp_Total_Work_Time_1 = (ts4.Hours).ToString().Trim() + ":" + (ts4.Minutes).ToString().Trim();
                    //         Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                    //         if ((Left_Val(Time_Minus_Value_Check[0], 1) == "-") || (Left_Val(Time_Minus_Value_Check[1], 1) == "-"))
                    //         {
                    //             Emp_Total_Work_Time_1 = "00:00";
                    //         }
                    //         if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                    //         {
                    //             Emp_Total_Work_Time_1 = "00:00";
                    //         }
                    //         if (Left_Val(Total_Time_get, 1) == "-")
                    //         {
                    //             date2 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                    //             ts = date2.Subtract(date1);
                    //             ts = date2.Subtract(date1);
                    //             Total_Time_get = (ts.Hours).ToString().Trim(); //'& ":" & Trim(ts.Minutes)
                    //             time_Check_dbl = Convert.ToDouble(Total_Time_get);
                    //             // 'Total_Time_get = Right_Val(Total_Time_get, Len(Total_Time_get) - 1)
                    //             Emp_Total_Work_Time_1 = (ts.Hours).ToString().Trim() + ":" + (ts.Minutes).ToString().Trim();
                    //             Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                    //             if ((Left_Val(Time_Minus_Value_Check[0], 1) == "-") || (Left_Val(Time_Minus_Value_Check[1], 1) == "-"))
                    //             {
                    //                 Emp_Total_Work_Time_1 = "00:00";
                    //             }
                    //             if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                    //             {
                    //                 Emp_Total_Work_Time_1 = "00:00";
                    //             }
                    //         }
                    //         else
                    //         {
                    //             time_Check_dbl = Convert.ToDouble(Total_Time_get);
                    //         }
                    //     }
                    // }


                    //Emp_Total_Work_Time
                    string Emp_Total_Work_Time = "";
                    string Final_OT_Work_Time = "00:00";
                    string Final_OT_Work_Time_Val = "00:00";
                    Emp_Total_Work_Time = Emp_Total_Work_Time_1;
                    //Find Week OFF
                    string Employee_Week_Name = "";
                    string Assign_Week_Name = "";
                    //    mLocalDS = Nothing
                    Employee_Week_Name = Convert.ToDateTime(Date_Value_Str).DayOfWeek.ToString();
                    SSQL = "Select WeekOff from Shiftrrecording where MachineNo='" + OT_Week_OFF_Machine_No + "'";
                    mLocalDS = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (mLocalDS.Rows.Count <= 0)
                    {
                        Assign_Week_Name = "";
                    }
                    else
                    {
                        //Assign_Week_Name = mLocalDS.Tables(0).Rows(0)(0)
                        Assign_Week_Name = "";
                    }
                    string qry_nfh = "Select NFHDate from NFH_Mst where NFHDate=convert(varchar,'" + Date_Value_Str + "',103)";
                    mLocalDS = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (mLocalDS.Rows.Count == 12)  //'nfh nOT cHECK
                    {
                        isPresent = true;
                        NFH_Days_Count = NFH_Days_Count + 1;
                    }
                    else
                    {
                        if ((Employee_Week_Name).ToUpper() == (Assign_Week_Name).ToUpper())
                        {
                            //'Skip
                        }
                        else
                        {
                            //Get Employee Work Time
                            Calculate_Attd_Work_Time = 0;
                            Calculate_Attd_Work_Time_half = 0;
                            SSQL = "Select * from Employee_MST where MachineID='" + OT_Week_OFF_Machine_No + "' and Calculate_Work_Hours <> '' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And IsActive='Yes'";
                            mLocalDS = objdata.RptEmployeeMultipleDetails(SSQL);
                            if (mLocalDS.Rows.Count <= 0)
                            {
                                Calculate_Attd_Work_Time = 7;
                            }
                            else
                            {
                                Calculate_Attd_Work_Time = (!string.IsNullOrEmpty(mLocalDS.Rows[0]["Calculate_Work_Hours"].ToString()) ? Convert.ToInt16(mLocalDS.Rows[0]["Calculate_Work_Hours"]) : 0);

                                if (Calculate_Attd_Work_Time == 0)
                                {
                                    Calculate_Attd_Work_Time = 7;
                                }
                                else
                                {
                                    Calculate_Attd_Work_Time = 7;
                                }
                            }


                            string Wages_Name_Check = "";
                            DataTable MDS_DS = new DataTable();
                            SSQL = "Select * from Employee_MST where MachineID='" + OT_Week_OFF_Machine_No + "' and CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                            MDS_DS = objdata.RptEmployeeMultipleDetails(SSQL);
                            if (MDS_DS.Rows.Count != 0)
                            {
                                Wages_Name_Check = MDS_DS.Rows[0]["Wages"].ToString();
                            }

                            if ((Wages_Name_Check).ToUpper() == ("STAFF").ToUpper() || (Wages_Name_Check).ToUpper() == ("Watch & Ward").ToUpper() || (Wages_Name_Check).ToUpper() == ("Manager").ToUpper())
                            {
                                Calculate_Attd_Work_Time = 8;
                            }
                            else
                            {
                                Calculate_Attd_Work_Time = 8;
                            }

                            string qry_Days = "Select Present from LogTime_Days where MachineID='" + OT_Week_OFF_Machine_No + "' And Attn_Date=convert(varchar,'" + Date_Value_Str + "',103) And LocCode='" + SessionLcode + "' And CompCode='" + SessionCcode + "'";
                            mLocalDS = objdata.RptEmployeeMultipleDetails(qry_Days);
                            if (mLocalDS.Rows.Count != 0)
                            {
                                string Present = "0";
                                Present = mLocalDS.Rows[0]["Present"].ToString();
                                if (Present == "0.5")
                                {
                                    isPresent = true;
                                    halfPresent = "1";
                                    time_Check_dbl = 4;
                                }
                                else if (Present == "1.0")
                                {
                                    isPresent = true;
                                    halfPresent = "0";
                                    time_Check_dbl = 8;
                                }
                                else
                                {
                                    time_Check_dbl = 0;
                                }
                            }

                            //Calculate_Attd_Work_Time_half = 4.0;
                            //halfPresent = "0"; //Half Days Calculate Start
                            //if (time_Check_dbl >= Calculate_Attd_Work_Time_half && time_Check_dbl < Calculate_Attd_Work_Time)
                            //{
                            //    isPresent = true;
                            //    halfPresent = "1";
                            //}
                            ////Half Days Calculate End
                            //if (time_Check_dbl >= Calculate_Attd_Work_Time)
                            //{
                            //    isPresent = true;
                            //    halfPresent = "0";
                            //}
                        }
                    }
                    //Week OF Day Check
                    string Attn_Date_Day = DateTime.Parse(Date_Value_Str).DayOfWeek.ToString();
                    if ((Emp_WH_Day).ToUpper() == (Attn_Date_Day).ToUpper())
                    {
                        Month_WH_Count = Month_WH_Count + 1;
                        if (isPresent == true)
                        {
                            if (halfPresent == "1")
                            {
                                Present_WH_Count = Present_WH_Count + Convert.ToDecimal(0.5);
                            }
                            else if (halfPresent == "0")
                            {
                                Present_WH_Count = Present_WH_Count + 1;
                            }
                        }
                    }

                    if (isPresent == true)
                    {
                        if (halfPresent == "1")
                        {
                            Present_Count = Present_Count + Convert.ToDecimal(0.5);
                        }
                        else if (halfPresent == "0")
                        {
                            Present_Count = Present_Count + 1;
                        }
                    }
                    else
                    {
                        Appsent_Count = Appsent_Count + 1;
                    }
                    //    'colIndex += shiftCount
                    intK += 1;
                    Total_Days_Count = Total_Days_Count + 1;
                }
                Final_Count = Present_Count;
                //'Final_Count = Total_Days_Count - Appsent_Count
                //'Update Employee Token No
                string Token_No_Get = "";
                string Machine_ID_Str_Excel = "";
                DataTable Token_DS = new DataTable();
                //'MsgBox(.Cells(intI, 1).value)
                int Fixed_Work_Days = 0;

                if (ddlWages.SelectedItem.Text.ToUpper() == ("STAFF").ToUpper() || ddlWages.SelectedItem.Text.ToUpper() == ("Watch & Ward").ToUpper() || ddlWages.SelectedItem.Text.ToUpper() == ("Manager").ToUpper())
                {
                    Fixed_Work_Days = Convert.ToInt32(Total_Days_Count - Month_WH_Count);
                    Final_Count = Final_Count - Present_WH_Count;
                }
                else
                {
                    Fixed_Work_Days = 0;
                    Final_Count = Final_Count - Present_WH_Count;
                }
                Final_Count = Final_Count - NFH_Days_Count;

                //'Epay Transfer Var Declaration
                string Machine_ID_EPay = "";
                string Token_No_EPay = "";
                string EmpName_EPay = "";
                string Final_Work_Days_EPay = "0";
                string H_Allowed_EPay = "0";
                string NFH_EPay = "0";
                string OTDays_EPay = "0";
                string Spinning_Days_EPay = "0";
                string CanteenDays_Minus_EPay = "0";
                string OTHours_EPay = "0";
                string WeekOff_Work_Days_EPay = "0";
                string Fixed_Work_Days_EPay = "0";
                bool Days_Update_Check_blb = false;
                string[] Fin_Year_Spilit;
                Fin_Year_Spilit = ddlFinyear.SelectedItem.Text.Split('-');
                Final_Work_Days_EPay = "0";
                if (Final_Count != 0)
                {
                    Machine_ID_EPay = Emp_DS.Rows[intRow]["MachineID"].ToString();
                    EmpName_EPay = Emp_DS.Rows[intRow]["MachineID"].ToString();

                    Final_Work_Days_EPay = (Final_Count + Present_WH_Count + Convert.ToDecimal(NFH_Days_Count)).ToString();
                    Days_Update_Check_blb = true;
                }
                else
                {
                    //Zero Days Update Skip
                    Days_Update_Check_blb = false;
                }

                //'EPay Update
                if (Days_Update_Check_blb == true)
                {
                    Machine_ID_EPay = Emp_DS.Rows[intRow]["MachineID"].ToString();
                    if (EPay_EmpNo_Get != "")
                    {
                        Transfer_Check_blb = true;
                        //'Epay Civil Incentive Days Det Update And Insert
                        //'Check Already Insert Or Not
                        SSQL = "Select * from [" + SessionPayroll + "]..eAlert_Civil_Incent_Days_Det where EmpNo='" + EPay_EmpNo_Get + "' And Ccode='" + SessionCcode + "'";
                        SSQL = SSQL + " And Lcode='" + SessionLcode + "' And Month='" + ddlMonth.SelectedItem.Text + "' And FinancialYear='" + Fin_Year_Spilit[0] + "'";
                        SSQL = SSQL + " and FromDate = '" + Convert.ToDateTime(txtFromDate.Text).ToString("yyyy/MM/dd") + "' and ToDate = '" + Convert.ToDateTime(txtToDate.Text).ToString("yyyy/MM/dd") + "'";
                        mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (mDataSet.Rows.Count != 0)
                        {
                            //'Delete
                            SSQL = "Delete from [" + SessionPayroll + "]..eAlert_Civil_Incent_Days_Det where EmpNo='" + EPay_EmpNo_Get + "' And Ccode='" + SessionCcode + "'";
                            SSQL = SSQL + " And Lcode='" + SessionLcode + "' And Month='" + ddlMonth.SelectedItem.Text + "' And FinancialYear='" + Fin_Year_Spilit[0] + "'";
                            SSQL = SSQL + " and FromDate = '" + Convert.ToDateTime(txtFromDate.Text).ToString("yyyy/MM/dd") + "' and ToDate = '" + Convert.ToDateTime(txtToDate.Text).ToString("yyyy/MM/dd") + "'";
                            objdata.RptEmployeeMultipleDetails(SSQL);
                        }
                        //'Insert Civil Incentive Days Details
                        SSQL = "Insert Into [" + SessionPayroll + "]..eAlert_Civil_Incent_Days_Det(Ccode,Lcode,Month,FinancialYear,Wages,MachineID,ExisistingCode,EmpName,";
                        SSQL = SSQL + "EmpNo,Incent_Days,FromDate_Str,FromDate,ToDate_Str,ToDate) Values('" + SessionCcode + "','" + SessionLcode + "',";
                        SSQL = SSQL + "'" + ddlMonth.SelectedItem.Text + "','" + Fin_Year_Spilit[0] + "','" + ddlWages.SelectedItem.Text + "',";
                        SSQL = SSQL + "'" + Machine_ID_EPay + "','" + EPay_TokenNo_Get + "','" + EPay_EmpName_Get + "',";
                        SSQL = SSQL + "'" + EPay_EmpNo_Get + "','" + Final_Work_Days_EPay + "','" + txtFromDate.Text + "',";
                        SSQL = SSQL + "'" + Convert.ToDateTime(txtFromDate.Text).ToString("yyyy/MM/dd") + "','" + txtToDate.Text + "',";
                        SSQL = SSQL + "'" + Convert.ToDateTime(txtToDate.Text).ToString("yyyy/MM/dd") + "')";
                        objdata.RptEmployeeMultipleDetails(SSQL);
                    }
                }
                //PBar_EPay.Value += 1
                //PBar_EPay.Refresh()

                intI += 1;
                Total_Days_Count = 0;
                Final_Count = 0;
                Appsent_Count = 0;
                Present_Count = 0;
                Fixed_Work_Days = 0;
                Month_WH_Count = 0;
                Present_WH_Count = 0;
            }
            if (Transfer_Check_blb == true)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('All Civil Incentive Days Transfer Successfully.!');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Record Transfer.!');", true);
            }
            //PBar_EPay.Visible = False
            //Exit Sub
        }
        catch (Exception e)
        {
        }

    }

    private static string UTF8Encryption_OLD(string password)
    {
        string strmsg = string.Empty;
        byte[] encode = new byte[password.Length];
        encode = Encoding.UTF8.GetBytes(password);
        strmsg = Convert.ToBase64String(encode);
        return strmsg;
    }

    public string UTF8Encryption(string mvarPlanText)
    {
        string cipherText = "";
        try
        {
            string passPhrase = "Altius";
            string saltValue = "info@altius.co.in";
            string hashAlgorithm = "SHA1";
            string initVector = "@1B2c3D4e5F6g7H8";
            int passwordIterations = 2;
            int keySize = 256;
            byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
            byte[] saltValueBytes = Encoding.ASCII.GetBytes(saltValue);
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(mvarPlanText);
            PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, saltValueBytes, hashAlgorithm, passwordIterations);
            byte[] keyBytes = password.GetBytes(keySize / 8);
            RijndaelManaged symmetricKey = new RijndaelManaged();
            symmetricKey.Mode = CipherMode.CBC;
            ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes);
            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
            cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
            cryptoStream.FlushFinalBlock();
            byte[] cipherTextBytes = memoryStream.ToArray();
            memoryStream.Close();
            cryptoStream.Close();
            cipherText = Convert.ToBase64String(cipherTextBytes);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }

        return cipherText;
    }

    private static string UTF8Decryption(string encryptpwd)
    {
        string decryptpwd = string.Empty;
        UTF8Encoding encodepwd = new UTF8Encoding();
        Decoder Decode = encodepwd.GetDecoder();
        byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
        int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
        char[] decoded_char = new char[charCount];
        Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
        decryptpwd = new String(decoded_char);
        return decryptpwd;
    }

    protected void btnConform_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        DataTable DT = new DataTable();
        string Month_Name_Str = "";

        //Total Days Get
        DateTime date1 = Convert.ToDateTime(txtFromDate.Text);
        DateTime date2 = Convert.ToDateTime(txtToDate.Text);
        int Total_Days_Count = (int)((date2 - date1).TotalDays);

        if (Total_Days_Count >= 0)
        {
        }
        else
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select Proper From Date And ToDate.!');", true);
        }

     //Check Month
      
        if(ddlWages.SelectedItem.Text.ToUpper() == ("STAFF").ToUpper() || ddlWages.SelectedItem.Text.ToUpper() == ("SUB-STAFF").ToUpper() || ddlWages.SelectedItem.Text.ToUpper() == ("Watch & Ward").ToUpper() || ddlWages.SelectedItem.Text.ToUpper() == ("REGULAR").ToUpper() || ddlWages.SelectedItem.Text.ToUpper() == ("HOSTEL").ToUpper() || ddlWages.SelectedItem.Text.ToUpper() == ("Manager").ToUpper())
        {
            //FromDate Check
            DateTime FrmDate = Convert.ToDateTime(txtFromDate.Text);
            //Month_Name_Str = FrmDate.Month.ToString();

            if (FrmDate.Month.ToString() == "1")
            {
                Month_Name_Str = "January";
            }
            else if (FrmDate.Month.ToString() == "2")
            {
                Month_Name_Str = "February";
            }
            else if (FrmDate.Month.ToString() == "3")
            {
                Month_Name_Str = "March";
            }
            else if (FrmDate.Month.ToString() == "4")
            {
                Month_Name_Str = "April";
            }
            else if (FrmDate.Month.ToString() == "5")
            {
                Month_Name_Str = "May";
            }
            else if (FrmDate.Month.ToString() == "6")
            {
                Month_Name_Str = "June";
            }
            else if (FrmDate.Month.ToString() == "7")
            {
                Month_Name_Str = "July";
            }
            else if (FrmDate.Month.ToString() == "8")
            {
                Month_Name_Str = "August";
            }
            else if (FrmDate.Month.ToString() == "9")
            {
                Month_Name_Str = "September";
            }
            else if (FrmDate.Month.ToString() == "10")
            {
                Month_Name_Str = "October";
            }
            else if (FrmDate.Month.ToString() == "11")
            {
                Month_Name_Str = "November";
            }
            else if (FrmDate.Month.ToString() == "12")
            {
                Month_Name_Str = "December";
            }

            if ((Month_Name_Str).ToUpper() != ddlMonth.SelectedItem.Text.ToUpper())
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with MONTH NAME AND FROM DATE.!');", true);
                txtFromDate.Focus();
            }

            //ToDate Month Name Check
            DateTime ToDate = Convert.ToDateTime(txtToDate.Text);
            //Month_Name_Str = FrmDate.Month.ToString();

            if (ToDate.Month.ToString() == "1")
            {
                Month_Name_Str = "January";
            }
            else if (ToDate.Month.ToString() == "2")
            {
                Month_Name_Str = "February";
            }
            else if (ToDate.Month.ToString() == "3")
            {
                Month_Name_Str = "March";
            }
            else if (ToDate.Month.ToString() == "4")
            {
                Month_Name_Str = "April";
            }
            else if (ToDate.Month.ToString() == "5")
            {
                Month_Name_Str = "May";
            }
            else if (ToDate.Month.ToString() == "6")
            {
                Month_Name_Str = "June";
            }
            else if (ToDate.Month.ToString() == "7")
            {
                Month_Name_Str = "July";
            }
            else if (ToDate.Month.ToString() == "8")
            {
                Month_Name_Str = "August";
            }
            else if (ToDate.Month.ToString() == "9")
            {
                Month_Name_Str = "September";
            }
            else if (ToDate.Month.ToString() == "10")
            {
                Month_Name_Str = "October";
            }
            else if (ToDate.Month.ToString() == "11")
            {
                Month_Name_Str = "November";
            }
            else if (ToDate.Month.ToString() == "12")
            {
                Month_Name_Str = "December";
            }

            if ((Month_Name_Str).ToUpper() != ddlMonth.SelectedItem.Text.ToUpper())
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with MONTH NAME AND TO DATE.!');", true);
                txtToDate.Focus();
            }


            //Date Check
            int days = DateTime.DaysInMonth(Convert.ToDateTime(txtFromDate.Text).Year, Convert.ToDateTime(txtFromDate.Text).Month);

            if (Left_Val(txtFromDate.Text.ToString(), 2) != "01")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check FROM DATE.!');", true);
                txtFromDate.Focus();
            }
            if (days.ToString() != Left_Val(txtToDate.Text.ToString(), 2))
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check TO DATE.!');", true);
                txtToDate.Focus();
            }

            //Check Financial Year
            int CurrentYear;
            string Fin_Year_Str;
            //Financial Year Add
            CurrentYear = Convert.ToDateTime(txtFromDate.Text).Year;
            if (Convert.ToDateTime(txtFromDate.Text).Month >= 1 && Convert.ToDateTime(txtFromDate.Text).Month <= 3)
            {
                CurrentYear = CurrentYear - 1;
            }
            else
            {
                CurrentYear = CurrentYear;
            }
            Fin_Year_Str = Convert.ToString(CurrentYear) + "-" + Convert.ToString(CurrentYear + 1);
            if (Fin_Year_Str != ddlFinyear.SelectedItem.Text)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check With Financial Year...!');", true);
            }

           
        }

        if (!ErrFlag)
        {
            //'Update Salary Conformation
            string[] Fin_Year_Spilit;
            string Employee_Type_Str = "";
           

            Fin_Year_Spilit = ddlFinyear.SelectedItem.Text.Split('-');

            if (ddlWages.SelectedItem.Text.ToUpper() == ("STAFF").ToUpper())
            {
                Employee_Type_Str = "1";
            }
            if (ddlWages.SelectedItem.Text.ToUpper() == ("SUB-STAFF").ToUpper())
            {
                Employee_Type_Str = "2";
            }
            if (ddlWages.SelectedItem.Text.ToUpper() == ("Watch & Ward").ToUpper())
            {
                Employee_Type_Str = "6";
            }
            if (ddlWages.SelectedItem.Text.ToUpper() == ("REGULAR").ToUpper())
            {
                Employee_Type_Str = "3";
            }
            if (ddlWages.SelectedItem.Text.ToUpper() == ("HOSTEL").ToUpper())
            {
                Employee_Type_Str = "4";
            }
            if (ddlWages.SelectedItem.Text.ToUpper() == ("CIVIL").ToUpper())
            {
                Employee_Type_Str = "5";
            }
            if (ddlWages.SelectedItem.Text.ToUpper() == ("Manager").ToUpper())
            {
                Employee_Type_Str = "7";
            }
           
            //'get Employee Type

            SSQL = "UPDATE SD SET SD.Salary_Conform = '1' FROM [" + SessionPayroll + "]..SalaryDetails AS SD";
            SSQL = SSQL + " INNER JOIN [" + SessionPayroll + "]..EmployeeDetails AS ED ON ED.EmpNo = SD.EmpNo And ED.Lcode=SD.Lcode";
            SSQL = SSQL + " WHERE ED.EmployeeType='" + Employee_Type_Str + "' And SD.Lcode = '" + SessionLcode + "'";
            SSQL = SSQL + " And SD.Ccode='" + SessionCcode + "' And SD.Month='" + ddlMonth.SelectedItem.Text + "'";
            SSQL = SSQL + " And SD.FromDate = '" + Convert.ToDateTime(txtFromDate.Text).ToString("yyyy/MM/dd") + "'";
            SSQL = SSQL + " And SD.ToDate = '" + Convert.ToDateTime(txtToDate.Text).ToString("yyyy/MM/dd") + "'";
            SSQL = SSQL + " And SD.FinancialYear='" + Fin_Year_Spilit[0] + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('SALARY CONFORMATION UPDATED.!');", true);

            Clear_All_Field();

            //MsgBox("SALARY CONFORMATION UPDATED", MsgBoxStyle.Information)
            //Exit Sub
        }
    }
    protected void btnVoucher_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        DataTable DT = new DataTable();
        string Month_Name_Str = "";
        string query = "";

        if (ddlWages.SelectedItem.Text == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select Wages Type..!');", true);

        }

        if (ddlFinyear.SelectedItem.Text == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select Finance Year..!');", true);

        }
        if (ddlMonth.SelectedItem.Text == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select Month..!');", true);

        }


        if (!ErrFlag)
        {

            //'Update Salary Conformation
            string[] Fin_Year_Spilit;
            string Employee_Type_Str = "";
            string Year1 = "";

            Fin_Year_Spilit = ddlFinyear.SelectedItem.Text.Split('-');

            int Month_Int = 0;
            Month_Int = Convert.ToDateTime(txtFromDate.Text).Month;

            Year1 = Convert.ToDateTime(txtFromDate.Text).Year.ToString();




            if (ddlWages.SelectedItem.Text.ToUpper() == ("MONTHLY - STAFF").ToUpper())
            {
                Employee_Type_Str = "1";
            }


            if (ddlWages.SelectedItem.Text.ToUpper() == ("WEEKLY-HINDI").ToUpper())
            {
                Employee_Type_Str = "4";
            }


            if (ddlWages.SelectedItem.Text.ToUpper() == ("WEEKLY").ToUpper())
            {
                Employee_Type_Str = "2";
            }

            //'get Employee 
            DataTable DT_Emp = new DataTable();
            SSQL = "Select ED.FirstName,SD.ExisistingCode,SD.MachineNo,ED.Salary_Through as Salarythrough,ED.Wages as EmployeeType from [SVT_Epay]..SalaryDetails SD inner join Employee_Mst ED";
            SSQL = SSQL + " on SD.ExisistingCode COLLATE Latin1_General_CI_AS=ED.ExistingCode COLLATE Latin1_General_CI_AS";
            SSQL = SSQL + " where SD.Ccode='" + SessionCcode + "' And SD.Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And ED.CompCode='" + SessionCcode + "' And ED.LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And ED.Wages='" + ddlWages.SelectedItem.Text + "'";
            SSQL = SSQL + " And SD.Month='" + ddlMonth.SelectedItem.Text + "' And SD.FinancialYear='" + Fin_Year_Spilit[0] + "'";
            SSQL = SSQL + " And SD.FromDate='" + Convert.ToDateTime(txtFromDate.Text).ToString("yyyy/MM/dd") + "'";
            SSQL = SSQL + " And SD.ToDate='" + Convert.ToDateTime(txtToDate.Text).ToString("yyyy/MM/dd") + "'";
            if (ddlWages.SelectedItem.Text.ToUpper() == ("REGULAR").ToUpper())
            {
                SSQL = SSQL + " And ED.Salary_Through='1'";
            }
            if (ddlTokenNo.SelectedItem.Text != "-Select-")
            {
                SSQL = SSQL + " And ED.ExistingCode='" + ddlTokenNo.SelectedItem.Text + "'";
            }
            DT_Emp = objdata.RptEmployeeMultipleDetails(SSQL);

            string Payroll_EmpNo = "";
            string Payroll_EmployeeType = "";
            string Payroll_SalaryType = "";

            double Gross_Earnings = 0;
            double Basic_Salary = 0;
            double Extra_Allowence = 0;
            double Total_Deduction = 0;
            double Bank_Salary = 0;
            double Bank_Emp_Salary = 0;
            double NFH_Worked_Days = 0;
            double NFH_Worked_Days_Statutory = 0;
            double NFH_Worked_Amt = 0;
            double NFH_Worked_Amt_Statutory = 0;
            double Without_Round_off_Net_Amount = 0;
            double With_Round_Off_Net_Amount = 0;
            double Final_Display_Net_Pay = 0;
            string EmpName = "";
            string MachineNo = "";


            for (int i = 0; i < DT_Emp.Rows.Count; i++)
            {
                Gross_Earnings = 0;
                Basic_Salary = 0;
                Extra_Allowence = 0;
                Total_Deduction = 0;
                Bank_Salary = 0;
                Bank_Emp_Salary = 0;
                NFH_Worked_Days = 0;
                NFH_Worked_Days_Statutory = 0;
                NFH_Worked_Amt = 0;
                NFH_Worked_Amt_Statutory = 0;
                Without_Round_off_Net_Amount = 0;
                With_Round_Off_Net_Amount = 0;
                Final_Display_Net_Pay = 0;

                Payroll_EmpNo = DT_Emp.Rows[i]["ExisistingCode"].ToString();
                EmpName = DT_Emp.Rows[i]["FirstName"].ToString();
                MachineNo = DT_Emp.Rows[i]["MachineNo"].ToString();



                if (DT_Emp.Rows[i]["EmployeeType"].ToString().ToUpper() == ("STAFF").ToUpper()) { Payroll_EmployeeType = "1"; }
          
                if (DT_Emp.Rows[i]["EmployeeType"].ToString().ToUpper() == ("HINDI BOYS").ToUpper()) { Payroll_EmployeeType = "4"; }
               
                if (DT_Emp.Rows[i]["EmployeeType"].ToString().ToUpper() == ("WEEKLY").ToUpper()) { Payroll_EmployeeType = "2"; }

                Payroll_SalaryType = DT_Emp.Rows[i]["Salarythrough"].ToString();

                DataTable NFH_DT = new DataTable();
                //Get NFH_Worked Days
                SSQL = "Select * from [SVT_Epay]..AttenanceDetails where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And ExistingCode='" + Payroll_EmpNo + "' And Months='" + ddlMonth.SelectedItem.Text + "' And FinancialYear='" + Fin_Year_Spilit[0] + "'";
                SSQL = SSQL + " And FromDate='" + Convert.ToDateTime(txtFromDate.Text).ToString("yyyy/MM/dd") + "'";
                SSQL = SSQL + " And ToDate='" + Convert.ToDateTime(txtToDate.Text).ToString("yyyy/MM/dd") + "'";
                NFH_DT = objdata.RptEmployeeMultipleDetails(SSQL);
                if (NFH_DT.Rows.Count != 0)
                {
                    if (NFH_DT.Rows[0]["NFH_Work_Days"].ToString() == "")
                    {
                        NFH_Worked_Days = 0;
                    }
                    else
                    {
                        NFH_Worked_Days = Convert.ToDouble(NFH_DT.Rows[0]["NFH_Work_Days"]);
                    }

                    if (NFH_DT.Rows[0]["NFH_Work_Days_Statutory"].ToString() == "")
                    {
                        NFH_Worked_Days_Statutory = 0;
                    }
                    else
                    {
                        NFH_Worked_Days_Statutory = Convert.ToDouble(NFH_DT.Rows[0]["NFH_Work_Days_Statutory"]);
                    }

                    //NFH_Worked_Days_Statutory = IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("NFH_Work_Days_Statutory")), "0", mDataSet.Tables(0).Rows(0)("NFH_Work_Days_Statutory"))
                }
                else
                {
                    NFH_Worked_Days = 0;
                    NFH_Worked_Days_Statutory = 0;
                }

                if (Payroll_EmployeeType == "1" || Payroll_EmployeeType == "16")
                {
                    NFH_Worked_Days = 0;
                    NFH_Worked_Days_Statutory = 0;
                }
                else
                {
                    NFH_Worked_Days = NFH_Worked_Days;
                    NFH_Worked_Days_Statutory = NFH_Worked_Days_Statutory;
                }

                DataTable Sal_DT = new DataTable();

                SSQL = "Select * from [SVT_Epay]..SalaryDetails where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And ExisistingCode='" + Payroll_EmpNo + "' And Month='" + ddlMonth.SelectedItem.Text + "' And FinancialYear='" + Fin_Year_Spilit[0] + "'";
                SSQL = SSQL + " And FromDate='" + Convert.ToDateTime(txtFromDate.Text).ToString("yyyy/MM/dd") + "'";
                SSQL = SSQL + " And ToDate='" + Convert.ToDateTime(txtToDate.Text).ToString("yyyy/MM/dd") + "'";
                Sal_DT = objdata.RptEmployeeMultipleDetails(SSQL);


                if (Sal_DT.Rows.Count != 0)
                {
                    //Get Total Aamount
                    Basic_Salary = 0;
                    if (Sal_DT.Rows[0]["Netpay"].ToString() == "")
                    {
                        Basic_Salary = Basic_Salary + 0;
                    }
                    else
                    {
                        Basic_Salary = Basic_Salary + Convert.ToDouble(Sal_DT.Rows[0]["Netpay"]);
                    }

                    Extra_Allowence = 0;
                    NFH_Worked_Amt = 0;
                    NFH_Worked_Days_Statutory = 0;
                    //if (Sal_DT.Rows[0]["Basic_SM"].ToString() == "")
                    //{
                    //    NFH_Worked_Days_Statutory = NFH_Worked_Days_Statutory + 0;
                    //}
                    //else
                    //{
                    //    NFH_Worked_Days_Statutory = NFH_Worked_Days_Statutory * Convert.ToDouble(Sal_DT.Rows[0]["Basic_SM"]);
                    //}


                    Gross_Earnings = Basic_Salary + Extra_Allowence + NFH_Worked_Amt;

                    Total_Deduction = 0;


                    double d1 = 0;


                    Without_Round_off_Net_Amount = Gross_Earnings - Total_Deduction;
                    Final_Display_Net_Pay = Without_Round_off_Net_Amount;
                    //                    With_Round_Off_Net_Amount = ((Without_Round_off_Net_Amount + 5) / 10) * 10;




                    DataTable DT_Check = new DataTable();
                    SSQL = "Select *from [SVT_Epay]..SalaryVoucherPayment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                    SSQL = SSQL + " And MachineNo='" + MachineNo + "' And ExisistingCode='" + Payroll_EmpNo + "' And Month='" + ddlMonth.SelectedItem.Text + "'";
                    SSQL = SSQL + " And FinancialYear='" + Fin_Year_Spilit[0] + "'";
                    SSQL = SSQL + " And FromDate='" + Convert.ToDateTime(txtFromDate.Text).ToString("yyyy/MM/dd") + "'";
                    SSQL = SSQL + " And ToDate='" + Convert.ToDateTime(txtToDate.Text).ToString("yyyy/MM/dd") + "'";
                    DT_Check = objdata.RptEmployeeMultipleDetails(SSQL);

                    if (DT_Check.Rows.Count != 0)
                    {

                        SSQL = "Update [SVT_Epay]..SalaryVoucherPayment set Voucher_Amt='" + Final_Display_Net_Pay + "' where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                        SSQL = SSQL + " And MachineNo='" + MachineNo + "' And ExisistingCode='" + Payroll_EmpNo + "' And Month='" + ddlMonth.SelectedItem.Text + "'";
                        SSQL = SSQL + " And FinancialYear='" + Fin_Year_Spilit[0] + "'";
                        SSQL = SSQL + " And FromDate='" + Convert.ToDateTime(txtFromDate.Text).ToString("yyyy/MM/dd") + "'";
                        SSQL = SSQL + " And ToDate='" + Convert.ToDateTime(txtToDate.Text).ToString("yyyy/MM/dd") + "'";
                        objdata.RptEmployeeMultipleDetails(SSQL);
                    }
                    else
                    {

                        SSQL = "insert into [SVT_Epay]..SalaryVoucherPayment(Ccode,Lcode,MachineNo,ExisistingCode,";
                        SSQL = SSQL + "EmpName,Month,Year,FinancialYear,FromDate,ToDate,Voucher_Amt,Status)Values ( ";
                        SSQL = SSQL + "'" + SessionCcode + "','" + SessionLcode + "','" + MachineNo + "','" + Payroll_EmpNo + "',";
                        SSQL = SSQL + "'" + EmpName + "','" + ddlMonth.SelectedItem.Text + "','" + Year1 + "','" + Fin_Year_Spilit[0] + "',";
                        SSQL = SSQL + "'" + Convert.ToDateTime(txtFromDate.Text).ToString("yyyy/MM/dd") + "',";
                        SSQL = SSQL + "'" + Convert.ToDateTime(txtToDate.Text).ToString("yyyy/MM/dd") + "','" + Final_Display_Net_Pay + "','0')";
                        objdata.RptEmployeeMultipleDetails(SSQL);
                    }

                    //lblNetAmt.Text = "NET AMOUNT : " & Format(Final_Display_Net_Pay, "0.00")
                    //AmountInWords_Str = Class_Check.ConvertNumberToWords(Final_Display_Net_Pay)

                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "ProgressBarHide();", true);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Voucher Payment Details Saved Successfully..!');", true);


                    //'lblNetAmt.Text = "NET AMOUNT : " & IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("NetAmount_Get")), "", mDataSet.Tables(0).Rows(0)("NetAmount_Get"))
                    //'AmountInWords_Str = Class_Check.ConvertNumberToWords(IIf(IsDBNull(mDataSet.Tables(0).Rows(0)("NetAmount_Get")), "0.0", mDataSet.Tables(0).Rows(0)("NetAmount_Get")))
                }
                else
                {
                    //lblNetAmt.Text = "NET AMOUNT : 0.00";
                    //AmountInWords_Str = Class_Check.ConvertNumberToWords("0.0");
                }



            }





        }
    }
}
