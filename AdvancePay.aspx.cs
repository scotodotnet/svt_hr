﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Payroll;
using Payroll.Configuration;
using Payroll.Data;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class AdvancePay : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string Query;
    string SessionPayroll;
    string mydate;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
       
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        Load_DB();
        if (!IsPostBack)
        {
            Load_WagesCode();
            ddlWagesType_SelectedIndexChanged(sender, e);
        }
    }
    public void Load_DB()
    {
        //Get Database Name
        string query = "";
        DataTable dt_DB = new DataTable();
        query = "select CMS,Rights,Sales,Stores,Weaving,Maintaince,Production,Payroll from [SVT_Rights]..MstDBname";
        dt_DB = objdata.RptEmployeeMultipleDetails(query);
        if (dt_DB.Rows.Count > 0)
        {
            SessionPayroll = dt_DB.Rows[0]["Payroll"].ToString();
            //SessionRights = dt_DB.Rows[0]["Rights"].ToString();
        }
    }
    public void Load_WagesCode()
    {
        DataTable dtdsupp = new DataTable();
        ddlWagesType.Items.Clear();
        Query = "Select EmpTypeCd,EmpType from MstEmployeeType  ";
        dtdsupp = objdata.RptEmployeeMultipleDetails(Query);
        ddlWagesType.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpType"] = "-Select-";
        dr["EmpTypeCd"] = "0";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlWagesType.DataTextField = "EmpType";
        ddlWagesType.DataValueField = "EmpTypeCd";
        ddlWagesType.DataBind();
    }
    protected void ddlWagesType_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dtdsupp = new DataTable();
        ddlExistingCode.Items.Clear();
        Query = "Select ExistingCode from Employee_Mst where Wages='"+ddlWagesType.SelectedItem.Text +"' and CompCode='"+ SessionCcode +"' and LocCode='"+ SessionLcode +"' And IsActive='Yes'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(Query);
        ddlExistingCode.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["ExistingCode"] = "-Select-";
        dr["ExistingCode"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlExistingCode.DataTextField = "ExistingCode";
        ddlExistingCode.DataValueField = "ExistingCode";
        ddlExistingCode.DataBind();
    }
    protected void ddlExistingCode_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dtdsupp = new DataTable();
        DataTable advance_DT = new DataTable();

        Query = "Select * from Employee_Mst where ExistingCode='" + ddlExistingCode.SelectedItem.Text + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'  ";
        dtdsupp = objdata.RptEmployeeMultipleDetails(Query);

        if (dtdsupp.Rows.Count != 0)
        {
            string datee = dtdsupp.Rows[0]["DOJ"].ToString();
            string[] date1 = datee.Split(' ');
            txtEmpName.Text = dtdsupp.Rows[0]["FirstName"].ToString();
            txtTokenNo.Text = dtdsupp.Rows[0]["MachineID"].ToString();
            txtDOJ.Text = date1[0];
            txtDesignation.Text = dtdsupp.Rows[0]["Designation"].ToString();
            txtBasicSal.Text = dtdsupp.Rows[0]["BaseSalary"].ToString();

            Query = " Select EmpNo from[" + SessionPayroll + "].. AdvancePayment where EmpNo='" + txtTokenNo.Text + "'and Completed='N' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' ";
            advance_DT = objdata.RptEmployeeMultipleDetails(Query);
            if (advance_DT.Rows.Count != 0)
            {
                string EmpNo1 = advance_DT.Rows[0]["EmpNo"].ToString();
                if (EmpNo1 == txtTokenNo.Text)
                {
                    DataTable dtret = new DataTable();
                    Query = " Select DueMonth,Amount,BalanceAmount,MonthlyDeduction from[" + SessionPayroll + "].. AdvancePayment where EmpNo='" + txtTokenNo.Text + "' ";
                    Query = Query + " and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'  and Completed='N' ";
                    dtret = objdata.RptEmployeeMultipleDetails(Query);

                    txtAdvAmt.Text = dtret.Rows[0]["Amount"].ToString();
                    txtBalanceAmt.Text = dtret.Rows[0]["BalanceAmount"].ToString();
                    txtMonthlyDed.Text = dtret.Rows[0]["MonthlyDeduction"].ToString();
                }
            }
        }
    }

    protected void btnCalc_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        DataTable advance_DT = new DataTable();
        txtMonthlyDed.Text = "";
        if (txtAdvAmt.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Amount....!');", true);
            //System.Windows.Forms.MessageBox.Show("Enter the Amount....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        else if (txtNoofMonths.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the No of Months....!');", true);
            //System.Windows.Forms.MessageBox.Show("Enter the No of Months....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            if (Convert.ToDecimal(txtAdvAmt.Text) == 0)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Amount Properly....!');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Amount Properly....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (Convert.ToDecimal(txtNoofMonths.Text) > Convert.ToDecimal(txtAdvAmt.Text))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Amount Properly....!');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Amount Properly....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (Convert.ToDecimal(txtNoofMonths.Text) == 0)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the No of Months Properly....!');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the No of Months Properly....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                Query = " Select EmpNo from[" + SessionPayroll + "].. AdvancePayment where EmpNo='" + txtTokenNo.Text + "'and Completed='N' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' ";
                advance_DT = objdata.RptEmployeeMultipleDetails(Query);
                if (advance_DT.Rows.Count != 0)
                {
                    string EmpNo1 = advance_DT.Rows[0]["EmpNo"].ToString();
                    if (EmpNo1 == txtTokenNo.Text)
                    {
                        decimal repay = (Convert.ToDecimal(txtBalanceAmt.Text) / Convert.ToDecimal(txtNoofMonths.Text));
                        repay = Math.Round(repay, 2, MidpointRounding.ToEven);
                        txtMonthlyDed.Text = repay.ToString();
                    }
                }
                else
                {
                    decimal repay = (Convert.ToDecimal(txtAdvAmt.Text) / Convert.ToDecimal(txtNoofMonths.Text));
                    repay = Math.Round(repay, 2, MidpointRounding.ToEven);
                    txtMonthlyDed.Text = repay.ToString();
                }


            }
            else
            {
                txtMonthlyDed.Text = "";
            }
        }
    }
    protected void txtNoofMonths_TextChanged(object sender, EventArgs e)
    {
        btnCalc_Click(sender, e);
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        DataTable advance_DT = new DataTable();
        try
        {

            bool ErrFlag = false;
            bool issaved = false;
            bool IsUpdated = true;

            if (ddlWagesType.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Wages Type....!');", true);
                ErrFlag = true;
            }
            if (ddlExistingCode.SelectedValue == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the ExistingCode....!');", true);
                ErrFlag = true;
            }

            else if (txtEmpName.Text  == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Search the Employee....!');", true);
                  ErrFlag = true;
            }
            else if (txtFromDate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Date...!');", true);
                ErrFlag = true;
            }
            else if (txtAdvAmt.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Advance Amount....!');", true);
                ErrFlag = true;
            }
            else if (Convert.ToDecimal(txtAdvAmt.Text) == 0)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Advance Amount Properly....!');", true);
                ErrFlag = true;
            }
            else if (txtMonthlyDed.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Monthly Deduction....!');", true);
               ErrFlag = true;
            }
            else if (Convert.ToDecimal(txtMonthlyDed.Text) == 0)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Monthly Deduction Properly....!');", true);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
               

                if (Convert.ToDecimal(txtMonthlyDed.Text) > Convert.ToDecimal(txtAdvAmt.Text))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Monthly Deduction Properly....!');", true);
                    ErrFlag = true;
                }
              
                if (!ErrFlag)
                {


                    //if (filUpload.HasFile)
                    //{
                      
                    //    mydate = Convert.ToDateTime(txtFromDate.Text).ToString("dd-MM-yyyy");


                    //    string UNIT_Folder = "";
                    //    if (SessionLcode.ToUpper() == "UNIT I") { UNIT_Folder = "Emp_Scan/UNIT_I/AdvanceCopy"; }
                    //    if (SessionLcode.ToUpper() == "UNIT II") { UNIT_Folder = "Emp_Scan/UNIT_II/AdvanceCopy"; }
                    //    if (SessionLcode.ToUpper() == "UNIT III") { UNIT_Folder = "Emp_Scan/UNIT_III/AdvanceCopy"; }
                    //    if (SessionLcode.ToUpper() == "UNIT IV") { UNIT_Folder = "Emp_Scan/UNIT_IV/AdvanceCopy"; }

                    //    string FileName = Server.MapPath(filUpload.PostedFile.FileName);
                    //    string Exten = System.IO.Path.GetExtension(this.filUpload.PostedFile.FileName);
                    //    if (Exten == ".jpg")
                    //    {
                    //        filUpload.SaveAs(Server.MapPath("~/" + UNIT_Folder + "/" + mydate + "-" + txtTokenNo.Text + Exten));

                    //    }


                        Query = " Select EmpNo from[" + SessionPayroll + "].. AdvancePayment where EmpNo='" + txtTokenNo.Text + "'and Completed='N' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' ";
                        advance_DT = objdata.RptEmployeeMultipleDetails(Query);
                        if (advance_DT.Rows.Count != 0)
                        {
                            string EmpNo1 = advance_DT.Rows[0]["EmpNo"].ToString();

                            if (EmpNo1 == txtTokenNo.Text)
                            {
                                decimal balance = (Convert.ToDecimal(txtBalanceAmt.Text) / Convert.ToInt32(txtNoofMonths.Text));
                                if (balance == Convert.ToDecimal(txtMonthlyDed.Text))
                                {

                                    Query = " Update [" + SessionPayroll + "]..AdvancePayment set DueMonth='" + txtNoofMonths.Text + "',MonthlyDeduction='" + txtMonthlyDed.Text + "',ReductionMonth='" + txtNoofMonths.Text + "'";
                                    Query = Query + "where EmpNo='" + txtTokenNo.Text + "' and Ccode='" + SessionCcode + "'and Lcode='" + SessionLcode + "' and Completed='N' and BalanceAmount='" + txtBalanceAmt.Text + "'";
                                    objdata.RptEmployeeMultipleDetails(Query);
                                    IsUpdated = true;
                                }

                            }
                        }
                        else
                        {
                            mydate = Convert.ToDateTime(txtFromDate.Text).ToString("dd-MM-yyyy");

                            Query = "INSERT INTO [" + SessionPayroll + "]..AdvancePayment (EmpNo,ExistNo,TransDate,DueMonth,Amount,BalanceAmount,MonthlyDeduction,";
                            Query = Query + " CreateDate,Completed,IncreaseMonth,ReductionMonth,Modifieddate,Ccode,Lcode)";
                            Query = Query + " values('" + txtTokenNo.Text + "','" + ddlExistingCode.SelectedItem.Text + "',convert(datetime,'" + mydate + "',105), ";
                            Query = Query + " '" + txtNoofMonths.Text + "','" + txtAdvAmt.Text + "','" + txtAdvAmt.Text + "','" + txtMonthlyDed.Text + "',convert(datetime,getDate(),105),";
                            Query = Query + " 'N','0','" + txtNoofMonths.Text + "',convert(datetime,'" + mydate + "',105),'" + SessionCcode + "','" + SessionLcode + "')";
                            objdata.RptEmployeeMultipleDetails(Query);


                            issaved = true;
                        }
                        if (issaved == true)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Saved Successfully....!');", true);
                            //System.Windows.Forms.MessageBox.Show("Saved Successfully....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        }
                        if (IsUpdated == true)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Updated Successfully....!');", true);
                            //System.Windows.Forms.MessageBox.Show("Saved Successfully....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        }
                        clear();
                    //}
                    //else
                    //{
                    //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Please Upload the image...!');", true);
                    //}
                
               }
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Contact Admin....!');", true);
            //System.Windows.Forms.MessageBox.Show("Contact Admin....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        }
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        clear();
        //ddlExistingCode.SelectedValue = "0";
        //ddlExistingCode.SelectedValue = "0";
       
    }
    private void clear()
    {
        //txtempNo.Text = "";
        //txtexist.Text = "";
        txtTokenNo.Text = "";
        txtFromDate.Text = "";
        txtFromDate.Text = "";
        txtMonthlyDed.Text = "";
        txtAdvAmt.Text = "";   
        txtDOJ.Text = "";
        txtDesignation.Text = "";
        txtBasicSal.Text = "";
        txtNoofMonths.Text = "";
        txtBalanceAmt.Text = "0";
        ddlWagesType.SelectedValue = "0";
        ddlExistingCode.SelectedValue = "-Select-";
        txtEmpName.Text = "";
    }

   

        
       
      
    
}
