﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="MstTimeIN.aspx.cs" Inherits="MstTimeIN" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


<script src="assets/js/master_list_jquery.min.js"></script>
<script src="assets/js/master_list_jquery-ui.min.js"></script>
<link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css"/>
<%--<script src="assets/js/form-wizards.demo.min.js"></script>--%>
 <script type="text/javascript">
     $(document).ready(function() {
         $('#example').dataTable();
      
     });
	</script>
<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.select2').select2();
                $('#example').dataTable();
            }
        });
    };
</script>
<asp:UpdatePanel ID="UpdatePanel5" runat="server">
 <ContentTemplate>
<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Grass&TimeIN</a></li>
				<li class="active">Grass&TimeIN Entry</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Grass&TimeIN Entry</h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">Grass&TimeIN Entry</h4>
                        </div>
                        <div class="panel-body">
                       
                             <h5>TIME IN ENTRY</h5>
                             <!-- begin row -->
			            <div class="row"> 
			             <!-- begin col-3 -->
			            <div class="col-md-2">
			            <div class="form-group">
                            <label>Start Minutes</label>
                        </div>
			            </div>
                       <!-- end col-3 -->
                       
			             <!-- begin col-3 -->
			            <div class="col-md-2">
			            <div class="form-group">
                             <label>End Minutes</label>
                         
                        </div>
			            </div>
                       <!-- end col-3 -->
                            
			             <!-- begin col-3 -->
			            <div class="col-md-2">
			            <div class="form-group">
                             <label>Total Minutes</label>
                         
                        </div>
			            </div>
                       <!-- end col-3 -->
                        </div>
                        <!-- end row -->

                             <!-- begin row -->
			            <div class="row"> 
			             <!-- begin col-3 -->
			            <div class="col-md-2">
			            <div class="form-group">
                             <asp:TextBox ID="txtTIStartMnts1" runat="server" CssClass="form-control" ></asp:TextBox>
                        </div>
			            </div>
                       <!-- end col-3 -->
                       
			             <!-- begin col-3 -->
			            <div class="col-md-2">
			            <div class="form-group">
                            <asp:TextBox ID="txtTIEndMnts1" runat="server" CssClass="form-control" ></asp:TextBox>
                         
                        </div>
			            </div>
                       <!-- end col-3 -->
                             <!-- begin col-3 -->
			            <div class="col-md-2">
			            <div class="form-group">
                            <asp:TextBox ID="txtTITotalMnts1" runat="server" CssClass="form-control" ></asp:TextBox>
                         
                        </div>
			            </div>
                       <!-- end col-3 -->
                        </div>
                        <!-- end row -->

                             <!-- begin row -->
			            <div class="row"> 
			             <!-- begin col-3 -->
			            <div class="col-md-2">
			            <div class="form-group">
                             <asp:TextBox ID="txtTIStartMnts2" runat="server" CssClass="form-control" ></asp:TextBox>
                        </div>
			            </div>
                       <!-- end col-3 -->
                       
			             <!-- begin col-3 -->
			            <div class="col-md-2">
			            <div class="form-group">
                            <asp:TextBox ID="txtTIEndMnts2" runat="server" CssClass="form-control" ></asp:TextBox>
                         
                        </div>
			            </div>
                       <!-- end col-3 -->
                             <!-- begin col-3 -->
			            <div class="col-md-2">
			            <div class="form-group">
                            <asp:TextBox ID="txtTITotalMnts2" runat="server" CssClass="form-control" ></asp:TextBox>
                         
                        </div>
			            </div>
                       <!-- end col-3 -->
                        </div>
                        <!-- end row -->

                             <!-- begin row -->
			            <div class="row"> 
			             <!-- begin col-3 -->
			            <div class="col-md-2">
			            <div class="form-group">
                            <asp:Button ID="btnTISave" runat="server" CssClass="btn btn-success" OnClick="btnSave_Click" Text="Save" />
                        
                            <asp:Button ID="btnTIClr" runat="server" CssClass="btn btn-danger" OnClick="btnClr_Click" Text="Clear" />
                        </div>
			            </div>
                       <!-- end col-3 -->
                        </div>
                        <!-- end row -->
                            <div id="new" runat="server" visible="false">
                             <h5>OVER TIME MINUTES</h5>

                             <!-- begin row -->
			            <div class="row"> 
			             <!-- begin col-3 -->
			            <div class="col-md-2">
			            <div class="form-group">
                            <label>Start Minutes</label>
                        </div>
			            </div>
                       <!-- end col-3 -->
                       
			             <!-- begin col-3 -->
			            <div class="col-md-2">
			            <div class="form-group">
                             <label>End Minutes</label>
                         
                        </div>
			            </div>
                       <!-- end col-3 -->
                             <!-- begin col-3 -->
			            <div class="col-md-2">
			            <div class="form-group">
                             <label>Total Minutes</label>
                         
                        </div>
			            </div>
                       <!-- end col-3 -->
                        </div>
                        <!-- end row -->

                             <!-- begin row -->
			            <div class="row"> 
			             <!-- begin col-3 -->
			            <div class="col-md-2">
			            <div class="form-group">
                             <asp:TextBox ID="txtOTStartMnts1" runat="server" CssClass="form-control" ></asp:TextBox>
                        </div>
			            </div>
                       <!-- end col-3 -->
                       
			             <!-- begin col-3 -->
			            <div class="col-md-2">
			            <div class="form-group">
                            <asp:TextBox ID="txtOTEndMnts1" runat="server" CssClass="form-control" ></asp:TextBox>
                         
                        </div>
			            </div>
                       <!-- end col-3 -->
                            <!-- begin col-3 -->
			            <div class="col-md-2">
			            <div class="form-group">
                            <asp:TextBox ID="txtOTTotalMnts1" runat="server" CssClass="form-control" ></asp:TextBox>
                         
                        </div>
			            </div>
                       <!-- end col-3 -->
                        </div>
                        <!-- end row -->
                             <!-- begin row -->
			            <div class="row"> 
			             <!-- begin col-3 -->
			            <div class="col-md-2">
			            <div class="form-group">
                             <asp:TextBox ID="txtOTStartMnts2" runat="server" CssClass="form-control" ></asp:TextBox>
                        </div>
			            </div>
                       <!-- end col-3 -->
                       
			             <!-- begin col-3 -->
			            <div class="col-md-2">
			            <div class="form-group">
                            <asp:TextBox ID="txtOTEndMnts2" runat="server" CssClass="form-control" ></asp:TextBox>
                         
                        </div>
			            </div>
                       <!-- end col-3 -->
                            <!-- begin col-3 -->
			            <div class="col-md-2">
			            <div class="form-group">
                            <asp:TextBox ID="txtOTTotalMnts2" runat="server" CssClass="form-control" ></asp:TextBox>
                         
                        </div>
			            </div>
                       <!-- end col-3 -->
                        </div>
                        <!-- end row -->

                             <!-- begin row -->
			            <div class="row"> 
			             <!-- begin col-3 -->
			            <div class="col-md-2">
			            <div class="form-group">
                            <asp:Button ID="btnOTSave" runat="server" CssClass="btn btn-success" OnClick="btnOTSave_Click" Text="Save" />
                        
                            <asp:Button ID="btnOTClr" runat="server" CssClass="btn btn-danger" OnClick="btnOTClr_Click" Text="Clear" />
                        </div>
			            </div>
                       <!-- end col-3 -->
                        </div>
                        <!-- end row -->
                        
                        </div>
                      
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
<!-- end #content -->

</ContentTemplate>
</asp:UpdatePanel>


</asp:Content>
