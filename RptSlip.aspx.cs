﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

using Payroll;
using Payroll.Data;
using Payroll.Configuration;
using System.IO;
//using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
public partial class RptSlip : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    DateTime TransDate;
    string SessionAdmin;
    string Stafflabour;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string NetBase;
    string NetFDA;
    string NetVDA;
    string NetHRA;
    string Nettotal;
    string NetPFEarnings;
    string NetPF;
    string NetESI;
    string NetUnion;
    string NetAdvance;
    string NetAll1;
    string NetAll2;
    string NetAll3;
    string NetAll4;
    string NetAll5;
    string NetDed1;
    string NetDed2;
    string NetDed3;
    string NetDed4;
    string NetDed5;
    string HomeDays;
    string Halfnight;
    string FullNight;
    string DayIncentive;
    string Spinning;
    string ThreeSided;
    string NetLOP;
    string NetStamp;
    string NetTotalDeduction;
    string NetOT;
    string NetAmt;
    string Network;
    string totNFh;
    string totweekoff;
    string totCL;
    string totwork;
    string Roundoff;
    DateTime MyDate;
    DateTime MyDate1;
    DateTime MyDate2;
    static decimal AdvAmt;
    static string ID;
    static string Adv_id = "";
    static string Dec_mont = "0";
    static string Adv_BalanceAmt = "0";
    static string Adv_due = "0";
    static string Increment_mont = "0";
    static decimal val;
    static string EmployeeDays = "0";
    string MyMonth;
    static string cl = "0";
    string TempDate;
    static string Fixedsal = "0";
    static string FixedOT = "0";
    static string Tot_OThr = "0";
    static string Emp_ESI_Code = "";
    static string NetPay_Grand_Total = "0";
    static string NetPay_Grand_Total_Words = "";
    static bool isUK = false;
    string SessionPayroll;

    DataTable Hindi_Cat = new DataTable();

    string SessionUserName;
    string SessionUserID;
    string SessionRights;

    System.Web.UI.WebControls.DataGrid grid =
                            new System.Web.UI.WebControls.DataGrid();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        Load_DB();
        if (!IsPostBack)
        {
           // LoadShitType();
            Load_WagesType();
            //Load_Department();
            Load_BankName();
            Months_load();
            Load_Dept();
            //ESICode_load();
            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddlFinance.Items.Add(new System.Web.UI.WebControls.ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }

            currentYear = Utility.GetCurrentYearOnly;
            for (int i = 0; i < 10; i++)
            {
                currentYear = currentYear - 1;
            }
            if (SessionUserType == "2")
            {
                IFUser_Fields_Hide();
            }
        }
    }

    private void LoadShitType()
    {
        string SSQL = "";
        SSQL = "";
        SSQL = "Select ShiftDesc from Shift_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' order by ShiftDesc ASC";
        ddlShiftType.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlShiftType.DataTextField = "ShiftDesc";
        ddlShiftType.DataValueField = "ShiftDesc";
        ddlShiftType.DataBind();
        ddlShiftType.Items.Insert(0, new ListItem("Select","0"));
    }

    public void IFUser_Fields_Hide()
    {
        //IF_Dept_Hide.Visible = false;
        //IF_FromDate_Hide.Visible = false;
        //IF_ToDate_Hide.Visible = false;
        //IF_Left_Employee_Hide.Visible = false;
        //IF_State_Hide.Visible = false;
        //btnBankSalary.Visible = false;
        //btnCivilAbstract.Visible = false;
        //BtnDeptManDays.Visible = false;
        //IF_Leave_Credit.Visible = false;
        //rdbPayslipFormat.Visible = false;
        rdbPayslipIFFormat.Visible = true;
        RdbCashBank.SelectedValue = "2";
        RdbPFNonPF.SelectedValue = "1";
        IF_Salary_Through_Hide.Visible = false;
        IF_PF_NON_PF_Hide.Visible = false;
        IF_Report_Type.Visible = true;
        //IF_rdbPayslipFormat_Hide.Visible = false;
    }
    public void Load_DB()
    {
        //Get Database Name
        string query = "";
        DataTable dt_DB = new DataTable();
        query = "select CMS,Rights,Sales,Stores,Weaving,Maintaince,Production,Payroll from [SVT_Rights]..MstDBname";
        dt_DB = objdata.RptEmployeeMultipleDetails(query);
        if (dt_DB.Rows.Count > 0)
        {
            SessionPayroll = dt_DB.Rows[0]["Payroll"].ToString();
            //SessionRights = dt_DB.Rows[0]["Rights"].ToString();
        }
    }
    private void Load_Dept()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlDept.Items.Clear();
        query = "select DeptCode1,DeptName from Department_Mst order by DeptName Asc";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlDept.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["DeptName"] = "-Select-";
        dr["DeptName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlDept.DataTextField = "DeptName";
        ddlDept.DataValueField = "DeptName";
        ddlDept.DataBind();
    }

    
    private void Load_BankName()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtBankName.Items.Clear();
        query = "Select BankName from MstBank order by BankName Asc";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtBankName.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["BankName"] = "-Select-";
        dr["BankName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtBankName.DataTextField = "BankName";
        txtBankName.DataValueField = "BankName";
        txtBankName.DataBind();
    }
    private void Load_WagesType()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtEmployeeType.Items.Clear();
        string Category_Str = "0";
        if (ddlcategory.SelectedItem.Text == "STAFF")
        {
            Category_Str = "1";
        }
        else if (ddlcategory.SelectedItem.Text == "LABOUR")
        {
            Category_Str = "2";
        }
        if (SessionUserType == "2")
        {
            query = "select EmpTypeCd,EmpType from MstEmployeeType where IF_='1' and EmpCategory='" + Category_Str + "'";
        }
        else
        {
            query = "select EmpTypeCd,EmpType from MstEmployeeType where EmpCategory='" + Category_Str + "'";
        }
       // query = "select EmpTypeCd,EmpType from MstEmployeeType where EmpCategory='" + Category_Str + "'";

        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtEmployeeType.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpTypeCd"] = "0";
        dr["EmpType"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtEmployeeType.DataTextField = "EmpType";
        txtEmployeeType.DataValueField = "EmpTypeCd";
        txtEmployeeType.DataBind();
    }

    private void Months_load()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlMonths.Items.Clear();
        query = "Select ID,Months from MonthDetails order by ID ASC";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlMonths.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["ID"] = "0";
        dr["Months"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlMonths.DataTextField = "Months";
        ddlMonths.DataValueField = "ID";
        ddlMonths.DataBind();
    }
    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_WagesType();
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            string CmpName = "";
            string Cmpaddress = "";
            int YR = 0;
            string SalaryType = "";
            string query = "";
            string ExemptedStaff = "";
            string report_head = "";
            string Basic_Report_Date = "";
            string Basic_Report_Type = "";
            string Title = "";

            
            if (ddlMonths.SelectedItem.Text == "January")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else if (ddlMonths.SelectedItem.Text == "February")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else if (ddlMonths.SelectedItem.Text == "March")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
            }

            if (ddlcategory.SelectedValue == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Category');", true);
                ErrFlag = true;
            }
            else if ((txtEmployeeType.SelectedValue == "") || (txtEmployeeType.SelectedValue == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Employee Type');", true);
                ErrFlag = true;
            }

            else if ((ddlMonths.SelectedValue == "0") || (ddlMonths.SelectedValue == ""))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Months');", true);
                ErrFlag = true;
            }
            if (Convert.ToDateTime(txtTo.Text) < Convert.ToDateTime(txtfrom.Text))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Enter the Dates Properly...');", true);
                ErrFlag = true;
            }
            string Other_State = "";
            string Non_Other_State = "";

            //For Weekly Wages
           
                DataTable Emp_Det = new DataTable();
                DataTable Atten_Sal_DT = new DataTable();
                DataTable Pay_Slip_DT = new DataTable();
                DateTime fmDate = Convert.ToDateTime(txtfrom.Text);
                DateTime toDate = Convert.ToDateTime(txtTo.Text);
                int daysAdded = 0;
                int dayscount = (int)(Convert.ToDateTime(txtTo.Text) - Convert.ToDateTime(txtfrom.Text)).TotalDays;


            if (SessionUserType == "2")
            {
                //IF_PayslipFormat();

                if (txtEmployeeType.SelectedItem.Text == "WEEKLY" || txtEmployeeType.SelectedItem.Text == "WEEKLY-HINDI")
                {
                    if (!ErrFlag)
                    {
                        Pay_Slip_DT.Columns.Add("Sno");
                        Pay_Slip_DT.Columns.Add("PFNo");
                        Pay_Slip_DT.Columns.Add("EmpNo");
                        Pay_Slip_DT.Columns.Add("Name");
                        while (dayscount >= 0)
                        {
                            //DateTime dayy = Convert.ToDateTime(fmDate.AddDays(daysAdded).ToShortDateString());
                            Pay_Slip_DT.Columns.Add("AT" + daysAdded);
                            //Pay_Slip_DT.Columns.Add("OT" + daysAdded);
                            dayscount -= 1;
                            daysAdded += 1;
                        }
                        Pay_Slip_DT.Columns.Add("Days");
                        Pay_Slip_DT.Columns.Add("Perday");
                        Pay_Slip_DT.Columns.Add("Gross");
                        Pay_Slip_DT.Columns.Add("A1");
                        Pay_Slip_DT.Columns.Add("A2");
                        Pay_Slip_DT.Columns.Add("A3");
                        Pay_Slip_DT.Columns.Add("A4");
                        Pay_Slip_DT.Columns.Add("GrossAmt");

                        //Pay_Slip_DT.Columns.Add("OThrs");
                        //Pay_Slip_DT.Columns.Add("PerHr");
                        //Pay_Slip_DT.Columns.Add("OTamt");
                        //if (txtEmployeeType.SelectedItem.Text == "WEEKLY")
                        //{
                        //    Pay_Slip_DT.Columns.Add("Businc");
                        //    Pay_Slip_DT.Columns.Add("MasthiriInc");
                        //}
                        //else
                        //{
                        //    Pay_Slip_DT.Columns.Add("Lunch");
                        //    Pay_Slip_DT.Columns.Add("OTFoodAmtNew");
                        //}

                        Pay_Slip_DT.Columns.Add("PF");
                        Pay_Slip_DT.Columns.Add("ESI");


                        //Pay_Slip_DT.Columns.Add("Adv");
                        //Pay_Slip_DT.Columns.Add("Fine");
                        Pay_Slip_DT.Columns.Add("TotalWages");
                        Pay_Slip_DT.Columns.Add("Sign");


                        decimal TotalBaseWages = 0;
                        decimal ToTalOTAmt = 0;
                        decimal ToTalBusInc = 0;
                        decimal ToTalMasthiriInc = 0;
                        decimal ToaTalAdv = 0;
                        decimal TotalWages = 0;

                        if (txtEmployeeType.SelectedItem.Text == "WEEKLY")
                        {
                            query = "";
                            query = "Select ED.FirstName,ED.PFNo,Attn_DT.EmpNo,Attn_DT.Days,Attn_DT.Months,Attn_DT.FinancialYear,Attn_DT.FromDate,Attn_DT.ToDate,";
                            query = query + "Attn_DT.Day1_WH,Attn_DT.Day1_OT,Attn_DT.Day2_WH,Attn_DT.Day2_OT,Attn_DT.Day3_WH,Attn_DT.Day3_OT,Attn_DT.Day4_WH,Attn_DT.Day4_OT,";
                            query = query + "Attn_DT.Day5_WH,Attn_DT.Day5_OT,Attn_DT.Day6_WH,Attn_DT.Day6_OT,Attn_DT.Day7_WH,Attn_DT.Day7_OT,Attn_DT.OTHoursNew,";
                            query = query + "Sal_DT.Fbasic,Sal_DT.BasicandDA,Sal_DT.GrossEarnings,Sal_DT.NetPay,Sal_DT.Advance,Sal_DT.OTHoursAmtNew as Per_hour,Sal_DT.Manual_OT,";
                            query = query + "Sal_DT.Bus_Incentive,Sal_DT.Masthiri_Incentive,Sal_DT.Lunch,Sal_DT.OTFoodAmtNew,Sal_DT.DedOthers1,Sal_DT.Deduction3,Sal_DT.Deduction4,Sal_DT.allowances1,Sal_DT.allowances2,Sal_DT.allowances3,Sal_DT.allowances4,Sal_DT.Emp_PF,Sal_DT.EmployeerESI ";
                            query = query + " from [" + SessionPayroll + "]..AttenanceDetails Attn_DT Inner Join [" + SessionPayroll + "]..SalaryDetails_IF Sal_DT on Attn_DT.EmpNo=Sal_DT.EmpNo";
                            query = query + " Inner Join Employee_Mst ED on Attn_DT.EmpNo=ED.EmpNo and Sal_DT.EmpNo=ED.EmpNo ";
                            query = query + " where  Attn_DT.Months=Sal_DT.Month and Attn_DT.FinancialYear=Sal_DT.FinancialYear  and ED.Eligible_PF='1' and ED.Eligible_ESI='1' and Attn_DT.Days>'0' ";
                            query = query + "and Convert(datetime,Attn_DT.FromDate,105)=Convert(datetime,Sal_DT.FromDate,105) and Convert(datetime,Attn_DT.ToDate,105)=Convert(datetime,Sal_DT.ToDate,105) ";
                            query = query + "and Attn_DT.Months='" + ddlMonths.SelectedItem.Text + "' and Attn_DT.FinancialYear='" + ddlFinance.SelectedValue + "' and Attn_DT.Ccode='" + SessionCcode + "' and Attn_DT.Lcode='" + SessionLcode + "' ";
                            query = query + "and Sal_DT.Ccode='" + SessionCcode + "' and Sal_DT.Lcode='" + SessionLcode + "' and convert(datetime,Attn_DT.FromDate,105) = convert(datetime,'" + txtfrom.Text + "',105) and convert(datetime, Attn_DT.ToDate, 105) = convert(datetime, '" + txtTo.Text + "', 105)";
                            //query = query + " ";
                        }
                        else
                        {
                            query = "";
                            query = "Select ED.FirstName,Attn_DT.EmpNo,Attn_DT.Days,Attn_DT.Months,Attn_DT.FinancialYear,Attn_DT.FromDate,Attn_DT.ToDate,";
                            query = query + "Attn_DT.Day1_WH,Attn_DT.Day1_OT,Attn_DT.Day2_WH,Attn_DT.Day2_OT,Attn_DT.Day3_WH,Attn_DT.Day3_OT,Attn_DT.Day4_WH,Attn_DT.Day4_OT,";
                            query = query + "Attn_DT.Day5_WH,Attn_DT.Day5_OT,Attn_DT.Day6_WH,Attn_DT.Day6_OT,Attn_DT.Day7_WH,Attn_DT.Day7_OT,Attn_DT.OTHoursNew,";
                            query = query + "Sal_DT.Fbasic,Sal_DT.BasicandDA,Sal_DT.GrossEarnings,Sal_DT.NetPay,Sal_DT.Advance,Sal_DT.OTHoursAmtNew as Per_hour,Sal_DT.Manual_OT,";
                            query = query + "Sal_DT.Bus_Incentive,Sal_DT.Masthiri_Incentive,Sal_DT.Lunch,Sal_DT.OTFoodAmtNew,Sal_DT.DedOthers1,Sal_DT.allowances1,Sal_DT.allowances2,Sal_DT.allowances3,Sal_DT.allowances4,Sal_DT.Emp_PF,Sal_DT.EmployeerESI ";
                            query = query + " from [" + SessionPayroll + "]..AttenanceDetails Attn_DT Inner Join [" + SessionPayroll + "]..SalaryDetails_IF Sal_DT on Attn_DT.EmpNo=Sal_DT.EmpNo";
                            query = query + " Inner Join Employee_Mst ED on Attn_DT.EmpNo=ED.EmpNo and Sal_DT.EmpNo=ED.EmpNo ";
                            query = query + " where  Attn_DT.Months=Sal_DT.Month and Attn_DT.FinancialYear=Sal_DT.FinancialYear and ED.Eligible_PF='1' and ED.Eligible_ESI='1' and Attn_DT.Days>'0' ";
                            query = query + "and Convert(datetime,Attn_DT.FromDate,105)=Convert(datetime,Sal_DT.FromDate,105) and Convert(datetime,Attn_DT.ToDate,105)=Convert(datetime,Sal_DT.ToDate,105) ";
                            query = query + "and Attn_DT.Months='" + ddlMonths.SelectedItem.Text + "' and Attn_DT.FinancialYear='" + ddlFinance.SelectedValue + "' and Attn_DT.Ccode='" + SessionCcode + "' and Attn_DT.Lcode='" + SessionLcode + "' ";
                            query = query + "and Sal_DT.Ccode='" + SessionCcode + "' and Sal_DT.Lcode='" + SessionLcode + "' and convert(datetime,Attn_DT.FromDate,105) = convert(datetime,'" + txtfrom.Text + "',105) and convert(datetime, Attn_DT.ToDate, 105) = convert(datetime, '" + txtTo.Text + "', 105)";
                           // query = query + " and ED.EligiblePF='1' and ED.ElgibleESI='1'";

                            if (ddlHindiCategory.SelectedItem.Text != "-Select-")
                            {
                                query = query + " and ED.Hindi_Category='" + ddlHindiCategory.SelectedItem.Text + "'";
                                Title = Title + "  Hindi_Category=" + ddlHindiCategory.SelectedItem.Text + " ";
                            }

                        }
                        if (ddlShiftType.SelectedItem.Text != "-Select-")
                        {
                            query = query + " and ED.ShiftType_New='" + ddlShiftType.SelectedItem.Text + "'";
                            Title = Title + "  Shift=" + ddlShiftType.SelectedItem.Text + " ";
                        }
                        if (ddlDept.SelectedItem.Text != "-Select-")
                        {
                            query = query + " and ED.DeptName='" + ddlDept.SelectedItem.Text + "'";
                            Title = Title + "   DeptName=" + ddlDept.SelectedItem.Text + " ";
                        }
                        if (txtEmployeeType.SelectedItem.Text != "-Select-")
                        {
                            query = query + " and ED.Wages='" + txtEmployeeType.SelectedItem.Text + "'";

                        }
                        if (txtmiltype.SelectedValue != "0")
                        {
                            query = query + " and ED.Unit_Type='" + txtmiltype.SelectedValue + "'";
                        }

                        query = query + " and Attn_DT.Days>'0'";


                        Atten_Sal_DT = objdata.RptEmployeeMultipleDetails(query);

                        if (Atten_Sal_DT.Rows.Count != 0)
                        {
                            for (int EmpRow = 0; EmpRow < Atten_Sal_DT.Rows.Count; EmpRow++)
                            {
                                Pay_Slip_DT.NewRow();
                                Pay_Slip_DT.Rows.Add();
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["Sno"] = Pay_Slip_DT.Rows.Count;
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["EmpNo"] = Atten_Sal_DT.Rows[EmpRow]["EmpNo"].ToString();
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["Name"] = Atten_Sal_DT.Rows[EmpRow]["FirstName"].ToString();
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["PFNo"] = Atten_Sal_DT.Rows[EmpRow]["PFNo"].ToString();
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["AT0"] = Atten_Sal_DT.Rows[EmpRow]["Day1_WH"].ToString();
                                //Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["OT0"] = Atten_Sal_DT.Rows[EmpRow]["Day1_OT"].ToString();
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["AT1"] = Atten_Sal_DT.Rows[EmpRow]["Day2_WH"].ToString();
                                //Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["OT1"] = Atten_Sal_DT.Rows[EmpRow]["Day2_OT"].ToString();
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["AT2"] = Atten_Sal_DT.Rows[EmpRow]["Day3_WH"].ToString();
                               // Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["OT2"] = Atten_Sal_DT.Rows[EmpRow]["Day3_OT"].ToString();
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["AT3"] = Atten_Sal_DT.Rows[EmpRow]["Day4_WH"].ToString();
                              //  Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["OT3"] = Atten_Sal_DT.Rows[EmpRow]["Day4_OT"].ToString();
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["AT4"] = Atten_Sal_DT.Rows[EmpRow]["Day5_WH"].ToString();
                               // Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["OT4"] = Atten_Sal_DT.Rows[EmpRow]["Day5_OT"].ToString();
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["AT5"] = Atten_Sal_DT.Rows[EmpRow]["Day6_WH"].ToString();
                               // Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["OT5"] = Atten_Sal_DT.Rows[EmpRow]["Day6_OT"].ToString();
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["AT6"] = Atten_Sal_DT.Rows[EmpRow]["Day7_WH"].ToString();
                               // Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["OT6"] = Atten_Sal_DT.Rows[EmpRow]["Day7_OT"].ToString();

                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["Days"] = Atten_Sal_DT.Rows[EmpRow]["Days"].ToString();
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["Perday"] = Atten_Sal_DT.Rows[EmpRow]["BasicandDA"].ToString();
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["Gross"] = Atten_Sal_DT.Rows[EmpRow]["Fbasic"].ToString();
                                //Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["OThrs"] = Atten_Sal_DT.Rows[EmpRow]["OTHoursNew"].ToString();
                                //Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["PerHr"] = Atten_Sal_DT.Rows[EmpRow]["Per_hour"].ToString();
                                //Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["OTamt"] = Atten_Sal_DT.Rows[EmpRow]["Manual_OT"].ToString();
                                if (txtEmployeeType.SelectedItem.Text == "WEEKLY")
                                {
                                    //Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["Businc"] = Atten_Sal_DT.Rows[EmpRow]["Bus_Incentive"].ToString();
                                    //Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["MasthiriInc"] = Atten_Sal_DT.Rows[EmpRow]["Masthiri_Incentive"].ToString();
                                }
                                else
                                {
                                    //Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["Lunch"] = Atten_Sal_DT.Rows[EmpRow]["Lunch"].ToString();
                                    //Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["OTFoodAmtNew"] = Atten_Sal_DT.Rows[EmpRow]["OTFoodAmtNew"].ToString();
                                }
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["GrossAmt"] = Atten_Sal_DT.Rows[EmpRow]["GrossEarnings"].ToString();
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["PF"] = Atten_Sal_DT.Rows[EmpRow]["Emp_PF"].ToString();
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["ESI"] = Atten_Sal_DT.Rows[EmpRow]["EmployeerESI"].ToString();

                                //Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["Fine"] = Atten_Sal_DT.Rows[EmpRow]["DedOthers1"].ToString();
                                //Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["Adv"] = Atten_Sal_DT.Rows[EmpRow]["Advance"].ToString();
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["A1"] = Atten_Sal_DT.Rows[EmpRow]["allowances1"].ToString();
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["A2"] = Atten_Sal_DT.Rows[EmpRow]["allowances2"].ToString();
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["A3"] = Atten_Sal_DT.Rows[EmpRow]["allowances3"].ToString();
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["A4"] = Atten_Sal_DT.Rows[EmpRow]["allowances4"].ToString();

                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["TotalWages"] = Atten_Sal_DT.Rows[EmpRow]["NetPay"].ToString();
                            }
                        }



                        string attachment = "attachment;filename=Payslip.xls";
                        Response.ClearContent();
                        Response.AddHeader("content-disposition", attachment);
                        Response.ContentType = "application/ms-excel";
                        StringWriter stw = new StringWriter();
                        HtmlTextWriter htextw = new HtmlTextWriter(stw);
                        DataTable dt = new DataTable();
                        query = "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                        dt = objdata.RptEmployeeMultipleDetails(query);
                        if (dt.Rows.Count > 0)
                        {
                            CmpName = dt.Rows[0]["Cname"].ToString().ToUpper();
                            Cmpaddress = (dt.Rows[0]["Address1"].ToString().ToUpper() + " - " + dt.Rows[0]["Address2"].ToString().ToUpper());// "-" + dt.Rows[0]["Pincode"].ToString());
                        }

                        Response.Write("<table style='font-weight:bold;'>");
                        Response.Write("<tr align='Center'>");
                        Response.Write("<td colspan='22'>");
                        Response.Write(CmpName.ToString().ToUpper());
                        Response.Write("</td>");
                        Response.Write("</tr>");
                        Response.Write("<tr align='Center'>");
                        Response.Write("<td colspan='22'>");
                        Response.Write("" + Cmpaddress + "");
                        Response.Write("</td>");
                        Response.Write("</tr>");
                        Response.Write("<tr align='Center'>");
                        Response.Write("<td colspan='22'>");
                        Response.Write(txtEmployeeType.SelectedItem.Text + " WAGES FOR " + txtfrom.Text + "  TO  " + txtTo.Text);
                        Response.Write(Title);
                        Response.Write("</td>");
                        Response.Write("</tr>");
                        Response.Write("</table>");
                        Response.Write("<table border='1'>");
                        Response.Write("<tr style='font-weight: bold;' align='center'>");

                        if (Pay_Slip_DT.Rows.Count > 0)
                        {
                            Response.Write("<td>S.NO</td><td>PF NUMBER</td><td>EMP NO</td><td>NAME</td>");
                            DateTime date1;
                            DateTime date2 = new DateTime();
                            date1 = Convert.ToDateTime(txtfrom.Text);
                            date2 = Convert.ToDateTime(txtTo.Text);
                            dayscount = (int)((date2 - date1).TotalDays + 1);
                            daysAdded = 0;

                            int daycount = (int)((toDate - fmDate).TotalDays);
                            daysAdded = 0;

                            while (daycount >= 0)
                            {
                                int dayy = Convert.ToDateTime(fmDate.AddDays(daysAdded)).Day;
                                Response.Write("<td>");
                                Response.Write("<table border='1'><tr  style='font-weight: bold;' align='center'><td colspan='1' align='center'>" + dayy + "</td></tr><tr  style='font-weight: bold;' align='center'><td>AT</td></tr></table>");
                                Response.Write("</td>");
                                daycount -= 1;
                                daysAdded += 1;
                            }

                            Response.Write("<td>");
                            Response.Write("<table border='1'><tr  style='font-weight: bold;' align='center'><td colspan='8' align='center'>BASE WAGES</td></tr><tr  style='font-weight: bold;' align='center'><td>DAYS</td><td>PER DAY</td><td>BASIC</td><td align='center'>A1</td><td align='center'>A2</td><td align='center'>A3</td><td align='center'>A4</td><td align='center'>TOTAL</td></tr></table>");
                            Response.Write("</td>");
                            //Response.Write("<td>");
                            //Response.Write("<table border='1'><tr  style='font-weight: bold;' align='center'><td colspan='3' align='center'>OT</td></tr><tr  style='font-weight: bold;' align='center'><td>HRS</td><td>PER HOUR</td><td>AMT</td></tr></table>");
                            //Response.Write("</td>");
                            //if (txtEmployeeType.SelectedItem.Text == "WEEKLY")
                            //{
                            //    Response.Write("<td>");
                            //    Response.Write("<table border='1'><tr  style='font-weight: bold;' align='center'><td align='center'>BUS/INCENT</td></tr><tr  style='font-weight: bold;' align='center'><td>AMT</td></tr></table>");
                            //    Response.Write("</td>");
                            //    Response.Write("<td>");
                            //    Response.Write("<table border='1'><tr  style='font-weight: bold;' align='center'><td align='center'>MASTHIRI</td></tr><tr  style='font-weight: bold;' align='center'><td>AMT</td></tr></table>");
                            //    Response.Write("</td>");
                            //}
                            //else if (txtEmployeeType.SelectedItem.Text == "WEEKLY-HINDI")
                            //{
                            //    Response.Write("<td>");
                            //    Response.Write("<table border='1'><tr  style='font-weight: bold;' align='center'><td align='center'>LUNCH</td></tr><tr  style='font-weight: bold;' align='center'><td>AMT</td></tr></table>");
                            //    Response.Write("</td>");
                            //    Response.Write("<td>");
                            //    Response.Write("<table border='1'><tr  style='font-weight: bold;' align='center'><td align='center'>OTFOOD</td></tr><tr  style='font-weight: bold;' align='center'><td>AMT</td></tr></table>");
                            //    Response.Write("</td>");
                            //}

                            Response.Write("<td>");
                            Response.Write("<table border='1'><tr  style='font-weight: bold;' align='center'><td align='center'>PF</td></tr></table>");
                            Response.Write("</td>");
                            Response.Write("<td>");
                            Response.Write("<table border='1'><tr  style='font-weight: bold;' align='center'><td align='center'>ESI</td></tr></table>");
                            Response.Write("</td>");

                            //Response.Write("<td>");
                            //Response.Write("<table border='1'><tr  style='font-weight: bold;' align='center'><td align='center'>ADV</td></tr><tr  style='font-weight: bold;' align='center'><td>AMT</td></tr></table>");
                            //Response.Write("</td>");
                            //Response.Write("<td>");
                            //Response.Write("<table border='1'><tr  style='font-weight: bold;' align='center'><td align='center'>FINE</td></tr><tr  style='font-weight: bold;' align='center'><td>AMT</td></tr></table>");
                            //Response.Write("</td>");
                            //Response.Write("<td>");
                            //Response.Write("<table border='1'><tr  style='font-weight: bold;' align='center'><td align='center'>A1</td></tr></table>");
                            //Response.Write("</td>");
                            //Response.Write("<td>");
                            //Response.Write("<table border='1'><tr  style='font-weight: bold;' align='center'><td align='center'>A2</td></tr></table>");
                            //Response.Write("</td>");
                            //Response.Write("<td>");
                            //Response.Write("<table border='1'><tr  style='font-weight: bold;' align='center'><td align='center'>A3</td></tr></table>");
                            //Response.Write("</td>");
                            //Response.Write("<td>");
                            //Response.Write("<table border='1'><tr  style='font-weight: bold;' align='center'><td align='center'>A4</td></tr></table>");
                            //Response.Write("</td>");
                            Response.Write("<td>");
                            Response.Write("<table border='1'><tr  style='font-weight: bold;' align='center'><td align='center'>TOTAL WAGES</td></tr><tr  style='font-weight: bold;' align='center'><td>NET AMT</td></tr></table>");
                            Response.Write("</td>");
                            Response.Write("<td>SIGN</td>");
                            Response.Write("</tr>");

                            grid.DataSource = Pay_Slip_DT;
                            grid.DataBind();
                            grid.ShowHeader = false;
                            grid.RenderControl(htextw);
                            Response.Write(stw.ToString());
                        }
                        Response.Write("<tr>");
                        Response.Write("<td colspan='29'>");
                        Response.Write("<table border='1'>");
                        Response.Write("<tr style='font-weight: bold;'>");
                        Response.Write("<td colspan='13'   align='center'>");
                        Response.Write("TOTAL");
                        Response.Write("</td>");
                        //Response.Write("<td>=sum(M6:M" + Convert.ToInt32(Pay_Slip_DT.Rows.Count + 5) + ")</td>");
                       // Response.Write("<td></td><td></td>");
                       // Response.Write("<td>=sum(M6:M" + Convert.ToInt32(Pay_Slip_DT.Rows.Count + 5) + ")</td>");
                        Response.Write("<td>=sum(N6:N" + Convert.ToInt32(Pay_Slip_DT.Rows.Count + 5) + ")</td>");
                        Response.Write("<td>=sum(O6:O" + Convert.ToInt32(Pay_Slip_DT.Rows.Count + 5) + ")</td>");
                        Response.Write("<td>=sum(P6:P" + Convert.ToInt32(Pay_Slip_DT.Rows.Count + 5) + ")</td>");
                        Response.Write("<td>=sum(Q6:Q" + Convert.ToInt32(Pay_Slip_DT.Rows.Count + 5) + ")</td>");
                        Response.Write("<td>=sum(R6:R" + Convert.ToInt32(Pay_Slip_DT.Rows.Count + 5) + ")</td>");
                        Response.Write("<td>=sum(S6:S" + Convert.ToInt32(Pay_Slip_DT.Rows.Count + 5) + ")</td>");
                        Response.Write("<td>=sum(T6:T" + Convert.ToInt32(Pay_Slip_DT.Rows.Count + 5) + ")</td>");
                        Response.Write("<td>=sum(U6:U" + Convert.ToInt32(Pay_Slip_DT.Rows.Count + 5) + ")</td>");
                        Response.Write("<td>=sum(V6:V" + Convert.ToInt32(Pay_Slip_DT.Rows.Count + 5) + ")</td>");
                        Response.Write("</tr>");
                        Response.Write("</table>");
                        Response.Write("</td>");
                        Response.Write("</tr>");
                        Response.Write("</table>");

                        Response.End();
                        Response.Clear();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Downloaded Successfully');", true);
                        ErrFlag = false;
                    }
                }
                else //staff Salary Payslip
                {
                    if (!ErrFlag)
                    {
                        Pay_Slip_DT.Columns.Add("Sno");
                        Pay_Slip_DT.Columns.Add("PFNo");
                        Pay_Slip_DT.Columns.Add("EmpNo");
                        Pay_Slip_DT.Columns.Add("Name");
                        Pay_Slip_DT.Columns.Add("Days");
                        Pay_Slip_DT.Columns.Add("Perday");
                        Pay_Slip_DT.Columns.Add("Gross");
                        Pay_Slip_DT.Columns.Add("A1");
                        Pay_Slip_DT.Columns.Add("A2");
                        Pay_Slip_DT.Columns.Add("A3");
                        Pay_Slip_DT.Columns.Add("A4");
                        Pay_Slip_DT.Columns.Add("GrossAmt");
                        //Pay_Slip_DT.Columns.Add("Adv");
                        //Pay_Slip_DT.Columns.Add("Fine");
                        Pay_Slip_DT.Columns.Add("PF");
                        Pay_Slip_DT.Columns.Add("ESI");
                        Pay_Slip_DT.Columns.Add("TotalWages");
                        Pay_Slip_DT.Columns.Add("Sign");

                        query = "";
                        query = "Select ED.FirstName,ED.PFNo,Attn_DT.EmpNo,Attn_DT.Days,Attn_DT.Months,Attn_DT.FinancialYear,Attn_DT.FromDate,Attn_DT.ToDate,";
                        query = query + "Attn_DT.Day1_WH,Attn_DT.Day1_OT,Attn_DT.Day2_WH,Attn_DT.Day2_OT,Attn_DT.Day3_WH,Attn_DT.Day3_OT,Attn_DT.Day4_WH,Attn_DT.Day4_OT,";
                        query = query + "Attn_DT.Day5_WH,Attn_DT.Day5_OT,Attn_DT.Day6_WH,Attn_DT.Day6_OT,Attn_DT.Day7_WH,Attn_DT.Day7_OT,Attn_DT.OTHoursNew,";
                        query = query + "Sal_DT.Fbasic,Sal_DT.BasicandDA,Sal_DT.GrossEarnings,Sal_DT.NetPay,Sal_DT.Advance,Sal_DT.OTHoursAmtNew as Per_hour,Sal_DT.Manual_OT,";
                        query = query + "Sal_DT.Bus_Incentive,Sal_DT.Masthiri_Incentive,Sal_DT.Lunch,Sal_DT.OTFoodAmtNew,Sal_DT.DedOthers1,Sal_DT.allowances1,Sal_DT.allowances2,Sal_DT.allowances3,Sal_DT.allowances4,Sal_DT.Emp_PF,Sal_DT.EmployeerESI ";
                        query = query + " from [" + SessionPayroll + "]..AttenanceDetails Attn_DT Inner Join [" + SessionPayroll + "]..SalaryDetails_IF Sal_DT on Attn_DT.EmpNo=Sal_DT.EmpNo";
                        query = query + " Inner Join [VPS_Spay]..Employee_Mst ED on Attn_DT.EmpNo=ED.EmpNo and Sal_DT.EmpNo=ED.EmpNo ";
                        query = query + " where  Attn_DT.Months=Sal_DT.Month and Attn_DT.FinancialYear=Sal_DT.FinancialYear  and ED.Eligible_PF='1' and ED.Eligible_ESI='1'";
                        query = query + "and Convert(datetime,Attn_DT.FromDate,105)=Convert(datetime,Sal_DT.FromDate,105) and Convert(datetime,Attn_DT.ToDate,105)=Convert(datetime,Sal_DT.ToDate,105) ";
                        query = query + "and Attn_DT.Months='" + ddlMonths.SelectedItem.Text + "' and Attn_DT.FinancialYear='" + ddlFinance.SelectedValue + "' and Attn_DT.Ccode='" + SessionCcode + "' and Attn_DT.Lcode='" + SessionLcode + "' ";
                        query = query + "and Sal_DT.Ccode='" + SessionCcode + "' and Sal_DT.Lcode='" + SessionLcode + "' and convert(datetime,Attn_DT.FromDate,105) = convert(datetime,'" + txtfrom.Text + "',105) and convert(datetime, Attn_DT.ToDate, 105) = convert(datetime, '" + txtTo.Text + "', 105)";
                        //query = query + " and ED.EligiblePF='1' and ED.ElgibleESI='1'";

                        if (ddlShiftType.SelectedItem.Text != "-Select-")
                        {
                            query = query + " and ED.ShiftType_New='" + ddlShiftType.SelectedItem.Text + "'";
                        }
                        if (ddlDept.SelectedItem.Text != "-Select-")
                        {
                            query = query + " and ED.DeptName='" + ddlDept.SelectedItem.Text + "'";
                        }
                        if (txtEmployeeType.SelectedItem.Text != "-Select-")
                        {
                            query = query + " and ED.Wages='" + txtEmployeeType.SelectedItem.Text + "'";
                        }
                        query = query + " and Attn_DT.Days>'0'";
                        Atten_Sal_DT = objdata.RptEmployeeMultipleDetails(query);
                        if (Atten_Sal_DT.Rows.Count != 0)
                        {
                            for (int EmpRow = 0; EmpRow < Atten_Sal_DT.Rows.Count; EmpRow++)
                            {
                                Pay_Slip_DT.NewRow();
                                Pay_Slip_DT.Rows.Add();
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["Sno"] = Pay_Slip_DT.Rows.Count;
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count-1]["PFNo"] = Atten_Sal_DT.Rows[EmpRow]["PFNo"].ToString();
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["EmpNo"] = Atten_Sal_DT.Rows[EmpRow]["EmpNo"].ToString();
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["Name"] = Atten_Sal_DT.Rows[EmpRow]["FirstName"].ToString();
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["Days"] = Atten_Sal_DT.Rows[EmpRow]["Days"].ToString();
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["Perday"] = Atten_Sal_DT.Rows[EmpRow]["BasicandDA"].ToString();
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["Gross"] = Atten_Sal_DT.Rows[EmpRow]["Fbasic"].ToString();
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["PF"] = Atten_Sal_DT.Rows[EmpRow]["Emp_PF"].ToString();
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["ESI"] = Atten_Sal_DT.Rows[EmpRow]["EmployeerESI"].ToString();
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["GrossAmt"] = Atten_Sal_DT.Rows[EmpRow]["GrossEarnings"].ToString();
                                //Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["Fine"] = Atten_Sal_DT.Rows[EmpRow]["DedOthers1"].ToString();
                                //Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["Adv"] = Atten_Sal_DT.Rows[EmpRow]["Advance"].ToString();
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["A1"] = Atten_Sal_DT.Rows[EmpRow]["allowances1"].ToString();
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["A2"] = Atten_Sal_DT.Rows[EmpRow]["allowances2"].ToString();
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["A3"] = Atten_Sal_DT.Rows[EmpRow]["allowances3"].ToString();
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["A4"] = Atten_Sal_DT.Rows[EmpRow]["allowances4"].ToString();
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["TotalWages"] = Atten_Sal_DT.Rows[EmpRow]["NetPay"].ToString();
                            }
                        }
                        string attachment = "attachment;filename=Payslip.xls";
                        Response.ClearContent();
                        Response.AddHeader("content-disposition", attachment);
                        Response.ContentType = "application/ms-excel";
                        StringWriter stw = new StringWriter();
                        HtmlTextWriter htextw = new HtmlTextWriter(stw);
                        DataTable dt = new DataTable();
                        query = "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                        dt = objdata.RptEmployeeMultipleDetails(query);
                        if (dt.Rows.Count > 0)
                        {
                            CmpName = dt.Rows[0]["Cname"].ToString().ToUpper();
                            Cmpaddress = (dt.Rows[0]["Address1"].ToString().ToUpper() + " - " + dt.Rows[0]["Address2"].ToString().ToUpper());// "-" + dt.Rows[0]["Pincode"].ToString());
                        }
                        Response.Write("<table style='font-weight:bold;'>");
                        Response.Write("<tr align='Center'>");
                        Response.Write("<td colspan='15'>");
                        Response.Write(CmpName.ToString().ToUpper());
                        Response.Write("</td>");
                        Response.Write("</tr>");
                        Response.Write("<tr align='Center'>");
                        Response.Write("<td colspan='15'>");
                        Response.Write("" + Cmpaddress + "");
                        Response.Write("</td>");
                        Response.Write("</tr>");
                        Response.Write("<tr align='Center'>");
                        Response.Write("<td colspan='15'>");
                        Response.Write(txtEmployeeType.SelectedItem.Text + " WAGES FOR " + txtfrom.Text + "  TO  " + txtTo.Text);
                        Response.Write("</td>");
                        Response.Write("</tr>");
                        Response.Write("</table>");
                        Response.Write("<table border='1'>");
                        Response.Write("<tr style='font-weight: bold;' align='center'>");
                        if (Pay_Slip_DT.Rows.Count > 0)
                        {
                            Response.Write("<td>S.NO</td><td>PF Number</td><td>EMP NO</td><td>NAME</td>");
                            DateTime date1;
                            DateTime date2 = new DateTime();
                            date1 = Convert.ToDateTime(txtfrom.Text);
                            date2 = Convert.ToDateTime(txtTo.Text);
                            dayscount = (int)((date2 - date1).TotalDays + 1);
                            daysAdded = 0;

                            int daycount = (int)((toDate - fmDate).TotalDays);
                            daysAdded = 0;
                            Response.Write("<td>");
                            Response.Write("<table border='1'><tr  style='font-weight: bold;' align='center'><td colspan='8' align='center'>BASE WAGES</td></tr><tr  style='font-weight: bold;' align='center'><td>DAYS</td><td>PER DAY</td><td>BASIC</td><td align='center'>A1</td><td align='center'>A2</td><td align='center'>A3</td><td align='center'>A4</td><td align='center'>TOTAL</td></tr></table>");
                            Response.Write("</td>");

                            
                            //Response.Write("<td>");
                            //Response.Write("<table border='1'><tr  style='font-weight: bold;' align='center'><td align='center'>FINE</td></tr><tr  style='font-weight: bold;' align='center'><td>AMT</td></tr></table>");
                            //Response.Write("</td>");
                            //Response.Write("<td>");
                            //Response.Write("<table border='1'><tr  style='font-weight: bold;' align='center'><td align='center'>ADV</td></tr><tr  style='font-weight: bold;' align='center'><td>AMT</td></tr></table>");
                            //Response.Write("</td>");
                            
                            Response.Write("</td>");
                            Response.Write("<td>");
                            Response.Write("<table border='1'><tr  style='font-weight: bold;' align='center'><td align='center'>PF</td></tr></table>");
                            Response.Write("</td>");
                            Response.Write("<td>");
                            Response.Write("<table border='1'><tr  style='font-weight: bold;' align='center'><td align='center'>ESI</td></tr></table>");
                            Response.Write("</td>");
                            Response.Write("<td>");
                            Response.Write("<table border='1'><tr  style='font-weight: bold;' align='center'><td align='center'>TOTAL WAGES</td></tr><tr  style='font-weight: bold;' align='center'><td>AMT</td></tr></table>");
                            Response.Write("</td>");
                            Response.Write("<td>SIGN</td>");
                            Response.Write("</tr>");

                            grid.DataSource = Pay_Slip_DT;
                            grid.DataBind();
                            grid.ShowHeader = false;
                            grid.RenderControl(htextw);
                            Response.Write(stw.ToString());

                        }
                        Response.Write("<tr>");
                        Response.Write("<td colspan='29'>");
                        Response.Write("<table border='1'>");
                        Response.Write("<tr style='font-weight: bold;'>");
                        Response.Write("<td colspan='6'   align='center'>");
                        Response.Write("TOTAL");
                        Response.Write("</td>");
                        Response.Write("<td>=sum(G6:G" + Convert.ToInt32(Pay_Slip_DT.Rows.Count + 5) + ")</td>");
                        Response.Write("<td></td><td></td><td></td><td></td>");
                        Response.Write("<td>=sum(L6:L" + Convert.ToInt32(Pay_Slip_DT.Rows.Count + 5) + ")</td>");
                        Response.Write("<td>=sum(M6:M" + Convert.ToInt32(Pay_Slip_DT.Rows.Count + 5) + ")</td>");
                        Response.Write("<td>=sum(N6:N" + Convert.ToInt32(Pay_Slip_DT.Rows.Count + 5) + ")</td>");
                        Response.Write("<td>=sum(O6:O" + Convert.ToInt32(Pay_Slip_DT.Rows.Count + 5) + ")</td>");

                        Response.Write("</tr>");
                        Response.Write("</table>");
                        Response.Write("</td>");
                        Response.Write("</tr>");
                        Response.Write("</table>");

                        Response.End();
                        Response.Clear();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Downloaded Successfully');", true);
                        ErrFlag = false;

                    }
                }
            }
            else//NormalUser
            {

                if (txtEmployeeType.SelectedItem.Text == "WEEKLY" || txtEmployeeType.SelectedItem.Text == "WEEKLY-HINDI")
                {
                    if (!ErrFlag)
                    {
                        Pay_Slip_DT.Columns.Add("Sno");
                        Pay_Slip_DT.Columns.Add("EmpNo");
                        Pay_Slip_DT.Columns.Add("Name");
                        while (dayscount >= 0)
                        {
                            //DateTime dayy = Convert.ToDateTime(fmDate.AddDays(daysAdded).ToShortDateString());
                            Pay_Slip_DT.Columns.Add("AT" + daysAdded);
                            Pay_Slip_DT.Columns.Add("OT" + daysAdded);
                            dayscount -= 1;
                            daysAdded += 1;
                        }
                        Pay_Slip_DT.Columns.Add("Days");
                        Pay_Slip_DT.Columns.Add("Perday");
                        Pay_Slip_DT.Columns.Add("Gross");
                        Pay_Slip_DT.Columns.Add("OThrs");
                        Pay_Slip_DT.Columns.Add("PerHr");
                        Pay_Slip_DT.Columns.Add("OTamt");
                        if (txtEmployeeType.SelectedItem.Text == "WEEKLY")
                        {
                            Pay_Slip_DT.Columns.Add("Businc");
                            Pay_Slip_DT.Columns.Add("MasthiriInc");
                        }
                        else
                        {
                            Pay_Slip_DT.Columns.Add("Lunch");
                            Pay_Slip_DT.Columns.Add("OTFoodAmtNew");
                        }
                        if (txtEmployeeType.SelectedItem.Text == "WEEKLY")
                        {
                            Pay_Slip_DT.Columns.Add("PF");
                            Pay_Slip_DT.Columns.Add("ESI");
                        }

                        Pay_Slip_DT.Columns.Add("Adv");
                        Pay_Slip_DT.Columns.Add("Fine");
                        Pay_Slip_DT.Columns.Add("DayIncentive");
                        Pay_Slip_DT.Columns.Add("TotalWages");
                        Pay_Slip_DT.Columns.Add("Sign");


                        decimal TotalBaseWages = 0;
                        decimal ToTalOTAmt = 0;
                        decimal ToTalBusInc = 0;
                        decimal ToTalMasthiriInc = 0;
                        decimal ToaTalAdv = 0;
                        decimal TotalWages = 0;

                        if (txtEmployeeType.SelectedItem.Text == "WEEKLY")
                        {
                            query = "";
                            query = "Select (ED.FirstName+'.'+ED.LastName) as FirstName,Attn_DT.EmpNo,Attn_DT.Days,Attn_DT.Months,Attn_DT.FinancialYear,Attn_DT.FromDate,Attn_DT.ToDate,";
                            query = query + "Attn_DT.Day1_WH,Attn_DT.Day1_OT,Attn_DT.Day2_WH,Attn_DT.Day2_OT,Attn_DT.Day3_WH,Attn_DT.Day3_OT,Attn_DT.Day4_WH,Attn_DT.Day4_OT,";
                            query = query + "Attn_DT.Day5_WH,Attn_DT.Day5_OT,Attn_DT.Day6_WH,Attn_DT.Day6_OT,Attn_DT.Day7_WH,Attn_DT.Day7_OT,Attn_DT.OTHoursNew,";
                            query = query + "Sal_DT.Fbasic,Sal_DT.BasicandDA,Sal_DT.GrossEarnings,Sal_DT.RoundOffNetpay as Netpay,Sal_DT.Advance,Sal_DT.OTHoursAmtNew as Per_hour,Sal_DT.Manual_OT,";
                            query = query + "Sal_DT.Bus_Incentive,Sal_DT.Masthiri_Incentive,Sal_DT.Lunch,Sal_DT.OTFoodAmtNew,Sal_DT.DedOthers1,Sal_DT.Deduction3,Sal_DT.Deduction4,Sal_DT.DayIncentive ";
                            query = query + " from [" + SessionPayroll + "]..AttenanceDetails Attn_DT Inner Join [" + SessionPayroll + "]..SalaryDetails Sal_DT on Attn_DT.EmpNo=Sal_DT.EmpNo";
                            query = query + " Inner Join Employee_Mst ED on Attn_DT.EmpNo=ED.EmpNo and Sal_DT.EmpNo=ED.EmpNo ";
                            query = query + " where  Attn_DT.Months=Sal_DT.Month and Attn_DT.FinancialYear=Sal_DT.FinancialYear ";
                            query = query + "and Convert(datetime,Attn_DT.FromDate,105)=Convert(datetime,Sal_DT.FromDate,105) and Convert(datetime,Attn_DT.ToDate,105)=Convert(datetime,Sal_DT.ToDate,105) ";
                            query = query + "and Attn_DT.Months='" + ddlMonths.SelectedItem.Text + "' and Attn_DT.FinancialYear='" + ddlFinance.SelectedValue + "' and Attn_DT.Ccode='" + SessionCcode + "' and Attn_DT.Lcode='" + SessionLcode + "' ";
                            query = query + "and Sal_DT.Ccode='" + SessionCcode + "' and Sal_DT.Lcode='" + SessionLcode + "' and convert(datetime,Attn_DT.FromDate,105) = convert(datetime,'" + txtfrom.Text + "',105) and convert(datetime, Attn_DT.ToDate, 105) = convert(datetime, '" + txtTo.Text + "', 105)";
                        }
                        else
                        {
                            query = "";
                            query = "Select ED.FirstName,Attn_DT.EmpNo,Attn_DT.Days,Attn_DT.Months,Attn_DT.FinancialYear,Attn_DT.FromDate,Attn_DT.ToDate,";
                            query = query + "Attn_DT.Day1_WH,Attn_DT.Day1_OT,Attn_DT.Day2_WH,Attn_DT.Day2_OT,Attn_DT.Day3_WH,Attn_DT.Day3_OT,Attn_DT.Day4_WH,Attn_DT.Day4_OT,";
                            query = query + "Attn_DT.Day5_WH,Attn_DT.Day5_OT,Attn_DT.Day6_WH,Attn_DT.Day6_OT,Attn_DT.Day7_WH,Attn_DT.Day7_OT,Attn_DT.OTHoursNew,";
                            query = query + "Sal_DT.Fbasic,Sal_DT.BasicandDA,Sal_DT.GrossEarnings,Sal_DT.RoundOffNetpay as Netpay,Sal_DT.Advance,Sal_DT.OTHoursAmtNew as Per_hour,Sal_DT.Manual_OT,";
                            query = query + "Sal_DT.Bus_Incentive,Sal_DT.Masthiri_Incentive,Sal_DT.Lunch,Sal_DT.OTFoodAmtNew,Sal_DT.DedOthers1 ";
                            query = query + " from [" + SessionPayroll + "]..AttenanceDetails Attn_DT Inner Join [" + SessionPayroll + "]..SalaryDetails Sal_DT on Attn_DT.EmpNo=Sal_DT.EmpNo";
                            query = query + " Inner Join Employee_Mst ED on Attn_DT.EmpNo=ED.EmpNo and Sal_DT.EmpNo=ED.EmpNo ";
                            query = query + " where  Attn_DT.Months=Sal_DT.Month and Attn_DT.FinancialYear=Sal_DT.FinancialYear ";
                            query = query + "and Convert(datetime,Attn_DT.FromDate,105)=Convert(datetime,Sal_DT.FromDate,105) and Convert(datetime,Attn_DT.ToDate,105)=Convert(datetime,Sal_DT.ToDate,105) ";
                            query = query + "and Attn_DT.Months='" + ddlMonths.SelectedItem.Text + "' and Attn_DT.FinancialYear='" + ddlFinance.SelectedValue + "' and Attn_DT.Ccode='" + SessionCcode + "' and Attn_DT.Lcode='" + SessionLcode + "' ";
                            query = query + "and Sal_DT.Ccode='" + SessionCcode + "' and Sal_DT.Lcode='" + SessionLcode + "' and convert(datetime,Attn_DT.FromDate,105) = convert(datetime,'" + txtfrom.Text + "',105) and convert(datetime, Attn_DT.ToDate, 105) = convert(datetime, '" + txtTo.Text + "', 105)";

                            if (ddlHindiCategory.SelectedItem.Text != "-Select-")
                            {
                                query = query + " and ED.Hindi_Category='" + ddlHindiCategory.SelectedItem.Text + "'";
                                Title = Title + "  Hindi_Category=" + ddlHindiCategory.SelectedItem.Text + " ";
                            }

                        }
                        if (ddlShiftType.SelectedItem.Text != "-Select-")
                        {
                            query = query + " and ED.ShiftType_New='" + ddlShiftType.SelectedItem.Text + "'";
                            Title = Title + "  Shift=" + ddlShiftType.SelectedItem.Text + " ";
                        }
                        if (ddlDept.SelectedItem.Text != "-Select-")
                        {
                            query = query + " and ED.DeptName='" + ddlDept.SelectedItem.Text + "'";
                            Title = Title + "   DeptName=" + ddlDept.SelectedItem.Text + " ";
                        }
                        if (txtEmployeeType.SelectedItem.Text != "-Select-")
                        {
                            query = query + " and ED.Wages='" + txtEmployeeType.SelectedItem.Text + "'";

                        }
                        if (txtmiltype.SelectedValue != "0")
                        {
                            query = query + " and ED.Unit_Type='" + txtmiltype.SelectedValue + "'";
                        }
                        query = query + "  and Attn_Dt.Days !='0' order by ED.EmpNo";


                        Atten_Sal_DT = objdata.RptEmployeeMultipleDetails(query);

                        if (Atten_Sal_DT.Rows.Count != 0)
                        {
                            for (int EmpRow = 0; EmpRow < Atten_Sal_DT.Rows.Count; EmpRow++)
                            {
                                Pay_Slip_DT.NewRow();
                                Pay_Slip_DT.Rows.Add();
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["Sno"] = Pay_Slip_DT.Rows.Count;
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["EmpNo"] = Atten_Sal_DT.Rows[EmpRow]["EmpNo"].ToString();
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["Name"] = Atten_Sal_DT.Rows[EmpRow]["FirstName"].ToString();

                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["AT0"] = Atten_Sal_DT.Rows[EmpRow]["Day1_WH"].ToString();
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["OT0"] = Atten_Sal_DT.Rows[EmpRow]["Day1_OT"].ToString();
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["AT1"] = Atten_Sal_DT.Rows[EmpRow]["Day2_WH"].ToString();
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["OT1"] = Atten_Sal_DT.Rows[EmpRow]["Day2_OT"].ToString();
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["AT2"] = Atten_Sal_DT.Rows[EmpRow]["Day3_WH"].ToString();
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["OT2"] = Atten_Sal_DT.Rows[EmpRow]["Day3_OT"].ToString();
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["AT3"] = Atten_Sal_DT.Rows[EmpRow]["Day4_WH"].ToString();
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["OT3"] = Atten_Sal_DT.Rows[EmpRow]["Day4_OT"].ToString();
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["AT4"] = Atten_Sal_DT.Rows[EmpRow]["Day5_WH"].ToString();
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["OT4"] = Atten_Sal_DT.Rows[EmpRow]["Day5_OT"].ToString();
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["AT5"] = Atten_Sal_DT.Rows[EmpRow]["Day6_WH"].ToString();
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["OT5"] = Atten_Sal_DT.Rows[EmpRow]["Day6_OT"].ToString();
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["AT6"] = Atten_Sal_DT.Rows[EmpRow]["Day7_WH"].ToString();
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["OT6"] = Atten_Sal_DT.Rows[EmpRow]["Day7_OT"].ToString();

                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["Days"] = Atten_Sal_DT.Rows[EmpRow]["Days"].ToString();
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["Perday"] = Atten_Sal_DT.Rows[EmpRow]["BasicandDA"].ToString();
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["Gross"] = Atten_Sal_DT.Rows[EmpRow]["Fbasic"].ToString();
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["OThrs"] = Atten_Sal_DT.Rows[EmpRow]["OTHoursNew"].ToString();
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["PerHr"] = Atten_Sal_DT.Rows[EmpRow]["Per_hour"].ToString();
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["OTamt"] = Atten_Sal_DT.Rows[EmpRow]["Manual_OT"].ToString();
                                if (txtEmployeeType.SelectedItem.Text == "WEEKLY")
                                {
                                    Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["Businc"] = Atten_Sal_DT.Rows[EmpRow]["Bus_Incentive"].ToString();
                                    Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["MasthiriInc"] = Atten_Sal_DT.Rows[EmpRow]["Masthiri_Incentive"].ToString();
                                }
                                else
                                {
                                    Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["Lunch"] = Atten_Sal_DT.Rows[EmpRow]["Lunch"].ToString();
                                    Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["OTFoodAmtNew"] = Atten_Sal_DT.Rows[EmpRow]["OTFoodAmtNew"].ToString();
                                }
                                if (txtEmployeeType.SelectedItem.Text == "WEEKLY")
                                {
                                    Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["PF"] = Atten_Sal_DT.Rows[EmpRow]["Deduction3"].ToString();
                                    Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["ESI"] = Atten_Sal_DT.Rows[EmpRow]["Deduction4"].ToString();
                                }

                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["Fine"] = Atten_Sal_DT.Rows[EmpRow]["DedOthers1"].ToString();
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["Adv"] = Atten_Sal_DT.Rows[EmpRow]["Advance"].ToString();
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["DayIncentive"] = Atten_Sal_DT.Rows[EmpRow]["DayIncentive"].ToString();
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["TotalWages"] = Atten_Sal_DT.Rows[EmpRow]["NetPay"].ToString();
                            }
                        }



                        string attachment = "attachment;filename=Payslip.xls";
                        Response.ClearContent();
                        Response.AddHeader("content-disposition", attachment);
                        Response.ContentType = "application/ms-excel";
                        StringWriter stw = new StringWriter();
                        HtmlTextWriter htextw = new HtmlTextWriter(stw);
                        DataTable dt = new DataTable();
                        query = "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                        dt = objdata.RptEmployeeMultipleDetails(query);
                        if (dt.Rows.Count > 0)
                        {
                            CmpName = dt.Rows[0]["Cname"].ToString().ToUpper();
                            Cmpaddress = (dt.Rows[0]["Address1"].ToString().ToUpper() + " - " + dt.Rows[0]["Address2"].ToString().ToUpper());// "-" + dt.Rows[0]["Pincode"].ToString());
                        }

                        Response.Write("<table style='font-weight:bold;'>");
                        Response.Write("<tr align='Center'>");
                        Response.Write("<td colspan='28'>");
                        Response.Write(CmpName.ToString().ToUpper());
                        Response.Write("</td>");
                        Response.Write("</tr>");
                        Response.Write("<tr align='Center'>");
                        Response.Write("<td colspan='28'>");
                        Response.Write("" + Cmpaddress + "");
                        Response.Write("</td>");
                        Response.Write("</tr>");
                        Response.Write("<tr align='Center'>");
                        Response.Write("<td colspan='28'>");
                        Response.Write(txtEmployeeType.SelectedItem.Text + " WAGES FOR " + txtfrom.Text + "  TO  " + txtTo.Text);
                        Response.Write(Title);
                        Response.Write("</td>");
                        Response.Write("</tr>");
                        Response.Write("</table>");
                        Response.Write("<table border='1'>");
                        Response.Write("<tr style='font-weight: bold;' align='center'>");

                        if (Pay_Slip_DT.Rows.Count > 0)
                        {
                            Response.Write("<td>S.NO</td><td>EMP NO</td><td>NAME</td>");
                            DateTime date1;
                            DateTime date2 = new DateTime();
                            date1 = Convert.ToDateTime(txtfrom.Text);
                            date2 = Convert.ToDateTime(txtTo.Text);
                            dayscount = (int)((date2 - date1).TotalDays + 1);
                            daysAdded = 0;

                            int daycount = (int)((toDate - fmDate).TotalDays);
                            daysAdded = 0;

                            while (daycount >= 0)
                            {
                                int dayy = Convert.ToDateTime(fmDate.AddDays(daysAdded)).Day;
                                Response.Write("<td>");
                                Response.Write("<table border='1'><tr  style='font-weight: bold;' align='center'><td colspan='2' align='center'>" + dayy + "</td></tr><tr  style='font-weight: bold;' align='center'><td>AT</td><td>OT</td></tr></table>");
                                Response.Write("</td>");
                                daycount -= 1;
                                daysAdded += 1;
                            }

                            Response.Write("<td>");
                            Response.Write("<table border='1'><tr  style='font-weight: bold;' align='center'><td colspan='3' align='center'>BASE WAGES</td></tr><tr  style='font-weight: bold;' align='center'><td>DAYS</td><td>PER DAY</td><td>AMT</td></tr></table>");
                            Response.Write("</td>");
                            Response.Write("<td>");
                            Response.Write("<table border='1'><tr  style='font-weight: bold;' align='center'><td colspan='3' align='center'>OT</td></tr><tr  style='font-weight: bold;' align='center'><td>HRS</td><td>PER HOUR</td><td>AMT</td></tr></table>");
                            Response.Write("</td>");
                            if (txtEmployeeType.SelectedItem.Text == "WEEKLY")
                            {
                                Response.Write("<td>");
                                Response.Write("<table border='1'><tr  style='font-weight: bold;' align='center'><td align='center'>BUS/INCENT</td></tr><tr  style='font-weight: bold;' align='center'><td>AMT</td></tr></table>");
                                Response.Write("</td>");
                                Response.Write("<td>");
                                Response.Write("<table border='1'><tr  style='font-weight: bold;' align='center'><td align='center'>MASTHIRI</td></tr><tr  style='font-weight: bold;' align='center'><td>AMT</td></tr></table>");
                                Response.Write("</td>");
                            }
                            else if (txtEmployeeType.SelectedItem.Text == "WEEKLY-HINDI")
                            {
                                Response.Write("<td>");
                                Response.Write("<table border='1'><tr  style='font-weight: bold;' align='center'><td align='center'>LUNCH</td></tr><tr  style='font-weight: bold;' align='center'><td>AMT</td></tr></table>");
                                Response.Write("</td>");
                                Response.Write("<td>");
                                Response.Write("<table border='1'><tr  style='font-weight: bold;' align='center'><td align='center'>OTFOOD</td></tr><tr  style='font-weight: bold;' align='center'><td>AMT</td></tr></table>");
                                Response.Write("</td>");
                            }
                            if (txtEmployeeType.SelectedItem.Text == "WEEKLY")
                            {
                                Response.Write("<td>");
                                Response.Write("<table border='1'><tr  style='font-weight: bold;' align='center'><td align='center'>PF</td></tr><tr  style='font-weight: bold;' align='center'><td>AMT</td></tr></table>");
                                Response.Write("</td>");
                                Response.Write("<td>");
                                Response.Write("<table border='1'><tr  style='font-weight: bold;' align='center'><td align='center'>ESI</td></tr><tr  style='font-weight: bold;' align='center'><td>AMT</td></tr></table>");
                                Response.Write("</td>");
                            }

                            Response.Write("<td>");
                            Response.Write("<table border='1'><tr  style='font-weight: bold;' align='center'><td align='center'>ADV</td></tr><tr  style='font-weight: bold;' align='center'><td>AMT</td></tr></table>");
                            Response.Write("</td>");
                            Response.Write("<td>");
                            Response.Write("<table border='1'><tr  style='font-weight: bold;' align='center'><td align='center'>FINE</td></tr><tr  style='font-weight: bold;' align='center'><td>AMT</td></tr></table>");
                            Response.Write("</td>");
                            Response.Write("<td>");
                            Response.Write("<table border='1'><tr  style='font-weight: bold;' align='center'><td align='center'>DAYS INCENTIVE</td></tr><tr  style='font-weight: bold;' align='center'><td>AMT</td></tr></table>");
                            Response.Write("</td>");
                            Response.Write("<td>");
                            Response.Write("<table border='1'><tr  style='font-weight: bold;' align='center'><td align='center'>TOTAL WAGES</td></tr><tr  style='font-weight: bold;' align='center'><td>AMT</td></tr></table>");
                            Response.Write("</td>");
                            Response.Write("<td>SIGN</td>");
                            Response.Write("</tr>");

                            grid.DataSource = Pay_Slip_DT;
                            grid.DataBind();
                            grid.ShowHeader = false;
                            grid.RenderControl(htextw);
                            Response.Write(stw.ToString());
                        }
                        Response.Write("<tr>");
                        Response.Write("<td colspan='28'>");
                        Response.Write("<table border='1'>");
                        Response.Write("<tr style='font-weight: bold;'>");
                        Response.Write("<td colspan='19'   align='center'>");
                        Response.Write("TOTAL");
                        Response.Write("</td>");
                        Response.Write("<td>=sum(T6:T" + Convert.ToInt32(Pay_Slip_DT.Rows.Count + 5) + ")</td>");
                        Response.Write("<td></td><td></td>");
                        Response.Write("<td>=sum(W6:W" + Convert.ToInt32(Pay_Slip_DT.Rows.Count + 5) + ")</td>");
                        Response.Write("<td>=sum(X6:X" + Convert.ToInt32(Pay_Slip_DT.Rows.Count + 5) + ")</td>");
                        Response.Write("<td>=sum(Y6:Y" + Convert.ToInt32(Pay_Slip_DT.Rows.Count + 5) + ")</td>");
                        Response.Write("<td>=sum(Z6:Z" + Convert.ToInt32(Pay_Slip_DT.Rows.Count + 5) + ")</td>");
                        Response.Write("<td>=sum(AA6:AA" + Convert.ToInt32(Pay_Slip_DT.Rows.Count + 5) + ")</td>");
                        Response.Write("<td>=sum(AB6:AB" + Convert.ToInt32(Pay_Slip_DT.Rows.Count + 5) + ")</td>");
                        Response.Write("<td>=sum(AC6:AC" + Convert.ToInt32(Pay_Slip_DT.Rows.Count + 5) + ")</td>");
                        Response.Write("<td>=sum(AD6:AD" + Convert.ToInt32(Pay_Slip_DT.Rows.Count + 5) + ")</td>");
                        Response.Write("</tr>");
                        Response.Write("</table>");
                        Response.Write("</td>");
                        Response.Write("</tr>");
                        Response.Write("</table>");

                        Response.End();
                        Response.Clear();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Downloaded Successfully');", true);
                        ErrFlag = false;
                    }
                }
                else //staff Salary Payslip
                {
                    if (!ErrFlag)
                    {
                        Pay_Slip_DT.Columns.Add("Sno");
                        Pay_Slip_DT.Columns.Add("EmpNo");
                        Pay_Slip_DT.Columns.Add("Name");
                        Pay_Slip_DT.Columns.Add("Days");
                        Pay_Slip_DT.Columns.Add("Perday");
                        Pay_Slip_DT.Columns.Add("Gross");
                        Pay_Slip_DT.Columns.Add("Adv");
                        Pay_Slip_DT.Columns.Add("Fine");
                        Pay_Slip_DT.Columns.Add("TotalWages");
                        Pay_Slip_DT.Columns.Add("Sign");

                        query = "";
                        query = "Select ED.FirstName,Attn_DT.EmpNo,Attn_DT.Days,Attn_DT.Months,Attn_DT.FinancialYear,Attn_DT.FromDate,Attn_DT.ToDate,";
                        query = query + "Attn_DT.Day1_WH,Attn_DT.Day1_OT,Attn_DT.Day2_WH,Attn_DT.Day2_OT,Attn_DT.Day3_WH,Attn_DT.Day3_OT,Attn_DT.Day4_WH,Attn_DT.Day4_OT,";
                        query = query + "Attn_DT.Day5_WH,Attn_DT.Day5_OT,Attn_DT.Day6_WH,Attn_DT.Day6_OT,Attn_DT.Day7_WH,Attn_DT.Day7_OT,Attn_DT.OTHoursNew,";
                        query = query + "Sal_DT.Fbasic,Sal_DT.BasicandDA,Sal_DT.GrossEarnings,Sal_DT.NetPay,Sal_DT.Advance,Sal_DT.OTHoursAmtNew as Per_hour,Sal_DT.Manual_OT,";
                        query = query + "Sal_DT.Bus_Incentive,Sal_DT.Masthiri_Incentive,Sal_DT.Lunch,Sal_DT.OTFoodAmtNew,Sal_DT.DedOthers1 ";
                        query = query + " from [" + SessionPayroll + "]..AttenanceDetails Attn_DT Inner Join [" + SessionPayroll + "]..SalaryDetails Sal_DT on Attn_DT.EmpNo=Sal_DT.EmpNo";
                        query = query + " Inner Join [VPS_Spay]..Employee_Mst ED on Attn_DT.EmpNo=ED.EmpNo and Sal_DT.EmpNo=ED.EmpNo ";
                        query = query + " where  Attn_DT.Months=Sal_DT.Month and Attn_DT.FinancialYear=Sal_DT.FinancialYear ";
                        query = query + "and Convert(datetime,Attn_DT.FromDate,105)=Convert(datetime,Sal_DT.FromDate,105) and Convert(datetime,Attn_DT.ToDate,105)=Convert(datetime,Sal_DT.ToDate,105) ";
                        query = query + "and Attn_DT.Months='" + ddlMonths.SelectedItem.Text + "' and Attn_DT.FinancialYear='" + ddlFinance.SelectedValue + "' and Attn_DT.Ccode='" + SessionCcode + "' and Attn_DT.Lcode='" + SessionLcode + "' ";
                        query = query + "and Sal_DT.Ccode='" + SessionCcode + "' and Sal_DT.Lcode='" + SessionLcode + "' and convert(datetime,Attn_DT.FromDate,105) = convert(datetime,'" + txtfrom.Text + "',105) and convert(datetime, Attn_DT.ToDate, 105) = convert(datetime, '" + txtTo.Text + "', 105)";

                        if (ddlShiftType.SelectedItem.Text != "-Select-")
                        {
                            query = query + " and ED.ShiftType_New='" + ddlShiftType.SelectedItem.Text + "'";
                        }
                        if (ddlDept.SelectedItem.Text != "-Select-")
                        {
                            query = query + " and ED.DeptName='" + ddlDept.SelectedItem.Text + "'";
                        }
                        if (txtEmployeeType.SelectedItem.Text != "-Select-")
                        {
                            query = query + " and ED.Wages='" + txtEmployeeType.SelectedItem.Text + "'";
                        }
                        query = query + "  and Attn_Dt.Days !='0' order by ED.EmpNo";
                        Atten_Sal_DT = objdata.RptEmployeeMultipleDetails(query);
                        if (Atten_Sal_DT.Rows.Count != 0)
                        {
                            for (int EmpRow = 0; EmpRow < Atten_Sal_DT.Rows.Count; EmpRow++)
                            {
                                Pay_Slip_DT.NewRow();
                                Pay_Slip_DT.Rows.Add();
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["Sno"] = Pay_Slip_DT.Rows.Count;
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["EmpNo"] = Atten_Sal_DT.Rows[EmpRow]["EmpNo"].ToString();
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["Name"] = Atten_Sal_DT.Rows[EmpRow]["FirstName"].ToString();
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["Days"] = Atten_Sal_DT.Rows[EmpRow]["Days"].ToString();
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["Perday"] = Atten_Sal_DT.Rows[EmpRow]["BasicandDA"].ToString();
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["Gross"] = Atten_Sal_DT.Rows[EmpRow]["Fbasic"].ToString();
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["Fine"] = Atten_Sal_DT.Rows[EmpRow]["DedOthers1"].ToString();
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["Adv"] = Atten_Sal_DT.Rows[EmpRow]["Advance"].ToString();
                                Pay_Slip_DT.Rows[Pay_Slip_DT.Rows.Count - 1]["TotalWages"] = Atten_Sal_DT.Rows[EmpRow]["NetPay"].ToString();
                            }
                        }
                        string attachment = "attachment;filename=Payslip.xls";
                        Response.ClearContent();
                        Response.AddHeader("content-disposition", attachment);
                        Response.ContentType = "application/ms-excel";
                        StringWriter stw = new StringWriter();
                        HtmlTextWriter htextw = new HtmlTextWriter(stw);
                        DataTable dt = new DataTable();
                        query = "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                        dt = objdata.RptEmployeeMultipleDetails(query);
                        if (dt.Rows.Count > 0)
                        {
                            CmpName = dt.Rows[0]["Cname"].ToString().ToUpper();
                            Cmpaddress = (dt.Rows[0]["Address1"].ToString().ToUpper() + " - " + dt.Rows[0]["Address2"].ToString().ToUpper());// "-" + dt.Rows[0]["Pincode"].ToString());
                        }
                        Response.Write("<table style='font-weight:bold;'>");
                        Response.Write("<tr align='Center'>");
                        Response.Write("<td colspan='10'>");
                        Response.Write(CmpName.ToString().ToUpper());
                        Response.Write("</td>");
                        Response.Write("</tr>");
                        Response.Write("<tr align='Center'>");
                        Response.Write("<td colspan='10'>");
                        Response.Write("" + Cmpaddress + "");
                        Response.Write("</td>");
                        Response.Write("</tr>");
                        Response.Write("<tr align='Center'>");
                        Response.Write("<td colspan='10'>");
                        Response.Write(txtEmployeeType.SelectedItem.Text + " WAGES FOR " + txtfrom.Text + "  TO  " + txtTo.Text);
                        Response.Write("</td>");
                        Response.Write("</tr>");
                        Response.Write("</table>");
                        Response.Write("<table border='1'>");
                        Response.Write("<tr style='font-weight: bold;' align='center'>");
                        if (Pay_Slip_DT.Rows.Count > 0)
                        {
                            Response.Write("<td>S.NO</td><td>EMP NO</td><td>NAME</td>");
                            DateTime date1;
                            DateTime date2 = new DateTime();
                            date1 = Convert.ToDateTime(txtfrom.Text);
                            date2 = Convert.ToDateTime(txtTo.Text);
                            dayscount = (int)((date2 - date1).TotalDays + 1);
                            daysAdded = 0;

                            int daycount = (int)((toDate - fmDate).TotalDays);
                            daysAdded = 0;
                            Response.Write("<td>");
                            Response.Write("<table border='1'><tr  style='font-weight: bold;' align='center'><td colspan='3' align='center'>BASE WAGES</td></tr><tr  style='font-weight: bold;' align='center'><td>DAYS</td><td>PER DAY</td><td>AMT</td></tr></table>");
                            Response.Write("</td>");

                            Response.Write("<td>");
                            Response.Write("<table border='1'><tr  style='font-weight: bold;' align='center'><td align='center'>ADV</td></tr><tr  style='font-weight: bold;' align='center'><td>AMT</td></tr></table>");
                            Response.Write("</td>");
                            Response.Write("<td>");
                            Response.Write("<table border='1'><tr  style='font-weight: bold;' align='center'><td align='center'>FINE</td></tr><tr  style='font-weight: bold;' align='center'><td>AMT</td></tr></table>");
                            Response.Write("</td>");
                            Response.Write("<td>");
                            Response.Write("<table border='1'><tr  style='font-weight: bold;' align='center'><td align='center'>TOTAL WAGES</td></tr><tr  style='font-weight: bold;' align='center'><td>AMT</td></tr></table>");
                            Response.Write("</td>");
                            Response.Write("<td>SIGN</td>");
                            Response.Write("</tr>");

                            grid.DataSource = Pay_Slip_DT;
                            grid.DataBind();
                            grid.ShowHeader = false;
                            grid.RenderControl(htextw);
                            Response.Write(stw.ToString());

                        }
                        Response.Write("<tr>");
                        Response.Write("<td colspan='28'>");
                        Response.Write("<table border='1'>");
                        Response.Write("<tr style='font-weight: bold;'>");
                        Response.Write("<td colspan='6'   align='center'>");
                        Response.Write("TOTAL");
                        Response.Write("</td>");
                        Response.Write("<td>=sum(G6:G" + Convert.ToInt32(Pay_Slip_DT.Rows.Count + 5) + ")</td>");
                        Response.Write("<td>=sum(H6:H" + Convert.ToInt32(Pay_Slip_DT.Rows.Count + 5) + ")</td>");
                        Response.Write("<td>=sum(I6:I" + Convert.ToInt32(Pay_Slip_DT.Rows.Count + 5) + ")</td>");

                        Response.Write("</tr>");
                        Response.Write("</table>");
                        Response.Write("</td>");
                        Response.Write("</tr>");
                        Response.Write("</table>");

                        Response.End();
                        Response.Clear();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Downloaded Successfully');", true);
                        ErrFlag = false;

                    }
                }
            }

           
            
            
            //For other Wages
            if (!ErrFlag)
            {
                if (ddlcategory.SelectedValue == "STAFF")
                {
                    Stafflabour = "STAFF";
                }
                else if (ddlcategory.SelectedValue == "LABOUR")
                {
                    Stafflabour = "LABOUR";
                }

                string constr = ConfigurationManager.AppSettings["ConnectionString"];
                SqlConnection con = new SqlConnection(constr);

                string Emp_ESI_Code = "";
                string Payslip_Format_Type = "";
                if (SessionUserType == "2")
                {
                    Payslip_Format_Type = rdbPayslipIFFormat.SelectedValue.ToString();
                }
                else
                {
                    Payslip_Format_Type = "";
                }

                string Str_PFType = "";
                if (SessionUserType == "2")
                {
                    Str_PFType = RdpIFPF.SelectedValue.ToString();
                }
                else
                {
                    Str_PFType = RdbPFNonPF.SelectedValue.ToString();
                }

                string Str_ChkLeft = "";
                //Str_ChkLeft = ChkLeft.SelectedValue.ToString();
                Str_ChkLeft = "0";

               
                if (SessionUserType == "2")
                {
                    report_head = "PAYSLIP FOR THE MONTH OF " + ddlMonths.SelectedItem.Text + " " + YR.ToString();
                }
                else
                {
                    report_head = "PAYSLIP FOR THE MONTH OF " + txtfrom.ToString() + " - " + txtTo.ToString();
                }

                Basic_Report_Date = "";
                Basic_Report_Type = "OLD";

                //Get Company Name
                DataTable dt = new DataTable();
                query = "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dt = objdata.RptEmployeeMultipleDetails(query);
                if (dt.Rows.Count > 0)
                {
                    CmpName = dt.Rows[0]["Cname"].ToString();
                    Cmpaddress = (dt.Rows[0]["Address1"].ToString() + ", " + dt.Rows[0]["Address2"].ToString() + ", " + dt.Rows[0]["Location"].ToString() + "-" + dt.Rows[0]["Pincode"].ToString());
                }

                //Check PF Category
                if (Str_PFType.ToString() != "0")
                {
                    if (Str_PFType.ToString() == "1")
                    {
                        if ((txtEmployeeType.SelectedValue == "12") || (txtEmployeeType.SelectedValue == "13") || (txtEmployeeType.SelectedValue == "10") || (txtEmployeeType.SelectedValue == "11") || (txtEmployeeType.SelectedValue == "9") || (txtEmployeeType.SelectedValue == "6") || (txtEmployeeType.SelectedValue == "7") || (txtEmployeeType.SelectedValue == "8")) //Tamil Girls Worker
                        {
                            query = "Select SalDet.EmpNo,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName,MstDpt.DeptName as DepartmentNm," +
                                   " SalDet.LOPDays,(SalDet.FFDA) as Basic_SM,SalDet.BasicAndDANew,SalDet.BasicHRA," +
                                   " SalDet.GrossEarnings,SalDet.ProvidentFund as PF,SalDet.Advance,SalDet.ESI,(SalDet.ProvidentFund + SalDet.ESI + SalDet.advance) as DedPF," +
                                   " ((SalDet.GrossEarnings)- (SalDet.ProvidentFund + SalDet.ESI + SalDet.advance)) as NetpayIF,EmpDet.DeptName as Department,SalDet.WorkedDays " +
                                   " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                                   " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                                   " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                                   " where SalDet.Month='" + ddlMonths.SelectedItem.Text + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.CatName='" + ddlcategory.SelectedItem.Text + "' and " +
                                   " AttnDet.Months='" + ddlMonths.SelectedItem.Text + "' AND AttnDet.FinancialYear='" + ddlFinance.SelectedValue + "' and " +
                                   " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And " +
                                   " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Wages='" + txtEmployeeType.SelectedItem.Text + "'" +
                                   " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + txtfrom.Text + "', 105) Or EmpDet.IsActive='Yes') and EmpDet.Eligible_PF='1' ";

                            query = query + "group by SalDet.EmpNo,EmpDet.ExistingCode,EmpDet.FirstName,MstDpt.DeptName,SalDet.NetPay,SalDet.FFDA," +
                           " EmpDet.DeptName,SalDet.WorkedDays,SalDet.LOPDays,SalDet.Basic_SM,SalDet.BasicAndDANew,SalDet.BasicHRA," +
                           " SalDet.GrossEarnings,SalDet.ProvidentFund,SalDet.Advance,SalDet.ESI " +
                           " Order by EmpDet.ExistingCode Asc";
                        }
                        else if(txtEmployeeType.SelectedValue == "2")
                        {
                            query = "Select SalDet.EmpNo,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName,MstDpt.DeptName as DepartmentNm," +
                                      " SalDet.LOPDays,(SalDet.FFDA) as Basic_SM,SalDet.BasicAndDANew,SalDet.BasicHRA," +
                                      " SalDet.GrossEarnings,SalDet.ProvidentFund as PF,SalDet.Advance,SalDet.ESI,(SalDet.ProvidentFund + SalDet.ESI + SalDet.advance) as DedPF," +
                                      " ((SalDet.GrossEarnings)- (SalDet.ProvidentFund + SalDet.ESI + SalDet.advance)) as NetpayIF,EmpDet.DeptName as Department,SalDet.WorkedDays " +
                                      " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                                      " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                                      " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                                      " where SalDet.Month='" + ddlMonths.SelectedItem.Text + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.CatName='" + ddlcategory.SelectedItem.Text + "' and " +
                                      " AttnDet.Months='" + ddlMonths.SelectedItem.Text + "' AND AttnDet.FinancialYear='" + ddlFinance.SelectedValue + "' and " +
                                      " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And " +
                                      " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Wages='" + txtEmployeeType.SelectedItem.Text + "'" +
                                      " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + txtfrom.Text + "', 105) Or EmpDet.IsActive='Yes') and EmpDet.Eligible_PF='1' ";

                            query = query + "group by SalDet.EmpNo,EmpDet.ExistingCode,EmpDet.FirstName,MstDpt.DeptName,SalDet.NetPay,SalDet.FFDA," +
                           " EmpDet.DeptName,SalDet.WorkedDays,SalDet.LOPDays,SalDet.Basic_SM,SalDet.BasicAndDANew,SalDet.BasicHRA," +
                           " SalDet.GrossEarnings,SalDet.ProvidentFund,SalDet.Advance,SalDet.ESI " +
                           " Order by EmpDet.ExistingCode Asc";
                        }
                    }
                    else
                    {
                        //Non PF Employee
                        query = query + " and (EmpDet.Eligible_PF='2')";
                    }
                }
                else
                {
                    if ((txtEmployeeType.SelectedValue == "12") || (txtEmployeeType.SelectedValue == "13") || (txtEmployeeType.SelectedValue == "10") || (txtEmployeeType.SelectedValue == "11") || (txtEmployeeType.SelectedValue == "9") || (txtEmployeeType.SelectedValue == "6") || (txtEmployeeType.SelectedValue == "7") || (txtEmployeeType.SelectedValue == "8"))
                    {
                        query = "Select SalDet.EmpNo,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName,MstDpt.DeptName as DepartmentNm," +
                               " SalDet.WorkedDays,EmpDet.DeptName as Department,SalDet.OTHoursNew,(SalDet.BasicandDA + SalDet.FDA) as NonPFWages," +
                               " SalDet.OTHoursAmtNew,(SalDet.GrossEarnings + SalDet.OTHoursAmtNew + SalDet.FDA) as GrossEarningsOT,SalDet.ProvidentFund as PF,SalDet.Advance,SalDet.ESI," +
                               " (SalDet.Advance + SalDet.Deduction3 + SalDet.Deduction4 + SalDet.DedOthers1 + SalDet.DedOthers2) as TotDed,SalDet.TotalDeductions," +
                               " SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Basic_SM,SalDet.Advance, " +
                               " ((SalDet.BasicandDA + SalDet.OTHoursAmtNew + SalDet.FDA)-(SalDet.TotalDeductions)) as NetPay " +
                               " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                               " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                               " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                               " where SalDet.Month='" + ddlMonths.SelectedItem.Text + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.CatName='" + ddlcategory.SelectedItem.Text + "' and " +
                               " AttnDet.Months='" + ddlMonths.SelectedItem.Text + "' AND AttnDet.FinancialYear='" + ddlFinance.SelectedValue + "' and " +
                               " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And " +
                               " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Wages='" + txtEmployeeType.SelectedItem.Text + "'" +
                               " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + txtfrom.Text + "', 105) Or EmpDet.IsActive='Yes')";

                        query = query + "group by SalDet.EmpNo,EmpDet.ExistingCode,EmpDet.FirstName,MstDpt.DeptName,SalDet.NetPay,SalDet.TotalDeductions," +
                       " EmpDet.DeptName,SalDet.WorkedDays,SalDet.FDA,SalDet.OTHoursNew,SalDet.OTHoursAmtNew,SalDet.BasicandDA,SalDet.ProvidentFund,SalDet.Advance,SalDet.ESI," +
                       " SalDet.GrossEarnings,SalDet.Advance,SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.NetPay,SalDet.Basic_SM,SalDet.Advance  " +
                       " Order by EmpDet.ExistingCode Asc";
                    }
                    else if (txtEmployeeType.SelectedValue == "2")
                    {
                        query = "Select SalDet.EmpNo,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName,MstDpt.DeptName as DepartmentNm," +
                                  " SalDet.WorkedDays,EmpDet.DeptName as Department,SalDet.OTHoursNew,(SalDet.BasicandDA) as NonPFWages," +
                                  " SalDet.OTHoursAmtNew,(SalDet.BasicandDA + SalDet.OTHoursAmtNew) as GrossEarningsOT,SalDet.ProvidentFund as PF,SalDet.Advance,SalDet.ESI," +
                                  " (SalDet.Advance + SalDet.Deduction3 + SalDet.Deduction4 + SalDet.DedOthers1 + SalDet.DedOthers2) as TotDed,SalDet.TotalDeductions," +
                                  " SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Basic_SM,SalDet.Advance, " +
                                  " ((SalDet.BasicandDA + SalDet.OTHoursAmtNew)-(SalDet.TotalDeductions)) as NetPay,SalDet.New_Cash_Amt,SalDet.New_Bank_Amt,SalDet.New_Tot_Amt, " +
                                  " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                                  " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                                  " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                                  " where SalDet.Month='" + ddlMonths.SelectedItem.Text + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.CatName='" + ddlcategory.SelectedItem.Text + "' and " +
                                  " AttnDet.Months='" + ddlMonths.SelectedItem.Text + "' AND AttnDet.FinancialYear='" + ddlFinance.SelectedValue + "' and " +
                                  " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And " +
                                  " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Wages='" + txtEmployeeType.SelectedItem.Text + "'" +
                                  " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + txtfrom.Text + "', 105) Or EmpDet.IsActive='Yes')";

                        query = query + "group by SalDet.EmpNo,EmpDet.ExistingCode,EmpDet.FirstName,MstDpt.DeptName,SalDet.NetPay,SalDet.TotalDeductions,SalDet.New_Cash_Amt,SalDet.New_Bank_Amt,SalDet.New_Tot_Amt," +
                       " EmpDet.DeptName,SalDet.WorkedDays,SalDet.FDA,SalDet.OTHoursNew,SalDet.OTHoursAmtNew,SalDet.BasicandDA,SalDet.ProvidentFund,SalDet.Advance,SalDet.ESI," +
                       " SalDet.GrossEarnings,SalDet.Advance,SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.NetPay,SalDet.Basic_SM,SalDet.Advance  " +
                       " Order by EmpDet.ExistingCode Asc";
                    }
                }

                DataTable dt_1 = new DataTable();
                dt_1 = objdata.RptEmployeeMultipleDetails(query);

                Decimal NetGrTot = Convert.ToDecimal(NetPay_Grand_Total.ToString());
                NetPay_Grand_Total_Words = NumerictoNumber(Convert.ToInt32(NetGrTot), isUK).ToString() + " " + "Only";

                
                   //Check PF Category
                   if (Str_PFType.ToString() == "1")
                    {
                        //PF Employee List
                        GVHostel.DataSource = dt_1;
                        GVHostel.DataBind();
                    }
                    else
                    {
                        //Non PF Employee List
                        if ((txtEmployeeType.SelectedValue == "12") || (txtEmployeeType.SelectedValue == "13") || (txtEmployeeType.SelectedValue == "10") || (txtEmployeeType.SelectedValue == "11") || (txtEmployeeType.SelectedValue == "9") || (txtEmployeeType.SelectedValue == "8") || (txtEmployeeType.SelectedValue == "7") || (txtEmployeeType.SelectedValue == "6"))
                        {
                            GVCivil.DataSource = dt_1;
                            GVCivil.DataBind();
                        }
                        else if ((txtEmployeeType.SelectedValue == "2"))
                        {
                            GVCivil.DataSource = dt_1;
                            GVCivil.DataBind();
                        }
                    }
                
               

                string attachment = "attachment;filename=Payslip.xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";
                //DataTable dt = new DataTable();
                query = "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dt = objdata.RptEmployeeMultipleDetails(query);
                if (dt.Rows.Count > 0)
                {
                    CmpName = dt.Rows[0]["Cname"].ToString();
                    Cmpaddress = (dt.Rows[0]["Address1"].ToString() + ", " + dt.Rows[0]["Address2"].ToString() + "-" + dt.Rows[0]["Pincode"].ToString());
                }

                NetBase = "0";
                NetFDA = "0";
                NetVDA = "0";
                Nettotal = "0";
                NetPFEarnings = "0";
                NetPF = "0";
                NetESI = "0";
                NetUnion = "0";
                NetAdvance = "0";
                NetAll1 = "0";
                NetAll2 = "0";
                NetAll3 = "0";
                NetAll4 = "0";
                NetDed1 = "0";
                NetDed2 = "0";
                NetDed3 = "0";
                NetDed4 = "0";
                NetAll5 = "0";
                NetDed5 = "0";
                NetLOP = "0";
                NetStamp = "0";
                NetTotalDeduction = "0";
                NetOT = "0";
                NetAmt = "0";
                Network = "0";
                totCL = "0";
                totNFh = "0";
                totweekoff = "0";
                Roundoff = "0";
                Fixedsal = "0";
                FixedOT = "0";
                Tot_OThr = "0";
                //HomeDays = "0";
                //Halfnight = "0";
                //FullNight = "0";
                //Spinning = "0";
                //DayIncentive = "0";
                //ThreeSided = "0";
                //totwork = "0";

                StringWriter stw = new StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);

                //Check PF Category
                    if (Str_PFType.ToString() == "1")
                    {
                        //PF Employee
                        if ((txtEmployeeType.SelectedValue == "2") || (txtEmployeeType.SelectedValue == "12") || (txtEmployeeType.SelectedValue == "13") || (txtEmployeeType.SelectedValue == "10") || (txtEmployeeType.SelectedValue == "11") || (txtEmployeeType.SelectedValue == "9") || (txtEmployeeType.SelectedValue == "6") || (txtEmployeeType.SelectedValue == "8") || (txtEmployeeType.SelectedValue == "7"))
                        {
                            GVHostel.RenderControl(htextw);
                        }
                    }
                    else
                    {
                        //Non PF Employee
                        if ((txtEmployeeType.SelectedValue == "2") || (txtEmployeeType.SelectedValue == "12") || (txtEmployeeType.SelectedValue == "13") || (txtEmployeeType.SelectedValue == "10") || (txtEmployeeType.SelectedValue == "11") || (txtEmployeeType.SelectedValue == "9") || (txtEmployeeType.SelectedValue == "8") || (txtEmployeeType.SelectedValue == "7") || (txtEmployeeType.SelectedValue == "6"))
                        {
                            GVCivil.RenderControl(htextw);
                        }
                    }
                
                Response.Write("<table>");
                Response.Write("<tr align='Center'>");
                Response.Write("<td colspan='19'>");
                Response.Write("" + CmpName + " - PAYSLIP REPORT");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr align='Center'>");
                Response.Write("<td colspan='19'>");
                Response.Write("" + SessionLcode + "");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr align='Center'>");
                Response.Write("<td colspan='19'>");
                Response.Write("" + Cmpaddress + "");
                Response.Write("</td>");
                Response.Write("</tr>");
                string Salary_Head = "";

                if ((txtEmployeeType.SelectedValue == "8") || (txtEmployeeType.SelectedValue == "7") || (txtEmployeeType.SelectedValue == "12") || (txtEmployeeType.SelectedValue == "9") || (txtEmployeeType.SelectedValue == "10") || (txtEmployeeType.SelectedValue == "11"))
                {
                    Salary_Head = "" + txtEmployeeType.SelectedItem.Text + " WORKERS REPORT FOR THE MONTH OF " + txtfrom.Text + " - " + txtTo.Text;
                }
                else if ((txtEmployeeType.SelectedValue == "2") || (txtEmployeeType.SelectedValue == "3") || (txtEmployeeType.SelectedValue == "4") || (txtEmployeeType.SelectedValue == "5"))
                {
                    Salary_Head = "" + txtEmployeeType.SelectedItem.Text + " WORKERS REPORT FOR THE MONTH OF " + txtfrom.Text + " - " + txtTo.Text;
                }

                Response.Write("<tr align='Center'>");
                Response.Write("<td colspan='19'>");
                //Response.Write("Salary Month of " + txtfrom.Text + " - " + txtTo.Text);
                Response.Write(Salary_Head);
                Response.Write("</td>");
                Response.Write("</tr>");
                //}
                Response.Write("</table>");

                Response.Write(stw.ToString());

                //Response.Write("<table border='1'>");
                //Response.Write("<tr Font-Bold='true'>");
                //Response.Write("<td font-Bold='true' align='right' colspan='5'>");
                //Response.Write("Grand Total");
                //Response.Write("</td>");

                Int32 Grand_Tot_End = 0;

                //Check PF Category
               
                    if (Str_PFType.ToString() == "1")
                    {
                        //PF Employee
                        if ((txtEmployeeType.SelectedValue == "2") || (txtEmployeeType.SelectedValue == "12") || (txtEmployeeType.SelectedValue == "13") || (txtEmployeeType.SelectedValue == "10") || (txtEmployeeType.SelectedValue == "11") || (txtEmployeeType.SelectedValue == "9") || (txtEmployeeType.SelectedValue == "6") || (txtEmployeeType.SelectedValue == "7") || (txtEmployeeType.SelectedValue == "8"))
                        {
                            Response.Write("<table border='1'>");
                            Response.Write("<tr Font-Bold='true'>");
                            Response.Write("<td font-Bold='true' align='right' colspan='5'>");
                            Response.Write("Grand Total");
                            Response.Write("</td>");

                            Grand_Tot_End = GVHostel.Rows.Count + 5;
                        }

                        if ((txtEmployeeType.SelectedValue == "2") || (txtEmployeeType.SelectedValue == "12") || (txtEmployeeType.SelectedValue == "13") || (txtEmployeeType.SelectedValue == "10") || (txtEmployeeType.SelectedValue == "11") || (txtEmployeeType.SelectedValue == "9") || (txtEmployeeType.SelectedValue == "6") || (txtEmployeeType.SelectedValue == "7") || (txtEmployeeType.SelectedValue == "8"))
                        {
                            Response.Write("<td>=sum(F6:F" + Grand_Tot_End.ToString() + ")</td>");
                            Response.Write("<td>=sum(G6:G" + Grand_Tot_End.ToString() + ")</td>");
                            Response.Write("<td>=sum(H6:H" + Grand_Tot_End.ToString() + ")</td>");
                            Response.Write("<td>=sum(I6:I" + Grand_Tot_End.ToString() + ")</td>");
                            Response.Write("<td>=sum(J6:J" + Grand_Tot_End.ToString() + ")</td>");
                            Response.Write("<td>=sum(K6:K" + Grand_Tot_End.ToString() + ")</td>");
                            Response.Write("<td>=sum(L6:L" + Grand_Tot_End.ToString() + ")</td>");
                            Response.Write("<td>=sum(M6:M" + Grand_Tot_End.ToString() + ")</td>");
                         
                        }
                    }
                    else
                    {
                        //Non PF Employee
                        if ((txtEmployeeType.SelectedValue == "2") || (txtEmployeeType.SelectedValue == "12") || (txtEmployeeType.SelectedValue == "13") || (txtEmployeeType.SelectedValue == "10") || (txtEmployeeType.SelectedValue == "11") || (txtEmployeeType.SelectedValue == "9") || (txtEmployeeType.SelectedValue == "6") || (txtEmployeeType.SelectedValue == "7") || (txtEmployeeType.SelectedValue == "8"))
                        {
                            Response.Write("<table border='1'>");
                            Response.Write("<tr Font-Bold='true'>");
                            Response.Write("<td font-Bold='true' align='right' colspan='5'>");
                            Response.Write("Grand Total");
                            Response.Write("</td>");

                            Grand_Tot_End = GVCivil.Rows.Count + 5;
                        }



                        if ((txtEmployeeType.SelectedValue == "2") || (txtEmployeeType.SelectedValue == "12") || (txtEmployeeType.SelectedValue == "13") || (txtEmployeeType.SelectedValue == "10") || (txtEmployeeType.SelectedValue == "11") || (txtEmployeeType.SelectedValue == "9") || (txtEmployeeType.SelectedValue == "6") || (txtEmployeeType.SelectedValue == "7") || (txtEmployeeType.SelectedValue == "8"))
                        {
                            Response.Write("<td>=sum(F6:F" + Grand_Tot_End.ToString() + ")</td>");
                            Response.Write("<td>=sum(G6:G" + Grand_Tot_End.ToString() + ")</td>");
                            Response.Write("<td>=sum(H6:H" + Grand_Tot_End.ToString() + ")</td>");
                            Response.Write("<td>=sum(I6:I" + Grand_Tot_End.ToString() + ")</td>");
                            Response.Write("<td>=sum(J6:J" + Grand_Tot_End.ToString() + ")</td>");
                            Response.Write("<td>=sum(K6:K" + Grand_Tot_End.ToString() + ")</td>");
                            Response.Write("<td>=sum(L6:L" + Grand_Tot_End.ToString() + ")</td>");

                            Response.Write("<td>=sum(M6:M" + Grand_Tot_End.ToString() + ")</td>");
                            Response.Write("<td>=sum(N6:N" + Grand_Tot_End.ToString() + ")</td>");
                            Response.Write("<td>=sum(O6:O" + Grand_Tot_End.ToString() + ")</td>");
                            Response.Write("<td>=sum(P6:P" + Grand_Tot_End.ToString() + ")</td>");
                            Response.Write("<td>=sum(Q6:Q" + Grand_Tot_End.ToString() + ")</td>");
                            Response.Write("<td>=sum(R6:R" + Grand_Tot_End.ToString() + ")</td>");
                            Response.Write("<td>=sum(S6:S" + Grand_Tot_End.ToString() + ")</td>");
                            Response.Write("<td>=sum(T6:T" + Grand_Tot_End.ToString() + ")</td>");
                        }
                        //want to put grand total
                    }

                Response.Write("</tr></table>");

                Response.Write("</tr>");
                Response.Write("</table>");

                Response.End();
                Response.Clear();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Downloaded Successfully');", true);
                
            }
          
        }
        catch (Exception ex)
        {
            
            throw;
        }
    }

    private void IF_PayslipFormat()
    {
        throw new NotImplementedException();
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
          server control at run time. */
    }
    public string NumerictoNumber(int Number, bool isUK)
    {
        if (Number == 0)
            return "Zero";

        string and = isUK ? "and " : "";
        if (Number == -2147483648)
            return "Minus Two Billion One Hundred " + and +
        "Forty Seven Million Four Hundred " + and + "Eighty Three Thousand " +
        "Six Hundred " + and + "Forty Eight";

        int[] num = new int[4];
        int first = 0;
        int u, h, t = 0;
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        if (Number < 0)
        {
            sb.Append("Minus");
            Number = -Number;
        }

        string[] words0 = { "", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine" };
        string[] words1 = { "Ten ", "Eleven ", "Twelve ", "Thirteen ", "Fourteen ", "Fifteen ", "Sixteen ", "Seventeen ", "Eighteen ", "Nineteen " };
        string[] words2 = { "Twenty ", "Thirty ", "Forty ", "Fifty ", "Sixty ", "Seventy ", "Eighty ", "Ninety " };
        string[] words3 = { "Thousand ", "Lak", "Crore", "Million ", "Billion " };
        num[0] = Number % 1000;
        num[1] = Number / 1000;
        num[2] = Number / 1000000;
        num[1] = num[1] - 1000 * num[2];  // thousands
        num[2] = num[2] - 100 * num[3];//laks
        num[3] = Number / 10000000;     // billions
        num[2] = num[2] - 1000 * num[3];  // millions

        for (int i = 3; i > 0; i--)
        {
            if (num[i] != 0)
            {
                first = i;
                break;
            }
        }
        for (int i = first; i >= 0; i--)
        {
            if (num[i] == 0) continue;
            u = num[i] % 10;
            t = num[i] / 10;
            h = num[i] / 100;
            t = t - 10 * h;

            if (h > 0)
                sb.Append(words0[h] + " Hundred ");
            if (u > 0 || t > 0)
            {
                if (h > 0 || i < first)
                    sb.Append(and);

                if (t == 0)
                    sb.Append(words0[u]);
                else if (t == 1)
                    sb.Append(words1[u]);
                else
                    sb.Append(words2[t - 2] + words0[u]);

            }

            if (i != 0)
                sb.Append(words3[i - 1]);

        }

        return sb.ToString().TrimEnd();



    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
    }
    protected void gvSalary_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void txtEmployeeType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (txtEmployeeType.SelectedItem.Text == "WEEKLY-HINDI")
        {
            LoadHindiCategory();

        }else 
        DivHinCat.Visible = false;
    }

    private void LoadHindiCategory()
    {
        DivHinCat.Visible = true;
        DataTable HindiCat = new DataTable();
        string SSQL = "";
        SSQL = "";
        SSQL = "Select * from MstHindiCat where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        HindiCat = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlHindiCategory.DataSource = HindiCat;
        DataRow dr = HindiCat.NewRow();
        dr["CatCode"] = "0";
        dr["CatName"] = "-Select-";
        HindiCat.Rows.InsertAt(dr, 0);
        ddlHindiCategory.DataTextField = "CatName";
        ddlHindiCategory.DataValueField = "CatCode";
        ddlHindiCategory.DataBind();
        //ddlHindiCategory.Items.Insert(0, new ListItem("Select", "0"));
    }
    protected void btnSummary_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            string CmpName = "";
            string Cmpaddress = "";
            int YR = 0;
            if (ddlMonths.SelectedItem.Text == "January")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else if (ddlMonths.SelectedItem.Text == "February")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else if (ddlMonths.SelectedItem.Text == "March")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
            }

            if (ddlcategory.SelectedValue == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Category');", true);
                ErrFlag = true;
            }
            else if ((txtEmployeeType.SelectedValue == "") || (txtEmployeeType.SelectedValue == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Employee Type');", true);
                ErrFlag = true;
            }

            else if ((ddlMonths.SelectedValue == "0") || (ddlMonths.SelectedValue == ""))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Months');", true);
                ErrFlag = true;
            }


            if (!ErrFlag)
            {
                //if (ddldept.SelectedValue == "0")
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Department Properly');", true);
                //    ErrFlag = true;
                //}
                if (!ErrFlag)
                {
                    if (ddlcategory.SelectedValue == "STAFF")
                    {
                        Stafflabour = "STAFF";
                    }
                    else if (ddlcategory.SelectedValue == "LABOUR")
                    {
                        Stafflabour = "LABOUR";
                    }
                    string constr = ConfigurationManager.AppSettings["ConnectionString"];
                    SqlConnection con = new SqlConnection(constr);
                    //foreach (GridViewRow gvsal in gvSalary.Rows)
                    //{
                    //    Label lblEmpNo = (Label)gvsal.FindControl("lblExist");
                    //    string qry = "Update SalaryDetails set Process_Mode='" + 1 + "' where Month='" + ddlMonths.SelectedValue + "' and FinancialYear='" + ddlFinance.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and ExisistingCode='" + lblEmpNo.Text + "'";
                    //    SqlCommand cmd = new SqlCommand(qry, con);
                    //    con.Open();
                    //    cmd.ExecuteNonQuery();
                    //    con.Close();
                    //}
                    string Emp_ESI_Code = "";//ddESI.SelectedValue.ToString();

                  

                    string Str_PFType = RdbPFNonPF.SelectedValue.ToString();
                
                    string SalaryType = "";
                    ResponseHelper.Redirect("ViewReport_Opt.aspx?Cate=" + Stafflabour + "&Months=" + ddlMonths.SelectedItem.Text + "&yr=" + ddlFinance.SelectedValue + "&fromdate=" + txtfrom.Text + "&ToDate=" + txtTo.Text + "&Salary=" + SalaryType + "&EmpTypeCd=" + txtEmployeeType.SelectedValue.ToString() + "&EmpType=" + txtEmployeeType.SelectedItem.Text.ToString() + "&Report_Type=DeptSummary", "_blank", "");
                }
            }
        }
        catch (Exception ex)
        {
        }
    }
}
