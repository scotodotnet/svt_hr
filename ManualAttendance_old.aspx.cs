﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using Payroll;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Security.Cryptography;
using System.IO;


public partial class ManualAttendance : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SSQL = "";
    bool isPresent = false;
    string SessionUserType;
    System.Web.UI.WebControls.DataGrid grid =
                             new System.Web.UI.WebControls.DataGrid();

    protected void Page_Load(object sender, EventArgs e)
    {
        
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        
        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Manual Attendance";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");
           
            Load_Shift();
            Load_Data_EmpDet();
            Load_Department();
            Load_Designation();
        }
        
    }

    private void Load_Shift()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlShiftType.Items.Clear();
        query = "Select *from Shift_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlShiftType.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["ShiftDesc"] = "-Select-";
        dr["ShiftDesc"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlShiftType.DataTextField = "ShiftDesc";
        ddlShiftType.DataValueField = "ShiftDesc";
        ddlShiftType.DataBind();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
         bool ErrFlag = false;
         DataTable DT=new DataTable();
         string[] Time_IN_Count_Check;
         string[] Time_OUT_Count_Check;
         string MachineID;
         string In_Machine_IP="";
         string Out_Machine_IP="";
         DateTime ShiftDate = new DateTime();
         
         DataTable DT_Shift = new DataTable();

         DataTable DT_Date = new DataTable();
         string Query = "Select convert(varchar,getdate(),105) as ServerDate";
         DT_Date = objdata.RptEmployeeMultipleDetails(Query);
         if (DT_Date.Rows.Count != 0)
         {
            string dtserver = DT_Date.Rows[0]["ServerDate"].ToString();
            if (Convert.ToDateTime(ShiftDate) > Convert.ToDateTime(dtserver))
             {
                 ErrFlag = true;
                 ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check with Date..!');", true);
             }
         }
         if (RdbShiftWise.SelectedValue == "2")
         {

         }
         else
         {
             if (ddlShiftType.SelectedItem.Text != "-Select-")
             {
                 ShiftDate = Convert.ToDateTime(txtShiftDate.Text);
                 SSQL = "Select *from  Shift_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                 SSQL = SSQL + " And ShiftDesc='" + ddlShiftType.SelectedItem.Text + "'";
                 DT_Shift = objdata.RptEmployeeMultipleDetails(SSQL);

                 if (DT_Shift.Rows.Count != 0)
                 {
                     txtINDate.Text = ShiftDate.ToString("dd/MM/yyyy");
                     txtTimeIN.Text = DT_Shift.Rows[0]["StartTime"].ToString();
                     if (DT_Shift.Rows[0]["EndOUT_Days"].ToString() == "1")
                     {
                         txtOUTDate.Text = ShiftDate.AddDays(1).ToString("dd/MM/yyyy");
                     }
                     else
                     {
                         txtOUTDate.Text = ShiftDate.ToString("dd/MM/yyyy");
                     }
                     txtTimeOUT.Text = DT_Shift.Rows[0]["EndTime"].ToString();
                 }
             }
         }
        Time_IN_Count_Check = txtTimeIN.Text.Split(':');
        Time_OUT_Count_Check = txtTimeOUT.Text.Split(':');

        if (Time_IN_Count_Check.Length != 2)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Time IN Invalid...!');", true);
        }

        if (Time_OUT_Count_Check.Length != 2)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Time OUT Invalid...!');", true);
        }

        DataTable dt_Admin = new DataTable();
        SSQL = "Select Salary,Manual,Medical,GetDate() as CurrDate from MstAdminRights where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " And UserCode='" + SessionUserType + "'";
        dt_Admin = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt_Admin.Rows.Count != 0)
        {
            if (dt_Admin.Rows[0]["Manual"].ToString() == "Yes")
            {
                DateTime MnlAttn_Days = Convert.ToDateTime(dt_Admin.Rows[0]["CurrDate"].ToString()).AddDays(-3);
                DateTime MnlAttn_Date = Convert.ToDateTime(txtINDate.Text);
                if (Convert.ToDateTime(MnlAttn_Days) > Convert.ToDateTime(MnlAttn_Date))
                {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You dont have Rights to Select Current date to Before 4 Days!');", true);
                }
            }
        }

        if (!ErrFlag)
        {
            DateTime dtin = Convert.ToDateTime(txtINDate.Text);
            string login = dtin.ToString("dd/MM/yyyy");
            
            DateTime dtout = Convert.ToDateTime(txtOUTDate.Text);
            string logout = dtout.ToString("dd/MM/yyyy");
            //string SSQL; 

            string logtimein = login + " " + txtTimeIN.Text;
            string logtimeout = logout + " " + txtTimeOUT.Text;

            DateTime TimeIN = Convert.ToDateTime(logtimein);
            DateTime TimeOUT = Convert.ToDateTime(logtimeout);

            if (ddlAttndType.SelectedItem.Text == "H") // Adding 4 hours to TimeIN for Half day 
            {
                TimeOUT = Convert.ToDateTime(logtimein).AddHours(4);
            }
            else if (ddlAttndType.SelectedItem.Text == "A") // Adding 2 hours to TimeIN for Absent
            {
                TimeOUT = Convert.ToDateTime(logtimein).AddHours(2);
            }

            MachineID = UTF8Encryption(txtMachineID.Value);

            DataTable mDataSet=new DataTable();
           
            //Get IP Address
            SSQL = "";
            SSQL = "Select * from IPAddress_Mst where CompCode = '" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
            mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);
            for(int i =0;i<mDataSet.Rows.Count;i++)
            {
                if(mDataSet.Rows[i]["IPMode"].ToString().ToUpper() == ("IN"))
                In_Machine_IP = mDataSet.Rows[i]["IPAddress"].ToString();

                if(mDataSet.Rows[i]["IPMode"].ToString().ToUpper() == ("OUT"))
                Out_Machine_IP = mDataSet.Rows[i]["IPAddress"].ToString();

            }

             //Delete Improper Punches
            SSQL = "Delete from LogTime_IN where MachineID='" + MachineID + "'";
            SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And TimeIN >='" + TimeIN.AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00" + "'";
            SSQL = SSQL + " And TimeIN <='" + TimeIN.AddDays(1).ToString("yyyy/MM/dd") + " " + "02:00" + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "Delete from LogTime_OUT where MachineID='" + MachineID + "'";
            SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And TimeOUT >='" + TimeIN.AddDays(0).ToString("yyyy/MM/dd") + " " + "08:00" + "'";
            SSQL = SSQL + " And TimeOUT <='" + TimeIN.AddDays(1).ToString("yyyy/MM/dd") + " " + "08:00" + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "Delete from ManAttn_Details where Machine_No='" + txtMachineID.Value + "' and AttnDate = '" + txtINDate.Text + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);
            
           

            //Insert logtime in and logtime out
            //Insert Manual Attn. Record
            SSQL = "Insert Into ManAttn_Details(CompCode,LocCode,EmpNo,ExistingCode,Machine_No,EmpName,AttnStatus,AttnDate,";
            SSQL = SSQL + "AttnDateStr,TimeIn,TimeOut,LogTimeIn,LogTimeOut,Enter_Date,UserName,ShiftWise,Shift,OTHrs) Values(";
            SSQL = SSQL + "'" + SessionCcode + "','" + SessionLcode + "','" + txtMachineID.Value + "','" + txtExistingCode.SelectedValue + "',";
            SSQL = SSQL + "'" + txtMachineID.Value + "','" + txtEmpName.Text + "',";
            SSQL = SSQL + "'" + ddlAttndType.SelectedValue + "','" + txtINDate.Text + "','" + txtINDate.Text + "','" + txtTimeIN.Text + "','" + txtTimeOUT.Text + "',";
            SSQL = SSQL + "'" + Convert.ToDateTime(TimeIN).ToString("yyyy/MM/dd HH:mm") + "','" + Convert.ToDateTime(TimeOUT).ToString("yyyy/MM/dd HH:mm") + "',GetDate(),'" + SessionUserName + "','" + RdbShiftWise.SelectedValue + "','" + ddlShiftType.SelectedItem.Text + "','" + txtOTHrs.Text + "')";
            objdata.RptEmployeeMultipleDetails(SSQL);

            //Insert LogTime_IN
            SSQL = "Insert Into LogTime_IN(CompCode,LocCode,IPAddress,MachineID,TimeIN) Values(";
            SSQL = SSQL + "'" + SessionCcode + "','" + SessionLcode + "','" + In_Machine_IP + "','" + MachineID + "','" + Convert.ToDateTime(TimeIN).ToString("yyyy/MM/dd HH:mm") + "')";
            objdata.RptEmployeeMultipleDetails(SSQL);
            
            //Insert LogTime_OUT
            SSQL = "Insert Into LogTime_OUT(CompCode,LocCode,IPAddress,MachineID,TimeOUT) Values(";
            SSQL = SSQL + "'" + SessionCcode + "','" + SessionLcode + "','" + Out_Machine_IP + "','" + MachineID + "','" + Convert.ToDateTime(TimeOUT).ToString("yyyy/MM/dd HH:mm") + "')";
            objdata.RptEmployeeMultipleDetails(SSQL);

            Get_Working_Days_Save_DB(txtMachineID.Value, txtINDate.Text, txtINDate.Text);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Manual Attendance Saved Successfully...!');", true);
            Clear_All_Field();
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    private void Clear_All_Field()
    {
        txtMachineID.Value = "";
        txtEmpName.Text = "";
        txtDepartment.Text = "";
        txtExistingCode.SelectedValue = "-Select-";
        RdbShiftWise.SelectedValue = "1";
        if (RdbShiftWise.SelectedValue == "2")
        {
            div_ShiftType.Visible = false;
            div_ShiftDate.Visible = false;
            Panel1.Visible = true;
        }
        else
        {
            div_ShiftType.Visible = true;
            div_ShiftDate.Visible = true;
            Panel1.Visible = false;
        }
        ddlShiftType.SelectedValue = "-Select-";
        txtShiftDate.Text = "";
        txtOTHrs.Text = "";
        txtTimeIN.Text = ""; txtTimeOUT.Text = ""; txtINDate.Text = "";
        txtOUTDate.Text = "";
        ddlAttndType.SelectedValue = "-Select-";

        btnSave.Text = "Save";
    }

    protected void GridEditEnquiryClick(object sender, CommandEventArgs e)
    {
        string query;
        DataTable DT = new DataTable();

        query = "Select * from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        query = query + " And EmpNo='" + e.CommandName.ToString() + "'"; 
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            txtExistingCode.SelectedValue = DT.Rows[0]["ExistingCode"].ToString();
            txtEmpName.Text = DT.Rows[0]["FirstName"].ToString();
            txtDepartment.Text = DT.Rows[0]["DeptName"].ToString();
            txtMachineID.Value = DT.Rows[0]["MachineID"].ToString();
        }
        else
        {
            txtExistingCode.SelectedValue = "-Select-";
            txtEmpName.Text = "";
            txtDepartment.Text = "";
            txtMachineID.Value = "";
        }
    }
    protected void chkapproval_CheckedChanged(object sender, EventArgs e)
    {
        if (chkapproval.Checked == true)
        {
            chkPending.Checked = false;
        }
        Load_Data_EmpDet();
    }
    protected void chkPending_CheckedChanged(object sender, EventArgs e)
    {
        if (chkPending.Checked == true)
        {
            chkapproval.Checked = false;
        }
        Load_Data_EmpDet();
    }

    private void Load_Data_EmpDet()
    {

        string TableName;
        if (chkapproval.Checked == true)
        {
            TableName = "Employee_Mst";
        }
        else
        {
            TableName = "Employee_Mst_New_Emp";
        }
        
        string query = "";
        DataTable DT = new DataTable();

        //Repeater1.DataSource = DT;
        //Repeater1.DataBind();

        txtExistingCode.Items.Clear();
        query = "Select ExistingCode from " + TableName + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        query = query + " And IsActive='Yes'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        txtExistingCode.DataSource = DT;
        DataRow dr = DT.NewRow();
        dr["ExistingCode"] = "-Select-";
        dr["ExistingCode"] = "-Select-";
        DT.Rows.InsertAt(dr, 0);
        txtExistingCode.DataTextField = "ExistingCode";
        txtExistingCode.DataValueField = "ExistingCode";
        txtExistingCode.DataBind();
    }
    protected void txtExistingCode_SelectedIndexChanged(object sender, EventArgs e)
    {
        string query;
        DataTable DT = new DataTable();
        string TableName;
        if (chkapproval.Checked == true)
        {
            TableName = "Employee_Mst";
        }
        else
        {
            TableName = "Employee_Mst_New_Emp";
        }
        if (txtExistingCode.SelectedItem.Text != "-Select-")
        {
            query = "Select * from " + TableName + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            query = query + " And ExistingCode='" + txtExistingCode.SelectedItem.Text + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);

            if (DT.Rows.Count != 0)
            {

                txtMachineID.Value = DT.Rows[0]["EmpNo"].ToString();
                txtEmpName.Text = DT.Rows[0]["FirstName"].ToString();
                txtDepartment.Text = DT.Rows[0]["DeptName"].ToString();
            }
            else
            {
                txtExistingCode.SelectedValue = "-Select-";
                txtEmpName.Text = "";
                txtDepartment.Text = "";
                txtMachineID.Value = "";
            }
        }
        else
        {
            txtExistingCode.SelectedValue = "-Select-";
            txtEmpName.Text = "";
            txtDepartment.Text = "";
            txtMachineID.Value = "";
        }
    }

    private static string UTF8Encryption_OLD(string password)
    {
        string strmsg = string.Empty;
        byte[] encode = new byte[password.Length];
        encode = Encoding.UTF8.GetBytes(password);
        strmsg = Convert.ToBase64String(encode);
        return strmsg;
    }

    public string UTF8Encryption(string mvarPlanText)
    {
        string cipherText = "";
        try
        {
            string passPhrase = "Altius";
            string saltValue = "info@altius.co.in";
            string hashAlgorithm = "SHA1";
            string initVector = "@1B2c3D4e5F6g7H8";
            int passwordIterations = 2;
            int keySize = 256;
            byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
            byte[] saltValueBytes = Encoding.ASCII.GetBytes(saltValue);
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(mvarPlanText);
            PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, saltValueBytes, hashAlgorithm, passwordIterations);
            byte[] keyBytes = password.GetBytes(keySize / 8);
            RijndaelManaged symmetricKey = new RijndaelManaged();
            symmetricKey.Mode = CipherMode.CBC;
            ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes);
            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
            cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
            cryptoStream.FlushFinalBlock();
            byte[] cipherTextBytes = memoryStream.ToArray();
            memoryStream.Close();
            cryptoStream.Close();
            cipherText = Convert.ToBase64String(cipherTextBytes);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }

        return cipherText;
    }

    private static string UTF8Decryption(string encryptpwd)
    {
        string decryptpwd = string.Empty;
        UTF8Encoding encodepwd = new UTF8Encoding();
        Decoder Decode = encodepwd.GetDecoder();
        byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
        int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
        char[] decoded_char = new char[charCount];
        Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
        decryptpwd = new String(decoded_char);
        return decryptpwd;
    }
   
    public void Get_Working_Days_Save_DB(string Get_Machine_ID, string Get_Punching_Date1, string Get_Punching_Date2)
    {
        DateTime Date1 = Convert.ToDateTime(Get_Punching_Date1);
        DateTime Date2 = Convert.ToDateTime(Get_Punching_Date2);

        int daycount = (int)((Date2 - Date1).TotalDays);
        int daysAdded = daycount + 1;

        int intI;
        int intK;
        int intCol;
        string Fin_Year = "";
        string Months_Full_Str = "";
        string Date_Col_Str = "";
        int Month_Int = 1;
        string Spin_Machine_ID_Str = "";

        string[] Time_Minus_Value_Check;

        DataTable mLocalDS = new DataTable();

        DataTable mLocalDS1 = new DataTable();

        //Attn_Flex Col Add Var
        string[] Att_Date_Check;
        string Att_Already_Date_Check;
        string Att_Year_Check = "";
        int Month_Name_Change;
        string halfPresent = "0";
        int EPay_Total_Days_Get = 0;
        intCol = 4;
        Month_Name_Change = 1;

        EPay_Total_Days_Get = 1;

        intI = 2;
        intK = 1;

        bool ErrFlag = false;


        DataTable Emp_DS = new DataTable();

        SSQL = "Select * from Employee_Mst where CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " And MachineID='" + Get_Machine_ID + "'";
        Emp_DS = objdata.RptEmployeeMultipleDetails(SSQL);

        if (Emp_DS.Rows.Count == 0)
        {
            SSQL = "Select * from Employee_Mst_New_Emp where CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And MachineID='" + Get_Machine_ID + "'";
            Emp_DS = objdata.RptEmployeeMultipleDetails(SSQL);

            if (Emp_DS.Rows.Count == 0)
            {

            }
        }

        if (Emp_DS.Rows.Count > 0)
        {
            ErrFlag = false;
        }
        else
        {
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            for (int intRow = 0; intRow < Emp_DS.Rows.Count; intRow++)
            {
                intK = 1;

                string Emp_Total_Work_Time_1 = "00:00";
                //Get Employee Week OF DAY
                DataTable DS_WH = new DataTable();
                string Emp_WH_Day;
                string DOJ_Date_Str;
                string MachineID1 = Emp_DS.Rows[intRow]["MachineID"].ToString();

                string Query = " Select * from Employee_Mst where MachineID='" + MachineID1 + "' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' ";
                DS_WH = objdata.RptEmployeeMultipleDetails(Query);
                if (DS_WH.Rows.Count != 0)
                {
                    Emp_WH_Day = DS_WH.Rows[0]["WeekOff"].ToString();
                    DOJ_Date_Str = DS_WH.Rows[0]["DOJ"].ToString();
                }
                else
                {
                    Emp_WH_Day = "";
                    DOJ_Date_Str = "";
                }

                int colIndex = intK;
                string DayofWeekName = "";
                decimal Weekoff_Count = 0;
                isPresent = false;
                bool isWHPresent = false;
                decimal Present_Count = 0;
                decimal Present_WH_Count = 0;
                int Appsent_Count = 0;
                decimal Final_Count = 0;
                decimal Total_Days_Count = 0;
                string Already_Date_Check;
                int Days_Insert_RowVal = 0;
                decimal FullNight_Shift_Count = 0;
                int NFH_Days_Count = 0;
                decimal NFH_Days_Present_Count = 0;
                Already_Date_Check = "";
                string Year_Check = "";
                string Time_IN_Str = "";
                string Time_Out_Str = "";
                int g = 1;

                double OT_Time;

                OT_Time = 0;

                string INTime = "";
                string OUTTime = "";

                for (intCol = 0; (intCol <= (daysAdded - 1)); intCol++)
                {

                    Emp_Total_Work_Time_1 = "00:00";
                    string Machine_ID_Str = "";
                    string OT_Week_OFF_Machine_No;
                    string Date_Value_Str = "";
                    string Total_Time_get = "";
                    string Final_OT_Work_Time_1 = "";
                    //DataSet mLocalDS = new DataSet();

                    //DataTable mLocalDS = new DataTable();

                    decimal Month_WH_Count = 0;
                    int j = 0;
                    double time_Check_dbl = 0;
                    string Date_Value_Str1 = "";
                    string Employee_Shift_Name_DB = "No Shift";
                    isPresent = false;
                    isWHPresent = false;

                    //Shift Change Check Variable Declaration Start
                    DateTime InTime_Check = new DateTime();
                    DateTime InToTime_Check = new DateTime();
                    TimeSpan InTime_TimeSpan;
                    string From_Time_Str = "";
                    string To_Time_Str = "";
                    //DataSet DS_InTime = new DataSet();
                    DataTable DS_Time = new DataTable();
                    //DataSet DS_Time = new DataSet();
                    DataTable DS_InTime = new DataTable();
                    DateTime EmpdateIN_Change = new DateTime();
                    DataTable InsertData = new DataTable();
                    string Final_InTime = "";
                    string Final_OutTime = "";
                    string Final_Shift = "";

                    int K = 0;
                    bool Shift_Check_blb = false;
                    string Shift_Start_Time_Change;
                    string Shift_End_Time_Change;
                    string Employee_Time_Change = "";
                    DateTime ShiftdateStartIN_Change = new DateTime();
                    DateTime ShiftdateEndIN_Change = new DateTime();

                    //Shift Change Check Variable Declaration End
                    string Employee_Punch = "";
                    Spin_Machine_ID_Str = Emp_DS.Rows[intRow]["MachineID"].ToString();

                    if (Spin_Machine_ID_Str == "9512")
                    {
                        Spin_Machine_ID_Str = "9512";
                    }

                    Machine_ID_Str = UTF8Encryption(Emp_DS.Rows[intRow]["MachineID"].ToString());
                    //Machine_ID_Str = Emp_DS.Rows[intRow]["MachineID_Encrypt"].ToString();
                    OT_Week_OFF_Machine_No = Emp_DS.Rows[intRow]["MachineID"].ToString();
                    Date_Value_Str = Convert.ToDateTime(Get_Punching_Date1).AddDays(intCol).ToShortDateString();
                    Date_Value_Str1 = Convert.ToDateTime(Date_Value_Str).AddDays(1).ToShortDateString();

                    string Out_Punch_Date = Convert.ToDateTime(Get_Punching_Date1).AddDays(-1).ToShortDateString();
                    //TimeIN Get

                    SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                    SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    SSQL = SSQL + " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";
                    mLocalDS = objdata.RptEmployeeMultipleDetails(SSQL);



                    //TimeOUT Get
                    SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                    SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    SSQL = SSQL + " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT Asc";
                    mLocalDS1 = objdata.RptEmployeeMultipleDetails(SSQL);

                    if (mLocalDS.Rows.Count == 0 && mLocalDS1.Rows.Count == 0)
                    {
                        //Here Out Punch Check
                        SSQL = OutPunch_Get(Machine_ID_Str, Date_Value_Str, Date_Value_Str1, Out_Punch_Date);
                        mLocalDS1 = objdata.RptEmployeeMultipleDetails(SSQL);
                    }

                    //Shift Change Check Start
                    if (mLocalDS.Rows.Count != 0)
                    {
                        InTime_Check = Convert.ToDateTime(mLocalDS.Rows[0]["TimeIN"].ToString());
                        InToTime_Check = InTime_Check.AddHours(2);

                        InTime_Check = Convert.ToDateTime(string.Format(Convert.ToString(InTime_Check), "HH:mm:ss"));
                        InToTime_Check = Convert.ToDateTime(string.Format(Convert.ToString(InToTime_Check), "HH:mm:ss"));

                        //Old Time check error for 24 format

                        string TimeIN = InTime_Check.ToString();
                        string[] TimeIN_Split = TimeIN.Split(' ');
                        string TimeIN1 = TimeIN_Split[1].ToString();
                        string[] final = TimeIN1.Split(':');
                        string Final_IN = "";
                        if (Convert.ToInt32(InTime_Check.Hour) >= 10)
                        {
                            if (Convert.ToInt32(InTime_Check.Minute) >= 10)
                            {
                                Final_IN = InTime_Check.Hour + ":" + InTime_Check.Minute;
                            }
                            else
                            {
                                Final_IN = InTime_Check.Hour + ":0" + InTime_Check.Minute;
                            }
                        }
                        else
                        {
                            if (Convert.ToInt32(InTime_Check.Minute) >= 10)
                            {
                                Final_IN = "0" + InTime_Check.Hour + ":" + InTime_Check.Minute;
                            }
                            else
                            {
                                Final_IN = "0" + InTime_Check.Hour + ":0" + InTime_Check.Minute;
                            }
                        }


                        string TimeOUT = InToTime_Check.ToString();
                        string[] TimeOUT_Split = TimeOUT.Split(' ');
                        string TimeOUT1 = TimeOUT_Split[1].ToString();
                        string[] final1 = TimeOUT1.Split(':');
                        string Final_IN1 = "";
                        if (Convert.ToInt32(InToTime_Check.Hour) >= 10)
                        {
                            if (Convert.ToInt32(InToTime_Check.Minute) >= 10)
                            {
                                Final_IN1 = InToTime_Check.Hour + ":" + InToTime_Check.Minute;
                            }
                            else
                            {
                                Final_IN1 = InToTime_Check.Hour + ":0" + InToTime_Check.Minute;
                            }
                        }
                        else
                        {
                            if (Convert.ToInt32(InToTime_Check.Minute) >= 10)
                            {
                                Final_IN1 = "0" + InToTime_Check.Hour + ":" + InToTime_Check.Minute;
                            }
                            else
                            {
                                Final_IN1 = "0" + InToTime_Check.Hour + ":0" + InToTime_Check.Minute;
                            }
                        }

                        //Two Hours OutTime Check
                        SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                                " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN + "' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN1 + "' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Asc";
                        DS_Time = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (DS_Time.Rows.Count != 0)
                        {
                            //After Out In Time Check
                            DateTime OUT_Winth_InTime_Check = Convert.ToDateTime(DS_Time.Rows[0]["TimeOUT"].ToString());
                            if (Convert.ToInt32(OUT_Winth_InTime_Check.Hour) >= 10)
                            {
                                if (Convert.ToInt32(OUT_Winth_InTime_Check.Minute) >= 10)
                                {
                                    Final_IN1 = OUT_Winth_InTime_Check.Hour + ":" + OUT_Winth_InTime_Check.Minute;
                                }
                                else
                                {
                                    Final_IN1 = OUT_Winth_InTime_Check.Hour + ":0" + OUT_Winth_InTime_Check.Minute;
                                }
                            }
                            else
                            {
                                if (Convert.ToInt32(OUT_Winth_InTime_Check.Minute) >= 10)
                                {
                                    Final_IN1 = "0" + OUT_Winth_InTime_Check.Hour + ":" + OUT_Winth_InTime_Check.Minute;
                                }
                                else
                                {
                                    Final_IN1 = "0" + OUT_Winth_InTime_Check.Hour + ":0" + OUT_Winth_InTime_Check.Minute;
                                }
                            }
                            SSQL = "Select TimeIN from LogTime_IN where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                            " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN1 + "' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";
                            DS_InTime = objdata.RptEmployeeMultipleDetails(SSQL);
                            if (DS_InTime.Rows.Count != 0)
                            {
                                //DataSet Shift_DS_Change = new DataSet();

                                DataTable Shift_DS_Change = new DataTable();

                                Final_InTime = DS_InTime.Rows[0]["TimeIN"].ToString();
                                //Check With IN Time Shift
                                SSQL = "Select * from Shift_Mst Where ShiftDesc like '%SHIFT%'";
                                SSQL = SSQL + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                Shift_DS_Change = objdata.RptEmployeeMultipleDetails(SSQL);
                                Shift_Check_blb = false;
                                for (int k = 0; k < Shift_DS_Change.Rows.Count; k++)
                                {
                                    Shift_Start_Time_Change = Date_Value_Str + " " + Shift_DS_Change.Rows[k]["StartIN"].ToString();
                                    if (Shift_DS_Change.Rows[k]["EndIN_Days"].ToString() == "1")
                                    {
                                        Shift_End_Time_Change = Date_Value_Str1 + " " + Shift_DS_Change.Rows[k]["EndIN"].ToString();
                                    }
                                    else
                                    {
                                        Shift_End_Time_Change = Date_Value_Str + " " + Shift_DS_Change.Rows[k]["EndIN"].ToString();
                                    }

                                    ShiftdateStartIN_Change = System.Convert.ToDateTime(Shift_Start_Time_Change);
                                    ShiftdateEndIN_Change = System.Convert.ToDateTime(Shift_End_Time_Change);

                                    if (EmpdateIN_Change >= ShiftdateStartIN_Change && EmpdateIN_Change <= ShiftdateEndIN_Change)
                                    {
                                        Final_Shift = Shift_DS_Change.Rows[k]["ShiftDesc"].ToString();
                                        Shift_Check_blb = true;
                                        break;
                                    }
                                    ShiftdateStartIN_Change = System.Convert.ToDateTime(Shift_Start_Time_Change);
                                    ShiftdateEndIN_Change = System.Convert.ToDateTime(Shift_End_Time_Change);
                                    EmpdateIN_Change = System.Convert.ToDateTime(Final_InTime);
                                    if (EmpdateIN_Change >= ShiftdateStartIN_Change && EmpdateIN_Change <= ShiftdateEndIN_Change)
                                    {
                                        Final_Shift = Shift_DS_Change.Rows[k]["ShiftDesc"].ToString();
                                        Shift_Check_blb = true;
                                        break;
                                    }
                                }
                                if (Shift_Check_blb == true)
                                {

                                    //IN Time Query Update
                                    string Querys = "";
                                    Querys = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'" +
                                    " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN1 + "' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";
                                    mLocalDS = objdata.RptEmployeeMultipleDetails(Querys);

                                    if (Final_Shift.ToUpper() == "SHIFT2")
                                    {
                                        Querys = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'" +
                                        " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "13:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "03:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Asc";
                                    }
                                    else
                                    {
                                        Querys = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'" +
                                                " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "17:40' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Asc";

                                    }
                                    mLocalDS1 = objdata.RptEmployeeMultipleDetails(Querys);
                                }
                                else
                                {
                                    //Get Employee In Time
                                    SSQL = "Select TimeIN from LogTime_IN where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                                    " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN1 + "' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";
                                    mLocalDS = objdata.RptEmployeeMultipleDetails(SSQL);
                                    //TimeOUT Get
                                    SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'" +
                                           " And TimeOUT >'" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN1 + "' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Asc";
                                    mLocalDS1 = objdata.RptEmployeeMultipleDetails(SSQL);
                                }
                            }
                            else
                            {
                                //Get Employee In Time
                                SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'" +
                                       " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";
                                mLocalDS = objdata.RptEmployeeMultipleDetails(SSQL);

                                //TimeOUT Get
                                //SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'" +
                                //      " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Asc";
                                SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                                " And TimeOUT >'" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN1 + "' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "23:55" + "' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Asc";

                                mLocalDS1 = objdata.RptEmployeeMultipleDetails(SSQL);
                                if (mLocalDS1.Rows.Count == 0)
                                {
                                    SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                                    " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN + "' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN1 + "' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Asc";

                                    mLocalDS1 = objdata.RptEmployeeMultipleDetails(SSQL);
                                }

                            }
                        }
                        else
                        {
                            //Get Employee In Time
                            SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'" +
                                   " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";
                            mLocalDS = objdata.RptEmployeeMultipleDetails(SSQL);

                            //TimeOUT Get
                            SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'" +
                                  " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Asc";
                            mLocalDS1 = objdata.RptEmployeeMultipleDetails(SSQL);
                        }
                    }
                    //Shift Change Check End 


                    if (mLocalDS.Rows.Count <= 0)
                    {
                        Time_IN_Str = "";
                        INTime = "";
                    }
                    else
                    {
                        DateTime datetime1 = Convert.ToDateTime(mLocalDS.Rows[0][0].ToString());
                        INTime = datetime1.ToString("hh:mm tt");
                        //INTime = string.Format("{0:hh:mm tt}", mLocalDS.Rows[0][0].ToString());
                    }

                    if (mLocalDS1.Rows.Count <= 0)
                    {
                        Time_Out_Str = "";
                        OUTTime = "";
                    }
                    else
                    {
                        DateTime datetime = Convert.ToDateTime(mLocalDS1.Rows[0][0].ToString());
                        OUTTime = datetime.ToString("hh:mm tt");
                        //OUTTime = string.Format(Convert.ToString(datetime), "HH:mm:ss tt");
                        //OUTTime = string.Format("{0:hh:mm tt}", mLocalDS1.Rows[0][0].ToString());

                    }

                    if (mLocalDS.Rows.Count == 0)
                    {
                        Employee_Punch = "";
                    }
                    else
                    {
                        Employee_Punch = mLocalDS.Rows[0][0].ToString();
                    }
                    if (mLocalDS.Rows.Count > 1)
                    {
                        for (int tin = 0; tin < mLocalDS.Rows.Count; tin++)
                        {
                            Time_IN_Str = mLocalDS.Rows[tin][0].ToString();
                            if (mLocalDS1.Rows.Count > tin)
                            {
                                Time_Out_Str = mLocalDS1.Rows[tin][0].ToString();
                            }
                            else if (mLocalDS1.Rows.Count > mLocalDS.Rows.Count)
                            {
                                Time_Out_Str = mLocalDS1.Rows[mLocalDS1.Rows.Count - 1][0].ToString();
                            }
                            else
                            {
                                Time_Out_Str = "";
                            }
                            TimeSpan ts4 = new TimeSpan();
                            ts4 = Convert.ToDateTime(string.Format("{0:hh\\:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
                            if (mLocalDS.Rows.Count <= 0)
                            {
                                Time_IN_Str = "";
                            }
                            else
                            {
                                Time_IN_Str = mLocalDS.Rows[tin][0].ToString();
                            }
                            if (Time_IN_Str == "" || Time_Out_Str == "")
                            {
                                time_Check_dbl = time_Check_dbl;
                            }
                            else
                            {

                                DateTime date1 = System.Convert.ToDateTime(Time_IN_Str);
                                DateTime date2 = System.Convert.ToDateTime(Time_Out_Str);
                                TimeSpan ts = new TimeSpan();
                                ts = date2.Subtract(date1);

                                //& ":" & Trim(ts.Minutes)
                                Total_Time_get = (ts.Hours).ToString();
                                ts4 = ts4.Add(ts);

                                //OT Time Get
                                Emp_Total_Work_Time_1 = (ts4.Hours) + ":" + (ts4.Minutes);
                                Emp_Total_Work_Time_1 = (ts4.Hours) + ":" + (ts4.Minutes);

                                if (Left_Val(ts4.Minutes.ToString(), 1) == "-" || Left_Val(ts4.Hours.ToString(), 1) == "-" || Emp_Total_Work_Time_1 == "0:-1")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";

                                }
                                if (Left_Val(Total_Time_get, 1) == "-")
                                {
                                    date2 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                                    ts = date2.Subtract(date1);
                                    ts = date2.Subtract(date1);
                                    Total_Time_get = (ts.Hours).ToString();
                                    time_Check_dbl = double.Parse(Total_Time_get);
                                    Emp_Total_Work_Time_1 = (ts.Hours) + ":" + (ts.Minutes).ToString();
                                    Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                                    if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" || Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    {
                                        Emp_Total_Work_Time_1 = "00:00";
                                    }
                                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" || Emp_Total_Work_Time_1 == "0:1" || Emp_Total_Work_Time_1 == "0:-3")
                                    {
                                        Emp_Total_Work_Time_1 = "00:00";
                                    }

                                }
                                else
                                {
                                    time_Check_dbl = time_Check_dbl + double.Parse(Total_Time_get);
                                }
                            }

                            //break;

                        }//For End
                    }
                    else
                    {
                        TimeSpan ts4 = new TimeSpan();
                        ts4 = Convert.ToDateTime(string.Format("{0:hh\\:mm}", Emp_Total_Work_Time_1)).TimeOfDay;

                        if (mLocalDS.Rows.Count <= 0)
                        {
                            Time_IN_Str = "";
                        }
                        else
                        {
                            Time_IN_Str = mLocalDS.Rows[0][0].ToString();
                        }
                        for (int tout = 0; tout < mLocalDS1.Rows.Count; tout++)
                        {
                            if (mLocalDS1.Rows.Count <= 0)
                            {
                                Time_Out_Str = "";
                            }
                            else
                            {
                                Time_Out_Str = mLocalDS1.Rows[0][0].ToString();
                            }

                            //break;

                        }
                        //Emp_Total_Work_Time
                        if (Time_IN_Str == "" || Time_Out_Str == "")
                        {
                            time_Check_dbl = 0;
                        }
                        else
                        {
                            DateTime date1 = System.Convert.ToDateTime(Time_IN_Str);
                            DateTime date2 = System.Convert.ToDateTime(Time_Out_Str);
                            TimeSpan ts = new TimeSpan();
                            ts = date2.Subtract(date1);
                            ts = date2.Subtract(date1);
                            Total_Time_get = (ts.Hours).ToString();
                            ts4 = ts4.Add(ts);
                            //OT Time Get
                            Emp_Total_Work_Time_1 = (ts4.Hours) + ":" + (ts4.Minutes);
                            Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                            if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" || Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                            {
                                Emp_Total_Work_Time_1 = "00:00";
                            }
                            if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" || Emp_Total_Work_Time_1 == "0:-1" || Emp_Total_Work_Time_1 == "0:-3")
                            {
                                Emp_Total_Work_Time_1 = "00:00";
                            }
                            if (Left_Val(Total_Time_get, 1) == "-")
                            {

                                date2 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                                ts = date2.Subtract(date1);
                                ts = date2.Subtract(date1);
                                Total_Time_get = ts.Hours.ToString();
                                time_Check_dbl = double.Parse(Total_Time_get);
                                Emp_Total_Work_Time_1 = (ts.Hours) + ":" + (ts.Minutes);
                                Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" || Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";

                                    time_Check_dbl = 0;
                                }
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" || Emp_Total_Work_Time_1 == "0:-1" || Emp_Total_Work_Time_1 == "0:-3")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";

                                    time_Check_dbl = 0;
                                }
                            }
                            else
                            {
                                time_Check_dbl = double.Parse(Total_Time_get);
                            }
                        }
                    }
                    //Emp_Total_Work_Time

                    //Emp_Total_Work_Time
                    string Emp_Total_Work_Time = "";
                    string Final_OT_Work_Time = "00:00";
                    string Final_OT_Work_Time_Val = "00:00";
                    Emp_Total_Work_Time = Emp_Total_Work_Time_1;
                    //Find Week OFF
                    string Employee_Week_Name = "";
                    string Assign_Week_Name = "";
                    //mLocalDS = Nothing
                    Employee_Week_Name = Convert.ToDateTime(Date_Value_Str).DayOfWeek.ToString();
                    SSQL = "Select WeekOff from Shiftrrecording where MachineNo='" + OT_Week_OFF_Machine_No + "'";
                    mLocalDS = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (mLocalDS.Rows.Count <= 0)
                    {
                        Assign_Week_Name = "";
                    }
                    else
                    {
                        //Assign_Week_Name = mLocalDS.Tables(0).Rows(0)(0)
                        Assign_Week_Name = "";
                    }

                    //NFH Days Check Start
                    bool NFH_Day_Check = false;
                    DateTime NFH_Date = new DateTime();
                    DateTime DOJ_Date_Format = new DateTime();
                    String qry_nfh = "Select NFHDate from NFH_Mst where NFHDate=convert(varchar,'" + Convert.ToDateTime(Date_Value_Str).ToString("yyyy/MM/dd") + "',103)";
                    mLocalDS = objdata.RptEmployeeMultipleDetails(qry_nfh);
                    //If mLocalDS.Tables(0).Rows.Count > 0 Then
                    //    'Date OF Joining Check Start
                    //    If DOJ_Date_Str <> "" Then
                    //        NFH_Date = mLocalDS.Tables(0).Rows(0)("NFHDate")
                    //        DOJ_Date_Format = DOJ_Date_Str
                    //        If DOJ_Date_Format.Date < NFH_Date.Date Then
                    //            'isPresent = True
                    //            NFH_Day_Check = True
                    //            NFH_Days_Count = NFH_Days_Count + 1
                    //        Else
                    //            'Skip
                    //       End If
                    //    Else
                    //        'isPresent = True
                    //        NFH_Day_Check = True
                    //        NFH_Days_Count = NFH_Days_Count + 1
                    //    End If
                    //    'Date OF Joining Check End
                    //End If
                    //NFH Days Check End

                    if (Employee_Week_Name.ToUpper() == Assign_Week_Name.ToUpper())
                    {
                        //Skip
                    }
                    else
                    {
                        //Get Employee Work Time
                        Double Calculate_Attd_Work_Time = 0;
                        Double Calculate_Attd_Work_Time_half = 0;
                        SSQL = "Select * from Employee_MST where MachineID='" + OT_Week_OFF_Machine_No + "' and Calculate_Work_Hours <> '' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'"; // And IsActive='Yes'";
                        mLocalDS = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (mLocalDS.Rows.Count <= 0)
                        {
                            Calculate_Attd_Work_Time = 7;
                        }
                        else
                        {
                            if (mLocalDS.Rows[0]["Calculate_Work_Hours"].ToString() != "")
                            {
                                Calculate_Attd_Work_Time = Convert.ToDouble(mLocalDS.Rows[0]["Calculate_Work_Hours"]);
                            }
                            else
                            {
                                Calculate_Attd_Work_Time = 0;
                            }
                            if (Calculate_Attd_Work_Time == 0)
                            {
                                Calculate_Attd_Work_Time = 7;
                            }
                            else
                            {
                                Calculate_Attd_Work_Time = 7;
                            }
                        }
                        Calculate_Attd_Work_Time_half = 4.0;

                        string Wages_Name_Check = "";
                        DataTable MDS_DS = new DataTable();
                        SSQL = "Select * from Employee_MST where MachineID='" + OT_Week_OFF_Machine_No + "' and CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                        MDS_DS = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (MDS_DS.Rows.Count != 0)
                        {
                            Wages_Name_Check = MDS_DS.Rows[0]["Wages"].ToString();
                        }
                        if (((Wages_Name_Check).ToUpper() == ("STAFF")) || ((Wages_Name_Check).ToUpper() == ("Watch & Ward").ToUpper()) || ((Wages_Name_Check).ToUpper() == ("Manager").ToUpper()))
                        {
                            Calculate_Attd_Work_Time = 7;
                        }
                        else
                        {
                            Calculate_Attd_Work_Time = 7;
                        }

                        halfPresent = "0"; //Half Days Calculate Start
                        if (time_Check_dbl >= Calculate_Attd_Work_Time_half && time_Check_dbl < Calculate_Attd_Work_Time)
                        {
                            isPresent = true;
                            halfPresent = "1";
                        }
                        //Half Days Calculate End
                        if (time_Check_dbl >= Calculate_Attd_Work_Time)
                        {
                            isPresent = true;
                            halfPresent = "0";
                        }
                    }
                    //Shift Check Code Start
                    DataTable Shift_Ds = new DataTable();
                    string Start_IN = "";
                    string End_In = "";
                    string Shift_Name_Store = "";
                    bool Shift_Check_blb_Check = false;

                    DateTime ShiftdateStartIN_Check = new DateTime();
                    DateTime ShiftdateEndIN_Check = new DateTime();
                    DateTime EmpdateIN_Check = new DateTime();
                    //if( isPresent == true)
                    //{
                    //    Shift_Check_blb_Check = false;
                    //    if((Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("STAFF")) || (Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper()== ("Watch & Ward")))
                    //    {
                    //        Employee_Shift_Name_DB = "GENERAL";
                    //    }
                    //    else
                    //    {

                    //        //Shift Master Check Code Start
                    //        SSQL = "Select * from Shift_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And ShiftDesc <> 'GENERAL' Order by ShiftDesc Asc";
                    //        Shift_Ds = objdata.RptEmployeeMultipleDetails(SSQL);
                    //        if(Shift_Ds.Rows.Count != 0 )
                    //        {
                    //            SSQL = "Select * from Shift_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And ShiftDesc like '%SHIFT%'";
                    //            Shift_Ds = objdata.RptEmployeeMultipleDetails(SSQL);
                    //            Shift_Check_blb = false;
                    //            for(K = 0;K < Shift_Ds.Rows.Count;K++)
                    //            {
                    //                Start_IN = Convert.ToDateTime(Date_Value_Str).AddDays(Convert.ToDouble(Shift_Ds.Rows[K]["StartIN_Days"])).ToString("dd/MM/yyyy") + " " + Shift_Ds.Rows[K]["StartIN"].ToString();
                    //                End_In = Convert.ToDateTime(Date_Value_Str).AddDays(Convert.ToDouble(Shift_Ds.Rows[K]["EndIN_Days"])).ToString("dd/MM/yyyy") + " " + Shift_Ds.Rows[K]["EndIN"].ToString();

                    //                ShiftdateStartIN_Check = Convert.ToDateTime(Start_IN);
                    //                ShiftdateEndIN_Check = Convert.ToDateTime(End_In);
                    //                EmpdateIN_Check = Convert.ToDateTime(Employee_Punch);
                    //                if(EmpdateIN_Check >= ShiftdateStartIN_Check && EmpdateIN_Check <= ShiftdateEndIN_Check)
                    //                {
                    //                    Employee_Shift_Name_DB = Shift_Ds.Rows[K]["ShiftDesc"].ToString();
                    //                    Shift_Check_blb_Check = true ;
                    //                    break;
                    //                }
                    //            }
                    //            if(Shift_Check_blb_Check == false)
                    //            {
                    //                Employee_Shift_Name_DB = "No Shift";
                    //            }
                    //        }
                    //        else
                    //        {
                    //            Employee_Shift_Name_DB = "No Shift";
                    //        }
                    //        //Shift Master Check Code End

                    //    }
                    //}
                    //else
                    //{
                    //    Shift_Name_Store = "No Shift";
                    //}



                    if (Time_IN_Str != "")
                    {
                        Shift_Check_blb_Check = false;
                        //if ((Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("STAFF")) || (Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("Watch & Ward").ToUpper()) || (Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("Manager").ToUpper()))
                          if ((Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("MONTHLY - STAFF") && Emp_DS.Rows[intRow]["ShiftType"].ToString().ToUpper() == ("GENERAL").ToUpper()) || (Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("WEEKLY").ToUpper() && Emp_DS.Rows[intRow]["ShiftType"].ToString().ToUpper() == ("GENERAL").ToUpper()) || (Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("CLEANING").ToUpper() && Emp_DS.Rows[intRow]["ShiftType"].ToString().ToUpper() == ("GENERAL").ToUpper()) || (Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("MIXING").ToUpper() && Emp_DS.Rows[intRow]["ShiftType"].ToString().ToUpper() == ("GENERAL").ToUpper()) || (Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("WEEKLY-HINDI").ToUpper() && Emp_DS.Rows[intRow]["ShiftType"].ToString().ToUpper() == ("GENERAL").ToUpper()))
                            {
                            Employee_Shift_Name_DB = "GENERAL";
                        }
                        else
                        {  //Shift Master Check Code Start
                            SSQL = "Select * from Shift_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And ShiftDesc <> 'GENERAL' Order by ShiftDesc Asc";
                            Shift_Ds = objdata.RptEmployeeMultipleDetails(SSQL);
                            if (Shift_Ds.Rows.Count != 0)
                            {
                                SSQL = "Select * from Shift_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And ShiftDesc like '%SHIFT%'";
                                Shift_Ds = objdata.RptEmployeeMultipleDetails(SSQL);
                                Shift_Check_blb = false;
                                for (K = 0; K < Shift_Ds.Rows.Count; K++)
                                {
                                    Start_IN = Convert.ToDateTime(Date_Value_Str).AddDays(Convert.ToDouble(Shift_Ds.Rows[K]["StartIN_Days"])).ToString("dd/MM/yyyy") + " " + Shift_Ds.Rows[K]["StartIN"].ToString();
                                    End_In = Convert.ToDateTime(Date_Value_Str).AddDays(Convert.ToDouble(Shift_Ds.Rows[K]["EndIN_Days"])).ToString("dd/MM/yyyy") + " " + Shift_Ds.Rows[K]["EndIN"].ToString();

                                    ShiftdateStartIN_Check = Convert.ToDateTime(Start_IN);
                                    ShiftdateEndIN_Check = Convert.ToDateTime(End_In);
                                    EmpdateIN_Check = Convert.ToDateTime(Employee_Punch);
                                    if (EmpdateIN_Check >= ShiftdateStartIN_Check && EmpdateIN_Check <= ShiftdateEndIN_Check)
                                    {
                                        Employee_Shift_Name_DB = Shift_Ds.Rows[K]["ShiftDesc"].ToString();
                                        Shift_Check_blb_Check = true;
                                        break;
                                    }
                                }
                                if (Shift_Check_blb_Check == false)
                                {
                                    Employee_Shift_Name_DB = "No Shift";
                                }
                            }
                            else
                            {
                                Employee_Shift_Name_DB = "No Shift";
                            }
                        }
                    }
                    else
                    {
                        Shift_Name_Store = "No Shift";
                        Employee_Shift_Name_DB = "No Shift";
                    }
                    string ShiftType = "";
                    string Pre_Abs = "";
                    if (isPresent == true)
                    {
                        if (Employee_Shift_Name_DB != "No Shift")
                        {
                            ShiftType = "Proper";

                            if (halfPresent == "1")
                            {
                                Pre_Abs = "Half Day";
                            }
                            else
                            {
                                Pre_Abs = "Present";
                            }

                        }
                        else
                        {
                            ShiftType = "Mismatch";

                            if (halfPresent == "1")
                            {
                                Pre_Abs = "Half Day";
                            }
                            else
                            {
                                Pre_Abs = "Present";
                            }

                        }
                    }
                    else
                    {
                        if (INTime == "" && OUTTime == "")
                        {
                            ShiftType = "Leave";
                            Employee_Shift_Name_DB = "Leave";
                            Pre_Abs = "Leave";

                        }
                        else if (INTime == "")
                        {
                            if (OUTTime != "")
                            {
                                ShiftType = "Improper";
                            }
                            Pre_Abs = "Absent";

                        }
                        else if (OUTTime == "")
                        {
                            if (INTime != "")
                            {
                                ShiftType = "Improper";
                            }
                            Pre_Abs = "Absent";

                        }
                        else if (INTime != "" && OUTTime != "")
                        {
                            ShiftType = "Absent";
                            Pre_Abs = "Absent";

                        }
                    }
                    bool Check_Week_Off_DOJ = false;
                    string Attn_Date_Day = DateTime.Parse(Date_Value_Str).DayOfWeek.ToString();
                    if ((Emp_WH_Day).ToUpper() == (Attn_Date_Day).ToUpper())
                    {
                        DateTime Week_Off_Date;
                        DateTime WH_DOJ_Date_Format;

                        // if (((Emp_DS.Rows[intRow]["Wages"].ToString().Trim().ToUpper()) == ("STAFF").ToUpper()) || ((Emp_DS.Rows[intRow]["Wages"].ToString().Trim().ToUpper()) == ("Watch & Ward").ToUpper()))
                        if ((Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("MONTHLY - STAFF") && Emp_DS.Rows[intRow]["ShiftType"].ToString().ToUpper() == ("GENERAL").ToUpper()) || (Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("WEEKLY").ToUpper() && Emp_DS.Rows[intRow]["ShiftType"].ToString().ToUpper() == ("GENERAL").ToUpper()) || (Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("CLEANING").ToUpper() && Emp_DS.Rows[intRow]["ShiftType"].ToString().ToUpper() == ("GENERAL").ToUpper()) || (Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("MIXING").ToUpper() && Emp_DS.Rows[intRow]["ShiftType"].ToString().ToUpper() == ("GENERAL").ToUpper()) || (Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("WEEKLY-HINDI").ToUpper() && Emp_DS.Rows[intRow]["ShiftType"].ToString().ToUpper() == ("GENERAL").ToUpper()))
                        {
                            if (DOJ_Date_Str == "")
                            {
                                Week_Off_Date = Convert.ToDateTime(Date_Value_Str); //Report Date
                                WH_DOJ_Date_Format = Convert.ToDateTime(DOJ_Date_Str);
                                if (WH_DOJ_Date_Format <= Week_Off_Date)
                                {
                                    Check_Week_Off_DOJ = true;
                                }
                                else
                                {
                                    Check_Week_Off_DOJ = false;
                                }
                            }
                            else
                            {
                                Check_Week_Off_DOJ = true;
                            }
                        }
                        else
                        {
                            Check_Week_Off_DOJ = true;
                        }
                        Month_WH_Count = Month_WH_Count + 1;
                        if (isPresent == true)
                        {
                            if (NFH_Day_Check == false)
                            {
                                if (halfPresent == "1")
                                {
                                    Present_WH_Count = Convert.ToDecimal(Convert.ToDouble(Present_WH_Count) + (0.5));
                                }
                                else if (halfPresent == "0")
                                {
                                    Present_WH_Count = Present_WH_Count + 1;
                                }
                            }
                        }
                    }
                    if (isPresent == true)
                    {
                        if (halfPresent == "1")
                        {
                            Present_Count = Convert.ToDecimal(Convert.ToDouble(Present_Count) + (0.5));
                        }
                        else if (halfPresent == "0")
                        {
                            Present_Count = Present_Count + 1;
                        }
                    }
                    else
                    {
                        Appsent_Count = Appsent_Count + 1;
                    }
                    DataTable mDataSet = new DataTable();
                    SSQL = "Select * from LogTime_Days where MachineID='" + Get_Machine_ID + "' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And Attn_Date_Str='" + Date_Value_Str + "'";
                    mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (mDataSet.Rows.Count != 0)
                    {
                        SSQL = "Delete from LogTime_Days where MachineID='" + Get_Machine_ID + "' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And Attn_Date_Str='" + Date_Value_Str + "'";
                        objdata.RptEmployeeMultipleDetails(SSQL);
                    }
                    SSQL = "Insert Into LogTime_Days(CompCode,LocCode,MachineID,ExistingCode,FirstName,LastName,DeptName,";
                    SSQL = SSQL + "Designation,DOJ,Wages,Present,Wh_Check,Wh_Count,Wh_Present_Count,Shift,Total_Hrs,Attn_Date_Str,Attn_Date,";
                    SSQL = SSQL + "TimeIN,TimeOUT,Total_Hrs1,TypeName,Present_Absent,Division)";
                    SSQL = SSQL + " Values('" + SessionCcode + "','" + SessionLcode + "','" + Get_Machine_ID + "','" + Emp_DS.Rows[intRow]["ExistingCode"] + "',";
                    SSQL = SSQL + "'" + Emp_DS.Rows[intRow]["FirstName"] + "','" + Emp_DS.Rows[intRow]["LastName"] + "',";
                    SSQL = SSQL + "'" + Emp_DS.Rows[intRow]["DeptName"] + "','" + Emp_DS.Rows[intRow]["Designation"] + "',";
                    SSQL = SSQL + "'" + Emp_DS.Rows[intRow]["DOJ"] + "','" + Emp_DS.Rows[intRow]["Wages"] + "','" + Present_Count + "','" + Check_Week_Off_DOJ + "',";
                    SSQL = SSQL + "'" + Month_WH_Count + "','" + Present_WH_Count + "','" + Employee_Shift_Name_DB + "','" + time_Check_dbl + "','" + Date_Value_Str + "','" + Convert.ToDateTime(Date_Value_Str).ToString("yyyy/MM/dd") + "',";
                    SSQL = SSQL + "'" + INTime + "','" + OUTTime + "','" + Emp_Total_Work_Time_1 + "','" + ShiftType + "','" + Pre_Abs + "','" + Emp_DS.Rows[intRow]["Division"].ToString() + "')";
                    objdata.RptEmployeeMultipleDetails(SSQL);
                    Present_Count = 0;
                    Check_Week_Off_DOJ = false;
                    Month_WH_Count = 0;
                    Present_WH_Count = 0;
                    Employee_Shift_Name_DB = "No Shift";
                }

                intI += 1;
                Total_Days_Count = 0;
                Final_Count = 0;
                Appsent_Count = 0;
                Present_Count = 0;
                //Month_WH_Count = 0;
                Present_WH_Count = 0;
                NFH_Days_Count = 0;
                NFH_Days_Present_Count = 0;
            }
        }
    }

    public string OutPunch_Get(string MachineID_Out, string OUT_Date_Str, string OUT_Date_Str1, string Time_Previous_Date)
    {
        string query = "";
        string SSQL = "";
        DataTable mLocalDS_New = new DataTable();
        DataTable mDataSet_New = new DataTable();

        SSQL = "Select MachineID,Max(TimeOUT) as [TimeOUT] from LogTime_OUT where MachineID='" + MachineID_Out + "'";
        SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " And TimeOUT >='" + Convert.ToDateTime(OUT_Date_Str).ToString("yyyy/MM/dd") + " " + "08:00' And TimeOUT <='" + Convert.ToDateTime(OUT_Date_Str1).ToString("yyyy/MM/dd") + " " + "10:00'";
        SSQL = SSQL + " Group By MachineID";
        SSQL = SSQL + " Order By Max(TimeOUT)";
        mLocalDS_New = objdata.RptEmployeeMultipleDetails(SSQL);

        if (mLocalDS_New.Rows.Count == 0)
        {
            query = "";
            return query;
        }
        else
        {
            for (int iTabRow = 0; iTabRow < mLocalDS_New.Rows.Count; iTabRow++)
            {
                string Machine_ID_Check = "";
                string Time_IN_Check_Shift = "";
                string Time_OUT_Check = "";

                Machine_ID_Check = mLocalDS_New.Rows[iTabRow]["MachineID"].ToString();
                SSQL = "Select MachineID,TimeIN from LogTime_IN where MachineID='" + Machine_ID_Check + "' And Compcode='" + SessionCcode + "'";
                SSQL = SSQL + " And LocCode='" + SessionLcode + "'";
                SSQL = SSQL + " And TimeIN >='" + Convert.ToDateTime(OUT_Date_Str).ToString("yyyy/MM/dd") + " " + "02:00'";
                SSQL = SSQL + " And TimeIN <='" + Convert.ToDateTime(OUT_Date_Str1).ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN Desc";
                mDataSet_New = objdata.RptEmployeeMultipleDetails(SSQL);
                if (mDataSet_New.Rows.Count == 0)
                {
                    Time_IN_Check_Shift = "";
                    Time_OUT_Check = mLocalDS_New.Rows[iTabRow]["TimeOUT"].ToString();
                    //Check Night Shift IN Time
                    //2nd Shift Check
                    string Shift_Start_Time = OUT_Date_Str + " " + "06:00";
                    string Shift_End_Time = OUT_Date_Str + " " + "10:00";
                    string Employee_Time = "";
                    DateTime ShiftdateStartIN;
                    DateTime ShiftdateEndIN;
                    DateTime EmpdateIN;

                    Employee_Time = Time_OUT_Check;
                    ShiftdateStartIN = System.Convert.ToDateTime(Shift_Start_Time);
                    ShiftdateEndIN = System.Convert.ToDateTime(Shift_End_Time);
                    EmpdateIN = System.Convert.ToDateTime(Employee_Time);

                    if ((EmpdateIN >= ShiftdateStartIN) && (EmpdateIN <= ShiftdateEndIN))
                    {
                        //Check Previous Day Night Shift In Time
                        SSQL = "Select MachineID,TimeIN from LogTime_IN where MachineID='" + Machine_ID_Check + "' And Compcode='" + SessionCcode + "'";
                        SSQL = SSQL + " And LocCode='" + SessionLcode + "'";
                        SSQL = SSQL + " And TimeIN >='" + Convert.ToDateTime(Time_Previous_Date).ToString("yyyy/MM/dd") + " " + "18:00'";
                        SSQL = SSQL + " And TimeIN <='" + Convert.ToDateTime(OUT_Date_Str).ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN Desc";
                        mDataSet_New = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (mDataSet_New.Rows.Count <= 0)
                        {
                            SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + MachineID_Out + "'";
                            SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                            SSQL = SSQL + " And TimeOUT >='" + Convert.ToDateTime(OUT_Date_Str).ToString("yyyy/MM/dd") + " " + "08:00' And TimeOUT <='" + Convert.ToDateTime(OUT_Date_Str1).ToString("yyyy/MM/dd") + " " + "10:00'";
                            SSQL = SSQL + " Order by TimeOUT";
                            query = SSQL;
                            return query;
                        }
                        else
                        {
                            query = "";
                            return query;
                        }
                    }
                    else
                    {
                        Shift_Start_Time = OUT_Date_Str1 + " " + "07:00";
                        Shift_End_Time = OUT_Date_Str1 + " " + "09:45";
                        Employee_Time = Time_OUT_Check;

                        ShiftdateStartIN = System.Convert.ToDateTime(Shift_Start_Time);
                        ShiftdateEndIN = System.Convert.ToDateTime(Shift_End_Time);
                        EmpdateIN = System.Convert.ToDateTime(Employee_Time);

                        if ((EmpdateIN >= ShiftdateStartIN) && (EmpdateIN <= ShiftdateEndIN))
                        {
                            SSQL = "Select MachineID,TimeIN from LogTime_IN where MachineID='" + Machine_ID_Check + "' And Compcode='" + SessionCcode + "'";
                            SSQL = SSQL + " And LocCode='" + SessionLcode + "'";
                            SSQL = SSQL + " And TimeIN >='" + Convert.ToDateTime(OUT_Date_Str1).ToString("yyyy/MM/dd") + " " + "06:00' And TimeIN <='" + Convert.ToDateTime(OUT_Date_Str1).ToString("yyyy/MM/dd") + " " + "09:45' Order by TimeIN Desc";
                            mDataSet_New = objdata.RptEmployeeMultipleDetails(SSQL);
                            if (mDataSet_New.Rows.Count <= 0)
                            {
                                //Check With Current Date Night Shift
                                SSQL = "Select MachineID,TimeIN from LogTime_IN where MachineID='" + Machine_ID_Check + "' And Compcode='" + SessionCcode + "'";
                                SSQL = SSQL + " And LocCode='" + SessionLcode + "'";
                                SSQL = SSQL + " And TimeIN >='" + Convert.ToDateTime(OUT_Date_Str).ToString("yyyy/MM/dd") + " " + "18:00' And TimeIN <='" + Convert.ToDateTime(OUT_Date_Str1).ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN Desc";
                                mDataSet_New = objdata.RptEmployeeMultipleDetails(SSQL);
                                if (mDataSet_New.Rows.Count <= 0)
                                {
                                    SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + MachineID_Out + "'";
                                    SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                    SSQL = SSQL + " And TimeOUT >='" + Convert.ToDateTime(OUT_Date_Str).ToString("yyyy/MM/dd") + " " + "08:00' And TimeOUT <='" + Convert.ToDateTime(OUT_Date_Str1).ToString("yyyy/MM/dd") + " " + "10:00'";
                                    SSQL = SSQL + " Order by TimeOUT";
                                    query = SSQL;
                                    return query;
                                }
                                else
                                {
                                    query = "";
                                    return query;
                                }
                            }
                            else
                            {
                                query = "";
                                return query;
                            }
                        }
                        else
                        {
                            SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + MachineID_Out + "'";
                            SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                            SSQL = SSQL + " And TimeOUT >='" + Convert.ToDateTime(OUT_Date_Str).ToString("yyyy/MM/dd") + " " + "08:00' And TimeOUT <='" + Convert.ToDateTime(OUT_Date_Str1).ToString("yyyy/MM/dd") + " " + "10:00'";
                            SSQL = SSQL + " Order by TimeOUT";
                            query = SSQL;
                            return query;
                        }
                    }

                }
                else
                {
                    query = "";
                    return query;
                }
            }
        }

        return query;
    }
    public string Left_Val(string Value, int Length)
    {
        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }
    public string Right_Val(string Value, int Length)
    {
        int i = 0;
        i = 0;
        if (Value.Length >= Length)
        {
            //i = Value.Length - Length
            return Value.Substring(Value.Length - Length, Length);
        }
        else
        {
            return Value;
        }
    }
    private void Load_Department()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlDepartment.Items.Clear();
        query = "Select *from Department_Mst";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlDepartment.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["DeptName"] = "-Select-";
        dr["DeptName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlDepartment.DataTextField = "DeptName";
        ddlDepartment.DataValueField = "DeptName";
        ddlDepartment.DataBind();
    }
    private void Load_Designation()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlDesignation.Items.Clear();
        query = "Select *from Designation_Mst where DeptName='" + ddlDepartment.SelectedItem.Text + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlDesignation.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["DesignName"] = "-Select-";
        dr["DesignName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlDesignation.DataTextField = "DesignName";
        ddlDesignation.DataValueField = "DesignName";
        ddlDesignation.DataBind();
    }
    protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Designation();
    }
    protected void btnDownLoad_Click(object sender, EventArgs e)
    {
        DataTable DT = new DataTable();

        SSQL = "Select ExistingCode,FirstName,'' as Shift,'' as Date,'' as Type from Employee_Mst Where CompCode='" + SessionCcode + "'";
        SSQL = SSQL + " And LocCode='" + SessionLcode + "' And IsActive='Yes' And DeptName='" + ddlDepartment.SelectedItem.Text + "'";
        DT = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT.Rows.Count > 0)
        {
            grid.DataSource = DT;
            grid.DataBind();
            string attachment = "attachment;filename=ManualAttendanceDownLoad.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);
            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();
        }
    }
    protected void btnUpload_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        string[] Time_IN_Count_Check;
        string[] Time_OUT_Count_Check;
        string In_Machine_IP = "";
        string Out_Machine_IP = "";

        if (FileUpload.HasFile)
        {
            //FileUpload.SaveAs(Server.MapPath("Upload/" + FileUpload.FileName));
            FileUpload.SaveAs(Server.MapPath("Upload/" + FileUpload.FileName));
        }
        else
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Upload the file...');", true);
        }
        if (!ErrFlag)
        {
            string fname = FileUpload.FileName;

            string connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Server.MapPath("Upload/" + FileUpload.FileName) + ";" + "Extended Properties=Excel 8.0;";
            OleDbConnection sSourceConnection = new OleDbConnection(connectionString);
            DataTable dts = new DataTable();
            using (sSourceConnection)
            {
                sSourceConnection.Open();
                OleDbCommand command = new OleDbCommand("Select * FROM [Sheet1$];", sSourceConnection);
                sSourceConnection.Close();

                using (OleDbCommand cmd = new OleDbCommand())
                {
                    command.CommandText = "Select * FROM [Sheet1$]";
                   
                    sSourceConnection.Open();
                }
                using (OleDbDataReader dr = command.ExecuteReader())
                {
                    if (dr.HasRows)
                    {

                    }
                }
                OleDbDataAdapter objDataAdapter = new OleDbDataAdapter();
                objDataAdapter.SelectCommand = command;
                DataSet ds = new DataSet();

                objDataAdapter.Fill(ds);
                DataTable dt = new DataTable();
                dt = ds.Tables[0];
                string constr = ConfigurationManager.AppSettings["ConnectionString"];

                if (dt.Rows.Count != 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        string ExistingCode = "";
                        string MachineID = "";
                        string FirstName = "";
                        string Shift = "";
                        string MnlDate = "";
                        string AttnType = "";

                        if (dt.Rows[i]["ExistingCode"].ToString() == "")
                        {
                            ErrFlag = true;
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "ProgressBarHide();", true);
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Check the ExistingCode. The Row Number is " + (i + 2) + "');", true);
                        }

                        if (dt.Rows[i]["FirstName"].ToString() == "")
                        {
                            ErrFlag = true;
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "ProgressBarHide();", true);
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Check the Name. The Row Number is " + (i + 2) + "');", true);
                        }

                        if (dt.Rows[i]["Shift"].ToString() == "")
                        {
                            ErrFlag = true;
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "ProgressBarHide();", true);
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Check the Shift. The Row Number is " + (i + 2) + "');", true);
                        }

                        if (dt.Rows[i]["Date"].ToString() == "")
                        {
                            ErrFlag = true;
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "ProgressBarHide();", true);
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Check the Date. The Row Number is " + (i + 2) + "');", true);
                        }

                        if (dt.Rows[i]["Type"].ToString() == "")
                        {
                            ErrFlag = true;
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "ProgressBarHide();", true);
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Check the Type. The Row Number is " + (i + 2) + "');", true);
                        }

                        if (!ErrFlag)
                        {
                            ExistingCode = dt.Rows[i]["ExistingCode"].ToString();
                            DataTable Machine_DT = new DataTable();
                            string SS = "Select MachineID from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                            SS = SS + " And ExistingCode='" + ExistingCode + "'";
                            Machine_DT = objdata.RptEmployeeMultipleDetails(SS);
                            if (Machine_DT.Rows.Count != 0)
                            {
                                MachineID = Machine_DT.Rows[0]["MachineID"].ToString();
                            }
                            FirstName = dt.Rows[i]["FirstName"].ToString();
                            Shift = dt.Rows[i]["Shift"].ToString();
                            MnlDate = dt.Rows[i]["Date"].ToString();
                            AttnType = dt.Rows[i]["Type"].ToString();

                            DateTime ShiftDate = new DateTime();
                            ShiftDate = Convert.ToDateTime(MnlDate);
                            DataTable DT_Shift = new DataTable();
                            string INDate = ""; string TimeIN_New = "";
                            string OUTDate = ""; string TimeOUT_New = "";
                            if (Shift != "")
                            {
                                SSQL = "Select *from  Shift_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                SSQL = SSQL + " And ShiftDesc='" + Shift.ToUpper() + "'";
                                DT_Shift = objdata.RptEmployeeMultipleDetails(SSQL);

                                if (DT_Shift.Rows.Count != 0)
                                {
                                    INDate = ShiftDate.ToString("dd/MM/yyyy");
                                    TimeIN_New = DT_Shift.Rows[0]["StartTime"].ToString();
                                    if (DT_Shift.Rows[0]["EndOUT_Days"].ToString() == "1")
                                    {
                                        OUTDate = ShiftDate.AddDays(1).ToString("dd/MM/yyyy");
                                    }
                                    else
                                    {
                                        OUTDate = ShiftDate.ToString("dd/MM/yyyy");
                                    }
                                    TimeOUT_New = DT_Shift.Rows[0]["EndTime"].ToString();
                                }
                            }
                            Time_IN_Count_Check = TimeIN_New.Split(':');
                            Time_OUT_Count_Check = TimeOUT_New.Split(':');

                            if (Time_IN_Count_Check.Length != 2)
                            {
                                ErrFlag = true;
                                ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "ProgressBarHide();", true);
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Time IN Invalid...!');", true);
                            }

                            if (Time_OUT_Count_Check.Length != 2)
                            {
                                ErrFlag = true;
                                ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "ProgressBarHide();", true);
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Time OUT Invalid...!');", true);
                            }

                            if (!ErrFlag)
                            {
                                DateTime dtin = Convert.ToDateTime(INDate);
                                string login = dtin.ToString("dd/MM/yyyy");

                                DateTime dtout = Convert.ToDateTime(OUTDate);
                                string logout = dtout.ToString("dd/MM/yyyy");
                                //string SSQL; 

                                string logtimein = login + " " + TimeIN_New;
                                string logtimeout = logout + " " + TimeOUT_New;

                                DateTime TimeIN = Convert.ToDateTime(logtimein);
                                DateTime TimeOUT = Convert.ToDateTime(logtimeout);

                                if (AttnType == "H") // Adding 4 hours to TimeIN for Half day 
                                {
                                    TimeOUT = Convert.ToDateTime(logtimein).AddHours(4);
                                    TimeOUT_New = TimeOUT.ToString("HH:mm");
                                }
                                else if (AttnType == "A") // Adding 2 hours to TimeIN for Absent
                                {
                                    TimeOUT = Convert.ToDateTime(logtimein).AddHours(2);
                                    TimeOUT_New = TimeOUT.ToString("HH:mm");
                                }

                                string MachineID_New = UTF8Encryption(MachineID);

                                DataTable mDataSet = new DataTable();

                                //Get IP Address
                                SSQL = "";
                                SSQL = "Select * from IPAddress_Mst where CompCode = '" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                                mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);
                                for (int j = 0; j < mDataSet.Rows.Count; j++)
                                {
                                    if (mDataSet.Rows[j]["IPMode"].ToString().ToUpper() == ("IN"))
                                        In_Machine_IP = mDataSet.Rows[j]["IPAddress"].ToString();

                                    if (mDataSet.Rows[j]["IPMode"].ToString().ToUpper() == ("OUT"))
                                        Out_Machine_IP = mDataSet.Rows[j]["IPAddress"].ToString();

                                }

                                //Delete Improper Punches
                                SSQL = "Delete from LogTime_IN where MachineID='" + MachineID_New + "'";
                                SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                SSQL = SSQL + " And TimeIN >='" + TimeIN.AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00" + "'";
                                SSQL = SSQL + " And TimeIN <='" + TimeIN.AddDays(1).ToString("yyyy/MM/dd") + " " + "02:00" + "'";
                                objdata.RptEmployeeMultipleDetails(SSQL);

                                SSQL = "Delete from LogTime_OUT where MachineID='" + MachineID_New + "'";
                                SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                SSQL = SSQL + " And TimeOUT >='" + TimeIN.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
                                SSQL = SSQL + " And TimeOUT <='" + TimeIN.AddDays(1).ToString("yyyy/MM/dd") + " " + "07:00" + "'";
                                objdata.RptEmployeeMultipleDetails(SSQL);

                                SSQL = "Delete ManAttn_Details where Machine_No='" + MachineID + "' and AttnDate = '" + INDate + "'";
                                objdata.RptEmployeeMultipleDetails(SSQL);

                                
                                SSQL = "Insert Into ManAttn_Details(CompCode,LocCode,EmpNo,ExistingCode,Machine_No,EmpName,AttnStatus,AttnDate,";
                                SSQL = SSQL + "AttnDateStr,TimeIn,TimeOut,LogTimeIn,LogTimeOut,Enter_Date,UserName,ShiftWise,Shift,OTHrs) Values(";
                                SSQL = SSQL + "'" + SessionCcode + "','" + SessionLcode + "','" + MachineID + "','" + ExistingCode + "',";
                                SSQL = SSQL + "'" + MachineID + "','" + FirstName + "','" + AttnType + "',";
                                SSQL = SSQL + "'" + INDate + "','" + INDate + "','" + TimeIN_New + "','" + TimeOUT_New + "',";
                                SSQL = SSQL + "'" + Convert.ToDateTime(TimeIN).ToString("yyyy/MM/dd HH:mm") + "','" + Convert.ToDateTime(TimeOUT).ToString("yyyy/MM/dd HH:mm") + "',GetDate(),'" + SessionUserName + "','2','" + Shift.ToUpper() + "','')";
                                objdata.RptEmployeeMultipleDetails(SSQL);

                                //Insert LogTime_IN
                                SSQL = "Insert Into LogTime_IN(CompCode,LocCode,IPAddress,MachineID,TimeIN) Values(";
                                SSQL = SSQL + "'" + SessionCcode + "','" + SessionLcode + "','" + In_Machine_IP + "','" + MachineID_New + "','" + Convert.ToDateTime(TimeIN).ToString("yyyy/MM/dd HH:mm") + "')";
                                objdata.RptEmployeeMultipleDetails(SSQL);

                                //Insert LogTime_OUT
                                SSQL = "Insert Into LogTime_OUT(CompCode,LocCode,IPAddress,MachineID,TimeOUT) Values(";
                                SSQL = SSQL + "'" + SessionCcode + "','" + SessionLcode + "','" + Out_Machine_IP + "','" + MachineID_New + "','" + Convert.ToDateTime(TimeOUT).ToString("yyyy/MM/dd HH:mm") + "')";
                                objdata.RptEmployeeMultipleDetails(SSQL);

                                Get_Working_Days_Save_DB(MachineID, INDate, INDate);
                                ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "ProgressBarHide();", true);
                                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Manual Attendance Saved Successfully...!');", true);

                            }
                        }
                    }
                }
            }
        }
    }
    protected void RdbShiftWise_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (RdbShiftWise.SelectedValue == "2")
        {
            div_ShiftType.Visible = false;
            div_ShiftDate.Visible = false;
            Panel1.Visible = true;
        }
        else
        {
            div_ShiftType.Visible = true;
            div_ShiftDate.Visible = true;
            Panel1.Visible = false;
        }
    }
}
