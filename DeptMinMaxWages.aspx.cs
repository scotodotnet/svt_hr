﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;

public partial class DeptMinMaxWages : System.Web.UI.Page
{
    string ShiftType1 = "";
    string Date = "";

    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string ss = "";
    ReportDocument report = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();
    //  EmployeeDetailsClass objEmpDetails = new EmployeeDetailsClass();



    string SSQL = "";

    DataSet ds = new DataSet();
    string WagesType = "";
    string Division = "";
    DataTable AutoDTable = new DataTable();
    DataTable DataCell = new DataTable();
    DataTable dt = new DataTable();
    DataTable mDataSet = new DataTable();
    
   
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Report-Department Min Max Wages";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Dashboard"));
                //li.Attributes.Add("class", "droplink active open");
            }
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();

            string TempWages = Request.QueryString["Wages"].ToString();
            WagesType = TempWages.Replace("_", "&");
           // Date = Request.QueryString["Date"].ToString();
            Division = Request.QueryString["Division"].ToString();

            GetDepartment_Wise_MinAndMax_Wages();
        }
    }

    public void GetDepartment_Wise_MinAndMax_Wages()
    {

        DataCell.Columns.Add("Department_Name");
        DataCell.Columns.Add("Designation");
        DataCell.Columns.Add("UnitI_Min_Wages");
        DataCell.Columns.Add("UnitI_Max_Wages");
        DataCell.Columns.Add("UnitII_Min_Wages");
           DataCell.Columns.Add("UnitII_Max_Wages");
        DataCell.Columns.Add("UnitIII_Min_Wages");
         DataCell.Columns.Add("UnitIII_Max_Wages");
         DataCell.Columns.Add("UnitIV_Min_Wages");
         DataCell.Columns.Add("UnitIV_Max_Wages");

        string Get_Employee_Type="";
        DataTable UNIT_DT=new DataTable();
          
        SSQL = "Delete from Dept_Wise_Min_Max_Wages_Rpt";
        objdata.RptEmployeeMultipleDetails(SSQL);

        // 'UNIT I Process Start
         SSQL = "Select EM.DeptName,EM.Designation,Min(EM.BaseSalary) as Min_Wages,MAX(EM.BaseSalary) as Max_Wages from Employee_Mst EM ";
      SSQL = SSQL + " Where EM.CompCode='"+SessionCcode + "' And EM.LocCode='UNIT I' And EM.Wages='" + WagesType + "' And EM.IsActive='Yes'";
      SSQL = SSQL + " And EM.BaseSalary !='0.00'";
        SSQL = SSQL + " Group by EM.DeptName,EM.Designation";
        SSQL = SSQL + " order by EM.DeptName,EM.Designation Asc";

        UNIT_DT = objdata.RptEmployeeMultipleDetails(SSQL);


        if(UNIT_DT.Rows.Count !=0)
        {
for (int i=0;i< UNIT_DT.Rows.Count;i++)
{
      SSQL = "Select * from Dept_Wise_Min_Max_Wages_Rpt where Department_Name='" + UNIT_DT.Rows[i]["DeptName"].ToString() + "'";
                SSQL = SSQL + " And Designation='" + UNIT_DT.Rows[i]["Designation"].ToString() + "'";
                mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);
                if (mDataSet.Rows.Count != 0)
                {
                    //Update
                    SSQL = "Update Dept_Wise_Min_Max_Wages_Rpt set UnitI_Min_Wages='" + UNIT_DT.Rows[i]["Min_Wages"].ToString() + "',";
                    SSQL = SSQL + " UnitI_Max_Wages='" + UNIT_DT.Rows[i]["Max_Wages"].ToString() + "' where Department_Name='" + UNIT_DT.Rows[i]["DeptName"].ToString() + "'";
                    SSQL = SSQL + " And Designation='" + UNIT_DT.Rows[i]["Designation"].ToString() + "'";
                    objdata.RptEmployeeMultipleDetails(SSQL);
                }
                else
                {
                    //Insert
                    SSQL = "Insert Into Dept_Wise_Min_Max_Wages_Rpt(Department_Name,Designation,UnitI_Min_Wages,UnitI_Max_Wages,";
                    SSQL = SSQL + " UnitII_Min_Wages,UnitII_Max_Wages,UnitIII_Min_Wages,UnitIII_Max_Wages,UnitIV_Min_Wages,UnitIV_Max_Wages)";
                    SSQL = SSQL + " Values('" + UNIT_DT.Rows[i]["DeptName"].ToString() + "','" + UNIT_DT.Rows[i]["Designation"].ToString() + "',";
                    SSQL = SSQL + " '" + UNIT_DT.Rows[i]["Min_Wages"].ToString() + "','" + UNIT_DT.Rows[i]["Max_Wages"].ToString() + "',";
                    SSQL = SSQL + " '0','0','0','0','0','0')";
                    objdata.RptEmployeeMultipleDetails(SSQL);
                }
}

        }
        
        //'UNIT I Process End

        //'UNIT II Process Start
        SSQL = "Select EM.DeptName,EM.Designation,Min(EM.BaseSalary) as Min_Wages,MAX(EM.BaseSalary) as Max_Wages from Employee_Mst EM ";
        SSQL = SSQL + " Where EM.CompCode='" + SessionCcode + "' And EM.LocCode='UNIT II' And EM.Wages='" + WagesType + "' And EM.IsActive='Yes'";
        SSQL = SSQL + " And EM.BaseSalary !='0.00'";
        SSQL = SSQL + " Group by EM.DeptName,EM.Designation";
        SSQL = SSQL + " order by EM.DeptName,EM.Designation Asc";

        UNIT_DT = objdata.RptEmployeeMultipleDetails(SSQL);

        if (UNIT_DT.Rows.Count != 0)
        {
            for (int i = 0; i < UNIT_DT.Rows.Count; i++)
            {
                SSQL = "Select * from Dept_Wise_Min_Max_Wages_Rpt where Department_Name='" + UNIT_DT.Rows[i]["DeptName"].ToString() + "'";
                SSQL = SSQL + " And Designation='" + UNIT_DT.Rows[i]["Designation"].ToString() + "'";
                mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);
                if (mDataSet.Rows.Count != 0)
                {
                    //Update
                    SSQL = "Update Dept_Wise_Min_Max_Wages_Rpt set UnitII_Min_Wages='" + UNIT_DT.Rows[i]["Min_Wages"].ToString() + "',";
                    SSQL = SSQL + " UnitII_Max_Wages='" + UNIT_DT.Rows[i]["Max_Wages"].ToString() + "' where Department_Name='" + UNIT_DT.Rows[i]["DeptName"].ToString() + "'";
                    SSQL = SSQL + " And Designation='" + UNIT_DT.Rows[i]["Designation"].ToString() + "'";
                    objdata.RptEmployeeMultipleDetails(SSQL);

                }
                else
                {
                    //Insert
                   
                     SSQL = "Insert Into Dept_Wise_Min_Max_Wages_Rpt(Department_Name,Designation,UnitI_Min_Wages,UnitI_Max_Wages,";
                     SSQL = SSQL + " UnitII_Min_Wages,UnitII_Max_Wages,UnitIII_Min_Wages,UnitIII_Max_Wages,UnitIV_Min_Wages,UnitIV_Max_Wages)";
                    SSQL = SSQL + " Values('" + UNIT_DT.Rows[i]["DeptName"].ToString() + "','" + UNIT_DT.Rows[i]["Designation"].ToString() + "',";
                    SSQL = SSQL + " '0','0','" + UNIT_DT.Rows[i]["Min_Wages"].ToString() + "','" + UNIT_DT.Rows[i]["Max_Wages"].ToString() + "',";
                    SSQL = SSQL + " '0','0','0','0')";
                    objdata.RptEmployeeMultipleDetails(SSQL);
                }
            }

        }

        //'UNIT II Process End


        //'UNIT III Process Start
        SSQL = "Select EM.DeptName,EM.Designation,Min(EM.BaseSalary) as Min_Wages,MAX(EM.BaseSalary) as Max_Wages from Employee_Mst EM ";
        SSQL = SSQL + " Where EM.CompCode='" + SessionCcode + "' And EM.LocCode='UNIT III' And EM.Wages='" + WagesType + "' And EM.IsActive='Yes'";
        SSQL = SSQL + " And EM.BaseSalary !='0.00'";
        SSQL = SSQL + " Group by EM.DeptName,EM.Designation";
        SSQL = SSQL + " order by EM.DeptName,EM.Designation Asc";

        UNIT_DT = objdata.RptEmployeeMultipleDetails(SSQL);

        if (UNIT_DT.Rows.Count != 0)
        {
            for (int i = 0; i < UNIT_DT.Rows.Count; i++)
            {
                SSQL = "Select * from Dept_Wise_Min_Max_Wages_Rpt where Department_Name='" + UNIT_DT.Rows[i]["DeptName"].ToString() + "'";
                SSQL = SSQL + " And Designation='" + UNIT_DT.Rows[i]["Designation"].ToString() + "'";
                mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);
                if (mDataSet.Rows.Count != 0)
                {
                    //Update
                    SSQL = "Update Dept_Wise_Min_Max_Wages_Rpt set UnitIII_Min_Wages='" + UNIT_DT.Rows[i]["Min_Wages"].ToString() + "',";
                    SSQL = SSQL + " UnitIII_Max_Wages='" + UNIT_DT.Rows[i]["Max_Wages"].ToString() + "' where Department_Name='" + UNIT_DT.Rows[i]["DeptName"].ToString() + "'";
                    SSQL = SSQL + " And Designation='" + UNIT_DT.Rows[i]["Designation"].ToString() + "'";
                    objdata.RptEmployeeMultipleDetails(SSQL);

                }
                else
                {
                    //Insert

                     SSQL = "Insert Into Dept_Wise_Min_Max_Wages_Rpt(Department_Name,Designation,UnitI_Min_Wages,UnitI_Max_Wages,";
                    SSQL = SSQL + " UnitII_Min_Wages,UnitII_Max_Wages,UnitIII_Min_Wages,UnitIII_Max_Wages,UnitIV_Min_Wages,UnitIV_Max_Wages)";
                    SSQL = SSQL + " Values('" + UNIT_DT.Rows[i]["DeptName"].ToString() + "','" + UNIT_DT.Rows[i]["Designation"].ToString() + "',";
                    SSQL = SSQL + " '0','0','0','0','" + UNIT_DT.Rows[i]["Min_Wages"].ToString() + "','" + UNIT_DT.Rows[i]["Max_Wages"].ToString() + "',";
                    SSQL = SSQL + " '0','0')";
                   
                    objdata.RptEmployeeMultipleDetails(SSQL);
                }
            }

        }

        //'UNIT III Process End



        //'UNIT IV Process Start
        SSQL = "Select EM.DeptName,EM.Designation,Min(EM.BaseSalary) as Min_Wages,MAX(EM.BaseSalary) as Max_Wages from Employee_Mst EM ";
        SSQL = SSQL + " Where EM.CompCode='" + SessionCcode + "' And EM.LocCode='UNIT IV' And EM.Wages='" + WagesType + "' And EM.IsActive='Yes'";
        SSQL = SSQL + " And EM.BaseSalary !='0.00'";
        SSQL = SSQL + " Group by EM.DeptName,EM.Designation";
        SSQL = SSQL + " order by EM.DeptName,EM.Designation Asc";

        UNIT_DT = objdata.RptEmployeeMultipleDetails(SSQL);

        if (UNIT_DT.Rows.Count != 0)
        {
            for (int i = 0; i < UNIT_DT.Rows.Count; i++)
            {
                SSQL = "Select * from Dept_Wise_Min_Max_Wages_Rpt where Department_Name='" + UNIT_DT.Rows[i]["DeptName"].ToString() + "'";
                SSQL = SSQL + " And Designation='" + UNIT_DT.Rows[i]["Designation"].ToString() + "'";
                mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);
                if (mDataSet.Rows.Count != 0)
                {
                    //Update
                    SSQL = "Update Dept_Wise_Min_Max_Wages_Rpt set UnitIV_Min_Wages='" + UNIT_DT.Rows[i]["Min_Wages"].ToString() + "',";
                    SSQL = SSQL + " UnitIV_Max_Wages='" + UNIT_DT.Rows[i]["Max_Wages"].ToString() + "' where Department_Name='" + UNIT_DT.Rows[i]["DeptName"].ToString() + "'";
                    SSQL = SSQL + " And Designation='" + UNIT_DT.Rows[i]["Designation"].ToString() + "'";
                    objdata.RptEmployeeMultipleDetails(SSQL);

                }
                else
                {
                    //Insert

                      SSQL = "Insert Into Dept_Wise_Min_Max_Wages_Rpt(Department_Name,Designation,UnitI_Min_Wages,UnitI_Max_Wages,";
                    SSQL = SSQL + " UnitII_Min_Wages,UnitII_Max_Wages,UnitIII_Min_Wages,UnitIII_Max_Wages,UnitIV_Min_Wages,UnitIV_Max_Wages)";
                    SSQL = SSQL + " Values('" + UNIT_DT.Rows[i]["DeptName"].ToString() + "','" + UNIT_DT.Rows[i]["Designation"].ToString() + "',";
                    SSQL = SSQL + " '0','0','0','0','0','0','" + UNIT_DT.Rows[i]["Min_Wages"].ToString() + "','" + UNIT_DT.Rows[i]["Max_Wages"].ToString() + "')";
                   

                    objdata.RptEmployeeMultipleDetails(SSQL);
                }
            }

        }

        //'UNIT IV Process End

        
        SSQL = "Select * from Dept_Wise_Min_Max_Wages_Rpt Order by Department_Name,Designation Asc";
        mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);

        for (int iRow = 0;iRow<mDataSet.Rows.Count;iRow++)
        {
               

           DataCell.NewRow();
            DataCell.Rows.Add();
            DataCell.Rows[iRow]["Department_Name"]=mDataSet.Rows[iRow]["Department_Name"].ToString();
            DataCell.Rows[iRow]["Designation"]=mDataSet.Rows[iRow]["Designation"].ToString();
            DataCell.Rows[iRow]["UnitI_Min_Wages"]=mDataSet.Rows[iRow]["UnitI_Min_Wages"].ToString();
            DataCell.Rows[iRow]["UnitI_Max_Wages"]=mDataSet.Rows[iRow]["UnitI_Max_Wages"].ToString();
            DataCell.Rows[iRow]["UnitII_Min_Wages"]=mDataSet.Rows[iRow]["UnitII_Min_Wages"].ToString();
            DataCell.Rows[iRow]["UnitII_Max_Wages"]=mDataSet.Rows[iRow]["UnitII_Max_Wages"].ToString();
            DataCell.Rows[iRow]["UnitIII_Min_Wages"]=mDataSet.Rows[iRow]["UnitIII_Min_Wages"].ToString();
            DataCell.Rows[iRow]["UnitIII_Max_Wages"]=mDataSet.Rows[iRow]["UnitIII_Max_Wages"].ToString();
            DataCell.Rows[iRow]["UnitIV_Min_Wages"]=mDataSet.Rows[iRow]["UnitIV_Min_Wages"].ToString();
            DataCell.Rows[iRow]["UnitIV_Max_Wages"]=mDataSet.Rows[iRow]["UnitIV_Max_Wages"].ToString();

        }

           ds.Tables.Add(DataCell);
                    //ReportDocument report = new ReportDocument();
                    report.Load(Server.MapPath("crystal/Dept_Wise_Min_Max_Report.rpt"));
                    
                    report.Database.Tables[0].SetDataSource(ds.Tables[0]);
                    report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                    CrystalReportViewer1.ReportSource = report;

    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        CrystalReportViewer1.Dispose();
    }
}
