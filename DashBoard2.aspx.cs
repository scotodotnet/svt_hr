﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Web.UI.DataVisualization.Charting;
using System.Globalization;

using Payroll;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Drawing;

public partial class DashBoard2 : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionUserName = Session["Usernmdisplay"].ToString();
            SessionUserID = Session["UserId"].ToString();
            SessionRights = Session["Rights"].ToString();
        }
        if (!IsPostBack)
        {
           
            
            UnitI();
            UnitII();
            UnitIII();
            UnitIV();


        }
    }
    private void UnitI()
    {
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();
        DataTable dt_break = new DataTable();
        DataTable dt_latein = new DataTable();
        DataTable dt_Amt=new DataTable();
        
        DataTable Datacells = new DataTable();
        string SSQL = "";
        DateTime todaydate = DateTime.Now.AddDays(-1);
        string date = "01/12/2017";
        DateTime dtdate = Convert.ToDateTime(date.ToString());

        Datacells.Columns.Add("TypeName");
        Datacells.Columns.Add("Count");
       
        SSQL = "";
        SSQL = "select Count(LD.MachineID)as presentCount from LogTime_Days LD inner join Employee_Mst EM on EM.MachineID = LD.MachineID  ";
        SSQL = SSQL + " where LD.CompCode='" + Session["Ccode"].ToString() + "' ANd LD.LocCode='UNIT I' And EM.CompCode='" + Session["Ccode"].ToString() + "' ANd EM.LocCode='UNIT I'";
        SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(todaydate).ToString("dd/MM/yyyy") + "',103)";
        SSQL = SSQL + "  And Shift !='No Shift' And TimeIN!=''  ";

        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count != 0)
        {
            Datacells.NewRow();
            Datacells.Rows.Add();
            Datacells.Rows[Datacells.Rows.Count - 1]["TypeName"] = "DayAttendance";
            Datacells.Rows[Datacells.Rows.Count - 1]["Count"] = dt.Rows[0]["presentCount"].ToString();

        }
        else
        {
            Datacells.NewRow();
            Datacells.Rows.Add();
            Datacells.Rows[Datacells.Rows.Count - 1]["TypeName"] = "DayAttendance";
            Datacells.Rows[Datacells.Rows.Count - 1]["Count"] = 0;

        }

        SSQL = "select Count(MachineID) as Improper";
        SSQL = SSQL + " from LogTime_Days   ";
        SSQL = SSQL + " where CompCode='" + SessionCcode.ToString() + "' ANd LocCode='UNIT I'";
        SSQL = SSQL + " And  CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(todaydate).ToString("dd/MM/yyyy") + "',103)";
        SSQL = SSQL + " And TypeName='Improper'";

        dt1 = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt1.Rows.Count != 0)
        {
            Datacells.NewRow();
            Datacells.Rows.Add();
            Datacells.Rows[Datacells.Rows.Count - 1]["TypeName"] = "Improper";
            Datacells.Rows[Datacells.Rows.Count - 1]["Count"] = dt1.Rows[0]["Improper"].ToString();

        }
        else
        {
            Datacells.NewRow();
            Datacells.Rows.Add();
            Datacells.Rows[Datacells.Rows.Count - 1]["TypeName"] = "Improper";
            Datacells.Rows[Datacells.Rows.Count - 1]["Count"] = 0;

        }


        SSQL = "Select Count(MachineID)as Hostel from LogTimeHostel_Lunch where  CompCode='" + SessionCcode + "' and LocCode='UNIT I' ";
        SSQL = SSQL + " And TimeIN >='" + todaydate.AddDays(0).ToString("yyyy/MM/dd") + " " + "07:00" + "'";
        SSQL = SSQL + " And TimeIN <='" + todaydate.AddDays(0).ToString("yyyy/MM/dd") + " " + "23:00" + "' ";
        dt_break = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt_break.Rows.Count != 0)
        {
            Datacells.NewRow();
            Datacells.Rows.Add();
            Datacells.Rows[Datacells.Rows.Count - 1]["TypeName"] = "HostelCountName";
            Datacells.Rows[Datacells.Rows.Count - 1]["Count"] = dt_break.Rows[0]["Hostel"].ToString();

        }
        else
        {
            Datacells.NewRow();
            Datacells.Rows.Add();
            Datacells.Rows[Datacells.Rows.Count - 1]["TypeName"] = "HostelCountName";
            Datacells.Rows[Datacells.Rows.Count - 1]["Count"] = 0;

        }

        SSQL = "";
        SSQL = "Select Count(MachineID)as presentCount from LogTime_Days ";
        SSQL = SSQL + " where CompCode='" + Session["Ccode"].ToString() + "' ANd LocCode='UNIT I'";
        SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(todaydate).ToString("dd/MM/yyyy") + "',103)";
        SSQL = SSQL + "  And Shift ='No Shift' And TimeIN !=''  ";
        dt_latein = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt_latein.Rows.Count != 0)
        {
            Datacells.NewRow();
            Datacells.Rows.Add();
            Datacells.Rows[Datacells.Rows.Count - 1]["TypeName"] = "LateIN";
            Datacells.Rows[Datacells.Rows.Count - 1]["Count"] = dt_latein.Rows[0]["presentCount"].ToString();

        }
        else
        {
            Datacells.NewRow();
            Datacells.Rows.Add();
            Datacells.Rows[Datacells.Rows.Count - 1]["TypeName"] = "LateIN";
            Datacells.Rows[Datacells.Rows.Count - 1]["Count"] = 0;

        }
        SSQL = "";
        SSQL = " select  isnull (sum((case EM.CatName when 'LABOUR' then BaseSalary else (BaseSalary/26) end)),0) as Amt ";
        SSQL = SSQL + "from Employee_Mst EM inner join LogTime_Days LD on EM.MachineID =LD.MachineID ";
        SSQL = SSQL + " where LD.CompCode='" + Session["Ccode"].ToString() + "' ANd LD.LocCode='UNIT I'";
        SSQL = SSQL + " and EM.CompCode='" + Session["Ccode"].ToString() + "' ANd EM.LocCode='UNIT I'";
        SSQL = SSQL + " And CONVERT(DATETIME,LD.Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(todaydate).ToString("dd/MM/yyyy") + "',103)";
        SSQL = SSQL + "  And LD.Present !='0.0'  ";
        dt_Amt = objdata.RptEmployeeMultipleDetails(SSQL);


        if (dt_Amt.Rows.Count != 0)
        {
            Decimal amt = Convert.ToDecimal(dt_Amt.Rows[0]["Amt"].ToString());
            decimal rounded = Math.Round(amt, 2);
            Datacells.NewRow();
            Datacells.Rows.Add();
            Datacells.Rows[Datacells.Rows.Count - 1]["TypeName"] = "Costing";
            Datacells.Rows[Datacells.Rows.Count - 1]["Count"] = rounded;

        }
        else
        {
            Datacells.NewRow();
            Datacells.Rows.Add();
            Datacells.Rows[Datacells.Rows.Count - 1]["TypeName"] = "Costing";
            Datacells.Rows[Datacells.Rows.Count - 1]["Count"] = 0;

        }
        UnitoneChart.DataSource = Datacells;
        UnitoneChart.Series["Series1"].XValueMember = "TypeName";
        UnitoneChart.Series["Series1"].YValueMembers = "Count";

        this.UnitoneChart.Series[0]["PieLabelStyle"] = "Outside";
        this.UnitoneChart.ChartAreas[0].Area3DStyle.Enable3D = true;
        this.UnitoneChart.ChartAreas[0].Area3DStyle.Inclination = 1;
        this.UnitoneChart.Series[0].BorderWidth = 1;
        this.UnitoneChart.Series[0].BorderColor = System.Drawing.Color.FromArgb(26, 59, 105);
        this.UnitoneChart.Legends.Clear();
        this.UnitoneChart.Legends.Add("Legend1");
        this.UnitoneChart.Legends[0].Enabled = true;
        this.UnitoneChart.Legends[0].Docking = Docking.Bottom;
        this.UnitoneChart.Legends[0].Alignment = System.Drawing.StringAlignment.Center;
        this.UnitoneChart.Series[0].LegendText = "#PERCENT";
        //this.Current_Month_Sale_Chart.DataManipulator.Sort(PointSortOrder.Descending, Chart2.Series[0]);
        UnitoneChart.DataBind();
    }


    private void UnitII()
    {
   
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();
        DataTable dt_break = new DataTable();
        DataTable dt_latein = new DataTable();
        DataTable dt_Amt = new DataTable();
        DataTable Datacells = new DataTable();
        string SSQL = "";
        DateTime todaydate = DateTime.Now.AddDays(-1);
        string date = "01/12/2017";
        DateTime dtdate = Convert.ToDateTime(date.ToString());

        Datacells.Columns.Add("TypeName");
        Datacells.Columns.Add("Count");

        SSQL = "";
        SSQL = "select Count(LD.MachineID)as presentCount from LogTime_Days LD inner join Employee_Mst EM on EM.MachineID = LD.MachineID  ";
        SSQL = SSQL + " where LD.CompCode='" + Session["Ccode"].ToString() + "' ANd LD.LocCode='UNIT II' And EM.CompCode='" + Session["Ccode"].ToString() + "' ANd EM.LocCode='UNIT II'";
        SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(todaydate).ToString("dd/MM/yyyy") + "',103)";
        SSQL = SSQL + "  And Shift !='No Shift' And TimeIN!=''  ";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count != 0)
        {
            Datacells.NewRow();
            Datacells.Rows.Add();
            Datacells.Rows[Datacells.Rows.Count - 1]["TypeName"] = "DayAttendance";
            Datacells.Rows[Datacells.Rows.Count - 1]["Count"] = dt.Rows[0]["presentCount"].ToString();

        }
        else
        {
            Datacells.NewRow();
            Datacells.Rows.Add();
            Datacells.Rows[Datacells.Rows.Count - 1]["TypeName"] = "DayAttendance";
            Datacells.Rows[Datacells.Rows.Count - 1]["Count"] = 0;

        }

        SSQL = "select Count(MachineID) as Improper";
        SSQL = SSQL + " from LogTime_Days   ";
        SSQL = SSQL + " where CompCode='" + SessionCcode.ToString() + "' ANd LocCode='UNIT II'";
        SSQL = SSQL + " And  CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(todaydate).ToString("dd/MM/yyyy") + "',103)";
        SSQL = SSQL + " And TypeName='Improper'";

        dt1 = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt1.Rows.Count != 0)
        {
            Datacells.NewRow();
            Datacells.Rows.Add();
            Datacells.Rows[Datacells.Rows.Count - 1]["TypeName"] = "Improper";
            Datacells.Rows[Datacells.Rows.Count - 1]["Count"] = dt1.Rows[0]["Improper"].ToString();

        }
        else
        {
            Datacells.NewRow();
            Datacells.Rows.Add();
            Datacells.Rows[Datacells.Rows.Count - 1]["TypeName"] = "Improper";
            Datacells.Rows[Datacells.Rows.Count - 1]["Count"] = 0;

        }


        SSQL = "Select Count(MachineID)as Hostel from LogTimeHostel_Lunch where  CompCode='" + SessionCcode + "' and LocCode='UNIT II' ";
        SSQL = SSQL + " And TimeIN >='" + todaydate.AddDays(0).ToString("yyyy/MM/dd") + " " + "07:00" + "'";
        SSQL = SSQL + " And TimeIN <='" + todaydate.AddDays(0).ToString("yyyy/MM/dd") + " " + "23:00" + "' ";
        dt_break = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt_break.Rows.Count != 0)
        {
            Datacells.NewRow();
            Datacells.Rows.Add();
            Datacells.Rows[Datacells.Rows.Count - 1]["TypeName"] = "HostelCountName";
            Datacells.Rows[Datacells.Rows.Count - 1]["Count"] = dt_break.Rows[0]["Hostel"].ToString();

        }
        else
        {
            Datacells.NewRow();
            Datacells.Rows.Add();
            Datacells.Rows[Datacells.Rows.Count - 1]["TypeName"] = "HostelCountName";
            Datacells.Rows[Datacells.Rows.Count - 1]["Count"] = 0;

        }
        SSQL = "";
        SSQL = "Select Count(MachineID)as presentCount from LogTime_Days ";
        SSQL = SSQL + " where CompCode='" + Session["Ccode"].ToString() + "' ANd LocCode='UNIT II'";
        SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(todaydate).ToString("dd/MM/yyyy") + "',103)";
        SSQL = SSQL + "  And Shift ='No Shift' And TimeIN !=''  ";
        dt_latein = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt_latein.Rows.Count != 0)
        {
            Datacells.NewRow();
            Datacells.Rows.Add();
            Datacells.Rows[Datacells.Rows.Count - 1]["TypeName"] = "LateIN";
            Datacells.Rows[Datacells.Rows.Count - 1]["Count"] = dt_latein.Rows[0]["presentCount"].ToString();

        }
        else
        {
            Datacells.NewRow();
            Datacells.Rows.Add();
            Datacells.Rows[Datacells.Rows.Count - 1]["TypeName"] = "LateIN";
            Datacells.Rows[Datacells.Rows.Count - 1]["Count"] = 0;

        }
        SSQL = "";
        SSQL = " select isnull(sum((case EM.CatName when 'LABOUR' then BaseSalary else (BaseSalary/26) end)),0) as Amt ";
        SSQL = SSQL + "from Employee_Mst EM inner join LogTime_Days LD on EM.MachineID =LD.MachineID ";
        SSQL = SSQL + " where LD.CompCode='" + Session["Ccode"].ToString() + "' ANd LD.LocCode='UNIT II'";
        SSQL = SSQL + " And EM.CompCode='" + Session["Ccode"].ToString() + "' ANd EM.LocCode='UNIT II'";
        SSQL = SSQL + " And CONVERT(DATETIME,LD.Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(todaydate).ToString("dd/MM/yyyy") + "',103)";
        SSQL = SSQL + "  And LD.Present !='0.0'  ";
        dt_Amt = objdata.RptEmployeeMultipleDetails(SSQL);


        if (dt_Amt.Rows.Count != 0)
        {

            Decimal amt = Convert.ToDecimal(dt_Amt.Rows[0]["Amt"].ToString());
            decimal rounded = Math.Round(amt, 2);
            Datacells.NewRow();
            Datacells.Rows.Add();
            Datacells.Rows[Datacells.Rows.Count - 1]["TypeName"] = "Costing";
            Datacells.Rows[Datacells.Rows.Count - 1]["Count"] = rounded;
        }
        else
        {
            Datacells.NewRow();
            Datacells.Rows.Add();
            Datacells.Rows[Datacells.Rows.Count - 1]["TypeName"] = "Costing";
            Datacells.Rows[Datacells.Rows.Count - 1]["Count"] = 0;

        }

        UnittwoCart.DataSource = Datacells;
        UnittwoCart.Series["Series1"].XValueMember = "TypeName";
        UnittwoCart.Series["Series1"].YValueMembers = "Count";

        this.UnittwoCart.Series[0]["PieLabelStyle"] = "Outside";
        this.UnittwoCart.ChartAreas[0].Area3DStyle.Enable3D = true;
        this.UnittwoCart.ChartAreas[0].Area3DStyle.Inclination = 1;
        this.UnittwoCart.Series[0].BorderWidth = 1;
        this.UnittwoCart.Series[0].BorderColor = System.Drawing.Color.FromArgb(26, 59, 105);
        this.UnittwoCart.Legends.Clear();
        this.UnittwoCart.Legends.Add("Legend1");
        this.UnittwoCart.Legends[0].Enabled = true;
        this.UnittwoCart.Legends[0].Docking = Docking.Bottom;
        this.UnittwoCart.Legends[0].Alignment = System.Drawing.StringAlignment.Center;
        this.UnittwoCart.Series[0].LegendText = "#PERCENT";
        //this.Current_Month_Sale_Chart.DataManipulator.Sort(PointSortOrder.Descending, Chart2.Series[0]);
        UnittwoCart.DataBind();
    
    }



    private void UnitIII()
    {


        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();
        DataTable dt_break = new DataTable();
        DataTable dt_latein = new DataTable();
        DataTable dt_Amt = new DataTable();
        DataTable Datacells = new DataTable();
        string SSQL = "";
        DateTime todaydate = DateTime.Now.AddDays(-1);
        string date = "01/12/2017";
        DateTime dtdate = Convert.ToDateTime(date.ToString());

        Datacells.Columns.Add("TypeName");
        Datacells.Columns.Add("Count");

        SSQL = "";
        SSQL = "select Count(LD.MachineID)as presentCount from LogTime_Days LD inner join Employee_Mst EM on EM.MachineID = LD.MachineID  ";
        SSQL = SSQL + " where LD.CompCode='" + Session["Ccode"].ToString() + "' ANd LD.LocCode='UNIT III' And EM.CompCode='" + Session["Ccode"].ToString() + "' ANd EM.LocCode='UNIT III'";
        SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(todaydate).ToString("dd/MM/yyyy") + "',103)";
        SSQL = SSQL + "  And Shift !='No Shift' And TimeIN!=''  ";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count != 0)
        {
            Datacells.NewRow();
            Datacells.Rows.Add();
            Datacells.Rows[Datacells.Rows.Count - 1]["TypeName"] = "DayAttendance";
            Datacells.Rows[Datacells.Rows.Count - 1]["Count"] = dt.Rows[0]["presentCount"].ToString();

        }
        else
        {
            Datacells.NewRow();
            Datacells.Rows.Add();
            Datacells.Rows[Datacells.Rows.Count - 1]["TypeName"] = "DayAttendance";
            Datacells.Rows[Datacells.Rows.Count - 1]["Count"] = 0;

        }

        SSQL = "select Count(MachineID) as Improper";
        SSQL = SSQL + " from LogTime_Days   ";
        SSQL = SSQL + " where CompCode='" + SessionCcode.ToString() + "' ANd LocCode='UNIT III'";
        SSQL = SSQL + " And  CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(todaydate).ToString("dd/MM/yyyy") + "',103)";
        SSQL = SSQL + " And TypeName='Improper'";

        dt1 = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt1.Rows.Count != 0)
        {
            Datacells.NewRow();
            Datacells.Rows.Add();
            Datacells.Rows[Datacells.Rows.Count - 1]["TypeName"] = "Improper";
            Datacells.Rows[Datacells.Rows.Count - 1]["Count"] = dt1.Rows[0]["Improper"].ToString();

        }
        else
        {
            Datacells.NewRow();
            Datacells.Rows.Add();
            Datacells.Rows[Datacells.Rows.Count - 1]["TypeName"] = "Improper";
            Datacells.Rows[Datacells.Rows.Count - 1]["Count"] = 0;

        }


        SSQL = "Select Count(MachineID)as Hostel from LogTimeHostel_Lunch where  CompCode='" + SessionCcode + "' and LocCode='UNIT III' ";
        SSQL = SSQL + " And TimeIN >='" + todaydate.AddDays(0).ToString("yyyy/MM/dd") + " " + "07:00" + "'";
        SSQL = SSQL + " And TimeIN <='" + todaydate.AddDays(0).ToString("yyyy/MM/dd") + " " + "23:00" + "' ";
        dt_break = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt_break.Rows.Count != 0)
        {
            Datacells.NewRow();
            Datacells.Rows.Add();
            Datacells.Rows[Datacells.Rows.Count - 1]["TypeName"] = "HostelCountName";
            Datacells.Rows[Datacells.Rows.Count - 1]["Count"] = dt_break.Rows[0]["Hostel"].ToString();

        }
        else
        {
            Datacells.NewRow();
            Datacells.Rows.Add();
            Datacells.Rows[Datacells.Rows.Count - 1]["TypeName"] = "HostelCountName";
            Datacells.Rows[Datacells.Rows.Count - 1]["Count"] = 0;

        }
        SSQL = "";
        SSQL = "Select Count(MachineID)as presentCount from LogTime_Days ";
        SSQL = SSQL + " where CompCode='" + Session["Ccode"].ToString() + "' ANd LocCode='UNIT III'";
        SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(todaydate).ToString("dd/MM/yyyy") + "',103)";
        SSQL = SSQL + "  And Shift ='No Shift' And TimeIN !=''  ";
        dt_latein = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt_latein.Rows.Count != 0)
        {
            Datacells.NewRow();
            Datacells.Rows.Add();
            Datacells.Rows[Datacells.Rows.Count - 1]["TypeName"] = "LateIN";
            Datacells.Rows[Datacells.Rows.Count - 1]["Count"] = dt_latein.Rows[0]["presentCount"].ToString();

        }
        else
        {
            Datacells.NewRow();
            Datacells.Rows.Add();
            Datacells.Rows[Datacells.Rows.Count - 1]["TypeName"] = "LateIN";
            Datacells.Rows[Datacells.Rows.Count - 1]["Count"] = 0;

        }
        SSQL = "";
        SSQL = " select isnull(sum((case EM.CatName when 'LABOUR' then BaseSalary else (BaseSalary/26) end)),0) as Amt ";
        SSQL = SSQL + "from Employee_Mst EM inner join LogTime_Days LD on EM.MachineID =LD.MachineID ";
        SSQL = SSQL + " where LD.CompCode='" + Session["Ccode"].ToString() + "' ANd LD.LocCode='UNIT III'";
        SSQL = SSQL + " And EM.CompCode='" + Session["Ccode"].ToString() + "' ANd EM.LocCode='UNIT III'";
        SSQL = SSQL + " And CONVERT(DATETIME,LD.Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(todaydate).ToString("dd/MM/yyyy") + "',103)";
        SSQL = SSQL + "  And LD.Present !='0.0'  ";
        dt_Amt = objdata.RptEmployeeMultipleDetails(SSQL);


        if (dt_Amt.Rows.Count != 0)
        {

            Decimal amt = Convert.ToDecimal(dt_Amt.Rows[0]["Amt"].ToString());
            decimal rounded = Math.Round(amt, 2);
            Datacells.NewRow();
            Datacells.Rows.Add();
            Datacells.Rows[Datacells.Rows.Count - 1]["TypeName"] = "Costing";
            Datacells.Rows[Datacells.Rows.Count - 1]["Count"] = rounded;
        }
        else
        {
            Datacells.NewRow();
            Datacells.Rows.Add();
            Datacells.Rows[Datacells.Rows.Count - 1]["TypeName"] = "Costing";
            Datacells.Rows[Datacells.Rows.Count - 1]["Count"] = 0;

        }
        unitthreeChart.DataSource = Datacells;
        unitthreeChart.Series["Series1"].XValueMember = "TypeName";
        unitthreeChart.Series["Series1"].YValueMembers = "Count";

        this.unitthreeChart.Series[0]["PieLabelStyle"] = "Outside";
        this.unitthreeChart.ChartAreas[0].Area3DStyle.Enable3D = true;
        this.unitthreeChart.ChartAreas[0].Area3DStyle.Inclination = 1;
        this.unitthreeChart.Series[0].BorderWidth = 1;
        this.unitthreeChart.Series[0].BorderColor = System.Drawing.Color.FromArgb(26, 59, 105);
        this.unitthreeChart.Legends.Clear();
        this.unitthreeChart.Legends.Add("Legend1");
        this.unitthreeChart.Legends[0].Enabled = true;
        this.unitthreeChart.Legends[0].Docking = Docking.Bottom;
        this.unitthreeChart.Legends[0].Alignment = System.Drawing.StringAlignment.Center;
        this.unitthreeChart.Series[0].LegendText = "#PERCENT";
        //this.Current_Month_Sale_Chart.DataManipulator.Sort(PointSortOrder.Descending, Chart2.Series[0]);
        unitthreeChart.DataBind();

    }
    private void UnitIV()
    {


        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();
        DataTable dt_break = new DataTable();
        DataTable dt_latein = new DataTable();
        DataTable dt_Amt = new DataTable();
        DataTable Datacells = new DataTable();
        string SSQL = "";
        DateTime todaydate = DateTime.Now.AddDays(-1);
        string date = "01/12/2017";
        DateTime dtdate = Convert.ToDateTime(date.ToString());

        Datacells.Columns.Add("TypeName");
        Datacells.Columns.Add("Count");

        SSQL = "";
        SSQL = "select Count(LD.MachineID)as presentCount from LogTime_Days LD inner join Employee_Mst EM on EM.MachineID = LD.MachineID  ";
        SSQL = SSQL + " where LD.CompCode='" + Session["Ccode"].ToString() + "' ANd LD.LocCode='UNIT IV' And EM.CompCode='" + Session["Ccode"].ToString() + "' ANd EM.LocCode='UNIT IV'";
        SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(todaydate).ToString("dd/MM/yyyy") + "',103)";
        SSQL = SSQL + "  And Shift !='No Shift' And TimeIN!=''  ";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count != 0)
        {
            Datacells.NewRow();
            Datacells.Rows.Add();
            Datacells.Rows[Datacells.Rows.Count - 1]["TypeName"] = "DayAttendance";
            Datacells.Rows[Datacells.Rows.Count - 1]["Count"] = dt.Rows[0]["presentCount"].ToString();

        }
        else
        {
            Datacells.NewRow();
            Datacells.Rows.Add();
            Datacells.Rows[Datacells.Rows.Count - 1]["TypeName"] = "DayAttendance";
            Datacells.Rows[Datacells.Rows.Count - 1]["Count"] = 0;

        }

        SSQL = "select Count(MachineID) as Improper";
        SSQL = SSQL + " from LogTime_Days   ";
        SSQL = SSQL + " where CompCode='" + SessionCcode.ToString() + "' ANd LocCode='UNIT IV'";
        SSQL = SSQL + " And  CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(todaydate).ToString("dd/MM/yyyy") + "',103)";
        SSQL = SSQL + " And TypeName='Improper'";

        dt1 = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt1.Rows.Count != 0)
        {
            Datacells.NewRow();
            Datacells.Rows.Add();
            Datacells.Rows[Datacells.Rows.Count - 1]["TypeName"] = "Improper";
            Datacells.Rows[Datacells.Rows.Count - 1]["Count"] = dt1.Rows[0]["Improper"].ToString();

        }
        else
        {
            Datacells.NewRow();
            Datacells.Rows.Add();
            Datacells.Rows[Datacells.Rows.Count - 1]["TypeName"] = "Improper";
            Datacells.Rows[Datacells.Rows.Count - 1]["Count"] = 0;

        }


        SSQL = "Select Count(MachineID)as Hostel from LogTimeHostel_Lunch where  CompCode='" + SessionCcode + "' and LocCode='UNIT IV' ";
        SSQL = SSQL + " And TimeIN >='" + todaydate.AddDays(0).ToString("yyyy/MM/dd") + " " + "07:00" + "'";
        SSQL = SSQL + " And TimeIN <='" + todaydate.AddDays(0).ToString("yyyy/MM/dd") + " " + "23:00" + "' ";
        dt_break = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt_break.Rows.Count != 0)
        {
            Datacells.NewRow();
            Datacells.Rows.Add();
            Datacells.Rows[Datacells.Rows.Count - 1]["TypeName"] = "HostelCountName";
            Datacells.Rows[Datacells.Rows.Count - 1]["Count"] = dt_break.Rows[0]["Hostel"].ToString();

        }
        else
        {
            Datacells.NewRow();
            Datacells.Rows.Add();
            Datacells.Rows[Datacells.Rows.Count - 1]["TypeName"] = "HostelCountName";
            Datacells.Rows[Datacells.Rows.Count - 1]["Count"] = 0;

        }
        SSQL = "";
        SSQL = "Select Count(MachineID)as presentCount from LogTime_Days ";
        SSQL = SSQL + " where CompCode='" + Session["Ccode"].ToString() + "' ANd LocCode='UNIT IV'";
        SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(todaydate).ToString("dd/MM/yyyy") + "',103)";
        SSQL = SSQL + "  And Shift ='No Shift' And TimeIN !=''  ";
        dt_latein = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt_latein.Rows.Count != 0)
        {
            Datacells.NewRow();
            Datacells.Rows.Add();
            Datacells.Rows[Datacells.Rows.Count - 1]["TypeName"] = "LateIN";
            Datacells.Rows[Datacells.Rows.Count - 1]["Count"] = dt_latein.Rows[0]["presentCount"].ToString();

        }
        else
        {
            Datacells.NewRow();
            Datacells.Rows.Add();
            Datacells.Rows[Datacells.Rows.Count - 1]["TypeName"] = "LateIN";
            Datacells.Rows[Datacells.Rows.Count - 1]["Count"] = 0;

        }
        SSQL = "";
        SSQL = " select isnull(sum((case EM.CatName when 'LABOUR' then BaseSalary else (BaseSalary/26) end)),0) as Amt ";
        SSQL = SSQL + "from Employee_Mst EM inner join LogTime_Days LD on EM.MachineID =LD.MachineID ";
        SSQL = SSQL + " where LD.CompCode='" + Session["Ccode"].ToString() + "' ANd LD.LocCode='UNIT IV'";
        SSQL = SSQL + " And EM.CompCode='" + Session["Ccode"].ToString() + "' ANd EM.LocCode='UNIT IV'";
        SSQL = SSQL + " And CONVERT(DATETIME,LD.Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(todaydate).ToString("dd/MM/yyyy") + "',103)";
        SSQL = SSQL + "  And LD.Present !='0.0'  ";
        dt_Amt = objdata.RptEmployeeMultipleDetails(SSQL);


        if (dt_Amt.Rows.Count != 0)
        {
            Datacells.NewRow();
            Datacells.Rows.Add();
            Datacells.Rows[Datacells.Rows.Count - 1]["TypeName"] = "Costing";
            Datacells.Rows[Datacells.Rows.Count - 1]["Count"] = dt_Amt.Rows[0]["Amt"].ToString();

        }
        else
        {

            Decimal amt = Convert.ToDecimal(dt_Amt.Rows[0]["Amt"].ToString());
            decimal rounded = Math.Round(amt, 2);
            Datacells.NewRow();
            Datacells.Rows.Add();
            Datacells.Rows[Datacells.Rows.Count - 1]["TypeName"] = "Costing";
            Datacells.Rows[Datacells.Rows.Count - 1]["Count"] = rounded;

        }
        unitfourChart.DataSource = Datacells;
        unitfourChart.Series["Series1"].XValueMember = "TypeName";
        unitfourChart.Series["Series1"].YValueMembers = "Count";

        this.unitfourChart.Series[0]["PieLabelStyle"] = "Outside";
        this.unitfourChart.ChartAreas[0].Area3DStyle.Enable3D = true;
        this.unitfourChart.ChartAreas[0].Area3DStyle.Inclination = 1;
        this.unitfourChart.Series[0].BorderWidth = 1;
        this.unitfourChart.Series[0].BorderColor = System.Drawing.Color.FromArgb(26, 59, 105);
        this.unitfourChart.Legends.Clear();
        this.unitfourChart.Legends.Add("Legend1");
        this.unitfourChart.Legends[0].Enabled = true;
        this.unitfourChart.Legends[0].Docking = Docking.Bottom;
        this.unitfourChart.Legends[0].Alignment = System.Drawing.StringAlignment.Center;
        this.unitfourChart.Series[0].LegendText = "#PERCENT";
        //this.Current_Month_Sale_Chart.DataManipulator.Sort(PointSortOrder.Descending, Chart2.Series[0]);
        unitfourChart.DataBind();

    }
}
