﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;
public partial class DayAttendanceWorkingHouseReport : System.Web.UI.Page
{

    string ShiftType1 = "";
    string Date = "";
    string Status = "";

    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string ss = "";
    ReportDocument report = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();
    //  EmployeeDetailsClass objEmpDetails = new EmployeeDetailsClass();



    string SSQL = "";
    string Adolescent_Shift;
    DataSet ds = new DataSet();

    string Division = "";
    DataTable AutoDTable = new DataTable();
    DataTable DataCell = new DataTable();
    DataTable dt = new DataTable();


    string Datestr = "";
    string Datestr1 = "";
    string ShiftType = "";
    DataTable mDataSet = new DataTable();

    DataTable mLocalDS_INTAB = new DataTable();
    DataTable mLocalDS_OUTTAB = new DataTable();
    string Date_Value_Str;
    string Time_IN_Str = "";
    string Time_Out_Str = "";
    int time_Check_dbl = 0;
    string Total_Time_get = "";

    DataRow dtRow;

    DataTable MLocal_Day = new DataTable();
    DataTable Payroll_DS = new DataTable();

    string Shift_Start_Time;
    string Shift_End_Time;
    string Employee_Time = "";
    DateTime ShiftdateStartIN = new DateTime();
    DateTime ShiftdateEndIN = new DateTime();
    DateTime EmpdateIN = new DateTime();
    string ColumnName = "";
    double Totol_Hours_Check = 0;
    double Totol_Hours_Check_1 = 8.2;
    string[] Time_Split_Str;
    string Time_IN_Get;
    string Time_Out_Update = "";
    long Random_No_Get;
    string[] Time_Minus_Value_Check;


    public string Left_Val(string Value, int Length)
    {
        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Report-Day Attendance Day Wise";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Dashboard"));
                //li.Attributes.Add("class", "droplink active open");
            }
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();

            Status = Request.QueryString["Status"].ToString();
            ShiftType1 = Request.QueryString["ShiftType1"].ToString();
            Date = Request.QueryString["Date"].ToString();
            Division = Request.QueryString["Division"].ToString();
            //if (SessionUserType == "1")
            //{
            //    GetAttdDayWise_Change();
            //}
            if (SessionUserType == "2")
            {
                NonAdminGetAttdDayWise_Change();
            }
            else
            {
                GetAttdDayWise_Change();
            }
        }
    }
    public void NonAdminGetAttdDayWise_Change()
    {


        string TableName = "";

        if (Status == "Pending")
        {
            TableName = "Employee_Mst_New_Emp";
        }

        else
        {
            TableName = "Employee_Mst";
        }


        AutoDTable.Columns.Add("SNo");
        AutoDTable.Columns.Add("Dept");
        AutoDTable.Columns.Add("Type");
        AutoDTable.Columns.Add("Shift");


        AutoDTable.Columns.Add("EmpCode");
        AutoDTable.Columns.Add("ExCode");
        AutoDTable.Columns.Add("Name");
        AutoDTable.Columns.Add("TimeIN");
        AutoDTable.Columns.Add("TimeOUT");
        AutoDTable.Columns.Add("MachineID");
        AutoDTable.Columns.Add("Category");
        AutoDTable.Columns.Add("SubCategory");
        AutoDTable.Columns.Add("TotalMIN");
        AutoDTable.Columns.Add("GrandTOT");
        AutoDTable.Columns.Add("ShiftDate");
        AutoDTable.Columns.Add("CompanyName");
        AutoDTable.Columns.Add("LocationName");

        DataCell.Columns.Add("CompanyName");
        DataCell.Columns.Add("LocationName");
        DataCell.Columns.Add("ShiftDate");
        DataCell.Columns.Add("SNo");
        DataCell.Columns.Add("Dept");
        DataCell.Columns.Add("Type");
        DataCell.Columns.Add("Shift");
        DataCell.Columns.Add("Category");
        DataCell.Columns.Add("SubCategory");
        DataCell.Columns.Add("EmpCode");
        DataCell.Columns.Add("ExCode");
        DataCell.Columns.Add("Name");
        DataCell.Columns.Add("TimeIN");
        DataCell.Columns.Add("TimeOUT");

        DataCell.Columns.Add("TotalMIN");
        DataCell.Columns.Add("GrandTOT");
        DataCell.Columns.Add("MachineID");
        DataCell.Columns.Add("PrepBy");
        DataCell.Columns.Add("PrepDate");






        DataTable mLocalDS = new DataTable();

        string ng = string.Format(Date, "MM-dd-yyyy");
        Datestr = Convert.ToDateTime(Date).AddDays(0).ToShortDateString();
        Datestr1 = Convert.ToDateTime(Date).AddDays(1).ToShortDateString();
        DateTime date1 = Convert.ToDateTime(ng);
        DateTime date2 = date1.AddDays(1);

        SSQL = "";
        SSQL = "Select shiftDesc,StartIN,EndIN,StartOUT,EndOUT,StartIN_Days,";
        SSQL = SSQL + " EndIN_Days,StartOUT_Days,EndOUT_Days ";
        SSQL = SSQL + " from Shift_Mst Where CompCode='" + SessionCcode + "'";
        SSQL = SSQL + " And LocCode='" + SessionLcode + "'";
        if (ShiftType1 != "ALL")
        {
            SSQL = SSQL + " And shiftDesc='" + ShiftType1 + "'";
        }
        SSQL = SSQL + " Order By shiftDesc";

        mLocalDS = objdata.RptEmployeeMultipleDetails(SSQL);

        if (SessionLcode == "UNIT I")
        {
            for (int i = 0; i < mLocalDS.Rows.Count; i++)
            {
                if (mLocalDS.Rows[i]["shiftDesc"] == "SHIFT10")
                {
                    mLocalDS.Rows[i]["StartIN_Days"] = -1;
                    mLocalDS.Rows[i]["EndIN_Days"] = 0;
                }
            }

        }

        int mStartINRow = 0;
        int mStartOUTRow = 0;

        for (int iTabRow = 0; iTabRow < mLocalDS.Rows.Count; iTabRow++)
        {
            if (AutoDTable.Rows.Count <= 1)
            {
                mStartOUTRow = 0;
            }
            else
            {
                mStartOUTRow = AutoDTable.Rows.Count - 1;
            }

            if (mLocalDS.Rows[iTabRow]["shiftDesc"].ToString() == "GENERAL")
            {
                ShiftType = "GENERAL";

            }
            else
            {
                ShiftType = "SHIFT";
            }
            string sIndays_str = mLocalDS.Rows[iTabRow]["StartIN_Days"].ToString();
            double sINdays = Convert.ToDouble(sIndays_str);
            string eIndays_str = mLocalDS.Rows[iTabRow]["EndIN_Days"].ToString();
            double eINdays = Convert.ToDouble(eIndays_str);
            if (ShiftType == "GENERAL")
            {
                SSQL = "";
                SSQL = "Select LT.MachineID,Min(LT.TimeIN) as [TimeIN] from LogTime_IN LT inner join " + TableName + " EM on EM.MachineID_Encrypt=LT.MachineID ";
                SSQL = SSQL + " Where LT.Compcode='" + SessionCcode + "'";
                SSQL = SSQL + " And LT.LocCode='" + SessionLcode + "' and EM.Eligible_PF='1' ";

                if (Division != "-Select-")
                {
                    SSQL = SSQL + " And EM.Division = '" + Division + "'";
                }



                SSQL = SSQL + "And LT.TimeIN >='" + date1.ToString("yyyy/MM/dd") + " " + "02:00" + "'";
                SSQL = SSQL + "And LT.TimeIN <='" + date2.ToString("yyyy/MM/dd") + " " + "02:00" + "'";
                SSQL = SSQL + " AND EM.ShiftType='" + ShiftType + "' And EM.Compcode='" + SessionCcode + "'";
                SSQL = SSQL + " And EM.LocCode='" + SessionLcode + "' And (EM.IsActive='Yes' or CONVERT(DATETIME,EM.DOR,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(Date).ToString("dd/MM/yyyy") + "',103))";
                SSQL = SSQL + " Group By LT.MachineID";
                SSQL = SSQL + " Order By Min(LT.TimeIN)";
            }

            else if (ShiftType == "SHIFT")
            {

                SSQL = "";
                SSQL = "Select LT.MachineID,Min(LT.TimeIN) as [TimeIN] from LogTime_IN LT inner join " + TableName + " EM on EM.MachineID_Encrypt=LT.MachineID ";
                SSQL = SSQL + " Where LT.Compcode='" + SessionCcode + "'";
                SSQL = SSQL + " And LT.LocCode='" + SessionLcode + "' and EM.Eligible_PF='1'";
                SSQL = SSQL + " And EM.Compcode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "' And (EM.IsActive='Yes' or CONVERT(DATETIME,EM.DOR,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(Date).ToString("dd/MM/yyyy") + "',103))";



                if (Division != "-Select-")
                {
                    SSQL = SSQL + " And EM.Division = '" + Division + "'";
                }


                SSQL = SSQL + "  And LT.TimeIN >='" + date1.AddDays(sINdays).ToString("yyyy/MM/dd") + " " + mLocalDS.Rows[iTabRow]["StartIN"].ToString() + "' ";
                SSQL = SSQL + " And LT.TimeIN <='" + date1.AddDays(eINdays).ToString("yyyy/MM/dd") + " " + mLocalDS.Rows[iTabRow]["EndIN"].ToString() + "'";

                SSQL = SSQL + " Group By LT.MachineID";
                SSQL = SSQL + " Order By Min(LT.TimeIN)";
            }
            else
            {
                SSQL = "";
                SSQL = "Select LT.MachineID,Min(LT.TimeIN) as [TimeIN] from LogTime_IN LT inner join " + TableName + " EM on EM.MachineID_Encrypt=LT.MachineID ";
                SSQL = SSQL + " Where LT.Compcode='" + SessionCcode + "'";
                SSQL = SSQL + " And LT.LocCode='" + SessionLcode + "'And (EM.IsActive='Yes' or CONVERT(DATETIME,EM.DOR,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(Date).ToString("dd/MM/yyyy") + "',103))";
                if (Division != "-Select-")
                {
                    SSQL = SSQL + " And EM.Division = '" + Division + "'";
                }


                SSQL = SSQL + "  And LT.TimeIN >='" + date1.AddDays(sINdays).ToString("yyyy/MM/dd") + " " + mLocalDS.Rows[iTabRow]["StartIN"].ToString() + "' ";
                SSQL = SSQL + " And LT.TimeIN <='" + date2.AddDays(eINdays).ToString("yyyy/MM/dd") + " " + mLocalDS.Rows[iTabRow]["EndIN"].ToString() + "' ";
                SSQL = SSQL + " AND EM.ShiftType='" + ShiftType + "' and EM.IsNonAdmin='2'";
                SSQL = SSQL + " Group By LT.MachineID Order By Min(LT.TimeIN)";
            }



            mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);



            if (mDataSet.Rows.Count > 0)
            {
                string MachineID;

                for (int iRow = 0; iRow < mDataSet.Rows.Count; iRow++)
                {

                    Boolean chkduplicate = false;
                    chkduplicate = false;

                    for (int ia = 0; ia < AutoDTable.Rows.Count; ia++)
                    {
                        string id = mDataSet.Rows[iRow]["MachineID"].ToString();

                        if (id == AutoDTable.Rows[ia][9].ToString())
                        {
                            chkduplicate = true;
                        }
                    }




                    MachineID = UTF8Decryption(mDataSet.Rows[iRow]["MachineID"].ToString());
                    // 'Get Employee Week OF DAY
                    DataTable DS_WH = new DataTable();
                    string Emp_WH_Day = "";
                    string DOJ_Date_Str = "";

                    SSQL = "Select * from " + TableName + " where MachineID='" + MachineID + "' And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    DS_WH = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (DS_WH.Rows.Count != 0)
                    {
                        Emp_WH_Day = DS_WH.Rows[0]["WeekOff"].ToString();
                        DOJ_Date_Str = DS_WH.Rows[0]["DOJ"].ToString();
                    }
                    else
                    {
                        Emp_WH_Day = "";
                    }

                    // 'Check Week off
                    DateTime CurrentDate = Convert.ToDateTime(Date.ToString());
                    string day = CurrentDate.DayOfWeek.ToString();
                    string MonthName = CurrentDate.ToString("dddd");
                    if (Emp_WH_Day.ToString() == MonthName.ToString())
                    {
                        ColumnName = "WH";
                    }

                    Date_Value_Str = string.Format(Date, "yyyy/MM/dd");


                    // 'check NFH
                    string qry_nfh = "Select NFHDate from NFH_Mst where NFHDate= CONVERT(DATETIME,'" + Convert.ToDateTime(Date).ToString("dd/MM/yyyy") + "',103) AND Form25_NFH_Present='No'";
                    MLocal_Day = objdata.RptEmployeeMultipleDetails(qry_nfh);




                    //'check NFH Form25_NFH_Present='Yes'

                    string qry_nfh1 = "Select NFHDate from NFH_Mst where NFHDate=CONVERT(DATETIME,'" + Convert.ToDateTime(Date).ToString("dd/MM/yyyy") + "',103) AND Form25_NFH_Present='Yes'";
                    MLocal_Day = objdata.RptEmployeeMultipleDetails(qry_nfh1);
                    if (MLocal_Day.Rows.Count > 0)
                    {
                        ColumnName = "NH-";
                    }
                    // 'Check SHIFT CHANGE TIME DETAILS
                    string Query = "";
                    DataTable Shift_Change_DS = new DataTable();

                    Query = "Select * from ShiftChange_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                    Query = Query + " And ShiftDate='" + Date + "' and Machine_Encrypt='" + mDataSet.Rows[iRow]["MachineID"].ToString() + "'";
                    Shift_Change_DS = objdata.RptEmployeeMultipleDetails(Query);
                    if (Shift_Change_DS.Rows.Count != 0)
                    {
                        if (ShiftType1 == "ALL" || ShiftType == Shift_Change_DS.Rows[0]["ShiftDesc"])
                        {

                        }
                        else
                        {
                            chkduplicate = true;
                        }

                    }

                    if (chkduplicate == false)
                    {
                        AutoDTable.NewRow();
                        AutoDTable.Rows.Add();

                        AutoDTable.Rows[AutoDTable.Rows.Count - 1][3] = mLocalDS.Rows[iTabRow]["ShiftDesc"].ToString();

                        if (ShiftType == "SHIFT")
                        {
                            string str = mDataSet.Rows[iRow][1].ToString();


                            if (ColumnName == "WH")
                            {
                                AutoDTable.Rows[AutoDTable.Rows.Count - 1][7] = "WH";
                            }
                            else if (ColumnName == "NH-")
                            {
                                AutoDTable.Rows[AutoDTable.Rows.Count - 1][7] = "NH-" + String.Format("{0:hh:mm tt}", mDataSet.Rows[iRow]["TimeIN"]);
                            }
                            else
                            {
                                AutoDTable.Rows[AutoDTable.Rows.Count - 1][7] = String.Format("{0:hh:mm tt}", mDataSet.Rows[iRow]["TimeIN"]);
                            }

                        }
                        else
                        {
                            if (ColumnName == "WH")
                            {
                                AutoDTable.Rows[AutoDTable.Rows.Count - 1][7] = "WH";
                            }
                            else if (ColumnName == "NH-")
                            {
                                AutoDTable.Rows[AutoDTable.Rows.Count - 1][7] = "NH-" + String.Format("{0:hh:mm tt}", mDataSet.Rows[iRow]["TimeIN"]);
                            }
                            else
                            {
                                AutoDTable.Rows[AutoDTable.Rows.Count - 1][7] = String.Format("{0:hh:mm tt}", mDataSet.Rows[iRow]["TimeIN"]);
                            }
                        }
                        MachineID = (UTF8Decryption(mDataSet.Rows[iRow]["MachineID"].ToString()));

                        AutoDTable.Rows[AutoDTable.Rows.Count - 1][9] = MachineID.ToString();


                    }
                    mStartINRow += 1;
                    ColumnName = "";
                }

            }


            SSQL = "";
            SSQL = "Select MachineID,Max(TimeOUT) as [TimeOUT] from LogTime_OUT ";


            SSQL = SSQL + " Where Compcode='" + SessionCcode + "'";
            SSQL = SSQL + " And LocCode='" + SessionLcode + "'";


            if (mLocalDS.Rows[iTabRow]["shiftDesc"] == "SHIFT1")
            {
                SSQL = SSQL + " and TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
                SSQL = SSQL + " and TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
            }
            else
            {
                SSQL = SSQL + " and TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
                SSQL = SSQL + " and TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
            }


            SSQL = SSQL + " Group By MachineID";
            SSQL = SSQL + " Order By Max(TimeOUT)";

            mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);
            string InMachine_IP = "";
            DataTable mLocalDS_out = new DataTable();
            long Random_No_Fixed = 1;

            for (int iRow2 = mStartOUTRow; iRow2 < AutoDTable.Rows.Count; iRow2++)
            {
                InMachine_IP = UTF8Encryption(AutoDTable.Rows[iRow2][9].ToString());


                SSQL = "Select MachineID,TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
                SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";

                //'Day Atten. Time Order by Change (Eveready MILL)
                if (AutoDTable.Rows[iRow2][3] == "SHIFT1")
                {
                    SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "08:00" + "'";
                    SSQL = SSQL + " And TimeOUT <='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "23:45" + "' Order by TimeOUT Asc";

                }
                else
                {
                    SSQL = SSQL + " and TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
                    SSQL = SSQL + " and TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00" + "' Order by TimeOUT Asc";

                }
                mLocalDS_out = objdata.RptEmployeeMultipleDetails(SSQL);


                if (mLocalDS_out.Rows.Count <= 0)
                {

                }
                else
                {
                    if (AutoDTable.Rows[iRow2][9].ToString() == UTF8Decryption(mLocalDS_out.Rows[0][0].ToString()))
                    {
                        AutoDTable.Rows[iRow2][8] = string.Format("{0:hh:mm tt}", mLocalDS_out.Rows[0][1]);
                    }

                }


                Time_IN_Str = "";
                Time_Out_Str = "";

                Date_Value_Str = string.Format(Date, "yyyy/MM/dd");
                if (SessionLcode == "UNIT I" && mLocalDS.Rows[iTabRow]["shiftDesc"] == "SHIFT10")
                {
                    SSQL = "Select TimeIN from LogTime_IN where MachineID='" + InMachine_IP + "'";
                    SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    SSQL = SSQL + " And TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "23:00' And TimeIN <='" + date1.AddDays(-1).ToString("yyyy/MM/dd") + " " + "00:59' Order by TimeIN ASC";

                }
                else
                {
                    SSQL = "Select TimeIN from LogTime_IN where MachineID='" + InMachine_IP + "'";
                    SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    SSQL = SSQL + " And TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";

                }

                mLocalDS_INTAB = objdata.RptEmployeeMultipleDetails(SSQL);

                DateTime InTime_Check = new DateTime();
                DateTime InToTime_Check = new DateTime();
                TimeSpan InTime_TimeSpan;
                string From_Time_Str = "";
                string To_Time_Str = "";
                DataTable DS_Time = new DataTable();
                DataTable DS_InTime = new DataTable();
                string Final_InTime = "";
                string Final_OutTime = "";
                string Final_Shift = "";
                DataTable Shift_DS = new DataTable();
                int K = 0;
                Boolean Shift_Check_blb = false;




                //'Get Employee Week OF DAY
                string MachineID;
                MachineID = (UTF8Decryption(InMachine_IP));

                //  ' Agin Calculate Week Off because ColumnName = "" so
                DataTable DS_WH = new DataTable();
                string Emp_WH_Day = "";
                string DOJ_Date_Str = "";

                SSQL = "Select * from " + TableName + " where MachineID='" + MachineID + "' And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                DS_WH = objdata.RptEmployeeMultipleDetails(SSQL);
                if (DS_WH.Rows.Count != 0)
                {
                    Emp_WH_Day = DS_WH.Rows[0]["WeekOff"].ToString();
                    DOJ_Date_Str = DS_WH.Rows[0]["DOJ"].ToString();
                }
                else
                {
                    Emp_WH_Day = "";
                }

                DateTime CurrentDate = Convert.ToDateTime(Date.ToString());
                string day = CurrentDate.DayOfWeek.ToString();
                string MonthName = CurrentDate.ToString("dddd");
                if (Emp_WH_Day.ToString() == MonthName.ToString())
                {
                    ColumnName = "WH";
                }

                // 'check NFH Form25_NFH_Present='Yes'

                string qry_nfh1 = "Select NFHDate from NFH_Mst where NFHDate=CONVERT(DATETIME,'" + Convert.ToDateTime(Date).ToString("dd/MM/yyyy") + "',103) AND Form25_NFH_Present='Yes'";
                MLocal_Day = objdata.RptEmployeeMultipleDetails(qry_nfh1);
                if (MLocal_Day.ToString() == MonthName.ToString())
                {
                    ColumnName = "NH-";
                }

                Date_Value_Str = string.Format(Date, "yyyy/MM/dd");
                if (ColumnName == "WH")
                {
                    AutoDTable.Rows[iRow2][8] = ColumnName;
                    AutoDTable.Rows[iRow2][12] = ColumnName;
                    AutoDTable.Rows[iRow2][13] = ColumnName;

                }
                else
                {
                    if (AutoDTable.Rows[iRow2][3] == "SHIFT1" || AutoDTable.Rows[iRow2][3] == "SHIFT2")
                    {
                        if (ColumnName == "NH-")
                        {
                            string[] NHandTime = AutoDTable.Rows[iRow2][7].ToString().Split('-');
                            Time_IN_Get = NHandTime[1];
                            InTime_Check = Convert.ToDateTime(Time_IN_Get);

                        }
                        else
                        {
                            InTime_Check = Convert.ToDateTime(AutoDTable.Rows[iRow2][7].ToString());
                        }


                        InToTime_Check = InTime_Check.AddHours(2);

                        string InTime_Check_str = string.Format("{0:hh\\:mm\\:ss}", InTime_Check);
                        string InToTime_Check_str = string.Format("{0:hh\\:mm\\:ss}", InToTime_Check);
                        InTime_TimeSpan = TimeSpan.Parse(InTime_Check_str);
                        From_Time_Str = InTime_TimeSpan.Hours + ":" + InTime_TimeSpan.Minutes;
                        InTime_TimeSpan = TimeSpan.Parse(InToTime_Check_str);
                        To_Time_Str = InTime_TimeSpan.Hours + ":" + InTime_TimeSpan.Minutes;



                        //'Two Hours OutTime Check
                        SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
                        SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                        SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + From_Time_Str + "' And TimeOUT <='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + To_Time_Str + "' Order by TimeOUT Asc";
                        DS_Time = objdata.RptEmployeeMultipleDetails(SSQL);

                        if (DS_Time.Rows.Count != 0)
                        {
                            SSQL = "Select TimeIN from LogTime_IN where MachineID='" + InMachine_IP + "'";
                            SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                            SSQL = SSQL + " And TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + To_Time_Str + "' And TimeIN <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";
                            DS_InTime = objdata.RptEmployeeMultipleDetails(SSQL);
                            if (DS_InTime.Rows.Count != 0)
                            {
                                Final_InTime = DS_InTime.Rows[0][0].ToString();


                                //'Check With IN Time Shift
                                SSQL = "Select * from Shift_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And ShiftDesc like '%SHIFT%'";
                                Shift_DS = objdata.RptEmployeeMultipleDetails(SSQL);
                                Shift_Check_blb = false;
                                for (int k = 0; k < Shift_DS.Rows.Count; k++)
                                {
                                    string a = Shift_DS.Rows[k]["StartIN_Days"].ToString();
                                    int b = Convert.ToInt16(a.ToString());
                                    Shift_Start_Time = date1.AddDays(b).ToString("yyyy/MM/dd") + " " + Shift_DS.Rows[k]["StartIN"].ToString();
                                    string a1 = Shift_DS.Rows[k]["EndIN_Days"].ToString();
                                    int b1 = Convert.ToInt16(a1.ToString());
                                    Shift_End_Time = date1.AddDays(b1).ToString("yyyy/MM/dd") + " " + Shift_DS.Rows[k]["EndIN"].ToString();

                                    ShiftdateStartIN = Convert.ToDateTime(Shift_Start_Time.ToString());
                                    ShiftdateEndIN = Convert.ToDateTime(Shift_End_Time.ToString());

                                    EmpdateIN = Convert.ToDateTime(Final_InTime.ToString());
                                    if (EmpdateIN >= ShiftdateStartIN && EmpdateIN <= ShiftdateEndIN)
                                    {
                                        Final_Shift = Shift_DS.Rows[k]["ShiftDesc"].ToString();
                                        Shift_Check_blb = true;
                                    }
                                }
                                if (Shift_Check_blb == true)
                                {
                                    //'IN Time And Shift Update
                                    AutoDTable.Rows[iRow2][3] = Final_Shift.ToString();
                                    if (ColumnName == "NH-")
                                    {
                                        AutoDTable.Rows[iRow2][7] = "NH-" + string.Format("{0:hh:mm tt}", Final_InTime);
                                    }
                                    else
                                    {
                                        AutoDTable.Rows[iRow2][7] = string.Format("{0:hh:mm tt}", Final_InTime);
                                    }
                                    // 'IN Time Query Update
                                    SSQL = "Select TimeIN from LogTime_IN where MachineID='" + InMachine_IP + "'";
                                    SSQL = SSQL + " And Compcode='" + SessionLcode + "' And LocCode='" + SessionLcode + "'";
                                    SSQL = SSQL + " And TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + To_Time_Str + "' And TimeIN <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";
                                    mLocalDS_INTAB = objdata.RptEmployeeMultipleDetails(SSQL);

                                    // 'Out Time Query Update
                                    if (Final_Shift == "SHIFT2")
                                    {
                                        SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
                                        SSQL = SSQL + " And Compcode='" + SessionLcode + "' And LocCode='" + SessionLcode + "'";
                                        SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "13:00' And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "03:00' Order by TimeOUT Asc";

                                    }
                                    else
                                    {
                                        SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
                                        SSQL = SSQL + " And Compcode='" + SessionLcode + "' And LocCode='" + SessionLcode + "'";
                                        SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "17:40' And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT Asc";

                                    }

                                    DS_Time = objdata.RptEmployeeMultipleDetails(SSQL);

                                    if (DS_Time.Rows.Count != 0)
                                    {
                                        if (ColumnName == "NH-")
                                        {
                                            AutoDTable.Rows[iRow2][8] = "NH-" + string.Format("{0:hh:mm tt}", (DS_Time.Rows[0][0]));
                                        }
                                        else
                                        {
                                            AutoDTable.Rows[iRow2][8] = String.Format("{0:hh:mm tt}", DS_Time.Rows[0][0]);
                                        }

                                    }

                                }
                                else
                                {
                                    if (AutoDTable.Rows[iRow2][3] == "SHIFT1")
                                    {
                                        SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
                                        SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                        SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "06:00' And TimeOUT <='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "13:00' Order by TimeOUT Asc";

                                    }
                                    else
                                    {
                                        SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
                                        SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                        SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "13:00' And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "03:00' Order by TimeOUT Asc";

                                    }
                                    DS_Time = objdata.RptEmployeeMultipleDetails(SSQL);
                                    // 'Out Time Update
                                    if (DS_Time.Rows.Count != 0)
                                    {
                                        if (ColumnName == "NH-")
                                        {
                                            AutoDTable.Rows[iRow2][8] = "NH-" + string.Format("{0:hh:mm tt}", (DS_Time.Rows[0][0]));
                                        }
                                        else
                                        {
                                            AutoDTable.Rows[iRow2][8] = String.Format("{0:hh:mm tt}", DS_Time.Rows[0][0]);
                                        }
                                    }

                                }
                            }
                            else
                            {
                                if (AutoDTable.Rows[iRow2][3] == "SHIFT1")
                                {
                                    SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
                                    SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                    SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "06:00' And TimeOUT <='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "14:00' Order by TimeOUT Asc";

                                }
                                else
                                {
                                    SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
                                    SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                    SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "13:00' And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "03:00' Order by TimeOUT Asc";

                                }


                                DS_Time = objdata.RptEmployeeMultipleDetails(SSQL);
                                //'Out Time Update
                                if (DS_Time.Rows.Count != 0)
                                {
                                    AutoDTable.Rows[iRow2][8] = String.Format("{0:hh:mm tt}", DS_Time.Rows[0][0]);
                                }

                            }

                        }
                        else
                        {
                            SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
                            SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                            SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT Asc";

                        }

                    }


                    else
                    {
                        SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + InMachine_IP + "'";
                        SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                        SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT Asc";

                    }
                    mLocalDS_OUTTAB = objdata.RptEmployeeMultipleDetails(SSQL);

                    string Emp_Total_Work_Time_1 = "00:00";
                    if (mLocalDS_INTAB.Rows.Count > 1)
                    {
                        for (int tin = 0; tin < mLocalDS_INTAB.Rows.Count; tin++)
                        {
                            Time_IN_Str = mLocalDS_INTAB.Rows[tin][0].ToString();
                            if (mLocalDS_OUTTAB.Rows.Count > tin)
                            {
                                Time_Out_Str = mLocalDS_OUTTAB.Rows[tin][0].ToString();
                            }
                            else if (mLocalDS_OUTTAB.Rows.Count > mLocalDS_INTAB.Rows.Count)
                            {
                                Time_Out_Str = mLocalDS_OUTTAB.Rows[mLocalDS_OUTTAB.Rows.Count - 1][0].ToString();
                            }

                            else
                            {
                                Time_Out_Str = "";
                            }
                            TimeSpan ts4;
                            ts4 = Convert.ToDateTime(String.Format("{0:hh:mm tt}", Emp_Total_Work_Time_1)).TimeOfDay;

                            if (mLocalDS.Rows.Count <= 0)
                            {
                                Time_IN_Str = "";
                            }
                            else
                            {
                                Time_IN_Str = mLocalDS_INTAB.Rows[tin][0].ToString();
                            }
                            if (Time_IN_Str == "" || Time_Out_Str == "")
                            {
                                time_Check_dbl = time_Check_dbl;
                            }
                            else
                            {
                                DateTime date3 = Convert.ToDateTime(Time_IN_Str);
                                DateTime date4 = Convert.ToDateTime(Time_Out_Str);
                                TimeSpan ts1;
                                ts1 = date4.Subtract(date3);
                                Total_Time_get = ts1.Hours.ToString();
                                Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;

                                string[] Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }
                                if (Left_Val(Total_Time_get, 1) == "-")
                                {
                                    date4 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                                    ts1 = date4.Subtract(date3);
                                    ts1 = date4.Subtract(date3);
                                    ts4 = ts4.Add(ts1);
                                    Total_Time_get = ts1.Hours.ToString();
                                    time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                    Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                    Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');


                                    if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    {

                                        Emp_Total_Work_Time_1 = "00:00";
                                    }
                                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                    {

                                        Emp_Total_Work_Time_1 = "00:00";
                                    }

                                }

                                else
                                {


                                    ts4 = ts4.Add(ts1);
                                    Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                    Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                    if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    {
                                        Emp_Total_Work_Time_1 = "00:00";
                                    }
                                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                    {
                                        Emp_Total_Work_Time_1 = "00:00";
                                        time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                    }
                                }
                            }

                            Time_Split_Str = Emp_Total_Work_Time_1.Split(':');

                            if (ColumnName == "NH-")
                            {
                                string[] NHandTime = AutoDTable.Rows[iRow2][7].ToString().Split('-');
                                Time_IN_Get = NHandTime[1];
                            }
                            else
                            {
                                Time_IN_Get = AutoDTable.Rows[iRow2][7].ToString();
                            }
                            Time_Out_Update = "";
                            Totol_Hours_Check = Convert.ToDouble(Time_Split_Str[0] + "." + Time_Split_Str[1]);
                            if (Totol_Hours_Check_1 < Totol_Hours_Check)
                            {
                                string GetRndpm = GetRandom(Convert.ToInt32(Random_No_Fixed), 15);
                                Random_No_Get = Convert.ToInt64(GetRndpm);
                                DateTime NewDT = Convert.ToDateTime(Time_IN_Get);

                                NewDT = NewDT.AddHours(8);//'It adds 8 hours from the Time IN Datetime.
                                NewDT = NewDT.AddMinutes(Random_No_Get); //'It adds Random Minutes from the Time IN datetime.
                                //  'Time_Out_Update = Format((Convert.ToDateTime(NewDT.ToString())), "dd/MM/yyyy HH:mm:ss")
                                Time_Out_Update = String.Format("{0:hh:mm tt}", NewDT);
                                Emp_Total_Work_Time_1 = "08" + ":" + Random_No_Get;

                                if (ColumnName == "NH-")
                                {
                                    AutoDTable.Rows[iRow2][8] = "NH-" + Time_Out_Update;
                                }
                                else
                                {
                                    AutoDTable.Rows[iRow2][8] = Time_Out_Update;
                                }

                            }


                            if (ColumnName == "NH-")
                            {
                                AutoDTable.Rows[iRow2][13] = " " + Emp_Total_Work_Time_1;

                            }
                            else
                            {
                                AutoDTable.Rows[iRow2][13] = Emp_Total_Work_Time_1;
                            }




                        }

                    }
                    else
                    {
                        TimeSpan ts4;
                        ts4 = Convert.ToDateTime(String.Format("{0:h:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
                        if (mLocalDS_INTAB.Rows.Count <= 0)
                        {
                            Time_IN_Str = "";
                        }
                        else
                        {
                            Time_IN_Str = mLocalDS_INTAB.Rows[0][0].ToString();
                        }

                        for (int tout = 0; tout < mLocalDS_OUTTAB.Rows.Count; tout++)
                        {
                            if (mLocalDS_OUTTAB.Rows.Count <= 0)
                            {
                                Time_Out_Str = "";
                            }
                            else
                            {
                                Time_Out_Str = mLocalDS_OUTTAB.Rows[0][0].ToString();
                            }

                        }
                        //'Emp_Total_Work_Time
                        if (Time_IN_Str == "" || Time_Out_Str == "")
                        {
                            time_Check_dbl = 0;
                        }
                        else
                        {
                            DateTime date3 = System.Convert.ToDateTime(Time_IN_Str);
                            DateTime date4 = System.Convert.ToDateTime(Time_Out_Str);
                            TimeSpan ts1;

                            ts1 = date4.Subtract(date3);
                            ts1 = date4.Subtract(date3);
                            Total_Time_get = ts1.Hours.ToString(); //'& ":" & Trim(ts.Minutes)


                            if (Left_Val(Total_Time_get, 1) == "-")
                            {
                                date4 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                                ts1 = date4.Subtract(date3);
                                ts1 = date4.Subtract(date3);
                                ts4 = ts4.Add(ts1);
                                Total_Time_get = ts1.Hours.ToString();
                                Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                {

                                    Emp_Total_Work_Time_1 = "00:00";
                                }
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                {

                                    Emp_Total_Work_Time_1 = "00:00";
                                }
                            }
                            else
                            {
                                ts4 = ts4.Add(ts1);
                                Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                    time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                }

                            }
                            Time_Split_Str = Emp_Total_Work_Time_1.Split(':');
                            if (ColumnName == "NH-")
                            {
                                string[] NHandTime = AutoDTable.Rows[iRow2][7].ToString().Split('-');
                                Time_IN_Get = NHandTime[1];
                            }
                            else
                            {
                                Time_IN_Get = AutoDTable.Rows[iRow2][7].ToString();
                            }
                            Time_Out_Update = "";
                            Totol_Hours_Check = Convert.ToDouble(Time_Split_Str[0] + "." + Time_Split_Str[1]);
                            if (Totol_Hours_Check_1 < Totol_Hours_Check)
                            {
                                //'Update Time Out And Total
                                string GetRndpm = GetRandom(Convert.ToInt32(Random_No_Fixed), 15);
                                Random_No_Get = Convert.ToInt64(GetRndpm);
                                //Random_No_Get = GetRandom(Random_No_Fixed, 15);
                                if (ColumnName == "NH-")
                                {
                                    string[] NHandTime = AutoDTable.Rows[iRow2][7].ToString().Split('-');
                                    Time_IN_Get = NHandTime[1];
                                }
                                else
                                {
                                    Time_IN_Get = AutoDTable.Rows[iRow2][7].ToString();
                                }
                                if (ColumnName != "WH")
                                {
                                    if (ColumnName != "NH")
                                    {
                                        DateTime NewDT = Convert.ToDateTime(Time_IN_Get);

                                        NewDT = NewDT.AddHours(8); //'It adds 8 hours from the Time IN Datetime.
                                        NewDT = NewDT.AddMinutes(Random_No_Get);//'It adds Random Minutes from the Time IN datetime.
                                        //  'Time_Out_Update = Format((Convert.ToDateTime(NewDT.ToString())), "dd/MM/yyyy HH:mm:ss")
                                        Time_Out_Update = String.Format("{0:hh:mm tt}", NewDT);
                                        Emp_Total_Work_Time_1 = "08" + ":" + Random_No_Get;

                                    }
                                }
                                if (ColumnName == "NH-")
                                {
                                    AutoDTable.Rows[iRow2][8] = "NH-" + Time_Out_Update;

                                }
                                else
                                {
                                    AutoDTable.Rows[iRow2][8] = Time_Out_Update;
                                }



                            }
                            if (ColumnName == "NH-")
                            {
                                AutoDTable.Rows[iRow2][12] = "NH-" + Emp_Total_Work_Time_1;
                            }
                            else
                            {
                                AutoDTable.Rows[iRow2][12] = Emp_Total_Work_Time_1;
                            }



                        }

                    }
                    if (ColumnName == "NH-")
                    {
                        AutoDTable.Rows[iRow2][13] = "NH-" + Emp_Total_Work_Time_1;
                    }
                    else
                    {
                        AutoDTable.Rows[iRow2][13] = Emp_Total_Work_Time_1;
                    }

                }



                //' CODE FOR ADOLESCENT SHIFT1
                DataTable DS_WH1 = new DataTable();
                Adolescent_Shift = "";
                string[] Adole_Time;
                int Adole_NoTime;


                SSQL = "Select * from " + TableName + " where MachineID='" + AutoDTable.Rows[iRow2][9].ToString() + "' And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                DS_WH1 = objdata.RptEmployeeMultipleDetails(SSQL);
                DateTime birthday = new DateTime();
                DateTime today = DateTime.Now;
                string DOB = DS_WH1.Rows[0]["BirthDate"].ToString();

                birthday = Convert.ToDateTime(DOB);
                // 'Dim Years = today.Year - birthday.Year 

                //date1 = Convert.ToDateTime(FromDate);
                // string dd =string.Format("{dd/MM/yyyy}",TxtTo.Text);
                //date2 = Convert.ToDateTime(ToDate);
                //int dayCount = (int)((date2 - date1).TotalDays);

                //int Age_Years =DateDiff(DateInterval.Year, birthday, ) - 1;

                int Age_Years = (today.Year - birthday.Year) - 1;
                //int Age_Months = DateDiff(DateInterval.Month, birthday, today) % 12;
                int Age_Months = 0;
                if (birthday.Month > today.Month)
                {
                    Age_Months = (birthday.Month - today.Month) % 12;
                }
                else
                {
                    Age_Months = (today.Month - birthday.Month) % 12;
                }
                string Years_Check = Age_Years + "." + Age_Months.ToString();
                double Years = Convert.ToDouble(Years_Check);
                if (Years <= 18.0)
                {
                    Adolescent_Shift = "SHIFT1";
                    AutoDTable.Rows[iRow2][3] = "SHIFT1";
                }
                if (ColumnName != "WH")
                {
                    if (Adolescent_Shift == "SHIFT1")
                    {
                        if (AutoDTable.Rows[iRow2][8].ToString() == "")
                        {
                            SSQL = "Select * from Shift_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And ShiftDesc = 'SHIFT1'";
                            Shift_DS = objdata.RptEmployeeMultipleDetails(SSQL);
                            Shift_Check_blb = false;

                            for (int k = 0; k < Shift_DS.Rows.Count; k++)
                            {
                                string a = Shift_DS.Rows[k]["StartIN_Days"].ToString();
                                int b = Convert.ToInt16(a.ToString());
                                Shift_Start_Time = date1.AddDays(b).ToString("yyyy/MM/dd") + " " + Shift_DS.Rows[k]["StartIN"].ToString();
                                string a1 = Shift_DS.Rows[k]["EndIN_Days"].ToString();
                                int b1 = Convert.ToInt16(a1.ToString());
                                Shift_End_Time = date1.AddDays(b1).ToString("yyyy/MM/dd") + " " + Shift_DS.Rows[k]["EndIN"].ToString();

                                ShiftdateStartIN = System.Convert.ToDateTime(Shift_Start_Time);
                                ShiftdateEndIN = System.Convert.ToDateTime(Shift_End_Time);
                                if (ColumnName == "NH-")
                                {
                                    string[] NHandTime = AutoDTable.Rows[iRow2][7].ToString().Split('-');
                                    Time_IN_Get = NHandTime[1];
                                }
                                else
                                {
                                    Time_IN_Get = AutoDTable.Rows[iRow2][7].ToString();
                                }
                                EmpdateIN = System.Convert.ToDateTime(Time_IN_Get);

                                if (EmpdateIN >= ShiftdateStartIN && EmpdateIN <= ShiftdateEndIN)
                                {
                                    if (ColumnName == "NH-")
                                    {
                                        AutoDTable.Rows[iRow2][7] = "NH-" + string.Format("{0:hh:mm tt}", Time_IN_Get);
                                    }
                                    else
                                    {
                                        AutoDTable.Rows[iRow2][7] = string.Format("{0:hh:mm tt}", Time_IN_Get);
                                    }

                                }
                                else
                                {
                                    string GetRndpm = GetRandom(Convert.ToInt32(Random_No_Fixed), 15);
                                    Random_No_Get = Convert.ToInt64(GetRndpm);
                                    //Random_No_Get = GetRandom(Random_No_Fixed, 15);
                                    Time_IN_Get = "08" + ":" + Random_No_Get;

                                    if (ColumnName == "NH-")
                                    {
                                        AutoDTable.Rows[iRow2][7] = "NH-" + string.Format("{0:hh:mm tt}", Time_IN_Get);
                                    }
                                    else
                                    {
                                        AutoDTable.Rows[iRow2][7] = string.Format("{0:hh:mm tt}", Time_IN_Get);
                                    }



                                }
                            }

                        }
                        else
                        {
                            if (ColumnName == "NH-")
                            {

                                string[] stringSeparators = new string[] { "NH-" };
                                string[] Totaltime = AutoDTable.Rows[iRow2][13].ToString().Split(stringSeparators, StringSplitOptions.None);
                                //string[] Totaltime = AutoDTable.Rows[iRow2][13].ToString().Split(new String() {"NH-"}, StringSplitOptions.RemoveEmptyEntries);
                                //.Split("NH-");.Split(New String() {"NH-"}, StringSplitOptions.RemoveEmptyEntries)
                                Adole_Time = Totaltime[0].Split(':');
                            }
                            else
                            {
                                Adole_Time = AutoDTable.Rows[iRow2][13].ToString().Split(':');
                                if (Adole_Time[0] == "")
                                {
                                    Adole_Time[0] = "0";
                                    //Adole_Time[1] = "0";
                                }
                            }
                            Adole_NoTime = Convert.ToInt16(Adole_Time[0]);
                            if (Adole_NoTime >= 8)
                            {
                                SSQL = "Select * from Shift_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And ShiftDesc = 'SHIFT1'";
                                Shift_DS = objdata.RptEmployeeMultipleDetails(SSQL);

                                Shift_Check_blb = false;
                                for (int k = 0; k < Shift_DS.Rows.Count; k++)
                                {
                                    string a = Shift_DS.Rows[k]["StartIN_Days"].ToString();
                                    int b = Convert.ToInt16(a.ToString());
                                    Shift_Start_Time = date1.AddDays(b).ToString("yyyy/MM/dd") + " " + Shift_DS.Rows[k]["StartIN"].ToString();
                                    string a1 = Shift_DS.Rows[k]["EndIN_Days"].ToString();
                                    int b1 = Convert.ToInt16(a1.ToString());
                                    Shift_End_Time = date1.AddDays(b1).ToString("yyyy/MM/dd") + " " + Shift_DS.Rows[k]["EndIN"].ToString();

                                    ShiftdateStartIN = System.Convert.ToDateTime(Shift_Start_Time);
                                    ShiftdateEndIN = System.Convert.ToDateTime(Shift_End_Time);
                                    if (ColumnName == "NH-")
                                    {
                                        string[] NHandTime = AutoDTable.Rows[iRow2][7].ToString().Split('-');
                                        Time_IN_Get = NHandTime[1];
                                    }
                                    else
                                    {
                                        Time_IN_Get = AutoDTable.Rows[iRow2][7].ToString();
                                    }

                                    EmpdateIN = System.Convert.ToDateTime(Time_IN_Get);


                                    if (EmpdateIN >= ShiftdateStartIN && EmpdateIN <= ShiftdateEndIN)
                                    {
                                        if (ColumnName == "NH-")
                                        {
                                            AutoDTable.Rows[iRow2][7] = "NH-" + string.Format("{0:hh:mm tt}", Time_IN_Get);
                                        }
                                        else
                                        {
                                            AutoDTable.Rows[iRow2][7] = string.Format("{0:hh:mm tt}", Time_IN_Get);
                                        }

                                    }
                                    else
                                    {
                                        string GetRndpm = GetRandom(Convert.ToInt32(Random_No_Fixed), 15);
                                        Random_No_Get = Convert.ToInt64(GetRndpm);
                                        //Random_No_Get = GetRandom(Random_No_Fixed, 15);
                                        Time_IN_Get = "08" + ":" + Random_No_Get;

                                        if (ColumnName == "NH-")
                                        {
                                            AutoDTable.Rows[iRow2][7] = "NH-" + string.Format("{0:hh:mm tt}", Time_IN_Get);
                                        }
                                        else
                                        {
                                            AutoDTable.Rows[iRow2][7] = string.Format("{0:hh:mm tt}", Time_IN_Get);
                                        }
                                    }



                                }
                                if (ColumnName == "NH-")
                                {
                                    string[] NHandTime = AutoDTable.Rows[iRow2][7].ToString().Split('-');
                                    Time_IN_Get = NHandTime[1];
                                }
                                else
                                {
                                    Time_IN_Get = AutoDTable.Rows[iRow2][7].ToString();
                                }
                                DateTime NewDT = Convert.ToDateTime(Time_IN_Get);
                                string Emp_Total_Work_Time_11;


                                NewDT = NewDT.AddHours(8); //'It adds 8 hours from the Time IN Datetime.
                                NewDT = NewDT.AddMinutes(Random_No_Get); //'It adds Random Minutes from the Time IN datetime.

                                Time_Out_Update = String.Format("{0:hh:mm tt}", NewDT);
                                Emp_Total_Work_Time_11 = "08" + ":" + Random_No_Get;
                                if (ColumnName == "NH-")
                                {
                                    AutoDTable.Rows[iRow2][8] = "NH-" + Time_Out_Update;
                                }
                                else
                                {
                                    AutoDTable.Rows[iRow2][8] = Time_Out_Update;
                                }
                                if (ColumnName == "NH-")
                                {
                                    AutoDTable.Rows[iRow2][13] = "NH-" + Emp_Total_Work_Time_11;
                                }
                                else
                                {
                                    AutoDTable.Rows[iRow2][13] = Emp_Total_Work_Time_11;
                                }


                            }
                            else
                            {


                                SSQL = "Select * from Shift_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And ShiftDesc = 'SHIFT1'";
                                Shift_DS = objdata.RptEmployeeMultipleDetails(SSQL);

                                Shift_Check_blb = false;
                                for (int k = 0; k < Shift_DS.Rows.Count; k++)
                                {
                                    string a = Shift_DS.Rows[k]["StartIN_Days"].ToString();
                                    int b = Convert.ToInt16(a.ToString());
                                    Shift_Start_Time = date1.AddDays(b).ToString("yyyy/MM/dd") + " " + Shift_DS.Rows[k]["StartIN"].ToString();
                                    string a1 = Shift_DS.Rows[k]["EndIN_Days"].ToString();
                                    int b1 = Convert.ToInt16(a1.ToString());
                                    Shift_End_Time = date1.AddDays(b1).ToString("yyyy/MM/dd") + " " + Shift_DS.Rows[k]["EndIN"].ToString();

                                    ShiftdateStartIN = System.Convert.ToDateTime(Shift_Start_Time);
                                    ShiftdateEndIN = System.Convert.ToDateTime(Shift_End_Time);

                                    if (ColumnName == "NH-")
                                    {
                                        string[] NHandTime = AutoDTable.Rows[iRow2][7].ToString().Split('-');
                                        Time_IN_Get = NHandTime[1];
                                    }
                                    else
                                    {
                                        Time_IN_Get = AutoDTable.Rows[iRow2][7].ToString();
                                    }
                                    EmpdateIN = System.Convert.ToDateTime(Time_IN_Get);

                                    if (EmpdateIN >= ShiftdateStartIN && EmpdateIN <= ShiftdateEndIN)
                                    {
                                        if (ColumnName == "NH-")
                                        {
                                            AutoDTable.Rows[iRow2][7] = "NH-" + string.Format("{0:hh:mm tt}", Time_IN_Get);
                                        }
                                        else
                                        {
                                            AutoDTable.Rows[iRow2][7] = string.Format("{0:hh:mm tt}", Time_IN_Get);
                                        }

                                    }

                                    else
                                    {

                                        string GetRndpm = GetRandom(Convert.ToInt32(Random_No_Fixed), 15);
                                        Random_No_Get = Convert.ToInt64(GetRndpm);
                                        //Random_No_Get = GetRandom(Random_No_Fixed, 15);
                                        Time_IN_Get = "08" + ":" + Random_No_Get;

                                        if (ColumnName == "NH-")
                                        {
                                            AutoDTable.Rows[iRow2][7] = "NH-" + string.Format("{0:hh:mm tt}", Time_IN_Get);
                                        }
                                        else
                                        {
                                            AutoDTable.Rows[iRow2][7] = string.Format("{0:hh:mm tt}", Time_IN_Get);
                                        }
                                    }



                                }
                                if (ColumnName == "NH-")
                                {
                                    string[] NHandTime = AutoDTable.Rows[iRow2][7].ToString().Split('-');
                                    Time_IN_Get = NHandTime[1];
                                }
                                else
                                {
                                    Time_IN_Get = AutoDTable.Rows[iRow2][7].ToString();
                                }

                                Adole_NoTime = Convert.ToInt16(Adole_Time[0]);
                                int Adole_NoTime1 = 0;
                                if (Adole_Time.Length > 1)
                                {
                                    Adole_NoTime1 = Convert.ToInt16(Adole_Time[1]);
                                }
                                else
                                {
                                    Adole_NoTime1 = 0;
                                }


                                DateTime NewDT = Convert.ToDateTime(Time_IN_Get);

                                string Emp_Total_Work_Time_11;


                                NewDT = NewDT.AddHours(Adole_NoTime); //' 0 It adds 4 hours from the Time IN Datetime.
                                NewDT = NewDT.AddMinutes(Adole_NoTime1); //56 It adds Random Minutes from the Time IN datetime.

                                Time_Out_Update = String.Format("{0:hh:mm tt}", NewDT);

                                DateTime MDate1 = Convert.ToDateTime(Time_IN_Get.ToString());
                                DateTime MDate2 = Convert.ToDateTime(Time_Out_Update.ToString());

                                TimeSpan ts1;

                                ts1 = MDate2.Subtract(MDate1);

                                Emp_Total_Work_Time_11 = ts1.Hours.ToString() + ":" + ts1.Minutes.ToString();

                                if (ColumnName == "NH-")
                                {
                                    AutoDTable.Rows[iRow2][8] = "NH-" + Time_Out_Update;
                                }
                                else
                                {
                                    AutoDTable.Rows[iRow2][8] = Time_Out_Update;
                                }
                                if (ColumnName == "NH-")
                                {
                                    AutoDTable.Rows[iRow2][13] = "NH-" + Emp_Total_Work_Time_11;
                                }
                                else
                                {
                                    AutoDTable.Rows[iRow2][13] = Emp_Total_Work_Time_11;
                                }
                                //'end of 4 hours
                            }



                        }

                    }
                }

                //'ADOLSCENT SHIFT 1 END


                ColumnName = "";
            }


        }

        DataTable mEmployeeDS = new DataTable();

        for (int iRow2 = 0; iRow2 < AutoDTable.Rows.Count; iRow2++)
        {

            SSQL = "";
            SSQL = "select Distinct isnull(EM.MachineID,'') as [MachineID],isnull(EM.DeptName,'') as [DeptName]";
            SSQL = SSQL + ",isnull(EM.TypeName,'') as [TypeName],EM.EmpNo,isnull(EM.ExistingCode,'') as [ExistingCode]";
            SSQL = SSQL + ",isnull(EM.FirstName,'') + '.'+ isnull(EM.MiddleInitial,'') as [FirstName]";
            SSQL = SSQL + ",isnull(EM.CatName,'') as [CatName], isnull(EM.SubCatName,'') as [SubCatName]";
            SSQL = SSQL + ",DM.DeptCode from " + TableName + " EM inner Join Department_Mst DM on DM.DeptName=EM.DeptName Where EM.Compcode='" + SessionCcode + "'";
            SSQL = SSQL + " And EM.LocCode='" + SessionLcode + "'and EM.Eligible_PF='1' and EM.MachineID='" + AutoDTable.Rows[iRow2]["MachineID"].ToString() + "'  And (EM.IsActive='Yes' or CONVERT(DATETIME,EM.DOR,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(Date).ToString("dd/MM/yyyy") + "',103))";
            if (Division != "-Select-")
            {
                SSQL = SSQL + " And EM.Division = '" + Division + "'";
            }

            SSQL = SSQL + " order by DM.DeptCode Asc";

            mEmployeeDS = objdata.RptEmployeeMultipleDetails(SSQL);
            if (mEmployeeDS.Rows.Count > 0)
            {
                for (int iRow1 = 0; iRow1 < mEmployeeDS.Rows.Count; iRow1++)
                {

                    AutoDTable.Rows[iRow2][1] = mEmployeeDS.Rows[iRow1]["DeptName"];
                    AutoDTable.Rows[iRow2][2] = mEmployeeDS.Rows[iRow1]["TypeName"];
                    AutoDTable.Rows[iRow2][4] = mEmployeeDS.Rows[iRow1]["EmpNo"];
                    AutoDTable.Rows[iRow2][5] = mEmployeeDS.Rows[iRow1]["ExistingCode"];
                    AutoDTable.Rows[iRow2][6] = mEmployeeDS.Rows[iRow1]["FirstName"];
                    AutoDTable.Rows[iRow2][10] = mEmployeeDS.Rows[iRow1]["CatName"];
                    AutoDTable.Rows[iRow2][11] = mEmployeeDS.Rows[iRow1]["SubCatName"];


                }
            }
        }


        DataTable DtdIFReport = new DataTable();

        SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "' ";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);


        string name = dt.Rows[0]["CompName"].ToString();

        DataTable table_DT = new DataTable();
        table_DT.Columns.Add("CompanyName");
        table_DT.Columns.Add("LocationName");
        table_DT.Columns.Add("ShiftDate");

        table_DT.Columns.Add("SNo");
        table_DT.Columns.Add("Dept");
        table_DT.Columns.Add("Type");
        table_DT.Columns.Add("Shift");
        table_DT.Columns.Add("Category");
        table_DT.Columns.Add("SubCategory");
        table_DT.Columns.Add("EmpCode");
        table_DT.Columns.Add("ExCode");
        table_DT.Columns.Add("Name");
        table_DT.Columns.Add("TimeIN");
        table_DT.Columns.Add("TimeOUT");
        table_DT.Columns.Add("MachineID");
        table_DT.Columns.Add("PrepBy");
        table_DT.Columns.Add("PrepDate");
        table_DT.Columns.Add("TotalMIN");
        table_DT.Columns.Add("GrandTOT");

        int sno = 1;
        for (int iRow1 = 0; iRow1 < AutoDTable.Rows.Count; iRow1++)
        {
            dtRow = table_DT.NewRow();
            dtRow["CompanyName"] = name;
            dtRow["LocationName"] = SessionLcode;
            dtRow["ShiftDate"] = Date;
            dtRow["SNo"] = sno;
            dtRow["Dept"] = AutoDTable.Rows[iRow1][1].ToString();
            dtRow["Type"] = AutoDTable.Rows[iRow1][2].ToString();
            dtRow["Shift"] = AutoDTable.Rows[iRow1][3].ToString();
            dtRow["Category"] = AutoDTable.Rows[iRow1][10].ToString();
            dtRow["SubCategory"] = AutoDTable.Rows[iRow1][11].ToString();
            dtRow["EmpCode"] = AutoDTable.Rows[iRow1][4].ToString();
            dtRow["ExCode"] = AutoDTable.Rows[iRow1][5].ToString();
            dtRow["Name"] = AutoDTable.Rows[iRow1][6].ToString();

            if (AutoDTable.Rows[iRow1][7].ToString() != "")
            {
                ColumnName = AutoDTable.Rows[iRow1][7].ToString();
                if (ColumnName.ToString() == "WH")
                {
                    dtRow["TimeIN"] = "WH";
                }
                else
                {
                    string ColumnNew = ColumnName.Substring(0, 2);
                    if (ColumnNew == "NH")
                    {
                        string[] columnnew = ColumnName.Split('-');
                        dtRow["TimeIN"] = "NH/" + String.Format("{0:hh:mm tt}", (columnnew[1]));
                    }
                    else
                    {
                        dtRow["TimeIN"] = String.Format("{0:hh:mm tt}", AutoDTable.Rows[iRow1][7].ToString());
                    }

                }


            }

            else
            {
                dtRow["TimeIN"] = String.Format("{0:hh:mm tt}", AutoDTable.Rows[iRow1][7].ToString());
            }



            if (AutoDTable.Rows[iRow1][8].ToString() != "")
            {
                ColumnName = AutoDTable.Rows[iRow1][8].ToString();
                if (ColumnName.ToString() == "WH")
                {
                    dtRow["TimeIN"] = "WH";
                }
                else
                {
                    string ColumnNew = ColumnName.Substring(0, 2);
                    // string[] ColumnNew = ColumnName.Split('-');
                    if (ColumnNew == "NH")
                    {
                        string[] columnnew = ColumnName.Split('-');
                        dtRow["TimeOUT"] = "NH/" + String.Format("{0:hh:mm tt}", (columnnew[1]));
                        //dtRow["TimeOUT"] = "NH/" + String.Format("{0:hh:mm tt}", (ColumnNew[1]));
                    }
                    else
                    {
                        dtRow["TimeOUT"] = String.Format("{0:hh:mm tt}", AutoDTable.Rows[iRow1][8].ToString());
                    }

                }


            }

            else
            {
                dtRow["TimeOUT"] = String.Format("{0:hh:mm tt}", AutoDTable.Rows[iRow1][8].ToString());
            }


            dtRow["MachineID"] = AutoDTable.Rows[iRow1][9].ToString();

            dtRow["PrepBy"] = "User";
            dtRow["PrepDate"] = Date;

            if (AutoDTable.Rows[iRow1][12].ToString() != "")
            {
                ColumnName = AutoDTable.Rows[iRow1][12].ToString();
                if (ColumnName.ToString() == "WH")
                {
                    dtRow["TotalMIN"] = "WH";
                }
                else
                {
                    string ColumnNew = ColumnName.Substring(0, 2);
                    if (ColumnNew == "NH")
                    {
                        string[] columnnew = ColumnName.Split('-');
                        dtRow["TotalMIN"] = "NH/" + String.Format("{0:hh:mm tt}", (columnnew[1]));
                        //dtRow["TotalMIN"] = "NH/" + String.Format("{0:hh:mm tt}", (ColumnNew[1]));
                    }
                    else
                    {
                        dtRow["TotalMIN"] = String.Format("{0:hh:mm tt}", AutoDTable.Rows[iRow1][12].ToString());
                    }
                }
            }

            if (AutoDTable.Rows[iRow1][8].ToString() == "" || AutoDTable.Rows[iRow1][13].ToString() == "00:00")
            {
                dtRow["GrandTOT"] = "00:00";
            }

            else
            {
                if (AutoDTable.Rows[iRow1][13].ToString() != "")
                {
                    ColumnName = AutoDTable.Rows[iRow1][13].ToString();
                    if (ColumnName.ToString() == "WH")
                    {
                        dtRow["GrandTOT"] = "WH";
                    }
                    else
                    {
                        string ColumnNew = ColumnName.Substring(0, 2);
                        if (ColumnNew == "NH")
                        {
                            string[] columnnew = ColumnName.Split('-');

                            dtRow["GrandTOT"] = "NH/" + (columnnew[1]);
                        }
                        else
                        {
                            dtRow["GrandTOT"] = AutoDTable.Rows[iRow1][13].ToString();
                        }
                    }
                }
            }
            Date_Value_Str = string.Format(Date, "yyyy/MM/dd");
            SSQL = "select * from IFReport_DayAttendance where AttendanceDate = '" + Convert.ToDateTime(Date_Value_Str).ToString("yyyy/MM/dd") + "'";
            SSQL = SSQL + " And Compcode='" + SessionCcode + "'";
            SSQL = SSQL + " And LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And MachineID='" + dtRow["MachineID"] + "'";


            DtdIFReport = objdata.RptEmployeeMultipleDetails(SSQL);


            if (DtdIFReport.Rows.Count == 0)
            {

                SSQL = "insert into IFReport_DayAttendance(CompCode,LocCode,MachineID,ExistingCode,FirstName,DeptName,Shift,TimeIn,";
                SSQL = SSQL + "TimeOut,Category,SubCategory,TotalMIN,GrandTOT,AttendanceDate,PrepBy,TypeName)values('" + SessionCcode + "',";
                SSQL = SSQL + "'" + SessionLcode + "','" + dtRow["MachineID"] + "','" + dtRow["ExCode"] + "','" + dtRow["Name"] + "',";
                SSQL = SSQL + "'" + dtRow["Dept"] + "','" + dtRow["Shift"] + "','" + dtRow["TimeIN"] + "','" + dtRow["TimeOUT"] + "',";
                SSQL = SSQL + "'" + dtRow["Category"] + "','" + dtRow["SubCategory"] + "','" + dtRow["TotalMIN"] + "',";
                SSQL = SSQL + "'" + dtRow["GrandTOT"] + "','" + Convert.ToDateTime(dtRow["ShiftDate"]).ToString("yyyy/MM/dd") + "','User','" + dtRow["Type"] + "')";

                objdata.RptEmployeeMultipleDetails(SSQL);


            }
            table_DT.Rows.Add(dtRow);

            sno = sno + 1;

        }
        Date_Value_Str = Convert.ToDateTime(Date).ToString("yyyy/MM/dd");
        SSQL = "select * from IFReport_DayAttendance where AttendanceDate = '" + Date_Value_Str + "'";
        SSQL = SSQL + " And Compcode='" + SessionCcode + "'";
        SSQL = SSQL + " And LocCode='" + SessionLcode + "'";


        if (ShiftType1 != "ALL")
        {
            SSQL = SSQL + " And Shift='" + dtRow["Shift"] + "'";
        }


        DtdIFReport = objdata.RptEmployeeMultipleDetails(SSQL);

        int ssno = 1;
        for (int iRow = 0; iRow < DtdIFReport.Rows.Count; iRow++)
        {

            DataCell.NewRow();
            DataCell.Rows.Add();

            DataCell.Rows[iRow]["CompanyName"] = DtdIFReport.Rows[iRow]["CompCode"].ToString();
            DataCell.Rows[iRow]["LocationName"] = DtdIFReport.Rows[iRow]["LocCode"].ToString();
            DataCell.Rows[iRow]["ShiftDate"] = DtdIFReport.Rows[iRow]["AttendanceDate"].ToString();
            DataCell.Rows[iRow]["SNo"] = ssno;
            DataCell.Rows[iRow]["Dept"] = DtdIFReport.Rows[iRow]["DeptName"].ToString();
            DataCell.Rows[iRow]["Type"] = DtdIFReport.Rows[iRow]["TypeName"].ToString();
            DataCell.Rows[iRow]["Shift"] = DtdIFReport.Rows[iRow]["Shift"].ToString();
            DataCell.Rows[iRow]["Category"] = DtdIFReport.Rows[iRow]["Category"].ToString();
            DataCell.Rows[iRow]["SubCategory"] = DtdIFReport.Rows[iRow]["SubCategory"].ToString();
            DataCell.Rows[iRow]["EmpCode"] = DtdIFReport.Rows[iRow]["MachineID"].ToString();
            DataCell.Rows[iRow]["ExCode"] = DtdIFReport.Rows[iRow]["ExistingCode"].ToString();
            DataCell.Rows[iRow]["Name"] = DtdIFReport.Rows[iRow]["FirstName"].ToString();
            DataCell.Rows[iRow]["TimeIN"] = DtdIFReport.Rows[iRow]["TimeIn"].ToString();
            DataCell.Rows[iRow]["TimeOUT"] = DtdIFReport.Rows[iRow]["TimeOut"].ToString();

            DataCell.Rows[iRow]["TotalMIN"] = DtdIFReport.Rows[iRow]["TotalMIN"].ToString();
            DataCell.Rows[iRow]["GrandTOT"] = DtdIFReport.Rows[iRow]["GrandTOT"].ToString();
            DataCell.Rows[iRow]["MachineID"] = DtdIFReport.Rows[iRow]["MachineID"].ToString();
            DataCell.Rows[iRow]["PrepBy"] = DtdIFReport.Rows[iRow]["PrepBy"].ToString();
            DataCell.Rows[iRow]["PrepDate"] = DtdIFReport.Rows[iRow]["AttendanceDate"].ToString();


            ssno = ssno + 1;
        }
        ds.Tables.Add(DataCell);
        //ReportDocument report = new ReportDocument();
        report.Load(Server.MapPath("crystal/Attendance.rpt"));

        report.Database.Tables[0].SetDataSource(ds.Tables[0]);
        report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
        CrystalReportViewer1.ReportSource = report;


    }







    public void GetAttdDayWise_Change()
    {
        string TableName = "";

        string ng = string.Format(Date, "MM-dd-yyyy");
        Datestr = Convert.ToDateTime(Date).AddDays(0).ToShortDateString();
        Datestr1 = Convert.ToDateTime(Date).AddDays(1).ToShortDateString();
        DateTime date1 = Convert.ToDateTime(ng);
        DateTime date2 = date1.AddDays(1);

        if (Status == "Pending")
        {
            TableName = "Employee_Mst_New_Emp";
        }

        else
        {
            TableName = "Employee_Mst";
        }

        DataCell.Columns.Add("SNo");
        DataCell.Columns.Add("Dept");
        DataCell.Columns.Add("Type");
        DataCell.Columns.Add("Shift");


        DataCell.Columns.Add("EmpCode");
        DataCell.Columns.Add("ExCode");
        DataCell.Columns.Add("Name");
        DataCell.Columns.Add("TimeIN");
        DataCell.Columns.Add("TimeOUT");
        DataCell.Columns.Add("MachineID");
        DataCell.Columns.Add("Category");
        DataCell.Columns.Add("SubCategory");
        DataCell.Columns.Add("TotalMIN");
        DataCell.Columns.Add("GrandTOT");
        DataCell.Columns.Add("ShiftDate");
        DataCell.Columns.Add("CompanyName");
        DataCell.Columns.Add("LocationName");
        DataCell.Columns.Add("INHouse_IN");

        SSQL = "";
        SSQL = "select LD.MachineID,LD.ExistingCode,isnull(LD.DeptName,'') As DeptName,LD.Shift,LD.TypeName,isnull(LD.FirstName,'') as FirstName,";
        SSQL = SSQL + " LD.TimeIN,LD.TimeOUT,LD.Total_Hrs1 as Total_Hrs,EM.CatName as CatName,EM.SubCatName as SubCatName,EM.MachineID_Encrypt from LogTime_Days LD";
        SSQL = SSQL + " inner join " + TableName + " EM on EM.MachineID = LD.MachineID";
        //SSQL = SSQL + " inner join MstCategory MC on EM.WageCategoty = MC.CateID";
        //SSQL = SSQL + " inner join AgentMst AM on EM.BrokerName = AM.AgentID";
        //SSQL = SSQL + " inner join MstGrade MG on EM.Grade = MG.GradeID";
        SSQL = SSQL + " where LD.CompCode='" + Session["Ccode"].ToString() + "' ANd LD.LocCode='" + Session["Lcode"].ToString() + "'";
        SSQL = SSQL + " And EM.CompCode='" + Session["Ccode"].ToString() + "' ANd EM.LocCode='" + Session["Lcode"].ToString() + "'";
        //SSQL = SSQL + " And AM.CompCode='" + Session["SessionCcode"].ToString() + "' ANd AM.LocCode='" + Request.Cookies["SessionLcode"].Value.ToString() + "'";
        //SSQL = SSQL + " And MG.CompCode='" + Session["SessionCcode"].ToString() + "' ANd MG.LocCode='" + Request.Cookies["SessionLcode"].Value.ToString() + "'";

        if (Division != "-Select-")
        {
            SSQL = SSQL + " And EM.Division = '" + Division + "'";
        }
        if (ShiftType1 != "ALL")
        {
            if (ShiftType1 == "GENERAL")
            {
                SSQL = SSQL + " And (LD.Wages='MANAGER' or LD.Wages='STAFF' or LD.Wages='Watch & Ward')";
            }
            else
            {
                SSQL = SSQL + " And Shift='" + ShiftType1 + "'";
            }
        }
        if (Date != "")
        {
            SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(Date).ToString("dd/MM/yyyy") + "',103)";
        }
        //if (ShiftType1 == "GENERAL")
        //{
        //    SSQL = SSQL + "  And TimeIN!=''  ";
        //}
        //else
        //{
        //    SSQL = SSQL + "  And Shift !='No Shift' And TimeIN!=''  ";
        //}
        // 
        AutoDTable = objdata.RptEmployeeMultipleDetails(SSQL);


        SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "' ";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        string name = dt.Rows[0]["CompName"].ToString();
        if (AutoDTable.Rows.Count != 0)
        {
            int sno = 1;
            for (int i = 0; i < AutoDTable.Rows.Count; i++)
            {
                bool Record_Add = false;
                if (AutoDTable.Rows[i]["TimeIN"].ToString() == "")
                {
                    SSQL = " Select * from WorkingHouse_IN where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and MachineID='" + AutoDTable.Rows[i]["MachineID_Encrypt"].ToString() + "' ";
                    SSQL = SSQL + " and TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "04:00" + "'";
                    SSQL = SSQL + " and TimeIN <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "04:00" + "'";
                    dt = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (dt.Rows.Count != 0)
                    {
                        Record_Add = true;
                    }
                    else
                    {
                        Record_Add = false;
                    }
                }
                else
                {
                    Record_Add = true;
                }
                if (Record_Add == true)
                {
                    DataCell.NewRow();
                    DataCell.Rows.Add();


                    DataCell.Rows[DataCell.Rows.Count - 1]["SNo"] = sno;
                    DataCell.Rows[DataCell.Rows.Count - 1]["Dept"] = AutoDTable.Rows[i]["DeptName"].ToString();
                    DataCell.Rows[DataCell.Rows.Count - 1]["Type"] = AutoDTable.Rows[i]["TypeName"].ToString();
                    if (ShiftType1 == "GENERAL")
                    {
                        DataCell.Rows[DataCell.Rows.Count - 1]["Shift"] = AutoDTable.Rows[i]["Shift"].ToString();
                        //DataCell.Rows[DataCell.Rows.Count - 1]["Shift"] = "GENERAL";//AutoDTable.Rows[i]["Shift"].ToString();
                    }
                    else
                    {
                        DataCell.Rows[DataCell.Rows.Count - 1]["Shift"] = AutoDTable.Rows[i]["Shift"].ToString();
                    }
                    DataCell.Rows[DataCell.Rows.Count - 1]["EmpCode"] = AutoDTable.Rows[i]["MachineID"].ToString();
                    DataCell.Rows[DataCell.Rows.Count - 1]["ExCode"] = AutoDTable.Rows[i]["ExistingCode"].ToString();
                    DataCell.Rows[DataCell.Rows.Count - 1]["Name"] = AutoDTable.Rows[i]["FirstName"].ToString();
                    DataCell.Rows[DataCell.Rows.Count - 1]["TimeIN"] = AutoDTable.Rows[i]["TimeIN"].ToString();
                    DataCell.Rows[DataCell.Rows.Count - 1]["TimeOUT"] = AutoDTable.Rows[i]["TimeOUT"].ToString();
                    DataCell.Rows[DataCell.Rows.Count - 1]["MachineID"] = AutoDTable.Rows[i]["MachineID"].ToString();
                    DataCell.Rows[DataCell.Rows.Count - 1]["Category"] = AutoDTable.Rows[i]["CatName"].ToString();
                    DataCell.Rows[DataCell.Rows.Count - 1]["SubCategory"] = AutoDTable.Rows[i]["SubCatName"].ToString();
                    DataCell.Rows[DataCell.Rows.Count - 1]["TotalMIN"] = AutoDTable.Rows[i]["Total_Hrs"].ToString();
                    DataCell.Rows[DataCell.Rows.Count - 1]["GrandTOT"] = AutoDTable.Rows[i]["Total_Hrs"].ToString();
                    DataCell.Rows[DataCell.Rows.Count - 1]["ShiftDate"] = Date;
                    DataCell.Rows[DataCell.Rows.Count - 1]["CompanyName"] = name.ToString();
                    DataCell.Rows[DataCell.Rows.Count - 1]["LocationName"] = SessionLcode;





                    SSQL = " Select * from WorkingHouse_IN where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and MachineID='" + AutoDTable.Rows[i]["MachineID_Encrypt"].ToString() + "' ";
                    SSQL = SSQL + " and TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "04:00" + "'";
                    SSQL = SSQL + " and TimeIN <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "04:00" + "'";
                    dt = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (dt.Rows.Count != 0)
                    {
                        DataCell.Rows[DataCell.Rows.Count - 1]["INHouse_IN"] = String.Format("{0:hh:mm tt}", dt.Rows[0]["TimeIN"]);
                    }
                    else
                    {
                        DataCell.Rows[DataCell.Rows.Count - 1]["INHouse_IN"] = "";
                    }

                    sno += 1;
                }
            }







            ds.Tables.Add(DataCell);
            //ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("crystal/AttendanceWithWorkingHouse.rpt"));

            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Records Found');", true);
        }
    }

    private static string UTF8Decryption(string encryptpwd)
    {
        string decryptpwd = "0";
        BALDataAccess objdata_new = new BALDataAccess();

        string query = "Select * from Employee_Mst where MachineID_Encrypt='" + encryptpwd + "'";
        DataTable DT = new DataTable();
        DT = objdata_new.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            decryptpwd = DT.Rows[0]["MachineID"].ToString();
        }
        else
        {
            query = "Select * from Employee_Mst_New_Emp where MachineID_Encrypt='" + encryptpwd + "'";
            DT = objdata_new.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                decryptpwd = DT.Rows[0]["MachineID"].ToString();
            }
            else
            {
                decryptpwd = "0";
            }
        }

        //UTF8Encoding encodepwd = new UTF8Encoding();
        //Decoder Decode = encodepwd.GetDecoder();
        //byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
        //int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
        //char[] decoded_char = new char[charCount];
        //Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
        //decryptpwd = new String(decoded_char);
        return decryptpwd;
    }
    public static string GetRandom(int Min, int Max)
    {
        System.Random Generator = new System.Random();

        return Generator.Next(Min, Max).ToString();


    }
    public static string UTF8Encryption_OLD(string password)
    {
        string strmsg = string.Empty;
        byte[] encode = new byte[password.Length];
        encode = Encoding.UTF8.GetBytes(password);
        strmsg = Convert.ToBase64String(encode);
        return strmsg;
    }

    public string UTF8Encryption(string mvarPlanText)
    {
        string cipherText = "";
        try
        {
            string passPhrase = "Altius";
            string saltValue = "info@altius.co.in";
            string hashAlgorithm = "SHA1";
            string initVector = "@1B2c3D4e5F6g7H8";
            int passwordIterations = 2;
            int keySize = 256;
            byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
            byte[] saltValueBytes = Encoding.ASCII.GetBytes(saltValue);
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(mvarPlanText);
            PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, saltValueBytes, hashAlgorithm, passwordIterations);
            byte[] keyBytes = password.GetBytes(keySize / 8);
            RijndaelManaged symmetricKey = new RijndaelManaged();
            symmetricKey.Mode = CipherMode.CBC;
            ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes);
            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
            cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
            cryptoStream.FlushFinalBlock();
            byte[] cipherTextBytes = memoryStream.ToArray();
            memoryStream.Close();
            cryptoStream.Close();
            cipherText = Convert.ToBase64String(cipherTextBytes);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }

        return cipherText;
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        CrystalReportViewer1.Dispose();
    }
}
