﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class VoucherEntry : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SSQL = "";
    string SessionPayroll;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();

        Load_DB();
        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Voucher Entry";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");
            Initial_Data_Referesh();
            Fin_Year_Add();
            Load_WagesType();
            Load_Data_EmpDet();
        }
        Load_Data();
    }
    public void Load_DB()
    {
        //Get Database Name
        string query = "";
        DataTable dt_DB = new DataTable();
        query = "select CMS,Rights,Sales,Stores,Weaving,Maintaince,Production,Payroll from [SVT_Rights]..MstDBname";
        dt_DB = objdata.RptEmployeeMultipleDetails(query);
        if (dt_DB.Rows.Count > 0)
        {
            SessionPayroll = dt_DB.Rows[0]["Payroll"].ToString();
            //SessionRights = dt_DB.Rows[0]["Rights"].ToString();
        }
    }
    private void Load_WagesType()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlWages.Items.Clear();
        query = "Select *from MstEmployeeType";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlWages.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpType"] = "-Select-";
        dr["EmpType"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlWages.DataTextField = "EmpType";
        ddlWages.DataValueField = "EmpType";
        ddlWages.DataBind();
    }

    private void Load_Data_EmpDet()
    {
        string query = "";
        DataTable DT = new DataTable();

        //Repeater1.DataSource = DT;
        //Repeater1.DataBind();

        txtTokenNo.Items.Clear();
        query = "Select ExistingCode from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        query = query + " And IsActive='Yes' And Wages='" + ddlWages.SelectedItem.Text + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        txtTokenNo.DataSource = DT;
        DataRow dr = DT.NewRow();
        dr["ExistingCode"] = "-Select-";
        dr["ExistingCode"] = "-Select-";
        DT.Rows.InsertAt(dr, 0);
        txtTokenNo.DataTextField = "ExistingCode";
        txtTokenNo.DataValueField = "ExistingCode";
        txtTokenNo.DataBind();
    }
    public void Fin_Year_Add()
    {
        int CurrentYear;
        int i;
        //Financial Year Add
        CurrentYear = DateTime.Now.Year;
        

        ddlFinyear.Items.Clear();
        ddlFinyear.Items.Add("-Select-");

        for (i = 0; i < 11; i++)
        {
            ddlFinyear.Items.Add(Convert.ToString(CurrentYear));
            CurrentYear = CurrentYear - 1;
        }
    }

    protected void ddlWages_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Data_EmpDet();
    }
    protected void txtTokenNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        string query;
        DataTable DT = new DataTable();

        query = "Select * from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        query = query + " And ExistingCode='" + txtTokenNo.SelectedItem.Text + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            txtEmpName.Text = DT.Rows[0]["FirstName"].ToString();

        }
        else
        {
            txtTokenNo.SelectedValue = "-Select-";
            txtEmpName.Text = "";
          

        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
         bool ErrFlag = false;
         double Worked_Day_dbl = 0;
         double Wages_Amount_dbl = 0;
         DataTable DT=new DataTable();

        if(txtWorkDay.Text=="")
        {
            Worked_Day_dbl = 0;
        }
        else
        {
            Worked_Day_dbl = Convert.ToDouble(txtWorkDay.Text);
        }

        if(txtPaidAmt.Text=="")
        {
            Wages_Amount_dbl = 0;
        }
        else
        {
            Wages_Amount_dbl = Convert.ToDouble(txtPaidAmt.Text);
        }

        if (Worked_Day_dbl == 0 || Wages_Amount_dbl == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to enter Worked Days And Wages Amount.!');", true);
        }

        if (!ErrFlag)
        {
            SSQL = "Select * from [" + SessionPayroll + "]..SalaryDetails_Voucher where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And Bonus_Year='" + ddlFinyear.SelectedItem.Text + "' and ExisistingCode='" + txtTokenNo.SelectedItem.Text + "'";
            DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT.Rows.Count != 0)
            {
                //Delete OLD Record
                SSQL = "Delete from [" + SessionPayroll + "]..SalaryDetails_Voucher where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And Bonus_Year='" + ddlFinyear.SelectedItem.Text + "' and ExisistingCode='" + txtTokenNo.SelectedItem.Text + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);

            }

         //Insert Weekly OT HOURS
        SSQL = "Insert Into [" + SessionPayroll + "]..SalaryDetails_Voucher(Ccode,Lcode,ExisistingCode,EmpName,Total_Days,Voucher_Amt,Bonus_Year,Month_Name,Wages_Type,FromDate,ToDate)";
        SSQL = SSQL + " Values('" + SessionCcode + "','" + SessionLcode + "','" + txtTokenNo.SelectedItem.Text + "','" + txtEmpName.Text + "','" + Worked_Day_dbl + "',";
        SSQL = SSQL + "'" + Wages_Amount_dbl + "','" + ddlFinyear.SelectedItem.Text + "','" + ddlMonth.SelectedItem.Text + "','" + ddlWages.SelectedItem.Text + "',";
        SSQL = SSQL + "'" + Convert.ToDateTime(txtFromDate.Text).ToString("yyyy/MM/dd") + "','" + Convert.ToDateTime(txtToDate.Text).ToString("yyyy/MM/dd") + "')";
        objdata.RptEmployeeMultipleDetails(SSQL);

        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Voucher Amount Saved Successfully.!');", true);
        Clear_All_Field();
      
        }
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    private void Clear_All_Field()
    {
        ddlMonth.SelectedValue = "-Select-";
        ddlFinyear.SelectedValue = "-Select-";
        ddlWages.SelectedValue = "-Select-";
        txtTokenNo.SelectedValue = "-Select-";
        txtEmpName.Text = ""; txtTokenNo.SelectedValue = "-Select-";
        txtFromDate.Text = ""; txtToDate.Text = "";
        txtPaidAmt.Text = "0"; txtWorkDay.Text = "0";
        Initial_Data_Referesh();
        Load_Data();
        btnSave.Text = "Save";
    }

    private void Load_Data()
    {

        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];

        Repeater1.DataSource = dt;
        Repeater1.DataBind();

    }

    private void Initial_Data_Referesh()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("ExisistingCode", typeof(string)));
        dt.Columns.Add(new DataColumn("EmpName", typeof(string)));
        dt.Columns.Add(new DataColumn("Total_Days", typeof(string)));
        dt.Columns.Add(new DataColumn("Voucher_Amt", typeof(string)));
        

        Repeater1.DataSource = dt;
        Repeater1.DataBind();

        ViewState["ItemTable"] = Repeater1.DataSource;

        //dt = Repeater1.DataSource;
    }

    public void Voucher_Load_Det()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from [" + SessionPayroll + "]..SalaryDetails_Voucher where Ccode='" + SessionCcode + "'";
        query = query + " And Bonus_Year='" + ddlFinyear.SelectedItem.Text + "' And Wages_Type='" + ddlWages.SelectedItem.Text + "'";
        query = query + " And Lcode='" + SessionLcode + "'";

        DT = objdata.RptEmployeeMultipleDetails(query);

        Repeater1.DataSource = DT;
        Repeater1.DataBind();

        ViewState["ItemTable"] = Repeater1.DataSource;
    }

    protected void GridEditEnquiryClick(object sender, CommandEventArgs e)
    {
        DataTable DT = new DataTable();
        DataTable DT_Check = new DataTable();
      
     

        SSQL = "Select * from [" + SessionPayroll + "]..SalaryDetails_Voucher Where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And ExisistingCode='" + e.CommandName.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT.Rows.Count != 0)
        {
            ddlWages.SelectedValue = DT.Rows[0]["Wages_Type"].ToString();
            Load_Data_EmpDet();
            ddlMonth.SelectedValue = DT.Rows[0]["Month_Name"].ToString();
            txtTokenNo.SelectedValue = DT.Rows[0]["ExisistingCode"].ToString();
            txtEmpName.Text = DT.Rows[0]["EmpName"].ToString();
            ddlFinyear.SelectedValue = DT.Rows[0]["Bonus_Year"].ToString();
            txtWorkDay.Text = DT.Rows[0]["Total_Days"].ToString();
            txtPaidAmt.Text = DT.Rows[0]["Voucher_Amt"].ToString();
            txtFromDate.Text = Convert.ToDateTime(DT.Rows[0]["FromDate"]).ToString("dd/MM/yyyy");
            txtToDate.Text = Convert.ToDateTime(DT.Rows[0]["ToDate"]).ToString("dd/MM/yyyy");
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Voucher Details not found.!');", true);
        }
        Voucher_Load_Det();
        Load_Data();
    }

    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {
        DataTable DT = new DataTable();
        DataTable DT_Check = new DataTable();
        

       

        SSQL = "Select * from [" + SessionPayroll + "]..SalaryDetails_Voucher Where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And ExisistingCode='" + e.CommandName.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT.Rows.Count != 0)
        {
            SSQL = "Delete from [" + SessionPayroll + "]..SalaryDetails_Voucher Where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And ExisistingCode='" + e.CommandName.ToString() + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Voucher Details Deleted Succesfully..!');", true);

        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Voucher Details not found.!');", true);
        }
        Voucher_Load_Det();
        Load_Data();
    }

    protected void ddlFinyear_SelectedIndexChanged(object sender, EventArgs e)
    {
        Voucher_Load_Det();
    }
}
