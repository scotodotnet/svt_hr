<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="EpayTransfer.aspx.cs" Inherits="EpayTransfer" Title="" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script src="assets/js/master_list_jquery.min.js"></script>
<script src="assets/js/master_list_jquery-ui.min.js"></script>
<link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css"/>
 <script>
     $(document).ready(function() {
     $('#example1').dataTable();
     $('.datepicker').datepicker({
         format: "dd/mm/yyyy",
         autoclose: true
     });
     });
 </script>

 
<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example1').dataTable();
                $('.select2').select2();
                $('.datepicker').datepicker({
                    format: "dd/mm/yyyy",
                    autoclose: true
                });
            }
        });
    };
</script>
  <script type="text/javascript">
      function SaveMsgAlert(msg) {
          swal(msg);
      }
</script>
 <script type="text/javascript">
     function ProgressBarShow() {
         $('#Download_loader').show();
     }
</script>

<script type="text/javascript">
    function ProgressBarHide() {
        $('#Download_loader').hide();
    }
</script>
<asp:UpdatePanel ID="UpdatePanel5" runat="server">
 <ContentTemplate>
<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Salary Process</a></li>
				<li class="active">E-Pay Transfer</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">E-Pay Transfer </h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">E-Pay Transfer</h4>
                        </div>
                        <div class="panel-body">
                        <div class="form-group">
                       
                        <div class="row">
                            
                                <!-- begin col-4 -->
                                 <div class="col-md-4">
									<div class="form-group">
										<label>Month</label>
										<asp:DropDownList ID="ddlMonth" runat="server" class="form-control select2" Width="100%"
										AutoPostBack="true" 
                                        onselectedindexchanged="ddlMonth_SelectedIndexChanged">
                                         <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
								        <asp:ListItem Value="January">January</asp:ListItem>
								        <asp:ListItem Value="February">February</asp:ListItem>
								        <asp:ListItem Value="March">March</asp:ListItem>
								        <asp:ListItem Value="April">April</asp:ListItem>
								        <asp:ListItem Value="May">May</asp:ListItem>
								        <asp:ListItem Value="June">June</asp:ListItem>
								        <asp:ListItem Value="July">July</asp:ListItem>
								        <asp:ListItem Value="August">August</asp:ListItem>
								        <asp:ListItem Value="September">September</asp:ListItem>
								        <asp:ListItem Value="October">October</asp:ListItem>
								        <asp:ListItem Value="November">November</asp:ListItem>
								        <asp:ListItem Value="December">December</asp:ListItem>
                                       </asp:DropDownList>
                                       <asp:RequiredFieldValidator ControlToValidate="ddlMonth" InitialValue="-Select-" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                       </asp:RequiredFieldValidator>
									</div>
                                  </div>
                               <!-- end col-4 -->
                                <!-- begin col-4 -->
                                 <div class="col-md-4">
									<div class="form-group">
										<label>Fin. Year</label>
										 <asp:DropDownList ID="ddlFinyear" runat="server" class="form-control select2" Width="100%"
										 AutoPostBack="true" 
                                        onselectedindexchanged="ddlFinyear_SelectedIndexChanged">
                                         </asp:DropDownList>
                                         <asp:RequiredFieldValidator ControlToValidate="ddlFinyear" InitialValue="-Select-" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                       </asp:RequiredFieldValidator>
									</div>
                                  </div>
                               <!-- end col-4 -->
                               <!-- begin col-4 -->
                                 <div class="col-md-4">
									<div class="form-group">
											<label>Wages Type</label>
										 <asp:DropDownList ID="ddlWages" runat="server" class="form-control select2" 
                                            Width="100%" AutoPostBack="true" 
                                            onselectedindexchanged="ddlWages_SelectedIndexChanged">
                                         </asp:DropDownList>
                                          <asp:RequiredFieldValidator ControlToValidate="ddlWages" InitialValue="-Select-" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                       </asp:RequiredFieldValidator>
									</div>
                                  </div>
                               <!-- end col-4 -->
                        </div>
                       <!-- begin row -->
                          <div class="row">
                            
                               <!-- begin col-4 -->
                                 <div class="col-md-2">
									<div class="form-group">
										<label>Token No</label>
										 <asp:DropDownList ID="ddlTokenNo" runat="server" class="form-control select2" 
                                            Width="100%" AutoPostBack="true" 
                                            onselectedindexchanged="ddlTokenNo_SelectedIndexChanged">
                                         </asp:DropDownList>
                                          <%--<asp:RequiredFieldValidator ControlToValidate="ddlTokenNo" InitialValue="-Select-" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                       </asp:RequiredFieldValidator>--%>
									</div>
                                  </div>
                               <!-- end col-4 -->
                               <!-- begin col-4 -->
                                 <div class="col-md-4">
									<div class="form-group">
										<label>Name</label>
										<asp:Label runat="server" ID="txtEmpName" class="form-control" BackColor="#F3F3F3"></asp:Label>
									</div>
                                  </div>
                               <!-- end col-4 -->
                                <!-- begin col-4 -->
                                 <div class="col-md-3">
									<div class="form-group">
										<label>From Date</label>
										<asp:TextBox runat="server" ID="txtFromDate" class="form-control datepicker" placeholder="dd/MM/YYYY"></asp:TextBox>
										<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                           TargetControlID="txtFromDate" ValidChars="0123456789/">
                                         </cc1:FilteredTextBoxExtender>
                                          <asp:RequiredFieldValidator ControlToValidate="txtFromDate" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator6" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                       </asp:RequiredFieldValidator>
									</div>
                                  </div>
                               <!-- end col-4 -->
                                 <!-- begin col-4 -->
                                 <div class="col-md-3">
									<div class="form-group">
										<label>To Date</label>
										<asp:TextBox runat="server" ID="txtToDate" class="form-control datepicker" placeholder="dd/MM/YYYY"></asp:TextBox>
										<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                           TargetControlID="txtToDate" ValidChars="0123456789/">
                                         </cc1:FilteredTextBoxExtender>
                                         <asp:RequiredFieldValidator ControlToValidate="txtToDate" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator7" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                       </asp:RequiredFieldValidator>
									</div>
                                  </div>
                               <!-- end col-4 -->
                                </div>
                          <!-- begin row -->
                          <div class="row">
                              
                          </div>
                        <!-- end row -->  
                        <!-- begin row -->  
                        <div class="row">
                         <div class="col-md-4"></div>
                           <!-- begin col-4 -->
                                <div class="col-md-6">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btnTransfer" Text="Transfer" 
                                         class="btn btn-success"  ValidationGroup="Validate_Field" 
                                         onclick="btnTransfer_Click" OnClientClick="ProgressBarShow();"/>
									<asp:Button runat="server" id="btnIncentive" Text="Civil Incentive Transfer" Visible="false"
                                         class="btn btn-info" ValidationGroup="Validate_Field" 
                                         onclick="btnIncentive_Click" OnClientClick="ProgressBarShow();" />
									<asp:Button runat="server" id="btnClear" Text="Clear" class="btn btn-danger" 
                                         onclick="btnClear_Click" />
									<asp:Button runat="server" id="btnConform" Text="Conformation" 
                                         class="btn btn-warning" ValidationGroup="Validate_Field" onclick="btnConform_Click" />
                                         
                                     <asp:Button ID="btnVoucher" runat="server" Text="Voucher Generation" 
                                         class="btn btn-info" onclick="btnVoucher_Click" OnClientClick="ProgressBarShow();" />
								 </div>
                               </div>
                              <!-- end col-4 -->
                              <div class="col-md-4"></div>
                         </div>
                        <!-- end row -->  
                        <div id="Download_loader" style="display:none"/></div>
                        </div>
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
<!-- end #content -->
</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>

