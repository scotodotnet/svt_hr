﻿<%@ Page Language="C#" MasterPageFile="~/PayrollMaster.master" AutoEventWireup="true" CodeFile="AdvancePay.aspx.cs" Inherits="AdvancePay" Title="" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

            <script src="assets/js/master_list_jquery.min.js"></script>
            <script src="assets/js/master_list_jquery-ui.min.js"></script>
            <link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css" />
            <script>
                $(document).ready(function () {
                    $('#example').dataTable();
                });
            </script>


            <script type="text/javascript">
                //On UpdatePanel refresh
                var prm = Sys.WebForms.PageRequestManager.getInstance();
                if (prm != null) {
                    prm.add_endRequest(function (sender, e) {
                        if (sender._postBackSettings.panelsToUpdate != null) {
                            $('#example').dataTable();
                            $('.select2').select2();
                            $('.datepicker').datepicker({
                                format: "dd/mm/yyyy",
                                autoclose: true
                            });
                        }
                    });
                };

            </script>

            <!-- begin #content -->
            <div id="content" class="content">
                <!-- begin breadcrumb -->
                <ol class="breadcrumb pull-right">
                    <li><a href="javascript:;">Advance</a></li>
                    <li class="active">Advance Payment</li>
                </ol>
                <!-- end breadcrumb -->
                <!-- begin page-header -->
                <h1 class="page-header">Advance Payment</h1>
                <!-- end page-header -->

                <!-- begin row -->
                <div class="row">
                    <!-- begin col-12 -->
                    <div class="col-md-12">
                        <!-- begin panel -->
                        <div class="panel panel-inverse">
                            <div class="panel-heading">
                                <div class="panel-heading-btn">
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                                </div>
                                <h4 class="panel-title">Advance Payment</h4>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <!-- begin row -->
                                    <div class="row">
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Wages Type</label>
                                                <asp:DropDownList runat="server" ID="ddlWagesType" class="form-control select2" Style="width: 100%;"
                                                    AutoPostBack="true" OnSelectedIndexChanged="ddlWagesType_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Existing Code</label>
                                                <asp:DropDownList runat="server" ID="ddlExistingCode" class="form-control select2" Style="width: 100%;"
                                                    AutoPostBack="true" OnSelectedIndexChanged="ddlExistingCode_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Employee Name</label>
                                                <asp:Label runat="server" ID="txtEmpName" class="form-control" BackColor="#F3F3F3"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end row -->
                                    <!-- begin row -->
                                    <div class="row">
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Machine ID</label>
                                                <asp:Label runat="server" ID="txtTokenNo" class="form-control" BackColor="#F3F3F3"></asp:Label>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>DOJ</label>
                                                <asp:Label runat="server" ID="txtDOJ" class="form-control" BackColor="#F3F3F3"></asp:Label>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Designation</label>
                                                <asp:Label runat="server" ID="txtDesignation" class="form-control" BackColor="#F3F3F3"></asp:Label>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                    </div>
                                    <!-- end row -->

                                    <!-- begin row -->
                                    <div class="row">
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Basic Salary</label>
                                                <asp:Label runat="server" ID="txtBasicSal" class="form-control" BackColor="#F3F3F3"></asp:Label>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Date</label>
                                                <asp:TextBox runat="server" ID="txtFromDate" class="form-control datepicker"></asp:TextBox>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Advance Amount</label>
                                                <asp:TextBox runat="server" ID="txtAdvAmt" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                    </div>
                                    <!-- end row -->

                                    <!-- begin row -->
                                    <div class="row">
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Balance Amount</label>
                                                <asp:TextBox runat="server" ID="txtBalanceAmt" class="form-control" Enabled="false" Text="0"></asp:TextBox>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>No Of Installment</label>
                                                <asp:TextBox runat="server" ID="txtNoofMonths" class="form-control"
                                                    AutoPostBack="true" OnTextChanged="txtNoofMonths_TextChanged"></asp:TextBox>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->



                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Monthly Deduction</label>
                                                <asp:TextBox runat="server" ID="txtMonthlyDed" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end row -->
                                    <!-- end col-4 -->
                                    <div class="row" runat="server" visible="false">
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <br />
                                                <asp:FileUpload ID="filUpload" runat="server" CssClass="btn btn-default fileinput-button" Style="margin-bottom: 20px;" />
                                            </div>
                                        </div>
                                        <!-- end col-4 -->


                                    </div>
                                    <!-- end row -->
                                    <!-- begin row -->

                                    <!-- begin row -->
                                    <div class="row">
                                        <div class="col-md-4"></div>
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <br />
                                                <asp:Button runat="server" ID="btnSave" Text="Save" class="btn btn-success"
                                                    OnClick="btnSave_Click" />
                                                <asp:Button runat="server" ID="btnClear" Text="Clear" class="btn btn-danger"
                                                    OnClick="btnClear_Click" />
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <div class="col-md-4"></div>
                                    </div>
                                    <!-- end row -->

                                </div>
                            </div>
                        </div>
                        <!-- end panel -->
                    </div>
                    <!-- end col-12 -->
                </div>
                <!-- end row -->
            </div>
            <!-- end #content -->
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSave" />
        </Triggers>
    </asp:UpdatePanel>



</asp:Content>

