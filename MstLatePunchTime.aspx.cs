﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;
public partial class MstLatePunchTime : System.Web.UI.Page
{
    string ShiftType1 = "";
    string Date = "";
    string Status = "";

    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string ss = "";

    ReportDocument report = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();

    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();

        LoadGrid();

        if(!IsPostBack)
        LoadShift();
    }

    private void LoadShift()
    {
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails("Select ShiftDesc from Shift_Mst");
        ddlShift.DataSource = dt;
        ddlShift.DataTextField = "ShiftDesc";
        ddlShift.DataValueField = "ShiftDesc";
        ddlShift.DataBind();
        ddlShift.Items.Insert(0,new ListItem("Select Shift","0"));
    }

    private void LoadGrid()
    {
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails("Select * from Mst_LatePunchTime");
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }

    protected void txtSno_txt_TextChanged(object sender, EventArgs e)
    {

    }

    protected void txttime1_TextChanged(object sender, EventArgs e)
    {

    }

    protected void txttime1_TextChanged1(object sender, EventArgs e)
    {

    }

    protected void txtTime2_TextChanged(object sender, EventArgs e)
    {

    }

    protected void txtTime3_TextChanged(object sender, EventArgs e)
    {

    }

    protected void txtTime3_TextChanged1(object sender, EventArgs e)
    {

    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (txtMinHr.Text != "" && txtMinMnts.Text != "" && txtMaxHr.Text != "" && txtMaxMnts.Text != "")
        {

            objdata.RptEmployeeMultipleDetails("Delete From Mst_LatePunchTime where Shift='"+ddlShift.SelectedItem.Text+"'");
            objdata.RptEmployeeMultipleDetails("Insert into Mst_LatePunchTime values('"+ ddlShift.SelectedItem.Text + "','"+txtMinHr.Text+"','"+txtMinMnts.Text+"','"+txtMaxHr.Text+"','"+txtMaxMnts.Text+"')");
            ScriptManager.RegisterStartupScript(this,this.GetType(),"Alert","alert('Time Added Succesfully');",true);
            Clear();
            LoadGrid();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert('Please Check the fields');", true);
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear();
    }

    private void Clear()
    {
        ddlShift.ClearSelection();
        txtMinHr.Text = string.Empty;
        txtMinMnts.Text = string.Empty;
        txtMaxHr.Text = string.Empty;
        txtMaxMnts.Text = string.Empty;
    }

    protected void btnEditIssueEntry_Grid_Command(object sender, CommandEventArgs e)
    {
        if (e.CommandName == "Edit")
        {
            //string SQL = "Delete from Mst_LatePunchTime where Shift='"+e.CommandArgument.ToString()+"'";
            DataTable dt = new DataTable();
            dt = objdata.RptEmployeeMultipleDetails("Select * from Mst_LatePunchTime where Shift='"+e.CommandArgument.ToString()+"'");
            ddlShift.SelectedItem.Text = dt.Rows[0][0].ToString();
            txtMinHr.Text = dt.Rows[0][1].ToString();
            txtMinMnts.Text = dt.Rows[0][2].ToString();
            txtMaxHr.Text = dt.Rows[0][3].ToString();
            txtMaxMnts.Text = dt.Rows[0][4].ToString();
        }
    }

    protected void btnDeleteEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {
        objdata.RptEmployeeMultipleDetails("Delete From Mst_LatePunchTime where Shift='"+e.CommandArgument.ToString()+"'");
        LoadGrid();
    }

    protected void ddlShift_TextChanged(object sender, EventArgs e)
    {

    }
}