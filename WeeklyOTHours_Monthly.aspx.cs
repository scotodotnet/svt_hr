﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Globalization;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;

public partial class WeeklyOTHours_Monthly : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string Division;
    string SessionPayroll;
    double Final_Count;

    double Present_Count;
    double Absent_Count;
    decimal Fixed_Work_Days;
    decimal Month_WH_Count;
    double Present_WH_Count;
    double NFH_Days_Count;
    double NFH_Days_Present_Count;

    BALDataAccess objdata = new BALDataAccess();

    string SSQL = "";
    DataTable AutoDTable = new DataTable();
    DataTable mEmployeeDT = new DataTable();
    DataTable MEmployeeDS = new DataTable();
    DataTable mDataSet = new DataTable();
    DataTable OTHrs_DS = new DataTable();


    DateTime date1;
    DateTime Date2 = new DateTime();
    int intK;
    string FromDate;
    string ToDate;
    string Date_Value_Str;
    string Date_value_str1;
    string[] Time_Minus_Value_Check;
    string WagesType;
    double leaveCount;
    ReportDocument report = new ReportDocument();
    DataSet ds = new DataSet();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            Load_DB();
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | REPORT - MONTHLY OT HOURS PAYSLIP";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("ManualEntry"));
                //li.Attributes.Add("class", "droplink active open");
            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            //SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            Division = Request.QueryString["Division"].ToString();
            string TempWages = Request.QueryString["Wages"].ToString();
            WagesType = TempWages.Replace("_", "&");
            //WagesType = Request.QueryString["Wages"].ToString();

            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            GetAttdTable_Weekly_OTHOURS();
            
            ds.Tables.Add(AutoDTable);
            //ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("crystal/Weekly_Monthly_OTReport_Payslip.rpt"));

            report.Database.Tables[0].SetDataSource(ds.Tables[0]);

            DataTable dt = new DataTable();
            string CmpName = "";
            string Cmpaddress = "";
            SSQL = "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt.Rows.Count > 0)
            {
                CmpName = dt.Rows[0]["Cname"].ToString();
                Cmpaddress = (dt.Rows[0]["Address1"].ToString() + ", " + dt.Rows[0]["Address2"].ToString() + ", " + dt.Rows[0]["Location"].ToString() + "-" + dt.Rows[0]["Pincode"].ToString());
            }

            if (WagesType == "REGULAR")
            {
                report.DataDefinition.FormulaFields["Report_Head"].Text = "'" + "MOTIVATIONAL INCENTIVE PAID TO REGULAR FOR THE PERIOD FROM" + "'";
            }
            if (WagesType == "HOSTEL")
            {
                report.DataDefinition.FormulaFields["Report_Head"].Text = "'" + "MOTIVATIONAL INCENTIVE PAID TO HOSTEL FOR THE PERIOD FROM" + "'";
            }

            if (WagesType == "CIVIL")
            {
                report.DataDefinition.FormulaFields["Report_Head"].Text = "'" + "MOTIVATIONAL INCENTIVE PAID TO CIVIL FOR THE PERIOD FROM" + "'";
            }

            if (WagesType == "SUB-STAFF")
            {
                report.DataDefinition.FormulaFields["Report_Head"].Text = "'" + "MOTIVATIONAL INCENTIVE PAID TO SUB-STAFF FOR THE PERIOD FROM" + "'";
            }

            if (WagesType == "STAFF")
            {
                report.DataDefinition.FormulaFields["Report_Head"].Text = "'" + "MOTIVATIONAL INCENTIVE PAID TO STAFF FOR THE PERIOD FROM" + "'";
            }

            if (WagesType == "Watch")
            {
                report.DataDefinition.FormulaFields["Report_Head"].Text = "'" + "MOTIVATIONAL INCENTIVE PAID TO WATCH & WARD FOR THE PERIOD FROM" + "'";
            }

            if (WagesType == "Manager")
            {
                report.DataDefinition.FormulaFields["Report_Head"].Text = "'" + "MOTIVATIONAL INCENTIVE PAID TO MANAGER FOR THE PERIOD FROM" + "'";
            }


            report.DataDefinition.FormulaFields["FromDate_Str"].Text = "'" + FromDate.ToString() + "'";
            report.DataDefinition.FormulaFields["To_Date_Str"].Text = "'" + ToDate.ToString() + "'";
            report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName.ToString() + "'";
            report.DataDefinition.FormulaFields["LocationName"].Text = "'" + SessionLcode.ToString() + "'";
            if (Division != "-Select-")
            {
                report.DataDefinition.FormulaFields["Division_Name_Str"].Text = "'" + Division.ToString() + "'";
            }
            else
            {
                report.DataDefinition.FormulaFields["Division_Name_Str"].Text = "'" + "" + "'";
            }

            DataTable DT_Inc = new DataTable();
            SSQL = "Select *from OT_Incentive_Mst where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            DT_Inc = objdata.RptEmployeeMultipleDetails(SSQL);
            string INCAMT = "0";

            if (DT_Inc.Rows.Count != 0)
            {
                INCAMT = DT_Inc.Rows[0]["OTIncAmt"].ToString();
            }
            report.DataDefinition.FormulaFields["IncentiveAmt"].Text = "'" + INCAMT.ToString() + "'";

            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;
        }
    }
    public void Load_DB()
    {
        //Get Database Name
        string query = "";
        DataTable dt_DB = new DataTable();
        query = "select CMS,Rights,Sales,Stores,Weaving,Maintaince,Production,Payroll from [SVT_Rights]..MstDBname";
        dt_DB = objdata.RptEmployeeMultipleDetails(query);
        if (dt_DB.Rows.Count > 0)
        {
            SessionPayroll = dt_DB.Rows[0]["Payroll"].ToString();
            //SessionRights = dt_DB.Rows[0]["Rights"].ToString();
        }
    }

    public void GetAttdTable_Weekly_OTHOURS()
    {
        AutoDTable.Columns.Add("Basic");
        AutoDTable.Columns.Add("Dept");
        AutoDTable.Columns.Add("Designation");
        AutoDTable.Columns.Add("TokenNo");
        AutoDTable.Columns.Add("EmpName");
        AutoDTable.Columns.Add("MonthOTHrs");
        AutoDTable.Columns.Add("SPGTotalHrs");
        AutoDTable.Columns.Add("OTHrs_Rate");

        DataTable DT_MOT = new DataTable();
        int i;
        DataRow dtRow;

        SSQL = "Select EM.BaseSalary as Basic,WOT.Dept_Name as Dept,WOT.Designation as Designation,WOT.ExistingCode as TokenNo,";
        SSQL = SSQL + " EM.FirstName as EmpName,sum(WOT.OTHrs) as MonthOTHrs,'0' as SPGTotalHrs,'0' as OTHrs_Rate from Weekly_OTHours WOT";
        SSQL = SSQL + " inner join Employee_Mst EM on EM.LocCode=WOT.LocCode And EM.CompCode=WOT.CompCode And EM.ExistingCode=WOT.ExistingCode";
        SSQL = SSQL + " where WOT.LocCode='" + SessionLcode + "' And EM.LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " And WOT.CompCode='" + SessionCcode + "' And EM.CompCode='" + SessionCcode + "'";
        SSQL = SSQL + " And Convert(Datetime,WOT.OTDate,103) >= CONVERT(Datetime,'" + FromDate + "',103)";
        SSQL = SSQL + " And Convert(Datetime,WOT.OTDate,103) <= CONVERT(Datetime,'" + ToDate + "',103)";
        SSQL = SSQL + " And WOT.Wages='" + WagesType + "' and EM.Wages='" + WagesType + "'";
        if (Division != "-Select-")
        {
            SSQL = SSQL + " And EM.Division = '" + Division + "'";

        }
        SSQL = SSQL + " group by EM.BaseSalary,WOT.Dept_Name,WOT.Designation,WOT.ExistingCode,EM.FirstName";
        SSQL = SSQL + " order by WOT.ExistingCode Asc";
        DT_MOT = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT_MOT.Rows.Count != 0)
        {
            for (i = 0; i < DT_MOT.Rows.Count; i++)
            {
                dtRow = AutoDTable.NewRow();
                dtRow["Basic"] = DT_MOT.Rows[i]["Basic"].ToString();
                dtRow["Dept"] = DT_MOT.Rows[i]["Dept"].ToString();
                dtRow["Designation"] = DT_MOT.Rows[i]["Designation"].ToString();
                dtRow["TokenNo"] = DT_MOT.Rows[i]["TokenNo"].ToString();
                dtRow["EmpName"] = DT_MOT.Rows[i]["EmpName"].ToString();
                dtRow["MonthOTHrs"] = DT_MOT.Rows[i]["MonthOTHrs"].ToString();

                //Get Spinning OT Hrs Start
                SSQL = "Select isnull(sum(cast(OTHrs as decimal(18,2))),0) as SPGTotalHrs from Weekly_OTHours where CompCode='" + SessionCcode + "'";
                SSQL = SSQL + " And LocCode='" + SessionLcode + "' And ExistingCode='" + DT_MOT.Rows[i]["TokenNo"].ToString() + "'";
                SSQL = SSQL + " And SPG_Inc='True'";
                SSQL = SSQL + " And Convert(Datetime,OTDate,103) >= CONVERT(Datetime,'" + FromDate + "',103)";
                SSQL = SSQL + " And Convert(Datetime,OTDate,103) <= CONVERT(Datetime,'" + ToDate + "',103)";
                OTHrs_DS = objdata.RptEmployeeMultipleDetails(SSQL);
                if (OTHrs_DS.Rows.Count != 0)
                {
                    dtRow["SPGTotalHrs"] = OTHrs_DS.Rows[0]["SPGTotalHrs"].ToString();
                }
                else
                {
                    dtRow["SPGTotalHrs"] = 0;
                }
                //Get Deptartment & Designation Incentive Rate
                SSQL = "Select OTIncAmt from Incentive_Mst where DeptName='" + DT_MOT.Rows[i]["Dept"].ToString() + "'";
                SSQL = SSQL + " And Designation='" + DT_MOT.Rows[i]["Designation"].ToString() + "' And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And Wages='" + WagesType + "'";
                OTHrs_DS = objdata.RptEmployeeMultipleDetails(SSQL);
                if (OTHrs_DS.Rows.Count != 0)
                {
                    dtRow["OTHrs_Rate"] = OTHrs_DS.Rows[0]["OTIncAmt"].ToString();
                }
                else
                {
                    dtRow["OTHrs_Rate"] = 0;
                }
                AutoDTable.Rows.Add(dtRow);
            }
        }
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        CrystalReportViewer1.Dispose();
    }
}

