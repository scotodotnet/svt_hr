﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class MstComissionHindi : System.Web.UI.Page
{
    static int CurrentYear;
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode; string SessionUser;
    string SessionDivision; string SessionRights;
    BALDataAccess objdata = new BALDataAccess();
    string ss1 = "";
    string SSQL = "";

    string SessionUserType;
    string Date1_str;
    string Date2_str;
    string Wages_Type;
    bool Errflag = false;
    bool Errflag1 = false;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionRights = Session["Rights"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report- Late IN Between Dates";
                LoadGroup();
            }
          
        }
    }

    private void LoadGroup()
    {
        
        SSQL = "Select * from MstHindiCat where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        ddlWages1.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlWages1.DataTextField = "CatName";
        ddlWages1.DataValueField = "CatCode";
        ddlWages1.DataBind();
        ddlWages1.Items.Insert(0, new ListItem("Select", "0"));
    }

    protected void btnCommissionSv_Click(object sender, EventArgs e)
    {
        bool ErrFlg = false;

        if (ddlWages1.SelectedItem.Text == "-Select-" )
        {
            ddlWages1.Focus();
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please Select the group');", true);
            return;
        }

        if (!ErrFlg)
        {
            string Query = "";
            Query = "Delete from MstCommissionHindi where GroupCD='" + ddlWages1.SelectedItem.Value + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";

            objdata.RptEmployeeMultipleDetails(Query);

            Query = "Insert into MstCommissionHindi values('" + SessionCcode + "','" + SessionLcode + "','" + ddlWages1.SelectedItem.Value + "','" + ddlWages1.SelectedItem.Text.ToUpper() + "','" + txtAmount.Text + "');";
            objdata.RptEmployeeMultipleDetails(Query);
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Data Saved Successfully!!!');", true);
        }
    }

    protected void btnCommissionClr_Click(object sender, EventArgs e)
    {
        ddlWages1.ClearSelection();
        txtAmount.Text = "";
    }

    protected void ddlWages1_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlWages1.SelectedItem.Text != "Select")
        {
            SSQL = "";
            SSQL = "Select * from MstCommissionHindi where GroupCD='" + ddlWages1.SelectedItem.Value + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
            DataTable dt = new DataTable();
            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt.Rows.Count > 0)
            {
                if (dt.Rows[0]["Amt"].ToString() != "")
                {
                    txtAmount.Text = dt.Rows[0]["Amt"].ToString();
                }else
                {
                    txtAmount.Text = "0";
                }
            }else
            {
                txtAmount.Text = "0";
            }
        }
    }
}