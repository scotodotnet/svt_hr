﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.IO;
using System.Data.SqlClient;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.Collections.Generic;
using System.IO;
public partial class unitSalaryDisplay : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string Sessionyear;
    string SessionMonth;

    string SSQL;

    DataTable mDataSet = new DataTable();
    DataTable dt = new DataTable();
    BALDataAccess objdata = new BALDataAccess();
    DataTable AutoDataTable = new DataTable();
    DataSet ds = new DataSet();
    string Division;
    string SessionPayroll;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            Load_DB();
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Report-Unitwise Salary SummaryReport";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("SalaryProcess"));
                //li.Attributes.Add("class", "droplink active open");
            }


            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            Sessionyear= Request.QueryString["year"].ToString();
            SessionMonth = Request.QueryString["Month"].ToString();
            Report();
        }
    }

    public void Load_DB()
    {
        //Get Database Name
        string query = "";
        DataTable dt_DB = new DataTable();
        query = "select CMS,Rights,Sales,Stores,Weaving,Maintaince,Production,Payroll from [SVT_Rights]..MstDBname";
        dt_DB = objdata.RptEmployeeMultipleDetails(query);
        if (dt_DB.Rows.Count > 0)
        {
            SessionPayroll = dt_DB.Rows[0]["Payroll"].ToString();
            //SessionRights = dt_DB.Rows[0]["Rights"].ToString();
        }
    }
    public void Report()
    {
        AutoDataTable.Columns.Add("CompanyName");
        AutoDataTable.Columns.Add("LocationName");
        AutoDataTable.Columns.Add("SNo");
        AutoDataTable.Columns.Add("Dept");
        AutoDataTable.Columns.Add("UnitI");
        AutoDataTable.Columns.Add("UnitII");
        AutoDataTable.Columns.Add("UnitIII");
        AutoDataTable.Columns.Add("UnitIV");


        SSQL = "select DeptName,isnull([UNIT I],0) as UnitI,isnull([UNIT II],0) as UnitII,";
        SSQL = SSQL + " isnull([UNIT III],0) as UnitIII,isnull([UNIT IV],0) as UnitIV from ";
        SSQL = SSQL + " (Select EM.DeptName ,(SD.Lcode) as Shift_Join,Sum(SD.RoundOffNetPay)as Salary  from Employee_Mst EM ";
        SSQL = SSQL + " inner Join  [" + SessionPayroll + "]..SalaryDetails SD ";
        SSQL = SSQL + " on EM.EmpNo=SD.EmpNo  where EM.CompCode='" + SessionCcode + "' and SD.Ccode='" + SessionCcode + "'and SD.Month='" + SessionMonth + "'and SD.Year='" + Sessionyear + "' ";
        SSQL = SSQL + " group by EM.DeptName,SD.Lcode )as p pivot (max(Salary) for Shift_Join ";
        SSQL = SSQL + " in ( [UNIT I],[UNIT II],[UNIT III],[UNIT IV] )) as pvtt";
        mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);
        if (mDataSet.Rows.Count != 0)
        {
            for (int i = 0; i < mDataSet.Rows.Count; i++)
            {
                SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "' ";
                dt = objdata.RptEmployeeMultipleDetails(SSQL);
                string name = dt.Rows[0]["CompName"].ToString();

                AutoDataTable.NewRow();
                AutoDataTable.Rows.Add();
                AutoDataTable.Rows[i]["CompanyName"] = name;
                AutoDataTable.Rows[i]["LocationName"] = SessionLcode;
                AutoDataTable.Rows[i]["SNo"] = i + 1;
                AutoDataTable.Rows[i]["Dept"] = mDataSet.Rows[i]["DeptName"].ToString();
                AutoDataTable.Rows[i]["UnitI"] = mDataSet.Rows[i]["UnitI"].ToString();
                AutoDataTable.Rows[i]["UnitII"] = mDataSet.Rows[i]["UnitII"].ToString();
                AutoDataTable.Rows[i]["UnitIII"] = mDataSet.Rows[i]["UnitIII"].ToString();
                AutoDataTable.Rows[i]["UnitIV"] = mDataSet.Rows[i]["UnitIV"].ToString();

            }
            ds.Tables.Add(AutoDataTable);
            ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("crystal/unitwiseSalary.rpt"));
            
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Records Found');", true);
        }
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        CrystalReportViewer1.Dispose();
    }
}
