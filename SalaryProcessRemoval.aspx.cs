﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Payroll;
using Payroll.Configuration;
using Payroll.Data;
using Altius.BusinessAccessLayer.BALDataAccess;
public partial class SalaryProcessRemoval : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string Query;
    string mydate;
    string SessionPayroll;
    DataTable dt = new DataTable();
    DataTable dt1 = new DataTable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }

        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        Load_DB();
        if (!IsPostBack)
        {
            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddlFinYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
               currentYear = currentYear - 1;
            }
            Load_WagesCode();
            ddlWagesType_SelectedIndexChanged(sender, e);
        }
    }
    public void Load_DB()
    {
        //Get Database Name
        string query = "";
        DataTable dt_DB = new DataTable();
        query = "select CMS,Rights,Sales,Stores,Weaving,Maintaince,Production,Payroll from [SVT_Rights]..MstDBname";
        dt_DB = objdata.RptEmployeeMultipleDetails(query);
        if (dt_DB.Rows.Count > 0)
        {
            SessionPayroll = dt_DB.Rows[0]["Payroll"].ToString();
            //SessionRights = dt_DB.Rows[0]["Rights"].ToString();
        }
    }
    public void Load_WagesCode()
    {
        DataTable dtdsupp = new DataTable();
        ddlWagesType.Items.Clear();
        Query = "Select EmpTypeCd,EmpType from MstEmployeeType  ";
        dtdsupp = objdata.RptEmployeeMultipleDetails(Query);
        ddlWagesType.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpType"] = "-Select-";
        dr["EmpTypeCd"] = "0";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlWagesType.DataTextField = "EmpType";
        ddlWagesType.DataValueField = "EmpTypeCd";
        ddlWagesType.DataBind();
    }
    protected void ddlWagesType_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dtdsupp = new DataTable();
        ddlExistingCode.Items.Clear();
        Query = "Select ExistingCode from Employee_Mst where Wages='" + ddlWagesType.SelectedItem.Text + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' And IsActive='Yes'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(Query);
        ddlExistingCode.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["ExistingCode"] = "-Select-";
        dr["ExistingCode"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlExistingCode.DataTextField = "ExistingCode";
        ddlExistingCode.DataValueField = "ExistingCode";
        ddlExistingCode.DataBind();
        Load_TwoDates();
    }
    protected void ddlMonth_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_TwoDates();
    }
    protected void ddlFinYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_TwoDates();
    }
    public void Load_TwoDates()
    {

        if (ddlMonth.SelectedValue != "-Select-" && ddlFinYear.SelectedValue != "-Select-" && ddlWagesType.SelectedValue != "0")
        {
            if (ddlWagesType.SelectedValue != "2" && ddlWagesType.SelectedValue != "5" && ddlWagesType.SelectedValue != "10")
            {
                decimal Month_Total_days = 0;
                string Month_Last = "";
                string Year_Last = "0";
                string Temp_Years = "";
                string[] Years;
                string FromDate = "";
                string ToDate = "";

                Temp_Years = ddlFinYear.SelectedValue;
                Years = Temp_Years.Split('-');
                if ((ddlMonth.SelectedValue == "January") || (ddlMonth.SelectedValue == "March") || (ddlMonth.SelectedValue == "May") || (ddlMonth.SelectedValue == "July") || (ddlMonth.SelectedValue == "August") || (ddlMonth.SelectedValue == "October") || (ddlMonth.SelectedValue == "December"))
                {
                    Month_Total_days = 31;
                }
                else if ((ddlMonth.SelectedValue == "April") || (ddlMonth.SelectedValue == "June") || (ddlMonth.SelectedValue == "September") || (ddlMonth.SelectedValue == "November"))
                {
                    Month_Total_days = 30;
                }

                else if (ddlMonth.SelectedValue == "February")
                {
                    int yrs = (Convert.ToInt32(Years[0]) + 1);
                    if ((yrs % 4) == 0)
                    {
                        Month_Total_days = 29;
                    }
                    else
                    {
                        Month_Total_days = 28;
                    }
                }
                switch (ddlMonth.SelectedItem.Text)
                {
                    case "January": Month_Last = "01";
                        break;
                    case "February": Month_Last = "02";
                        break;
                    case "March": Month_Last = "03";
                        break;
                    case "April": Month_Last = "04";
                        break;
                    case "May": Month_Last = "05";
                        break;
                    case "June": Month_Last = "06";
                        break;
                    case "July": Month_Last = "07";
                        break;
                    case "August": Month_Last = "08";
                        break;
                    case "September": Month_Last = "09";
                        break;
                    case "October": Month_Last = "10";
                        break;
                    case "November": Month_Last = "11";
                        break;
                    case "December": Month_Last = "12";
                        break;
                    default:
                        break;
                }

                if ((ddlMonth.SelectedValue == "January") || (ddlMonth.SelectedValue == "February") || (ddlMonth.SelectedValue == "March"))
                {
                    Year_Last = Years[0];
                }
                else
                {
                    Year_Last = Years[0];
                }
                FromDate = "01" + "/" + Month_Last + "/" + Year_Last;
                ToDate = Month_Total_days + "/" + Month_Last + "/" + Year_Last;

                txtFromDate.Text = FromDate.ToString();
                txtToDate.Text = ToDate.ToString();

            }
            else
            {
                txtFromDate.Text = "";
                txtToDate.Text = "";
            }

        }
    }
    protected void btnDelete_Click(object sender, EventArgs e)
    {
        bool attdelete = false;
        bool saldelete = false;
        Query="Select * from [" + SessionPayroll + "]..AttenanceDetails where ExistingCode='"+ddlExistingCode.SelectedItem.Text +"' ";
        Query = Query + " and Months='"+ ddlMonth.Text +"'  and FinancialYear='"+ ddlFinYear.SelectedValue +"' and ";
        Query = Query + " convert(datetime,FromDate,105) = convert(datetime,'" + txtFromDate.Text + "',105) and convert(datetime,TODate,105) = convert(datetime,'" + txtToDate.Text + "',105) ";
        dt = objdata.RptEmployeeMultipleDetails(Query);

        if (dt.Rows.Count != 0)
        {
            Query = "Delete from [" + SessionPayroll + "]..AttenanceDetails where ExistingCode='" + ddlExistingCode.SelectedItem.Text + "' ";
            Query = Query + " and Months='" + ddlMonth.Text + "'  and FinancialYear='" + ddlFinYear.SelectedValue + "' and ";
            Query = Query + " convert(datetime,FromDate,105) = convert(datetime,'" + txtFromDate.Text + "',105) and convert(datetime,TODate,105) = convert(datetime,'" + txtToDate.Text + "',105) ";
             objdata.RptEmployeeMultipleDetails(Query);
             attdelete = true;
        }

        Query = "Select * from [" + SessionPayroll + "]..SalaryDetails where ExisistingCode='" + ddlExistingCode.SelectedItem.Text + "' ";
        Query = Query + " and Month='" + ddlMonth.Text + "'  and FinancialYear='" + ddlFinYear.SelectedValue + "' and ";
        Query = Query + " convert(datetime,FromDate,105) = convert(datetime,'" + txtFromDate.Text + "',105) and convert(datetime,TODate,105) = convert(datetime,'" + txtToDate.Text + "',105) ";
        dt1 = objdata.RptEmployeeMultipleDetails(Query);

        if (dt1.Rows.Count != 0)
        {
            Query = "Delete from [" + SessionPayroll + "]..SalaryDetails where ExisistingCode='" + ddlExistingCode.SelectedItem.Text + "' ";
            Query = Query + " and Month='" + ddlMonth.Text + "'  and FinancialYear='" + ddlFinYear.SelectedValue + "' and ";
            Query = Query + " convert(datetime,FromDate,105) = convert(datetime,'" + txtFromDate.Text + "',105) and convert(datetime,TODate,105) = convert(datetime,'" + txtToDate.Text + "',105) ";
            objdata.RptEmployeeMultipleDetails(Query);
            saldelete = true;
        }
        if (attdelete == true && saldelete == true)
        {
    
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Deleted Successfully....');", true);
            Clear();
         
        }
        else if (attdelete == false)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Data found in Attendance Details....!');", true);
         
           
        }
        else if (saldelete == false)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Data found in Salary Details....!');", true);
         
        }              
    }
    public void Clear()
    {
        ddlExistingCode.SelectedValue = "-Select-";
        ddlWagesType.SelectedValue = "0";
        ddlMonth.SelectedValue = "-Select-";
        txtFromDate.Text = "";
        txtToDate.Text = "";
    }
}
