﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master"  AutoEventWireup="true" CodeFile="OnDutyProcessAdd.aspx.cs" Inherits="OnDutyProcess" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">



<script src="assets/js/master_list_jquery.min.js"></script>
<script src="assets/js/master_list_jquery-ui.min.js"></script>
<link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css"/>
 <script>
     $(document).ready(function() {
     $('#example').dataTable();
     $('.datepicker').datepicker({
         format: "dd/mm/yyyy",
         autoclose: true
     });
     });
 </script>


<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
                $('.select2').select2();
                $('.datepicker').datepicker({
                    format: "dd/mm/yyyy",
                    autoclose: true
                });
            }
        });
    };
</script>
<asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                <ContentTemplate>
<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Manual</a></li>
				<li class="active">On Duty</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">On Duty</h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">On Duty</h4>
                        </div>
                        <div class="panel-body">
                        <div class="form-group">
                        <!-- begin row -->
                          <div class="row">
                            <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>TransID</label>
								<asp:TextBox runat="server" ID="txtTransid" class="form-control"></asp:TextBox>
								  <asp:RequiredFieldValidator ControlToValidate="txtTransid" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
								</div>
                               </div>
                           <!-- end col-4 -->
                           <!-- begin col-4 -->
                         <%--  --%>
                             <div class="col-md-4">
							   <div class="form-group">
								<label>Token No</label>
								 <asp:DropDownList runat="server" ID="ddlTokenNo" class="form-control select2" 
                                        style="width:100%;" AutoPostBack="true" 
                                        onselectedindexchanged="ddlTokenNo_SelectedIndexChanged" >
							 	 </asp:DropDownList>
							 	 <asp:RequiredFieldValidator ControlToValidate="ddlTokenNo" InitialValue="-Select-" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator6" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                           </asp:RequiredFieldValidator>
								</div>
                             </div>
                          
                           <!-- end col-4 -->
                           <!-- begin col-4 -->
                             <div class="col-md-4">
							   <div class="form-group">
								<label>EmpName</label>
								<asp:TextBox runat="server" ID="txtEmpName" class="form-control" Enabled="false"></asp:TextBox>
								</div>
                             </div>
                        
                           <!-- end col-4 -->
                          
                          </div>
                        <!-- end row -->
                       <!-- begin row -->
                          <div class="row">
                          <!-- begin col-4 -->
                             <div class="col-md-4">
                            <div class="form-group">
                           <label>ONDuty FromDate</label>
                          	<asp:TextBox runat="server" ID="txtFromDate" class="form-control datepicker" placeholder="dd/MM/YYYY"></asp:TextBox>
										<asp:RequiredFieldValidator ControlToValidate="txtFromDate" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
                                       
                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                TargetControlID="txtFromDate" ValidChars="0123456789/">
                                                            </cc1:FilteredTextBoxExtender>
                          </div>
                         </div>
                           <!-- end col-4 -->
                           <!-- begin col-4 -->
                             <div class="col-md-4">
                            <div class="form-group">
                           <label>ONDuty ToDate</label>
                          	<asp:TextBox runat="server" ID="txtToDate" class="form-control datepicker" placeholder="dd/MM/YYYY"></asp:TextBox>
										<asp:RequiredFieldValidator ControlToValidate="txtToDate" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
                                       
                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                TargetControlID="txtToDate" ValidChars="0123456789/">
                                                            </cc1:FilteredTextBoxExtender>
                          </div>
                         </div>
                           <!-- end col-4 -->
                            <!-- begin col-4 -->
                            <div class="col-md-4">
							   <div class="form-group">
								<label>Days</label>
								<asp:TextBox runat="server" ID="txtDays" class="form-control"></asp:TextBox>
								</div>
                             </div>
                           <!-- end col-4 -->
                           </div>
                        <!-- end row -->
                         <!-- begin row -->
                          <div class="row">
                             <!-- begin col-4 -->
                                 <div class="col-md-4">
									<div class="form-group">
										<label>FromArea</label>
								<asp:TextBox runat="server" ID="txtFromArea" class="form-control"></asp:TextBox>
								 <asp:RequiredFieldValidator ControlToValidate="txtFromArea" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
							 	
									</div>
                                  </div>
                               <!-- end col-4 -->
                               <!-- begin col-4 -->
                                 <div class="col-md-4">
									<div class="form-group">
										<label>ToArea</label>
										<asp:TextBox runat="server" ID="txtToArea" class="form-control"></asp:TextBox>
										 <asp:RequiredFieldValidator ControlToValidate="txtToArea" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
									</div>
                                  </div>
                               <!-- end col-4 -->
                                <!-- begin col-4 -->
                                  <div class="col-md-4">
									<div class="form-group">
										<label>Reason</label>
										<asp:TextBox runat="server" ID="txtReason" class="form-control" TextMode="MultiLine" ></asp:TextBox>
										</div>
                                  </div>
                                    <!-- end col-4 -->
                                <!-- begin col-4 -->
                                 <div class="col-md-4">
									<div class="form-group">
										<label>Veichle No</label>
										<asp:TextBox runat="server" ID="txtVechileno" class="form-control"></asp:TextBox>
										</div>
                                  </div>
                                    <!-- end col-4 -->
                                <!-- begin col-4 -->
                                 <div class="col-md-4">
									<div class="form-group">
										<label>Travel Charge</label>
										<asp:TextBox runat="server" ID="txtCharge" class="form-control" Text="0"></asp:TextBox>
										 <asp:RequiredFieldValidator ControlToValidate="txtCharge" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator7" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
									</div>
                                  </div>
                                
                                  <div class="col-md-4">
									<div class="form-group">
										<label>OnDuty Return Date</label>
										<asp:TextBox runat="server" ID="txtReturnDate" class="form-control datepicker" placeholder="dd/MM/YYYY" ></asp:TextBox>
									</div>
                                  </div>
                                  <!-- end col-4 -->
                                <!-- begin col-4 -->
                          </div>
                       <!-- end row -->  
                      <!-- begin row -->  
                        <div class="row">
                       
                         <div class="col-md-4">
                        
                         </div>
                         
                              <div class="col-md-4"></div>
                         </div>
                        <!-- end row --> 
                      
                 
                       <!-- begin row -->  
                        <div class="row">
                         <div class="col-md-4"></div>
                           <!-- begin col-4 -->
                                <div class="col-md-4">
								 <div class="form-group">
									<br />
									
									<asp:Button runat="server" id="btnSave" Text="Save" class="btn btn-success" 
                                        ValidationGroup="Validate_Field" onclick="btnSave_Click" />
									<asp:Button runat="server" id="btnClear" Text="Clear" class="btn btn-danger" 
                                         onclick="btnClear_Click"  />
                                         	<asp:Button runat="server" id="btnBack" Text="Back" class="btn btn-warning" 
                                          onclick="btnBack_Click" />
								 </div>
                               </div>
                              <!-- end col-4 -->
                              <div class="col-md-4"></div>
                         </div>
                        <!-- end row -->   
                        </div>
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
<!-- end #content -->
</ContentTemplate>
</asp:UpdatePanel>



</asp:Content>

