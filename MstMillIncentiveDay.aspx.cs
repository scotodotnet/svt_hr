﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class MstMillIncentiveDay : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SSQL = "";
    bool ErrFlag = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Mill Incentive Master";
            Load_Shift();
        }
        Load_Data();
    }

    private void Load_Shift()
    {
        SSQL = "";
        SSQL = "Select * from Shift_mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        DataTable dt_Shift = new DataTable();
        dt_Shift = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlShift.DataSource = dt_Shift;
        ddlShift.DataTextField = "ShiftDesc";
        ddlShift.DataValueField = "ShiftDesc";
        ddlShift.DataBind();
        ddlShift.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }

    private void Load_Data()
    {
        SSQL = "";
        SSQL = "Select * from MstMillIncentiveDay where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (txtDate.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('please Select the Date Properly')", true);
            return;
        }
        if (txtShortName.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Insert the Short Name')", true);
            return;
        }
        if (ddlShift.SelectedValue == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please select the Shift')", true);
            return;
        }
        if (ddlPresent.SelectedValue == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please select the Present')", true);
            return;
        }
        if (!ErrFlag)
        {
            SSQL = "";
            SSQL = "Select * from MstMillIncentiveDay where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " and Shift='" + ddlShift.SelectedValue + "' and Convert(date,date,103)=Convert(date,'" + txtDate.Text + "',103)";
            DataTable dt_check = new DataTable();
            dt_check = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt_check.Rows.Count > 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('The Details Already Present on a date and Shift')", true);
                return;
            }
            if (!ErrFlag)
            {
                SSQL = "";
                SSQL = "insert into MstMillIncentiveDay(Ccode,Lcode,Date,Shift,ShortName,Description,Present)";
                SSQL = SSQL + " values('" + SessionCcode + "','" + SessionLcode + "','" + txtDate.Text + "','" + ddlShift.SelectedValue + "','" + txtShortName.Text + "','" + txtDescription.Text + "','" + ddlPresent.SelectedValue + "')";
                objdata.RptEmployeeMultipleDetails(SSQL);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Data Saved Successfully!!!')", true);
                Load_Data();
                btnClear_Click(sender, e);
            }
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtDate.Text = "";
        txtDescription.Text = "";
        txtShortName.Text = "";
        ddlShift.ClearSelection();
        ddlPresent.ClearSelection();
        hiddenID.Value = "";
    }

    protected void btnDeleteEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {
        SSQL = "";
        SSQL = "Delete from MstMillIncentiveDay where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and AutoID='" + e.CommandName.ToString() + "'";
        objdata.RptEmployeeMultipleDetails(SSQL);
        Load_Data();
    }
}