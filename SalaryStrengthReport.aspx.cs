﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Globalization;

public partial class SalaryStrengthReport : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string SessionEpay;
    string Month;
    string Status;
    string TokenNo = "";
    string FinYearVal = "";
    string FinYearCode = "";

    DataTable dt_Cat = new DataTable();

    BALDataAccess objdata = new BALDataAccess();

    string SSQL = "";
   

    System.Web.UI.WebControls.DataGrid grid =
                            new System.Web.UI.WebControls.DataGrid();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {

            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report- Muster Report Between Dates";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("ManualEntry"));
                //li.Attributes.Add("class", "droplink active open");
            }


            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            SessionEpay = Session["SessionEpay"].ToString();

            FinYearCode = Request.QueryString["FinYearCode"].ToString();
            FinYearVal = Request.QueryString["FinYearVal"].ToString();
            Month = Request.QueryString["Month"].ToString();
           // TokenNo = Request.QueryString["TokenNo"].ToString();

            string SessionUserType_1 = SessionUserType;

            if (SessionUserType_1 == SessionUserType)
            {
                Report();
            }
        }
    }

    private void Report()
    {
        DataTable AutoDTable = new DataTable();
        AutoDTable.Columns.Add("DeptName");
        AutoDTable.Columns.Add("StaffStrength");
        AutoDTable.Columns.Add("StaffSalary");
        AutoDTable.Columns.Add("LabourStrength");
        AutoDTable.Columns.Add("LabourSalary");
        AutoDTable.Columns.Add("TotalStrength");
        AutoDTable.Columns.Add("TotalSalary");

        SSQL = "";
        SSQL = "select deptname from Department_Mst order by deptname";
        DataTable Depet_dt = new DataTable();
        Depet_dt = objdata.RptEmployeeMultipleDetails(SSQL);

        if (Depet_dt.Rows.Count > 0)
        {
            for (int i = 0; i < Depet_dt.Rows.Count; i++)
            {
                AutoDTable.NewRow();
                AutoDTable.Rows.Add();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["DeptName"] = Depet_dt.Rows[i]["Deptname"].ToString();
            }
        }

        SSQL = "";
        SSQL = "Select DISTINCT CatName from Employee_Mst where Compcode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and IsActive='Yes'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);

        if (Depet_dt.Rows.Count > 0)
        {
            for (int dept = 0; dept < Depet_dt.Rows.Count; dept++)
            {
                string TotalStrength = "0";
                string TotalSalary = "0";

                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        string CatName = dt.Rows[i]["CatName"].ToString();
                       
                        SSQL = "";
                        SSQL = "Select EM.DeptName, isnull(Count(distinct SD.MachineNo),0) as Present,isnull(Sum(SD.NetPay),0) as Salary from [" + SessionEpay + "]..SalaryDetails SD inner join Employee_Mst Em on SD.MachineNo=EM.MachineID where SD.CCode='" + SessionCcode + "' and SD.LCode='" + SessionLcode + "'";
                        SSQL = SSQL + " and Em.CatName='" + CatName + "' and EM.DeptName='" + Depet_dt.Rows[dept]["DeptName"].ToString() + "' and SD.Month='" + Month + "' and SD.FinancialYear='" + FinYearCode + "' group by EM.DeptName order by EM.DeptName";
                        DataTable lab = new DataTable();
                        lab = objdata.RptEmployeeMultipleDetails(SSQL);

                        if (lab.Rows.Count > 0)
                        {
                            for (int j = 0; j < lab.Rows.Count; j++)
                            {
                                if (CatName == "STAFF")
                                {
                                    if (AutoDTable.Rows[dept]["DeptName"].ToString() == Depet_dt.Rows[dept]["DeptName"].ToString())
                                    {
                                        AutoDTable.Rows[dept]["StaffStrength"] = lab.Rows[j]["Present"].ToString();
                                        AutoDTable.Rows[dept]["StaffSalary"] = lab.Rows[j]["Salary"].ToString();

                                        TotalStrength = (Convert.ToDecimal(TotalStrength.ToString()) + Convert.ToDecimal(lab.Rows[j]["Present"].ToString())).ToString();
                                        TotalSalary = (Convert.ToDecimal(TotalSalary.ToString()) + Convert.ToDecimal(lab.Rows[j]["Salary"].ToString())).ToString();
                                    }
                                }
                                else if (CatName == "LABOUR")
                                {
                                    if (AutoDTable.Rows[dept]["DeptName"].ToString() == Depet_dt.Rows[dept]["DeptName"].ToString().Trim())
                                    {
                                        AutoDTable.Rows[dept]["labourStrength"] = lab.Rows[j]["Present"].ToString();
                                        AutoDTable.Rows[dept]["labourSalary"] = lab.Rows[j]["Salary"].ToString();

                                         TotalStrength = (Convert.ToDecimal(TotalStrength.ToString()) + Convert.ToDecimal(lab.Rows[j]["Present"].ToString())).ToString();
                                        TotalSalary = (Convert.ToDecimal(TotalSalary.ToString()) + Convert.ToDecimal(lab.Rows[j]["Salary"].ToString())).ToString();
                                    }
                                }
                            }
                        }
                    }
                }
                if (AutoDTable.Rows[dept]["DeptName"].ToString() == Depet_dt.Rows[dept]["DeptName"].ToString().Trim())
                {
                    AutoDTable.Rows[dept]["TotalStrength"] = TotalStrength.ToString();
                    AutoDTable.Rows[dept]["TotalSalary"] = TotalSalary.ToString();
                }
                for (int nullchk = 1; nullchk < AutoDTable.Columns.Count; nullchk++)
                {
                    if (AutoDTable.Rows[dept][nullchk].ToString() == "" | AutoDTable.Rows[dept][nullchk].ToString() == string.Empty | AutoDTable.Rows[dept][nullchk].ToString() == null)
                    {
                        AutoDTable.Rows[dept][nullchk] = "0";
                    }
                }
            }
        }

        if (AutoDTable.Rows.Count > 0)
        {
            SSQL = "";
            SSQL = SSQL + "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionEpay + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            //dt = objdata.RptEmployeeMultipleDetails(SSQL);
            dt_Cat = objdata.RptEmployeeMultipleDetails(SSQL);
            string CmpName = "";
            string Cmpaddress = "";
            if (dt.Rows.Count > 0)
            {
                CmpName = dt_Cat.Rows[0]["Cname"].ToString();
                Cmpaddress = (dt_Cat.Rows[0]["Location"].ToString());
            }

            grid.DataSource = AutoDTable;
            grid.DataBind();
            string attachment = "attachment;filename=SALARY STRENGTH REPORT.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);
            Response.Write("<table>");
         
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='7'>");
            Response.Write("<a style=\"font-weight:bold\">"+ CmpName + "</a>");

            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='7'>");
            Response.Write("<a style=\"font-weight:bold\">" + Cmpaddress + "</a>");

            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='7'>");
            Response.Write("<a style=\"font-weight:bold\">SALARY STRENGTH REPORT FOR THE MONTH OF " + Month + " - " + FinYearCode + "</a>");

            Response.Write("</td>");
            
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true'>");
            Response.Write("<td colspan='5'>");
            Response.Write("<a style=\"font-weight:bold\">MONTH - " + Month + "</a>");

            Response.Write("</td>");
            Response.Write("<td align='right' colspan='2'>");
            Response.Write("<a style=\"font-weight:bold\">FINANCIAL YEAR - " + FinYearVal + "</a>");

            Response.Write("</td>");
            Response.Write("</tr>");

            Response.Write("</table>");
            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();
        }
    }
}