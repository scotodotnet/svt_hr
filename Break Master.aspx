﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PayrollMaster.master" AutoEventWireup="true" CodeFile="Break Master.aspx.cs" Inherits="Break_Master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


<script src="assets/js/master_list_jquery.min.js"></script>
<script src="assets/js/master_list_jquery-ui.min.js"></script>
<link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css"/>
<%--<script src="assets/js/form-wizards.demo.min.js"></script>--%>
 <script type="text/javascript">
     $(document).ready(function() {
         $('#example').dataTable();
      
     });
	</script>
<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.select2').select2();
                $('#example').dataTable();
            }
        });
    };
</script>
<asp:UpdatePanel ID="UpdatePanel5" runat="server">
 <ContentTemplate>
<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Break Rate Entry</a></li>
				<li class="active">Break Rate Entry</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Break Rate Entry</h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">Break Rate Entry</h4>
                        </div>
                        <div class="panel-body">
                       
                        <!-- begin row -->
			            <div class="row"> 
			            <div class="col-md-3">
			            <div class="form-group">
                          <label>Wages</label>
                          <asp:DropDownList ID="ddlWages" runat="server"  class="form-control select2" style="width:100%">
                          </asp:DropDownList>
                        </div>
			            </div>
                        </div>
                        <!-- end row -->
                        
                          <!-- begin row -->
			            <div class="row"> 
			             <!-- begin col-3 -->
			            <div class="col-md-2">
			            <div class="form-group">
                          <label>Lunch Rate</label>
                          <asp:TextBox ID="txtLunch" runat="server" CssClass="form-control" ></asp:TextBox>
                        </div>
			            </div>
                       <!-- end col-3 -->
                        </div>
                        <!-- end row -->
                               <!-- begin row -->
			            <div class="row"> 
			             <!-- begin col-3 -->
			            <div class="col-md-2">
			            <div class="form-group">
                            <asp:Button ID="btnLuch" runat="server" CssClass="btn btn-success" OnClick="btnLuch_Click" Text="Save" />
                        
                            <asp:Button ID="btnLunchClr" runat="server" CssClass="btn btn-danger" OnClick="btnLunchClr_Click" Text="Clear" />
                        </div>
			            </div>
                       <!-- end col-3 -->
                        </div>
                        <!-- end row -->

                            <!-- table start -->
					<div class="col-md-12">
					    <div class="row">
					        <asp:Repeater ID="RepeaterLunch" runat="server" EnableViewState="false">
			                    <HeaderTemplate>
                                    <table id="example" class="display table">
                                        <thead>
                                            <tr>
                                                <th>Wages</th>
                                                <th>Rate</th>
                                                <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                    <td><%# Eval("Wages")%></td>
                                    <td><%# Eval("Luch")%></td>
                                    
                                    <td>
                                     <asp:LinkButton ID="btnApprvEnquiry_Grid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                                           Text="" OnCommand="btnApprvEnquiry_Grid_Command" CommandArgument="Lunch_Edit" CommandName='<%# Eval("Wages")%>'
                                           CausesValidation="true" >
                                     </asp:LinkButton>
                                    
                                     
                                     <asp:LinkButton ID="btnCancelEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash"  runat="server" 
                                        Text="" OnCommand="btnCancelEnquiry_Grid_Command" CommandArgument="Lunch_Delete" CommandName='<%# Eval("Wages")%>' 
                                       CausesValidation="true" OnClientClick="return confirm('Are you sure you want to Cancel this Employee details?');">
                                     </asp:LinkButton>
                                    
                                    </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater>
					    </div>
					</div>
					<!-- table End -->




                              <!-- begin row -->
			            <div class="row"> 
			            <div class="col-md-3">
			            <div class="form-group">
                          <label>Wages</label>
                          <asp:DropDownList ID="ddlWage1" runat="server"  class="form-control select2" style="width:100%">
                          </asp:DropDownList>
                        </div>
			            </div>
                        </div>
                        <!-- end row -->
                        
                          <!-- begin row -->
			            <div class="row"> 
			             <!-- begin col-3 -->
			            <div class="col-md-2">
			            <div class="form-group">
                          <label>OT Rate</label>
                          <asp:TextBox ID="txtOT" runat="server" CssClass="form-control" ></asp:TextBox>
                        </div>
			            </div>
                       <!-- end col-3 -->
                        </div>
                        <!-- end row -->
                               <!-- begin row -->
			            <div class="row"> 
			             <!-- begin col-3 -->
			            <div class="col-md-2">
			            <div class="form-group">
                            <asp:Button ID="btnOT" runat="server" CssClass="btn btn-success" OnClick="btnOT_Click" Text="Save" />
                        
                            <asp:Button ID="btnOTClr" runat="server" CssClass="btn btn-danger" OnClick="btnOTClr_Click" Text="Clear" />
                        </div>
			            </div>
                       <!-- end col-3 -->
                        </div>
                        <!-- end row -->

                            <!-- table start -->
					<div class="col-md-12">
					    <div class="row">
					        <asp:Repeater ID="RepeaterOT" runat="server" EnableViewState="false">
			                    <HeaderTemplate>
                                    <table id="example" class="display table">
                                        <thead>
                                            <tr>
                                                <th>Wages</th>
                                                <th>Rate</th>
                                                <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                    <td><%# Eval("Wages")%></td>
                                    <td><%# Eval("OT")%></td>
                                    
                                    <td>
                                     <asp:LinkButton ID="btnApprvEnquiry_Grid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                                           Text="" OnCommand="btnApprvEnquiry_Grid_Command" CommandArgument="OT_Edit" CommandName='<%# Eval("Wages")%>'
                                           CausesValidation="true" >
                                     </asp:LinkButton>
                                    
                                     
                                     <asp:LinkButton ID="btnCancelEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash"  runat="server" 
                                        Text="" OnCommand="btnCancelEnquiry_Grid_Command" CommandArgument="OT_Delete" CommandName='<%# Eval("Wages")%>' 
                                       CausesValidation="true" OnClientClick="return confirm('Are you sure you want to Cancel this Employee details?');">
                                     </asp:LinkButton>
                                    
                                    </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater>
					    </div>
					</div>
					<!-- table End -->
                      
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
<!-- end #content -->

</ContentTemplate>
</asp:UpdatePanel>


</asp:Content>


