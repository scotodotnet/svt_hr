﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class TrainLevelChanged : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        if (!IsPostBack)
        {
            Load_TraineeLevel();
            Load_SemiExpLevel();
            Load_ReTraineeLevel();
        }
    }

    public void Load_TraineeLevel()
    {
        string query = "";
        DataTable dt = new DataTable();

        query = "Select ED.EmpNo,ED.MachineID,ED.ExistingCode,convert(varchar,ED.DOJ,103) as DOJ,MT.Days,";
        query= query + "convert(varchar,GETDATE(),103) as CurrDate from Employee_Mst ED inner join MstTrainingDays MT on ED.DeptCode=MT.DeptCode";
        query = query + " where ED.CompCode='" + SessionCcode + "' And ED.LocCode='" + SessionLcode + "' And ED.IsActive='Yes' And ED.Training_Status='1'";
        query = query + " And MT.Ccode='" + SessionCcode + "' And MT.Lcode='" + SessionLcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(query);

        if (dt.Rows.Count != 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DateTime TraineDate = new DateTime();
                int days = Convert.ToInt32(dt.Rows[i]["Days"]);
                TraineDate = Convert.ToDateTime(dt.Rows[i]["DOJ"].ToString()).AddDays(days);
                string currDate = dt.Rows[i]["CurrDate"].ToString();
                if (TraineDate.ToString("dd/MM/yyyy") == dt.Rows[i]["CurrDate"].ToString())
                {
                    query = "insert into Training_Level_Change(Ccode,Lcode,EmpNo,MachineID,ExistingCode,";
                    query = query + "Training_Level,Level_Date)values('" + SessionCcode + "','" + SessionLcode + "',";
                    query = query + "'" + dt.Rows[i]["EmpNo"].ToString() + "','" + dt.Rows[i]["MachineID"].ToString() + "',";
                    query = query + "'" + dt.Rows[i]["ExistingCode"].ToString() + "','SemiSkilled','" + currDate + "')";
                    objdata.RptEmployeeMultipleDetails(query);

                    query = "Update Employee_Mst set EmpLevel='SemiSkilled',Training_Status='2'";
                    query = query + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    query = query + " And EmpNo='" + dt.Rows[i]["EmpNo"].ToString() + "' And ExistingCode='" + dt.Rows[i]["ExistingCode"].ToString() + "'";
                    objdata.RptEmployeeMultipleDetails(query);
                }
            }
        }

    }

    public void Load_SemiExpLevel()
    {
        string query = "";
        DataTable dt = new DataTable();

        query = "Select ED.EmpNo,ED.MachineID,ED.ExistingCode,MT.Level_Date,";
        query = query + "convert(varchar,GETDATE(),103) as CurrDate from Employee_Mst ED inner join Training_Level_Change MT on ED.EmpNo=MT.EmpNo";
        query = query + " where ED.CompCode='" + SessionCcode + "' And ED.LocCode='" + SessionLcode + "' And ED.IsActive='Yes' And ED.Training_Status='2'";
        query = query + " And MT.Ccode='" + SessionCcode + "' And MT.Lcode='" + SessionLcode + "' And MT.Training_Level='SemiSkilled'";
        dt = objdata.RptEmployeeMultipleDetails(query);

        if (dt.Rows.Count != 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DateTime TraineDate = new DateTime();
                int days =365;
                TraineDate = Convert.ToDateTime(dt.Rows[i]["Level_Date"].ToString()).AddDays(days);
                string currDate = dt.Rows[i]["CurrDate"].ToString();
                if (TraineDate.ToString("dd/MM/yyyy") == dt.Rows[i]["CurrDate"].ToString())
                {
                    query = "Update Training_Level_Change set Training_Level='Skilled',Level_Date='" + currDate + "'";
                    query = query + " where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                    query = query + " And EmpNo='" + dt.Rows[i]["EmpNo"].ToString() + "'";
                    query = query + " And MachineID='" + dt.Rows[i]["MachineID"].ToString() + "'";
                    query = query + " And ExistingCode='" + dt.Rows[i]["ExistingCode"].ToString() + "'";
                    objdata.RptEmployeeMultipleDetails(query);

                    query = "Update Employee_Mst set EmpLevel='Skilled',Training_Status='3'";
                    query = query + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    query = query + " And EmpNo='" + dt.Rows[i]["EmpNo"].ToString() + "' And ExistingCode='" + dt.Rows[i]["ExistingCode"].ToString() + "'";
                    objdata.RptEmployeeMultipleDetails(query);
                }
            }
        }

    }

    public void Load_ReTraineeLevel()
    {
        string query = "";
        DataTable dt = new DataTable();

        query = "Select ED.EmpNo,ED.MachineID,ED.ExistingCode,MT.Level_Date,TD.Days,";
        query = query + "convert(varchar,GETDATE(),103) as CurrDate from Employee_Mst ED inner join Training_Level_Change MT on ED.EmpNo=MT.EmpNo";
        query = query + " inner join MstTrainingDays TD on ED.DeptCode=TD.DeptCode";
        query = query + " where ED.CompCode='" + SessionCcode + "' And ED.LocCode='" + SessionLcode + "' And ED.IsActive='Yes' And ED.Training_Status='4'";
        query = query + " And MT.Ccode='" + SessionCcode + "' And MT.Lcode='" + SessionLcode + "' And MT.Training_Level='Re-Trainee'";
        dt = objdata.RptEmployeeMultipleDetails(query);

        if (dt.Rows.Count != 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DateTime TraineDate = new DateTime();
                int days = Convert.ToInt32(dt.Rows[i]["Days"]);
                TraineDate = Convert.ToDateTime(dt.Rows[i]["Level_Date"].ToString()).AddDays(days);
                string currDate = dt.Rows[i]["CurrDate"].ToString();
                DateTime Date1 = new DateTime();
                Date1 = Convert.ToDateTime(currDate);
                int daycount = (int)((TraineDate - Date1).TotalDays);
                int daysAdded = daycount + 1;

                if (TraineDate.ToString("dd/MM/yyyy") == dt.Rows[i]["CurrDate"].ToString())
                {
                    query = "Update Training_Level_Change set Training_Level='SemiSkilled',Level_Date='" + currDate + "'";
                    query = query + " where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                    query = query + " And EmpNo='" + dt.Rows[i]["EmpNo"].ToString() + "'";
                    query = query + " And MachineID='" + dt.Rows[i]["MachineID"].ToString() + "'";
                    query = query + " And ExistingCode='" + dt.Rows[i]["ExistingCode"].ToString() + "'";
                    objdata.RptEmployeeMultipleDetails(query);

                    query = "Update Employee_Mst set EmpLevel='SemiSkilled',Training_Status='2'";
                    query = query + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    query = query + " And EmpNo='" + dt.Rows[i]["EmpNo"].ToString() + "' And ExistingCode='" + dt.Rows[i]["ExistingCode"].ToString() + "'";
                    objdata.RptEmployeeMultipleDetails(query);
                }
            }
        }

    }
}
