﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
public partial class Pay_Incentive_Mst : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string Query = "";
    string SessionPayroll;
    string SessionEpay;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        Load_DB();
        if (!IsPostBack)
        {
            Load_MonthIncentive();
            Load_CivilIncentive();
            LoadWages();
        }
        Load_Data_Incentive();
    }
    public void Load_DB()
    {
        //Get Database Name
        string query = "";
        DataTable dt_DB = new DataTable();
        query = "select CMS,Rights,Sales,Stores,Weaving,Maintaince,Production,Payroll from [SVT_Rights]..MstDBname";
        dt_DB = objdata.RptEmployeeMultipleDetails(query);
        if (dt_DB.Rows.Count > 0)
        {
            SessionPayroll = dt_DB.Rows[0]["Payroll"].ToString();
            //SessionRights = dt_DB.Rows[0]["Rights"].ToString();
        }
    }
    private void Load_Data_Incentive()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from [" + SessionPayroll + "]..WorkerIncentive_mst where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater1.DataSource = DT;
        Repeater1.DataBind();

    }


    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {

        string query;
        DataTable DT = new DataTable();


        query = "select * from [" + SessionPayroll + "]..WorkerIncentive_mst where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'  And AutoID='" + e.CommandArgument.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count > 0)
        {
            query = "delete from [" + SessionPayroll + "]..WorkerIncentive_mst where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And AutoID='" + e.CommandArgument.ToString() + "'";
            objdata.RptEmployeeMultipleDetails(query);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Incentive Deleted Successfully...!');", true);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Data Found..!');", true);
        }
        Load_Data_Incentive();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            string MessFlag = "Insert";

            //if ((txtDaysOfMonth.Text.Trim() == "") || (txtDaysOfMonth.Text.Trim() == "0"))
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Day of Month');", true);
            //    ErrFlag = true;
            //}
             if ((txtMinDaysWorked.Text.Trim() == "") || (txtMinDaysWorked.Text.Trim() == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Minimum Days Worked');", true);
                ErrFlag = true;
            }
            //else if ((txtIncent_Amount.Text.Trim() == "") || (txtIncent_Amount.Text.Trim() == "0"))
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Incentive Amount');", true);
            //    ErrFlag = true;
            //}
            else if (ddlEmpType.SelectedValue == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Enter the Employee Type');", true);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                DataTable dt = new DataTable();
                Query = "Select * from [" + SessionPayroll + "]..WorkerIncentive_mst where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and MonthDays='" + txtDaysOfMonth.Text + "' and Wages='" + ddlEmpType.SelectedValue + "'";
                Query = Query + " and WorkerDays='" + txtMinDaysWorked.Text + "'";
                dt = objdata.RptEmployeeMultipleDetails(Query);
                if (dt.Rows.Count > 0)
                {
                    Query = "delete from [" + SessionPayroll + "]..WorkerIncentive_mst where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'  and MonthDays='" + txtDaysOfMonth.Text + "'";
                    dt = objdata.RptEmployeeMultipleDetails(Query);
                    MessFlag = "Update";
                }
                Query = "Insert Into [" + SessionPayroll + "]..WorkerIncentive_mst (Ccode,Lcode,WorkerDays,WorkerAmt,MonthDays,Wages)";
                Query = Query + "values ('" + SessionCcode + "','" + SessionLcode + "','" + txtMinDaysWorked.Text + "','" + txtIncent_Amount.Text + "','" + txtDaysOfMonth.Text + "','" + ddlEmpType.SelectedValue + "')";
                objdata.RptEmployeeMultipleDetails(Query);


                if (MessFlag == "Insert")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Update Successfully');", true);
                }
                Load_Data_Incentive();
                txtMinDaysWorked.Text = "0";
                txtDaysOfMonth.Text = "0";
                txtIncent_Amount.Text = "0";
                ddlEmpType.ClearSelection();
            }
        }
        catch (Exception Ex)
        {

        }
    }
    protected void btnIncen_Full_Amt_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            string MessFlag = "Insert";

            if ((txtIncent_Month_Full_Amount.Text.Trim() == "") || (txtIncent_Month_Full_Amount.Text.Trim() == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Hostel Amount....!');", true);
                ErrFlag = true;
            }

            if (!ErrFlag)
            {
                DataTable dt = new DataTable();
                Query = "Select Amt from [" + SessionPayroll + "]..HostelIncentive where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Amt='" + txtIncent_Month_Full_Amount.Text + "'";
                dt = objdata.RptEmployeeMultipleDetails(Query);
                if (dt.Rows.Count > 0)
                {
                    Query = "delete from [" + SessionPayroll + "]..HostelIncentive where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Amt='" + txtIncent_Month_Full_Amount.Text + "'";
                    dt = objdata.RptEmployeeMultipleDetails(Query);
                    MessFlag = "Update";
                }

                Query = "Insert Into [" + SessionPayroll + "]..HostelIncentive (Ccode,Lcode,Amt,WDays,HAllowed,OTDays,NFHDays,NFHWorkDays,CalWDays,CalHAllowed,CalOTDays,CalNFHDays,CalNFHWorkDays)";
                Query = Query + "values ('" + SessionCcode + "','" + SessionLcode + "','" + txtIncent_Month_Full_Amount.Text + "','" + WorkDays.Checked.ToString() + "','" + Hallowed.Checked.ToString() + "',";
                Query = Query + "'" + OTdays.Checked.ToString() + "','" + NFHdays.Checked.ToString() + "','" + NFHwork.Checked.ToString() + "','" + CalWorkdays.Checked.ToString() + "',";
                Query = Query + "'" + CalHallowed.Checked.ToString() + "','" + CalOTdays.Checked.ToString() + "','" + CalNFHdays.Checked.ToString() + "','" + CalNFHwork.Checked.ToString() + "')";
                objdata.RptEmployeeMultipleDetails(Query);

                if (MessFlag == "Insert")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Update Successfully');", true);
                }
                txtMinDaysWorked.Text = "0";
                txtDaysOfMonth.Text = "0";
                txtIncent_Amount.Text = "0";
            }
        }
        catch (Exception Ex)
        {

        }
    }
    public void Load_MonthIncentive()
    {
        DataTable dt = new DataTable();
        Query = "Select Amt,WDays,HAllowed,OTDays,NFHDays,NFHWorkDays,CalWDays,CalHAllowed,CalOTDays,CalNFHDays,CalNFHWorkDays from [" + SessionPayroll + "]..HostelIncentive where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(Query);
        if (dt.Rows.Count > 0)
        {
            txtIncent_Month_Full_Amount.Text = dt.Rows[0]["Amt"].ToString();
            if (dt.Rows[0]["WDays"].ToString().ToUpper() == "true".ToUpper()) { WorkDays.Checked = true; } else { WorkDays.Checked = false; }
            if (dt.Rows[0]["HAllowed"].ToString().ToUpper() == "true".ToUpper()) { Hallowed.Checked = true; } else { Hallowed.Checked = false; }
            if (dt.Rows[0]["OTDays"].ToString().ToUpper() == "true".ToUpper()) { OTdays.Checked = true; } else { OTdays.Checked = false; }
            if (dt.Rows[0]["NFHDays"].ToString().ToUpper() == "true".ToUpper()) { NFHdays.Checked = true; } else { NFHdays.Checked = false; }
            if (dt.Rows[0]["NFHWorkDays"].ToString().ToUpper() == "true".ToUpper()) { NFHwork.Checked = true; } else { NFHwork.Checked = false; }

            if (dt.Rows[0]["CalWDays"].ToString().ToUpper() == "true".ToUpper()) { CalWorkdays.Checked = true; } else { CalWorkdays.Checked = false; }
            if (dt.Rows[0]["CalHAllowed"].ToString().ToUpper() == "true".ToUpper()) { CalHallowed.Checked = true; } else { CalHallowed.Checked = false; }
            if (dt.Rows[0]["CalOTDays"].ToString().ToUpper() == "true".ToUpper()) { CalOTdays.Checked = true; } else { CalOTdays.Checked = false; }
            if (dt.Rows[0]["CalNFHDays"].ToString().ToUpper() == "true".ToUpper()) { CalNFHdays.Checked = true; } else { CalNFHdays.Checked = false; }
            if (dt.Rows[0]["CalNFHWorkDays"].ToString().ToUpper() == "true".ToUpper()) { CalNFHwork.Checked = true; } else { CalNFHwork.Checked = false; }
        }
        else
        {
            txtIncent_Month_Full_Amount.Text = "0.00";
            WorkDays.Checked = false; Hallowed.Checked = false; OTdays.Checked = false; NFHdays.Checked = false;
            NFHwork.Checked = false; CalWorkdays.Checked = false; CalHallowed.Checked = false; CalOTdays.Checked = false;
            CalNFHdays.Checked = false; CalNFHwork.Checked = false;
        }

    }
    protected void btnCivil_Incent_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            bool SaveFlag = true;
            DataTable dt = new DataTable();
            if (txtEligible_Days.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Eligible Days....! ');", true);
                ErrFlag = true;
            }
            if (txtCivil_Incen_Amt.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Incentive Amount....! ');", true);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                Query = "";
                Query = Query + "select * from [" + SessionPayroll + "]..CivilIncentive where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                dt = objdata.RptEmployeeMultipleDetails(Query);
                if (dt.Rows.Count > 0)
                {
                    Query = Query + "delete from [" + SessionPayroll + "]..CivilIncentive where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                    dt = objdata.RptEmployeeMultipleDetails(Query);
                    SaveFlag = false;
                }
                Query = "Insert into [" + SessionPayroll + "]..CivilIncentive(Ccode,Lcode,Amt,ElbDays) values(";
                Query = Query + "'" + SessionCcode + "','" + SessionLcode + "','" + txtEligible_Days.Text + "','" + txtCivil_Incen_Amt.Text + "')";
                objdata.RptEmployeeMultipleDetails(Query);

                if (SaveFlag == true)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully....! ');", true);

                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Update Successfully....! ');", true);
                }
                txtEligible_Days.Text = "0";
                txtCivil_Incen_Amt.Text = "0";
                Load_CivilIncentive();
            }
        }
        catch (Exception ex)
        {
        }
    }
    public void Load_CivilIncentive()
    {
        DataTable dt = new DataTable();
        Query = "";
        Query = Query + "select * from [" + SessionPayroll + "]..CivilIncentive where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(Query);
        if (dt.Rows.Count > 0)
        {
            txtEligible_Days.Text = dt.Rows[0]["ElbDays"].ToString();
            txtCivil_Incen_Amt.Text = dt.Rows[0]["Amt"].ToString();
        }
    }

    protected void ddlWages1_SelectedIndexChanged(object sender, EventArgs e)
    {
        string SSQL = "";
        if (ddlWages1.SelectedItem.Text != "-Select-")
        {
            SSQL = "Select * from [" + SessionPayroll + "]..Masthiri_Inc where Wages='" + ddlWages1.SelectedItem.Text + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
            DataTable Masthiri_Inc_DT = new DataTable();
            Masthiri_Inc_DT = objdata.RptEmployeeMultipleDetails(SSQL);
            if (Masthiri_Inc_DT.Rows.Count > 0)
            {
                txtAmount.Text = Masthiri_Inc_DT.Rows[0]["Amt"].ToString();
            }
            else
            {
                txtAmount.Text = "0";
            }
        }
    }

    protected void btnMasthiriSave_Click(object sender, EventArgs e)
    {
        bool ErrFlg = false;

        if (ddlWages1.SelectedItem.Text == "-Select-" || ddlWages1.SelectedItem.Value == "-Select-")
        {
            ddlWages1.Focus();
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please Select the Wages Type');", true);
            return;
        }

        if (!ErrFlg)
        {
            string Query = "";
            Query = "Delete from [" + SessionPayroll + "]..Masthiri_Inc where Wages='" + ddlWages1.SelectedItem.Text + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";

            objdata.RptEmployeeMultipleDetails(Query);

            Query = "Insert into [" + SessionPayroll + "]..Masthiri_Inc values('" + SessionCcode + "','" + SessionLcode + "','" + ddlWages1.SelectedItem.Text.ToUpper() + "','" + txtAmount.Text + "');";
            objdata.RptEmployeeMultipleDetails(Query);

        }
    }

    protected void btnMasthiriClr_Click(object sender, EventArgs e)
    {
        ddlWages1.ClearSelection();
        txtAmount.Text = "";
    }
    private void LoadWages()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        // ddlWages.Items.Clear();
        ddlWages1.Items.Clear();
        ddlEmpType.Items.Clear();
        query = "Select *from MstEmployeeType";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        // ddlWages.DataSource = dtdsupp;
        ddlWages1.DataSource = dtdsupp;
        ddlEmpType.DataSource = objdata.RptEmployeeMultipleDetails(query);
        DataRow dr = dtdsupp.NewRow();
        dr["EmpType"] = "-Select-";
        dr["EmpType"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        // ddlWages.DataTextField = "EmpType";
        ddlWages1.DataTextField = "EmpType";
        // ddlWages.DataValueField = "EmpType";
        ddlWages1.DataValueField = "EmpType";
        // ddlWages.DataBind();
        ddlWages1.DataBind();


        ddlEmpType.DataTextField = "EmpType";
        ddlEmpType.DataValueField = "EmpType";
        ddlEmpType.DataBind();
        ddlEmpType.Items.Insert(0, new ListItem("-Select-", "-Select-", true));

    }
}
