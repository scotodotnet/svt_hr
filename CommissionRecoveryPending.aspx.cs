﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class CommissionRecoveryPending : System.Web.UI.Page
{
    string SessionLcode;
    string SessionCcode;
    string SSQL;
    string SessionAdmin;
    string SessionUserType;
    BALDataAccess objdata = new BALDataAccess();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {



            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();

            SessionUserType = Session["Isadmin"].ToString();
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Report-Commission Transaction Report";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("SalaryProcess"));
                //li.Attributes.Add("class", "droplink active open");

                ddlType_SelectedIndexChanged(sender, e);
            }
        }
    }
    protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dtempty = new DataTable();
        ddlagentparentName.DataSource = dtempty;
        ddlagentparentName.DataBind();
        DataTable dt = new DataTable();
        if (ddlType.SelectedValue == "Agent")
        {
            SSQL = "Select AgentName from MstAgent";
            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            ddlagentparentName.DataSource = dt;
            DataRow dr = dt.NewRow();
            dr["AgentName"] = "-Select-";
            dr["AgentName"] = "-Select-";
            dt.Rows.InsertAt(dr, 0);
            ddlagentparentName.DataTextField = "AgentName";
            ddlagentparentName.DataValueField = "AgentName";
            ddlagentparentName.DataBind();
        }
        else if (ddlType.SelectedValue == "Parent")
        {
            SSQL = "Select Distinct(RefParentsName)as AgentName from Employee_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and RefParentsName!='' ";
            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            ddlagentparentName.DataSource = dt;
            DataRow dr = dt.NewRow();
            dr["AgentName"] = "-Select-";
            dr["AgentName"] = "-Select-";
            dt.Rows.InsertAt(dr, 0);
            ddlagentparentName.DataTextField = "AgentName";
            ddlagentparentName.DataValueField = "AgentName";
            ddlagentparentName.DataBind();
        }
        else
        {
            dt.Columns.Add("AgentName");
            ddlagentparentName.DataSource = dt;
            DataRow dr = dt.NewRow();
            dr["AgentName"] = "-Select-";
            dr["AgentName"] = "-Select-";
            dt.Rows.InsertAt(dr, 0);
            ddlagentparentName.DataTextField = "AgentName";
            ddlagentparentName.DataValueField = "AgentName";
            ddlagentparentName.DataBind();
        }

    }
    protected void btnReport_Click(object sender, EventArgs e)
    {
        ResponseHelper.Redirect("CommissonPending_ReportDisplay.aspx?Type=" + ddlType.SelectedItem.Text + "&Name=" + ddlagentparentName.SelectedItem.Text, "_blank", "");

    }
}
