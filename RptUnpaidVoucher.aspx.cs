﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Text;
using Payroll;
using Payroll.Data;
using Payroll.Configuration;

using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.Security.Cryptography;
using System.IO;

public partial class RptUnpaidVoucher : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    DateTime TransDate;
    string SessionAdmin;
    string Stafflabour;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string SessionUserName;
    string SessionUserID;
    string SessionRights;
    DataTable AutoDtable = new DataTable();
    DataTable dt_Employe = new DataTable();
    string dor = "";
    string tday = "";
    DateTime MyDate1;
    DateTime MyDate2;
    string SSQL = "";
    string MachineIDEncrypt;
    DataTable dt_Salary = new DataTable();
    DataTable dt_LogSalary = new DataTable();
    DataTable DataCells = new DataTable();
    DataSet ds = new DataSet();
    ReportDocument report = new ReportDocument();
    DataTable dt = new DataTable();
    string SessionPayroll;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        Load_DB();
        if (!IsPostBack)
        {
            Load_TokenNo();
           
            Initial_Data_Referesh();
            Report();
        }
        Load_OLD_data();
    }
    public void Load_DB()
    {
        //Get Database Name
        string query = "";
        DataTable dt_DB = new DataTable();
        query = "select CMS,Rights,Sales,Stores,Weaving,Maintaince,Production,Payroll from [SVT_Rights]..MstDBname";
        dt_DB = objdata.RptEmployeeMultipleDetails(query);
        if (dt_DB.Rows.Count > 0)
        {
            SessionPayroll = dt_DB.Rows[0]["Payroll"].ToString();
            //SessionRights = dt_DB.Rows[0]["Rights"].ToString();
        }
    }

    private void Initial_Data_Referesh()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("ExistingCode", typeof(string)));
        dt.Columns.Add(new DataColumn("EmpNo", typeof(string)));
        dt.Columns.Add(new DataColumn("Month", typeof(string)));
        dt.Columns.Add(new DataColumn("FinancialYear", typeof(string)));
        dt.Columns.Add(new DataColumn("NetAmount", typeof(string)));


        Repeater1.DataSource = dt;
        Repeater1.DataBind();
        ViewState["ItemTable"] = Repeater1.DataSource;

        //dt = Repeater1.DataSource;
    }

    private void Load_OLD_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }

      // and IsActive='No'
    public void Load_TokenNo()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();

        string FirstMonth = DateTime.Now.AddMonths(-6).Month.ToString();
        string FromYear = DateTime.Now.AddMonths(-6).Year.ToString();
        string FirstDate = "";
        if (FirstMonth == "1")
        {
            FirstDate = "01/01/" + FromYear;
        }
        else if (FirstMonth == "2")
        {
            FirstDate = "01/02/" + FromYear;
        }
        else if (FirstMonth == "3")
        {
            FirstDate = "01/03/" + FromYear;
        }
        else if (FirstMonth == "4")
        {
            FirstDate = "01/04/" + FromYear;
        }
        else if (FirstMonth == "5")
        {
            FirstDate = "01/05/" + FromYear;
        }
        else if (FirstMonth == "6")
        {
            FirstDate = "01/06/" + FromYear;
        }
        else if (FirstMonth == "7")
        {
            FirstDate = "01/07/" + FromYear;
        }
        else if (FirstMonth == "8")
        {
            FirstDate = "01/08/" + FromYear;
        }
        else if (FirstMonth == "9")
        {
            FirstDate = "01/09/" + FromYear;
        }
        else if (FirstMonth == "10")
        {
            FirstDate = "01/10/" + FromYear;
        }
        else if (FirstMonth == "11")
        {
            FirstDate = "01/11/" + FromYear;
        }
        else if (FirstMonth == "12")
        {
            FirstDate = "01/12/" + FromYear;
        }
        string LastMonth = Convert.ToDateTime(FirstDate).AddMonths(5).Month.ToString();
        string ToYear = Convert.ToDateTime(FirstDate).AddMonths(5).Year.ToString();
        string LastDate = "";
        if (LastMonth == "1")
        {
            LastDate = "31/01/" + ToYear;
        }
        else if (LastMonth == "2")
        {
            int yrs = (Convert.ToInt32(ToYear));
            if ((yrs % 4) == 0)
            {
                LastDate = "29/02/" + ToYear;
            }
            else
            {
                LastDate = "28/02/" + ToYear;
            }
        }
        else if (LastMonth == "3")
        {
            LastDate = "31/03/" + ToYear;
        }
        else if (LastMonth == "4")
        {
            LastDate = "30/04/" + ToYear;
        }
        else if (LastMonth == "5")
        {
            LastDate = "31/05/" + ToYear;
        }
        else if (LastMonth == "6")
        {
            LastDate = "30/06/" + ToYear;
        }
        else if (LastMonth == "7")
        {
            LastDate = "31/07/" + ToYear;
        }
        else if (LastMonth == "8")
        {
            LastDate = "31/08/" + ToYear;
        }
        else if (LastMonth == "9")
        {
            LastDate = "30/09/" + ToYear;
        }
        else if (LastMonth == "10")
        {
            LastDate = "31/10/" + ToYear;
        }
        else if (LastMonth == "11")
        {
            LastDate = "30/11/" + ToYear;
        }
        else if (LastMonth == "12")
        {
            LastDate = "31/12/" + ToYear;
        }
        //query = query + " And SD.FromDate >= convert(datetime,'" + FirstDate + "', 105) and SD.ToDate <= Convert(Datetime,'" + LastDate + "', 105)";
        //query = query + " And SD.SalaryThrough='2'";


        ddltokenNo.Items.Clear();
        query = "Select Distinct EM.Existingcode from Employee_Mst EM inner join [" + SessionPayroll + "]..SalaryDetails SD on EM.Existingcode COLLATE DATABASE_DEFAULT =SD.ExisistingCode COLLATE DATABASE_DEFAULT ";
        query = query + " where EM.CompCode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "'";
        query = query + " And SD.Ccode='" + SessionCcode + "' And SD.Lcode='" + SessionLcode + "'";
        //query = query + " And DATEDIFF(MM,CONVERT(datetime,DOJ,103),Convert(datetime,DOR,103)) <6";
        query = query + " And SD.FromDate >= convert(datetime,'" + FirstDate + "', 105) and SD.ToDate <= Convert(Datetime,'" + LastDate + "', 105)";
        query = query + " And SD.SalaryThrough='2'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddltokenNo.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["Existingcode"] = "-Select-";
        dr["Existingcode"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddltokenNo.DataTextField = "Existingcode";
        ddltokenNo.DataValueField = "Existingcode";
        ddltokenNo.DataBind();
    }

      protected void btnReport_Click(object sender, EventArgs e)
      {

        //ResponseHelper.Redirect("RptunpaisVoucher_Display.aspx?TokenNO=" + ddltokenNo.SelectedItem.Text, "_blank", "");
          if (chksalry.Checked == true)
          {
              Initial_Data_Referesh();
              SalaryConfirmed();
          }
          else
          {
              Report();
          }
      }

      protected void GridEditEntryClick(object sender, CommandEventArgs e)
      {
          string query = "";
          string ExistingCode = e.CommandName.ToString();
          string[] SalDet;
          string Month = "";
          string FinYear = "";
          SalDet = e.CommandArgument.ToString().Split(',');

          if (SalDet.Length == 2)
          {
              Month = SalDet[0].ToString();
              FinYear = SalDet[1].ToString();
          }

          DataTable dt = new DataTable();
          dt = (DataTable)ViewState["ItemTable"];
          for (int i = 0; i < dt.Rows.Count; i++)
          {
              if (dt.Rows[i]["ExistingCode"].ToString() == ExistingCode.ToString() && dt.Rows[i]["Month"].ToString() == Month && dt.Rows[i]["FinancialYear"].ToString() == FinYear)
              {
                  DataTable DT_Check = new DataTable();
                  query = "Select *from UnpaidVoucher_Salary_Confirm where CompCode='" + SessionCcode + "'";
                  query = query + " And LocCode='" + SessionLcode + "' And ExistingCode='" + dt.Rows[i]["ExistingCode"].ToString() + "'";
                  query = query + " And Month='" + dt.Rows[i]["Month"].ToString() + "' And FinancialYear='" + dt.Rows[i]["FinancialYear"].ToString() + "'";
                  DT_Check = objdata.RptEmployeeMultipleDetails(query);

                  if (DT_Check.Rows.Count == 0)
                  {

                      query = "Insert into UnpaidVoucher_Salary_Confirm(CompCode,LocCode,ExistingCode,";
                      query = query + "EmpNo,Month,FinancialYear,NetAmount) Values ( ";
                      query = query + "'" + SessionCcode + "','" + SessionLcode + "','" + dt.Rows[i]["ExistingCode"].ToString() + "',";
                      query = query + "'" + dt.Rows[i]["EmpNo"].ToString() + "','" + dt.Rows[i]["Month"].ToString() + "',";
                      query = query + "'" + dt.Rows[i]["FinancialYear"].ToString() + "','" + dt.Rows[i]["NetAmount"].ToString() + "'";
                      query = query + ")";
                      objdata.RptEmployeeMultipleDetails(query);

                      ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Salary Confirmed');", true);
                  }
                  else
                  {
                      ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Already Salary Confirmed. Check the details.');", true);
                  }
              }
          }
          Report();
          //ViewState["ItemTable"] = dt;
      }

      protected void GridPrintEntryClick(object sender, CommandEventArgs e)
      {
          string query = "";
          string ExistingCode = e.CommandName.ToString();
          string[] SalDet;
          string Month = "";
          string FinYear = "";
          SalDet = e.CommandArgument.ToString().Split(',');

          if (SalDet.Length == 2)
          {
              Month = SalDet[0].ToString();
              FinYear = SalDet[1].ToString();
          }
          ResponseHelper.Redirect("RptunpaisVoucher_Display.aspx?MonthStr=" + Month + "&FinYrStr=" + FinYear + "&TokenNO=" + ExistingCode.ToString(), "_blank", "");
        
      }

      public void SalaryConfirmed()
      {
          DataCells.Columns.Add("ExistingCode");
          DataCells.Columns.Add("Month");
          DataCells.Columns.Add("FinancialYear");
          DataCells.Columns.Add("NetAmount");

          DataTable DT_Check = new DataTable();
          SSQL="Select * from UnpaidVoucher_Salary_Confirm where CompCode='"+SessionCcode +"' and LocCode='"+SessionLcode +"' ";
          SSQL = SSQL + " and ExistingCode='" + ddltokenNo.SelectedItem.Text + "'";
          DT_Check = objdata.RptEmployeeMultipleDetails(SSQL);
          if (DT_Check.Rows.Count != 0)
          {
              for (int i = 0; i < DT_Check.Rows.Count; i++)
              {
                  DataCells.NewRow();
                  DataCells.Rows.Add();
                  DataCells.Rows[DataCells.Rows.Count - 1]["ExistingCode"] = DT_Check.Rows[i]["ExistingCode"].ToString();
                  DataCells.Rows[DataCells.Rows.Count - 1]["Month"] = DT_Check.Rows[i]["Month"].ToString();
                  DataCells.Rows[DataCells.Rows.Count - 1]["FinancialYear"] = DT_Check.Rows[i]["Financialyear"].ToString();
                  DataCells.Rows[DataCells.Rows.Count - 1]["NetAmount"] = DT_Check.Rows[i]["NetAmount"].ToString();
              }
              ViewState["ItemTable"] = DataCells;
              Repeater1.DataSource = DataCells;
              Repeater1.DataBind();
          }
          else
          {
              Initial_Data_Referesh();
          }
      }

      public void Report()
      {
          DataCells.Columns.Add("CompanyName");
          DataCells.Columns.Add("LocationName");
          DataCells.Columns.Add("ExistingCode");
          DataCells.Columns.Add("EmpNo");
          DataCells.Columns.Add("Name");
          DataCells.Columns.Add("Deptment");
          DataCells.Columns.Add("Month");
          DataCells.Columns.Add("FinancialYear");
          DataCells.Columns.Add("NetAmount");

          string Month = "";
          int Year;

          string FirstMonth = DateTime.Now.AddMonths(-6).Month.ToString();
          string FromYear = DateTime.Now.AddMonths(-6).Year.ToString();
          string FirstDate = "";
          if (FirstMonth == "1")
          {
              FirstDate = "01/01/"+FromYear;
          }
          else if (FirstMonth == "2")
          {
              FirstDate = "01/02/" + FromYear;
          }
          else if (FirstMonth == "3")
          {
              FirstDate = "01/03/" + FromYear;
          }
          else if (FirstMonth == "4")
          {
              FirstDate = "01/04/" + FromYear;
          }
          else if (FirstMonth == "5")
          {
              FirstDate = "01/05/" + FromYear;
          }
          else if (FirstMonth == "6")
          {
              FirstDate = "01/06/" + FromYear;
          }
          else if (FirstMonth == "7")
          {
              FirstDate = "01/07/" + FromYear;
          }
          else if (FirstMonth == "8")
          {
              FirstDate = "01/08/" + FromYear;
          }
          else if (FirstMonth == "9")
          {
              FirstDate = "01/09/" + FromYear;
          }
          else if (FirstMonth == "10")
          {
              FirstDate = "01/10/" + FromYear;
          }
          else if (FirstMonth == "11")
          {
              FirstDate = "01/11/" + FromYear;
          }
          else if (FirstMonth == "12")
          {
              FirstDate = "01/12/" + FromYear;
          }
          string LastMonth = Convert.ToDateTime(FirstDate).AddMonths(5).Month.ToString();
          string ToYear = Convert.ToDateTime(FirstDate).AddMonths(5).Year.ToString();
          string LastDate = "";
          if (LastMonth == "1")
          {
              LastDate = "31/01/" + ToYear;
          }
          else if (LastMonth == "2")
          {
              int yrs = (Convert.ToInt32(ToYear));
              if ((yrs % 4) == 0)
              {
                  LastDate = "29/02/" + ToYear;
              }
              else
              {
                  LastDate = "28/02/" + ToYear;
              }
          }
          else if (LastMonth == "3")
          {
              LastDate = "31/03/" + ToYear;
          }
          else if (LastMonth == "4")
          {
              LastDate = "30/04/" + ToYear;
          }
          else if (LastMonth == "5")
          {
              LastDate = "31/05/" + ToYear;
          }
          else if (LastMonth == "6")
          {
              LastDate = "30/06/" + ToYear;
          }
          else if (LastMonth == "7")
          {
              LastDate = "31/07/" + ToYear;
          }
          else if (LastMonth == "8")
          {
              LastDate = "31/08/" + ToYear;
          }
          else if (LastMonth == "9")
          {
              LastDate = "30/09/" + ToYear;
          }
          else if (LastMonth == "10")
          {
              LastDate = "31/10/" + ToYear;
          }
          else if (LastMonth == "11")
          {
              LastDate = "30/11/" + ToYear;
          }
          else if (LastMonth == "12")
          {
              LastDate = "31/12/" + ToYear;
          }

          SSQL = "Select Distinct EM.Existingcode,EM.MachineID,EM.EmpNo,EM.FirstName,EM.DeptName,EM.DOR from Employee_Mst EM inner join [" + SessionPayroll + "]..SalaryDetails SD on EM.Existingcode COLLATE DATABASE_DEFAULT =SD.ExisistingCode COLLATE DATABASE_DEFAULT ";
          SSQL = SSQL + " where EM.CompCode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "'";
          SSQL = SSQL + " And SD.Ccode='" + SessionCcode + "' And SD.Lcode='" + SessionLcode + "'";
          //query = query + " And DATEDIFF(MM,CONVERT(datetime,DOJ,103),Convert(datetime,DOR,103)) <6";
          SSQL = SSQL + " And SD.FromDate >= convert(datetime,'" + FirstDate + "', 105) and SD.ToDate <= Convert(Datetime,'" + LastDate + "', 105)";
          SSQL = SSQL + " And SD.SalaryThrough='2'";
          //SSQL = "Select * from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
          ////SSQL = SSQL + " and IsActive='No' and DOR !='' ";
          ////SSQL = SSQL + " And DATEDIFF(MM,CONVERT(datetime,DOJ,103),Convert(datetime,DOR,103)) <6";
          //SSQL = SSQL + " And DATEDIFF(MM,Convert(datetime,DOR,103),CONVERT(datetime,GETDATE(),103)) <6";
          if (ddltokenNo.SelectedItem.Text != "-Select-")
          {
              SSQL = SSQL + " and EM.ExistingCode='" + ddltokenNo.SelectedItem.Text + "'";
              SSQL = SSQL + " and SD.ExisistingCode='" + ddltokenNo.SelectedItem.Text + "'";
          }
          dt_Employe = objdata.RptEmployeeMultipleDetails(SSQL);
          if (dt_Employe.Rows.Count != 0)
          {
              for (int k = 0; k < dt_Employe.Rows.Count; k++)
              {
                  if (dt_Employe.Rows.Count != 0)
                  {
                      dor = dt_Employe.Rows[k]["DOR"].ToString();
                  }
                  tday = Convert.ToDateTime(LastDate).ToString("dd/MM/yyyy");
                  MyDate1 = DateTime.ParseExact(FirstDate, "dd/MM/yyyy", null);
                  MyDate2 = DateTime.ParseExact(tday, "dd/MM/yyyy", null);
                  MachineIDEncrypt = UTF8Encryption(dt_Employe.Rows[k]["MachineID"].ToString());

                  SSQL = "Select * from [" + SessionPayroll + "]..SalaryDetails Where CCode='" + SessionCcode + "' And LCode='" + SessionLcode + "' and ExisistingCode='" + dt_Employe.Rows[k]["ExistingCode"].ToString() + "'";
                  SSQL = SSQL + " And FromDate >= convert(datetime,'" + MyDate1 + "', 105) and ToDate <= Convert(Datetime,'" + MyDate2 + "', 105)";
                  SSQL = SSQL + " And SalaryThrough='2'";
                  dt_Salary = objdata.RptEmployeeMultipleDetails(SSQL);

                  if(dt_Salary.Rows.Count != 0)
                  {
                      for (int i = 0; i < dt_Salary.Rows.Count; i++)
                      {
                          Month = "";
                          Year = Convert.ToInt32(dt_Salary.Rows[i]["Financialyear"].ToString());
                          if (dt_Salary.Rows[i]["Month"].ToString() == "January")
                          {
                              Month = "02";
                          }
                          if (dt_Salary.Rows[i]["Month"].ToString() == "February")
                          {
                              Month = "03";
                          }
                          if (dt_Salary.Rows[i]["Month"].ToString() == "March")
                          {
                              Month = "04";
                          }
                          if (dt_Salary.Rows[i]["Month"].ToString() == "April")
                          {
                              Month = "05";
                          }
                          if (dt_Salary.Rows[i]["Month"].ToString() == "May")
                          {
                              Month = "06";
                          }
                          if (dt_Salary.Rows[i]["Month"].ToString() == "June")
                          {
                              Month = "07";
                          }
                          if (dt_Salary.Rows[i]["Month"].ToString() == "July")
                          {
                              Month = "08";
                          }
                          if (dt_Salary.Rows[i]["Month"].ToString() == "August")
                          {
                              Month = "09";
                          }
                          if (dt_Salary.Rows[i]["Month"].ToString() == "September")
                          {
                              Month = "10";
                          }
                          if (dt_Salary.Rows[i]["Month"].ToString() == "October")
                          {
                              Month = "11";
                          }
                          if (dt_Salary.Rows[i]["Month"].ToString() == "November")
                          {
                              Month = "12";
                          }
                          if (dt_Salary.Rows[i]["Month"].ToString() == "December")
                          {
                              Month = "01";
                              Year = Year + 1;
                          }

                          string fmdate = "01";
                          string tdate = "12";

                          string fromDate = Year.ToString() + "-" + Month + "-" + fmdate;
                          string ToDate = Year.ToString() + "-" + Month + "-" + tdate;
                          DateTime date1 = Convert.ToDateTime(fromDate);
                          DateTime date2 = Convert.ToDateTime(ToDate);

                          SSQL = "Select * from LogTime_Salary where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and MachineID='" + MachineIDEncrypt + "' ";
                          SSQL = SSQL + "And Payout >='" + date1.ToString("yyyy/MM/dd") + " " + "00:00" + "'";
                          SSQL = SSQL + "And payout <='" + date2.ToString("yyyy/MM/dd") + " " + "23:00" + "'";
                          dt_LogSalary = objdata.RptEmployeeMultipleDetails(SSQL);

                          if (dt_LogSalary.Rows.Count == 0)
                          {
                              DataTable DT_Check = new DataTable();
                              SSQL = "Select *from UnpaidVoucher_Salary_Confirm where CompCode='" + SessionCcode + "'";
                              SSQL = SSQL + " And LocCode='" + SessionLcode + "' And ExistingCode='" + dt_Employe.Rows[0]["ExistingCode"].ToString() + "'";
                              SSQL = SSQL + " And Month='" + dt_Salary.Rows[i]["Month"].ToString() + "' And FinancialYear='" + dt_Salary.Rows[i]["Financialyear"].ToString() + "'";
                              DT_Check = objdata.RptEmployeeMultipleDetails(SSQL);

                              if (DT_Check.Rows.Count == 0)
                              {

                                  SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "' ";
                                  dt = objdata.RptEmployeeMultipleDetails(SSQL);
                                  string name = dt.Rows[0]["CompName"].ToString();

                                  DataCells.NewRow();
                                  DataCells.Rows.Add();
                                  DataCells.Rows[DataCells.Rows.Count - 1]["CompanyName"] = name;
                                  DataCells.Rows[DataCells.Rows.Count - 1]["LocationName"] = SessionLcode;
                                  DataCells.Rows[DataCells.Rows.Count - 1]["ExistingCode"] = dt_Employe.Rows[k]["ExistingCode"].ToString();
                                  DataCells.Rows[DataCells.Rows.Count - 1]["EmpNo"] = dt_Employe.Rows[k]["EmpNo"].ToString();
                                  DataCells.Rows[DataCells.Rows.Count - 1]["Name"] = dt_Employe.Rows[k]["FirstName"].ToString();
                                  DataCells.Rows[DataCells.Rows.Count - 1]["Deptment"] = dt_Employe.Rows[k]["DeptName"].ToString();
                                  DataCells.Rows[DataCells.Rows.Count - 1]["Month"] = dt_Salary.Rows[i]["Month"].ToString();
                                  DataCells.Rows[DataCells.Rows.Count - 1]["FinancialYear"] = dt_Salary.Rows[i]["Financialyear"].ToString();
                                  DataCells.Rows[DataCells.Rows.Count - 1]["NetAmount"] = dt_Salary.Rows[i]["RoundOffNetPay"].ToString();
                              }
                          }
                      }

                  }

              }
              ViewState["ItemTable"] = DataCells;
              Repeater1.DataSource = DataCells;
              Repeater1.DataBind();
          }
          else
          {
              ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Records Found');", true);
          }
      }

      public static string UTF8Encryption_OLD(string password)
      {
          string strmsg = string.Empty;
          byte[] encode = new byte[password.Length];
          encode = Encoding.UTF8.GetBytes(password);
          strmsg = Convert.ToBase64String(encode);
          return strmsg;
      }

      public string UTF8Encryption(string mvarPlanText)
      {
          string cipherText = "";
          try
          {
              string passPhrase = "Altius";
              string saltValue = "info@altius.co.in";
              string hashAlgorithm = "SHA1";
              string initVector = "@1B2c3D4e5F6g7H8";
              int passwordIterations = 2;
              int keySize = 256;
              byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
              byte[] saltValueBytes = Encoding.ASCII.GetBytes(saltValue);
              byte[] plainTextBytes = Encoding.UTF8.GetBytes(mvarPlanText);
              PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, saltValueBytes, hashAlgorithm, passwordIterations);
              byte[] keyBytes = password.GetBytes(keySize / 8);
              RijndaelManaged symmetricKey = new RijndaelManaged();
              symmetricKey.Mode = CipherMode.CBC;
              ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes);
              MemoryStream memoryStream = new MemoryStream();
              CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
              cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
              cryptoStream.FlushFinalBlock();
              byte[] cipherTextBytes = memoryStream.ToArray();
              memoryStream.Close();
              cryptoStream.Close();
              cipherText = Convert.ToBase64String(cipherTextBytes);
          }
          catch (Exception ex)
          {
              throw new Exception(ex.Message);
          }

          return cipherText;
      }
     
}
