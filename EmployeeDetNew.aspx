﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="EmployeeDetNew.aspx.cs" Inherits="EmployeeDetNew" Title="" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


   <!-- begin #content -->
<div id="content" class="content">
  <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="javascript:;">Employee Profile</a></li>
        <li class="active">Employee Details</li>
    </ol>

    <!-- end breadcrumb -->
    
     <!-- begin page-header -->
    <h1 class="page-header">Employee History</h1>
    <!-- end page-header -->
    
     <!-- begin row -->
    <div class="row">
       <!-- begin col-12 -->
        <div class="col-md-12">
            <div>
                <!-- begin panel -->
                <div class="panel panel-inverse">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        </div>
                        <h4 class="panel-title">Employee Details</h4>
                    </div>
                    
                     <div class="panel-body">
                     <ul class="nav nav-tabs">
						<li class="active">
							<a href="#default-tab-1" data-toggle="tab">
								<span class="visible-xs">Tab 1</span>
								<span class="hidden-xs">Basic</span>
							</a>
						</li>
						<li class="">
							<a href="#default-tab-2" data-toggle="tab">
								<span class="visible-xs">Tab 2</span>
								<span class="hidden-xs">Salary</span>
							</a>
						</li>
						<li class="">
							<a href="#default-tab-3" data-toggle="tab">
								<span class="visible-xs">Tab 3</span>
								<span class="hidden-xs">Personal</span>
							</a>
						</li>
						<li class="">
							<a href="#default-tab-4" data-toggle="tab">
								<span class="visible-xs">Tab 4</span>
								<span class="hidden-xs">General</span>
							</a>
						</li>
						<li class="">
							<a href="#default-tab-5" data-toggle="tab">
								<span class="visible-xs">Tab 5</span>
								<span class="hidden-xs">Documents</span>
							</a>
						</li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane fade active in" id="default-tab-1">
						<asp:UpdatePanel ID="UpdatePanel2" runat="server">
                          <ContentTemplate>
							<fieldset>
                                     <legend class="pull-left width-full">Basic</legend>
                                        <div class="col-md-12">
                                         <div class="col-md-9">
                                           <!-- begin row -->
                                            <div class="row">
                                            <!-- begin col-4 -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Machine ID</label><span class="mandatory">*</span>
                                                        <asp:TextBox ID="txtMachineID" runat="server" class="form-control" 
                                                            AutoPostBack="true" ontextchanged="txtMachineID_TextChanged" ></asp:TextBox>
                                                        <asp:RequiredFieldValidator ControlToValidate="txtMachineID" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                        </asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                                <!-- end col-4 -->
                                             <!-- begin col-4 -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Existing Number</label><span class="mandatory">*</span>
                                                        <asp:TextBox ID="txtExistingCode" runat="server" class="form-control"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ControlToValidate="txtExistingCode" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                        </asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                                <!-- end col-4 -->
                                                
                                               
                                                <!-- begin col-4 -->
                                                <div class="col-md-4">
                                                    <div class="form-group block1">
                                                        <label>Ticket Number</label><span class="mandatory">*</span>
                                                        <asp:TextBox ID="txtTokenID" runat="server" class="form-control" Enabled="false"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <!-- end col-4 -->
                                            </div>
                                            <div class="row">
                                                <!-- begin col-4 -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Category</label><span class="mandatory">*</span>
                                                        <asp:DropDownList ID="ddlCategory" runat="server" class="form-control select2" style="width:100%;">
                                                        <asp:ListItem Value="0" Text="-Select-"></asp:ListItem>
                                                         <asp:ListItem Value="STAFF" Text="STAFF"></asp:ListItem>
                                                         <asp:ListItem Value="LABOUR" Text="LABOUR"></asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ControlToValidate="ddlCategory" InitialValue="0" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                        </asp:RequiredFieldValidator>
                                                    </div>
                                              </div>
                                                <!-- end col-4 -->
                                                <!-- begin col-4 -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Sub Category</label><span class="mandatory">*</span>
                                                        <asp:DropDownList ID="ddlSubCategory" runat="server" class="form-control select2" style="width:100%;">
                                                         <asp:ListItem Value="0" Text="-Select-"></asp:ListItem>
                                                         <asp:ListItem Value="INSIDER" Text="INSIDER"></asp:ListItem>
                                                         <asp:ListItem Value="OUTSIDER" Text="OUTSIDER"></asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ControlToValidate="ddlSubCategory" InitialValue="0" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                        </asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                                <!-- end col-4 -->
                                                <!-- begin col-4 -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Shift</label><span class="mandatory">*</span>
                                                        <asp:DropDownList ID="ddlShift" runat="server" class="form-control select2" style="width:100%;">
                                                        <asp:ListItem Value="0" Text="-Select-"></asp:ListItem>
                                                         <asp:ListItem Value="GENERAL" Text="GENERAL"></asp:ListItem>
                                                         <asp:ListItem Value="SHIFT" Text="SHIFT"></asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ControlToValidate="ddlShift" InitialValue="0" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                        </asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                                <!-- end col-4 -->
                                            </div>
                                           
                                            </div>
                                            <div class="col-md-3">
                                             <div class="media" style="margin-top: -19px;">
                                                    <a class="media-right" href="javascript:;" style="float: right;">
                                                        <asp:Image ID="Image3" runat="server" class="media-object" style="width: 158px;height: 161px;" ImageUrl="~/assets/img/login-bg/man-user-50.png" />
                                                        
                                                    </a>

                                                </div>
                                            </div>
                                            <div class="row">
                                                <!-- begin col-4 -->
                                                <div class="col-md-4">
                                                    <div class="form-group block1">
                                                        <label>First Name</label><span class="mandatory">*</span>
                                                        <asp:TextBox ID="txtFirstName" runat="server" class="form-control"></asp:TextBox>
                                                         <asp:RequiredFieldValidator ControlToValidate="txtFirstName" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator6" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                        </asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                                <!-- end col-4 -->
                                                <!-- begin col-4 -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Initial /Last Name</label><span class="mandatory">*</span>
                                                        <asp:TextBox ID="txtLastName" runat="server" class="form-control"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ControlToValidate="txtLastName" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator7" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                        </asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                                <!-- end col-4 -->
                                                <!-- begin col-4 -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Date of Birth</label><span class="mandatory">*</span>
                                                        <asp:TextBox ID="txtDOB" runat="server" class="form-control datepicker" 
                                                            placeholder="dd/MM/YYYY" AutoPostBack="true" 
                                                            ontextchanged="txtDOB_TextChanged" ></asp:TextBox>
                                                            <asp:RequiredFieldValidator ControlToValidate="txtDOB" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator8" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                            </asp:RequiredFieldValidator>
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                TargetControlID="txtDOB" ValidChars="0123456789/">
                                                            </cc1:FilteredTextBoxExtender>
                                                    </div>
                                                </div>
                                                <!-- end col-4 -->
                                            </div>
                                            <div class="row">
                                                <!-- begin col-4 -->
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label>Age</label><span class="mandatory">*</span>
                                                        <asp:TextBox ID="txtAge" runat="server" class="form-control" Enabled="false"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <!-- end col-4 -->
                                                <!-- begin col-4 -->
                                                <div class="col-md-2">
                                                    <div class="form-group block1">
                                                        <label>Gender</label><span class="mandatory">*</span>
                                                        <asp:DropDownList ID="ddlGender" runat="server" class="form-control select2" style="width:100%;">
                                                        <asp:ListItem Value="0" Text="-Select-"></asp:ListItem>
                                                         <asp:ListItem Value="MALE" Text="MALE"></asp:ListItem>
                                                         <asp:ListItem Value="FEMALE" Text="FEMALE"></asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ControlToValidate="ddlGender" InitialValue="0" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator9" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                        </asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                                <!-- end col-4 -->
                                                <!-- begin col-4 -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Date of Join</label><span class="mandatory">*</span>
                                                        <asp:TextBox ID="txtDOJ" runat="server" class="form-control datepicker" placeholder="dd/MM/YYYY"></asp:TextBox>
                                                         <asp:RequiredFieldValidator ControlToValidate="txtDOJ" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator10" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                        </asp:RequiredFieldValidator>
                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                TargetControlID="txtDOJ" ValidChars="0123456789/">
                                                            </cc1:FilteredTextBoxExtender>
                                                    </div>
                                                </div>
                                                <!-- end col-4 -->
                                               <!-- begin col-4 -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Department</label><span class="mandatory">*</span>
                                                        <asp:DropDownList ID="ddlDepartment" runat="server" 
                                                            class="form-control select2" style="width:100%;" 
                                                            onselectedindexchanged="ddlDepartment_SelectedIndexChanged" AutoPostBack="true">
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ControlToValidate="ddlDepartment" Display="Dynamic" InitialValue="0" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator11" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                        </asp:RequiredFieldValidator>
                                                    </div>
                                                   
                                                </div>
                                                <!-- end col-4 -->
                                               
                                            </div>
                                            <div class="row">
                                               
                                                <!-- begin col-4 -->
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label>Designation</label><span class="mandatory">*</span>
                                                        <asp:DropDownList ID="ddlDesignation" runat="server" class="form-control select2" style="width:100%;" >
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ControlToValidate="ddlDesignation" Display="Dynamic" InitialValue="-Select-" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator12" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                        </asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                                <!-- end col-4 -->
                                               
                                                 <!-- begin col-4 -->
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <div class="form-group">
                                                            <label>Qualification</label><span class="mandatory">*</span>
                                                            <asp:TextBox ID="txtQulification" runat="server" class="form-control">
                                                            </asp:TextBox>
                                                       
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- end col-4 -->
                                                <!-- begin col-4 -->
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label>Employee MobileNo</label><span class="mandatory">*</span>
                                                        <asp:TextBox ID="txtEmpMobileNo" runat="server" class="form-control" MaxLength="10"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ControlToValidate="txtEmpMobileNo" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator13" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                        </asp:RequiredFieldValidator>
                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                TargetControlID="txtEmpMobileNo" ValidChars="0123456789">
                                                        </cc1:FilteredTextBoxExtender>
                                                    </div>
                                                </div>
                                                <!-- end col-4 -->
                                                 <!-- begin col-4 -->
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label>OT Eligible</label><span class="mandatory">*</span>
                                                        <asp:DropDownList ID="ddlOTEligible" runat="server" class="form-control select2" style="width:100%;">
                                                        <asp:ListItem Value="0" Text="-Select-"></asp:ListItem>
                                                         <asp:ListItem Value="YES" Text="YES"></asp:ListItem>
                                                         <asp:ListItem Value="NO" Text="NO"></asp:ListItem>
                                                        </asp:DropDownList>
                                                         <asp:RequiredFieldValidator ControlToValidate="ddlOTEligible" InitialValue="0" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator14" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                        </asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                                <!-- end col-4 -->
                                                  <!-- begin col-2 -->
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label>Wages Type</label><span class="mandatory">*</span>
                                                        <asp:DropDownList ID="ddlWagesType" runat="server" class="form-control select2" style="width:100%;">
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ControlToValidate="ddlWagesType" InitialValue="0" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator15" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                        </asp:RequiredFieldValidator>
                                                    </div>
                                                   
                                                </div>
                                                <!-- end col-2 -->
                                                  <!-- begin col-2 -->
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label>Employee Level</label><span class="mandatory">*</span>
                                                        <asp:DropDownList ID="ddlEmpLevel" runat="server" class="form-control select2" style="width:100%;">
                                                        <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
                                                        <asp:ListItem Value="Trainee">Trainee</asp:ListItem>
                                                        <asp:ListItem Value="Semi-Exp">Semi-Exp</asp:ListItem>
                                                        <asp:ListItem Value="Experienced">Experienced</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ControlToValidate="ddlEmpLevel" InitialValue="-Select-" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator32" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                        </asp:RequiredFieldValidator>
                                                    </div>
                                                   
                                                </div>
                                                <!-- end col-2 -->
                                            </div>
                                            
                                            <div class="row">
                                                <!-- begin col-4 -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>PF Eligible</label><span class="mandatory">*</span> <asp:CheckBox ID="chkExment" runat="server" /> Exempted Staff 
                                                         <asp:RadioButtonList ID="RdbPFEligible" runat="server" class="form-control" 
                                                            RepeatColumns="2" AutoPostBack="true" onselectedindexchanged="RdbPFEligible_SelectedIndexChanged">
                                                              <asp:ListItem Value="1" Text="Yes" style="padding-right:40px" ></asp:ListItem>
                                                              <asp:ListItem Value="2" Text="No" Selected="True"></asp:ListItem>
                                                         </asp:RadioButtonList>
                                                    </div>
                                                </div>
                                               
                                                <!-- begin col-4 -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>PF No</label> 
                                                        <asp:TextBox ID="txtPFNo" runat="server" class="form-control" Enabled="false"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <!-- end col-4 -->
                                                <!-- begin col-4 -->
                                                <div class="col-md-4">
                                                    <div class="form-group block1">
                                                        <label>PF Date</label>
                                                        <asp:TextBox ID="txtPFDate" runat="server" class="form-control datepicker" Enabled="false" placeholder="dd/MM/YYYY"></asp:TextBox>
                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                TargetControlID="txtPFDate" ValidChars="0123456789/">
                                                        </cc1:FilteredTextBoxExtender>
                                                    </div>
                                                </div>
                                                <!-- end col-4 -->
                                            </div>
                                            <div class="row">
                                                <!-- begin col-4 -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>ESI Eligible</label><span class="mandatory">*</span>
                                                           <asp:RadioButtonList ID="RdbESIEligible" runat="server" class="form-control" 
                                                            RepeatColumns="2" AutoPostBack="true" onselectedindexchanged="RdbESIEligible_SelectedIndexChanged">
                                                              <asp:ListItem Value="1" Text="Yes" style="padding-right:40px" ></asp:ListItem>
                                                              <asp:ListItem Value="2" Text="No" Selected="True"></asp:ListItem>
                                                            </asp:RadioButtonList>
                                                      
                                                    </div>
                                                </div>
                                                <!-- end col-4 -->
                                                <!-- begin col-4 -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>ESI No</label>
                                                        <asp:TextBox ID="txtESINo" runat="server" class="form-control" Enabled="false"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <!-- end col-4 -->
                                                <!-- begin col-4 -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>ESI Date</label>
                                                        <asp:TextBox ID="txtESIDate" runat="server" class="form-control datepicker" Enabled="false" placeholder="dd/MM/YYYY"></asp:TextBox>
                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                TargetControlID="txtESIDate" ValidChars="0123456789/">
                                                        </cc1:FilteredTextBoxExtender>
                                                    </div>
                                                </div>
                                                <!-- end col-4 -->
                                            </div>
                                            <div class="row">
                                             
                                                <!-- begin col-4 -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>UAN</label>
                                                       <asp:TextBox ID="txtUAN" runat="server" class="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <!-- end col-4 -->
                                                  <!-- begin col-4 -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>PF Code</label>
                                                       <asp:DropDownList ID="ddlPFCode" runat="server" class="form-control select2" style="width:100%;">
                                                       </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <!-- end col-4 -->
                                                  <!-- begin col-4 -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>ESI Code</label>
                                                       <asp:DropDownList ID="ddlESICode" runat="server" class="form-control select2" style="width:100%;">
                                                       </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <!-- end col-4 -->
                                            </div>
                                            <div class="row"> 
                                              <!-- begin col-4 -->
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label>Hostel Room No</label>
                                                       <asp:TextBox ID="txtHostelRoom" runat="server" class="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <!-- end col-4 -->
                                                 <!-- begin col-4 -->
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label>Vehicle Type</label>
                                                       <asp:DropDownList ID="ddlVehicleType" runat="server" 
                                                            class="form-control select2" style="Width:100%" AutoPostBack="true" 
                                                            onselectedindexchanged="ddlVehicleType_SelectedIndexChanged">
                                                       <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
                                                       <asp:ListItem Value="Company">Company</asp:ListItem>
                                                       <asp:ListItem Value="OWN">OWN</asp:ListItem>
                                                       <asp:ListItem Value="Private">Private</asp:ListItem>
                                                       </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <!-- end col-4 -->
                                                <!-- begin col-4 -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Route</label>
                                                        <asp:DropDownList ID="txtVillage" runat="server" class="form-control select2" 
                                                            style="Width:100%" AutoPostBack="true" 
                                                            onselectedindexchanged="txtVillage_SelectedIndexChanged">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <!-- end col-4 -->
                                                 <!-- begin col-4 -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Bus No</label>
                                                        <asp:DropDownList ID="txtBusNo" runat="server" class="form-control select2" style="Width:100%">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                <!-- end col-4 -->
                                                
                                            </div>
                                            
                                            <div class="row">
                                                 <!-- begin col-4 -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Active Mode</label>
                                                        
                                                            <asp:RadioButtonList ID="dbtnActive" runat="server" class="form-control" 
                                                                RepeatDirection="Horizontal" AutoPostBack="true" 
                                                            onselectedindexchanged="dbtnActive_SelectedIndexChanged">
                                                              <asp:ListItem Value="1" Text="Yes" style="padding-right:40px" Selected="True"></asp:ListItem>
                                                              <asp:ListItem Value="2" Text="No"></asp:ListItem>
                                                            </asp:RadioButtonList>
                                                            
                                                        
                                                    </div>
                                                </div>
                                                <!-- end col-4 -->
                                                <!-- begin col-4 -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Relieved Date</label>
                                                        <asp:TextBox ID="txtReliveDate" runat="server" class="form-control datepicker" placeholder="dd/MM/YYYY"></asp:TextBox>
                                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                TargetControlID="txtReliveDate" ValidChars="0123456789/">
                                                            </cc1:FilteredTextBoxExtender>
                                                    </div>
                                                </div>
                                                <!-- end col-4 -->
                                                <!-- begin col-4 -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Reason</label>
                                                        <asp:TextBox ID="txtReason" runat="server" class="form-control" TextMode="MultiLine" Enabled="false"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <!-- end col-4 -->
                                                
                                            </div>
                                           
                                             <div class="row"> 
                                              <!-- begin col-4 -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>480 Days Completed</label>
                                                       <asp:TextBox ID="txt480Days" runat="server" class="form-control datepicker" placeholder="dd/MM/YYYY"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <!-- end col-4 -->
                                                
                                                <!-- begin col-4 -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Certificate</label>
                                                       <asp:TextBox ID="txtCertificate" runat="server" class="form-control" TextMode="MultiLine"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <!-- end col-4 -->
                                              
                                             <!-- begin col-4 -->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Week - Off</label><span class="mandatory">*</span>
                                                    <asp:DropDownList ID="ddlWeekOff" runat="server" class="form-control select2" Width="100%">
                                                        <asp:ListItem Value="0" Text="-Select-"></asp:ListItem>
                                                        <asp:ListItem Value="Sunday" Text="Sunday"></asp:ListItem>
                                                        <asp:ListItem Value="Monday" Text="Monday"></asp:ListItem>
                                                        <asp:ListItem Value="Tuesday" Text="Tuesday"></asp:ListItem>
                                                        <asp:ListItem Value="Wednesday" Text="Wednesday"></asp:ListItem>
                                                        <asp:ListItem Value="Thurday" Text="Thurday"></asp:ListItem>
                                                        <asp:ListItem Value="Friday" Text="Friday"></asp:ListItem>
                                                        <asp:ListItem Value="Saturday" Text="Saturday"></asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ControlToValidate="ddlWeekOff" InitialValue="0" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator19" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                        </asp:RequiredFieldValidator>
                                                    
                                                </div>
                                            </div>
                                            <!-- end col-4 -->
                                            
                                           </div>
                                           
                                           
                                        </div>
                                      
                                   </fieldset>
                          </ContentTemplate>
                        </asp:UpdatePanel>
						</div>
						<div class="tab-pane fade" id="default-tab-2">
						<asp:UpdatePanel ID="UpdatePanel3" runat="server">
                        <ContentTemplate>
							<fieldset>
                                      <legend class="pull-left width-full">Bank/Cash</legend>
                                        <!-- begin row -->
                                        <div class="row">
                                            <!-- begin col-3 -->
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>Salary Through</label><span class="mandatory">*</span>
                                                    <asp:RadioButtonList ID="rbtnSalaryThrough" runat="server" 
                                                        RepeatDirection="Horizontal" class="form-control" AutoPostBack="true" 
                                                        onselectedindexchanged="rbtnSalaryThrough_SelectedIndexChanged">
                                                    <asp:ListItem Value="1" Text="Cash" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Value="2" Text="Bank"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </div>
                                            </div>
                                            <!-- end col-3 -->
                                            <!-- begin col-3 -->
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>Bank Name</label>
                                                    <asp:DropDownList ID="ddlBankName" runat="server" class="form-control select2" style="width:100%;">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <!-- end col-3 -->
                                            <!-- begin col-3 -->
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>IFSC Code</label>
                                                    <asp:TextBox ID="txtIFSC" runat="server" class="form-control" Enabled="false"></asp:TextBox>
                                                    
                                                </div>
                                            </div>
                                            <!-- end col-3 -->
                                            <!-- begin col-3 -->
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>Branch</label>
                                                   <asp:TextBox ID="txtBranch" runat="server" class="form-control" Enabled="false"></asp:TextBox>
                                                </div>
                                            </div>
                                            <!-- end col-3 -->
                                        </div>
                                        <!-- end row -->
                                        <!-- begin row -->
                                        <div class="row">
                                         <!-- begin col-3 -->
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>Account Number</label><span class="mandatory">*</span>
                                                   <asp:TextBox ID="txtAccNo" runat="server" class="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <!-- end col-3 -->
                                        </div>
                                        <!-- end row -->
                                        <legend class="pull-left width-full">Fixed Salary</legend>
                                        <!-- begin row -->
                                        <div class="row">
                                            <!-- begin col-2 -->
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label>Basic Salary</label>
                                                    <asp:TextBox ID="txtBasic" runat="server" class="form-control" Text="0.0"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                TargetControlID="txtBasic" ValidChars="0123456789.">
                                                            </cc1:FilteredTextBoxExtender>
                                                </div>
                                            </div>
                                            <!-- end col-2 -->
                                            <!-- begin col-2 -->
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label>VPF</label>
                                                    <asp:TextBox ID="txtVPF" runat="server" class="form-control" Text="0.0"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender14" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                TargetControlID="txtVPF" ValidChars="0123456789.">
                                                            </cc1:FilteredTextBoxExtender>
                                                </div>
                                            </div>
                                            <!-- end col-2 -->
                                            <!-- begin col-2 -->
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label>Allowance 1</label>
                                                    <asp:TextBox ID="txtAllowance1" runat="server" class="form-control" Text="0.0"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender15" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                TargetControlID="txtAllowance1" ValidChars="0123456789.">
                                                    </cc1:FilteredTextBoxExtender>
                                                </div>
                                            </div>
                                            <!-- end col-2 -->
                                            <!-- begin col-2 -->
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label>Allowance 2</label>
                                                    <asp:TextBox ID="txtAllowance2" runat="server" class="form-control" Text="0.0"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender16" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                TargetControlID="txtAllowance2" ValidChars="0123456789.">
                                                    </cc1:FilteredTextBoxExtender>
                                                </div>
                                            </div>
                                            <!-- end col-2 -->
                                            <!-- begin col-2 -->
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label>Deduction 1</label>
                                                    <asp:TextBox ID="txtDeduction1" runat="server" class="form-control" Text="0.0"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender17" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                TargetControlID="txtDeduction1" ValidChars="0123456789.">
                                                    </cc1:FilteredTextBoxExtender>
                                                </div>
                                            </div>
                                            <!-- end col-2 -->
                                            <!-- begin col-2 -->
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label>Deduction 2</label>
                                                    <asp:TextBox ID="txtDeduction2" runat="server" class="form-control" Text="0.0"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender18" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                TargetControlID="txtDeduction2" ValidChars="0123456789.">
                                                    </cc1:FilteredTextBoxExtender>
                                                </div>
                                            </div>
                                            <!-- end col-2 -->
                                        </div>
                                        <!-- end row -->
                                       <!-- begin row -->
                                        <div class="row">
                                        <!-- begin col-2 -->
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label>OT Salary</label>
                                                    <asp:TextBox ID="txtOTSal" runat="server" class="form-control" Text="0.0"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender19" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                TargetControlID="txtOTSal" ValidChars="0123456789.">
                                                    </cc1:FilteredTextBoxExtender>
                                                </div>
                                            </div>
                                            <!-- end col-2 -->
                                        </div>
                                        <!-- end row -->
                                   </fieldset>
                        </ContentTemplate>
                        </asp:UpdatePanel>
						</div>
						<div class="tab-pane fade" id="default-tab-3">
							<fieldset>
                                        <legend class="pull-left width-full">Personal</legend>
                                        <div class="col-md-9">
                                        <!-- begin row -->
                                        <div class="row">
                                            <!-- begin col-4 -->
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>Martial Status</label><span class="mandatory">*</span>
                                                    <asp:DropDownList ID="ddlMartialStatus" runat="server" class="form-control select2" style="width:100%;">
                                                     <asp:ListItem Value="0" Text="-Select-"></asp:ListItem>
                                                     <asp:ListItem Value="SINGLE" Text="SINGLE"></asp:ListItem>
                                                     <asp:ListItem Value="MARRIED" Text="MARRIED"></asp:ListItem>
                                                     <asp:ListItem Value="DIVORCED" Text="DIVORCED"></asp:ListItem>
                                                    </asp:DropDownList>
                                                   <asp:RequiredFieldValidator ControlToValidate="ddlMartialStatus" InitialValue="0" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator16" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                   </asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                            <!-- end col-4 -->
                                            <!-- begin col-4 -->
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>Nationality</label>
                                                    <asp:TextBox ID="txtNationality" runat="server" Text="INDIAN" class="form-control">
                                                    </asp:TextBox>
                                                </div>
                                            </div>
                                            <!-- end col-4 -->
                                            <!-- begin col-4 -->
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label>Religion</label>
                                                    <asp:TextBox ID="txtReligion" runat="server" class="form-control">
                                                    </asp:TextBox>
                                                </div>
                                            </div>
                                            <!-- end col-4 -->
                                            <!-- begin col-2 -->
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label>Height</label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon">Cms</span>
                                                        <asp:TextBox ID="txtHeight" runat="server" class="form-control"></asp:TextBox>
                                                         <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                TargetControlID="txtHeight" ValidChars="0123456789.">
                                                        </cc1:FilteredTextBoxExtender>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- end col-2 -->
                                            <!-- begin col-2 -->
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label>Weight</label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon">Kg</span>
                                                        <asp:TextBox ID="txtWeight" runat="server" class="form-control"></asp:TextBox>
                                                      
                                                       <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                TargetControlID="txtWeight" ValidChars="0123456789.">
                                                        </cc1:FilteredTextBoxExtender>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

           
                                        <!-- end row -->
                                        <!-- begin row -->
                                        <div class="row">
                                            
                                            <!-- end col-2 -->
                                               <!-- begin col-2 -->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Std Working Hrs</label><span class="mandatory">*</span>
                                                    <asp:TextBox ID="txtStdWorkingHrs" runat="server" class="form-control"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                TargetControlID="txtStdWorkingHrs" ValidChars="0123456789">
                                                        </cc1:FilteredTextBoxExtender>
                                                </div>
                                            </div>
                                            <!-- end col-2 -->
                                            <!-- begin col-2 -->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Physically Challenged</label>
                                                    <asp:RadioButtonList ID="rbtnPhysically" runat="server" class="form-control" 
                                                        RepeatDirection="Horizontal" AutoPostBack="true" 
                                                        onselectedindexchanged="rbtnPhysically_SelectedIndexChanged">
                                                      <asp:ListItem Value="1" Text="Yes" style="padding-right:40px"></asp:ListItem>
                                                      <asp:ListItem Value="2" Text="No" Selected="True"></asp:ListItem>
                                                    </asp:RadioButtonList>

                                                </div>
                                            </div>
                                            <!-- end col-2 -->
                                            <!-- begin col-2 -->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Physically Reason</label>
                                                    <asp:TextBox ID="txtPhyReason" runat="server" class="form-control" Enabled="false"></asp:TextBox>
                                                </div>
                                            </div>
                                            <!-- end col-2 -->
                                         
                                          
                                        </div>
                                        <!-- end row -->
                                        </div>
                                         <div class="col-md-3">
                                             <div class="media" style="margin-top: -19px;">
                                                    <a class="media-right" href="javascript:;" style="float: right;">
                                                        <asp:Image ID="Image1" runat="server" class="media-object" style="width: 158px;height: 161px;" ImageUrl="~/assets/img/login-bg/man-user-50.png" />
                                                        
                                                    </a>

                                                </div>
                                            </div>
                                        <!-- begin row -->
                                        <div class="row">
                                            <!-- begin col-4 -->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Blood Group</label><br />
                                                    <asp:DropDownList ID="ddlBloodGrp" runat="server" class="form-control select2"  Width="100%">
                                                    <asp:ListItem Value="0" Text="-Select-"></asp:ListItem>
                                                    <asp:ListItem Value="A+" Text="A+"></asp:ListItem>
                                                    <asp:ListItem Value="A-" Text="A-"></asp:ListItem>
                                                    <asp:ListItem Value="B+" Text="B+"></asp:ListItem>
                                                    <asp:ListItem Value="B-" Text="B-"></asp:ListItem>
                                                    <asp:ListItem Value="AB+" Text="AB+"></asp:ListItem>
                                                    <asp:ListItem Value="AB-" Text="AB-"></asp:ListItem>
                                                    <asp:ListItem Value="O+" Text="O+"></asp:ListItem>
                                                    <asp:ListItem Value="O-" Text="O-"></asp:ListItem>
                                                    </asp:DropDownList>
                                                   <asp:RequiredFieldValidator ControlToValidate="ddlBloodGrp" InitialValue="0" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator17" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                   </asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                            <!-- end col-4 -->
                                            <!-- begin col-4 -->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Recruitment Through</label>
                                                    <asp:DropDownList ID="txtRecruitThrg" runat="server" 
                                                        class="form-control select2" style="width:100%" AutoPostBack="true" 
                                                        onselectedindexchanged="txtRecruitThrg_SelectedIndexChanged">
                                                    <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
                                                    <asp:ListItem Value="Recruitment Officer">Recruitment Officer</asp:ListItem>
                                                    <asp:ListItem Value="Existing Employee">Existing Employee</asp:ListItem>
                                                    <asp:ListItem Value="Agent">Agent</asp:ListItem>
                                                    <asp:ListItem Value="Parents">Parents</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ControlToValidate="ddlBloodGrp" InitialValue="-Select-" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator18" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                   </asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                            <!-- end col-4 -->
                                            <!-- begin col-4 -->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label runat="server" id="lblRecruit">Recruiter Name</label>
                                                    <label runat="server" id="lblAgent" visible="false">Agent Name</label>
                                                    <asp:DropDownList ID="txtRecruitmentName" runat="server" 
                                                        class="form-control select2" AutoPostBack="true" style="width:100%" 
                                                        onselectedindexchanged="txtRecruitmentName_SelectedIndexChanged"></asp:DropDownList>
                                                    
                                                </div>
                                            </div>
                                            <!-- end col-4 -->
                                        </div>
                                        <!-- end row -->
                                       
                                        <!-- begin row -->
                                        <div class="row">
                                           
                                             <!-- begin col-4 -->
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label>Unit</label>
                                                    <asp:DropDownList ID="ddlUnit" runat="server" class="form-control select2" style="width:100%">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <!-- end col-4 -->
                                            <!-- begin col-4 -->
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label>Existing Employee No</label>
                                                    <asp:TextBox ID="txtExistingEmpNo" runat="server" class="form-control"></asp:TextBox>
                                                    
                                                </div>
                                            </div>
                                            <!-- end col-4 -->
                                            <!-- begin col-4 -->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Recruiter Mobile</label>
                                                    <asp:TextBox ID="txtRecruitMobile" runat="server" class="form-control" MaxLength="10"></asp:TextBox>
                                                    
                                                </div>
                                            </div>
                                            <!-- end col-4 -->
                                            <!-- begin col-4 -->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Existing Employee Name</label>
                                                    <asp:TextBox ID="txtExistingEmpName" runat="server" class="form-control"></asp:TextBox>
                                                    
                                                </div>
                                            </div>
                                            <!-- end col-4 -->
                                        </div>
                                        <!-- end row -->
                                         <!-- begin row -->
                                        <div class="row">
                                            <!-- begin col-4 -->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Parents Name</label>
                                                    <asp:TextBox ID="txtRefParentsName" runat="server" class="form-control" Enabled="false"></asp:TextBox>
                                                    
                                                </div>
                                            </div>
                                            <!-- end col-4 -->
                                             <!-- begin col-4 -->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Mobile No</label>
                                                    <asp:TextBox ID="txtRefMobileNo" runat="server" class="form-control" MaxLength="10" Enabled="false"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender20" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                TargetControlID="txtRefMobileNo" ValidChars="0123456789">
                                                        </cc1:FilteredTextBoxExtender>
                                                </div>
                                            </div>
                                            <!-- end col-4 -->
                                             <!-- begin col-4 -->
                                            <div id="Div1" class="col-md-2" runat="server" visible="false">
                                                <div class="form-group">
                                                    <label>Working Unit</label>
                                                    <asp:DropDownList ID="ddlWorkingUnit" runat="server" class="form-control select2" style="width:100%">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <!-- end col-4 -->
                                             <!-- begin col-4 -->
                                            <div id="Div2" class="col-md-2" runat="server" visible="false">
                                                <div class="form-group">
                                                    <label>Salary Unit</label>
                                                    <asp:DropDownList ID="ddlSalaryUnit" runat="server" class="form-control select2" style="width:100%">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <!-- end col-4 -->
                                              <!-- begin col-4 -->
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label>Grade</label>
                                                    <asp:DropDownList ID="ddlGrade" runat="server" class="form-control select2" style="width:100%">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <!-- end col-4 -->
                                              <!-- begin col-4 -->
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label>Division</label>
                                                    <asp:DropDownList ID="ddlDivision" runat="server" class="form-control select2" style="width:100%">
                                                    </asp:DropDownList>
                                                   <asp:RequiredFieldValidator ControlToValidate="ddlDivision" InitialValue="-Select-" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator30" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                   </asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                            <!-- end col-4 -->
                                            </div>
                                        <!-- end row -->
                                         <!-- begin row -->
                                        <div class="row">
                                          <!-- begin col-4 -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Leave From</label>
                                                        <asp:TextBox ID="txtLeaveFrom" runat="server" class="form-control datepicker" 
                                                            placeholder="dd/MM/YYYY" ></asp:TextBox>
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender21" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                TargetControlID="txtLeaveFrom" ValidChars="0123456789/">
                                                            </cc1:FilteredTextBoxExtender>
                                                    </div>
                                                </div>
                                                <!-- end col-4 -->
                                                 <!-- begin col-4 -->
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Leave To</label>
                                                        <asp:TextBox ID="txtLeaveTo" runat="server" class="form-control datepicker" 
                                                            placeholder="dd/MM/YYYY" ></asp:TextBox>
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender22" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                TargetControlID="txtLeaveTo" ValidChars="0123456789/">
                                                            </cc1:FilteredTextBoxExtender>
                                                    </div>
                                                </div>
                                                <!-- end col-4 -->
                                        </div>
                                         <!-- end row -->
                                    </fieldset>
						</div>
						<div class="tab-pane fade" id="default-tab-4">
						 <fieldset>
                                        <legend class="pull-left width-full">General</legend>
                                        <!-- begin row -->
                                        <div class="row">
                                           
                                            <!-- begin col-4 -->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Nominee</label>
                                                    <asp:TextBox ID="txtNominee" runat="server" class="form-control"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ControlToValidate="txtNominee" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator31" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                   </asp:RequiredFieldValidator>

                                                </div>
                                            </div>
                                            <!-- end col-4 -->
                                            <!-- begin col-4 -->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Father Name/Spouse Name</label>
                                                    <asp:TextBox ID="txtFatherName" runat="server" class="form-control"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ControlToValidate="txtFatherName" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator34" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                    </asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                            <!-- end col-4 -->
                                             <!-- begin col-4 -->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Parents Mobile1</label>
                                                    <asp:TextBox ID="txtParentMob1" runat="server" class="form-control" MaxLength="10"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ControlToValidate="txtParentMob1" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator20" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                    </asp:RequiredFieldValidator>
                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                TargetControlID="txtParentMob1" ValidChars="0123456789">
                                                        </cc1:FilteredTextBoxExtender>
                                                </div>
                                            </div>
                                            <!-- end col-4 -->
                                        </div>
                                        <!-- end row -->
                                        <!-- begin row -->
                                        <div class="row">
                                       
                                            <!-- begin col-4 -->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Mother Name</label>
                                                    <asp:TextBox ID="txtMotherName" runat="server" class="form-control"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ControlToValidate="txtMotherName" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator35" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                    </asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                            <!-- end col-4 -->
                                             <!-- begin col-4 -->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Parents Mobile2</label>
                                                    <asp:TextBox ID="txtParentMob2" runat="server" class="form-control" MaxLength="10"></asp:TextBox>
                                                   <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                TargetControlID="txtParentMob2" ValidChars="0123456789">
                                                        </cc1:FilteredTextBoxExtender>
                                                </div>
                                            </div>
                                            <!-- end col-4 -->
                                           <!-- begin col-4 -->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Guardian Name</label>
                                                    <asp:TextBox ID="txtGuardianName" runat="server" class="form-control"></asp:TextBox>
                                                    
                                                </div>
                                            </div>
                                            <!-- end col-4 -->


                                        </div>
                                        <!-- end row -->
                                        <!-- begin row -->
                                        <div class="row">
                                         
                                             <!-- begin col-4 -->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Guardian Mobile</label>
                                                    <asp:TextBox ID="txtGuardianMobile" runat="server" class="form-control" MaxLength="10"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                TargetControlID="txtGuardianMobile" ValidChars="0123456789">
                                                        </cc1:FilteredTextBoxExtender>
                                                </div>
                                            </div>
                                            <!-- end col-4 -->
                                            <!-- begin col-4 -->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Permanent Address</label>
                                                    <asp:TextBox ID="txtPermAddr" runat="server" class="form-control" TextMode="MultiLine"></asp:TextBox>
                                                     <asp:RequiredFieldValidator ControlToValidate="txtPermAddr" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator21" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                    </asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                            <!-- end col-4 -->
                                             <!-- begin col-4 -->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Permanent Taluk</label>
                                                    <asp:DropDownList ID="txtPermTaluk" runat="server" class="form-control select2" style="width:100%"></asp:DropDownList>
                                                    <asp:RequiredFieldValidator ControlToValidate="txtPermTaluk" InitialValue="-Select-" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator22" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                    </asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                            <!-- end col-4 -->
                                        </div>
                                        <!-- end row -->
                                        <!-- begin row -->
                                        <div class="row">
                                        
                                             <!-- begin col-4 -->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Permanent District</label>
                                                    <asp:DropDownList ID="txtPermDist" runat="server" class="form-control select2" style="width:100%"></asp:DropDownList>
                                                     <asp:RequiredFieldValidator ControlToValidate="txtPermDist" InitialValue="-Select-" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator23" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                    </asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                            <!-- end col-4 -->
                                            <!-- begin col-4 -->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>State  </label>   <asp:CheckBox ID="chkOtherState" runat="server" /> OtherState
                                                         <asp:DropDownList ID="ddlState" runat="server" class="form-control select2" Width="100%">
                                                        <%--<asp:ListItem Value="0" Text="-Select-"></asp:ListItem>
                                                        <asp:ListItem Value="1" Text="Andhra Pradesh"></asp:ListItem>
                                                        <asp:ListItem Value="2" Text="Arunachal Pradesh"></asp:ListItem>
                                                        <asp:ListItem Value="3" Text="Assam"></asp:ListItem>
                                                        <asp:ListItem Value="4" Text="Bihar"></asp:ListItem>
                                                        <asp:ListItem Value="5" Text="Chhattisgarh"></asp:ListItem>
                                                        <asp:ListItem Value="6" Text="Goa"></asp:ListItem>
                                                        <asp:ListItem Value="7" Text="Gujarat"></asp:ListItem>
                                                        <asp:ListItem Value="8" Text="Haryana"></asp:ListItem>
                                                        <asp:ListItem Value="9" Text="Himachal Pradesh"></asp:ListItem>
                                                        <asp:ListItem Value="10" Text="Jammu and Kashmir"></asp:ListItem>
                                                        <asp:ListItem Value="11 Text="Jharkhand"></asp:ListItem>
                                                        <asp:ListItem Value="12" Text="Karnataka"></asp:ListItem>
                                                        <asp:ListItem Value="13" Text="Kerala"></asp:ListItem>
                                                        <asp:ListItem Value="14" Text="Madhya Pradesh"></asp:ListItem>
                                                        <asp:ListItem Value="15" Text="Maharashtra"></asp:ListItem>
                                                        <asp:ListItem Value="16" Text="Manipur"></asp:ListItem>
                                                        <asp:ListItem Value="17" Text="Meghalaya"></asp:ListItem>
                                                        <asp:ListItem Value="18 Text="Mizoram"></asp:ListItem>
                                                        <asp:ListItem Value="19" Text="Nagaland"></asp:ListItem>
                                                        <asp:ListItem Value="20" Text="Orissa"></asp:ListItem>
                                                        <asp:ListItem Value="21" Text="Punjab"></asp:ListItem>
                                                        <asp:ListItem Value="22" Text="Rajasthan"></asp:ListItem>
                                                        <asp:ListItem Value="23" Text="Sikkim"></asp:ListItem>
                                                        <asp:ListItem Value="24" Text="Tamil Nadu"></asp:ListItem>
                                                        <asp:ListItem Value="25 Text="Telangana"></asp:ListItem>
                                                        <asp:ListItem Value="26" Text="Tripura"></asp:ListItem>
                                                        <asp:ListItem Value="27" Text="Uttar Pradesh"></asp:ListItem>
                                                        <asp:ListItem Value="28" Text="Uttarakhand"></asp:ListItem>
                                                        <asp:ListItem Value="29" Text="West Bengal"></asp:ListItem>--%>
                                                        </asp:DropDownList>
                                                         <asp:RequiredFieldValidator ControlToValidate="ddlState" InitialValue="-Select-" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator24" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                    </asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                             <!-- begin col-4 -->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Temp Taluk</label>
                                                    <asp:DropDownList ID="txtTempTaluk" runat="server" class="form-control select2" style="width:100%"></asp:DropDownList>
                                                    <asp:RequiredFieldValidator ControlToValidate="txtTempTaluk" InitialValue="-Select-" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator25" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                    </asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                            <!-- end col-4 -->
                                        </div>
                                        <!-- end row -->
                                        <!-- begin row -->
                                        <div class="row">
                                       
                                             <!-- begin col-4 -->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Temp District</label>
                                                    <asp:DropDownList ID="txtTempDist" runat="server" class="form-control select2" style="width:100%"></asp:DropDownList>
                                                    <asp:RequiredFieldValidator ControlToValidate="txtTempDist" InitialValue="-Select-" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator26" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                    </asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                            <!-- end col-4 -->
                                         <!-- begin col-4 -->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Temp Address</label>  <asp:CheckBox ID="chkSame" runat="server" 
                                                        AutoPostBack="true" oncheckedchanged="chkSame_CheckedChanged" />Same as Permanent
                                                    <asp:TextBox ID="txtTempAddr" runat="server" class="form-control" TextMode="MultiLine"></asp:TextBox>
                                                     <asp:RequiredFieldValidator ControlToValidate="txtTempAddr" InitialValue="-Select-" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator27" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                    </asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                            <!-- end col-4 -->
                                             <!-- begin col-4 -->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Identification Mark1</label>
                                                    <asp:TextBox ID="txtIdenMark1" runat="server" class="form-control"></asp:TextBox>
                                                     <asp:RequiredFieldValidator ControlToValidate="txtIdenMark1" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator28" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                    </asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                            <!-- end col-4 -->
                                        </div>
                                         <!-- end row -->
                                        <!-- begin row -->
                                        <div class="row">
                                           
                                            <!-- begin col-4 -->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>Identification Mark2</label>
                                                    <asp:TextBox ID="txtIdenMark2" runat="server" class="form-control"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ControlToValidate="txtIdenMark2" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator29" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                    </asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                            <!-- end col-4 -->
                                        </div>
                                        <!-- end row -->
                                        <!-- begin row -->
                                        <div class="row">
                                           
                                        </div>
                                        <!-- end row -->
                                    </fieldset>
						</div>
						<div class="tab-pane fade" id="default-tab-5">
						<asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
						 <fieldset>
                                        <legend class="pull-left width-full">Documents</legend>

                                        <!-- begin row -->
                                        <div class="row">
                                            <!-- begin col-4 -->
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label>Document Type</label>
                                                    <asp:DropDownList ID="ddlDocType" runat="server" class="form-control select2" Width="100%">
                                                        <asp:ListItem Value="0" Text="-Select-"></asp:ListItem>
                                                        <asp:ListItem Value="1" Text="Adhar Card"></asp:ListItem>
                                                        <asp:ListItem Value="2" Text="Voter Card"></asp:ListItem>
                                                        <asp:ListItem Value="3" Text="Ration Card"></asp:ListItem>
                                                        <asp:ListItem Value="4" Text="Pan Card"></asp:ListItem>
                                                        <asp:ListItem Value="5" Text="Driving Licence"></asp:ListItem>
                                                        <asp:ListItem Value="6" Text="Smart Card"></asp:ListItem>
                                                        
                                                      </asp:DropDownList>
                                                   
                                                </div>
                                            </div>
                                            <!-- end col-4 -->
                                            <!-- begin col-4 -->
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>Document No</label>
                                                    <asp:TextBox ID="txtDocNo" runat="server" class="form-control"></asp:TextBox>
                                                </div>
                                            </div>
                                            <!-- end col-4 -->
                                            <!-- begin col-4 -->
                                            <%--<div class="col-md-3">
                                                <div class="form-group">
                                                    <label>Document Description</label>
                                                    <asp:TextBox ID="txtDocDesc" runat="server" class="form-control"></asp:TextBox>
                                                    
                                                </div>
                                            </div>--%>
                                            <!-- end col-4 -->
                                           
                                          <div class="col-md-2">
                                                <div class="form-group">
                                                    <asp:Button ID="btnDocAdd" runat="server" class="btn btn-success" style="margin-top: 16%;" Text="ADD" onclick="btnDocAdd_Click"/>
                                                </div>
                                            </div>
                                              <div class="col-md-2"></div>
                                            <div class="col-md-2">
                                                <div class="media" style="margin-top: -19px;">
                                                    <a class="media-right" href="javascript:;" style="float: right;">
                                                    <asp:Image ID="Image2" runat="server" class="media-object rounded-corner" style="width: 158px;height: 120px;" ImageUrl="~/assets/img/login-bg/man-user-50.png" />
                                                        
                                                    </a>

                                                </div>
                                            </div>

                                        </div>
                                        <!-- end row -->
                                         <!-- begin row -->
                                        <div class="row">
                                         <div class="col-md-2">
                                           <asp:FileUpload ID="filUpload" runat="server" CssClass="btn btn-default fileinput-button" style="margin-bottom: 20px;"  name="filUpload"/>
                                        </div>
                                       </div>
                                      <!-- end row -->
                                      <div class="row"> </div>
                                          <!-- table start -->
					<div class="col-md-12">
					    <div class="row">
					        <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
			                    <HeaderTemplate>
                                    <table id="example" class="display table">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th>DocType</th>
                                                <th>DocNo</th>
                                                <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Container.ItemIndex + 1 %></td>
                                        <td><%# Eval("DocType")%></td>
                                        <td><%# Eval("DocNo")%></td>
                                       
                                        
                                        <td>
                                            <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                Text="" OnCommand="GridDeleteClick" CommandArgument='Delete' CommandName='<%# Eval("DocNo")%>' 
                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this DocNo details?');">
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater>
					    </div>
					</div>
					<!-- table End -->
                                        
                                        <!-- begin row -->
                                        <div class="row">
                                            <label class="control-label col-md-5 col-sm-5"></label>
                                            <div class="col-md-3 col-sm-3">
                                               
                                                <asp:Button runat="server" id="btnEmpSave" Text="Save" ValidationGroup="Validate_Field" class="btn btn-success" 
                                                    onclick="btnEmpSave_Click"/>
									           <asp:Button runat="server" id="btnEmpClear" Text="Clear" class="btn btn-danger" 
                                                    onclick="btnEmpClear_Click" />
                                            </div>
                                            <div class="col-md-3 col-sm-3">

                                            </div>
                                        </div>
                                        <!-- end row -->
                                        
                                    </fieldset>
                        </ContentTemplate>
                        <Triggers>
                        <asp:PostBackTrigger ControlID="btnDocAdd" />
                        </Triggers>
                        </asp:UpdatePanel>
						</div>
					</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</asp:Content>

