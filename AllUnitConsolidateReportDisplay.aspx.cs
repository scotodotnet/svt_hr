﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.IO;
using System.Data.SqlClient;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.Collections.Generic;
using System.IO;
public partial class AllUnitConsolidateReportDisplay : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string SessionUnit;
    string FromDate;

    string SSQL;

    DataTable mDataSet = new DataTable();
    DataTable dt = new DataTable();
    DataTable presentDT = new DataTable();
    DataTable improperDT = new DataTable();
    DataTable MancostDT = new DataTable();
    BALDataAccess objdata = new BALDataAccess();
    DataTable AutoDataTable = new DataTable();
    DataSet ds = new DataSet();
    string Division;
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | All Unit Consolidated Report";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("SalaryProcess"));
                //li.Attributes.Add("class", "droplink active open");
            }


            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
          //  SessionUnit = Request.QueryString["unit"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            Report();
        }
    }
    public void Report()
    {
        AutoDataTable.Columns.Add("CompanyName");
        AutoDataTable.Columns.Add("LocationName");
        AutoDataTable.Columns.Add("UnitII");
        
        AutoDataTable.Columns.Add("SNo");
        AutoDataTable.Columns.Add("Dept");
        AutoDataTable.Columns.Add("UnitI");
        AutoDataTable.Columns.Add("BasicSalary");
       


     SSQL=" Select LocCode,Sum(Emp_Count) as Emp_Count from  Employee_Allotment_Mst_New ";
     SSQL = SSQL + " group by LocCode";

        mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);
        if (mDataSet.Rows.Count != 0)
        {
            for (int i = 0; i < mDataSet.Rows.Count; i++)
            {
                //Present Count
                SSQL = "Select Count(Present) as Present from LogTime_Days ";
                SSQL = SSQL + " where  LocCode='" + mDataSet.Rows[i]["LocCode"].ToString() + "' and CompCode='" + SessionCcode + "' and Present !='0.0' ";

                if (FromDate != "")
                {
                    SSQL = SSQL + " and Attn_Date_Str='" + FromDate + "' ";
                }
                presentDT = objdata.RptEmployeeMultipleDetails(SSQL);

                //Improper Count
                SSQL = "select Count(MachineID) as Improper";
                SSQL = SSQL + " from LogTime_Days   ";
                SSQL = SSQL + " where CompCode='" + SessionCcode.ToString() + "' ANd LocCode='" + mDataSet.Rows[i]["LocCode"].ToString() + "'";

                if (FromDate != "")
                {
                    SSQL = SSQL + " and Attn_Date_Str='" + FromDate + "' ";
                }              
                SSQL = SSQL + " And TypeName='Improper'";
                 improperDT = objdata.RptEmployeeMultipleDetails(SSQL);


                 SSQL = "Select ManDayCost from ManDayCost  where CompCode ='" + SessionCcode.ToString() + "' ANd LocCode='" + mDataSet.Rows[i]["LocCode"].ToString() + "'";
                 MancostDT = objdata.RptEmployeeMultipleDetails(SSQL);


                SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "' ";
                dt = objdata.RptEmployeeMultipleDetails(SSQL);
                string name = dt.Rows[0]["CompName"].ToString();

                AutoDataTable.NewRow();
                AutoDataTable.Rows.Add();
                AutoDataTable.Rows[i]["CompanyName"] = name;
                AutoDataTable.Rows[i]["LocationName"] = mDataSet.Rows[i]["LocCode"].ToString();
                AutoDataTable.Rows[i]["UnitII"] = mDataSet.Rows[i]["Emp_Count"].ToString();
                AutoDataTable.Rows[i]["SNo"] = i + 1;
                AutoDataTable.Rows[i]["Dept"] = presentDT.Rows[0]["Present"].ToString();
                AutoDataTable.Rows[i]["UnitI"] = improperDT.Rows[0]["Improper"].ToString();
                AutoDataTable.Rows[i]["BasicSalary"] = MancostDT.Rows[0]["ManDayCost"].ToString();
                
            }
            ds.Tables.Add(AutoDataTable);
            ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("crystal/UnitwiseConsolidate.rpt"));
            
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;
        }
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        CrystalReportViewer1.Dispose();
    }
}
