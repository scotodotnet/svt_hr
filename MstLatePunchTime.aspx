﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="MstLatePunchTime.aspx.cs" Inherits="MstLatePunchTime" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">



<script src="assets/js/master_list_jquery.min.js"></script>
<script src="assets/js/master_list_jquery-ui.min.js"></script>
<link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css"/>
 <script>
     $(document).ready(function() {
     $('#example').dataTable();
     $('.datepicker').datepicker({
         format: "dd/mm/yyyy",
         autoclose: true
     });
     });
 </script>


<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
                $('.select2').select2();
                $('.datepicker').datepicker({
                    format: "dd/mm/yyyy",
                    autoclose: true
                });
            }
        });
    };
</script>
<asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                <ContentTemplate>
<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Manual</a></li>
				<li class="active">Late Punch Time</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Late Punch Time</h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">Weekly OT Hours</h4>
                        </div>
                        <div class="panel-body">
                        <div class="form-group">
                        <!-- begin row -->
                          <div class="row">
                            <!-- begin col-4 -->
                              <div class="col-md-2">
								<div class="form-group">
								 <label>Shift</label>
								<asp:DropDownList ID="ddlShift" class="form-control" AutoPostBack="true" OnTextChanged="ddlShift_TextChanged" runat="server"></asp:DropDownList>
								</div>
                               </div>
                           <!-- end col-4 -->
                            <!-- begin col-4 -->
                            <div class="col-md-2">
							   <div class="form-group">
								<label>Min Hour</label>
								<asp:TextBox runat="server" ID="txtMinHr" class="form-control" 
                                        ontextchanged="txttime1_TextChanged1"></asp:TextBox>
								</div>
                             </div>
                           <!-- end col-4 -->
                           <!-- begin col-4 -->
                           <div class="col-md-2">
                            <div class="form-group">
                           <label>Min Minutes</label>
                          <asp:TextBox runat="server" ID="txtMinMnts" class="form-control" 
                                        ontextchanged="txtTime2_TextChanged"></asp:TextBox>
                          </div>
                         </div>
                           <!-- end col-4 -->
                               <!-- begin col-4 -->
                           <div class="col-md-2">
                            <div class="form-group">
                           <label>Max Hour</label>
                           <asp:TextBox runat="server" ID="txtMaxHr" class="form-control" 
                                        ontextchanged="txtTime3_TextChanged1"></asp:TextBox>
                          </div>
                         </div>
                          <!-- end row --> 
                               <!-- begin col-4 -->
                           <div class="col-md-2">
                            <div class="form-group">
                           <label>Max Minutes</label>
                           <asp:TextBox runat="server" ID="txtMaxMnts" class="form-control" 
                                        ontextchanged="txtTime3_TextChanged1"></asp:TextBox>
                          </div>
                         </div>
                          <!-- end row --> 
                    <!-- table start -->
					<div class="col-md-12">
					    <div class="row">
					        <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
			                    <HeaderTemplate>
                                    <table id="example" class="display table">
                                        <thead>
                                            <tr>
                                                
                                                <%--<th>Machine ID</th>--%>
                                                <th>Shift</th>
                                                <th>Min Hour</th>
                                                <th>Min Minute</th>
                                                <th>Max Hour</th>
                                                <th>Max Minute</th>
                                                <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <%--<td><%# Eval("MachineID")%></td>--%>
                                        <td><%# Eval("Shift")%></td>
                                        <td><%# Eval("MinHr")%></td>
                                        <td><%# Eval("MinMnts")%></td>
                                        <td><%# Eval("MaxHr")%></td>
                                       <td><%# Eval("MaxMnts")%></td>
                                        <td>
                                        <asp:LinkButton ID="btnEditIssueEntry_Grid" class="btn btn-primary btn-sm fa fa-pencil"  runat="server" 
                                                        Text="" OnCommand="btnEditIssueEntry_Grid_Command" CommandName="Edit"  CommandArgument='<%# Eval("Shift")%>' >
                                        </asp:LinkButton>
                                       
                                        <asp:LinkButton ID="btnDeleteEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                          Text=""  CommandArgument='<%# Eval("Shift")%>'
                                         CausesValidation="true" OnCommand="btnDeleteEnquiry_Grid_Command" CommandName="Delete" OnClientClick="return confirm('Are you sure you want to delete this OT details?');">
                                        </asp:LinkButton>
                                        </td>
                                       
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater>
					    </div>
					</div>
					<!-- table End -->
					
                       <!-- begin row -->  
                        <div class="row">
                         <div class="col-md-4"></div>
                           <!-- begin col-4 -->
                                <div class="col-md-4">
								 <div class="form-group">
									<br />
									
									<asp:Button runat="server" id="btnSave" Text="Save" class="btn btn-success" 
                                         onclick="btnSave_Click"/>
									<asp:Button runat="server" id="btnClear" Text="Clear" class="btn btn-danger" 
                                         onclick="btnClear_Click" />
								 </div>
                               </div>
                              <!-- end col-4 -->
                              <div class="col-md-4"></div>
                         </div>
                        <!-- end row -->   
                        </div>
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
<!-- end #content -->
</ContentTemplate>
</asp:UpdatePanel>

</asp:Content>

