﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

using Payroll;
using Payroll.Data;
using Payroll.Configuration;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

public partial class RptSettlement : System.Web.UI.Page
{
    private int[] monthDay = new int[12] { 31, -1, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
    private DateTime fromDate;
    private DateTime toDate;
    private int year;
    private int month;
    private int day;
    static string CmpName;
    static string Cmpaddress;
    string SessionPayroll;

    BALDataAccess objdata = new BALDataAccess();
    DateTime TransDate;
    string SessionAdmin;
    string Stafflabour;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string SessionUserName;
    string SessionUserID;
    string SessionRights;
    string TokenNo;
    string Year;
    string ReportName;

    protected void Page_Load(object sender, EventArgs e)
    {
        
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        TokenNo = Request.QueryString["TokenNo"].ToString();
        Year = Request.QueryString["Year"].ToString();
        ReportName = Request.QueryString["ReportName"].ToString();

        Load_DB();
        if (ReportName == "SETTLEMENT REPORT")
        {
            Settlement();
        }
        else if (ReportName == "BONUS REPORT")
        {
            Leavewage();
            Bonus();
        }
        else if (ReportName == "LEAVE WAGES")
        {
            Leavewage();
        }
        

      
        

       
    }
    public void Load_DB()
    {
        //Get Database Name
        string query = "";
        DataTable dt_DB = new DataTable();
        query = "select CMS,Rights,Sales,Stores,Weaving,Maintaince,Production,Payroll from [SVT_Rights]..MstDBname";
        dt_DB = objdata.RptEmployeeMultipleDetails(query);
        if (dt_DB.Rows.Count > 0)
        {
            SessionPayroll = dt_DB.Rows[0]["Payroll"].ToString();
            //SessionRights = dt_DB.Rows[0]["Rights"].ToString();
        }
    }
    public void Settlement()
    {
        DataTable Shift_DS = new DataTable();
        DataTable Emp_Ds = new DataTable();
        DataSet ds = new DataSet();
        string SSQL = "";
        SSQL = "select * from Settlement where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " and TokenNo='" + TokenNo + "' and Year='" + Year + "'";
        Emp_Ds = objdata.RptEmployeeMultipleDetails(SSQL);

        ds.Tables.Add(Emp_Ds);
        ReportDocument report = new ReportDocument();
        report.Load(Server.MapPath("crystal/Settlement.rpt"));
        
        report.Database.Tables[0].SetDataSource(ds.Tables[0]);
        report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
        CrystalReportViewer1.ReportSource = report;

    }
    public void Bonus()
    {
        DataTable ds_Bonus = new DataTable();
        string SSQL = "";
        string bonus_Period_From = "";
        string bonus_Period_To = "";
        DataSet ds = new DataSet();
        

        bonus_Period_From = "01-10-" + (Convert.ToDecimal(Year) - Convert.ToDecimal(1)).ToString();
        bonus_Period_To = "01-09-" + Year.ToString();

        SSQL = "Select (left(DATENAME(MM, SD.FromDate),3) + '-' + right(Year(SD.FromDate),2)) as Months,(AD.Days + AD.NFh) as TotalDays , ";
       
        SSQL = SSQL + " SD.NetPay ";
        SSQL = SSQL + " from  [" + SessionPayroll + "].. EmployeeDetails  ED inner join [" + SessionPayroll + "].. SalaryDetails SD on ED.EmpNo=SD.EmpNo And SD.Lcode=ED.Lcode";
        SSQL = SSQL + " inner join [" + SessionPayroll + "].. AttenanceDetails  AD on ED.EmpNo=AD.EmpNo And AD.Lcode=ED.Lcode and SD.Lcode=AD.Lcode And AD.FromDate=SD.FromDate";
        SSQL = SSQL + " inner join [" + SessionPayroll + "].. Officialprofile  OP on ED.EmpNo=OP.EmpNo";
        SSQL = SSQL + " where ED.Ccode='" + SessionCcode + "' And ED.Lcode='" + SessionLcode + "' and SD.ExisistingCode='" + TokenNo + "' ";
        SSQL = SSQL + " And SD.Ccode='" + SessionCcode + "' And SD.Lcode='" + SessionLcode + "' And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " And SD.FromDate >=convert(datetime,'" + bonus_Period_From + "', 105) And SD.FromDate <=convert(datetime,'" + bonus_Period_To + "', 105)";
        SSQL = SSQL + " And AD.FromDate >=convert(datetime,'" + bonus_Period_From + "', 105) And AD.FromDate <=convert(datetime,'" + bonus_Period_To + "', 105)";
        ds_Bonus = objdata.RptEmployeeMultipleDetails(SSQL);

        DataTable dt = new DataTable();
        SSQL = "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count > 0)
        {
            CmpName = dt.Rows[0]["Cname"].ToString();
            Cmpaddress = (dt.Rows[0]["Address1"].ToString() + ", " + dt.Rows[0]["Address2"].ToString() + ", " + dt.Rows[0]["Location"].ToString() + "-" + dt.Rows[0]["Pincode"].ToString());
        }


        if (ds_Bonus.Rows.Count != 0)
        {
            ds.Tables.Add(ds_Bonus);
            ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("crystal/Bonus.rpt"));
            
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            report.DataDefinition.FormulaFields["Lcode"].Text = "'" + SessionLcode + "'";
            report.DataDefinition.FormulaFields["CmpName"].Text = "'" + CmpName + "'";
            report.DataDefinition.FormulaFields["Cmpaddress"].Text = "'" + Cmpaddress + "'";
            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;
        }

    }
    public void Leavewage()
    {
        string SSQL = "";
        DataTable da_Leave = new DataTable();
        DataTable AutoTable = new DataTable();
        AutoTable.Columns.Add("Month");
        AutoTable.Columns.Add("Days");

        SSQL = "SELECT ED.EmpNo,ED.EmpName,ED.ExisistingCode,MD.DepartmentNm,ED.Designation, (SELECT Sum(AD.Days)  FROM AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo  ";
        SSQL = SSQL + " And month(AD.FromDate)='1' and AD.Months='January' And Year(AD.FromDate)='" + Year + "'";
        SSQL = SSQL + " And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "' and AD.ExistingCode='" + TokenNo + "') AS Jan,";
        SSQL = SSQL + " (SELECT Sum(AD.Days)  FROM AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And month(AD.FromDate)='2' ";
        SSQL = SSQL + " and AD.Months='February' And Year(AD.FromDate)='" + Year + "' And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "' and AD.ExistingCode='" + TokenNo + "') AS Feb,";
        SSQL = SSQL + " (SELECT Sum(AD.Days)  FROM AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And month(AD.FromDate)='3' ";
        SSQL = SSQL + " and AD.Months='March' And Year(AD.FromDate)='" + Year + "' And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "' and AD.ExistingCode='" + TokenNo + "') AS Mar, ";
        SSQL = SSQL + " (SELECT Sum(AD.Days)  FROM AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And month(AD.FromDate)='4' and AD.Months='April'";
        SSQL = SSQL + " And Year(AD.FromDate)='" + Year + "' And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "' and AD.ExistingCode='" + TokenNo + "') AS Apr,  ";
        SSQL = SSQL + " (SELECT Sum(AD.Days) FROM AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And month(AD.FromDate)='5' and AD.Months='May'  ";
        SSQL = SSQL + " And Year(AD.FromDate)='" + Year + "' And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "' and AD.ExistingCode='" + TokenNo + "') AS May, (SELECT Sum(AD.Days) ";
        SSQL = SSQL + " FROM AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And month(AD.FromDate)='6' and AD.Months='June' ";
        SSQL = SSQL + " And Year(AD.FromDate)='" + Year + "' And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "' and AD.ExistingCode='" + TokenNo + "') AS Jun, (SELECT Sum(AD.Days)";
        SSQL = SSQL + "  FROM AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And month(AD.FromDate)='7' and AD.Months='July'";
        SSQL = SSQL + " And Year(AD.FromDate)='" + Year + "' And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "' and AD.ExistingCode='" + TokenNo + "') AS Jul, (SELECT Sum(AD.Days)";
        SSQL = SSQL + " FROM AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And month(AD.FromDate)='8' and AD.Months='August'";
        SSQL = SSQL + " And Year(AD.FromDate)='" + Year + "' And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "' and AD.ExistingCode='" + TokenNo + "') AS Aug, (SELECT Sum(AD.Days) ";
        SSQL = SSQL + " FROM AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And month(AD.FromDate)='9' and AD.Months='September'";
        SSQL = SSQL + " And Year(AD.FromDate)='" + Year + "' And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "' and AD.ExistingCode='" + TokenNo + "') AS Sep, (SELECT Sum(AD.Days) ";
        SSQL = SSQL + " FROM AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And month(AD.FromDate)='10' and AD.Months='October'";
        SSQL = SSQL + " And Year(AD.FromDate)='" + Year + "' And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "' and AD.ExistingCode='" + TokenNo + "') AS Oct, (SELECT Sum(AD.Days)";
        SSQL = SSQL + " FROM AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And month(AD.FromDate)='11' and AD.Months='November'";
        SSQL = SSQL + " And Year(AD.FromDate)='" + Year + "' And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "' and AD.ExistingCode='" + TokenNo + "') AS Nov, (SELECT Sum(AD.Days)";
        SSQL = SSQL + " FROM AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And month(AD.FromDate)='12' and AD.Months='December'";
        SSQL = SSQL + " And Year(AD.FromDate)='" + Year + "' And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "' and AD.ExistingCode='" + TokenNo + "') AS Dec ";
        SSQL = SSQL + " FROM	EmployeeDetails ED inner join MstDepartment as MD on MD.DepartmentCd = ED.Department inner join AttenanceDetails as AD ";
        SSQL = SSQL + " on AD.EmpNo=ED.EmpNo inner join SalaryMaster as SM on SM.EmpNo=AD.EmpNo inner Join OfficialProfile OP on OP.EmpNo=AD.EmpNo";
        SSQL = SSQL + " WHERE (ED.Ccode='" + SessionCcode + "' And ED.Lcode='" + SessionLcode + "' And Year(AD.FromDate)='" + Year + "' And (SELECT Sum(AD.Days + AD.WH_Work_Days)  FROM";
        SSQL = SSQL + "  AttenanceDetails AD WHERE AD.EmpNo=ED.EmpNo And Year(AD.FromDate)='" + Year + "' And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "' and AD.ExistingCode='" + TokenNo + "') > = 0 ";
        SSQL = SSQL + " And ED.EmployeeType='1') Group by  ED.EmpNo,ED.EmpName,ED.ExisistingCode,MD.DepartmentNm,ED.Designation,OP.Dateofjoining,SM.Base";
        SSQL = SSQL + " Order by ED.ExisistingCode Asc ";
        da_Leave = objdata.RptEmployeeMultipleDetails(SSQL);
        if (da_Leave.Rows.Count != 0)
        {
            for (int k = 0; k <= da_Leave.Rows.Count; k++)
            {
                int j=6;
                string Heading ="";
                AutoTable.NewRow();
                AutoTable.Rows.Add();
                AutoTable.Rows[k][0] = da_Leave.Columns[k].ToString();
                Heading=da_Leave.Columns[k].ToString();
                if (da_Leave.Rows[k][j] == "")
                {
                    AutoTable.Rows[k][1] = da_Leave.Rows[k][0].ToString();
                }
                j++;

            }
        }



    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        CrystalReportViewer1.Dispose();
    }
  
}
