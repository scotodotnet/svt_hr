﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.Collections.Generic;
using System.IO;

public partial class RptUnpaisVoucher_Display : System.Web.UI.Page
{BALDataAccess objdata = new BALDataAccess();
    DateTime TransDate;
    string SessionAdmin;
    string Stafflabour;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string SessionUserName;
    string SessionUserID;
    string SessionRights;
    string TokenNO;
    DataTable AutoDtable = new DataTable();
    DataTable dt_Employe = new DataTable();
    string dor = "";
    string tday = "";
    DateTime MyDate1;
    DateTime MyDate2;
    string SSQL = "";
    string MachineIDEncrypt;
    DataTable dt_Salary = new DataTable();
    DataTable dt_LogSalary = new DataTable();
    DataTable DataCells = new DataTable();
    DataSet ds = new DataSet();
    ReportDocument report = new ReportDocument();
    DataTable dt = new DataTable(); string MonthStr = ""; string FinYrStr = "";
    string SessionPayroll;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        TokenNO = Request.QueryString["TokenNO"].ToString();
        MonthStr = Request.QueryString["MonthStr"].ToString();
        FinYrStr = Request.QueryString["FinYrStr"].ToString();
        Load_DB();
        if (!IsPostBack)
        {
            Report();
        }
    }
    public void Load_DB()
    {
        //Get Database Name
        string query = "";
        DataTable dt_DB = new DataTable();
        query = "select CMS,Rights,Sales,Stores,Weaving,Maintaince,Production,Payroll from [SVT_Rights]..MstDBname";
        dt_DB = objdata.RptEmployeeMultipleDetails(query);
        if (dt_DB.Rows.Count > 0)
        {
            SessionPayroll = dt_DB.Rows[0]["Payroll"].ToString();
            //SessionRights = dt_DB.Rows[0]["Rights"].ToString();
        }
    }

    public void Report()
    {
        DataCells.Columns.Add("CompanyName");
        DataCells.Columns.Add("LocationName");
        DataCells.Columns.Add("ExistingCode");
        DataCells.Columns.Add("EmpNo");
        DataCells.Columns.Add("Name");
        DataCells.Columns.Add("Deptment");
        DataCells.Columns.Add("Month");
        DataCells.Columns.Add("FinancialYear");
        DataCells.Columns.Add("NetAmount");
        DataCells.Columns.Add("AmountInWords");
        string MonthStr_IP = MonthStr;

        string FromDate = "";
        string ToDate1 = "";
        string Month = "";
        int Year;
        Month = "";
        Year = Convert.ToInt32(FinYrStr);//Convert.ToInt32(dt_Salary.Rows[i]["Financialyear"].ToString());
        if (MonthStr == "January")//dt_Salary.Rows[i]["Month"].ToString() == "January")
        {
            Month = "02";
        }
        if (MonthStr == "February")//dt_Salary.Rows[i]["Month"].ToString() == "February")
        {
           Month = "03";
        }
        if (MonthStr == "March")//dt_Salary.Rows[i]["Month"].ToString() == "March")
        {
            Month = "04";
        }
        if (MonthStr == "April")//dt_Salary.Rows[i]["Month"].ToString() == "April")
        {
            Month = "05";
        }
        if (MonthStr == "May")//dt_Salary.Rows[i]["Month"].ToString() == "May")
        {
            Month = "06";
        }
        if (MonthStr == "June")//dt_Salary.Rows[i]["Month"].ToString() == "June")
        {
            Month = "07";
        }
        if (MonthStr == "July")//dt_Salary.Rows[i]["Month"].ToString() == "July")
        {
            Month = "08";
        }
        if (MonthStr == "August")//dt_Salary.Rows[i]["Month"].ToString() == "August")
        {
            Month = "09";
        }
        if (MonthStr == "September")//dt_Salary.Rows[i]["Month"].ToString() == "September")
        {
            Month = "10";
        }
        if (MonthStr == "October")//dt_Salary.Rows[i]["Month"].ToString() == "October")
        {
            Month = "11";
        }
        if (MonthStr == "November")//dt_Salary.Rows[i]["Month"].ToString() == "November")
        {
            Month = "12";
        }
        if (MonthStr == "December")//dt_Salary.Rows[i]["Month"].ToString() == "December")
        {
            Month = "01";
            Year = Year + 1;
        }
       
        SSQL = "Select * from Employee_Mst where ExistingCode='" + TokenNO + "'  and CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        dt_Employe = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt_Employe.Rows.Count != 0)
        {
            dor = dt_Employe.Rows[0]["DOR"].ToString();
        }
        //tday = DateTime.Now.ToString("dd/MM/yyyy");
        //MyDate1 = DateTime.ParseExact(dor, "dd/MM/yyyy", null);
        //MyDate2 = DateTime.ParseExact(tday, "dd/MM/yyyy", null);
        MachineIDEncrypt = UTF8Encryption(dt_Employe.Rows[0]["MachineID"].ToString());

        SSQL = "Select * from [" + SessionPayroll + "]..SalaryDetails Where CCode='" + SessionCcode + "' And LCode='" + SessionLcode + "' and ExisistingCode='" + TokenNO + "'";
        SSQL = SSQL + " And Month='" + MonthStr + "' And FinancialYear='" + Year.ToString() + "'";
        //SSQL = SSQL + " And FromDate >= convert(datetime,'" + FromDate + "', 105) and ToDate <= Convert(Datetime,'" + ToDate1 + "', 105)";
        dt_Salary = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt_Salary.Rows.Count != 0)
        {
            for (int i = 0; i < dt_Salary.Rows.Count; i++)
            {
                

                string fmdate = "01";
                string tdate = "12";

                string fromDate = Year.ToString() + "-" + Month + "-" + fmdate;
                string ToDate = Year.ToString() + "-" + Month + "-" + tdate;
                DateTime date1 = Convert.ToDateTime(fromDate);
                DateTime date2 = Convert.ToDateTime(ToDate);

                SSQL = "Select * from LogTime_Salary where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and MachineID='" + MachineIDEncrypt + "' ";
                SSQL = SSQL + "And Payout >='" + date1.ToString("yyyy/MM/dd") + " " + "00:00" + "'";
                SSQL = SSQL + "And payout <='" + date2.ToString("yyyy/MM/dd") + " " + "23:00" + "'";
                dt_LogSalary = objdata.RptEmployeeMultipleDetails(SSQL);

                if (dt_LogSalary.Rows.Count == 0)
                {
                    bool isUK = false;
                    string NetPay = dt_Salary.Rows[i]["RoundOffNetPay"].ToString();
                    double d1 = Convert.ToDouble(NetPay.ToString());
                    //  int rounded1 = ((int)((d1 + 5) / 10)) * 10;  // Output is 20
                   string RoundOffNetPay = d1.ToString();
                   string Words = NumerictoNumber(Convert.ToInt32(RoundOffNetPay), isUK).ToString() + " " + "Only";
                
                    SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "' ";
                    dt = objdata.RptEmployeeMultipleDetails(SSQL);
                    string name = dt.Rows[0]["CompName"].ToString();

                    DataCells.NewRow();
                    DataCells.Rows.Add();
                    DataCells.Rows[DataCells.Rows.Count - 1]["CompanyName"] = name;
                    DataCells.Rows[DataCells.Rows.Count - 1]["LocationName"] = SessionLcode;
                    DataCells.Rows[DataCells.Rows.Count - 1]["ExistingCode"] = dt_Employe.Rows[0]["ExistingCode"].ToString();
                    DataCells.Rows[DataCells.Rows.Count - 1]["EmpNo"] = dt_Employe.Rows[0]["EmpNo"].ToString();
                    DataCells.Rows[DataCells.Rows.Count - 1]["Name"] = dt_Employe.Rows[0]["FirstName"].ToString();
                    DataCells.Rows[DataCells.Rows.Count - 1]["Deptment"] = dt_Employe.Rows[0]["DeptName"].ToString();
                    DataCells.Rows[DataCells.Rows.Count - 1]["Month"] = dt_Salary.Rows[i]["Month"].ToString();
                    DataCells.Rows[DataCells.Rows.Count - 1]["FinancialYear"] = dt_Salary.Rows[i]["Year"].ToString();
                    DataCells.Rows[DataCells.Rows.Count - 1]["NetAmount"] = dt_Salary.Rows[i]["RoundOffNetPay"].ToString();
                    DataCells.Rows[DataCells.Rows.Count - 1]["AmountInWords"] = Words.ToString();

                }
            }
            ds.Tables.Add(DataCells);
            //ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("crystal/UnPaidVoucher_New.rpt"));
            
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Records Found');", true);
        }
    }

    public string NumerictoNumber(int Number, bool isUK)
    {
        if (Number == 0)
            return "Zero";

        string and = isUK ? "and " : "";
        if (Number == -2147483648)
            return "Minus Two Billion One Hundred " + and +
        "Forty Seven Million Four Hundred " + and + "Eighty Three Thousand " +
        "Six Hundred " + and + "Forty Eight";

        int[] num = new int[4];
        int first = 0;
        int u, h, t = 0;
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        if (Number < 0)
        {
            sb.Append("Minus");
            Number = -Number;
        }

        string[] words0 = { "", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine" };
        string[] words1 = { "Ten ", "Eleven ", "Twelve ", "Thirteen ", "Fourteen ", "Fifteen ", "Sixteen ", "Seventeen ", "Eighteen ", "Nineteen " };
        string[] words2 = { "Twenty ", "Thirty ", "Forty ", "Fifty ", "Sixty ", "Seventy ", "Eighty ", "Ninety " };
        string[] words3 = { "Thousand ", "Lak", "Crore", "Million ", "Billion " };
        num[0] = Number % 1000;
        num[1] = Number / 1000;
        num[2] = Number / 1000000;
        num[1] = num[1] - 1000 * num[2];  // thousands
        num[2] = num[2] - 100 * num[3];//laks
        num[3] = Number / 10000000;     // billions
        num[2] = num[2] - 1000 * num[3];  // millions

        for (int i = 3; i > 0; i--)
        {
            if (num[i] != 0)
            {
                first = i;
                break;
            }
        }
        for (int i = first; i >= 0; i--)
        {
            if (num[i] == 0) continue;
            u = num[i] % 10;
            t = num[i] / 10;
            h = num[i] / 100;
            t = t - 10 * h;

            if (h > 0)
                sb.Append(words0[h] + " Hundred ");
            if (u > 0 || t > 0)
            {
                if (h > 0 || i < first)
                    sb.Append(and);

                if (t == 0)
                    sb.Append(words0[u]);
                else if (t == 1)
                    sb.Append(words1[u]);
                else
                    sb.Append(words2[t - 2] + words0[u]);

            }

            if (i != 0)
                sb.Append(words3[i - 1]);

        }

        return sb.ToString().TrimEnd();



    }

    public static string UTF8Encryption_OLD(string password)
    {
        string strmsg = string.Empty;
        byte[] encode = new byte[password.Length];
        encode = Encoding.UTF8.GetBytes(password);
        strmsg = Convert.ToBase64String(encode);
        return strmsg;
    }

    public string UTF8Encryption(string mvarPlanText)
    {
        string cipherText = "";
        try
        {
            string passPhrase = "Altius";
            string saltValue = "info@altius.co.in";
            string hashAlgorithm = "SHA1";
            string initVector = "@1B2c3D4e5F6g7H8";
            int passwordIterations = 2;
            int keySize = 256;
            byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
            byte[] saltValueBytes = Encoding.ASCII.GetBytes(saltValue);
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(mvarPlanText);
            PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, saltValueBytes, hashAlgorithm, passwordIterations);
            byte[] keyBytes = password.GetBytes(keySize / 8);
            RijndaelManaged symmetricKey = new RijndaelManaged();
            symmetricKey.Mode = CipherMode.CBC;
            ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes);
            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
            cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
            cryptoStream.FlushFinalBlock();
            byte[] cipherTextBytes = memoryStream.ToArray();
            memoryStream.Close();
            cryptoStream.Close();
            cipherText = Convert.ToBase64String(cipherTextBytes);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }

        return cipherText;
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        CrystalReportViewer1.Dispose();
    }

}
