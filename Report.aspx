﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="Report.aspx.cs" Inherits="Report" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="assets/js/master_list_jquery.min.js"></script>
    <script src="assets/js/master_list_jquery-ui.min.js"></script>
    <style type="text/css">
        .CalendarCSS {
            background-color: White;
            color: Black;
            border: 1px solid #646464;
            font-size: xx-large;
            font-weight: bold;
            margin-bottom: 40px;
            margin-top: 20px;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.datepicker').datepicker({
                format: "dd/mm/yyyy",
                autoclose: true
            });
            $("ul[id*=myid] li").click(function () {
                alert($(this).html()); // gets innerHTML of clicked li
                alert($(this).text()); // gets text contents of clicked li

            });
        });
    </script>

    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('.datepicker').datepicker({
                        format: "dd/mm/yyyy",
                        autoclose: true
                    });

                    $('.select2').select2();
                }
            });
        };
    </script>
    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
        <ContentTemplate>
            <div id="content" class="content">
                <div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                        <%--<h4><li class="active">Report</li></h4> --%>
                    </ol>
                    <h1 class="page-header">Report</h1>
                </div>


                <div id="main-wrapper" class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <div class="col-sm-5">
                                        <div class="panel panel-white">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading clearfix">
                                                    <h3 class="panel-title">Report Type</h3>
                                                </div>
                                            </div>
                                            <div class="panel-body">

                                                <div>
                                                    <div class="form-group col-md-6">
                                                        <label for="exampleInputName">REPORT NAME</label>
                                                        <asp:DropDownList ID="ddlRptName" runat="server"
                                                            class="form-control col-md-6 select2" AutoPostBack="true"
                                                            OnSelectedIndexChanged="ddlRptName_SelectedIndexChanged">
                                                            <asp:ListItem Value="1" Text="- select -"></asp:ListItem>
                                                            <asp:ListItem Value="2" Text="ALL"></asp:ListItem>
                                                            <asp:ListItem Value="3" Text="SPECIFIC"></asp:ListItem>
                                                            <asp:ListItem Value="4" Text="ABSTRACT"></asp:ListItem>
                                                            <asp:ListItem Value="5" Text="OTHERS"></asp:ListItem>
                                                        </asp:DropDownList>


                                                    </div>
                                                    <!-- <asp:ListItem Value="6" Text="EMPLOYEE WISE DAY ATTENDANCE - BETWEEN DATES"></asp:ListItem>
                                                            <asp:ListItem Value="7" Text="DAY ATTENDANCE SUMMARY"></asp:ListItem>
                                                            <asp:ListItem Value="8" Text="EMPLOYEE MASTER"></asp:ListItem>
                                                            <asp:ListItem Value="9" Text="EMPLOYEE FULL PROFILE"></asp:ListItem>-->


                                                    <div>

                                                        <asp:ListBox ID="ListRptName" runat="server" Width="360px" Height="409px"
                                                            Font-Bold="True" Font-Names="Times New Roman" Font-Size="Medium"
                                                            OnSelectedIndexChanged="ListRptName_SelectedIndexChanged" AutoPostBack="true">
                                                            <asp:ListItem></asp:ListItem>
                                                        </asp:ListBox>

                                                    </div>

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <!-- col-sm-6 -->

                                    <div class="col-sm-5">
                                        <div class="panel panel-white">
                                            <div class="panel panel-primary">
                                                <div class="panel-heading clearfix">
                                                    <h3 class="panel-title">Report</h3>
                                                </div>
                                            </div>
                                            <div class="panel-body">
                                                <div>
                                                    <asp:Label ID="RptLabel" runat="server" Font-Bold="True" Height="100%"
                                                        Width="100%" align="center">Option</asp:Label>
                                                </div>
                                                <div class="form-group row">
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">

                                                        <div class="form-group col-md-12">
                                                            <label for="exampleInputName">Department</label>
                                                            <asp:DropDownList ID="ddlDepartment" runat="server" class="form-control select2">
                                                            </asp:DropDownList>
                                                            <asp:DropDownList ID="ddlDivision" runat="server" Visible="false"
                                                                class="form-control select2">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <label for="exampleInputName">Wages Type</label>
                                                            <asp:DropDownList ID="ddlWagesType" runat="server" class="form-control select2">
                                                                <%--<asp:ListItem>STAFF</asp:ListItem>
                                                             <asp:ListItem>LABOUR</asp:ListItem>--%>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <!-- begin col-4 -->
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>Token No</label>
                                                                <asp:TextBox runat="server" ID="txtTokenNo" class="form-control"
                                                                    AutoPostBack="true" OnTextChanged="txtTokenNo_TextChanged"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <!-- end col-4 -->
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <label for="exampleInputName">Employee Name</label>
                                                            <asp:DropDownList ID="ddlEmpName" runat="server"
                                                                class="form-control select2"
                                                                OnSelectedIndexChanged="ddlEmpName_SelectedIndexChanged">
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="exampleInputName">Shift</label>
                                                            <asp:DropDownList ID="ddlShift" runat="server" class="form-control select2">
                                                            </asp:DropDownList>
                                                        </div>

                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <%--<label for="exampleInputName">Vehicle Type</label>--%>
                                                            <asp:DropDownList ID="ddlVehicleType" runat="server" Visible="false" class="form-control select2">
                                                                <asp:ListItem Value="ALL">ALL</asp:ListItem>
                                                                <asp:ListItem Value="Company">Company</asp:ListItem>
                                                                <asp:ListItem Value="OWN">OWN</asp:ListItem>
                                                                <asp:ListItem Value="Private">Private</asp:ListItem>

                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <%-- <label for="exampleInputName">Route No</label>--%>
                                                            <asp:DropDownList ID="ddlRouteNo" runat="server" Visible="false" class="form-control select2">
                                                                <asp:ListItem Value="1" Text="- select -"></asp:ListItem>


                                                            </asp:DropDownList>
                                                        </div>

                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <label for="exampleInputName">Shift Type</label>
                                                            <asp:DropDownList ID="ddlShiftType" runat="server" class="form-control select2">
                                                                <asp:ListItem Value="1" Text="- select -"></asp:ListItem>


                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="exampleInputName">Year</label>
                                                            <asp:DropDownList ID="ddlYear" runat="server" class="form-control select2">
                                                                <asp:ListItem Value="1" Text="- select -"></asp:ListItem>


                                                            </asp:DropDownList>
                                                        </div>


                                                    </div>


                                                    <div class="row">

                                                        <div class="form-group col-md-6">
                                                            <label class="LabelColor">MillType</label><span class="mandatory">*</span>
                                                            <asp:DropDownList ID="txtmiltype" runat="server" class="form-control BorderStyle select2" Style="width: 100%;">
                                                                <asp:ListItem Value="0" Text="-Select-"></asp:ListItem>
                                                                <asp:ListItem Value="1" Text="AB mill"></asp:ListItem>
                                                                <asp:ListItem Value="2" Text="New mill"></asp:ListItem>
                                                                <asp:ListItem Value="3" Text="Others"></asp:ListItem>
                                                            </asp:DropDownList>
                                                            <%--  <asp:RequiredFieldValidator ControlToValidate="ddlShift" InitialValue="0" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                        </asp:RequiredFieldValidator>--%>
                                                        </div>

                                                        <div class="form-group col-md-6">


                                                            <label for="exampleInputName">Leave Days</label>
                                                            <asp:TextBox ID="txtLLeaveDays" class="form-control col-md-6" runat="server"></asp:TextBox>
                                                        </div>
                                                        <%--<div class="form-group col-md-6">
                                                 <asp:CheckBox ID="btnApproved" runat="server" Text="Approved" 
                                                     AutoPostBack="true" />
                      
                   
                                    
                                   <asp:CheckBox ID="btnpending" runat="server" Text="Pending" 
                                             AutoPostBack="true" />  
                                               
                                               </div>--%>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <label for="exampleInputName">From Date</label>
                                                            <%--<asp:TextBox ID="txtFrmdate" class="form-control col-md-6" runat="server"></asp:TextBox>--%>
                                                            <asp:TextBox runat="server" ID="txtFrmdate" class="form-control datepicker col-md-6" placeholder="dd/MM/YYYY"></asp:TextBox>
                                                            <%--<cc1:CalendarExtender ID="CalendarExtender2" runat="server" 
                                                     CssClass="CalendarCSS" Enabled="true" Format="dd/MM/yyyy" 
                                                     TargetControlID="txtFrmdate">
                                                 </cc1:CalendarExtender>--%>
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server"
                                                                FilterMode="ValidChars"
                                                                FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom"
                                                                TargetControlID="txtFrmdate" ValidChars="0123456789/">
                                                            </cc1:FilteredTextBoxExtender>

                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="exampleInputName">To Date</label>
                                                            <%--<asp:TextBox ID="txtTodate" class="form-control col-md-6" runat="server"></asp:TextBox>--%>
                                                            <asp:TextBox runat="server" ID="txtTodate" class="form-control datepicker col-md-6" placeholder="dd/MM/YYYY"></asp:TextBox>
                                                            <%--<cc1:CalendarExtender ID="CalendarExtender1" runat="server" 
                                                     CssClass="CalendarCSS" Enabled="true" Format="dd/MM/yyyy" 
                                                     TargetControlID="txtTodate">
                                                 </cc1:CalendarExtender>--%>
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server"
                                                                FilterMode="ValidChars"
                                                                FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom"
                                                                TargetControlID="txtTodate" ValidChars="0123456789/">
                                                            </cc1:FilteredTextBoxExtender>
                                                        </div>

                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <label for="exampleInputName">PayFrom Date</label>
                                                            <%--<asp:TextBox ID="txtpayfromdate" class="form-control col-md-6" runat="server"></asp:TextBox>--%>
                                                            <asp:TextBox runat="server" ID="txtpayfromdate" Visible="true" class="form-control datepicker col-md-6" placeholder="dd/MM/YYYY"></asp:TextBox>
                                                            <%--<cc1:CalendarExtender ID="CalendarExtender3" runat="server" 
                                                     CssClass="CalendarCSS" Enabled="true" Format="dd/MM/yyyy" 
                                                     TargetControlID="txtpayfromdate">
                                                 </cc1:CalendarExtender>--%>
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server"
                                                                FilterMode="ValidChars"
                                                                FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom"
                                                                TargetControlID="txtpayfromdate" ValidChars="0123456789/">
                                                            </cc1:FilteredTextBoxExtender>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="exampleInputName">PayTo Date</label>
                                                            <%--<asp:TextBox ID="txtpaytodate" class="form-control col-md-6" runat="server"></asp:TextBox>--%>
                                                            <asp:TextBox runat="server" ID="txtpaytodate" Visible="true" class="form-control datepicker col-md-6" placeholder="dd/MM/YYYY"></asp:TextBox>
                                                            <%--<cc1:CalendarExtender ID="CalendarExtender4" runat="server" 
                                                     CssClass="CalendarCSS" Enabled="true" Format="dd/MM/yyyy" 
                                                     TargetControlID="txtpaytodate">
                                                 </cc1:CalendarExtender>--%>
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server"
                                                                FilterMode="ValidChars"
                                                                FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom"
                                                                TargetControlID="txtpaytodate" ValidChars="0123456789/">
                                                            </cc1:FilteredTextBoxExtender>
                                                        </div>

                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <%--<label for="exampleInputName" Visible="false">Below Hours</label>--%>
                                                            <asp:TextBox ID="txtbelowHrs" Visible="false" class="form-control col-md-6" runat="server"></asp:TextBox>
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server"
                                                                FilterMode="ValidChars"
                                                                FilterType="Numbers,Custom"
                                                                TargetControlID="txtbelowHrs" ValidChars="0123456789">
                                                            </cc1:FilteredTextBoxExtender>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <%--<label for="exampleInputName" Visible="false">Above Hours</label>--%>
                                                            <asp:TextBox ID="txtAboveHrs" Visible="false" class="form-control col-md-6" runat="server"></asp:TextBox>
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server"
                                                                FilterMode="ValidChars"
                                                                FilterType="Numbers,Custom"
                                                                TargetControlID="txtAboveHrs" ValidChars="0123456789">
                                                            </cc1:FilteredTextBoxExtender>
                                                        </div>

                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <label for="exampleInputName">Month</label>
                                                            <asp:DropDownList ID="ddlMonth" runat="server" class="form-control select2">
                                                                <asp:ListItem Value="0" Text="-Select-"></asp:ListItem>
                                                                <asp:ListItem Value="January" Text="January"></asp:ListItem>
                                                                <asp:ListItem Value="February" Text="February"></asp:ListItem>
                                                                <asp:ListItem Value="March" Text="March"></asp:ListItem>
                                                                <asp:ListItem Value="April" Text="April"></asp:ListItem>
                                                                <asp:ListItem Value="May" Text="May"></asp:ListItem>
                                                                <asp:ListItem Value="June" Text="June"></asp:ListItem>
                                                                <asp:ListItem Value="July" Text="July"></asp:ListItem>
                                                                <asp:ListItem Value="Auguest" Text="Auguest"></asp:ListItem>
                                                                <asp:ListItem Value="September" Text="September"></asp:ListItem>
                                                                <asp:ListItem Value="October" Text="October"></asp:ListItem>
                                                                <asp:ListItem Value="November" Text="November"></asp:ListItem>
                                                                <asp:ListItem Value="December" Text="December"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <label for="exampleInputName">Fin Year</label>
                                                            <asp:DropDownList ID="ddlFinYear" runat="server" class="form-control select2">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-md-3">
                                                            <asp:CheckBox Visible="false" ID="ChkNonOtherState" runat="server" />
                                                            <%--Non Other State --%>
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <asp:CheckBox ID="ChkOtherState" Visible="false" runat="server" />
                                                            <%-- Other State --%>
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <asp:CheckBox ID="chkapproval" runat="server" Checked="true" Visible="false"
                                                                OnCheckedChanged="chkapproval_CheckedChanged" AutoPostBack="true" />
                                                            <%-- Approval --%>
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <asp:CheckBox ID="chkPending" runat="server" Visible="false"
                                                                OnCheckedChanged="chkPending_CheckedChanged" AutoPostBack="true" />
                                                            <%-- Pending --%>
                                                        </div>
                                                        <div class="form-group col-md-3">
                                                            <%--<label for="exampleInputName">Type of certificate</label>--%>
                                                            <asp:DropDownList ID="ddlTypeofCertificate" Visible="false" runat="server" class="form-control select2">
                                                                <asp:ListItem Value="1" Text="- select -"></asp:ListItem>


                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="txtcenter">
                                                            <div class="form-group row">
                                                                <asp:Button ID="btnExcel" class="btn btn-info" runat="server" Text="Excel"
                                                                    OnClick="btnExcel_Click" />
                                                                <asp:Button ID="btnReport" class="btn btn-success" runat="server" Text="Report"
                                                                    OnClick="btnReport_Click" />
                                                                <asp:Button ID="btnClear" class="btn btn-danger" runat="server" Text="Clear" OnClick="btnClear_Click" />
                                                                <asp:Button ID="btnEmployee" class="btn btn-info" runat="server"
                                                                    Text="Employee" Visible="false" OnClick="btnEmployee_Click" />
                                                            </div>

                                                            <div class="form-group row">

                                                                <asp:Button ID="btnAttendanceDetails" Visible="false" class="btn btn-info" runat="server"
                                                                    Text="Attendance Details" OnClick="btnAttendanceDetails_Click" />
                                                                <asp:Button ID="Mail" class="btn btn-info" runat="server"
                                                                    Text="Mail" OnClick="Mail_Click" Visible="false" />
                                                                <asp:Button ID="deactive" class="btn btn-info" runat="server"
                                                                    Text="Deactive" OnClick="deactive_Click" Visible="false" />
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class="row">
                                                        <div class="form-group col-md-6">
                                                            <%--  <label for="exampleInputName" Visible="false">IsActive</label>--%>
                                                            <asp:DropDownList ID="ddlIsAct" Visible="false" runat="server" class="form-control col-md-6">
                                                                <asp:ListItem Value="1" Text="- select -"></asp:ListItem>
                                                                <asp:ListItem Value="2" Text="Yes"></asp:ListItem>
                                                                <asp:ListItem Value="3" Text="No"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="form-group col-md-6">
                                                            <%-- <label for="exampleInputName" visible="false">Category</label>--%>
                                                            <asp:DropDownList ID="ddlCategory" Visible="false" runat="server" class="form-control col-md-6">
                                                                <asp:ListItem Value="1" Text="- select -"></asp:ListItem>
                                                                <asp:ListItem Value="2" Text="STAFF"></asp:ListItem>
                                                                <asp:ListItem Value="3" Text="LABOUR"></asp:ListItem>

                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="row">

                                                        <div class="form-group col-md-6">
                                                            <%--<label for="exampleInputName" Visible="false">Type Of Certificate</label>--%>
                                                            <asp:TextBox ID="txtTypeOfCertificate" Visible="false" class="form-control col-md-6" runat="server"></asp:TextBox>
                                                        </div>


                                                        <div class="form-group col-md-6">
                                                            <%--  <label for="exampleInputName" Visible="false">Leave Days</label>--%>
                                                            <asp:TextBox ID="txtleavedays" class="form-control col-md-6" Visible="false" runat="server"></asp:TextBox>

                                                        </div>
                                                    </div>
                                                    <div class="row">

                                                        <div class="form-group col-md-6">
                                                            <%--  <label for="exampleInputName" Visible="false"  >OT Eligible</label>--%>
                                                            <asp:RadioButton ID="OTYes" runat="server" Text="Yes" Visible="false" AutoPostBack="true"
                                                                OnCheckedChanged="OTYes_CheckedChanged" />
                                                            <asp:RadioButton ID="OTNo" runat="server" Text="No" Visible="false" AutoPostBack="true"
                                                                OnCheckedChanged="OTNo_CheckedChanged" />
                                                        </div>

                                                        <div class="form-group col-md-6">
                                                            <%--  <label for="exampleInputName" Visible="false" >OT Eligible</label>--%>
                                                            <asp:Label ID="Label1" runat="server" Text="Gender" Visible="false"></asp:Label>
                                                            <asp:RadioButton ID="RdbGendarM" runat="server" Visible="false" Text="Male" AutoPostBack="true"
                                                                OnCheckedChanged="RdbGendarM_CheckedChanged" />
                                                            <asp:RadioButton ID="RdbGendarF" runat="server" Visible="false" Text="Female" AutoPostBack="true"
                                                                OnCheckedChanged="RdbGendarF_CheckedChanged" />
                                                        </div>
                                                    </div>

                                                </div>

                                                <!-- Button start -->
                                                <div class="form-group row">
                                                    <div class="txtcenter">
                                                        <%--<asp:Button ID="btn30days" class="btn btn-info"  runat="server" Text="30 days" />
                    <asp:Button ID="btn60days" class="btn btn-info" runat="server" Text="60 days" />--%>
                                                    </div>
                                                </div>

                                                <!-- Button End -->

                                            </div>

                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>

                        </div>
                    </div>
                </div>

            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

