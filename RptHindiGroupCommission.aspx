﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PayrollMaster.master" AutoEventWireup="true" CodeFile="RptHindiGroupCommission.aspx.cs" Inherits="Default2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
        <ContentTemplate>
            <div id="content" class="content">
                <div class="row">
                    <!-- begin col-12 -->
                    <div class="col-md-12">
                        <!-- begin panel -->
                        <div class="panel panel-inverse">
                            <div class="panel-heading">
                                <div class="panel-heading-btn">
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                                </div>
                                <h4 class="panel-title">Hindi Group Commission Details</h4>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <!-- begin row -->
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Group Name</label>
                                                <asp:DropDownList runat="server" ID="ddl_GroupName" class="form-control select2"
                                                    Style="width: 100%;" AutoPostBack="true" OnSelectedIndexChanged="ddl_GroupName_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>From Date</label>
                                                <asp:TextBox ID="txtFromDate" class="form-control datepicker" runat="server"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>To Date</label>
                                                <asp:TextBox ID="txtToDate" class="form-control datepicker" runat="server"></asp:TextBox>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin row -->
                                        <div class="row">
                                            <div class="col-md-4"></div>
                                            <!-- begin col-4 -->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <br />
                                                    <asp:Button runat="server" ID="btnAttnReport" Text="Report" class="btn btn-success" OnClick="btnAttnReport_Click" />

                                                </div>
                                            </div>
                                            <!-- end col-4 -->
                                            <div class="col-md-4"></div>
                                        </div>
                                        <!-- end row -->





                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnAttnReport" />
        </Triggers>
    </asp:UpdatePanel>

</asp:Content>


