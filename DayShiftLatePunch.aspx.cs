﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Globalization;

public partial class DayShiftLatePunch : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string Division;
    string Status;
    double Final_Count;
    string TokenNo;


    double Present_Count;
    double Absent_Count;
    decimal Fixed_Work_Days;
    decimal Month_WH_Count;
    double Present_WH_Count;
    double NFH_Days_Count;
    double NFH_Days_Present_Count;

    BALDataAccess objdata = new BALDataAccess();

    string SSQL = "";
    DataTable AutoDTable = new DataTable();
    DataTable mEmployeeDT = new DataTable();
    DataTable MEmployeeDS = new DataTable();
    DateTime date1;
    DateTime Date2 = new DateTime();
    int intK;
    string FromDate=null;
    string ToDate;
    string Shift;
    string Date_Value_Str;
    string Date_value_str1;
    string[] Time_Minus_Value_Check;
    string WagesType;
    double leaveCount;


    System.Web.UI.WebControls.DataGrid grid =
                            new System.Web.UI.WebControls.DataGrid();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report- Day Late Punch Between Dates";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("ManualEntry"));
                //li.Attributes.Add("class", "droplink active open");
            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            Status = Request.QueryString["Status"].ToString();
            Division = Request.QueryString["Division"].ToString();

            string TempWages = Request.QueryString["Wages"].ToString();
            WagesType = TempWages.Replace("_", "&");
            Shift = Request.QueryString["Shift"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();


            double Total_Time_get;
            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();
            DataTable mLocalDS = new DataTable();
            DataTable Shift_DT = new DataTable();

            AutoDTable.Columns.Add("MachineID");
            AutoDTable.Columns.Add("FirstName");
            AutoDTable.Columns.Add("DeptName");

            //AutoDTable.Columns.Add("Category");
            //AutoDTable.Columns.Add("Source");
            //AutoDTable.Columns.Add("Grade");

            date1 = Convert.ToDateTime(FromDate);
            string dat = ToDate;
            Date2 = Convert.ToDateTime(dat);

            int daycount = (int)((Date2 - date1).TotalDays);
            int daysAdded = 0;

            while (daycount >= 0)
            {
                DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded).ToShortDateString());
                //string day1=string.Format("{MM/dd/yyyy}",date1.AddDays(daysAdded));
                AutoDTable.Columns.Add(Convert.ToString(dayy.ToShortDateString()));

                daycount -= 1;
                daysAdded += 1;
            }

            AutoDTable.Columns.Add("Total Minutes");

            SSQL = "Select DISTINCT MachineID from LogTime_Days";
            dt1 = objdata.RptEmployeeMultipleDetails(SSQL);

           
           
            

            for (int EmpNoRw = 0; EmpNoRw < dt1.Rows.Count; EmpNoRw++)
            {
                SSQL = "Select MachineID,FirstName,DeptName,Present,Attn_Date,TimeIN,Shift from LogTime_Days";
                SSQL = SSQL + " where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                SSQL = SSQL + " and Convert(datetime,Attn_Date,103)>= Convert(datetime,'" + FromDate + "',103) and MachineID='" + dt1.Rows[EmpNoRw]["MachineID"] + "'";
                SSQL = SSQL + " and Convert(datetime,Attn_Date,103) <= Convert(datetime,'" + ToDate + "',103) and Present_Absent !='Absent' and Present_Absent != 'Leave' ORDER BY Attn_Date ASC, Shift ASC";

                dt = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt.Rows.Count > 0)
                {
                    int ColCount = 0, Recheck_Count = 0;
                    ColCount = AutoDTable.Columns.Count - 4;
                    double TotalLate_Minutes = 0;

                    AutoDTable.NewRow();
                    AutoDTable.Rows.Add();

                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["MachineID"] = dt.Rows[0]["MachineID"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["FirstName"] = dt.Rows[0]["FirstName"].ToString();
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["DeptName"] = dt.Rows[0]["DeptName"].ToString();

                    DATERECHECK:

                    for (int i = 0; i < dt.Rows.Count; i++)
                    {

                        double Late_Minutes = 0;

                        DateTime ColDT = new DateTime();
                        DateTime RowDT = new DateTime();

                        ColDT = Convert.ToDateTime(AutoDTable.Columns[(4 + Recheck_Count) - 1].ColumnName);

                        RowDT = Convert.ToDateTime(dt.Rows[i]["Attn_Date"].ToString());


                        //while (daycount >= 0)
                        //{
                        //    DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded).ToShortDateString());

                        if (ColDT == RowDT)
                        {
                            //if (dt.Rows[0]["MachineID"].ToString() == "2001")
                            //{
                            //    var s= 1;
                            //}
                            //for (int ShftChk = 0; ShftChk < Shift_DT.Rows.Count; ShftChk++)
                            //{
                            string Shift_Start_str = "0";
                            string Shift_End_str = "0";
                                string TimeIN_str = "0";
                                TimeIN_str = Convert.ToDateTime(dt.Rows[i]["TimeIN"].ToString()).ToString("HH:mm");
                                //Shift_Start_str = Convert.ToDateTime(Shift_DT.Rows[ShftChk]["StartTime"].ToString()).ToString("HH:mm");
                                DateTime Shift_Start_Time = new DateTime();
                                DateTime TimeIN = new DateTime();
                            DateTime Shift_End_Time = new DateTime();
                                //Shift_Start_Time = Convert.ToDateTime(Shift_Start_str.ToString());
                                TimeIN = Convert.ToDateTime(TimeIN_str);
                                double Start = 0;
                                double End = 10;

                                if (EmpNoRw == 2)
                                {
                                    var st = 1;
                                }

                            if (dt.Rows[i]["Shift"].ToString() == "GENERAL")
                            {
                                string ShiftQUERY = "";
                                ShiftQUERY = "Select * from Mst_LatePunchTime";
                                if (Shift != "ALL")
                                {
                                    ShiftQUERY = ShiftQUERY + " where Shift='" + Shift.ToString() + "'";
                                }else
                                {
                                    ShiftQUERY = ShiftQUERY + " where Shift='"+ dt.Rows[i]["Shift"].ToString() + "'";
                                }

                                Shift_DT = objdata.RptEmployeeMultipleDetails(ShiftQUERY);

                                //Shift Start Check
                                Shift_Start_str = Shift_DT.Rows[0]["MinHr"].ToString();
                                Shift_Start_str = Shift_Start_str + ":" + Shift_DT.Rows[0]["MinMnts"].ToString();
                                Shift_Start_str = Convert.ToDateTime(Shift_Start_str).ToString("HH:mm");
                                Shift_Start_Time = Convert.ToDateTime(Shift_Start_str);

                                //Shift End Check
                                Shift_End_str= Shift_DT.Rows[0]["MaxHr"].ToString();
                                Shift_End_str=Shift_End_str+":"+ Shift_DT.Rows[0]["MaxMnts"].ToString();
                                Shift_End_str = Convert.ToDateTime(Shift_End_str).ToString("HH:mm");
                                Shift_End_Time = Convert.ToDateTime(Shift_End_str);


                                TIMERECHECK:
                                if (TimeIN>=Shift_Start_Time.AddMinutes(Start) && TimeIN <= Shift_Start_Time.AddMinutes(End))
                                {
                                    Late_Minutes = (Shift_Start_Time.AddMinutes(End) - Shift_Start_Time).TotalMinutes;
                                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["" + AutoDTable.Columns[3 + Recheck_Count].ColumnName + ""] = Late_Minutes;
                                    TotalLate_Minutes = TotalLate_Minutes + Late_Minutes;
                                }
                                else if(TimeIN<Shift_Start_Time)
                                {
                                    Late_Minutes = 0;
                                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["" + AutoDTable.Columns[3 + Recheck_Count].ColumnName + ""] = Late_Minutes;
                                }
                                else if(End < 30)
                                {
                                    Start = End+1;
                                    End = End + 10;
                                    goto TIMERECHECK;
                                }else if(TimeIN>Shift_End_Time)
                                {
                                    Late_Minutes = 45;
                                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["" + AutoDTable.Columns[3 + Recheck_Count].ColumnName + ""] = "<span style=color:red><b>" + Late_Minutes + "</b></span>";
                                    TotalLate_Minutes = TotalLate_Minutes + Late_Minutes;
                                }

                            }
                            else if(dt.Rows[i]["Shift"].ToString() == "SHIFT1")
                            {
                                string ShiftQUERY = "";
                                ShiftQUERY = "Select * from Mst_LatePunchTime";
                                if (Shift != "ALL")
                                {
                                    ShiftQUERY = ShiftQUERY + " where Shift='" + Shift.ToString() + "'";
                                }
                                else
                                {
                                    ShiftQUERY = ShiftQUERY + " where Shift='" + dt.Rows[i]["Shift"].ToString() + "'";
                                }

                                Shift_DT = objdata.RptEmployeeMultipleDetails(ShiftQUERY);

                                //Shift Start Check
                                Shift_Start_str = Shift_DT.Rows[0]["MinHr"].ToString();
                                Shift_Start_str = Shift_Start_str + ":" + Shift_DT.Rows[0]["MinMnts"].ToString();
                                Shift_Start_str = Convert.ToDateTime(Shift_Start_str).ToString("HH:mm");
                                Shift_Start_Time = Convert.ToDateTime(Shift_Start_str);

                                //Shift End Check
                                Shift_End_str = Shift_DT.Rows[0]["MaxHr"].ToString();
                                Shift_End_str = Shift_End_str + ":" + Shift_DT.Rows[0]["MaxMnts"].ToString();
                                Shift_End_str = Convert.ToDateTime(Shift_End_str).ToString("HH:mm");
                                Shift_End_Time = Convert.ToDateTime(Shift_End_str);


                                TIMERECHECK:
                                if (TimeIN >= Shift_Start_Time.AddMinutes(Start) && TimeIN <= Shift_Start_Time.AddMinutes(End))
                                {
                                    Late_Minutes = (Shift_Start_Time.AddMinutes(End) - Shift_Start_Time).TotalMinutes;
                                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["" + AutoDTable.Columns[3 + Recheck_Count].ColumnName + ""] = Late_Minutes;
                                    TotalLate_Minutes = TotalLate_Minutes + Late_Minutes;
                                }
                                else if (TimeIN < Shift_Start_Time)
                                {
                                    Late_Minutes = 0;
                                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["" + AutoDTable.Columns[3 + Recheck_Count].ColumnName + ""] = Late_Minutes;
                                }
                                else if (End < 30)
                                {
                                    Start = End + 1;
                                    End = End + 10;
                                    goto TIMERECHECK;
                                }
                                else if (TimeIN > Shift_End_Time)
                                {
                                    Late_Minutes = 45;
                                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["" + AutoDTable.Columns[3 + Recheck_Count].ColumnName + ""] = "<span style=color:red><b>" + Late_Minutes + "</b></span>";
                                    TotalLate_Minutes = TotalLate_Minutes + Late_Minutes;
                                }
                            }
                            else if (dt.Rows[i]["Shift"].ToString() == "SHIFT2")
                            {
                                string ShiftQUERY = "";
                                ShiftQUERY = "Select * from Mst_LatePunchTime";
                                if (Shift != "ALL")
                                {
                                    ShiftQUERY = ShiftQUERY + " where Shift='" + Shift.ToString() + "'";
                                }
                                else
                                {
                                    ShiftQUERY = ShiftQUERY + " where Shift='" + dt.Rows[i]["Shift"].ToString() + "'";
                                }

                                Shift_DT = objdata.RptEmployeeMultipleDetails(ShiftQUERY);

                                //Shift Start Check
                                Shift_Start_str = Shift_DT.Rows[0]["MinHr"].ToString();
                                Shift_Start_str = Shift_Start_str + ":" + Shift_DT.Rows[0]["MinMnts"].ToString();
                                Shift_Start_str = Convert.ToDateTime(Shift_Start_str).ToString("HH:mm");
                                Shift_Start_Time = Convert.ToDateTime(Shift_Start_str);

                                //Shift End Check
                                Shift_End_str = Shift_DT.Rows[0]["MaxHr"].ToString();
                                Shift_End_str = Shift_End_str + ":" + Shift_DT.Rows[0]["MaxMnts"].ToString();
                                Shift_End_str = Convert.ToDateTime(Shift_End_str).ToString("HH:mm");
                                Shift_End_Time = Convert.ToDateTime(Shift_End_str);


                                TIMERECHECK:
                                if (TimeIN >= Shift_Start_Time.AddMinutes(Start) && TimeIN <= Shift_Start_Time.AddMinutes(End))
                                {
                                    Late_Minutes = (Shift_Start_Time.AddMinutes(End) - Shift_Start_Time).TotalMinutes;
                                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["" + AutoDTable.Columns[3 + Recheck_Count].ColumnName + ""] = Late_Minutes;
                                    TotalLate_Minutes = TotalLate_Minutes + Late_Minutes;
                                }
                                else if (TimeIN < Shift_Start_Time)
                                {
                                    Late_Minutes = 0;
                                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["" + AutoDTable.Columns[3 + Recheck_Count].ColumnName + ""] = Late_Minutes;
                                }
                                else if (End < 30)
                                {
                                    Start = End + 1;
                                    End = End + 10;
                                    goto TIMERECHECK;
                                }
                                else if (TimeIN > Shift_End_Time)
                                {
                                    Late_Minutes = 45;
                                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["" + AutoDTable.Columns[3 + Recheck_Count].ColumnName + ""] = "<span style=color:red><b>" + Late_Minutes + "</b></span>";
                                    TotalLate_Minutes = TotalLate_Minutes + Late_Minutes;
                                }
                            }
                            else if (dt.Rows[i]["Shift"].ToString() == "SHIFT3")
                            {
                                string ShiftQUERY = "";
                                ShiftQUERY = "Select * from Mst_LatePunchTime";
                                if (Shift != "ALL")
                                {
                                    ShiftQUERY = ShiftQUERY + " where Shift='" + Shift.ToString() + "'";
                                }
                                else
                                {
                                    ShiftQUERY = ShiftQUERY + " where Shift='" + dt.Rows[i]["Shift"].ToString() + "'";
                                }

                                Shift_DT = objdata.RptEmployeeMultipleDetails(ShiftQUERY);

                                //Shift Start Check
                                Shift_Start_str = Shift_DT.Rows[0]["MinHr"].ToString();
                                Shift_Start_str = Shift_Start_str + ":" + Shift_DT.Rows[0]["MinMnts"].ToString();
                                Shift_Start_str = Convert.ToDateTime(Shift_Start_str).ToString("HH:mm");
                                Shift_Start_Time = Convert.ToDateTime(Shift_Start_str);

                                //Shift End Check
                                Shift_End_str = Shift_DT.Rows[0]["MaxHr"].ToString();
                                Shift_End_str = Shift_End_str + ":" + Shift_DT.Rows[0]["MaxMnts"].ToString();
                                Shift_End_str = Convert.ToDateTime(Shift_End_str).ToString("HH:mm");
                                Shift_End_Time = Convert.ToDateTime(Shift_End_str);


                                TIMERECHECK:
                                if (TimeIN >= Shift_Start_Time.AddMinutes(Start) && TimeIN <= Shift_Start_Time.AddMinutes(End))
                                {
                                    Late_Minutes = (Shift_Start_Time.AddMinutes(End) - Shift_Start_Time).TotalMinutes;
                                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["" + AutoDTable.Columns[3 + Recheck_Count].ColumnName + ""] = Late_Minutes;
                                    TotalLate_Minutes = TotalLate_Minutes + Late_Minutes;
                                }
                                else if (TimeIN < Shift_Start_Time)
                                {
                                    Late_Minutes = 0;
                                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["" + AutoDTable.Columns[3 + Recheck_Count].ColumnName + ""] = Late_Minutes;
                                }
                                else if (End < 30)
                                {
                                    Start = End + 1;
                                    End = End + 10;
                                    goto TIMERECHECK;
                                }
                                else if (TimeIN > Shift_End_Time)
                                {
                                    Late_Minutes = 45;
                                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["" + AutoDTable.Columns[3 + Recheck_Count].ColumnName + ""] = "<span style=color:red><b>" + Late_Minutes + "</b></span>";
                                    TotalLate_Minutes = TotalLate_Minutes + Late_Minutes;
                                }
                            }


                            //TIMERECHECK:
                            //if (TimeIN >= Shift_Start_Time.AddMinutes(Start) && TimeIN < Shift_Start_Time.AddMinutes(End))
                            //{
                            //    Late_Minutes = (TimeIN - Shift_Start_Time).TotalMinutes;
                            //    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["" + AutoDTable.Columns[3 + Recheck_Count].ColumnName + ""] = Late_Minutes;
                            //    TotalLate_Minutes = TotalLate_Minutes + Late_Minutes;
                            //}
                            //else if (End < 30)
                            //{
                            //    Start = End;
                            //    End = End + 10;
                            //    goto TIMERECHECK;
                            //}
                            //else
                            //{
                            //    Late_Minutes = 0;
                            //}
                            //}
                            //}
                            //daycount -= 1;
                            //daysAdded += 1;
                        }
                        //Check the next Present Date
                        if (i + 1 == dt.Rows.Count)
                        {
                            if (AutoDTable.Rows[AutoDTable.Rows.Count - 1]["" + AutoDTable.Columns[3 + Recheck_Count].ColumnName + ""].ToString() == string.Empty)
                            {
                                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["" + AutoDTable.Columns[3 + Recheck_Count].ColumnName + ""] = "0";
                                Recheck_Count = Recheck_Count + 1;
                                goto REMAINIGCHECK;
                            }else
                            {
                                Recheck_Count = Recheck_Count + 1;
                                goto REMAINIGCHECK;
                            }
                        }
                    }

                    REMAINIGCHECK:
                    //Check the End Date in AutoDTable
                    if (Recheck_Count < ColCount)
                    {
                        goto DATERECHECK;
                    }
                    else
                    {
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Total Minutes"] = TotalLate_Minutes.ToString();
                    }
                }
            }

            //Add Red Red color for 45mnts
            //for(int i=0;i<AutoDTable.Rows.Count;i++)
            //{
            //    for(int j = 3; j < AutoDTable.Columns.Count; j++)
            //    {
            //        if (AutoDTable.Rows[i][j].ToString() == "45")
            //        {
            //            AutoDTable.Rows[i][j] = "<span style=color:red><b>" + AutoDTable.Rows[i][j].ToString() + "</b></span>";
            //        }
            //    }
            //}

            if (AutoDTable.Rows.Count != 0)
            {
                grid.DataSource = AutoDTable;
                grid.DataBind();
                string attachment = "attachment;filename=DAY SHIFT LATE PUNCH BETWEEN DATES.xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";
                grid.HeaderStyle.Font.Bold = true;
                System.IO.StringWriter stw = new System.IO.StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);
                grid.RenderControl(htextw);
                Response.Write("<table>");
                Response.Write("<tr Font-Bold='true' align='left'>");
                Response.Write("<td colspan='10'>");
                Response.Write("<a style=\"font-weight:bold\">DAY SHIFT LATE PUNCH BETWEEN DATES REPORT -" + Session["Lcode"].ToString() + "-" + FromDate + "-" + ToDate + "</a>");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");
                Response.Write(stw.ToString());
                Response.End();
                Response.Clear();
            }
        }
    }
}