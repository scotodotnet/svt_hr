﻿<%@ Page Language="C#" MasterPageFile="~/PayrollMaster.master" AutoEventWireup="true" CodeFile="RptPayEmployeeDet.aspx.cs" Inherits="RptPayEmployeeDet" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:UpdatePanel ID="UptPanel1" runat="server">
<ContentTemplate>

 <!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Report</a></li>
				<li class="active">Employee Details</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Employee Details</h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">Employee Details</h4>
                        </div>
                        <div class="panel-body">
                        <div class="form-group">
                    
                     <!-- begin row -->
                        <div class="row">
                        <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Category</label>
								 <asp:DropDownList runat="server" ID="ddlRptCategory" class="form-control select2" AutoPostBack="true" OnSelectedIndexChanged="ddlcategory_SelectedIndexChanged"  style="width:100%;">
								 <asp:ListItem Value="0">-Select-</asp:ListItem>
								 <asp:ListItem Value="1">Staff</asp:ListItem>
								 <asp:ListItem Value="2">Labour</asp:ListItem>
								</asp:DropDownList>
								</div>
                               </div>
                           <!-- end col-4 -->
                           <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Department</label>
								 <asp:DropDownList runat="server" ID="ddlDept" class="form-control select2" style="width:100%;">
								 </asp:DropDownList>
								</div>
                               </div>
                           <!-- end col-4 -->
                            <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Division</label>
								 <asp:DropDownList runat="server" ID="ddlDivision" class="form-control select2" style="width:100%;">
								 </asp:DropDownList>
								</div>
                               </div>
                           <!-- end col-4 -->
                        </div>
                       <!-- end row -->
                       <!-- begin row -->
                        <div class="row">
                        <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>From Year</label>
								 <asp:DropDownList runat="server" ID="ddlFromYear" class="form-control select2" style="width:100%;">
								 </asp:DropDownList>
								</div>
                               </div>
                           <!-- end col-4 -->
                           <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>To Year</label>
								 <asp:DropDownList runat="server" ID="ddlToYear" class="form-control select2" style="width:100%;">
								 </asp:DropDownList>
								</div>
                               </div>
                           <!-- end col-4 -->
                               <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Month</label>
								 <asp:DropDownList runat="server" ID="ddlMonth" class="form-control select2" style="width:100%;">
								 <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
								 <asp:ListItem Value="January">January</asp:ListItem>
								 <asp:ListItem Value="February">February</asp:ListItem>
								 <asp:ListItem Value="March">March</asp:ListItem>
								 <asp:ListItem Value="April">April</asp:ListItem>
								 <asp:ListItem Value="May">May</asp:ListItem>
								 <asp:ListItem Value="June">June</asp:ListItem>
								 <asp:ListItem Value="July">July</asp:ListItem>
								 <asp:ListItem Value="August">August</asp:ListItem>
								 <asp:ListItem Value="September">September</asp:ListItem>
								 <asp:ListItem Value="October">October</asp:ListItem>
								 <asp:ListItem Value="November">November</asp:ListItem>
								 <asp:ListItem Value="December">December</asp:ListItem>
							 	 </asp:DropDownList>
								</div>
                               </div>
                           <!-- end col-4 -->
                        </div>
                       <!-- end row -->
                         <!-- begin row -->
                        <div class="row">
                         <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Search Type</label>
								
								    <asp:RadioButtonList ID="RdbDOJDOB" runat="server" RepeatColumns="2" class="form-control">
								    <asp:ListItem Selected="False" Text="Date of Joining" style="padding-right:40px" Value="0"></asp:ListItem>
                                    <asp:ListItem Selected="False" Text="Date of Birth" Value="1"></asp:ListItem>
                                    </asp:RadioButtonList>
								 </div>
                               </div>
                           <!-- end col-4 -->
                           <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Qualification</label>
								 <asp:DropDownList runat="server" ID="ddlQualification" class="form-control select2" style="width:100%;">
								 </asp:DropDownList>
								</div>
                               </div>
                           <!-- end col-4 -->
                            <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Designation</label>
								 <asp:DropDownList runat="server" ID="ddlDesignation" class="form-control select2" style="width:100%;">
								 </asp:DropDownList>
								</div>
                               </div>
                           <!-- end col-4 -->
                        </div>
                         <!-- end row -->
                         
                           <!-- begin row -->
                        <div class="row">
                        
                           <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Employee Type</label>
								 <asp:DropDownList runat="server" ID="ddlEmpType" class="form-control select2" style="width:100%;">
								 </asp:DropDownList>
								</div>
                               </div>
                           <!-- end col-4 -->
                            <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Sort By</label>
								 <asp:DropDownList runat="server" ID="ddlSortBy" class="form-control select2" style="width:100%;">
								 </asp:DropDownList>
								</div>
                               </div>
                           <!-- end col-4 -->
                           <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Gender</label>
								
								    <asp:RadioButtonList ID="rbtngender" runat="server" RepeatColumns="3" class="form-control">
                                         <asp:ListItem Selected="True" Text="ALL" style="padding-right:40px" Value="0"></asp:ListItem>
                                         <asp:ListItem Selected="False" Text="Male" style="padding-right:40px" Value="1"></asp:ListItem>
                                         <asp:ListItem Selected="False" Text="Female" Value="2"></asp:ListItem>
                                        
                                     </asp:RadioButtonList>
								 </div>
                               </div>
                           <!-- end col-4 -->
                        </div>
                         <!-- end row -->
                         
                            <!-- begin row -->
                        <div class="row">
                        
                           <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>PF Grade</label>
								     <asp:RadioButtonList ID="rppfgrade" runat="server" RepeatColumns="3" class="form-control">
                                        <asp:ListItem Selected="True" Text="ALL" style="padding-right:40px" Value="0"></asp:ListItem>
                                        <asp:ListItem Selected="False" Text="On Role" style="padding-right:40px" Value="1"></asp:ListItem>
                                        <asp:ListItem Selected="False" Text="Off Role" Value="2"></asp:ListItem>
                                     </asp:RadioButtonList>
								 </div>
                               </div>
                           <!-- end col-4 -->
                           <!-- begin col-4 -->
                                 <div class="col-md-4">
									<div class="form-group">
										<label>DeActive From</label>
										<asp:TextBox runat="server" ID="txtDeActFrom" class="form-control datepicker"></asp:TextBox>
									</div>
                                  </div>
                               <!-- end col-4 -->
                                <!-- begin col-4 -->
                                 <div class="col-md-4">
									<div class="form-group">
										<label>DeActive To</label>
										<asp:TextBox runat="server" ID="txtDeActTo" class="form-control datepicker"></asp:TextBox>
									</div>
                                  </div>
                               <!-- end col-4 -->
                        </div>
                         <!-- end row -->
                         <!-- begin row -->  
                        <div class="row">
                         <!-- begin col-4 -->
                          <div class="col-md-4">
                               <div class="form-group">
                                   <div class="col-md-8">
                                        <label class="checkbox-inline">
                                            <asp:CheckBox ID="chkDeActive" runat="server" />De Active
                                        </label>
                                       
                                    </div>
                                </div>
                             </div>
                           <!-- end col-4 -->
                         </div>
                       <!-- end row -->
                             <!-- begin row -->  
                        <div class="row">
                         <div class="col-md-4"></div>
                           <!-- begin col-4 -->
                                <div class="col-md-4">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btnBonusView" Text="Report View" 
                                         class="btn btn-success" onclick="btnBonusView_Click" Visible="false" />
									<asp:Button runat="server" id="btnBonusExcelView" Text="Excel View" 
                                         class="btn btn-success" onclick="btnBonusExcelView_Click" />
                                         	<asp:Button runat="server" id="btnBonusPDFView" Text="PDF View" 
                                         class="btn btn-success" onclick="btnBonusPDFView_Click" Visible="false" />
						    	 </div>
                               </div>
                              <!-- end col-4 -->
                              <div class="col-md-4"></div>
                         </div>
                        <!-- end row --> 
                        </div>
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
                
                <asp:Panel ID="Result_Panel" Visible="false" runat="server">
				                                <table class="full">
				                                <tbody>
				                                    <tr>
                                                        <td colspan="4">
                                                            <asp:GridView ID="griddept" runat="server" Width="100%">
                                                                <%--<Columns>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>SL No</HeaderTemplate>
                                                                        <ItemTemplate><%# Container.DataItemIndex + 1 %></ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Emp No</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblempno" runat="server" Text='<%# Eval("EmpNo") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>OLD ID</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbloldid" runat="server" Text='<%# Eval("MachineID")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Exist.Code</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblExisistingCode" runat="server" Text='<%# Eval("ExistingCode")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Emp. Name</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                        <asp:Label ID="lblempname" runat="server" Text= '<%# Eval("FirstName") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Date of Joining</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbldoj" runat="server" Text='<%# Eval("DOJ") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                   
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Date of Birth</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbldob" runat="server" Text='<%# Eval("DOB") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Department</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDepartment" runat="server" Text='<%# Eval("DeptName") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Designation</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDesignation" runat="server" Text='<%# Eval("Designation") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField> 
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Emp. Type</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblEmpType" runat="server" Text='<%# Eval("EmpType") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                                                                                
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>SalaryThrough</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblSalaryThrough" runat="server" Text='<%# Eval("Salarythrough")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>ESINumber</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblESICnumber" runat="server" Text='<%# Eval("ESINo")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>PFNumber</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblPFnumber" runat="server" Text='<%# Eval("PFNo")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>WagesType</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblWagestype" runat="server" Text='<%# Eval("Wagestype")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Salary</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblBase_Salary" runat="server" Text='<%# Eval("Base_Salary")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                  
                                                                    
                                                                </Columns>--%>
                                                            </asp:GridView>
                                                            <asp:GridView ID="GridViewdeactiveded" runat="server" Width="100%">
                                                                <%--<Columns>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>SL No</HeaderTemplate>
                                                                        <ItemTemplate><%# Container.DataItemIndex + 1 %></ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Emp No</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblempno" runat="server" Text='<%# Eval("EmpNo") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>OLD ID</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbloldid" runat="server" Text='<%# Eval("MachineID")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Exist.Code</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblExisistingCode" runat="server" Text='<%# Eval("ExistingCode")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Emp. Name</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                        <asp:Label ID="lblempname" runat="server" Text= '<%# Eval("FirstName") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Date of Joining</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbldoj" runat="server" Text='<%# Eval("DOJ") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>De-Activate Date</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDeActivateDate" runat="server" Text='<%# Eval("deactivedate") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Date of Birth</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbldob" runat="server" Text='<%# Eval("DOB") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Department</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDepartment" runat="server" Text='<%# Eval("DeptName") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Designation</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDesignation" runat="server" Text='<%# Eval("Designation") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField> 
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Emp. Type</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblEmpType" runat="server" Text='<%# Eval("EmpType") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                                                                                  
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>SalaryThrough</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblSalaryThrough" runat="server" Text='<%# Eval("Salarythrough")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>ESINumber</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblESICnumber" runat="server" Text='<%# Eval("ESINo")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>PFNumber</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblPFnumber" runat="server" Text='<%# Eval("PFNo")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>WagesType</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblWagestype" runat="server" Text='<%# Eval("Wagestype")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Salary</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblBase_Salary" runat="server" Text='<%# Eval("Base_Salary")%>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                  
                                                                    
                                                                </Columns>--%>
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
				                                </tbody>
				                            </table>
				                            </asp:Panel>
                
                
            </div>
            <!-- end row -->
        </div>
<!-- end #content -->
</ContentTemplate>
<Triggers>
<asp:PostBackTrigger ControlID="btnBonusView" />
<asp:PostBackTrigger ControlID="btnBonusExcelView" />
<asp:PostBackTrigger ControlID="btnBonusPDFView" />
</Triggers>
</asp:UpdatePanel>
</asp:Content>

