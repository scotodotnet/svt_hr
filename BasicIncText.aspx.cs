﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using Payroll;
public partial class BasicIncText : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string Query = "";
    string SessionPayroll;
    DataTable dt = new DataTable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        Load_DB();
        if (!IsPostBack)
        {
            Months_load();
            int currentYear = DateTime.Now.Year;  //Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddlFinance.Items.Add(currentYear.ToString());
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }

            currentYear = Utility.GetCurrentYearOnly;
           Load_Data_Incentive();

        }
        Load_Data_Incentive();

 
    }
    public void Load_DB()
    {
        //Get Database Name
        string query = "";
        DataTable dt_DB = new DataTable();
        query = "select CMS,Rights,Sales,Stores,Weaving,Maintaince,Production,Payroll from [SVT_Rights]..MstDBname";
        dt_DB = objdata.RptEmployeeMultipleDetails(query);
        if (dt_DB.Rows.Count > 0)
        {
            SessionPayroll = dt_DB.Rows[0]["Payroll"].ToString();
            //SessionRights = dt_DB.Rows[0]["Rights"].ToString();
        }
    }
    private void Load_Data_Incentive()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from [" + SessionPayroll + "]..MstBasic_Incentive_text where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater1.DataSource = DT;
        Repeater1.DataBind();
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            string MessFlag = "Insert";

            string Incentive_Text = "";
            if (RdpIncentive.SelectedValue == "1")
            {
                Incentive_Text = "Att.Inc";
            }
            else if (RdpIncentive.SelectedValue == "2")
            {
                Incentive_Text = "Prod.Inc";
            }
            else
            {
                Incentive_Text = "Punc.Inc";
            }
            DataTable DT_Q = new DataTable();
            Query = "Select * from [" + SessionPayroll + "]..MstBasic_Incentive_text where Ccode='" + SessionCcode + "' And LCode='" + SessionLcode + "' and Years='" + ddlFinance.SelectedValue + "' and Month='" + ddlMonths.SelectedValue + "'";
            DT_Q = objdata.RptEmployeeMultipleDetails(Query);
            if (DT_Q.Rows.Count > 0)
            {
                Query = "delete from [" + SessionPayroll + "]..MstBasic_Incentive_text where Ccode='" + SessionCcode + "' And LCode='" + SessionLcode + "' and Years='" + ddlFinance.SelectedValue + "' and Month='" + ddlMonths.SelectedValue + "'";
                DT_Q = objdata.RptEmployeeMultipleDetails(Query);
                MessFlag = "Update";

            }
            Query = "Insert into [" + SessionPayroll + "]..MstBasic_Incentive_text(Ccode,LCode,Years,Month,Incent_ID,Incentive_Text)values(";
            Query = Query + "'" + SessionCcode + "','" + SessionLcode + "','" + ddlFinance.SelectedValue + "','" + ddlMonths.SelectedValue + "','" + RdpIncentive.SelectedValue + "','" + Incentive_Text + "')";
            objdata.RptEmployeeMultipleDetails(Query);
            if (MessFlag == "Insert")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Update Successfully');", true);
            }
            Load_Data_Incentive();
        }
        catch (Exception Ex)
        { 
        
        }
    }
    public void Months_load()
    {
        DataTable dt = new DataTable();
        Query = "Select 0 as ID,'-----Select----' as Months Union ";
        Query = Query + "Select ID,Months from [" + SessionPayroll + "]..MonthDetails order by ID ASC ";
        dt = objdata.RptEmployeeMultipleDetails(Query);
        ddlMonths.DataSource = dt;
        ddlMonths.DataTextField = "Months";
        ddlMonths.DataValueField = "Months";
        ddlMonths.DataBind();
    }

    protected void GridEditEnquiryClick(object sender, CommandEventArgs e)
    {
        Query = "Select * from [" + SessionPayroll + "]..MstBasic_Incentive_text where Ccode='" + SessionCcode + "' And LCode='" + SessionLcode + "' and Years='" + e.CommandName.ToString()+ "' and Month='" + e.CommandArgument.ToString() + "'";
        dt = objdata.RptEmployeeMultipleDetails(Query);
        if (dt.Rows.Count != 0)
        {
            ddlFinance.SelectedItem.Text = dt.Rows[0]["Years"].ToString();
            ddlMonths.SelectedItem.Text = dt.Rows[0]["Month"].ToString();
            string incentivecode = dt.Rows[0]["Incent_ID"].ToString();
            if (incentivecode == "1")
            {
                RdpIncentive.SelectedValue = "1";
            }
            else if (incentivecode == "2")
            {
                RdpIncentive.SelectedValue = "2";
            }
            else
            {
                RdpIncentive.SelectedValue = "3";
            }
        }

    }
    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {
        string query;
        DataTable DT = new DataTable();


        query = "select * from [" + SessionPayroll + "]..MstBasic_Incentive_text where Ccode='" + SessionCcode + "' And LCode='" + SessionLcode + "' and Years='" + e.CommandName.ToString() + "' and Month='" + e.CommandArgument.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count > 0)
        {
            query = "delete from [" + SessionPayroll + "]..MstBasic_Incentive_text where Ccode='" + SessionCcode + "' And LCode='" + SessionLcode + "' and Years='" + e.CommandName.ToString() + "' and Month='" + e.CommandArgument.ToString() + "'";
            objdata.RptEmployeeMultipleDetails(query);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Incentive Deleted Successfully...!');", true);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Data Found..!');", true);
        }
        Load_Data_Incentive();
    }
}
