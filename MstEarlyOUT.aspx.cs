﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class MstEarlyOUT : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights; string SessionAdmin;
    string SSQL;
    bool ErrFlag = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }

        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();

        if (!IsPostBack)
        {
            Load_ID();
            Load_shift();

           
        }
        Load_OlD();
        txtShiftTypeCd.ReadOnly = true;
    }

    private void Load_shift()
    {
        SSQL = "";
        SSQL = "Select ShiftDesc from Shift_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' order by ShiftDesc ASC";
        ddlShiftType.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlShiftType.DataTextField = "ShiftDesc";
        ddlShiftType.DataValueField = "ShiftDesc";
        ddlShiftType.DataBind();
        ddlShiftType.Items.Insert(0,new ListItem("Select","0"));
    }

    private void Load_OlD()
    {
        SSQL = "";
        SSQL = "Select * from MstEarlyOUT where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        Repeater1.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater1.DataBind();

    }

    private void Load_ID()
    {
        Int32 ID = 0;
        SSQL = "";
        SSQL = "Select Max(ShiftTypeCd) from MstEarlyOUT where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        DataTable ID_dt = new DataTable();
        ID_dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (ID_dt.Rows.Count > 0)
        {
            if (ID_dt.Rows[0][0] == null || ID_dt.Rows[0][0].ToString() == string.Empty)
            {
                ID = 1;
            }
            else
            {
                ID = 1 + Convert.ToInt32(ID_dt.Rows[0][0].ToString());
            }
        }
        else
        {
            ID = 1;
        }

        txtShiftTypeCd.Text = (ID).ToString();
        
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (txtShiftTypeCd.Text == "" || ddlShiftType.SelectedItem.Text == "Select"||txtStart.Text==""||txtEnd.Text=="")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please Check the Fields!!!');", true);
            return;
        }
        else
        {
            if (btnSave.Text == "Update")
            {
                SSQL = "";
                SSQL = "Delete From MstEarlyOUT where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and ShiftTypeCd='" + txtShiftTypeCd.Text + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);
                btnSave.Text = "Save";
            }
            if (btnSave.Text == "Save")
            {
                SSQL = "";
                SSQL = "Select * from MstEarlyOUT where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and ShiftTypeName='" + ddlShiftType.SelectedItem.Text.ToUpper() + "'";
                DataTable chk = new DataTable();
                chk = objdata.RptEmployeeMultipleDetails(SSQL);
                if (chk.Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Category Alredy Inserted!!!');", true);
                    btnClr_Click(sender, e);
                }
                else
                {
                    SSQL = "";
                    SSQL = "Insert Into MstEarlyOUT values('" + SessionCcode + "','" + SessionLcode + "','" + txtShiftTypeCd.Text + "','" + ddlShiftType.SelectedItem.Text.ToUpper() + "','" + ddlShiftType.SelectedItem.Value + "','" + txtStart.Text + "','" + txtEnd.Text + "')";
                    objdata.RptEmployeeMultipleDetails(SSQL);
                    Load_OlD();
                    btnClr_Click(sender, e);
                }
            }
        }
    }

    protected void btnClr_Click(object sender, EventArgs e)
    {
        ddlShiftType.ClearSelection();
        txtStart.Text = "";
        txtEnd.Text = "";
        Load_ID();
    }

    protected void btnApprvEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {
        if (e.CommandArgument.ToString() == "Edit")
        {
            SSQL = "";
            SSQL = "Select * from MstEarlyOUT where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and ShiftTypeCd='" + e.CommandName.ToString() + "'";
            DataTable dt = new DataTable();
            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt.Rows.Count > 0)
            {
                txtShiftTypeCd.Text = dt.Rows[0]["ShiftTypeCd"].ToString();
                ddlShiftType.SelectedValue= dt.Rows[0]["ShiftCd"].ToString();
                txtStart.Text= dt.Rows[0]["EarlyStart"].ToString();
                txtEnd.Text = dt.Rows[0]["EarlyEnd"].ToString();
                btnSave.Text = "Update";
            }
        }
        if (e.CommandArgument.ToString() == "Delete")
        {
            SSQL = "";
            SSQL = "Delete from MstEarlyOUT where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and ShiftTypeCd='" + e.CommandName.ToString() + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Details Deleted Succesfully!!!');", true);
            Load_OlD();
            Load_ID();
        }
    }

    protected void txtShiftTypeCd_TextChanged(object sender, EventArgs e)
    {

    }

    protected void ddlShiftType_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}