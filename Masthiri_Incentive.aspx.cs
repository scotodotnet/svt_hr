﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
public partial class Masthiri_Incentive : System.Web.UI.Page
{

    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SSQL;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Masthiri Incentive";

            LoadWages();

        }
    }

    private void LoadWages()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlWages.Items.Clear();
        query = "Select *from MstEmployeeType";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlWages.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpType"] = "-Select-";
        dr["EmpType"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlWages.DataTextField = "EmpType";
        ddlWages.DataValueField = "EmpType";
        ddlWages.DataBind();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        bool ErrFlg = false;

        if (ddlWages.SelectedItem.Text == "-Select-" || ddlWages.SelectedItem.Value == "-Select-")
        {
            ddlWages.Focus();
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please Select the Wages Type');", true);
            return;
        }

        if (!ErrFlg)
        {
            string Query = "";
            Query = "Delete from Masthiri_Inc where Wages='" + ddlWages.SelectedItem.Text + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";

            objdata.RptEmployeeMultipleDetails(Query);

            Query = "Insert into Masthiri_Inc values('" + SessionCcode + "','" + SessionLcode + "','" + ddlWages.SelectedItem.Text.ToUpper() + "','" + txtAmount.Text + "');";
            objdata.RptEmployeeMultipleDetails(Query);

        }
    }

    protected void btnClr_Click(object sender, EventArgs e)
    {
        ddlWages.ClearSelection();
        txtAmount.Text = "";
    }

    protected void ddlWages_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlWages.SelectedItem.Text != "-Select-")
        {
            SSQL = "Select * from  Masthiri_Inc where Wages='" + ddlWages.SelectedItem.Text + "'";
            DataTable Masthiri_Inc_DT = new DataTable();
            Masthiri_Inc_DT = objdata.RptEmployeeMultipleDetails(SSQL);
            if (Masthiri_Inc_DT.Rows.Count > 0)
            {
                txtAmount.Text = Masthiri_Inc_DT.Rows[0]["Amt"].ToString();
            }else
            {
                txtAmount.Text = "0";
            }
        }
    }

    protected void btnApprvEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {

    }

    protected void btnCancelEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {

    }
}