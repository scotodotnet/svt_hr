<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="SalaryUpdate.aspx.cs" Inherits="SalaryUpdate" Title="" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">




<script src="assets/js/master_list_jquery.min.js"></script>
<script src="assets/js/master_list_jquery-ui.min.js"></script>
<link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css"/>
 <script>
     $(document).ready(function() {
     $('#example').dataTable();
     });
 </script>


<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
                $('.select2').select2();
            }
        });
    };
</script>
<asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                <ContentTemplate>
<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Basic Update</a></li>
				<li class="active">Salary Update</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Salary Update</h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">Salary Update</h4>
                        </div>
                        <div class="panel-body">
                        <div class="form-group">
                        <!-- begin row -->
                          <div class="row">
                            <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Wages Type</label>
								 <asp:DropDownList runat="server" ID="ddlWages" class="form-control select2" 
                                        AutoPostBack="true" style="width:100%;" 
                                        onselectedindexchanged="ddlWages_SelectedIndexChanged">
							 	 </asp:DropDownList>
								</div>
                               </div>
                           <!-- end col-4 -->
                           
                           <!-- begin col-4 -->
                            <div class="col-md-4">
							   <div class="form-group">
								<label>Token No</label>
								<asp:TextBox runat="server" ID="txtTokenNo" class="form-control" 
                                       AutoPostBack="true" ontextchanged="txtTokenNo_TextChanged"></asp:TextBox>
								</div>
                             </div>
                           <!-- end col-4 -->
                          <!-- begin col-4 -->
                           <div class="col-md-4">
                            <div class="form-group">
                           <label>Machine ID</label>
                          <asp:DropDownList runat="server" ID="txtMachineID" class="form-control select2" 
                                    AutoPostBack="true" style="width:100%;" 
                                    onselectedindexchanged="txtMachineID_SelectedIndexChanged">
                          </asp:DropDownList>
                           </div>
                           </div>
                           <!-- end col-4 -->
                          </div>
                        <!-- end row -->
                       <!-- begin row -->
                          <div class="row">
                          <!-- begin col-4 -->
                            <div class="col-md-4">
							   <div class="form-group">
								<label>Employee Name</label>
								<asp:Label runat="server" ID="txtEmpName" class="form-control" BackColor="#F3F3F3"></asp:Label>
								</div>
                             </div>
                           <!-- end col-4 -->
                           <!-- begin col-4 -->
                            <div class="col-md-4">
							   <div class="form-group">
								<label>Department</label>
								<asp:Label runat="server" ID="txtDepartment" class="form-control" BackColor="#F3F3F3"></asp:Label>
								</div>
                             </div>
                           <!-- end col-4 -->
                            <!-- begin col-4 -->
                            <div class="col-md-4">
							   <div class="form-group">
								<label>DOJ</label>
								<asp:Label runat="server" ID="txtDOJ" class="form-control" BackColor="#F3F3F3"></asp:Label>
								</div>
                             </div>
                           <!-- end col-4 -->
                           </div>
                        <!-- end row -->
                        <!-- begin row -->
                          <div class="row">
                          <!-- begin col-4 -->
                          <div class="col-md-4">
                               <div class="form-group">
                                    <label class="col-md-12">Salary Type</label>
                                   <%-- <div class="col-md-8">--%>
                                        <label class="checkbox-inline">
                                            <asp:CheckBox ID="chkNewWages" runat="server" AutoPostBack="true" 
                                        oncheckedchanged="chkNewWages_CheckedChanged" />New Salary
                                        </label>
                                        <label class="checkbox-inline">
                                            <asp:CheckBox ID="chkOldWages" runat="server" AutoPostBack="true" 
                                        oncheckedchanged="chkOldWages_CheckedChanged" />Salary Increment
                                        </label>
                                    <%--</div>--%>
                                </div>
                             </div>
                           <!-- end col-4 -->
                            <!-- begin col-4 -->
                            <div class="col-md-4">
							   <div class="form-group">
								<label>OLD Salary</label>
								<asp:TextBox runat="server" ID="txtOldSalary" class="form-control"></asp:TextBox>
								</div>
                             </div>
                           <!-- end col-4 -->
                             <!-- begin col-4 -->
                            <div class="col-md-4">
							   <div class="form-group">
								<label>New Salary</label>
								<asp:TextBox runat="server" ID="txtNewSalary" class="form-control"></asp:TextBox>
								</div>
                             </div>
                           <!-- end col-4 -->
                          </div>
                        <!-- end row -->
                      <!-- begin row -->  
                        <div class="row">
                         <div class="col-md-4"></div>
                           <!-- begin col-4 -->
                                <div class="col-md-4">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btnSave" Text="Save" class="btn btn-success" 
                                         onclick="btnSave_Click" />
									<asp:Button runat="server" id="btnClear" Text="Clear" class="btn btn-danger" 
                                         onclick="btnClear_Click" />
								 </div>
                               </div>
                              <!-- end col-4 -->
                              <div class="col-md-4"></div>
                         </div>
                        <!-- end row --> 
                        
                    <!-- table start -->
					<div class="col-md-12">
					    <div class="row">
					        <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
			                    <HeaderTemplate>
                                    <table id="example" class="display table">
                                        <thead>
                                            <tr>
                                                
                                               <th>MachineID</th>
                                                <th>TokenNo</th>
                                                <th>EmpName</th>
                                                <th>DOJ</th>
                                                <th>Department</th>
                                                <th>Old Salary</th>
                                                <th>New Salary</th>
                                                <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                 <tr>
                               <td><%# Eval("MachineID")%></td>
                                    <td><%# Eval("ExistingCode")%></td>
                                    <td><%# Eval("FirstName")%></td>
                                    <td><%# Eval("DOJ")%></td>
                                    <td><%# Eval("DeptName")%></td>
                                    <td><%# Eval("OLD_Salary")%></td>
                                    <td><%# Eval("New_Salary")%></td>
                                        <td>
                                                <%--<asp:LinkButton ID="btnApprvEnquiry_Grid" class="btn btn-success btn-sm fa fa-check"  runat="server" 
                                           Text="" OnCommand="GridApproveEnquiryClick" CommandArgument='<%#Eval("Payroll_EmpNo")+","+ Eval("ExistingCode")%>' CommandName='<%# Eval("MachineID")%>'
                                           CausesValidation="true" OnClientClick="return confirm('Are you sure you want to Approve this Employee details?');">
                                     </asp:LinkButton>--%>
                                     <asp:LinkButton ID="btnCancelEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                        Text="" OnCommand="GridCancelEnquiryClick" CommandArgument='<%#Eval("Payroll_EmpNo")+","+ Eval("ExistingCode")%>' CommandName='<%# Eval("MachineID")%>' 
                                       CausesValidation="true" OnClientClick="return confirm('Are you sure you want to Delete this Employee details?');">
                                     </asp:LinkButton>
                                    </td>
                                       
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater>
					    </div>
					</div>
					<!-- table End -->
                        
                        </div>
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
<!-- end #content -->
</ContentTemplate>
</asp:UpdatePanel>



</asp:Content>

