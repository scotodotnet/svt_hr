﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;

public partial class RptShiftHourBase : System.Web.UI.Page
{
    string ShiftType1 = "";
    string Date = "";
    string Status = "";
    string fmDate = "";
    string Todate = "";
    string Wages = "";
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string ss = "";


    ReportDocument report = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();

    string SSQL = "";

    DataTable AutoDTable = new DataTable();
    string Shift = "";

    DateTime date1;
    DateTime Date2 = new DateTime();


    System.Web.UI.WebControls.DataGrid grid =
                           new System.Web.UI.WebControls.DataGrid();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Report-Shift Hour Based";
            }
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            Status = Request.QueryString["Status"].ToString();
            //Shift = Request.QueryString["Shift"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            fmDate = Request.QueryString["FromDate"].ToString();
            Todate = Request.QueryString["ToDate"].ToString();
            Wages = Request.QueryString["Wages"].ToString();

            Report();
        }
    }

    private void Report()
    {
        string TableName = "";
        if (Status == "Pending")
        {
            TableName = "Employee_Mst_New_Emp";
        }
        else
        {
            TableName = "Employee_Mst";
        }

        SSQL = "";
        SSQL = "Select * from " + TableName + " where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and IsActive='Yes'";
        if (Wages.ToString() != "-Select-")
        {
            SSQL = SSQL + " and Wages='" + Wages + "'";
        }
        DataTable EmpDt = new DataTable();
        EmpDt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (EmpDt.Rows.Count > 0)
        {
            AutoDTable.Columns.Add("S.No");
            AutoDTable.Columns.Add("MachineID");
            AutoDTable.Columns.Add("Name");
            AutoDTable.Columns.Add("DeptName");

            date1 = Convert.ToDateTime(fmDate);
            string dat = Todate;
            Date2 = Convert.ToDateTime(dat);

            int daycount = (int)((Date2 - date1).TotalDays);
            int daysAdded = 0;
            while (daycount >= 0)
            {
                DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded).ToShortDateString());
                AutoDTable.Columns.Add(dayy.ToString("dd/MM/yyyy"));

                daycount -= 1;
                daysAdded += 1;
            }

            AutoDTable.Columns.Add("Total Shift");
            AutoDTable.Columns.Add("Total Non_Shift");

            for (int EmpRw = 0; EmpRw < EmpDt.Rows.Count; EmpRw++)
            {

                decimal Total_Hrs = 0;
                string ShiftName = "";
                decimal EmpWrk_Hrs = 0;
                decimal Total_shift = 0;
                decimal Total_non_shift = 0;

                AutoDTable.NewRow();
                AutoDTable.Rows.Add();

                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["S.No"] = AutoDTable.Rows.Count;
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["MachineID"] = EmpDt.Rows[EmpRw]["MachineID"];
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Name"] = EmpDt.Rows[EmpRw]["FirstName"];
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["DeptName"] = EmpDt.Rows[EmpRw]["DeptName"];

                daycount = (int)((Date2 - date1).TotalDays);
                daysAdded = 0;

                while (daycount >= 0)
                {
                    DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded).ToShortDateString());

                    SSQL = "";
                    SSQL = "Select MachineID, FirstName, Total_Hrs1, TimeIN, TimeOUT, Shift from LogTime_Days where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and MachineID='" + EmpDt.Rows[EmpRw]["MachineID"] + "'";
                    SSQL = SSQL + " and Convert(datetime,Attn_Date_Str,103)=Convert(datetime,'" + dayy.ToString("dd/MM/yyyy") + "',103) and Present='1' and TimeIN!='' and TimeOUT!=''";

                    DataTable Log_DT = new DataTable();
                    Log_DT = objdata.RptEmployeeMultipleDetails(SSQL);

                    if (EmpDt.Rows[EmpRw]["MachineID"].ToString() == "503")
                    {
                        string emp = "0";
                    }

                    if (Log_DT.Rows.Count > 0)
                    {
                        if(Log_DT.Rows[0]["Shift"].ToString()!= "No Shift")
                        {
                            ShiftName = Log_DT.Rows[0]["Shift"].ToString()+"/";

                            Total_shift = Total_shift + 1;
                        }else
                        {
                            ShiftName = "";
                            Total_non_shift = Total_non_shift + 1;
                        }

                        if (Log_DT.Rows[0]["Total_Hrs1"].ToString()!= "")
                        {
                            DateTime Emp_Hr_DT = Convert.ToDateTime(Log_DT.Rows[0]["Total_Hrs1"].ToString());

                            EmpWrk_Hrs = Convert.ToDecimal(Convert.ToString(Emp_Hr_DT.Hour+"."+Emp_Hr_DT.Minute));

                            Total_Hrs = Total_Hrs + EmpWrk_Hrs;
                        }

                        if (ShiftName == "")
                        {
                            AutoDTable.Rows[AutoDTable.Rows.Count - 1][dayy.ToString("dd/MM/yyyy")] = "<b>"+ EmpWrk_Hrs+"</b>";
                        }
                        else
                            AutoDTable.Rows[AutoDTable.Rows.Count - 1][dayy.ToString("dd/MM/yyyy")] = ShiftName + EmpWrk_Hrs;

                    }
                    else
                    {
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1][dayy.ToString("dd/MM/yyyy")] = "";
                    }

                    daycount -= 1;
                    daysAdded += 1;
                }

                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Total Shift"] = Total_shift;

                if (Convert.ToInt32(Total_Hrs.ToString().Split('.').LastOrDefault()) >= 60)
                {
                    Total_Hrs = Convert.ToDecimal(Convert.ToInt32(Total_Hrs.ToString().Split('.').FirstOrDefault())+(Convert.ToDecimal(Total_Hrs.ToString().Split('.').LastOrDefault()) / 60));

                    Total_Hrs = Math.Round(Convert.ToDecimal(Total_Hrs),2,MidpointRounding.AwayFromZero);
                }

                if (Total_non_shift > 0 )
                {if (Total_non_shift >= 5)
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Total Non_Shift"] = "<b style=color:red;>" + Total_non_shift + "</b>";
                else
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Total Non_Shift"] = "<b>" + Total_non_shift + "</b>";
                }
                else 
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Total Non_Shift"] = Total_non_shift;

            }
        }

        if (AutoDTable.Rows.Count > 0)
        {
            grid.DataSource = AutoDTable;
            grid.DataBind();
            string attachment = "attachment;filename=SHIFT HOUR BASED REPORT.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);
            Response.Write("<table>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='6'>");
            Response.Write("<a style=\"font-weight:bold\">SHIFT HOUR BASED REPORT -" + SessionCcode + "-" + SessionLcode + "-" + fmDate + "-" + Todate + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("</table>");
            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();
        }
    }
}