﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="MstUserRights.aspx.cs" Inherits="MstUserRights" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.select2').select2();
                $('#example').dataTable();
            }
        });
    };
</script>


<asp:UpdatePanel>
<ContentTemplate>
<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Master</a></li>
				<li class="active">User Rights</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">User Rights </h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">User Rights</h4>
                        </div>
                        <div class="panel-body">
                        <!-- begin row -->
                          <div class="row">
                           <!-- begin col-4 -->
                              <div class="form-group col-md-3">
						        <label for="exampleInputName">User Name</label>
						        <asp:DropDownList ID="txtUserName" runat="server" class="form-control select2" style="Width:100%"
						          AutoPostBack="true" OnSelectedIndexChanged="txtUserName_SelectedIndexChanged">
                                 </asp:DropDownList>
					          </div>
                              <!-- end col-4 -->
                              <!-- begin col-4 -->
                              <div class="form-group col-md-4">
						                <label for="exampleInputName">Module Name</label>
						                <asp:DropDownList ID="txtModuleName" runat="server" class="form-control select2" style="Width:100%"
						                 AutoPostBack="true" OnSelectedIndexChanged="txtModuleName_SelectedIndexChanged">
                                        </asp:DropDownList>
					                </div>
                              <!-- end col-4 -->
                              
                             <!-- begin col-4 -->
                              <div class="form-group col-md-4">
						                <label for="exampleInputName">Menu Name</label>
						                <asp:DropDownList ID="txtMenuName" runat="server" class="form-control select2" style="Width:100%" 
						                AutoPostBack="true" OnSelectedIndexChanged="txtMenuName_SelectedIndexChanged">
                                        </asp:DropDownList>
					                </div>
                              <!-- end col-4 -->
                              <div class="form-group col-md-1">
					            <br />
					            <asp:Button ID="btnView" Width="50" Height="30" class="btn-success"  runat="server" Text="View" ValidationGroup="Item_Validate_Field"  OnClick="btnView_Click"/>
					        </div>
                              </div>
                        <!-- end row -->
                        <div class="col-md-12">
				                <div class="row">
				                    <div class="form-group col-md-3">
				                    <asp:CheckBox id="chkAll" runat="server" Text="Select / UnSelect" Visible="true" 
                                            oncheckedchanged="chkAll_CheckedChanged" AutoPostBack="true"/>
				                    </div>
				                </div>
				            </div>
                    <div class="col-md-12">
				        <div class="row">
				            <asp:Panel ID="GVPanel" runat="server" ScrollBars="None" Visible="true">
				                <asp:GridView id="GVModule" runat="server" AutoGenerateColumns="false" 
				                ClientIDMode="Static" class="gvv display table">
				                    <Columns>
				                        <asp:TemplateField  HeaderText="FormID" Visible="false">
				                            <ItemTemplate>
				                                <asp:Label id="FormID" runat="server" Text='<%# Eval("FormID") %>'/>
				                            </ItemTemplate>
				                        </asp:TemplateField>
				                        <asp:BoundField DataField="FormName" HeaderText="Form Name" />
				                        <asp:TemplateField  HeaderText="Add">
				                            <ItemTemplate>
				                                <asp:CheckBox id="chkSelect" runat="server"/>
				                            </ItemTemplate>
				                        </asp:TemplateField>
				                       <%-- <asp:TemplateField  HeaderText="Modify">
				                            <ItemTemplate>
				                                <asp:CheckBox id="chkModify" runat="server"/>
				                            </ItemTemplate>
				                        </asp:TemplateField>
				                        <asp:TemplateField  HeaderText="Delete">
				                            <ItemTemplate>
				                                <asp:CheckBox id="chkDelete" runat="server"/>
				                            </ItemTemplate>
				                        </asp:TemplateField>
				                        <asp:TemplateField  HeaderText="View">
				                            <ItemTemplate>
				                                <asp:CheckBox id="chkView" runat="server"/>
				                            </ItemTemplate>
				                        </asp:TemplateField>
				                        <asp:TemplateField  HeaderText="Approve">
				                            <ItemTemplate>
				                                <asp:CheckBox id="chkApprove" runat="server"/>
				                            </ItemTemplate>
				                        </asp:TemplateField>
				                        <asp:TemplateField  HeaderText="Printout">
				                            <ItemTemplate>
				                                <asp:CheckBox id="chkPrintout" runat="server"/>
				                            </ItemTemplate>
				                        </asp:TemplateField>--%>
				                    </Columns>
				                </asp:GridView>
				            </asp:Panel>
					    </div>
					</div>
                    <!-- Button start -->
                    <div class="col-md-12">
                        <div class="row">
                            <div class="txtcenter">
                                <asp:Button ID="btnSave" class="btn btn-success"  runat="server" Text="Save" ValidationGroup="Validate_Field" OnClick="btnSave_Click"/>
                                <%--<asp:Button ID="Button1" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" runat="server" Text="View" />--%>
                                <asp:Button ID="btnCancel" class="btn btn-danger" runat="server" Text="Cancel" OnClick="btnCancel_Click"/>
                            </div>
                        </div>
                    </div>                    
                    <!-- Button end -->       
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
       </div>
<!-- end #content -->
</ContentTemplate>
</asp:UpdatePanel>



</asp:Content>

