﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;


public partial class DayAttendanceSummary : System.Web.UI.Page
{

    DataTable AutoDataTable = new DataTable();
    string ModeType;
    string Date1;
    string Date2;
    string SSQL;
    string SSQL_OUT;
    DataTable dsEmployee = new DataTable();
    DataTable DataCells = new DataTable();
    BALDataAccess objdata = new BALDataAccess();
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;

    string mIpAddress_IN;
    string mIpAddress_OUT;
    string Machine_ID_Str;
    string Date_Value_Str;
    string Date_Value_Str1;
    int shiftCount = 0;
    DataTable mLocalDS = new DataTable();
    DataTable mLocalDS1 = new DataTable();
    DataTable mEmployeeDS = new DataTable();
    DateTime fromdate;
    DateTime todate;
    string SessionUserType;
    string Division = "";
    string Milltype = "";

    
    TimeSpan ts4 = new TimeSpan();
    string Emp_Total_Work_Time_1= "00:00";
    string[] Time_Minus_Value_Check;

    System.Web.UI.WebControls.DataGrid GridView1 =
                 new System.Web.UI.WebControls.DataGrid();

    protected void Page_Load(object sender, EventArgs e)
    {
        //if (Session["UserId"] == null)
        //{
        //    Response.Redirect("Default.aspx");
        //    Response.Write("Your session expired");
        //}
        //SessionCcode = Session["Ccode"].ToString();
        //SessionLcode = Session["Lcode"].ToString();
        if (SessionAdmin == "2")
        {
            Response.Redirect("DayAttendanceSummary.aspx");

        }



        if (!IsPostBack)
        {

            Page.Title = "Spay Module | Report-Day Attendance Summary";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Dashboard"));
            //li.Attributes.Add("class", "droplink active open");
        }

        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        //SessionAdmin = Session["Isadmin"].ToString();
        //SessionCompanyName = Session["CompanyName"].ToString();
        //SessionLocationName = Session["LocationName"].ToString();
        //SessionUserType = Session["UserType"].ToString();

        //ModeType = Request.QueryString["ModeType"].ToString();
        Date1 = Request.QueryString["Date1"].ToString();
      Division = Request.QueryString["Division"].ToString();
      Milltype = Request.QueryString["Milltype"].ToString();
        //Date2 = Request.QueryString["Date2"].ToString();

        DataTable dtIPaddress = new DataTable();

        //dtIPaddress = objdata.IPAddressForAll(SessionCcode.ToString(), SessionLcode.ToString());
        //if (dtIPaddress.Rows.Count > 0)
        //{
        //    for (int i = 0; i < dtIPaddress.Rows.Count; i++)
        //    {
        //        if (dtIPaddress.Rows[i]["IPMode"].ToString() == "IN")
        //        {
        //            mIpAddress_IN = dtIPaddress.Rows[i]["IPAddress"].ToString();
        //        }
        //        else if (dtIPaddress.Rows[i]["IPMode"].ToString() == "OUT")
        //        {
        //            mIpAddress_OUT = dtIPaddress.Rows[i]["IPAddress"].ToString();
        //        }
        //    }
        //}

        Fill_Multi_timeIN();
        Write_MultiIN();



        GridView1.HeaderStyle.Font.Bold = true;
        GridView1.DataSource = DataCells;
        GridView1.DataBind();
        string attachment = "attachment;filename=DAY ATTENDANCE SUMMARY.xls";
        Response.ClearContent();
        Response.AddHeader("content-disposition", attachment);
        Response.ContentType = "application/ms-excel";

        System.IO.StringWriter stw = new System.IO.StringWriter();
        HtmlTextWriter htextw = new HtmlTextWriter(stw);
        GridView1.RenderControl(htextw);
        Response.Write("<table>");
        //Response.Write("<tr Font-Bold='true' align='center'>");
        //Response.Write("<td font-Bold='true' colspan='12'>");
        //Response.Write("<a style=\"font-weight:bold\">" + SessionCompanyName + "</a>");
        //Response.Write("--");
        //Response.Write("<a style=\"font-weight:bold\">" + SessionLocationName + "</a>");
        //Response.Write("</td>");
        //Response.Write("</tr>");
        Response.Write("<tr Font-Bold='true' align='left'>");
        Response.Write("<td font-Bold='true' colspan='12'>");
        Response.Write("<a style=\"font-weight:bold\"> DAY ATTENDANCE SUMMARY REPORT &nbsp;&nbsp;&nbsp;" + Date1 + "</a>");
        Response.Write("</td>");
        Response.Write("</tr>");
        //Response.Write("<tr Font-Bold='true' align='center'>");
        //Response.Write("<td font-Bold='true' colspan='8'>");
        //Response.Write("<a style=\"font-weight:bold\">" + Date + "</a>");
        //Response.Write("</td>");
        //Response.Write("</tr>");
        Response.Write("</table>");
        Response.Write(stw.ToString());
        Response.End();
        Response.Clear();

    }

    public void Fill_Multi_timeIN()
    {
        try
        {
            //DateTime fromdate = Convert.ToDateTime(Date1);
            //DateTime todate = Convert.ToDateTime(Date2);
            //int dayCount = (int)((todate - fromdate).TotalDays);
            //if (dayCount > 0)
            //{
            //AutoDataTable.Columns.Add("DAY ATTENDANCE SUMMARY");
            //AutoDataTable.NewRow();
            //AutoDataTable.Rows.Add();


            //AutoDataTable.NewRow();
            //AutoDataTable.Rows.Add("DAY ATTENDANCE SUMMARY");
            //AutoDataTable.NewRow();
            //AutoDataTable.Rows.Add();
            //AutoDataTable.Columns.Add("SNo");
            AutoDataTable.Columns.Add("DeptName");
            AutoDataTable.Columns.Add("Designation");
            AutoDataTable.Columns.Add("MachineID");
            AutoDataTable.Columns.Add("MachineID_Encrypt");
            AutoDataTable.Columns.Add("EmpNo");
            AutoDataTable.Columns.Add("ExistingCode");
            AutoDataTable.Columns.Add("FirstName");
            AutoDataTable.Columns.Add("TimeIN1");
            AutoDataTable.Columns.Add("TimeIN2");
            AutoDataTable.Columns.Add("TimeIN3");
            AutoDataTable.Columns.Add("TimeIN4");
            AutoDataTable.Columns.Add("TimeOUT1");
            AutoDataTable.Columns.Add("TimeOUT2");
            AutoDataTable.Columns.Add("TimeOUT3");
            AutoDataTable.Columns.Add("TimeOUT4");





            SSQL = "";
            SSQL = "select isnull(DeptName,'') as [DeptName],Cast(MachineID As int) As MachineID,MachineID_Encrypt";
            SSQL = SSQL + ",EmpNo,isnull(ExistingCode,'') as [ExistingCode]";
            SSQL = SSQL + ",isnull(FirstName,'') + '.'+ isnull(MiddleInitial,'') as [FirstName],isnull(Designation,'')as[Designation]";
            SSQL = SSQL + " from Employee_Mst Where Compcode='" + SessionCcode + "' and LocCode='"+SessionLcode+"'";
            if (Division != "-Select-")
            {
                SSQL = SSQL + " And Division = '" + Division + "'";
            }
            if (Milltype != "0")
            {
                SSQL = SSQL + " And Unit_Type = '" + Milltype + "'";
            }
           
          //  And EmpCatCode='" + Division + "'
            //if (SessionUserType == "2")
            //{
            //    SSQL = SSQL + " And IsNonAdmin='1'";
            //}
            SSQL = SSQL + " Order By DeptName, MachineID";

            dsEmployee = objdata.RptEmployeeMultipleDetails(SSQL);

            if (dsEmployee.Rows.Count <= 0)
            {
                return;
            }
            else
            {
                Int32 DSVAL = 0;
                for (int i = 0; i < dsEmployee.Rows.Count; i++)
                {
                    AutoDataTable.NewRow();
                    AutoDataTable.Rows.Add();

                    AutoDataTable.Rows[DSVAL][0] = dsEmployee.Rows[i]["DeptName"].ToString();
                    AutoDataTable.Rows[DSVAL][1] = dsEmployee.Rows[i]["Designation"].ToString();
                    AutoDataTable.Rows[DSVAL][2] = dsEmployee.Rows[i]["MachineID"].ToString();
                    AutoDataTable.Rows[DSVAL][3] = dsEmployee.Rows[i]["MachineID_Encrypt"].ToString();
                    AutoDataTable.Rows[DSVAL][4] = dsEmployee.Rows[i]["EmpNo"].ToString();
                    AutoDataTable.Rows[DSVAL][5] = dsEmployee.Rows[i]["ExistingCode"].ToString();
                    AutoDataTable.Rows[DSVAL][6] = dsEmployee.Rows[i]["FirstName"].ToString();

                    DSVAL += 1;
                }
            }
            //}
        }
        catch (Exception e)
        {
        }
    }




    public void Write_MultiIN()
    {
        try
        {
            int intI = 1;
            int intK = 1;
            int intCol = 0;


            DataCells.Columns.Add("SNo");
            DataCells.Columns.Add("DeptName");
            DataCells.Columns.Add("MachineNo");
            DataCells.Columns.Add("EmpNo");
            DataCells.Columns.Add("ExistingCode");
            DataCells.Columns.Add("FirstName");
            DataCells.Columns.Add("TimeIN1");
            DataCells.Columns.Add("TimeIN2");
            DataCells.Columns.Add("TimeIN3");
            DataCells.Columns.Add("TimeIN4");
            DataCells.Columns.Add("TimeOUT1");
            DataCells.Columns.Add("TimeOUT2");
            DataCells.Columns.Add("TimeOUT3");
            DataCells.Columns.Add("TimeOUT4");
            DataCells.Columns.Add("Total_Hrs");


            for (intCol = 0; intCol < AutoDataTable.Rows.Count; intCol++)
            {
                DataCells.NewRow();
                DataCells.Rows.Add();

                DataCells.Rows[intCol]["SNo"] = intCol + 1;
                DataCells.Rows[intCol]["DeptName"] = AutoDataTable.Rows[intCol]["DeptName"];
                DataCells.Rows[intCol]["MachineNo"] = AutoDataTable.Rows[intCol]["MachineID"];
                DataCells.Rows[intCol]["EmpNo"] = AutoDataTable.Rows[intCol]["EmpNo"];
                DataCells.Rows[intCol]["ExistingCode"] = AutoDataTable.Rows[intCol]["ExistingCode"];
                DataCells.Rows[intCol]["FirstName"] = AutoDataTable.Rows[intCol]["FirstName"];
            }

            intCol = 5;


            fromdate = Convert.ToDateTime(Date1);
            //todate = Convert.ToDateTime(Date2);
            //int dayCount = (int)((todate - fromdate).TotalDays);

            //int daysAdded = 0;

            intK = 1;
            intI = 1;

            for (int intRow = 0; intRow < AutoDataTable.Rows.Count; intRow++)
            {
                intK = 1;
                int colIndex = intK;
                bool isPresent = false;
                isPresent = false;
                string Machine_ID_Str = "";
                string OT_Week_OFF_Machine_No = "";
                string Date_Value_Str = "";
                string Date_Value_Str1 = "";

                string Time_IN_Str = "";
                string Time_Out_Str = "";
                string Total_Time_get = "";
                Int32 j = 0;
                double time_Check_dbl = 0;
                isPresent = false;
                string INTime = "";
                string Emp_Total_Work_Time_1 = "00:00";
                string OUTTime = "";
                string Employee_Punch = "";


                Machine_ID_Str = AutoDataTable.Rows[intRow][3].ToString();
                Date_Value_Str = string.Format(Date1, "dd-MM-yyyy");

                DateTime Dateval = Convert.ToDateTime(Date_Value_Str).AddDays(1);
                if (Machine_ID_Str == "ODAyNQ==")
                {
                    Machine_ID_Str = "ODAyNQ==";
                }


                if (Machine_ID_Str == "ODAyNg==")
                {
                    Machine_ID_Str = "ODAyNg==";
                }

                Date_Value_Str1 = string.Format(Convert.ToString(Dateval), "dd-MM-yyyy");

                //Shift Check shift1
                string Final_Shift = "";
                DataTable Shift_DS = new DataTable();
                Int32 k = 0;
                bool Shift_Check_blb = false;
                string Shift_Start_Time = null;
                string Shift_End_Time = null;
                string Employee_Time = "";
                DateTime ShiftdateStartIN = default(DateTime);
                DateTime ShiftdateEndIN = default(DateTime);
                string Final_InTime = "";
                string Final_OutTime = "";
                string From_Time_Str = "";
                string To_Time_Str = "";

                double Total_Hrs = 0;
                string First_IN = "";
                string Last_OUT = "";

                DateTime EmpdateIN = default(DateTime);
                DayOfWeek day = fromdate.DayOfWeek;
                string WeekofDay = day.ToString();

                SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                SSQL = SSQL + " And TimeIN >='" + fromdate.AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + fromdate.AddDays(1).ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";
                mLocalDS = objdata.RptEmployeeMultipleDetails(SSQL);

                if (mLocalDS.Rows.Count != 0)
                {
                    SSQL = "Select * from Shift_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' ";
                    Shift_DS = objdata.RptEmployeeMultipleDetails(SSQL);
                    Shift_Check_blb = false;
                    Final_InTime = mLocalDS.Rows[0]["TimeIN"].ToString();
                    for (k = 0; k < Shift_DS.Rows.Count; k++)
                    {

                        string a = Shift_DS.Rows[k]["StartIN_Days"].ToString();
                        int b = Convert.ToInt16(a.ToString());
                        Shift_Start_Time = fromdate.AddDays(b).ToString("dd/MM/yyyy") + " " + Shift_DS.Rows[k]["StartIN"].ToString();
                        string a1 = Shift_DS.Rows[k]["EndIN_Days"].ToString();
                        int b1 = Convert.ToInt16(a1.ToString());
                        Shift_End_Time = fromdate.AddDays(b1).ToString("dd/MM/yyyy") + " " + Shift_DS.Rows[k]["EndIN"].ToString();

                        ShiftdateStartIN = Convert.ToDateTime(Shift_Start_Time.ToString());
                        ShiftdateEndIN = Convert.ToDateTime(Shift_End_Time.ToString());

                        EmpdateIN = Convert.ToDateTime(Final_InTime.ToString());

                        if (EmpdateIN >= ShiftdateStartIN & EmpdateIN <= ShiftdateEndIN)
                        {
                            Final_Shift = Shift_DS.Rows[k]["ShiftDesc"].ToString();
                            Shift_Check_blb = true;
                            break;
                        }
                    }

                    if (Shift_Check_blb == false)
                    {

                        SSQL_OUT = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                        SSQL_OUT = SSQL_OUT + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                        SSQL_OUT = SSQL_OUT + " And TimeOUT >='" + fromdate.ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + fromdate.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT Asc";

                    }
                    else
                    {
                        if (Final_Shift == "SHIFT1")
                        {
                            SSQL_OUT = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                            SSQL_OUT = SSQL_OUT + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                            SSQL_OUT = SSQL_OUT + " And TimeOUT >='" + fromdate.ToString("yyyy/MM/dd") + " " + "06:30' And TimeOUT <='" + fromdate.AddDays(1).ToString("yyyy/MM/dd") + " " + "03:00' Order by TimeOUT Asc";
                        }
                        else
                        {
                            SSQL_OUT = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                            SSQL_OUT = SSQL_OUT + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                            SSQL_OUT = SSQL_OUT + " And TimeOUT >='" + fromdate.ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + fromdate.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT Asc";

                        }
                    }

                }
                else
                {
                    SSQL_OUT = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                    SSQL_OUT = SSQL_OUT + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    SSQL_OUT = SSQL_OUT + " And TimeOUT >='" + fromdate.ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + fromdate.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT Asc";
                }


                //if (Final_Shift == "SHIFT1")
                //{
                //    SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                //    SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                //    SSQL = SSQL + " And TimeIN >='" + fromdate.AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + fromdate.AddDays(1).ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";
                //    mLocalDS = objdata.ReturnMultipleValue(SSQL);
                //}

                //else
                //{
                //    SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                //    SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                //    SSQL = SSQL + " And TimeIN >='" + fromdate.AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + fromdate.AddDays(1).ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";
                //    mLocalDS = objdata.ReturnMultipleValue(SSQL);
                //}


                mLocalDS1 = objdata.RptEmployeeMultipleDetails(SSQL_OUT);
                if (mLocalDS.Rows.Count >= 1)
                {

                    for (int ival = 0; ival < mLocalDS.Rows.Count; ival++)
                    {
                        int ival1 = 6;
                        if (ival == 0)
                        {

                            DataCells.Rows[intRow][ival1] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[ival]["TimeIN"]);

                            First_IN = string.Format("{0:HH:mm}", mLocalDS.Rows[ival]["TimeIN"]);

                        }
                        else if (ival == 1)
                        {
                            ival1 = 6 + ival;
                            DataCells.Rows[intRow][ival1] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[ival]["TimeIN"]);


                        }
                        else if (ival == 2)
                        {
                            ival1 = 6 + ival;
                            DataCells.Rows[intRow][ival1] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[ival]["TimeIN"]);


                        }
                        else if (ival == 3)
                        {
                            ival1 = 6 + ival;
                            DataCells.Rows[intRow][ival1] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[ival]["TimeIN"]);


                        }
                    }
                }

                if (mLocalDS1.Rows.Count >= 1)
                {
                    for (int ival = 0; ival < mLocalDS1.Rows.Count; ival++)
                    {
                        int ival1 = 10;

                        if (ival == 0)
                        {
                            //  ival1 = ival1 ;
                            DataCells.Rows[intRow][ival1] = string.Format("{0:hh:mm tt}", mLocalDS1.Rows[ival]["TimeOUT"]);
                            Last_OUT = string.Format("{0:HH:mm}", mLocalDS1.Rows[ival]["TimeOUT"]);

                        }
                        else if (ival == 1)
                        {
                            ival1 = 10 + ival;
                            DataCells.Rows[intRow][ival1] = string.Format("{0:hh:mm tt}", mLocalDS1.Rows[ival]["TimeOUT"]);


                        }
                        else if (ival == 2)
                        {
                            ival1 = 10 + ival;
                            DataCells.Rows[intRow][ival1] = string.Format("{0:hh:mm tt}", mLocalDS1.Rows[ival]["TimeOUT"]);


                        }
                        else if (ival == 3)
                        {
                            ival1 = 10 + ival;
                            DataCells.Rows[intRow][ival1] = string.Format("{0:hh:mm tt}", mLocalDS1.Rows[ival]["TimeOUT"]);
                        }
                    }
                }

                if (First_IN == "" || Last_OUT == "")
                {
                    time_Check_dbl = 0;
                }
                else
                {
                    if (mLocalDS.Rows.Count <= 0)
                    {
                        Time_IN_Str = "";
                        INTime = "";
                    }
                    else
                    {
                        DateTime datetime1 = Convert.ToDateTime(mLocalDS.Rows[0][0].ToString());
                        INTime = datetime1.ToString("hh:mm tt");
                        //INTime = string.Format("{0:hh:mm tt}", mLocalDS.Rows[0][0].ToString());
                    }

                    if (mLocalDS1.Rows.Count <= 0)
                    {
                        Time_Out_Str = "";
                        OUTTime = "";
                    }
                    else
                    {
                        DateTime datetime = Convert.ToDateTime(mLocalDS1.Rows[0][0].ToString());
                        OUTTime = datetime.ToString("hh:mm tt");
                        //OUTTime = string.Format(Convert.ToString(datetime), "HH:mm:ss tt");
                        //OUTTime = string.Format("{0:hh:mm tt}", mLocalDS1.Rows[0][0].ToString());

                    }

                    if (mLocalDS.Rows.Count == 0)
                    {
                        Employee_Punch = "";
                    }
                    else
                    {
                        Employee_Punch = mLocalDS.Rows[0][0].ToString();
                    }
                    if (mLocalDS.Rows.Count > 1)
                    {
                        for (int tin = 0; tin < mLocalDS.Rows.Count; tin++)
                        {
                            Time_IN_Str = mLocalDS.Rows[tin][0].ToString();
                            if (mLocalDS1.Rows.Count > tin)
                            {
                                Time_Out_Str = mLocalDS1.Rows[tin][0].ToString();
                            }
                            else if (mLocalDS1.Rows.Count > mLocalDS.Rows.Count)
                            {
                                Time_Out_Str = mLocalDS1.Rows[mLocalDS1.Rows.Count - 1][0].ToString();
                            }
                            else
                            {
                                Time_Out_Str = "";
                            }
                            TimeSpan ts4 = new TimeSpan();
                            ts4 = Convert.ToDateTime(string.Format("{0:hh\\:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
                            if (mLocalDS.Rows.Count <= 0)
                            {
                                Time_IN_Str = "";
                            }
                            else
                            {
                                Time_IN_Str = mLocalDS.Rows[tin][0].ToString();
                            }
                            if (Time_IN_Str == "" || Time_Out_Str == "")
                            {
                                time_Check_dbl = time_Check_dbl;
                            }
                            else
                            {

                                DateTime date1 = System.Convert.ToDateTime(Time_IN_Str);
                                DateTime date2 = System.Convert.ToDateTime(Time_Out_Str);
                                TimeSpan ts = new TimeSpan();
                                ts = date2.Subtract(date1);

                                //& ":" & Trim(ts.Minutes)
                                Total_Time_get = (ts.Hours).ToString();
                                ts4 = ts4.Add(ts);

                                //OT Time Get
                                Emp_Total_Work_Time_1 = (ts4.Hours) + ":" + (ts4.Minutes);
                                Emp_Total_Work_Time_1 = (ts4.Hours) + ":" + (ts4.Minutes);

                                if (Left_Val(ts4.Minutes.ToString(), 1) == "-" || Left_Val(ts4.Hours.ToString(), 1) == "-" || Emp_Total_Work_Time_1 == "0:-1")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";

                                }
                                if (Left_Val(Total_Time_get, 1) == "-")
                                {
                                    date2 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                                    ts = date2.Subtract(date1);
                                    ts = date2.Subtract(date1);
                                    Total_Time_get = (ts.Hours).ToString();
                                    time_Check_dbl = double.Parse(Total_Time_get);
                                    Emp_Total_Work_Time_1 = (ts.Hours) + ":" + (ts.Minutes).ToString();
                                    Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                                    if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" || Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    {
                                        Emp_Total_Work_Time_1 = "00:00";
                                    }
                                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" || Emp_Total_Work_Time_1 == "0:1" || Emp_Total_Work_Time_1 == "0:-3")
                                    {
                                        Emp_Total_Work_Time_1 = "00:00";
                                    }

                                }
                                else
                                {
                                    time_Check_dbl = time_Check_dbl + double.Parse(Total_Time_get);
                                }
                            }

                            //break;

                        }//For End
                    }
                    else
                    {
                        TimeSpan ts4 = new TimeSpan();
                        ts4 = Convert.ToDateTime(string.Format("{0:hh\\:mm}", Emp_Total_Work_Time_1)).TimeOfDay;

                        if (mLocalDS.Rows.Count <= 0)
                        {
                            Time_IN_Str = "";
                        }
                        else
                        {
                            Time_IN_Str = mLocalDS.Rows[0][0].ToString();
                        }
                        for (int tout = 0; tout < mLocalDS1.Rows.Count; tout++)
                        {
                            if (mLocalDS1.Rows.Count <= 0)
                            {
                                Time_Out_Str = "";
                            }
                            else
                            {
                                Time_Out_Str = mLocalDS1.Rows[0][0].ToString();
                            }

                            //break;

                        }
                        //Emp_Total_Work_Time
                        if (Time_IN_Str == "" || Time_Out_Str == "")
                        {
                            time_Check_dbl = 0;
                        }
                        else
                        {
                            DateTime date1 = System.Convert.ToDateTime(Time_IN_Str);
                            DateTime date2 = System.Convert.ToDateTime(Time_Out_Str);
                            TimeSpan ts = new TimeSpan();
                            ts = date2.Subtract(date1);
                            ts = date2.Subtract(date1);
                            Total_Time_get = (ts.Hours).ToString();
                            ts4 = ts4.Add(ts);
                            //OT Time Get
                            Emp_Total_Work_Time_1 = (ts4.Hours) + ":" + (ts4.Minutes);
                            Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                            if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" || Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                            {
                                Emp_Total_Work_Time_1 = "00:00";
                            }
                            if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" || Emp_Total_Work_Time_1 == "0:-1" || Emp_Total_Work_Time_1 == "0:-3")
                            {
                                Emp_Total_Work_Time_1 = "00:00";
                            }
                            if (Left_Val(Total_Time_get, 1) == "-")
                            {
                                //For Night shift Employee
                                date2 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                                ts = date2.Subtract(date1);
                                ts = date2.Subtract(date1);
                                Total_Time_get = ts.Hours.ToString();
                                time_Check_dbl = double.Parse(Total_Time_get);
                                Emp_Total_Work_Time_1 = (ts.Hours) + ":" + (ts.Minutes);
                                Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" || Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";

                                    time_Check_dbl = 0;
                                }
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" || Emp_Total_Work_Time_1 == "0:-1" || Emp_Total_Work_Time_1 == "0:-3")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";

                                    time_Check_dbl = 0;
                                }
                            }
                            else
                            {
                                time_Check_dbl = double.Parse(Total_Time_get);
                            }
                        }
                    }
                }
                if (Convert.ToDateTime(Emp_Total_Work_Time_1) > Convert.ToDateTime("0:00"))
                {
                    DataCells.Rows[intRow]["Total_Hrs"] = Emp_Total_Work_Time_1;
                }else
                {
                    DataCells.Rows[intRow]["Total_Hrs"] = "";
                }
                
                colIndex += shiftCount;
                intK += 1;
                intI += 1;
            }
        }
        catch (Exception ex)
        {
        }
    }

    public string Left_Val(string Value, int Length)
    {

        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }
    protected void UploadDataTableToExcel(DataTable dtRecords)
    {
        string value0 = "DAY ATTENDANCE SUMMARY" + "-" + fromdate.AddDays(0);

        string XlsPath = Server.MapPath(@"~/Add_data/DAY ATTENDANCE SUMMARY.xls");
        string attachment = string.Empty;
        if (XlsPath.IndexOf("\\") != -1)
        {
            string[] strFileName = XlsPath.Split(new char[] { '\\' });
            attachment = "attachment; filename=" + strFileName[strFileName.Length - 1];
        }
        else
            attachment = "attachment; filename=" + XlsPath;
        try
        {
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/vnd.ms-excel";


            //Response.Write("<table>");
            //Response.Write("<tr align='Center'>");
            //Response.Write("<td colspan='15'>");
            //Response.Write("" + value0 + " ");
            //Response.Write("</td>");
            //Response.Write("</tr>");
            //Response.Write("</table>");

            string tab = string.Empty;

            foreach (DataColumn datacol in dtRecords.Columns)
            {
                Response.Write(tab + datacol.ColumnName);
                tab = "\t";
            }
            Response.Write("\n");

            foreach (DataRow dr in dtRecords.Rows)
            {
                tab = "";
                for (int j = 0; j < dtRecords.Columns.Count; j++)
                {
                    Response.Write(tab + Convert.ToString(dr[j]));
                    tab = "\t";
                }

                Response.Write("\n");
            }

            Response.End();
        }
        catch (Exception ex)
        {
            //Response.Write(ex.Message);
        }
    }

}
