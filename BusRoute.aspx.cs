﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Globalization;


public partial class BusRoute : System.Web.UI.Page
{


    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string Division;

    string SSQL;
    DataTable DataCell = new DataTable();
    DataTable dt=new DataTable();
    DataTable dt1 = new DataTable();
    
    BALDataAccess objdata = new BALDataAccess();
    System.Web.UI.WebControls.DataGrid grid =
                          new System.Web.UI.WebControls.DataGrid();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {

            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report- Bus Route";
                //li.Attributes.Add("class", "droplink active open");
            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            //SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            Division = Request.QueryString["Division"].ToString();





            if (SessionUserType == "2")
            {
                 NonAdminBusRoute();
            }

            else
            {
                AdminBusRoute();
            }
        }
    }


    public void AdminBusRoute()
    {

        DataCell.Columns.Add("SI.No");
        DataCell.Columns.Add("Name");
        DataCell.Columns.Add("Token No");
        DataCell.Columns.Add("Department");
        DataCell.Columns.Add("Designation");
        DataCell.Columns.Add("DOJ");
        DataCell.Columns.Add("Vehicle Type");
        DataCell.Columns.Add("Route No");
        DataCell.Columns.Add("Village");
        DataCell.Columns.Add("Native Place");
        DataCell.Columns.Add("Remarks");

        SSQL = "Select * from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " And Wages='Regular'";

        if (Division != "-Select-")
        {
            SSQL = SSQL + " and EM.Division='" + Division + "' ";
        }

        SSQL = SSQL + " And IsActive='Yes'";
        SSQL = SSQL + " Order by ExistingCode Asc";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count != 0)
        {
            int sno = 1;

            for (int m = 0; m < dt.Rows.Count; m++)
            {
                DataCell.NewRow();
                DataCell.Rows.Add();


            


                DataCell.Rows[m]["SI.No"] = sno;
                DataCell.Rows[m]["Name"] = dt.Rows[m]["FirstName"].ToString();
                DataCell.Rows[m]["Token No"] = dt.Rows[m]["ExistingCode"].ToString();
                DataCell.Rows[m]["Department"] = dt.Rows[m]["DeptName"].ToString();
                DataCell.Rows[m]["Designation"] = dt.Rows[m]["Designation"].ToString();
                DataCell.Rows[m]["DOJ"] = Convert.ToDateTime(dt.Rows[m]["DOJ"]).ToString("dd/MM/yyyy");
                DataCell.Rows[m]["Vehicle Type"] = dt.Rows[m]["Vehicles_Type"].ToString();
                DataCell.Rows[m]["Route No"] = dt.Rows[m]["BusNo"].ToString();
                DataCell.Rows[m]["Village"] = dt.Rows[m]["BusRoute"].ToString();
                DataCell.Rows[m]["Native Place"] = dt.Rows[m]["Permanent_Dist"].ToString();
                DataCell.Rows[m]["Remarks"] = "";


                sno += 1;
            }


            SSQL = "Select * from Company_Mst ";
            dt1 = objdata.RptEmployeeMultipleDetails(SSQL);
            string name = dt1.Rows[0]["CompName"].ToString();

            grid.DataSource = DataCell;
            grid.DataBind();
            string attachment = "attachment;filename=Bus Route.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);
            Response.Write("<table border='1'>");

            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='11'>");
            Response.Write("<a style=\"font-weight:bold\">" + name + " - " + SessionLcode + "</a>");

            Response.Write("</td>");
            Response.Write("</tr>");

            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='11'>");
            Response.Write("<a style=\"font-weight:bold\">Employee's Bus Route Details </a>");

            Response.Write("</td>");
            Response.Write("</tr>");

            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='11'>");
            Response.Write("<a style=\"font-weight:bold\"></a>");

            Response.Write("</td>");
            Response.Write("</tr>");

            Response.Write("<tr Font-Bold='true' align='center'>");
            for (int k = 0; k < DataCell.Columns.Count; k++)
            {

                Response.Write("<td colspan='1'>");
                Response.Write("<a style=\"font-weight:bold\">" + DataCell.Columns[k] + " </a>");

                Response.Write("</td>");

            }
            Response.Write("</tr>");

            for (int n = 0; n < DataCell.Rows.Count; n++)
            {


                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='1'>");
                Response.Write("<a>" + DataCell.Rows[n]["SI.No"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td colspan='1'>");
                Response.Write("<a>" + DataCell.Rows[n]["Name"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td colspan='1'>");
                Response.Write("<a>" + DataCell.Rows[n]["Token No"].ToString() + " </a>");
                Response.Write("</td>");
                Response.Write("<td colspan='1'>");
                Response.Write("<a>" + DataCell.Rows[n]["Department"].ToString() + " </a>");
                Response.Write("</td>");
                Response.Write("<td colspan='1'>");
                Response.Write("<a>" + DataCell.Rows[n]["Designation"].ToString() + " </a>");
                Response.Write("</td>");
                Response.Write("<td colspan='1'>");
                Response.Write("<a>" + DataCell.Rows[n]["DOJ"].ToString() + " </a>");
                Response.Write("</td>");
                Response.Write("<td colspan='1'>");
                Response.Write("<a>" + DataCell.Rows[n]["Vehicle Type"].ToString() + " </a>");
                Response.Write("</td>");
                Response.Write("<td colspan='1'>");
                Response.Write("<a>" + DataCell.Rows[n]["Route No"].ToString() + " </a>");
                Response.Write("</td>");
                Response.Write("<td colspan='1'>");
                Response.Write("<a>" + DataCell.Rows[n]["Village"].ToString() + " </a>");
                Response.Write("</td>");
                Response.Write("<td colspan='1'>");
                Response.Write("<a>" + DataCell.Rows[n]["Native Place"].ToString() + " </a>");
                Response.Write("</td>");
                Response.Write("<td colspan='1'>");
                Response.Write("<a>" + DataCell.Rows[n]["Remarks"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("</tr>");
            }
            Response.Write("</table>");
            //Response.Write(stw.ToString());
            Response.End();
            Response.Clear();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Found..');", true);
        }
    }


    public void NonAdminBusRoute()
    {
        DataCell.Columns.Add("SI.No");
        DataCell.Columns.Add("Name");
        DataCell.Columns.Add("Token No");
        DataCell.Columns.Add("Department");
        DataCell.Columns.Add("Designation");
        DataCell.Columns.Add("DOJ");
        DataCell.Columns.Add("Vehicle Type");
        DataCell.Columns.Add("Route No");
        DataCell.Columns.Add("Village");
        DataCell.Columns.Add("Native Place");
        DataCell.Columns.Add("Remarks");

        SSQL = "Select * from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " And Wages='Regular' and Eligible_PF ='1'";

        if (Division != "-Select-")
        {
            SSQL = SSQL + " and EM.Division='" + Division + "' ";
        }
       
        SSQL = SSQL + " And IsActive='Yes'";
        SSQL = SSQL + " Order by ExistingCode Asc";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count != 0)
        {
            int sno = 1;

            for (int m = 0; m < dt.Rows.Count; m++)
            {
                DataCell.NewRow();
                DataCell.Rows.Add();

                DataCell.Rows[m]["SI.No"] = sno;
                DataCell.Rows[m]["Name"] = dt.Rows[m]["FirstName"].ToString();
                DataCell.Rows[m]["Token No"] = dt.Rows[m]["ExistingCode"].ToString();
                DataCell.Rows[m]["Department"] = dt.Rows[m]["DeptName"].ToString();
                DataCell.Rows[m]["Designation"] = dt.Rows[m]["Designation"].ToString();
                DataCell.Rows[m]["DOJ"] = Convert.ToDateTime(dt.Rows[m]["DOJ"]).ToString("dd/MM/yyyy");
                DataCell.Rows[m]["Vehicle Type"] = dt.Rows[m]["Vehicles_Type"].ToString();
                DataCell.Rows[m]["Route No"] = dt.Rows[m]["BusNo"].ToString();
                DataCell.Rows[m]["Village"] = dt.Rows[m]["BusRoute"].ToString();
                DataCell.Rows[m]["Native Place"] = dt.Rows[m]["Permanent_Dist"].ToString();
                DataCell.Rows[m]["Remarks"] = "";

                sno += 1;
            }


            SSQL = "Select * from Company_Mst ";
            dt1 = objdata.RptEmployeeMultipleDetails(SSQL);
            string name = dt1.Rows[0]["CompName"].ToString();

            grid.DataSource = DataCell;
            grid.DataBind();
            string attachment = "attachment;filename=Bus Route.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);
            Response.Write("<table border='1'>");

            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='11'>");
            Response.Write("<a style=\"font-weight:bold\">" + name + " - " + SessionLcode + "</a>");

            Response.Write("</td>");
            Response.Write("</tr>");

            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='11'>");
            Response.Write("<a style=\"font-weight:bold\">Employee's Bus Route Details </a>");

            Response.Write("</td>");
            Response.Write("</tr>");

            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='11'>");
            Response.Write("<a style=\"font-weight:bold\"></a>");

            Response.Write("</td>");
            Response.Write("</tr>");

            Response.Write("<tr Font-Bold='true' align='center'>");
            for (int k = 0; k < DataCell.Columns.Count; k++)
            {

                Response.Write("<td colspan='1'>");
                Response.Write("<a style=\"font-weight:bold\">" + DataCell.Columns[k] + " </a>");

                Response.Write("</td>");

            }
            Response.Write("</tr>");

            for (int n = 0; n < DataCell.Rows.Count; n++)
            {


                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='1'>");
                Response.Write("<a>" + DataCell.Rows[n]["SI.No"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td colspan='1'>");
                Response.Write("<a>" + DataCell.Rows[n]["Name"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("<td colspan='1'>");
                Response.Write("<a>" + DataCell.Rows[n]["Token No"].ToString() + " </a>");
                Response.Write("</td>");
                Response.Write("<td colspan='1'>");
                Response.Write("<a>" + DataCell.Rows[n]["Department"].ToString() + " </a>");
                Response.Write("</td>");
                Response.Write("<td colspan='1'>");
                Response.Write("<a>" + DataCell.Rows[n]["Designation"].ToString() + " </a>");
                Response.Write("</td>");
                Response.Write("<td colspan='1'>");
                Response.Write("<a>" + DataCell.Rows[n]["DOJ"].ToString() + " </a>");
                Response.Write("</td>");
                Response.Write("<td colspan='1'>");
                Response.Write("<a>" + DataCell.Rows[n]["Vehicle Type"].ToString() + " </a>");
                Response.Write("</td>");
                Response.Write("<td colspan='1'>");
                Response.Write("<a>" + DataCell.Rows[n]["Route No"].ToString() + " </a>");
                Response.Write("</td>");
                Response.Write("<td colspan='1'>");
                Response.Write("<a>" + DataCell.Rows[n]["Village"].ToString() + " </a>");
                Response.Write("</td>");
                Response.Write("<td colspan='1'>");
                Response.Write("<a>" + DataCell.Rows[n]["Native Place"].ToString() + " </a>");
                Response.Write("</td>");
                Response.Write("<td colspan='1'>");
                Response.Write("<a>" + DataCell.Rows[n]["Remarks"].ToString() + " </a>");
                Response.Write("</td>");

                Response.Write("</tr>");
            }
            Response.Write("</table>");
            //Response.Write(stw.ToString());
            Response.End();
            Response.Clear();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Records Found..');", true);
        }
    }

}
