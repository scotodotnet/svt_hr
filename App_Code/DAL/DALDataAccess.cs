﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for DALDataAccess
/// </summary>
namespace Altius.DataAccessLayer.DALDataAccess
{
    public class DALDataAccess : BaseDataAccess
    {

        public DataTable CheckUser_Login(string CompanyCode, string LocationCode, string UserName, string usert)
        {
            System.Data.SqlClient.SqlParameter[] UserRights = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", CompanyCode),
                new System.Data.SqlClient.SqlParameter("@Lcode", LocationCode),
                new System.Data.SqlClient.SqlParameter("@uname", UserName),
                new System.Data.SqlClient.SqlParameter("@utype", usert),
                //new System.Data.SqlClient.SqlParameter("@SNo", ss)
            };
            DataTable dt = SQL.ExecuteDatatable("CheckUserCreation_SP", UserRights);
            return dt;
        }



        public DataTable CheckAdminRights(string SessionCcode, string SessionLcode, string UserName)
        {
            System.Data.SqlClient.SqlParameter[] AdminRights = 
            {
                new System.Data.SqlClient.SqlParameter("@CompanyCode", SessionCcode),
                new System.Data.SqlClient.SqlParameter("@LocationCode", SessionLcode),
                new System.Data.SqlClient.SqlParameter("@UserName", UserName)
            };
            DataTable dt = SQL.ExecuteDatatable("CheckAdminRights_SP", AdminRights);
            return dt;
        }



        public string UserCreationCcodeCounting(string CompanyCode, string LocationCode)
        {
            System.Data.SqlClient.SqlParameter[] CompanyAndLocation = 
            {
                new System.Data.SqlClient.SqlParameter("@CompanyCode", CompanyCode),
                new System.Data.SqlClient.SqlParameter("@LocationCode", LocationCode),
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "UserCreationCcodeCounting_SP", CompanyAndLocation);
            return str;

        }

        public DataTable UserCreationRegistration(UserCreationClass objUsercreation)
        {
            System.Data.SqlClient.SqlParameter[] user = 
            {
                new System.Data.SqlClient.SqlParameter("@UserName",objUsercreation.UserName),
                new System.Data.SqlClient.SqlParameter("@Password",objUsercreation.Password),
                new System.Data.SqlClient.SqlParameter("@UserType",objUsercreation.UserCode),
                new System.Data.SqlClient.SqlParameter("@Ccode",objUsercreation.Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode",objUsercreation.Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("UserCreationRegistration_SP", user);
            return dt;
        }

        public DataTable CheckEmpType_AlreadyExist(string Employeetype)
        {
            System.Data.SqlClient.SqlParameter[] Designchk = 
            {
                new System.Data.SqlClient.SqlParameter("@DesignName", Employeetype)
            };
            DataTable dt = SQL.ExecuteDatatable("Desgination_Check", Designchk);
            return dt;
        }

        public DataTable DesginationRegister(string Employeetype)
        {
            System.Data.SqlClient.SqlParameter[] Design = 
            {
                new System.Data.SqlClient.SqlParameter("@DesignName", Employeetype)
            };
            DataTable dt = SQL.ExecuteDatatable("Desgination_Register", Design);
            return dt;
        }

        public DataTable EditDesignation(int DesignNo)
        {
            System.Data.SqlClient.SqlParameter[] user = 
            {
                new System.Data.SqlClient.SqlParameter("@DesignNo",DesignNo),
            };
            DataTable dt = SQL.ExecuteDatatable("DesignationEdit_SP", user);
            return dt;
        }
        public DataTable EditEmployeeType(int TypeID)
        {
            System.Data.SqlClient.SqlParameter[] user = 
            {
                new System.Data.SqlClient.SqlParameter("@TypeID",TypeID),
            };
            DataTable dt = SQL.ExecuteDatatable("EmployeeTypeEdit_SP", user);
            return dt;
        }
        public DataTable EditWagesType(string WgsTypeNo)
        {
            System.Data.SqlClient.SqlParameter[] user = 
            {
                new System.Data.SqlClient.SqlParameter("@WagesTypeName",WgsTypeNo),
            };
            DataTable dt = SQL.ExecuteDatatable("WagesTypeEdit_SP", user);
            return dt;
        }

        public DataTable CheckEmployeeDesignation(int DesignNo)
        {
            System.Data.SqlClient.SqlParameter[] user = 
            {
                new System.Data.SqlClient.SqlParameter("@DesignNo",DesignNo),
            };
            DataTable dt = SQL.ExecuteDatatable("DesignationChechEmp_SP", user);
            return dt;
        }
        public DataTable CheckingEmployeeType(string TypeName)
        {
            System.Data.SqlClient.SqlParameter[] user = 
            {
                new System.Data.SqlClient.SqlParameter("@TypeName",TypeName),
            };
            DataTable dt = SQL.ExecuteDatatable("TypeNameEmployee_SP", user);
            return dt;
        }

        public DataTable CheckingWagesType(string TypeName)
        {
            System.Data.SqlClient.SqlParameter[] user = 
            {
                new System.Data.SqlClient.SqlParameter("@TypeName",TypeName),
            };
            DataTable dt = SQL.ExecuteDatatable("TypeNameWages_SP", user);
            return dt;
        }

        public DataTable DeleteDesgination(int DesignNo)
        {
            System.Data.SqlClient.SqlParameter[] user = 
            {
                new System.Data.SqlClient.SqlParameter("@DesignNo",DesignNo),
            };
            DataTable dt = SQL.ExecuteDatatable("DeleteDesignation_SP", user);
            return dt;
        }


        public DataTable DeleteEmployeeeType(int TypeID)
        {
            System.Data.SqlClient.SqlParameter[] user = 
            {
                new System.Data.SqlClient.SqlParameter("@TypeID",TypeID),
            };
            DataTable dt = SQL.ExecuteDatatable("DeleteEmployeeType_SP", user);
            return dt;
        }


        public DataTable DeleteWagesType(string WgsTypeNo)
        {
            System.Data.SqlClient.SqlParameter[] user = 
            {
                new System.Data.SqlClient.SqlParameter("@WagesTypeName",WgsTypeNo),
            };
            DataTable dt = SQL.ExecuteDatatable("DeleteWagesType_SP", user);
            return dt;
        }

        public DataTable DesginationDisplay()
        {
            System.Data.SqlClient.SqlParameter[] user = 
            {
              
            };
            DataTable dt = SQL.ExecuteDatatable("DesginationDisplay_SP", user);
            return dt;
        }



        public DataTable EmployeeTypeDisplay()
        {
            System.Data.SqlClient.SqlParameter[] user = 
            {
              
            };
            DataTable dt = SQL.ExecuteDatatable("EmployeeTypeDisplay_SP", user);
            return dt;
        }

        public DataTable WagesTypeDisplay()
        {
            System.Data.SqlClient.SqlParameter[] user = 
            {
              
            };
            DataTable dt = SQL.ExecuteDatatable("WagesTypeDisplay_SP", user);
            return dt;
        }

        public DataTable DesignationUpdate(string DesignName, int ss)
        {
            System.Data.SqlClient.SqlParameter[] Design = 
            {
                new System.Data.SqlClient.SqlParameter("@DesignName", DesignName),
                new System.Data.SqlClient.SqlParameter("@DesignNo", ss)
            };
            DataTable dt = SQL.ExecuteDatatable("Desgination_Update", Design);
            return dt;
        }




        public DataTable WagesTypeUpdate(string TypeName, int ss)
        {
            System.Data.SqlClient.SqlParameter[] Design = 
            {
                new System.Data.SqlClient.SqlParameter("@WagesTypeName", TypeName),
                new System.Data.SqlClient.SqlParameter("@WagesTypeNo", ss)
            };
            DataTable dt = SQL.ExecuteDatatable("WagesType_Update", Design);
            return dt;
        }

        public DataTable EmployeeDisplay(string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] user = 
            {
              new System.Data.SqlClient.SqlParameter("@Ccode",Ccode),
              new System.Data.SqlClient.SqlParameter("@Lcode",Lcode),
            };
            DataTable dt = SQL.ExecuteDatatable("EmployeeDisplay_SP", user);
            return dt;
        }


        public DataTable EmployeeTypeUpdate(string TypeName, int ss)
        {
            System.Data.SqlClient.SqlParameter[] Design = 
            {
                new System.Data.SqlClient.SqlParameter("@TypeID", ss),
                new System.Data.SqlClient.SqlParameter("@TypeName", TypeName)
            };
            DataTable dt = SQL.ExecuteDatatable("EmployeeType_Update", Design);
            return dt;
        }

        public DataTable DepartmmentUpdate(string DeptName, int ss)
        {
            System.Data.SqlClient.SqlParameter[] Design = 
            {
                new System.Data.SqlClient.SqlParameter("@DeptName", DeptName),
                new System.Data.SqlClient.SqlParameter("@DeptNo", ss)
            };
            DataTable dt = SQL.ExecuteDatatable("Department_Update", Design);
            return dt;
        }

        public DataTable EmployeeTypeRegister(string Employeetype)
        {
            System.Data.SqlClient.SqlParameter[] Design = 
            {
                new System.Data.SqlClient.SqlParameter("@TypeName", Employeetype)
            };
            DataTable dt = SQL.ExecuteDatatable("EmployeeType_Register", Design);
            return dt;
        }

        public DataTable EditUserCreation(UserCreationClass objUsercreation)
        {
            System.Data.SqlClient.SqlParameter[] user = 
            {
                new System.Data.SqlClient.SqlParameter("@UserName",objUsercreation.UserName),
                new System.Data.SqlClient.SqlParameter("@Password",objUsercreation.Password),
                new System.Data.SqlClient.SqlParameter("@UserType",objUsercreation.UserCode),
                new System.Data.SqlClient.SqlParameter("@Ccode",objUsercreation.Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode",objUsercreation.Lcode),
                new System.Data.SqlClient.SqlParameter("@SNo",objUsercreation.UserID)

            };
            DataTable dt = SQL.ExecuteDatatable("EditUserCreation_SP", user);
            return dt;
        }


        public DataTable UserCreationDisplay(string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] user = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode",Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode",Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("UserCreationDisplay_SP", user);
            return dt;
        }



        public DataTable DepartmentIncentiveDisplay(string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] user = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode",Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode",Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("DepartmentIncenDisplay_SP", user);
            return dt;
        }



        public DataTable DepartmentDisplay()
        {
            System.Data.SqlClient.SqlParameter[] user = 
            {
              
            };
            DataTable dt = SQL.ExecuteDatatable("DepartmentDisplay_SP", user);
            return dt;
        }




        public DataTable DeleteUserCreation(int SNo)
        {
            System.Data.SqlClient.SqlParameter[] user = 
            {
                new System.Data.SqlClient.SqlParameter("@SNo",SNo),
            };
            DataTable dt = SQL.ExecuteDatatable("UserCreationDelete_SP", user);
            return dt;
        }



        public DataTable ChechDepartmentPermisssion(string permtype, string deptname, string tt, string CompanyCode, string LocationCode, string Monthname)
        {
            System.Data.SqlClient.SqlParameter[] user = 
            {
                new System.Data.SqlClient.SqlParameter("@PermissionType",permtype),
                new System.Data.SqlClient.SqlParameter("@deptname",deptname),
                new System.Data.SqlClient.SqlParameter("@finanYear",tt),
                new System.Data.SqlClient.SqlParameter("@Month", Monthname),
                new System.Data.SqlClient.SqlParameter("@Ccode", CompanyCode),
                new System.Data.SqlClient.SqlParameter("@Lcode", LocationCode)
            };
            DataTable dt = SQL.ExecuteDatatable("PermissionTypeCheck_SP", user);
            return dt;
        }



        public DataTable CheckingDesignEmployee(string DesignName)
        {
            System.Data.SqlClient.SqlParameter[] user = 
            {
                new System.Data.SqlClient.SqlParameter("@Designation",DesignName),
            };
            DataTable dt = SQL.ExecuteDatatable("DesginationNameInEmployee_SP", user);
            return dt;
        }


        public DataTable CheckingEmployee(string DeptName)
        {
            System.Data.SqlClient.SqlParameter[] user = 
            {
                new System.Data.SqlClient.SqlParameter("@DeptName",DeptName),
            };
            DataTable dt = SQL.ExecuteDatatable("DeptNameInEmployee_SP", user);
            return dt;
        }


        public DataTable DeleteDepartment(int DeptCode)
        {
            System.Data.SqlClient.SqlParameter[] user = 
            {
                new System.Data.SqlClient.SqlParameter("@DeptCode",DeptCode),
            };
            DataTable dt = SQL.ExecuteDatatable("DeleteDepartment_SP", user);
            return dt;
        }


        public DataTable EditUserCreation(int SNo)
        {
            System.Data.SqlClient.SqlParameter[] user = 
            {
                new System.Data.SqlClient.SqlParameter("@SNo",SNo),
            };
            DataTable dt = SQL.ExecuteDatatable("UserCreationEdit_SP", user);
            return dt;
        }



        public DataTable EditDepartment(string DeptCode)
        {
            System.Data.SqlClient.SqlParameter[] user = 
            {
                new System.Data.SqlClient.SqlParameter("@DeptName",DeptCode),
            };
            DataTable dt = SQL.ExecuteDatatable("DepartmentEdit_SP", user);
            return dt;
        }

        public DataTable CheckEmployeeDepartment(int DeptCode)
        {
            System.Data.SqlClient.SqlParameter[] user = 
            {
                new System.Data.SqlClient.SqlParameter("@DeptCode",DeptCode),
            };
            DataTable dt = SQL.ExecuteDatatable("DepartmentChechEmp_SP", user);
            return dt;
        }



        public DataTable AdministrationRegistration(UserCreationClass objUsercreation)
        {
            System.Data.SqlClient.SqlParameter[] user = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode",objUsercreation.Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode",objUsercreation.Lcode),
                new System.Data.SqlClient.SqlParameter("@UserName",objUsercreation.UserCode),
                new System.Data.SqlClient.SqlParameter("@Data",objUsercreation.Data),
                new System.Data.SqlClient.SqlParameter("@Attn_Log_Clear",objUsercreation.AttendanceLogClear),
                new System.Data.SqlClient.SqlParameter("@ManAttnCheck",objUsercreation.ManualattendanceCheck),
                new System.Data.SqlClient.SqlParameter("@ManAttnUpload",objUsercreation.ManAttnUpload),
                new System.Data.SqlClient.SqlParameter("@Emp_Approval",objUsercreation.EmpApproval),
                new System.Data.SqlClient.SqlParameter("@Emp_Status",objUsercreation.EmpStatus),
                new System.Data.SqlClient.SqlParameter("@Emp_Mst_Data_Per",objUsercreation.Emp_Mst_Data_Perm)
           };
            DataTable dt = SQL.ExecuteDatatable("AdministrationRegistration_SP", user);
            return dt;
        }


        public DataTable dropdown_UserName(string CompanyCode, string LocationCode)
        {
            System.Data.SqlClient.SqlParameter[] UserRights = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", CompanyCode),
                new System.Data.SqlClient.SqlParameter("@Lcode", LocationCode),
            };
            DataTable dt = SQL.ExecuteDatatable("dropdownUserName_SP", UserRights);
            return dt;
        }


        public DataTable dropdown_WagesType()
        {
            System.Data.SqlClient.SqlParameter[] UserRights = 
            {
               
            };
            DataTable dt = SQL.ExecuteDatatable("DropDownWagesType_SP", UserRights);
            return dt;
        }


        public DataTable dropdown_LeaveType()
        {
            System.Data.SqlClient.SqlParameter[] dd_company = 
            {
            };
            DataTable dt = SQL.ExecuteDatatable("LeaveTypeMST_SP", dd_company);
            return dt;
        }





        public DataTable Dropdown_Company()
        {
            System.Data.SqlClient.SqlParameter[] dd_company = 
            {
            };
            DataTable dt = SQL.ExecuteDatatable("Dropdown_Company", dd_company);
            return dt;
        }


        public DataTable Dropdown_Department()
        {
            System.Data.SqlClient.SqlParameter[] dd_dept = 
            {
            };
            DataTable dt = SQL.ExecuteDatatable("DeptDisplay_SP", dd_dept);
            return dt;
        }



        public DataTable DropDown_EmpType()
        {
            System.Data.SqlClient.SqlParameter[] dd_dept = 
            {
            };
            DataTable dt = SQL.ExecuteDatatable("DropDown_EmployeeType_SP", dd_dept);
            return dt;
        }



        public DataTable DropDown_WagesType()
        {
            System.Data.SqlClient.SqlParameter[] dd_dept = 
            {
            };
            DataTable dt = SQL.ExecuteDatatable("DropDownWagesType_SP", dd_dept);
            return dt;
        }

        public DataTable Dropdown_PayPeriod()
        {
            System.Data.SqlClient.SqlParameter[] dd_payperiod = 
            {
            };
            DataTable dt = SQL.ExecuteDatatable("DropDown_PayPeriod_SP", dd_payperiod);
            return dt;
        }


        public DataTable DropDown_MachineID()
        {
            System.Data.SqlClient.SqlParameter[] dd_machineID = 
            {
            };
            DataTable dt = SQL.ExecuteDatatable("DropDown_MachineID_SP", dd_machineID);
            return dt;
        }


        public DataTable Dropdown_Desgination()
        {
            System.Data.SqlClient.SqlParameter[] dd_desgination = 
            {
            };
            DataTable dt = SQL.ExecuteDatatable("DesginationDisplay_SP", dd_desgination);
            return dt;
        }



        public DataTable DropDown_LeaveTypeMale()
        {
            System.Data.SqlClient.SqlParameter[] dd_desgination = 
            {
            };
            DataTable dt = SQL.ExecuteDatatable("DropDownLeaveTypeMale_SP", dd_desgination);
            return dt;
        }



        public string Verification_verify()
        {
            System.Data.SqlClient.SqlParameter[] Verify_verification = 
            {
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "Verification_Get", Verify_verification);
            return str;
        }

        public DataTable AltiusLogin(string uname, string pwd1)
        {
            System.Data.SqlClient.SqlParameter[] ddloc = 
            {
                new System.Data.SqlClient.SqlParameter("@uname",uname),
                 new System.Data.SqlClient.SqlParameter("@pwd1", pwd1)
            };
            DataTable dt = SQL.ExecuteDatatable("AltiusLogin", ddloc);
            return dt;
        }



        public DataTable dropdown_ParticularLocation(string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] ddloc = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode",Ccode),
                 new System.Data.SqlClient.SqlParameter("@Lcode",Lcode)
             };
            DataTable dt = SQL.ExecuteDatatable("Dropdown_ParticularLocation", ddloc);
            return dt;
        }



        public DataTable dropdown_location(string Ccode)
        {
            System.Data.SqlClient.SqlParameter[] ddloc = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode",Ccode),
             };
            DataTable dt = SQL.ExecuteDatatable("LocationCode_Drop", ddloc);
            return dt;
        }



        public DataTable Value_Verify()
        {
            System.Data.SqlClient.SqlParameter[] Value_ver = 
            {
            };
            DataTable dt = SQL.ExecuteDatatable("Value_Verify", Value_ver);
            return dt;
        }

        public string ServerDate()
        {
            System.Data.SqlClient.SqlParameter[] ContractExcessLeave = 
            {
            };
            string str = (string)SQL.ExecuteScalar(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "ServerDate_SP", ContractExcessLeave);
            return str;
        }
        public DataTable UserLogin(string uname, string pwd)
        {
            System.Data.SqlClient.SqlParameter[] user = 
            {
                new System.Data.SqlClient.SqlParameter("@uname", uname),
                new System.Data.SqlClient.SqlParameter("@pwd", pwd),
                //new System.Data.SqlClient.SqlParameter("@Ccode",objuser.Ccode),
                //new System.Data.SqlClient.SqlParameter("@Lcode",objuser.Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("UserLogin", user);
            return dt;
        }



        public DataTable CheckUserCreation(string Ccode, string Lcode, string uname, string pwd)
        {
            System.Data.SqlClient.SqlParameter[] user = 
            {
                new System.Data.SqlClient.SqlParameter("@uname", uname),
                new System.Data.SqlClient.SqlParameter("@pwd", pwd),
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("CheckUserLogin_SP", user);
            return dt;
        }


        public DataTable Login_Rights(string ccode, string lcode, string uname)
        {
            System.Data.SqlClient.SqlParameter[] user = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", lcode),
                new System.Data.SqlClient.SqlParameter("@uname", uname)
            };
            DataTable dt = SQL.ExecuteDatatable("CheckUserRights_SP", user);
            return dt;
        }



        public DataTable CheckCompanyMst(string Ccode, string Cname)
        {
            System.Data.SqlClient.SqlParameter[] ddloc = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Cname", Cname)
            };
            DataTable dt = SQL.ExecuteDatatable("ChkCompanyMst_SP", ddloc);
            return dt;
        }



        public DataTable CheckLocationMst(string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] ddloc = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("ChkLocationMst_SP", ddloc);
            return dt;
        }



        public DataTable dropdown_loc(string Ccode)
        {
            System.Data.SqlClient.SqlParameter[] ddloc = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode", Ccode)
            };
            DataTable dt = SQL.ExecuteDatatable("Dropdown_Location", ddloc);
            return dt;
        }



        public void LocationRegistration(CompanyMasterClass Comobj)
        {
            System.Data.SqlClient.SqlParameter[] LocationMaster = 
            {
                new System.Data.SqlClient.SqlParameter("@LName",Comobj.LocationName),
                new System.Data.SqlClient.SqlParameter("@CCode",Comobj.Ccode),
                new System.Data.SqlClient.SqlParameter("@LCode",Comobj.Lcode),
                new System.Data.SqlClient.SqlParameter("@Address1",Comobj.AddressOne),
                new System.Data.SqlClient.SqlParameter("@Address2",Comobj.AddressTwo),
                new System.Data.SqlClient.SqlParameter("@PinCode",Comobj.PinCode),
                new System.Data.SqlClient.SqlParameter("@State",Comobj.State),
                new System.Data.SqlClient.SqlParameter("@MobileNo",Comobj.MobileNumber),
                new System.Data.SqlClient.SqlParameter("@EmailID",Comobj.EMail),
                new System.Data.SqlClient.SqlParameter("@EstCode",Comobj.EstablishmentCode),
                new System.Data.SqlClient.SqlParameter("@UdefI",Comobj.UserDefI),
                new System.Data.SqlClient.SqlParameter("@UdefII",Comobj.UserDefII),
                new System.Data.SqlClient.SqlParameter("@UdefIII",Comobj.UserDefIII)
             };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "RegistrationLocationMaster_SP", LocationMaster);
        }


        public void CompanyRegistration(CompanyMasterClass Comobj)
        {
            System.Data.SqlClient.SqlParameter[] CompanyMaster = 
            {
                new System.Data.SqlClient.SqlParameter("@CName",Comobj.CompanyName),
                new System.Data.SqlClient.SqlParameter("@CCode",Comobj.Ccode),
                new System.Data.SqlClient.SqlParameter("@Address1",Comobj.AddressOne),
                new System.Data.SqlClient.SqlParameter("@Address2",Comobj.AddressTwo),
                new System.Data.SqlClient.SqlParameter("@PinCode",Comobj.PinCode),
                new System.Data.SqlClient.SqlParameter("@State",Comobj.State),
                new System.Data.SqlClient.SqlParameter("@MobileNo",Comobj.MobileNumber),
                new System.Data.SqlClient.SqlParameter("@EmailID",Comobj.EMail),
                new System.Data.SqlClient.SqlParameter("@EstCode",Comobj.EstablishmentCode),
                new System.Data.SqlClient.SqlParameter("@UdefI",Comobj.UserDefI),
                new System.Data.SqlClient.SqlParameter("@UdefII",Comobj.UserDefII),
                new System.Data.SqlClient.SqlParameter("@UdefIII",Comobj.UserDefIII)
             };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "RegistrationCompanyMaster_SP", CompanyMaster);
        }




        public void CompanyMaster(CompanyMasterClass Comobj)
        {
            System.Data.SqlClient.SqlParameter[] CompanyMaster = 
            {
                new System.Data.SqlClient.SqlParameter("@CName",Comobj.CompanyName),
                new System.Data.SqlClient.SqlParameter("@CCode",Comobj.Ccode),
                new System.Data.SqlClient.SqlParameter("@Address1",Comobj.AddressOne),
                new System.Data.SqlClient.SqlParameter("@Address2",Comobj.AddressTwo),
                new System.Data.SqlClient.SqlParameter("@PinCode",Comobj.PinCode),
                new System.Data.SqlClient.SqlParameter("@State",Comobj.State),
                
             };
            SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "CompanyMaster_SP", CompanyMaster);
        }



        //public void DeductionAndOTRegistration(DeductionAndOTClass objdedOT)
        //{
        //    System.Data.SqlClient.SqlParameter[] DeductionOT = 
        //    {
        //        new System.Data.SqlClient.SqlParameter("@CompanyCode",Comobj.CompanyName),
        //        new System.Data.SqlClient.SqlParameter("@LocationCode",Comobj.LocationName),
        //        new System.Data.SqlClient.SqlParameter("@Month",Comobj.Ccode),
        //        new System.Data.SqlClient.SqlParameter("@FinancialYear",Comobj.Lcode),
        //        new System.Data.SqlClient.SqlParameter("@Wages",Comobj.AddressOne),
        //        new System.Data.SqlClient.SqlParameter("@MachineID",Comobj.AddressTwo),
        //        new System.Data.SqlClient.SqlParameter("@Name",Comobj.PinCode),
        //        new System.Data.SqlClient.SqlParameter("@TokenNo",Comobj.State),
        //        new System.Data.SqlClient.SqlParameter("@FromDate",Comobj.MobileNumber),
        //        new System.Data.SqlClient.SqlParameter("@ToDate",Comobj.EMail),
        //        new System.Data.SqlClient.SqlParameter("@AllowOtherOne",Comobj.EstablishmentCode),
        //        new System.Data.SqlClient.SqlParameter("@AllowOtherTwo",Comobj.UserType),
        //        new System.Data.SqlClient.SqlParameter("@Advance",Comobj.UserName),
        //        new System.Data.SqlClient.SqlParameter("@Canteen",Comobj.Password),
        //        new System.Data.SqlClient.SqlParameter("@LWF",Comobj.State),
        //        new System.Data.SqlClient.SqlParameter("@PTax",Comobj.MobileNumber),
        //        new System.Data.SqlClient.SqlParameter("@DeductionOtherOne",Comobj.EMail),
        //        new System.Data.SqlClient.SqlParameter("@DeductionOtherTwo",Comobj.EstablishmentCode),
        //        new System.Data.SqlClient.SqlParameter("@HAllowed",Comobj.UserType),
        //        new System.Data.SqlClient.SqlParameter("@OTHours",Comobj.UserName),
        //        new System.Data.SqlClient.SqlParameter("@CanteenDaysMinus",Comobj.Password),
        //        new System.Data.SqlClient.SqlParameter("@LOPDays",Comobj.UserType),
        //        new System.Data.SqlClient.SqlParameter("@Ccode",Comobj.UserName),
        //        new System.Data.SqlClient.SqlParameter("@Lcode",Comobj.Password),
        //     };
        //    SQL.ExecuteNonQuery(BaseDataAccess.ConnectionString, CommandType.StoredProcedure, "DeductionOT_SP", DeductionOT);
        //}


        public DataTable EmpIDRegistration(EmpIDDetails objEmpID)
        {
            System.Data.SqlClient.SqlParameter[] user = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode",objEmpID.Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode",objEmpID.Lcode),
                new System.Data.SqlClient.SqlParameter("@AdharNo",objEmpID.AdharNo),
                new System.Data.SqlClient.SqlParameter("@VoterID",objEmpID.VoterID),
                new System.Data.SqlClient.SqlParameter("@DrivingLicence",objEmpID.DrivingLicence),
                new System.Data.SqlClient.SqlParameter("@PassportNo",objEmpID.PassportNo),
                new System.Data.SqlClient.SqlParameter("@RationCardNo",objEmpID.RationCardNo),
                new System.Data.SqlClient.SqlParameter("@panCardNo",objEmpID.panCardNo),
                new System.Data.SqlClient.SqlParameter("@SmartCardNo",objEmpID.SmartCardNo),
                new System.Data.SqlClient.SqlParameter("@Other",objEmpID.Other)
            };
            DataTable dt = SQL.ExecuteDatatable("EmpIDdetails_SP", user);
            return dt;
        }


        public DataTable EmployeeDetails(EmployeeDetailsClass objEmpDetails)
        {
            System.Data.SqlClient.SqlParameter[] user = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode",objEmpDetails.Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode",objEmpDetails.Lcode),
                new System.Data.SqlClient.SqlParameter("@ShiftType",objEmpDetails.ShiftType),
                new System.Data.SqlClient.SqlParameter("@TypeName",objEmpDetails.EmpType),
                new System.Data.SqlClient.SqlParameter("@EmpPrefix",objEmpDetails.EmpPrefix),
                new System.Data.SqlClient.SqlParameter("@TokenNo",objEmpDetails.TokenNo),
                new System.Data.SqlClient.SqlParameter("@Category",objEmpDetails.Category),
                new System.Data.SqlClient.SqlParameter("@SubCategory",objEmpDetails.SubCategory),
                new System.Data.SqlClient.SqlParameter("@ExistingNo",objEmpDetails.ExistingNo),
                new System.Data.SqlClient.SqlParameter("@MachineID",objEmpDetails.MachineID),
                new System.Data.SqlClient.SqlParameter("@FirstName",objEmpDetails.FirstName),
                new System.Data.SqlClient.SqlParameter("@LastName",objEmpDetails.LastName),
                new System.Data.SqlClient.SqlParameter("@Initial",objEmpDetails.Initial),
                new System.Data.SqlClient.SqlParameter("@Gender",objEmpDetails.Gender),
                new System.Data.SqlClient.SqlParameter("@DOB",objEmpDetails.DOB),
                new System.Data.SqlClient.SqlParameter("@Age",objEmpDetails.Age),
                new System.Data.SqlClient.SqlParameter("@MartialStatus",objEmpDetails.MaritalStatus),
                new System.Data.SqlClient.SqlParameter("@DOJ",objEmpDetails.DOJ),
                new System.Data.SqlClient.SqlParameter("@Department",objEmpDetails.Department),
                new System.Data.SqlClient.SqlParameter("@Desgination",objEmpDetails.Desgination),
                new System.Data.SqlClient.SqlParameter("@PayPeriod_Desc",objEmpDetails.PayPeriod),
                new System.Data.SqlClient.SqlParameter("@BaseSalary",objEmpDetails.BaseSalary),
                new System.Data.SqlClient.SqlParameter("@PFNumber",objEmpDetails.PFNumber),
                new System.Data.SqlClient.SqlParameter("@Nominee",objEmpDetails.Nominee),
                new System.Data.SqlClient.SqlParameter("@ESINumber",objEmpDetails.ESINumber),
                new System.Data.SqlClient.SqlParameter("@StdWorkingHrs",objEmpDetails.StdWorkingHrs),
                new System.Data.SqlClient.SqlParameter("@OTEligible",objEmpDetails.OTEligible),
                new System.Data.SqlClient.SqlParameter("@Nationality",objEmpDetails.Nationality),
                new System.Data.SqlClient.SqlParameter("@Qualification",objEmpDetails.Qualification),
                new System.Data.SqlClient.SqlParameter("@Certificate",objEmpDetails.Certificate),
                new System.Data.SqlClient.SqlParameter("@FamilyDetails",objEmpDetails.FamilyDetails),
                new System.Data.SqlClient.SqlParameter("@RecuritThr", objEmpDetails.RecuritThr),
                new System.Data.SqlClient.SqlParameter("@IdentiMark1",objEmpDetails.IdentiMark1),
                new System.Data.SqlClient.SqlParameter("@IdentiMark2",objEmpDetails.IdentiMark2),
                new System.Data.SqlClient.SqlParameter("@BloodGroup",objEmpDetails.BloodGroup),
                new System.Data.SqlClient.SqlParameter("@Handicapped",objEmpDetails.Handicapped),
                new System.Data.SqlClient.SqlParameter("@Height",objEmpDetails.Height),
                new System.Data.SqlClient.SqlParameter("@Weight",objEmpDetails.Weight),
                new System.Data.SqlClient.SqlParameter("@Address1",objEmpDetails.PermanentAddr),
                new System.Data.SqlClient.SqlParameter("@Address2",objEmpDetails.TempAddress),
                new System.Data.SqlClient.SqlParameter("@BankName",objEmpDetails.BankName),
                new System.Data.SqlClient.SqlParameter("@BranchCode",objEmpDetails.BranchCode),
                new System.Data.SqlClient.SqlParameter("@AccountNo", objEmpDetails.AccountNo),
                new System.Data.SqlClient.SqlParameter("@EmpStatus", objEmpDetails.EmpStatus),
                new System.Data.SqlClient.SqlParameter("@IsActive",objEmpDetails.IsActive),
                new System.Data.SqlClient.SqlParameter("@Created_By",objEmpDetails.CreateBy),
                new System.Data.SqlClient.SqlParameter("@Created_Date",objEmpDetails.CreateDate),
                new System.Data.SqlClient.SqlParameter("@IsNonAdmin",objEmpDetails.IsNonAdmin),
                new System.Data.SqlClient.SqlParameter("@MachineEnc",objEmpDetails.MachineID_Encry),
                new System.Data.SqlClient.SqlParameter("@Working_Hours",objEmpDetails.Working_Hours),
                new System.Data.SqlClient.SqlParameter("@Calculate_Work_Hours",objEmpDetails.Calculate_Work_Hours),
                new System.Data.SqlClient.SqlParameter("@OTHrs", objEmpDetails.OTHrs),
                new System.Data.SqlClient.SqlParameter("@WagesType",objEmpDetails.WagesType),
                new System.Data.SqlClient.SqlParameter("@RecutersMob",objEmpDetails.RecutersMob),
                new System.Data.SqlClient.SqlParameter("@ParentsMobile1",objEmpDetails.ParentsMobile1),
                new System.Data.SqlClient.SqlParameter("@ParentsMobile2",objEmpDetails.ParentsMobile2),
                new System.Data.SqlClient.SqlParameter("@EmpMobile",objEmpDetails.EmpMobile),
                new System.Data.SqlClient.SqlParameter("@SamepresentAddress",objEmpDetails.SamepresentAddress),
                new System.Data.SqlClient.SqlParameter("@Religion",objEmpDetails.Religion),
                new System.Data.SqlClient.SqlParameter("@EmpLeft",objEmpDetails.EmpLeft),
                new System.Data.SqlClient.SqlParameter("@BusNo",objEmpDetails.BusNo),
                new System.Data.SqlClient.SqlParameter("@Reference", objEmpDetails.Reference),
                new System.Data.SqlClient.SqlParameter("@PermanentDst",objEmpDetails.PermanentDst),
                new System.Data.SqlClient.SqlParameter("@TempDst",objEmpDetails.TempDst),
                new System.Data.SqlClient.SqlParameter("@WeekOff", objEmpDetails.WeekOff),
                new System.Data.SqlClient.SqlParameter("@PFdoj", objEmpDetails.PFdoj),
                new System.Data.SqlClient.SqlParameter("@ESIdoj", objEmpDetails.ESIdoj),
                new System.Data.SqlClient.SqlParameter("@IFSCCode",objEmpDetails.IFSCCode),
                new System.Data.SqlClient.SqlParameter("@PFEligible",objEmpDetails.PFEligible),
                new System.Data.SqlClient.SqlParameter("@ESIEligible",objEmpDetails.ESIEligible),
                new System.Data.SqlClient.SqlParameter("@BusRoute",objEmpDetails.BusRoute),
                new System.Data.SqlClient.SqlParameter("@HostelRmNo",objEmpDetails.HostelRmNo),    
                new System.Data.SqlClient.SqlParameter("@BasicSalary",objEmpDetails.BasicSalary),
                new System.Data.SqlClient.SqlParameter("@Deduction1amt",objEmpDetails.Deduction1amt),
                new System.Data.SqlClient.SqlParameter("@Deduction2amt",objEmpDetails.Deduction2amt),
                new System.Data.SqlClient.SqlParameter("@Allowance1amt",objEmpDetails.Allowance1amt),
                new System.Data.SqlClient.SqlParameter("@Allowance2amt",objEmpDetails.Allowance2amt),
                new System.Data.SqlClient.SqlParameter("@PFS",objEmpDetails.PFS),
                new System.Data.SqlClient.SqlParameter("@SalaryThro",objEmpDetails.SalaryThro),
                new System.Data.SqlClient.SqlParameter("@Mother",objEmpDetails.Mother),
                new System.Data.SqlClient.SqlParameter("@Permanent_Taluk",objEmpDetails.Permanent_Taluk),
                new System.Data.SqlClient.SqlParameter("@Present_Taluk",objEmpDetails.Present_Taluk),
                new System.Data.SqlClient.SqlParameter("@BrokerName",objEmpDetails.BrokerName),
                new System.Data.SqlClient.SqlParameter("@BrokerType",objEmpDetails.Types),
                new System.Data.SqlClient.SqlParameter("@BrokerAgent",objEmpDetails.Types_Name),
                new System.Data.SqlClient.SqlParameter("@Community",objEmpDetails.Community),
                new System.Data.SqlClient.SqlParameter("@Caste",objEmpDetails.Caste),
                new System.Data.SqlClient.SqlParameter("@PhysicalRemarks",objEmpDetails.PhysicalRemarks),
                new System.Data.SqlClient.SqlParameter("@RejoinDate",objEmpDetails.RejoinDate),
                new System.Data.SqlClient.SqlParameter("RelieveDate",objEmpDetails.RelieveDate)

               

              };
            DataTable dt = SQL.ExecuteDatatable("EmployeeDetails_SP", user);
            return dt;
        }

        public DataTable EmpAdolescentRegistration(AdolescentClass objAdu)
        {
            System.Data.SqlClient.SqlParameter[] user = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode",objAdu.Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode",objAdu.Lcode),
                new System.Data.SqlClient.SqlParameter("@CertificateNo",objAdu.CertificateNumber),
                new System.Data.SqlClient.SqlParameter("@CertificateDate",objAdu.CertificateDate),
                new System.Data.SqlClient.SqlParameter("@NextDewDate",objAdu.NextNewDate),
                new System.Data.SqlClient.SqlParameter("@TypesOfCertificate",objAdu.TypesOfCertificate),
                new System.Data.SqlClient.SqlParameter("@IsAdolescent",objAdu.ISAdolescent),
                new System.Data.SqlClient.SqlParameter("@Remarks",objAdu.Remarks)
            };
            DataTable dt = SQL.ExecuteDatatable("EmpAdolescentdetails_SP", user);
            return dt;
        }

        public DataTable ManualAttendanceRegister(ManualAttendanceClass objManual)
        {
            System.Data.SqlClient.SqlParameter[] user = 
            {
                //new System.Data.SqlClient.SqlParameter("@CompanyName",objManual.CompanyCode),
                //new System.Data.SqlClient.SqlParameter("@LocationCode",objManual.LocationCode),
                new System.Data.SqlClient.SqlParameter("@TktNo",objManual.TktNo),
                new System.Data.SqlClient.SqlParameter("@ExistingCode",objManual.ExistingCode),
                new System.Data.SqlClient.SqlParameter("@EmpName",objManual.EmpName),
                new System.Data.SqlClient.SqlParameter("@AttendanceStatus",objManual.AttendanceStatus),
                new System.Data.SqlClient.SqlParameter("@TimeIn",objManual.TimeIn),
                new System.Data.SqlClient.SqlParameter("@TimeOut",objManual.TimeOut),
                new System.Data.SqlClient.SqlParameter("@LogTimeIN",objManual.LogTimeIN),
                new System.Data.SqlClient.SqlParameter("@LogTimeOUT",objManual.LogTimeOUT),
                new System.Data.SqlClient.SqlParameter("@CurrentDate",objManual.CurrentDate),
                new System.Data.SqlClient.SqlParameter("@UserName",objManual.UserName),
                new System.Data.SqlClient.SqlParameter("@AttendanceDate",objManual.AttendanceDate),
                new System.Data.SqlClient.SqlParameter("@CompensationDate",objManual.CompensationDate),
                new System.Data.SqlClient.SqlParameter("@CompAgainstDate",objManual.CoAgainstDate),
                new System.Data.SqlClient.SqlParameter("@Ccode",objManual.Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode",objManual.Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("ManualAttendanceRegister_SP", user);
            return dt;
        }





        public DataTable UpdateEmployeeDetails(EmployeeDetailsClass objEmpDetails)
        {
            System.Data.SqlClient.SqlParameter[] user = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode",objEmpDetails.Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode",objEmpDetails.Lcode),
                new System.Data.SqlClient.SqlParameter("@ShiftType",objEmpDetails.ShiftType),
                new System.Data.SqlClient.SqlParameter("@TypeName",objEmpDetails.EmpType),
                new System.Data.SqlClient.SqlParameter("@EmpPrefix",objEmpDetails.EmpPrefix),
                new System.Data.SqlClient.SqlParameter("@TokenNo",objEmpDetails.TokenNo),
                new System.Data.SqlClient.SqlParameter("@Category",objEmpDetails.Category),
                new System.Data.SqlClient.SqlParameter("@SubCategory",objEmpDetails.SubCategory),
                new System.Data.SqlClient.SqlParameter("@ExistingNo",objEmpDetails.ExistingNo),
                new System.Data.SqlClient.SqlParameter("@MachineID",objEmpDetails.MachineID),
                new System.Data.SqlClient.SqlParameter("@FirstName",objEmpDetails.FirstName),
                new System.Data.SqlClient.SqlParameter("@LastName",objEmpDetails.LastName),
                new System.Data.SqlClient.SqlParameter("@Initial",objEmpDetails.Initial),
                new System.Data.SqlClient.SqlParameter("@Gender",objEmpDetails.Gender),
                new System.Data.SqlClient.SqlParameter("@DOB",objEmpDetails.DOB),
                new System.Data.SqlClient.SqlParameter("@Age",objEmpDetails.Age),
                new System.Data.SqlClient.SqlParameter("@MartialStatus",objEmpDetails.MaritalStatus),
                new System.Data.SqlClient.SqlParameter("@DOJ",objEmpDetails.DOJ),
                new System.Data.SqlClient.SqlParameter("@Department",objEmpDetails.Department),
                new System.Data.SqlClient.SqlParameter("@Desgination",objEmpDetails.Desgination),
                new System.Data.SqlClient.SqlParameter("@PayPeriod_Desc",objEmpDetails.PayPeriod),
                new System.Data.SqlClient.SqlParameter("@BaseSalary",objEmpDetails.BaseSalary),
                new System.Data.SqlClient.SqlParameter("@PFNumber",objEmpDetails.PFNumber),
                new System.Data.SqlClient.SqlParameter("@Nominee",objEmpDetails.Nominee),
                new System.Data.SqlClient.SqlParameter("@ESINumber",objEmpDetails.ESINumber),
                new System.Data.SqlClient.SqlParameter("@StdWorkingHrs",objEmpDetails.StdWorkingHrs),
                new System.Data.SqlClient.SqlParameter("@OTEligible",objEmpDetails.OTEligible),
                new System.Data.SqlClient.SqlParameter("@Nationality",objEmpDetails.Nationality),
                new System.Data.SqlClient.SqlParameter("@Qualification",objEmpDetails.Qualification),
                new System.Data.SqlClient.SqlParameter("@Certificate",objEmpDetails.Certificate),
                new System.Data.SqlClient.SqlParameter("@FamilyDetails",objEmpDetails.FamilyDetails),
                new System.Data.SqlClient.SqlParameter("@RecuritThr", objEmpDetails.RecuritThr),
                new System.Data.SqlClient.SqlParameter("@IdentiMark1",objEmpDetails.IdentiMark1),
                new System.Data.SqlClient.SqlParameter("@IdentiMark2",objEmpDetails.IdentiMark2),
                new System.Data.SqlClient.SqlParameter("@BloodGroup",objEmpDetails.BloodGroup),
                new System.Data.SqlClient.SqlParameter("@Handicapped",objEmpDetails.Handicapped),
                new System.Data.SqlClient.SqlParameter("@Height",objEmpDetails.Height),
                new System.Data.SqlClient.SqlParameter("@Weight",objEmpDetails.Weight),
                new System.Data.SqlClient.SqlParameter("@Address1",objEmpDetails.PermanentAddr),
                new System.Data.SqlClient.SqlParameter("@Address2",objEmpDetails.TempAddress),
                new System.Data.SqlClient.SqlParameter("@BankName",objEmpDetails.BankName),
                new System.Data.SqlClient.SqlParameter("@BranchCode",objEmpDetails.BranchCode),
                new System.Data.SqlClient.SqlParameter("@AccountNo", objEmpDetails.AccountNo),
                new System.Data.SqlClient.SqlParameter("@EmpStatus", objEmpDetails.EmpStatus),
                new System.Data.SqlClient.SqlParameter("@IsActive",objEmpDetails.IsActive),
                new System.Data.SqlClient.SqlParameter("@Created_By",objEmpDetails.CreateBy),
                new System.Data.SqlClient.SqlParameter("@Created_Date",objEmpDetails.CreateDate),
                new System.Data.SqlClient.SqlParameter("@IsNonAdmin",objEmpDetails.IsNonAdmin),
                new System.Data.SqlClient.SqlParameter("@MachineEnc",objEmpDetails.MachineID_Encry),
                new System.Data.SqlClient.SqlParameter("@Working_Hours",objEmpDetails.Working_Hours),
                new System.Data.SqlClient.SqlParameter("@Calculate_Work_Hours",objEmpDetails.Calculate_Work_Hours),
                new System.Data.SqlClient.SqlParameter("@OTHrs", objEmpDetails.OTHrs),
                new System.Data.SqlClient.SqlParameter("@WagesType",objEmpDetails.WagesType),
                new System.Data.SqlClient.SqlParameter("@RecutersMob",objEmpDetails.RecutersMob),
                new System.Data.SqlClient.SqlParameter("@ParentsMobile1",objEmpDetails.ParentsMobile1),
                new System.Data.SqlClient.SqlParameter("@ParentsMobile2",objEmpDetails.ParentsMobile2),
                new System.Data.SqlClient.SqlParameter("@EmpMobile",objEmpDetails.EmpMobile),
                new System.Data.SqlClient.SqlParameter("@SamepresentAddress",objEmpDetails.SamepresentAddress),
                new System.Data.SqlClient.SqlParameter("@Religion",objEmpDetails.Religion),
                new System.Data.SqlClient.SqlParameter("@EmpLeft",objEmpDetails.EmpLeft),
                new System.Data.SqlClient.SqlParameter("@BusNo",objEmpDetails.BusNo),
                new System.Data.SqlClient.SqlParameter("@Reference", objEmpDetails.Reference),
                new System.Data.SqlClient.SqlParameter("@PermanentDst",objEmpDetails.PermanentDst),
                new System.Data.SqlClient.SqlParameter("@TempDst",objEmpDetails.TempDst),
                new System.Data.SqlClient.SqlParameter("@WeekOff", objEmpDetails.WeekOff),
                new System.Data.SqlClient.SqlParameter("@PFdoj", objEmpDetails.PFdoj),
                new System.Data.SqlClient.SqlParameter("@ESIdoj", objEmpDetails.ESIdoj),
                new System.Data.SqlClient.SqlParameter("@IFSCCode",objEmpDetails.IFSCCode),
                new System.Data.SqlClient.SqlParameter("@PFEligible",objEmpDetails.PFEligible),
                new System.Data.SqlClient.SqlParameter("@ESIEligible",objEmpDetails.ESIEligible),
                new System.Data.SqlClient.SqlParameter("@BusRoute",objEmpDetails.BusRoute),
                new System.Data.SqlClient.SqlParameter("@HostelRmNo",objEmpDetails.HostelRmNo),    
                new System.Data.SqlClient.SqlParameter("@BasicSalary",objEmpDetails.BasicSalary),
                new System.Data.SqlClient.SqlParameter("@Deduction1amt",objEmpDetails.Deduction1amt),
                new System.Data.SqlClient.SqlParameter("@Deduction2amt",objEmpDetails.Deduction2amt),
                new System.Data.SqlClient.SqlParameter("@Allowance1amt",objEmpDetails.Allowance1amt),
                new System.Data.SqlClient.SqlParameter("@Allowance2amt",objEmpDetails.Allowance2amt),
                new System.Data.SqlClient.SqlParameter("@PFS",objEmpDetails.PFS),
                new System.Data.SqlClient.SqlParameter("@SalaryThro",objEmpDetails.SalaryThro),
                new System.Data.SqlClient.SqlParameter("@Mother",objEmpDetails.Mother),
                new System.Data.SqlClient.SqlParameter("@Permanent_Taluk",objEmpDetails.Permanent_Taluk),
                new System.Data.SqlClient.SqlParameter("@Present_Taluk",objEmpDetails.Present_Taluk),
                new System.Data.SqlClient.SqlParameter("@BrokerName",objEmpDetails.BrokerName),
                new System.Data.SqlClient.SqlParameter("@BrokerType",objEmpDetails.Types),
                new System.Data.SqlClient.SqlParameter("@BrokerAgent",objEmpDetails.Types_Name),
                new System.Data.SqlClient.SqlParameter("@Community",objEmpDetails.Community),
                new System.Data.SqlClient.SqlParameter("@Caste",objEmpDetails.Caste),
                new System.Data.SqlClient.SqlParameter("@PhysicalRemarks",objEmpDetails.PhysicalRemarks),
                new System.Data.SqlClient.SqlParameter("@RejoinDate",objEmpDetails.RejoinDate),
                new System.Data.SqlClient.SqlParameter("RelieveDate",objEmpDetails.RelieveDate)
               

              };
            DataTable dt = SQL.ExecuteDatatable("UpdateEmployeeDetails_SP", user);
            return dt;
        }


        public DataTable UpdateEpayEmployeeDetails(EmployeeDetailsEpayClass objEpayEmpDetails)
        {
            System.Data.SqlClient.SqlParameter[] user = 
            {
                new System.Data.SqlClient.SqlParameter("@TokenNo",objEpayEmpDetails.TokenNo),
                new System.Data.SqlClient.SqlParameter("@MachineID",objEpayEmpDetails.MachineID),
                new System.Data.SqlClient.SqlParameter("@ExistingNo",objEpayEmpDetails.ExistingNo),
                new System.Data.SqlClient.SqlParameter("@EmpName",objEpayEmpDetails.EmpName),
                new System.Data.SqlClient.SqlParameter("@FatherName",objEpayEmpDetails.FatherName),
                new System.Data.SqlClient.SqlParameter("@Gender",objEpayEmpDetails.Gender),
                new System.Data.SqlClient.SqlParameter("@DOB",objEpayEmpDetails.DOB),
                new System.Data.SqlClient.SqlParameter("@PermanentAddr",objEpayEmpDetails.PermanentAddr),
                new System.Data.SqlClient.SqlParameter("@TempAddress",objEpayEmpDetails.TempAddress),
                new System.Data.SqlClient.SqlParameter("@PermanentTlk",objEpayEmpDetails.PermanentTlk),
                new System.Data.SqlClient.SqlParameter("@PermanentDst",objEpayEmpDetails.PermanentDst),
                new System.Data.SqlClient.SqlParameter("@EmpMobile",objEpayEmpDetails.EmpMobile),
                new System.Data.SqlClient.SqlParameter("@Qualification",objEpayEmpDetails.Qualification),
                new System.Data.SqlClient.SqlParameter("@Department",objEpayEmpDetails.Department),
                new System.Data.SqlClient.SqlParameter("@Desgination",objEpayEmpDetails.Desgination),
                new System.Data.SqlClient.SqlParameter("@Category",objEpayEmpDetails.Category),
                new System.Data.SqlClient.SqlParameter("@Ccode",objEpayEmpDetails.Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode",objEpayEmpDetails.Lcode),
                new System.Data.SqlClient.SqlParameter("@MaritalStatus",objEpayEmpDetails.MaritalStatus),


                new System.Data.SqlClient.SqlParameter("@IsActive",objEpayEmpDetails.IsActive),
                new System.Data.SqlClient.SqlParameter("@CreatedBy",objEpayEmpDetails.CreatedBy),
                new System.Data.SqlClient.SqlParameter("@CreatedDate",objEpayEmpDetails.CreatedDate),
                new System.Data.SqlClient.SqlParameter("@EmpType",objEpayEmpDetails.EmployeeType),
                new System.Data.SqlClient.SqlParameter("@Uneducated",objEpayEmpDetails.UnEducated),

                new System.Data.SqlClient.SqlParameter("@NonPFGrade",objEpayEmpDetails.NonPFGrade),
                new System.Data.SqlClient.SqlParameter("@IsValid",objEpayEmpDetails.IsValid),
                new System.Data.SqlClient.SqlParameter("@IsLeave",objEpayEmpDetails.IsLeave),
                new System.Data.SqlClient.SqlParameter("@Status",objEpayEmpDetails.Status),
                new System.Data.SqlClient.SqlParameter("@RoleCode",objEpayEmpDetails.RoleCode),
                new System.Data.SqlClient.SqlParameter("@Initial",objEpayEmpDetails.Initial),
                
            };
            DataTable dt = SQL.ExecuteDatatable("EPayUpdateEmployeeDetails_SP", user);
            return dt;
        }


        public DataTable UpdateEpayProfileDetails(OfficialProfileEPayClass objEpayProfDetails)
        {
            System.Data.SqlClient.SqlParameter[] user = 
            {
                new System.Data.SqlClient.SqlParameter("@TokenNo",objEpayProfDetails.TokenNo),
                new System.Data.SqlClient.SqlParameter("@MachineID",objEpayProfDetails.MachineID),
                new System.Data.SqlClient.SqlParameter("@DOJ",objEpayProfDetails.DOJ),
                new System.Data.SqlClient.SqlParameter("@ESINumber",objEpayProfDetails.ESINumber),
                new System.Data.SqlClient.SqlParameter("@PFNumber",objEpayProfDetails.PFNumber),
                new System.Data.SqlClient.SqlParameter("@BranchCode",objEpayProfDetails.BranchCode),
                new System.Data.SqlClient.SqlParameter("@BankName",objEpayProfDetails.BankName),
                new System.Data.SqlClient.SqlParameter("@AccountNo",objEpayProfDetails.AccountNo),
                new System.Data.SqlClient.SqlParameter("@WagesType",objEpayProfDetails.WagesType),
                new System.Data.SqlClient.SqlParameter("@Basic",objEpayProfDetails.Basic),
                new System.Data.SqlClient.SqlParameter("@ESIEligible",objEpayProfDetails.ESIEligible),
                new System.Data.SqlClient.SqlParameter("@PFEligible",objEpayProfDetails.PFEligible),
                new System.Data.SqlClient.SqlParameter("@Ccode",objEpayProfDetails.Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode",objEpayProfDetails.Lcode),
                //new System.Data.SqlClient.SqlParameter("@ESICode",objEpayProfDetails.ESICode),
                new System.Data.SqlClient.SqlParameter("@PFdoj",objEpayProfDetails.PFdoj),
                new System.Data.SqlClient.SqlParameter("@ESIdoj",objEpayProfDetails.ESIdoj),
                

            };
            DataTable dt = SQL.ExecuteDatatable("EPayUpdateProfileDetails_SP", user);
            return dt;
        }


        public DataTable EpayUpdateSalaryMasterDetails(SalaryMasterEpayClass objEpaySalaryMst)
        {
            System.Data.SqlClient.SqlParameter[] user = 
            {
                new System.Data.SqlClient.SqlParameter("@TokenNo",objEpaySalaryMst.TokenNo),
                new System.Data.SqlClient.SqlParameter("@Basic",objEpaySalaryMst.Basic),
                new System.Data.SqlClient.SqlParameter("@Allowance1",objEpaySalaryMst.Allowance1),
                new System.Data.SqlClient.SqlParameter("@Allowance2",objEpaySalaryMst.Allowance2),
                new System.Data.SqlClient.SqlParameter("@Deduction1",objEpaySalaryMst.Deduction1),
                new System.Data.SqlClient.SqlParameter("@Deduction2",objEpaySalaryMst.Deduction2),
                new System.Data.SqlClient.SqlParameter("@Ccode",objEpaySalaryMst.Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode",objEpaySalaryMst.Lcode),
                new System.Data.SqlClient.SqlParameter("@BaseSalary",objEpaySalaryMst.BaseSalary),
                 new System.Data.SqlClient.SqlParameter("@PFSalary",objEpaySalaryMst.PFSalary),
                
            };
            DataTable dt = SQL.ExecuteDatatable("EPayUpdateSalaryMasterDetails_SP", user);
            return dt;
        }











        public DataTable CheckDepartment_AlreadyExist(string DeptName)
        {
            System.Data.SqlClient.SqlParameter[] Deptchk = 
            {
                new System.Data.SqlClient.SqlParameter("@DeptName", DeptName)
            };
            DataTable dt = SQL.ExecuteDatatable("Department_Check", Deptchk);
            return dt;
        }

        public DataTable CheckEmployeeType_AlreadyExist(string Employeetype)
        {
            System.Data.SqlClient.SqlParameter[] EmployeeType = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpType", Employeetype)
            };
            DataTable dt = SQL.ExecuteDatatable("EmployeeType_Check", EmployeeType);
            return dt;
        }



        public DataTable CheckWagesType_AlreadyExist(string Wagestype)
        {
            System.Data.SqlClient.SqlParameter[] WagesType = 
            {
                new System.Data.SqlClient.SqlParameter("@WagesTypeName", Wagestype)
            };
            DataTable dt = SQL.ExecuteDatatable("WagesType_Check", WagesType);
            return dt;
        }



        public DataTable DepartmentRegister(string DeptName)
        {
            System.Data.SqlClient.SqlParameter[] DepReg = 
            {
                new System.Data.SqlClient.SqlParameter("@DeptName", DeptName)
            };
            DataTable dt = SQL.ExecuteDatatable("Department_Register", DepReg);
            return dt;
        }



        public DataTable WagesTypeRegister(string WagesTypeName)
        {
            System.Data.SqlClient.SqlParameter[] WagesReg = 
            {
                new System.Data.SqlClient.SqlParameter("@WagesTypeName", WagesTypeName)
            };
            DataTable dt = SQL.ExecuteDatatable("WagesType_Register", WagesReg);
            return dt;
        }



        public DataTable EmployeeRegister(string EmpType)
        {
            System.Data.SqlClient.SqlParameter[] EmpReg = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpType", EmpType)
            };
            DataTable dt = SQL.ExecuteDatatable("EmployeeType_Register", EmpReg);
            return dt;
        }

        public DataTable Sample_check()
        {
            System.Data.SqlClient.SqlParameter[] dd_company = 
            {
            };
            DataTable dt = SQL.ExecuteDatatable("Sample_Check", dd_company);
            return dt;
        }


        //public DataTable EmployeeDetails(EmployeeDetailsClass objEmpDetails)
        //{
        //    System.Data.SqlClient.SqlParameter[] user = 
        //    {
        //        new System.Data.SqlClient.SqlParameter("@Ccode",objEmpDetails.Ccode),
        //        new System.Data.SqlClient.SqlParameter("@Lcode",objEmpDetails.Lcode),
        //        new System.Data.SqlClient.SqlParameter("@TokenNo",objEmpDetails.TokenNo),
        //        new System.Data.SqlClient.SqlParameter("@ExistingNo",objEmpDetails.ExistingNo),
        //        new System.Data.SqlClient.SqlParameter("@MachineID",objEmpDetails.MachineID),
        //        new System.Data.SqlClient.SqlParameter("@ShiftType",objEmpDetails.ShiftType),
        //        new System.Data.SqlClient.SqlParameter("@Category",objEmpDetails.Category),
        //        new System.Data.SqlClient.SqlParameter("@SubCategory",objEmpDetails.SubCategory),
        //        new System.Data.SqlClient.SqlParameter("@FirstName",objEmpDetails.FirstName),
        //        new System.Data.SqlClient.SqlParameter("@Initial",objEmpDetails.Initial),
        //        new System.Data.SqlClient.SqlParameter("@Gender",objEmpDetails.Gender),
        //        new System.Data.SqlClient.SqlParameter("@DOB",objEmpDetails.DOB),
        //        new System.Data.SqlClient.SqlParameter("@Age",objEmpDetails.Age),
        //        new System.Data.SqlClient.SqlParameter("@MartialStatus",objEmpDetails.MaritalStatus),
        //        new System.Data.SqlClient.SqlParameter("@DOJ",objEmpDetails.DOJ),
        //        new System.Data.SqlClient.SqlParameter("@Department",objEmpDetails.Department),
        //        new System.Data.SqlClient.SqlParameter("@Desgination",objEmpDetails.Desgination),
        //        new System.Data.SqlClient.SqlParameter("@BaseSalary",objEmpDetails.BaseSalary),
        //        new System.Data.SqlClient.SqlParameter("@PFNumber",objEmpDetails.PFNumber),
        //        new System.Data.SqlClient.SqlParameter("@Nominee",objEmpDetails.Nominee),
        //        new System.Data.SqlClient.SqlParameter("@ESINumber",objEmpDetails.ESINumber),
        //        new System.Data.SqlClient.SqlParameter("@StdWorkingHrs",objEmpDetails.StdWorkingHrs),
        //        new System.Data.SqlClient.SqlParameter("@OTEligible",objEmpDetails.OTEligible),
        //        new System.Data.SqlClient.SqlParameter("@Nationality",objEmpDetails.Nationality),
        //        new System.Data.SqlClient.SqlParameter("@Qualification",objEmpDetails.Qualification),
        //        new System.Data.SqlClient.SqlParameter("@FamilyDetails",objEmpDetails.FamilyDetails),
        //        new System.Data.SqlClient.SqlParameter("@RecuritThr", objEmpDetails.RecuritThr),
        //        new System.Data.SqlClient.SqlParameter("@IdentiMark1",objEmpDetails.IdentiMark1),
        //        new System.Data.SqlClient.SqlParameter("@IdentiMark2",objEmpDetails.IdentiMark2),
        //        new System.Data.SqlClient.SqlParameter("@BloodGroup",objEmpDetails.BloodGroup),
        //        new System.Data.SqlClient.SqlParameter("@Handicapped",objEmpDetails.Handicapped),
        //        new System.Data.SqlClient.SqlParameter("@Height",objEmpDetails.Height),
        //        new System.Data.SqlClient.SqlParameter("@Weight",objEmpDetails.Weight),
        //        new System.Data.SqlClient.SqlParameter("@Address1",objEmpDetails.PermanentAddr),
        //        new System.Data.SqlClient.SqlParameter("@Address2",objEmpDetails.TempAddress),
        //        new System.Data.SqlClient.SqlParameter("@BankName",objEmpDetails.BankName),
        //        new System.Data.SqlClient.SqlParameter("@BranchCode",objEmpDetails.BranchCode),
        //        new System.Data.SqlClient.SqlParameter("@AccountNo", objEmpDetails.AccountNo),
        //        new System.Data.SqlClient.SqlParameter("@IsActive",objEmpDetails.IsActive),
        //        new System.Data.SqlClient.SqlParameter("@IsNonAdmin",objEmpDetails.IsNonAdmin),
        //        new System.Data.SqlClient.SqlParameter("@OTHrs", objEmpDetails.OTHrs),
        //        new System.Data.SqlClient.SqlParameter("@WagesType",objEmpDetails.WagesType),
        //        new System.Data.SqlClient.SqlParameter("@ParentsMobile1",objEmpDetails.ParentsMobile1),
        //        new System.Data.SqlClient.SqlParameter("@ParentsMobile2",objEmpDetails.ParentsMobile2),
        //        new System.Data.SqlClient.SqlParameter("@EmpMobile",objEmpDetails.EmpMobile),
        //        new System.Data.SqlClient.SqlParameter("@Religion",objEmpDetails.Religion),
        //        new System.Data.SqlClient.SqlParameter("@BusNo",objEmpDetails.BusNo),
        //        new System.Data.SqlClient.SqlParameter("@Reference", objEmpDetails.Reference),
        //        new System.Data.SqlClient.SqlParameter("@PermanentDst",objEmpDetails.PermanentDst),
        //        new System.Data.SqlClient.SqlParameter("@TempDst",objEmpDetails.TempDst),
        //        new System.Data.SqlClient.SqlParameter("@WeekOff", objEmpDetails.WeekOff),
        //        new System.Data.SqlClient.SqlParameter("@ESICode",objEmpDetails.ESICode),
        //        new System.Data.SqlClient.SqlParameter("@PFdoj", objEmpDetails.PFdoj),
        //        new System.Data.SqlClient.SqlParameter("@ESIdoj", objEmpDetails.ESIdoj),
        //        new System.Data.SqlClient.SqlParameter("@IFSCCode",objEmpDetails.IFSCCode),
        //        new System.Data.SqlClient.SqlParameter("@PFEligible",objEmpDetails.PFEligible),
        //        new System.Data.SqlClient.SqlParameter("@ESIEligible",objEmpDetails.ESIEligible),
        //        new System.Data.SqlClient.SqlParameter("@BusRoute",objEmpDetails.BusRoute),
        //        new System.Data.SqlClient.SqlParameter("@HostelRmNo",objEmpDetails.HostelRmNo),    



        //        //new System.Data.SqlClient.SqlParameter("@AdharNo",objEmpDetails.AdharNo),
        //        //new System.Data.SqlClient.SqlParameter("@VoterID",objEmpDetails.VoterID),
        //        //new System.Data.SqlClient.SqlParameter("@DrivingLicence",objEmpDetails.DrivingLicence),
        //        //new System.Data.SqlClient.SqlParameter("@PassportNo",objEmpDetails.PassportNo),
        //        //new System.Data.SqlClient.SqlParameter("@RationCardNo",objEmpDetails.RationCardNo),
        //        //new System.Data.SqlClient.SqlParameter("@panCardNo",objEmpDetails.panCardNo),
        //        //new System.Data.SqlClient.SqlParameter("@SmartCardNo",objEmpDetails.SmartCardNo),
        //        //new System.Data.SqlClient.SqlParameter("@Other",objEmpDetails.Other),

        //        //new System.Data.SqlClient.SqlParameter("@CertificateNo",objEmpDetails.CertificateNumber),
        //        //new System.Data.SqlClient.SqlParameter("@CertificateDate",objEmpDetails.CertificateDate),
        //        //new System.Data.SqlClient.SqlParameter("@NextDewDate",objEmpDetails.NextNewDate),
        //        //new System.Data.SqlClient.SqlParameter("@TypesOfCertificate",objEmpDetails.TypesOfCertificate),
        //        //new System.Data.SqlClient.SqlParameter("@IsAdolescent",objEmpDetails.ISAdolescent),
        //        //new System.Data.SqlClient.SqlParameter("@Remarks",objEmpDetails.Remarks),

        //        //new System.Data.SqlClient.SqlParameter("@BankName",objEmpDetails.BankName),
        //        //new System.Data.SqlClient.SqlParameter("@BranchCode",objEmpDetails.BranchCode),
        //        //new System.Data.SqlClient.SqlParameter("@AcountNumber",objEmpDetails.AccountNo),
        //        //new System.Data.SqlClient.SqlParameter("@IFSCNumber",objEmpDetails.IFSCnumber),


        //        //new System.Data.SqlClient.SqlParameter("@ExpInYear",objEmpDetails.ExpInYear),
        //        //new System.Data.SqlClient.SqlParameter("@ExpInMonth",objEmpDetails.ExpInMonth),
        //        //new System.Data.SqlClient.SqlParameter("@CompanyName",objEmpDetails.CompanyName),
        //        //new System.Data.SqlClient.SqlParameter("@ContactName",objEmpDetails.ContactName),


        //        //new System.Data.SqlClient.SqlParameter("@PeriodFrom",objEmpDetails.PeriodFrom),
        //        //new System.Data.SqlClient.SqlParameter("@periodTo",objEmpDetails.PeriodTo),
        //        //new System.Data.SqlClient.SqlParameter("@MobileNo1",objEmpDetails.MobileNo1),
        //        //new System.Data.SqlClient.SqlParameter("@MobileNo2",objEmpDetails.MobileNo2),
        //        //new System.Data.SqlClient.SqlParameter("@PhoneNo1",objEmpDetails.PhoneNo1),
        //        //new System.Data.SqlClient.SqlParameter("@PhoneNo2",objEmpDetails.PhoneNo2),







        //      };
        //    DataTable dt = SQL.ExecuteDatatable("EmployeeDetails_SP", user);
        //    return dt;
        //}

        public DataTable AdolescentDetails(AdolscentDetailsClass objAdlDetails)
        {
            System.Data.SqlClient.SqlParameter[] user = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode",objAdlDetails.Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode",objAdlDetails.Lcode),
                new System.Data.SqlClient.SqlParameter("@AdoleMode",objAdlDetails.Mode),
                new System.Data.SqlClient.SqlParameter("@ExistingNo",objAdlDetails.ExistingNo),
                new System.Data.SqlClient.SqlParameter("@MachineID",objAdlDetails.MachineID),
                new System.Data.SqlClient.SqlParameter("@DOB",objAdlDetails.DOB),
                new System.Data.SqlClient.SqlParameter("@AdlCertificateNo",objAdlDetails.AdlCertificateNo),
                new System.Data.SqlClient.SqlParameter("@AdlIssuseDate",objAdlDetails.AdlIssuseDate),
                new System.Data.SqlClient.SqlParameter("@AdlNextDueDate",objAdlDetails.AdlNextDueDate),
                new System.Data.SqlClient.SqlParameter("@AdlCertificateType",objAdlDetails.AdlCertificateType),
                new System.Data.SqlClient.SqlParameter("@Remarks",objAdlDetails.AdlRemarks)
            };
            DataTable dt = SQL.ExecuteDatatable("EmpAdolescentdetails_SP", user);
            return dt;
        }

        public DataTable ExperienceDetails(ExperienceDetailsClass objExpDetails)
        {
            System.Data.SqlClient.SqlParameter[] user = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode",objExpDetails.Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode",objExpDetails.Lcode),
                new System.Data.SqlClient.SqlParameter("@TokenNo",objExpDetails.TokenNo),
                new System.Data.SqlClient.SqlParameter("@ExistingNo",objExpDetails.ExistingNo),
                new System.Data.SqlClient.SqlParameter("@MachineID",objExpDetails.MachineID),
                new System.Data.SqlClient.SqlParameter("@YearExp",objExpDetails.YearExp),
                new System.Data.SqlClient.SqlParameter("@CompName",objExpDetails.CompName),
                new System.Data.SqlClient.SqlParameter("@Designation",objExpDetails.Designation),
                new System.Data.SqlClient.SqlParameter("@Department",objExpDetails.Department),
                new System.Data.SqlClient.SqlParameter("@PeriodFrom",objExpDetails.PeriodFrom),
                new System.Data.SqlClient.SqlParameter("@PeriodTo",objExpDetails.PeriodTo),
                new System.Data.SqlClient.SqlParameter("@ContactName",objExpDetails.ContactName),
                new System.Data.SqlClient.SqlParameter("@Mobile",objExpDetails.Mobile)
            };
            DataTable dt = SQL.ExecuteDatatable("EmpExperiencedetails_SP", user);
            return dt;
        }

        public DataTable EpayEmployeeDetails(EmployeeDetailsEpayClass objEpayEmpDetails)
        {
            System.Data.SqlClient.SqlParameter[] user = 
            {
                new System.Data.SqlClient.SqlParameter("@TokenNo",objEpayEmpDetails.TokenNo),
                new System.Data.SqlClient.SqlParameter("@MachineID",objEpayEmpDetails.MachineID),
                new System.Data.SqlClient.SqlParameter("@ExistingNo",objEpayEmpDetails.ExistingNo),
                new System.Data.SqlClient.SqlParameter("@EmpName",objEpayEmpDetails.EmpName),
                new System.Data.SqlClient.SqlParameter("@FatherName",objEpayEmpDetails.FatherName),
                new System.Data.SqlClient.SqlParameter("@Gender",objEpayEmpDetails.Gender),
                new System.Data.SqlClient.SqlParameter("@DOB",objEpayEmpDetails.DOB),
                new System.Data.SqlClient.SqlParameter("@PermanentAddr",objEpayEmpDetails.PermanentAddr),
                new System.Data.SqlClient.SqlParameter("@TempAddress",objEpayEmpDetails.TempAddress),
                new System.Data.SqlClient.SqlParameter("@PermanentTlk",objEpayEmpDetails.PermanentTlk),
                new System.Data.SqlClient.SqlParameter("@PermanentDst",objEpayEmpDetails.PermanentDst),
                new System.Data.SqlClient.SqlParameter("@EmpMobile",objEpayEmpDetails.EmpMobile),
                new System.Data.SqlClient.SqlParameter("@Qualification",objEpayEmpDetails.Qualification),
                new System.Data.SqlClient.SqlParameter("@Department",objEpayEmpDetails.Department),
                new System.Data.SqlClient.SqlParameter("@Desgination",objEpayEmpDetails.Desgination),
                new System.Data.SqlClient.SqlParameter("@Category",objEpayEmpDetails.Category),
                new System.Data.SqlClient.SqlParameter("@Ccode",objEpayEmpDetails.Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode",objEpayEmpDetails.Lcode),
                new System.Data.SqlClient.SqlParameter("@MaritalStatus",objEpayEmpDetails.MaritalStatus),


                new System.Data.SqlClient.SqlParameter("@IsActive",objEpayEmpDetails.IsActive),
                new System.Data.SqlClient.SqlParameter("@CreatedBy",objEpayEmpDetails.CreatedBy),
                new System.Data.SqlClient.SqlParameter("@CreatedDate",objEpayEmpDetails.CreatedDate),
                new System.Data.SqlClient.SqlParameter("@EmpType",objEpayEmpDetails.EmployeeType),
                new System.Data.SqlClient.SqlParameter("@Uneducated",objEpayEmpDetails.UnEducated),

                new System.Data.SqlClient.SqlParameter("@NonPFGrade",objEpayEmpDetails.NonPFGrade),
                new System.Data.SqlClient.SqlParameter("@IsValid",objEpayEmpDetails.IsValid),
                new System.Data.SqlClient.SqlParameter("@IsLeave",objEpayEmpDetails.IsLeave),
                new System.Data.SqlClient.SqlParameter("@Status",objEpayEmpDetails.Status),
                new System.Data.SqlClient.SqlParameter("@RoleCode",objEpayEmpDetails.RoleCode),
                new System.Data.SqlClient.SqlParameter("@Initial",objEpayEmpDetails.Initial),

                
            };
            DataTable dt = SQL.ExecuteDatatable("EPayEmployeeDetails_SP", user);
            return dt;
        }



        public DataTable EpayProfileDetails(OfficialProfileEPayClass objEpayProfDetails)
        {
            System.Data.SqlClient.SqlParameter[] user = 
            {
                new System.Data.SqlClient.SqlParameter("@TokenNo",objEpayProfDetails.TokenNo),
                new System.Data.SqlClient.SqlParameter("@MachineID",objEpayProfDetails.MachineID),
                new System.Data.SqlClient.SqlParameter("@DOJ",objEpayProfDetails.DOJ),
                new System.Data.SqlClient.SqlParameter("@ESINumber",objEpayProfDetails.ESINumber),
                new System.Data.SqlClient.SqlParameter("@PFNumber",objEpayProfDetails.PFNumber),
                new System.Data.SqlClient.SqlParameter("@BranchCode",objEpayProfDetails.BranchCode),
                new System.Data.SqlClient.SqlParameter("@BankName",objEpayProfDetails.BankName),
                new System.Data.SqlClient.SqlParameter("@AccountNo",objEpayProfDetails.AccountNo),
                new System.Data.SqlClient.SqlParameter("@WagesType",objEpayProfDetails.WagesType),
                new System.Data.SqlClient.SqlParameter("@Basic",objEpayProfDetails.Basic),
                new System.Data.SqlClient.SqlParameter("@ESIEligible",objEpayProfDetails.ESIEligible),
                new System.Data.SqlClient.SqlParameter("@PFEligible",objEpayProfDetails.PFEligible),
                new System.Data.SqlClient.SqlParameter("@Ccode",objEpayProfDetails.Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode",objEpayProfDetails.Lcode),
                //new System.Data.SqlClient.SqlParameter("@ESICode",objEpayProfDetails.ESICode),
                new System.Data.SqlClient.SqlParameter("@PFdoj",objEpayProfDetails.PFdoj),
                new System.Data.SqlClient.SqlParameter("@ESIdoj",objEpayProfDetails.ESIdoj),
                

            };
            DataTable dt = SQL.ExecuteDatatable("EPayProfileDetails_SP", user);
            return dt;
        }


        public DataTable EpaySalaryMasterDetails(SalaryMasterEpayClass objEpaySalaryMst)
        {
            System.Data.SqlClient.SqlParameter[] user = 
            {
                new System.Data.SqlClient.SqlParameter("@TokenNo",objEpaySalaryMst.TokenNo),
                new System.Data.SqlClient.SqlParameter("@Basic",objEpaySalaryMst.Basic),
                new System.Data.SqlClient.SqlParameter("@Allowance1",objEpaySalaryMst.Allowance1),
                new System.Data.SqlClient.SqlParameter("@Allowance2",objEpaySalaryMst.Allowance2),
                new System.Data.SqlClient.SqlParameter("@Deduction1",objEpaySalaryMst.Deduction1),
                new System.Data.SqlClient.SqlParameter("@Deduction2",objEpaySalaryMst.Deduction2),
                new System.Data.SqlClient.SqlParameter("@Ccode",objEpaySalaryMst.Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode",objEpaySalaryMst.Lcode),
                new System.Data.SqlClient.SqlParameter("@BaseSalary",objEpaySalaryMst.BaseSalary),
                 new System.Data.SqlClient.SqlParameter("@PFSalary",objEpaySalaryMst.PFSalary),
                
            };
            DataTable dt = SQL.ExecuteDatatable("EPaySalaryMasterDetails_SP", user);
            return dt;
        }



        public DataTable SaveDocumentReg(DocumentClass objdoc)
        {
            System.Data.SqlClient.SqlParameter[] user = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo",objdoc.EmployeeNo),
                new System.Data.SqlClient.SqlParameter("@EmpName",objdoc.EmployeeName),
                new System.Data.SqlClient.SqlParameter("@MachineID",objdoc.MachineID),
                new System.Data.SqlClient.SqlParameter("@DocType",objdoc.DocumentType),
                new System.Data.SqlClient.SqlParameter("@DocNo",objdoc.DocumentNo),
                new System.Data.SqlClient.SqlParameter("@DocDesc",objdoc.DocumentDesc),
                new System.Data.SqlClient.SqlParameter("@pathfile",objdoc.Pathfile),
                new System.Data.SqlClient.SqlParameter("@Ccode",objdoc.Ccode),
                new System.Data.SqlClient.SqlParameter("@Lcode",objdoc.Lcode),
                
            };
            DataTable dt = SQL.ExecuteDatatable("DocumentRegister_SP", user);
            return dt;
        }




        public DataTable ExperienceDisplay(string EmpNo)
        {
            System.Data.SqlClient.SqlParameter[] EmpNo1 = 
            {
                new System.Data.SqlClient.SqlParameter("@EmpNo", EmpNo),
            };
            DataTable dt = SQL.ExecuteDatatable("EmpNoExperience_SP", EmpNo1);
            return dt;
        }

        public DataTable DeleteExperience(string SNo)
        {
            System.Data.SqlClient.SqlParameter[] EmpNo1 = 
            {
                new System.Data.SqlClient.SqlParameter("@SNo",SNo),
            };
            DataTable dt = SQL.ExecuteDatatable("DeleteExperience_SP", EmpNo1);
            return dt;
        }

        public DataTable CheckExistingNO(string ExstNo)
        {
            System.Data.SqlClient.SqlParameter[] ExstNo1 = 
            {
                new System.Data.SqlClient.SqlParameter("@ExstNo", ExstNo),
            };
            DataTable dt = SQL.ExecuteDatatable("CheckExistingNo_SP", ExstNo1);
            return dt;
        }

        public DataTable CheckAndReturn(string MchID, string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] user = 
            {
                 new System.Data.SqlClient.SqlParameter("@MchID", MchID),
                 new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                 new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("CheckAndReturnDetails_SP", user);
            return dt;
        }

        public DataTable Manual_Data(string TokenNo)
        {
            System.Data.SqlClient.SqlParameter[] Manu = 
            {
                new System.Data.SqlClient.SqlParameter("@TokenNo", TokenNo)
            };
            DataTable dt = SQL.ExecuteDatatable("ManualAttend_Fetch", Manu);
            return dt;
        }


        public DataTable CheckStaff(string TokenNo, string SessionCcode, string SessionLcode)
        {
            System.Data.SqlClient.SqlParameter[] Manu = 
            {
                new System.Data.SqlClient.SqlParameter("@TokenNo", TokenNo),
                new System.Data.SqlClient.SqlParameter("@Ccode", SessionCcode),
                new System.Data.SqlClient.SqlParameter("@Lcode", SessionLcode)
            };
            DataTable dt = SQL.ExecuteDatatable("CheckStaff_SP", Manu);
            return dt;
        }




        public DataTable DisplayManualAttend(string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] user = 
            {
                 new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                 new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("ManualAttendanceDisplay_SP", user);
            return dt;
        }



        public DataTable DisplayTimeDelete()
        {
            System.Data.SqlClient.SqlParameter[] user = 
            {
              
            };
            DataTable dt = SQL.ExecuteDatatable("TimeDeleteDisplay_SP", user);
            return dt;
        }


        public DataTable IPAddressForAll(string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] ddloc = 
            {
                 new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                 new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("IPAddressForAll_SP", ddloc);
            return dt;
        }

        public DataTable IPAddressALL(string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] ddIPAddr = 
            {
                new System.Data.SqlClient.SqlParameter("@Ccode",Ccode),
                 new System.Data.SqlClient.SqlParameter("@Lcode",Lcode)
             };
            DataTable dt = SQL.ExecuteDatatable("IPAddress_SP", ddIPAddr);
            return dt;
        }


        public DataSet read(string Query_Val)
        {
            System.Data.SqlClient.SqlParameter[] AllTypeEmployeeDetails =
            {
                new System.Data.SqlClient.SqlParameter("@Query",Query_Val)
            };
            DataSet dt1 = SQL.ExecuteDataset("RptEmployeeDetailsMultiple_SP", AllTypeEmployeeDetails);
            return dt1;
        }

        public DataTable ReturnMultipleValue(string Query_Val)
        {
            System.Data.SqlClient.SqlParameter[] AllTypeEmployeeDetails =
            {
                new System.Data.SqlClient.SqlParameter("@Query",Query_Val)
            };
            DataTable dt = SQL.ExecuteDatatable("ReturnMultipleValue_SP", AllTypeEmployeeDetails);
            return dt;
        }

        public DataTable dropdown_TokenNumber(string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] ddloc = 
            {
              new System.Data.SqlClient.SqlParameter("@Ccode",Ccode),
              new System.Data.SqlClient.SqlParameter("@Lcode",Lcode)
             };
            DataTable dt = SQL.ExecuteDatatable("DropDown_TokenNumber", ddloc);
            return dt;
        }



        public DataTable Dropdown_ShiftType(string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] ddloc = 
            {
              new System.Data.SqlClient.SqlParameter("@Ccode",Ccode),
              new System.Data.SqlClient.SqlParameter("@Lcode",Lcode)
             };
            DataTable dt = SQL.ExecuteDatatable("DropDown_ShiftType", ddloc);
            return dt;
        }

        public DataTable BelowFourHours(string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] ddloc = 
            {
                 new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                 new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("BelowFourHours_SP", ddloc);
            return dt;
        }

        public DataTable ShiftMSt(string Ccode, string Lcode, string shift, string shifttype)
        {
            System.Data.SqlClient.SqlParameter[] ddloc = 
            {
                 new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                 new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                 new System.Data.SqlClient.SqlParameter("@shift", shift),
                 new System.Data.SqlClient.SqlParameter("@shifttype", shifttype)
            };
            DataTable dt = SQL.ExecuteDatatable("ShiftMstReport_SP", ddloc);
            return dt;
        }

        public DataTable ShiftMStForAll(string Ccode, string Lcode, string shifttype)
        {
            System.Data.SqlClient.SqlParameter[] ddloc = 
            {
                 new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                 new System.Data.SqlClient.SqlParameter("@Lcode", Lcode),
                 new System.Data.SqlClient.SqlParameter("@shifttype", shifttype)
            };
            DataTable dt = SQL.ExecuteDatatable("ShiftMstReportforALL_SP", ddloc);
            return dt;
        }

        public DataTable IPAddress(string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] ddloc = 
            {
                 new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                 new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("IPAddress_SP", ddloc);
            return dt;
        }

        public DataTable LateINIPAddress(string Ccode, string Lcode)
        {
            System.Data.SqlClient.SqlParameter[] ddloc = 
            {
                 new System.Data.SqlClient.SqlParameter("@Ccode", Ccode),
                 new System.Data.SqlClient.SqlParameter("@Lcode", Lcode)
            };
            DataTable dt = SQL.ExecuteDatatable("LateINIPAddress_SP", ddloc);
            return dt;
        }

        public DataTable RptEmployeeMultipleDetails(string Query_Val)
        {
            System.Data.SqlClient.SqlParameter[] AllTypeEmployeeDetails =
            {
                new System.Data.SqlClient.SqlParameter("@Query",Query_Val)
            };
            DataTable dt = SQL.ExecuteDatatable("RptEmployeeDetailsMultiple_SP", AllTypeEmployeeDetails);
            return dt;
        }

       

   


    }

}

