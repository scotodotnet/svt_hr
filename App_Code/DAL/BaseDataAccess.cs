﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Data.SqlClient;
/// <summary>
/// Summary description for BaseDataAccess
/// </summary>

namespace Altius.DataAccessLayer
{
    public class BaseDataAccess
    {
        public BaseDataAccess()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public static string ConnectionString
        {
            get
            {
                return ConfigurationManager.AppSettings["ConnectionString"].ToString();
            }
        }
        public static string eAlertConnectionString
        {
            get
            {
                return ConfigurationManager.AppSettings["eAlertConnectionString"].ToString();
            }
        }
    }
}
