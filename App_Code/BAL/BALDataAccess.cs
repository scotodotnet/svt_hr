﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.DataAccessLayer.DALDataAccess;


/// <summary>
/// Summary description for BALDataAccess
/// </summary>
namespace Altius.BusinessAccessLayer.BALDataAccess
{
    public class BALDataAccess : BaseBusiness
    {
        DALDataAccess objdata = new DALDataAccess();

        public DataTable CheckUser_Login(string Ccode, string Lcode, string UserName, string usert)
        {
            return objdata.CheckUser_Login(Ccode, Lcode, UserName, usert);
        }


        public DataTable CheckAdminRights(string SessionCcode, string SessionLcode, string UserName)
        {
            return objdata.CheckAdminRights(SessionCcode, SessionLcode, UserName);
        }


        public String UserCreationCcodeCounting(string CompanyCode, string LocationCode)
        {
            return objdata.UserCreationCcodeCounting(CompanyCode, LocationCode);
        }

        public DataTable UserCreationRegistration(UserCreationClass objUsercreation)
        {
            return objdata.UserCreationRegistration(objUsercreation);
        }


        public DataTable EditUserCreation(UserCreationClass objUsercreation)
        {
            return objdata.EditUserCreation(objUsercreation);
        }

        public DataTable CheckEmpType_AlreadyExist(string Employeetype)
        {
            return objdata.CheckEmpType_AlreadyExist(Employeetype);
        }

        public DataTable DesginationRegister(string Employeetype)
        {
            return objdata.DesginationRegister(Employeetype);
        }

        public DataTable EditDesignation(int DesignNo)
        {
            return objdata.EditDesignation(DesignNo);
        }


        public DataTable EditEmployeeType(int TypeID)
        {
            return objdata.EditEmployeeType(TypeID);
        }

        public DataTable EditWagesType(string WgsTypeNo)
        {
            return objdata.EditWagesType(WgsTypeNo);
        }

        public DataTable CheckEmployeeDesignation(int DesignNo)
        {
            return objdata.CheckEmployeeDesignation(DesignNo);
        }

        public DataTable CheckingEmployeeType(string TypeName)
        {
            return objdata.CheckingEmployeeType(TypeName);
        }

        public DataTable CheckingWagesType(string TypeName)
        {
            return objdata.CheckingWagesType(TypeName);
        }

        public DataTable UserCreationDisplay(string Ccode, string Lcode)
        {
            return objdata.UserCreationDisplay(Ccode, Lcode);
        }


        public DataTable DepartmentIncentiveDisplay(string Ccode, string Lcode)
        {
            return objdata.DepartmentIncentiveDisplay(Ccode, Lcode);
        }


        public DataTable DepartmentDisplay()
        {
            return objdata.DepartmentDisplay();
        }


        public DataTable DeleteUserCreation(int SNo)
        {
            return objdata.DeleteUserCreation(SNo);
        }



        public DataTable ChechDepartmentPermisssion(string permtype, string deptname, string tt, string CompanyCode, string LocationCode, string Monthname)
        {
            return objdata.ChechDepartmentPermisssion(permtype, deptname, tt, CompanyCode, LocationCode, Monthname);
        }

        public DataTable CheckingEmployee(string DeptName)
        {
            return objdata.CheckingEmployee(DeptName);
        }

        public DataTable CheckingDesignEmployee(string DeptName)
        {
            return objdata.CheckingDesignEmployee(DeptName);
        }



        public DataTable DeleteDepartment(int DeptCode)
        {
            return objdata.DeleteDepartment(DeptCode);
        }

        public DataTable DeleteDesgination(int DesignNo)
        {
            return objdata.DeleteDesgination(DesignNo);
        }

        public DataTable DeleteEmployeeeType(int TypeID)
        {
            return objdata.DeleteEmployeeeType(TypeID);
        }

        public DataTable DeleteWagesType(string WgsTypeNo)
        {
            return objdata.DeleteWagesType(WgsTypeNo);
        }

        public DataTable EditUserCreation(int DeptCode)
        {
            return objdata.EditUserCreation(DeptCode);
        }


        public DataTable EditDepartment(string DeptCode)
        {
            return objdata.EditDepartment(DeptCode);
        }


        public DataTable CheckEmployeeDepartment(int DeptCode)
        {
            return objdata.CheckEmployeeDepartment(DeptCode);
        }

        public DataTable DesginationDisplay()
        {
            return objdata.DesginationDisplay();
        }



        public DataTable EmployeeTypeDisplay()
        {
            return objdata.EmployeeTypeDisplay();
        }

        public DataTable WagesTypeDisplay()
        {
            return objdata.WagesTypeDisplay();
        }

        public DataTable AdministrationRegistration(UserCreationClass objUsercreation)
        {
            return objdata.AdministrationRegistration(objUsercreation);
        }


        public DataTable dropdown_UserName(string Ccode, string Lcode)
        {
            return objdata.dropdown_UserName(Ccode, Lcode);
        }


        public DataTable dropdown_WagesType()
        {
            return objdata.dropdown_WagesType();
        }



        public DataTable DesignationUpdate(string DesignName, int ss)
        {
            return objdata.DesignationUpdate(DesignName, ss);
        }


        public DataTable EmployeeTypeUpdate(string TypeName, int ss)
        {
            return objdata.EmployeeTypeUpdate(TypeName, ss);
        }



        public DataTable WagesTypeUpdate(string TypeName, int ss)
        {
            return objdata.WagesTypeUpdate(TypeName, ss);
        }

        public DataTable EmployeeDisplay(string Ccode, string Lcode)
        {
            return objdata.EmployeeDisplay(Ccode, Lcode);
        }

        public DataTable DepartmmentUpdate(string DeptName, int ss)
        {
            return objdata.DepartmmentUpdate(DeptName, ss);
        }
        public DataTable EmployeeTypeRegister(string Employeetype)
        {
            return objdata.EmployeeTypeRegister(Employeetype);
        }


        public DataTable Dropdown_Company()
        {
            return objdata.Dropdown_Company();
        }

        public DataTable dropdown_LeaveType()
        {
            return objdata.dropdown_LeaveType()
;
        }

        public DataTable UpdateEmployeeDetails(EmployeeDetailsClass objEmpDetails)
        {
            return objdata.UpdateEmployeeDetails(objEmpDetails);
        }

        public DataTable UpdateEpayEmployeeDetails(EmployeeDetailsEpayClass objEpayEmpDetails)
        {
            return objdata.UpdateEpayEmployeeDetails(objEpayEmpDetails);
        }

        public DataTable UpdateEpayProfileDetails(OfficialProfileEPayClass objEpayProfDetails)
        {
            return objdata.UpdateEpayProfileDetails(objEpayProfDetails);
        }

        public DataTable EpayUpdateSalaryMasterDetails(SalaryMasterEpayClass objEpaySalaryMst)
        {
            return objdata.EpayUpdateSalaryMasterDetails(objEpaySalaryMst);
        }

        public DataTable Dropdown_Department()
        {
            return objdata.Dropdown_Department();
        }


        public DataTable DropDown_EmpType()
        {
            return objdata.DropDown_EmpType();
        }


        public DataTable DropDown_WagesType()
        {
            return objdata.DropDown_WagesType();
        }

        public DataTable Dropdown_Desgination()
        {
            return objdata.Dropdown_Desgination();
        }

        public DataTable DropDown_LeaveTypeMale()
        {
            return objdata.DropDown_LeaveTypeMale();
        }



        public DataTable Dropdown_PayPeriod()
        {
            return objdata.Dropdown_PayPeriod();
        }

        public string Verification_verify()
        {
            return objdata.Verification_verify();
        }
        public DataTable AltiusLogin(string uname, string pwd1)
        {
            return objdata.AltiusLogin(uname, pwd1);
        }

        public DataTable dropdown_ParticularLocation(string Ccode, string Lcode)
        {
            return objdata.dropdown_ParticularLocation(Ccode, Lcode);
        }



        public DataTable dropdown_location(string Ccode)
        {
            return objdata.dropdown_location(Ccode);
        }
        public DataTable Value_Verify()
        {
            return objdata.Value_Verify();
        }

        public DataTable DropDown_MachineID()
        {
            return objdata.DropDown_MachineID();
        }

        public string ServerDate()
        {
            return objdata.ServerDate();
        }
        public DataTable UserLogin(string uname, string pwd)
        {
            return objdata.UserLogin(uname, pwd);
        }


        public DataTable CheckUserCreation(string ccode, string lcode, string uname, string pwd)
        {
            return objdata.CheckUserCreation(ccode, lcode, uname, pwd);
        }

        public DataTable Login_Rights(string ccode, string lcode, string uname)
        {
            return objdata.Login_Rights(ccode, lcode, uname);
        }

        public DataTable dropdown_loc(string Ccode)
        {
            return objdata.dropdown_loc(Ccode);
        }


        public DataTable CheckCompanyMst(string Ccode, string Cname)
        {
            return objdata.CheckCompanyMst(Ccode, Cname);
        }


        public DataTable CheckLocationMst(string Ccode, string Lcode)
        {
            return objdata.CheckLocationMst(Ccode, Lcode);
        }

        public void CompanyRegistration(CompanyMasterClass Comobj)
        {
            objdata.CompanyRegistration(Comobj);
        }


        public void LocationRegistration(CompanyMasterClass Comobj)
        {
            objdata.LocationRegistration(Comobj);
        }

        //public void DeductionAndOTRegistration(DeductionAndOTClass objdedOT)
        //{
        //    objdata.DeductionAndOTRegistration(objdedOT);
        //}



        public DataTable DepartmentRegister(string DeptName)
        {
            return objdata.DepartmentRegister(DeptName);
        }

        public DataTable EmployeeRegister(string EmpType)
        {
            return objdata.EmployeeRegister(EmpType);
        }


        public DataTable WagesTypeRegister(string WagesType)
        {
            return objdata.WagesTypeRegister(WagesType);
        }


        public DataTable CheckDepartment_AlreadyExist(string DeptName)
        {
            return objdata.CheckDepartment_AlreadyExist(DeptName);
        }

        public DataTable CheckEmployeeType_AlreadyExist(string Employeetype)
        {
            return objdata.CheckEmployeeType_AlreadyExist(Employeetype);
        }

        public DataTable CheckWagesType_AlreadyExist(string Wagestype)
        {
            return objdata.CheckWagesType_AlreadyExist(Wagestype);
        }

        public DataTable Sample_check()
        {
            return objdata.Sample_check();
        }
        public DataTable EmpIDRegistration(EmpIDDetails objEmpID)
        {
            return objdata.EmpIDRegistration(objEmpID);
        }

        public DataTable EmpAdolescentRegistration(AdolescentClass objAdu)
        {
            return objdata.EmpAdolescentRegistration(objAdu);
        }

        public DataTable EmployeeDetails(EmployeeDetailsClass objEmpDetails)
        {
            return objdata.EmployeeDetails(objEmpDetails);
        }

        public DataTable ManualAttendanceRegister(ManualAttendanceClass objManual)
        {
            return objdata.ManualAttendanceRegister(objManual);
        }
        //public DataTable EmployeeDetails(EmployeeDetailsClass objEmpDetails)
        //{
        //    return objdata.EmployeeDetails(objEmpDetails);
        //}
        public DataTable AdolescentDetails(AdolscentDetailsClass objAdlDetails)
        {
            return objdata.AdolescentDetails(objAdlDetails);
        }
        public DataTable ExperienceDetails(ExperienceDetailsClass objExpDetails)
        {
            return objdata.ExperienceDetails(objExpDetails);
        }
        public DataTable EpayEmployeeDetails(EmployeeDetailsEpayClass objEpayEmpDetails)
        {
            return objdata.EpayEmployeeDetails(objEpayEmpDetails);
        }
        public DataTable EpayProfileDetails(OfficialProfileEPayClass objEpayProfDetails)
        {
            return objdata.EpayProfileDetails(objEpayProfDetails);
        }

        public DataTable EpaySalaryMasterDetails(SalaryMasterEpayClass objEpaySalaryMst)
        {
            return objdata.EpaySalaryMasterDetails(objEpaySalaryMst);
        }


        public DataTable CheckExistingNO(string ExstNo)
        {
            return objdata.CheckExistingNO(ExstNo);
        }

        public DataTable CheckAndReturn(string MchID, string Ccode, string Lcode)
        {
            return objdata.CheckAndReturn(MchID, Ccode, Lcode);
        }

        public DataTable ExperienceDisplay(string EmpNo)
        {
            return objdata.ExperienceDisplay(EmpNo);
        }
        public DataTable DeleteExperience(string SNo)
        {
            return objdata.DeleteExperience(SNo);
        }
        public DataTable Manual_Data(string TokenNo)
        {
            return objdata.Manual_Data(TokenNo);
        }


        public DataTable CheckStaff(string TokenNo, string SessionCcode, string SessionLcode)
        {
            return objdata.CheckStaff(TokenNo, SessionCcode, SessionLcode);
        }


        public DataSet read(string Query_Val)
        {
            return objdata.read(Query_Val);

        }

        public DataTable DisplayManualAttend(string Ccode, string Lcode)
        {
            return objdata.DisplayManualAttend(Ccode, Lcode);
        }
        public DataTable IPAddressForAll(string Ccode, string Lcode)
        {
            return objdata.IPAddressForAll(Ccode, Lcode);
        }
        public DataTable IPAddressALL(string Ccode, string Lcode)
        {
            return objdata.IPAddressALL(Ccode, Lcode);
        }

        public DataTable ReturnMultipleValue(string Query_Val)
        {
            return objdata.ReturnMultipleValue(Query_Val);
        }

        public DataTable dropdown_TokenNumber(string Ccode, string Lcode)
        {
            return objdata.dropdown_TokenNumber(Ccode, Lcode);
        }


        public DataTable Dropdown_ShiftType(string Ccode, string Lcode)
        {
            return objdata.Dropdown_ShiftType(Ccode, Lcode);
        }

        public DataTable SaveDocumentReg(DocumentClass objdoc)
        {
            return objdata.SaveDocumentReg(objdoc);
        }

        public DataTable BelowFourHours(string Ccode, string Lcode)
        {
            return objdata.BelowFourHours(Ccode, Lcode);
        }

        public DataTable ShiftMSt(string Ccode, string Lcode, string shift, string shifttype)
        {
            return objdata.ShiftMSt(Ccode, Lcode, shift, shifttype);
        }

        public DataTable ShiftMStForAll(string Ccode, string Lcode, string shifttype)
        {
            return objdata.ShiftMStForAll(Ccode, Lcode, shifttype);
        }
        public DataTable IPAddress(string Ccode, string Lcode)
        {
            return objdata.IPAddress(Ccode, Lcode);
        }
        public DataTable LateINIPAddress(string Ccode, string Lcode)
        {
            return objdata.LateINIPAddress(Ccode, Lcode);
        }

        public DataTable RptEmployeeMultipleDetails(string Query_Val)
        {
            return objdata.RptEmployeeMultipleDetails(Query_Val);
        }
        //public DataTable HBCG(string GroupName, string TotalDays, string PerDay, string GrandTotal)
        //{
        //    return objdata.HindiBoysCommission(GroupName, TotalDays, PerDay, GrandTotal);
        //}
       



        }

    }

