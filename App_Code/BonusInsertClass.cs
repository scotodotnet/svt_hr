﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for BonusInsertClass
/// </summary>
public class BonusInsertClass
{
    private string _EmpNo;
    private string _FromDate;
    private string _ToDate;
    private string _WorkingDays;
    private string _Department;
    private string _CalMode;
    private string _BasBasicAmt;
    private string _BasAllowAmt;
    private string _BasbasicAllow;
    private string _basPer;
    private string _BasPerCal;
    private string _BasDays;
    private string _BasTotalBonus;
    private string _PerBasicAmt;
    private string _PerBasicpercent;
    private string _PerTotalbasicAmt;
    private string _PerAllowance;
    private string _PerAllowancePercent;
    private string _PerTotalAllow;
    private string _PerBasicAllowance;
    private string _PerDays;
    private string _PerTotalBonus;
    private string _CreatedDate; 
	public BonusInsertClass()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    public string EmpNo
    {
        get { return _EmpNo; }
        set { _EmpNo = value; }
    }
    public string FromDate
    {
        get { return _FromDate; }
        set { _FromDate = value; }
    }
    public string ToDate
    {
        get { return _ToDate; }
        set { _ToDate = value; }
    }
    public string WorkingDays
    {
        get { return _WorkingDays; }
        set { _WorkingDays = value; }
    }
    public string Department
    {
        get { return _Department; }
        set { _Department = value; }
    }
    public string CalMode
    {
        get { return _CalMode; }
        set { _CalMode = value; }
    }
    public string BasBasicAmt
    {
        get { return _BasBasicAmt; }
        set { _BasBasicAmt = value; }
    }
    public string BasAllowAmt
    {
        get { return _BasAllowAmt; }
        set { _BasAllowAmt = value; }
    }
    public string BasbasicAllow
    {
        get { return _BasbasicAllow; }
        set { _BasbasicAllow = value; }
    }
    public string basPer
    {
        get { return _basPer; }
        set { _basPer = value; }
    }
    public string BasPerCal
    {
        get { return _BasPerCal; }
        set { _BasPerCal = value; }
    }
    public string BasDays
    {
        get { return _BasDays; }
        set { _BasDays = value; }
    }
    public string BasTotalBonus
    {
        get { return _BasTotalBonus; }
        set { _BasTotalBonus = value; }
    }
    public string PerBasicAmt
    {
        get { return _PerBasicAmt; }
        set { _PerBasicAmt = value; }
    }
    public string PerBasicpercent
    {
        get { return _PerBasicpercent; }
        set { _PerBasicpercent = value; }
    }
    public string PerTotalbasicAmt
    {
        get { return _PerTotalbasicAmt; }
        set { _PerTotalbasicAmt = value; }
    }
    public string PerAllowance
    {
        get { return _PerAllowance; }
        set { _PerAllowance = value; }
    }
    public string PerAllowancePercent
    {
        get { return _PerAllowancePercent; }
        set { _PerAllowancePercent = value; }
    }
    public string PerTotalAllow
    {
        get { return _PerTotalAllow; }
        set { _PerTotalAllow = value; }
    }
    public string PerBasicAllowance
    {
        get { return _PerBasicAllowance; }
        set { _PerBasicAllowance = value; }
    }
    public string PerDays
    {
        get { return _PerDays; }
        set { _PerDays = value; }
    }
    public string PerTotalBonus
    {
        get { return _PerTotalBonus; }
        set { _PerTotalBonus = value; }
    }
    public string CreatedDate
    {
        get { return _CreatedDate; }
        set { _CreatedDate = value; }
    }
}
