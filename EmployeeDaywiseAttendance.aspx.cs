﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;
public partial class EmployeeDaywiseAttendance : System.Web.UI.Page
{
    string EmpCode1 = "";
    string ShiftType1 = "";
    string Date1 = "";
    string Date2 = "";
    string ddlShiftType = "";
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;
    string SessionUserType;

    BALDataAccess objdata = new BALDataAccess();
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;

    string SSQL = "";
    DateTime date1 = new DateTime();
    DateTime date2 = new DateTime();
    string[] Time_Minus_Value_Check;
    string EmpName = "";
    DataTable AutoDTable = new DataTable();
    DataTable mLocalDS = new DataTable();
    DataTable da_Weekoff_check = new DataTable();
    DataTable Da_Weekoff = new DataTable();
    int intK;
    string Adolescent_Shift;
    string Division = ""; string status = "";
    DateTime EmpdateIN;
    System.Web.UI.WebControls.DataGrid grid =
                             new System.Web.UI.WebControls.DataGrid();


    public string Left_Val(string Value, int Length)
    {
        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report-Employee DayWise Report";
            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();

            EmpCode1 = Request.QueryString["EmpCode"].ToString();
            Division = Request.QueryString["Division"].ToString();
            status = Request.QueryString["status"].ToString();
            Date1 = Request.QueryString["Date1"].ToString();
            Date2 = Request.QueryString["Date2"].ToString();

            DataSet ds = new DataSet();

            DataColumn auto = new DataColumn();
            auto.AutoIncrement = true;
            auto.AutoIncrementSeed = 1;

            AutoDTable.Columns.Add("Date");
            AutoDTable.Columns.Add("1st IN");
            AutoDTable.Columns.Add("1st OUT");
            AutoDTable.Columns.Add("1st Total Hours");
            AutoDTable.Columns.Add("2nd IN");
            AutoDTable.Columns.Add("2nd OUT");
            AutoDTable.Columns.Add("2nd Total Hours");
            //AutoDTable.Columns.Add("3nd IN");
            //AutoDTable.Columns.Add("3nd OUT");
            //AutoDTable.Columns.Add("3nd Total Hours");
            AutoDTable.Columns.Add("Final Total Hours");


            if (SessionUserType == "2")
            {
                NonAdminGetAttdDayWise_Change();
            }
            else
            {

                Fill_Emp_Day_Attd_Between_Dates();
            }

            //date1 = Convert.ToDateTime(Date1);
            //date2 = Convert.ToDateTime(Date2);

            // UploadDataTableToExcel(AutoDTable);




        }

    }

    public void NonAdminGetAttdDayWise_Change()
    {
        try
        {

            string ColumnName;
            string FirstName = "";
            string DeptName = "";
            string Shift = "";
            string Category = "";
            string SubCategory = "";
            string TotalMIN = "";
            string TypeName = "";
            string ExistingCode = "";
            string Token_No_Display = "";
            string Name_Display = "";
            string T_Query = "";
            T_Query="Select ExistingCode,FirstName from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And MachineID='" + EmpCode1 + "'";
            DataTable DT_To = new DataTable();
            DT_To = objdata.RptEmployeeMultipleDetails(T_Query);
            if (DT_To.Rows.Count != 0)
            {
                Token_No_Display = DT_To.Rows[0]["ExistingCode"].ToString();
                Name_Display = DT_To.Rows[0]["FirstName"].ToString();
            }

            date1 = Convert.ToDateTime(Date1);
            // string dd =string.Format("{dd/MM/yyyy}",TxtTo.Text);
            date2 = Convert.ToDateTime(Date2);
            if (date1 > date2)
                return;

            string iEmpDet = null;

            iEmpDet = EmpCode1;

            long iRowVal = 0;

            int daysAdded = 0;
            int daycount = (int)((date2 - date1).TotalDays);

            iRowVal = daycount;

            int i = 0;
            while (daycount >= 0)
            {
                DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded));
                AutoDTable.NewRow();
                AutoDTable.Rows.Add();
                AutoDTable.Rows[i]["Date"] = Convert.ToString(dayy.ToShortDateString());
                daycount -= 1;
                daysAdded += 1;
                i++;
            }

            DataTable mLocalShift = new DataTable();
            DataTable mEmployeeDT = new DataTable();
            DataTable DtdIFReport = new DataTable();
            string Machine_ID_Str = null;



            string Date_Value_Str;
            string Date_Value_Str1;
            string OT_Week_OFF_Machine_No;
            string Time_IN_Str;


            string Time_Out_Str;
            string Total_Time_get = "";
            string Total_Time_get_1 = "";

            string time_Check_dbl = "";
            int time_Check_dbl_1 = 0;
            long Random_No_Fixed = 1;

            for (int j = 0; j < daysAdded; j++)
            {

                ColumnName = "";
                double Totol_Hours_Check = 0;
                double Totol_Hours_Check_1 = 8.2;

                Machine_ID_Str = UTF8Encryption(EmpCode1);
                OT_Week_OFF_Machine_No = EmpCode1;
                long Random_No_Get = 0;
                string Datee = AutoDTable.Rows[j]["Date"].ToString();

                Date_Value_Str = Convert.ToDateTime(Datee).ToString("yyyy/MM/dd");
                Date_Value_Str1 = Convert.ToDateTime(Date_Value_Str).AddDays(1).ToString("yyyy/MM/dd");


                SSQL = "select * from IFReport_DayAttendance where AttendanceDate = '" + Date_Value_Str + "'";
                SSQL = SSQL + " And Compcode='" + SessionCcode + "'";
                SSQL = SSQL + " And LocCode='" + SessionLcode + "'";
                SSQL = SSQL + " And MachineID='" + EmpCode1 + "'";

                DtdIFReport = objdata.RptEmployeeMultipleDetails(SSQL);

                if (DtdIFReport.Rows.Count != 0)
                {
                    string SS = DtdIFReport.Rows[0]["TimeIn"].ToString();

                    AutoDTable.Rows[j]["1st IN"] = DtdIFReport.Rows[0]["TimeIn"].ToString();
                    AutoDTable.Rows[j]["1st OUT"] = DtdIFReport.Rows[0]["TimeOut"].ToString();
                    AutoDTable.Rows[j]["1st Total Hours"] = DtdIFReport.Rows[0]["TotalMIN"].ToString();
                    //AutoDTable.Rows[j]["Final Total Hours"] = DtdIFReport.Rows[0]["TotalMIN"].ToString();

                }
                else
                {
                    string Emp_Total_Work_Time_1 = "00:00";
                    string Final_OT_Work_Time_1 = "00:00";

                    //Find Week OFF
                    string Employee_Week_Name = "";
                    string Assign_Week_Name = "";

                    DataTable NFHDS = new DataTable();
                    string qry_nfh = "";

                    //Time In Query
                    DataTable mLocalDS1 = new DataTable();
                    time_Check_dbl = "0";
                    time_Check_dbl_1 = 0;


                    SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                    SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    SSQL = SSQL + " And TimeIN >='" + Date_Value_Str + " " + "02:00' And TimeIN <='" + Date_Value_Str1 + " " + "02:00' Order by TimeIN ASC";
                    mLocalDS = objdata.RptEmployeeMultipleDetails(SSQL);


                    if (mLocalDS.Rows.Count == 0)
                    {
                        Time_IN_Str = "";
                    }
                    else
                    {

                    }



                    //Find Shift Name Start                
                    string From_Time_Str = "";
                    string To_Time_Str = "";
                    string Final_InTime = "";
                    string Final_OutTime = "";
                    string Final_Shift = "";
                    DataTable Shift_DS = new DataTable();
                    string Time_Out_Update = "";
                    string Time_IN_Get = "";

                    Boolean Shift_Check_blb = false;
                    string Shift_Start_Time = "";
                    string Shift_End_Time;
                    string Employee_Time = "";
                    DateTime ShiftdateStartIN;
                    DateTime ShiftdateEndIN;


                    //Check With IN Time Shift
                    if (mLocalDS.Rows.Count != 0)
                    {
                        DataTable DS_WH = new DataTable();
                        string Emp_WH_Day = "";
                        string DOJ_Date_Str = "";


                        SSQL = "Select * from Employee_MST where MachineID='" + OT_Week_OFF_Machine_No + "' And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                        DS_WH = objdata.RptEmployeeMultipleDetails(SSQL);


                        FirstName = DS_WH.Rows[0]["FirstName"].ToString();
                        DeptName = DS_WH.Rows[0]["DeptName"].ToString();
                        Shift = DS_WH.Rows[0]["ShiftType"].ToString();
                        Category = DS_WH.Rows[0]["CatName"].ToString();
                        SubCategory = DS_WH.Rows[0]["SubCatName"].ToString();
                        TypeName = DS_WH.Rows[0]["TypeName"].ToString();
                        ExistingCode = DS_WH.Rows[0]["ExistingCode"].ToString();




                        DateTime birthday = new DateTime();
                        DateTime today = DateTime.Now;
                        string DOB = DS_WH.Rows[0]["BirthDate"].ToString();
                        birthday = Convert.ToDateTime(DOB);
                        int Age_Years = (today.Year - birthday.Year) - 1;

                        int Age_Months = 0;
                        if (birthday.Month > today.Month)
                        {
                            Age_Months = (birthday.Month - today.Month) % 12;
                        }
                        else
                        {
                            Age_Months = (today.Month - birthday.Month) % 12;
                        }
                        string Years_Check = Age_Years + "." + Age_Months.ToString();
                        double Years = Convert.ToDouble(Years_Check);


                        if (Years <= 18.0)
                        {
                            Final_Shift = "SHIFT1";

                        }

                        SSQL = "Select * from Shift_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And ShiftDesc = 'SHIFT1'";
                        Shift_DS = objdata.RptEmployeeMultipleDetails(SSQL);
                        Shift_Check_blb = false;

                        for (int k = 0; k < Shift_DS.Rows.Count; k++)
                        {

                            string a = Shift_DS.Rows[k]["StartIN_Days"].ToString();
                            int b = Convert.ToInt16(a.ToString());
                            Shift_Start_Time = date1.AddDays(b).ToString("yyyy/MM/dd") + " " + Shift_DS.Rows[k]["StartIN"].ToString();
                            string a1 = Shift_DS.Rows[k]["EndIN_Days"].ToString();
                            int b1 = Convert.ToInt16(a1.ToString());
                            Shift_End_Time = date1.AddDays(b1).ToString("yyyy/MM/dd") + " " + Shift_DS.Rows[k]["EndIN"].ToString();

                            ShiftdateStartIN = System.Convert.ToDateTime(Shift_Start_Time);
                            ShiftdateEndIN = System.Convert.ToDateTime(Shift_End_Time);
                            if (EmpdateIN >= ShiftdateStartIN && EmpdateIN <= ShiftdateEndIN)
                            {
                                if (Years <= 18)
                                {
                                    Final_Shift = "SHIFT1";
                                }
                                else
                                {
                                    Final_Shift = Shift_DS.Rows[k]["ShiftDesc"].ToString();
                                }

                                Shift_Check_blb = true;

                            }

                        }

                        if (Shift_Check_blb == false)
                        //'TimeOUT Get
                        {
                            SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                            SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                            SSQL = SSQL + " And TimeOUT >='" + Date_Value_Str + " " + "10:00' And TimeOUT <='" + Date_Value_Str1 + " " + "10:00' Order by TimeOUT Asc";

                        }
                        else
                        {

                            DateTime InTime_Check;
                            DateTime InToTime_Check;
                            TimeSpan InTime_TimeSpan;
                            DataTable DS_Time = new DataTable();
                            DataTable DS_InTime = new DataTable();
                            string Final_Shift_Two = "";
                            if (Final_Shift == "SHIFT1" || Final_Shift == "SHIFT2")
                            {
                                InTime_Check = Convert.ToDateTime(Final_InTime);
                                InToTime_Check = InTime_Check.AddHours(2);
                                string InTime_Check_str = string.Format("{0:hh\\:mm\\:ss}", InTime_Check);
                                string InToTime_Check_str = string.Format("{0:hh\\:mm\\:ss}", InToTime_Check);
                                InTime_TimeSpan = TimeSpan.Parse(InTime_Check_str);
                                From_Time_Str = InTime_TimeSpan.Hours + ":" + InTime_TimeSpan.Minutes;
                                InTime_TimeSpan = TimeSpan.Parse(InToTime_Check_str);
                                To_Time_Str = InTime_TimeSpan.Hours + ":" + InTime_TimeSpan.Minutes;

                                //'Two Hours OutTime Check
                                SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                                SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                SSQL = SSQL + " And TimeOUT >='" + Date_Value_Str + " " + From_Time_Str + "' And TimeOUT <='" + Date_Value_Str + " " + To_Time_Str + "' Order by TimeOUT Asc";
                                DS_Time = objdata.RptEmployeeMultipleDetails(SSQL);


                                if (DS_Time.Rows.Count != 0)
                                {
                                    SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                                    SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                    SSQL = SSQL + " And TimeIN >='" + Date_Value_Str + " " + To_Time_Str + "' And TimeIN <='" + Date_Value_Str1 + " " + "02:00' Order by TimeIN ASC";
                                    DS_InTime = objdata.RptEmployeeMultipleDetails(SSQL);
                                    if (DS_InTime.Rows.Count != 0)
                                    {
                                        Final_InTime = DS_InTime.Rows[0][0].ToString();

                                        //'Check With IN Time Shift
                                        SSQL = "Select * from Shift_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And ShiftDesc like '%SHIFT%'";
                                        Shift_DS = objdata.RptEmployeeMultipleDetails(SSQL);
                                        Shift_Check_blb = false;

                                        for (int k = 0; k < Shift_DS.Rows.Count; k++)
                                        {
                                            string a = Shift_DS.Rows[k]["StartIN_Days"].ToString();
                                            int b = Convert.ToInt16(a.ToString());
                                            Shift_Start_Time = date1.AddDays(b).ToString("yyyy/MM/dd") + " " + Shift_DS.Rows[k]["StartIN"].ToString();
                                            string a1 = Shift_DS.Rows[k]["EndIN_Days"].ToString();
                                            int b1 = Convert.ToInt16(a1.ToString());
                                            Shift_End_Time = date1.AddDays(b1).ToString("yyyy/MM/dd") + " " + Shift_DS.Rows[k]["EndIN"].ToString();

                                            ShiftdateStartIN = Convert.ToDateTime(Shift_Start_Time.ToString());
                                            ShiftdateEndIN = Convert.ToDateTime(Shift_End_Time.ToString());

                                            EmpdateIN = Convert.ToDateTime(Final_InTime.ToString());
                                            if (EmpdateIN >= ShiftdateStartIN && EmpdateIN <= ShiftdateEndIN)
                                            {
                                                Final_Shift = Shift_DS.Rows[k]["ShiftDesc"].ToString();
                                                Shift_Check_blb = true;
                                            }
                                        }


                                        if (Shift_Check_blb == true)
                                        {
                                            //'IN Time And Shift Update


                                            SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                                            SSQL = SSQL + " And Compcode='" + SessionLcode + "' And LocCode='" + SessionLcode + "'";
                                            SSQL = SSQL + " And TimeIN >='" + Date_Value_Str + " " + To_Time_Str + "' And TimeIN <='" + Date_Value_Str + " " + "02:00' Order by TimeIN ASC";
                                            mLocalDS = objdata.RptEmployeeMultipleDetails(SSQL);

                                            // 'Out Time Query Update
                                            if (Final_Shift == "SHIFT2")
                                            {
                                                SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                                                SSQL = SSQL + " And Compcode='" + SessionLcode + "' And LocCode='" + SessionLcode + "'";
                                                SSQL = SSQL + " And TimeOUT >='" + Date_Value_Str + " " + "13:00' And TimeOUT <='" + Date_Value_Str + " " + "03:00' Order by TimeOUT Asc";

                                            }
                                            else
                                            {
                                                SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                                                SSQL = SSQL + " And Compcode='" + SessionLcode + "' And LocCode='" + SessionLcode + "'";
                                                SSQL = SSQL + " And TimeOUT >='" + Date_Value_Str + " " + "17:40' And TimeOUT <='" + Date_Value_Str + " " + "10:00' Order by TimeOUT Asc";

                                            }

                                            DS_Time = objdata.RptEmployeeMultipleDetails(SSQL);
                                        }
                                        else
                                        {
                                            if (Final_Shift == "SHIFT1")
                                            {
                                                SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                                                SSQL = SSQL + " And Compcode='" + SessionLcode + "' And LocCode='" + SessionLcode + "'";
                                                SSQL = SSQL + " And TimeOUT >='" + Date_Value_Str + " " + "06:00' And TimeOUT <='" + Date_Value_Str + " " + "13:00' Order by TimeOUT Asc";

                                            }
                                            else
                                            {
                                                SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                                                SSQL = SSQL + " And Compcode='" + SessionLcode + "' And LocCode='" + SessionLcode + "'";
                                                SSQL = SSQL + " And TimeOUT >='" + Date_Value_Str + " " + "13:00' And TimeOUT <='" + Date_Value_Str + " " + "03:00' Order by TimeOUT Asc";

                                            }
                                            DS_Time = objdata.RptEmployeeMultipleDetails(SSQL);
                                        }
                                    }
                                    else
                                    {

                                        if (Final_Shift == "SHIFT1")
                                        {
                                            SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                                            SSQL = SSQL + " And Compcode='" + SessionLcode + "' And LocCode='" + SessionLcode + "'";
                                            SSQL = SSQL + " And TimeOUT >='" + Date_Value_Str + " " + "06:00' And TimeOUT <='" + Date_Value_Str + " " + "13:00' Order by TimeOUT Asc";

                                        }
                                        else
                                        {
                                            SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                                            SSQL = SSQL + " And Compcode='" + SessionLcode + "' And LocCode='" + SessionLcode + "'";
                                            SSQL = SSQL + " And TimeOUT >='" + Date_Value_Str + " " + "13:00' And TimeOUT <='" + Date_Value_Str + " " + "03:00' Order by TimeOUT Asc";

                                        }
                                        DS_Time = objdata.RptEmployeeMultipleDetails(SSQL);
                                    }

                                }
                                else
                                {
                                    SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                                    SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                    SSQL = SSQL + " And TimeOUT >='" + Date_Value_Str + " " + "10:00' And TimeOUT <='" + Date_Value_Str + " " + "10:00' Order by TimeOUT Asc";

                                }

                            }
                            else
                            {
                                SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                                SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                SSQL = SSQL + " And TimeOUT >='" + Date_Value_Str + " " + "10:00' And TimeOUT <='" + Date_Value_Str + " " + "10:00' Order by TimeOUT Asc";

                            }
                        }

                    }

                    else
                    {
                        SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                        SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                        SSQL = SSQL + " And TimeOUT >='" + Date_Value_Str + " " + "10:00' And TimeOUT <='" + Date_Value_Str + " " + "10:00' Order by TimeOUT Asc";

                    }
                    mLocalDS1 = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (mLocalDS1.Rows.Count <= 0)
                    {
                        Time_Out_Str = "";
                    }
                    else
                    {

                    }

                    //Find Week OFF

                    qry_nfh = "Select NFHDate from NFH_Mst where NFHDate=convert(varchar,'" + Date_Value_Str + "',103)";
                    NFHDS = objdata.RptEmployeeMultipleDetails(qry_nfh);
                    if (NFHDS.Rows.Count > 0)
                    {
                        //check NFH Form25_NFH_Present='Yes'
                        DataTable MLocal_Day = new DataTable();
                        string qry_nfh1 = "Select NFHDate from NFH_Mst where NFHDate=convert(varchar,'" + Date_Value_Str + "',103) AND Form25_NFH_Present='Yes'";
                        MLocal_Day = objdata.RptEmployeeMultipleDetails(qry_nfh1);
                        if (MLocal_Day.Rows.Count > 0)
                        {
                            ColumnName = "NH-";
                        }
                        else
                        {
                            ColumnName = "NH";
                        }

                    }
                    else
                    {
                        Employee_Week_Name = Convert.ToDateTime(Date_Value_Str).DayOfWeek.ToString();
                        //'Get Employee Week OF DAY
                        SSQL = "Select WeekOff from Employee_MST where MachineID='" + OT_Week_OFF_Machine_No + "' And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                        NFHDS = objdata.RptEmployeeMultipleDetails(SSQL);
                        string MonthName = NFHDS.Rows[0]["WeekOff"].ToString();
                        if (Employee_Week_Name.ToString() == MonthName.ToString())
                        {
                            ColumnName = "WH";
                        }

                    }
                    if (mLocalDS.Rows.Count > 5)
                    {
                        Final_OT_Work_Time_1 = "00:00";

                        for (int tin = 0; tin < mLocalDS.Rows.Count; tin++)
                        {
                            Time_IN_Str = mLocalDS.Rows[tin][0].ToString();
                            if (mLocalDS1.Rows.Count > tin)
                            {
                                Time_Out_Str = mLocalDS1.Rows[tin][0].ToString();
                            }
                            else if (mLocalDS1.Rows.Count > mLocalDS.Rows.Count)
                            {
                                Time_Out_Str = mLocalDS1.Rows[mLocalDS1.Rows.Count - 1][0].ToString();
                            }

                            else
                            {
                                Time_Out_Str = "";
                            }

                            if (tin == 0)
                            {
                                if (mLocalDS.Rows.Count <= 0)
                                {
                                    Time_IN_Str = "";

                                    if (ColumnName == "WH")
                                    {
                                        AutoDTable.Rows[j][1] = "WH";
                                    }
                                    else if (ColumnName == "NH")
                                    {
                                        AutoDTable.Rows[j][1] = "NH";
                                    }

                                    else if (ColumnName == "NH-")
                                    {
                                        AutoDTable.Rows[j][1] = "NH-" + string.Format("{0:hh:mm tt}", Time_IN_Str);
                                    }
                                    else
                                    {
                                        AutoDTable.Rows[j][1] = string.Format("{0:hh:mm tt}", Time_IN_Str);

                                    }


                                }
                                else
                                {
                                    if (ColumnName == "WH")
                                    {
                                        AutoDTable.Rows[j][1] = "WH";
                                    }
                                    else if (ColumnName == "NH")
                                    {
                                        AutoDTable.Rows[j][1] = "NH";
                                    }

                                    else if (ColumnName == "NH-")
                                    {
                                        AutoDTable.Rows[j][1] = "NH-" + string.Format("{0:hh:mm tt}", Time_IN_Str);
                                    }
                                    else
                                    {
                                        AutoDTable.Rows[j][1] = string.Format("{0:hh:mm tt}", Time_IN_Str);

                                    }
                                }

                                if (Time_Out_Str == "")
                                {


                                    if (ColumnName == "WH")
                                    {
                                        AutoDTable.Rows[j][2] = "WH";
                                    }
                                    else if (ColumnName == "NH")
                                    {
                                        AutoDTable.Rows[j][2] = "NH";
                                    }

                                    else if (ColumnName == "NH-")
                                    {
                                        AutoDTable.Rows[j][2] = "NH-" + string.Format("{0:hh:mm tt}", Time_Out_Str);
                                    }
                                    else
                                    {
                                        AutoDTable.Rows[j][2] = string.Format("{0:hh:mm tt}", Time_Out_Str);

                                    }


                                }
                                else
                                {
                                    if (ColumnName == "WH")
                                    {
                                        AutoDTable.Rows[j][2] = "WH";
                                    }
                                    else if (ColumnName == "NH")
                                    {
                                        AutoDTable.Rows[j][2] = "NH";
                                    }

                                    else if (ColumnName == "NH-")
                                    {
                                        AutoDTable.Rows[j][2] = "NH-" + string.Format("{0:hh:mm tt}", Time_Out_Str);
                                    }
                                    else
                                    {
                                        AutoDTable.Rows[j][2] = string.Format("{0:hh:mm tt}", Time_Out_Str);

                                    }
                                }



                            }
                            else
                            {

                                //First Time In And Time Out Update
                                if (mLocalDS.Rows.Count <= 0)
                                {
                                    if (ColumnName == "WH")
                                    {
                                        AutoDTable.Rows[j][4] = "WH";
                                    }
                                    else if (ColumnName == "NH")
                                    {
                                        AutoDTable.Rows[j][4] = "NH";
                                    }

                                    else if (ColumnName == "NH-")
                                    {
                                        AutoDTable.Rows[j][4] = "NH-" + string.Format("{0:hh:mm tt}", Time_IN_Str);
                                    }
                                    else
                                    {
                                        AutoDTable.Rows[j][4] = string.Format("{0:hh:mm tt}", Time_IN_Str);

                                    }


                                }
                                else
                                {
                                    if (ColumnName == "WH")
                                    {
                                        AutoDTable.Rows[j][4] = "WH";
                                    }
                                    else if (ColumnName == "NH")
                                    {
                                        AutoDTable.Rows[j][4] = "NH";
                                    }

                                    else if (ColumnName == "NH-")
                                    {
                                        AutoDTable.Rows[j][4] = "NH-" + string.Format("{0:hh:mm tt}", (mLocalDS.Rows[tin][0]));
                                    }
                                    else
                                    {
                                        AutoDTable.Rows[j][4] = string.Format("{0:hh:mm tt}", (mLocalDS.Rows[tin][0]));

                                    }
                                }
                                if (Time_Out_Str == "")
                                {
                                    if (ColumnName == "WH")
                                    {
                                        AutoDTable.Rows[j][5] = "WH";
                                    }
                                    else if (ColumnName == "NH")
                                    {
                                        AutoDTable.Rows[j][5] = "NH";
                                    }

                                    else if (ColumnName == "NH-")
                                    {
                                        AutoDTable.Rows[j][5] = "NH-" + string.Format("{0:hh:mm tt}", Time_Out_Str);
                                    }
                                    else
                                    {
                                        AutoDTable.Rows[j][5] = string.Format("{0:hh:mm tt}", Time_Out_Str);

                                    }
                                }
                                else
                                {
                                    if (ColumnName == "WH")
                                    {
                                        AutoDTable.Rows[j][5] = "WH";
                                    }
                                    else if (ColumnName == "NH")
                                    {
                                        AutoDTable.Rows[j][5] = "NH";
                                    }

                                    else if (ColumnName == "NH-")
                                    {
                                        AutoDTable.Rows[j][5] = "NH-" + string.Format("{0:hh:mm tt}", (mLocalDS1.Rows[tin][0]));
                                    }
                                    else
                                    {
                                        AutoDTable.Rows[j][5] = string.Format("{0:hh:mm tt}", (mLocalDS1.Rows[tin][0]));

                                    }
                                }
                            }
                            if (tin == 0)//'First Total Hours
                            {
                                time_Check_dbl = "";
                                if (Time_IN_Str == "" || Time_Out_Str == "")
                                {
                                    time_Check_dbl = "";
                                }
                                else
                                {
                                    DateTime date11 = System.Convert.ToDateTime(Time_IN_Str);
                                    DateTime date12 = System.Convert.ToDateTime(Time_Out_Str);
                                    TimeSpan ts = new TimeSpan();
                                    ts = date12.Subtract(date11);
                                    ts = date12.Subtract(date11);
                                    Total_Time_get = ts.Hours.ToString();
                                    if (Left_Val(Total_Time_get, 1) == "-")
                                    {
                                        date2 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                                        ts = date2.Subtract(date1);
                                        ts = date2.Subtract(date1);
                                        Total_Time_get = ts.Hours.ToString();
                                        time_Check_dbl = Total_Time_get + ":" + ts.Minutes.ToString();
                                    }
                                    else
                                    {
                                        time_Check_dbl = Total_Time_get + ":" + ts.Minutes.ToString();
                                    }
                                }



                                //'Find Week OFF
                                qry_nfh = "Select NFHDate from NFH_Mst where NFHDate=convert(varchar,'" + Date_Value_Str + "',103)";
                                NFHDS = objdata.RptEmployeeMultipleDetails(qry_nfh);
                                if (NFHDS.Rows.Count > 0)
                                {
                                    //check NFH Form25_NFH_Present='Yes'
                                    DataTable MLocal_Day = new DataTable();
                                    string qry_nfh1 = "Select NFHDate from NFH_Mst where NFHDate=convert(varchar,'" + Date_Value_Str + "',103) AND Form25_NFH_Present='Yes'";
                                    MLocal_Day = objdata.RptEmployeeMultipleDetails(qry_nfh1);
                                    if (MLocal_Day.Rows.Count > 0)
                                    {
                                        ColumnName = "NH-";
                                    }
                                    if (ColumnName == "NH-")
                                    {
                                        AutoDTable.Rows[j][3] = "NH / " + time_Check_dbl;
                                    }
                                    else
                                    {
                                        AutoDTable.Rows[j][3] = "NH";
                                    }

                                }
                                else
                                {
                                    Employee_Week_Name = Convert.ToDateTime(Date_Value_Str).DayOfWeek.ToString();
                                    //'Get Employee Week OF DAY
                                    SSQL = "Select WeekOff from Employee_MST where MachineID='" + OT_Week_OFF_Machine_No + "' And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                    NFHDS = objdata.RptEmployeeMultipleDetails(SSQL);
                                    if (NFHDS.Rows.Count <= 0)
                                    {
                                        Assign_Week_Name = "";
                                    }
                                    else
                                    {
                                        Assign_Week_Name = NFHDS.Rows[0][0].ToString();
                                    }
                                    if (Employee_Week_Name == Assign_Week_Name)
                                    {
                                        AutoDTable.Rows[j][3] = "WH / " + time_Check_dbl;
                                    }
                                    else
                                    {
                                        AutoDTable.Rows[j][3] = time_Check_dbl;
                                    }

                                }
                            }

                            else//' Second Total Hours
                            {

                                //'Time Duration Get
                                time_Check_dbl = "";
                                if (Time_IN_Str == "" || Time_Out_Str == "")
                                {
                                    time_Check_dbl = "";
                                }
                                else
                                {
                                    DateTime date11 = System.Convert.ToDateTime(Time_IN_Str);
                                    DateTime date12 = System.Convert.ToDateTime(Time_Out_Str);
                                    TimeSpan ts = new TimeSpan();
                                    ts = date12.Subtract(date11);
                                    ts = date12.Subtract(date11);
                                    Total_Time_get = ts.Hours.ToString();
                                    if (Left_Val(Total_Time_get, 1) == "-")
                                    {
                                        date2 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                                        ts = date2.Subtract(date1);
                                        ts = date2.Subtract(date1);
                                        Total_Time_get = ts.Hours.ToString();
                                        time_Check_dbl = Total_Time_get + ":" + ts.Minutes.ToString();
                                    }
                                    else
                                    {
                                        time_Check_dbl = Total_Time_get + ":" + ts.Minutes.ToString();
                                    }
                                }

                                // 'Find Week OFF
                                //   NFHDS = Nothing

                                qry_nfh = "Select NFHDate from NFH_Mst where NFHDate=convert(varchar,'" + Date_Value_Str + "',103)";
                                NFHDS = objdata.RptEmployeeMultipleDetails(qry_nfh);
                                if (NFHDS.Rows.Count > 0)
                                {
                                    //  'fg.set_TextMatrix(mGrdRow, 6, "NH / " & time_Check_dbl)

                                    //'check NFH Form25_NFH_Present='Yes'
                                    DataTable MLocal_Day = new DataTable();

                                    string qry_nfh1 = "Select NFHDate from NFH_Mst where NFHDate=convert(varchar,'" + date1.AddDays(0).ToString("yyyy/MM/dd") + "',103) AND Form25_NFH_Present='Yes'";
                                    MLocal_Day = objdata.RptEmployeeMultipleDetails(qry_nfh1);
                                    if (MLocal_Day.Rows.Count > 0)
                                    {
                                        ColumnName = "NH-";
                                    }
                                    if (ColumnName == "NH-")
                                    {
                                        AutoDTable.Rows[j][6] = "NH / " + time_Check_dbl;
                                    }
                                    else
                                    {
                                        AutoDTable.Rows[j][6] = "NH";
                                    }

                                }
                                else
                                {
                                    Employee_Week_Name = Convert.ToDateTime(Date_Value_Str).DayOfWeek.ToString();
                                    SSQL = "Select WeekOff from Shiftrrecording where MachineNo='" + OT_Week_OFF_Machine_No + "'";
                                    NFHDS = objdata.RptEmployeeMultipleDetails(SSQL);
                                    if (NFHDS.Rows.Count <= 0)
                                    {
                                        Assign_Week_Name = "";
                                    }
                                    else
                                    {
                                        Assign_Week_Name = NFHDS.Rows[0][0].ToString();
                                    }
                                    if (Employee_Week_Name == Assign_Week_Name)
                                    {
                                        AutoDTable.Rows[j][6] = "WH / " + time_Check_dbl;
                                    }
                                    else
                                    {
                                        AutoDTable.Rows[j][6] = time_Check_dbl;
                                    }


                                }
                            }

                            //'Final Total Time Get Start
                            Time_IN_Str = mLocalDS.Rows[tin][0].ToString();
                            if (mLocalDS1.Rows.Count > tin)
                            {
                                Time_Out_Str = mLocalDS1.Rows[tin][0].ToString();
                            }
                            else if (mLocalDS1.Rows.Count > mLocalDS.Rows.Count)
                            {
                                Time_Out_Str = mLocalDS1.Rows[mLocalDS1.Rows.Count - 1][0].ToString();
                            }
                            else
                            {
                                Time_Out_Str = "";
                            }
                            TimeSpan ts4Final = new TimeSpan();

                            ts4Final = Convert.ToDateTime(String.Format("{0:hh/:mm}", Final_OT_Work_Time_1)).TimeOfDay;
                            if (mLocalDS.Rows.Count <= 0)
                            {
                                Time_IN_Str = "";
                            }
                            else
                            {
                                Time_IN_Str = mLocalDS.Rows[tin][0].ToString();
                            }

                            //  'Emp_Total_Work_Time
                            if (Time_IN_Str == "" || Time_Out_Str == "")
                            {
                                //time_Check_dbl = time_Check_dbl;
                            }
                            else
                            {
                                DateTime date3Final = System.Convert.ToDateTime(Time_IN_Str);
                                DateTime date4Final = System.Convert.ToDateTime(Time_Out_Str);
                                TimeSpan ts1Final = new TimeSpan();
                                ts1Final = date4Final.Subtract(date3Final);
                                ts1Final = date4Final.Subtract(date3Final);
                                Total_Time_get = ts1Final.Hours.ToString();//+ ":" + .Minutes.ToString() 
                                //'ts4 = ts4.Add(ts1)
                                //'OT Time Get

                                Final_OT_Work_Time_1 = ts4Final.Hours + ":" + ts4Final.Minutes;
                                Time_Minus_Value_Check = Final_OT_Work_Time_1.Split(':');
                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                {

                                    Final_OT_Work_Time_1 = "00:00";
                                }
                                if (Left_Val(Final_OT_Work_Time_1, 1) == "0:" | Final_OT_Work_Time_1 == "0:-1" | Final_OT_Work_Time_1 == "0:-3")
                                {

                                    Final_OT_Work_Time_1 = "00:00";
                                }
                                // 'MessageBox.Show(Emp_Total_Work_Time_1.ToString() + "")
                                if (Left_Val(Total_Time_get, 1) == "-")
                                {
                                    date4Final = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                                    ts1Final = date4Final.Subtract(date3Final);
                                    ts1Final = date4Final.Subtract(date3Final);
                                    ts4Final = ts4Final.Add(ts1Final);
                                    Total_Time_get = ts1Final.Hours.ToString();
                                    Final_OT_Work_Time_1 = ts4Final.Hours + ":" + ts4Final.Minutes;
                                    Time_Minus_Value_Check = Final_OT_Work_Time_1.Split(':');
                                    if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    {

                                        Final_OT_Work_Time_1 = "00:00";
                                    }
                                    if (Left_Val(Final_OT_Work_Time_1, 1) == "0:" | Final_OT_Work_Time_1 == "0:-1" | Final_OT_Work_Time_1 == "0:-3")
                                    {

                                        Final_OT_Work_Time_1 = "00:00";
                                    }
                                }
                                else
                                {
                                    ts4Final = ts4Final.Add(ts1Final);
                                    Final_OT_Work_Time_1 = ts4Final.Hours + ":" + ts4Final.Minutes;
                                    Time_Minus_Value_Check = Final_OT_Work_Time_1.Split(':');
                                    if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    {
                                        Final_OT_Work_Time_1 = "00:00";
                                    }
                                    if (Left_Val(Final_OT_Work_Time_1, 1) == "0:" | Final_OT_Work_Time_1 == "0:-1" | Final_OT_Work_Time_1 == "0:-3")
                                    {
                                        Final_OT_Work_Time_1 = "00:00";
                                        time_Check_dbl = Convert.ToInt16(Total_Time_get).ToString();
                                    }
                                }


                                //'Final Total Time Get End
                            }
                        }
                        //'Update Final Work Total Time
                        if (Final_OT_Work_Time_1 != "00:00")
                        {
                            AutoDTable.Rows[j][7] = Final_OT_Work_Time_1;
                        }
                        else
                        {
                            AutoDTable.Rows[j][7] = "";
                        }
                    }//////////////////////////////////////////////////////////
                    else
                    {
                        time_Check_dbl = "";
                        //'Single Time In Record
                        //'Time IN Get
                        //'SSQL = "Select TimeIN from LogTime_IN where MachineID='" & Machine_ID_Str & "'"
                        //'SSQL = SSQL & " And Compcode='" & iStr1(0) & "' And LocCode='" & iStr2(0) & "'"
                        //'SSQL = SSQL & " And TimeIN >='" & Date_Value_Str & " " & "02:00' And TimeIN <='" & Date_Value_Str1 & " " & "02:00' Order by TimeIN ASC"
                        //'mLocalDS = ReturnMultipleValue(SSQL)

                        if (mLocalDS.Rows.Count <= 0)
                        {
                            Time_IN_Str = "";
                            if (ColumnName == "WH")
                            {
                                AutoDTable.Rows[j][1] = "WH";
                            }
                            else if (ColumnName == "NH")
                            {
                                AutoDTable.Rows[j][1] = "NH";
                            }
                            else if (ColumnName == "NH-")
                            {
                                AutoDTable.Rows[j][1] = "NH / " + String.Format("{0:hh:mm tt}", Time_IN_Str);

                            }
                            else
                            {
                                AutoDTable.Rows[j][1] = String.Format("{0:hh:mm tt}", Time_IN_Str);
                            }

                        }
                        else
                        {
                            Time_IN_Str = mLocalDS.Rows[0][0].ToString();
                            if (ColumnName == "WH")
                            {
                                AutoDTable.Rows[j][1] = "WH";
                            }
                            else if (ColumnName == "NH")
                            {
                                AutoDTable.Rows[j][1] = "NH";
                            }
                            else if (ColumnName == "NH-")
                            {
                                AutoDTable.Rows[j][1] = "NH / " + String.Format("{0:hh:mm tt}", mLocalDS.Rows[0][0]);

                            }

                            else
                            {
                                AutoDTable.Rows[j][1] = String.Format("{0:hh:mm tt}", mLocalDS.Rows[0][0]);
                            }

                        }
                        //'Time Out Get
                        if (mLocalDS1.Rows.Count <= 0)
                        {
                            Time_Out_Str = "";
                            if (ColumnName == "WH")
                            {
                                AutoDTable.Rows[j][2] = "WH";
                            }
                            else if (ColumnName == "NH")
                            {
                                AutoDTable.Rows[j][2] = "NH";
                            }
                            else if (ColumnName == "NH-")
                            {
                                AutoDTable.Rows[j][2] = "NH / " + String.Format("{0:hh:mm tt}", Time_Out_Str);

                            }
                            else
                            {
                                AutoDTable.Rows[j][2] = String.Format("{0:hh:mm tt}", Time_Out_Str);
                            }

                        }
                        else
                        {
                            Time_Out_Str = mLocalDS1.Rows[0][0].ToString();
                            if (ColumnName == "WH")
                            {
                                AutoDTable.Rows[j][2] = "WH";
                            }
                            else if (ColumnName == "NH")
                            {
                                AutoDTable.Rows[j][2] = "NH";
                            }
                            else if (ColumnName == "NH-")
                            {
                                AutoDTable.Rows[j][2] = "NH / " + String.Format("{0:hh:mm tt}", mLocalDS1.Rows[0][0]);

                            }

                            else
                            {
                                AutoDTable.Rows[j][2] = String.Format("{0:hh:mm tt}", mLocalDS1.Rows[0][0]);
                            }
                        }
                        for (int Z = 0; Z < mLocalDS1.Rows.Count; Z++)
                        {
                            if (Z == 0)
                            {
                                if (ColumnName == "WH")
                                {
                                    AutoDTable.Rows[j][2] = "WH";
                                }
                                else if (ColumnName == "NH")
                                {
                                    AutoDTable.Rows[j][2] = "NH";
                                }
                                else if (ColumnName == "NH-")
                                {
                                    AutoDTable.Rows[j][2] = "NH / " + String.Format("{0:hh:mm tt}", mLocalDS1.Rows[Z][0]);

                                }
                                else
                                {
                                    AutoDTable.Rows[j][2] = String.Format("{0:hh:mm tt}", mLocalDS1.Rows[Z][0]);
                                }
                            }
                            else
                            {

                            }
                        }

                        // 'Time Duration Get
                        if (Time_IN_Str == "" || Time_Out_Str == "")
                        {
                            time_Check_dbl = "";
                        }
                        else
                        {
                            DateTime date11 = System.Convert.ToDateTime(Time_IN_Str);
                            DateTime date12 = System.Convert.ToDateTime(Time_Out_Str);
                            TimeSpan ts = new TimeSpan();


                            ts = date12.Subtract(date11);
                            ts = date12.Subtract(date11);
                            Total_Time_get = ts.Hours.ToString(); //'& ":" & Trim(ts.Minutes)
                            if (Left_Val(Total_Time_get, 1) == "-")
                            {
                                date2 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                                ts = date2.Subtract(date1);
                                ts = date2.Subtract(date1);
                                Total_Time_get = ts.Hours.ToString();

                                time_Check_dbl = ts.Hours + ":" + ts.Minutes;

                            }
                            else
                            {
                                time_Check_dbl = Total_Time_get + ":" + ts.Minutes;
                            }
                            Totol_Hours_Check = Convert.ToDouble(Total_Time_get + "." + ts.Minutes);
                            Time_Out_Update = "";
                            Time_IN_Get = "";
                            if (Totol_Hours_Check_1 < Totol_Hours_Check)
                            {
                                // 'Update Time Out And Total
                                string GetRndpm = GetRandom(Convert.ToInt32(Random_No_Fixed), 15);
                                Random_No_Get = Convert.ToInt64(GetRndpm);
                                if (ColumnName == "NH-")
                                {
                                    string[] NHandTime = AutoDTable.Rows[j][1].ToString().Split('-');
                                    Time_IN_Get = NHandTime[1];
                                }
                                else
                                {
                                    Time_IN_Get = AutoDTable.Rows[j][1].ToString();
                                }
                                if (Time_IN_Get != "WH")
                                {
                                    if (Time_IN_Get != "NH")
                                    {
                                        if (Time_IN_Get != "NH-")
                                        {
                                            DateTime NewDT = Convert.ToDateTime(Time_IN_Get);

                                            NewDT = NewDT.AddHours(8); //'It adds 8 hours from the Time IN Datetime.
                                            NewDT = NewDT.AddMinutes(Random_No_Get);//'It adds Random Minutes from the Time IN datetime.
                                            //  'Time_Out_Update = Format((Convert.ToDateTime(NewDT.ToString())), "dd/MM/yyyy HH:mm:ss")
                                            Time_Out_Update = String.Format("{0:hh:mm tt}", NewDT);

                                        }
                                    }
                                }

                                if (ColumnName == "WH")
                                {
                                    AutoDTable.Rows[j][2] = "WH";
                                }
                                else if (ColumnName == "NH")
                                {
                                    AutoDTable.Rows[j][2] = "NH";
                                }
                                else if (ColumnName == "NH-")
                                {
                                    AutoDTable.Rows[j][2] = "NH / " + String.Format("{0:hh:mm tt}", Time_Out_Update);

                                }
                                else
                                {
                                    AutoDTable.Rows[j][2] = String.Format("{0:hh:mm tt}", Time_Out_Update);
                                }


                                time_Check_dbl = "08" + ":" + Random_No_Get;



                            }


                        }
                        if (ColumnName == "WH")
                        {
                            AutoDTable.Rows[j][3] = "WH";
                        }
                        else if (ColumnName == "NH")
                        {
                            AutoDTable.Rows[j][3] = "NH";
                        }
                        else if (ColumnName == "NH-")
                        {
                            AutoDTable.Rows[j][3] = "NH / " + time_Check_dbl;

                        }
                        else
                        {
                            AutoDTable.Rows[j][3] = time_Check_dbl;
                        }

                    }


                    //  ' CODE FOR ADOLESCENT SHIFT1
                    DataTable DS_WH1 = new DataTable();

                    string[] Adole_Time;
                    int Adole_NoTime;
                    string Time_IN_Get1 = "";
                    string Time_Out_Update1 = "";


                    SSQL = "Select * from Employee_MST where MachineID='" + OT_Week_OFF_Machine_No + "' And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    DS_WH1 = objdata.RptEmployeeMultipleDetails(SSQL);
                    DateTime birthday1 = new DateTime();
                    DateTime today1 = DateTime.Now;
                    string DOB1 = DS_WH1.Rows[0]["BirthDate"].ToString();
                    birthday1 = Convert.ToDateTime(DOB1);

                    DataTable da_DOB = new DataTable();
                    SSQL = "SELECT FLOOR((CAST (GetDate() AS INTEGER) - CAST(CONVERT(datetime,'" + birthday1 + "',103)  AS INTEGER)) / 365.25) AS Age";
                    da_DOB = objdata.RptEmployeeMultipleDetails(SSQL);
                    double Years1 = 0;
                    if (da_DOB.Rows.Count != 0) { Years1 = Convert.ToDouble(da_DOB.Rows[0][0].ToString()); }
                    //// 'Dim Years1 = Today.Year - birthday1.Year
                    //int Age_Years1 = (today1.Year - birthday1.Year) - 1;
                    //int Age_Months1 = (birthday1.Month - today1.Month) % 12;
                    //string Years_Check1 = Age_Years1 + "." + Age_Months1.ToString();
                    //double Years1 = Convert.ToDouble(Years_Check1);

                    if (Years1 <= 18.0)
                    {
                        Adolescent_Shift = "SHIFT1";

                    }
                    if (ColumnName != "WH")
                    {
                        if (ColumnName != "NH")
                        {
                            if (Adolescent_Shift == "SHIFT1")
                            {
                                if (AutoDTable.Rows[j][1] == "")
                                {
                                    if (AutoDTable.Rows[j][2] == "")
                                    {
                                        SSQL = "Select * from Shift_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And ShiftDesc = 'SHIFT1'";
                                        Shift_DS = objdata.RptEmployeeMultipleDetails(SSQL);
                                        Shift_Check_blb = false;
                                        for (int k = 0; k < Shift_DS.Rows.Count; k++)
                                        {
                                            string a = Shift_DS.Rows[k]["StartIN_Days"].ToString();
                                            int b = Convert.ToInt16(a.ToString());
                                            Shift_Start_Time = date1.AddDays(b).ToString("yyyy/MM/dd") + " " + Shift_DS.Rows[k]["StartIN"].ToString();
                                            string a1 = Shift_DS.Rows[k]["EndIN_Days"].ToString();
                                            int b1 = Convert.ToInt16(a1.ToString());
                                            Shift_End_Time = date1.AddDays(b1).ToString("yyyy/MM/dd") + " " + Shift_DS.Rows[k]["EndIN"].ToString();

                                            ShiftdateStartIN = System.Convert.ToDateTime(Shift_Start_Time);
                                            ShiftdateEndIN = System.Convert.ToDateTime(Shift_End_Time);

                                            if (ColumnName == "NH-")
                                            {
                                                string[] NHandTime = AutoDTable.Rows[j][7].ToString().Split('-');
                                                Time_IN_Get = NHandTime[1];
                                            }
                                            else
                                            {
                                                Time_IN_Get = AutoDTable.Rows[j][7].ToString();
                                            }
                                            EmpdateIN = System.Convert.ToDateTime(Time_IN_Get);
                                            if (EmpdateIN >= ShiftdateStartIN && EmpdateIN <= ShiftdateEndIN)
                                            {
                                                if (ColumnName == "NH-")
                                                {
                                                    AutoDTable.Rows[j][1] = "NH-" + string.Format("{0:hh:mm tt}", Time_IN_Get);
                                                }
                                                else
                                                {
                                                    AutoDTable.Rows[j][1] = string.Format("{0:hh:mm tt}", Time_IN_Get);
                                                }

                                            }
                                            else
                                            {
                                                string GetRndpm = GetRandom(Convert.ToInt32(Random_No_Fixed), 15);
                                                Random_No_Get = Convert.ToInt64(GetRndpm);
                                                //Random_No_Get = GetRandom(Random_No_Fixed, 15);
                                                Time_IN_Get1 = "08" + ":" + Random_No_Get;

                                                if (ColumnName == "NH-")
                                                {
                                                    AutoDTable.Rows[j][1] = "NH-" + string.Format("{0:hh:mm tt}", Time_IN_Get1);
                                                }
                                                else
                                                {
                                                    AutoDTable.Rows[j][1] = string.Format("{0:hh:mm tt}", Time_IN_Get1);
                                                }



                                            }
                                        }//FOR END
                                    }
                                    else
                                    {
                                        if (ColumnName == "NH-")
                                        {

                                            string[] stringSeparators = new string[] { "NH-" };
                                            string[] Totaltime = AutoDTable.Rows[j][3].ToString().Split(stringSeparators, StringSplitOptions.None);


                                            Adole_Time = Totaltime[0].Split(':');
                                        }
                                        else
                                        {
                                            Adole_Time = AutoDTable.Rows[j][3].ToString().Split(':');

                                        }
                                        Adole_NoTime = Convert.ToInt16(Adole_Time[0]);
                                        if (Adole_NoTime >= 8)
                                        {
                                            SSQL = "Select * from Shift_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And ShiftDesc = 'SHIFT1'";
                                            Shift_DS = objdata.RptEmployeeMultipleDetails(SSQL);
                                            for (int k = 0; k < Shift_DS.Rows.Count; k++)
                                            {
                                                string a = Shift_DS.Rows[k]["StartIN_Days"].ToString();
                                                int b = Convert.ToInt16(a.ToString());
                                                Shift_Start_Time = date1.AddDays(b).ToString("yyyy/MM/dd") + " " + Shift_DS.Rows[k]["StartIN"].ToString();
                                                string a1 = Shift_DS.Rows[k]["EndIN_Days"].ToString();
                                                int b1 = Convert.ToInt16(a1.ToString());
                                                Shift_End_Time = date1.AddDays(b1).ToString("yyyy/MM/dd") + " " + Shift_DS.Rows[k]["EndIN"].ToString();

                                                ShiftdateStartIN = System.Convert.ToDateTime(Shift_Start_Time);
                                                ShiftdateEndIN = System.Convert.ToDateTime(Shift_End_Time);
                                                if (ColumnName == "NH-")
                                                {
                                                    string[] NHandTime = AutoDTable.Rows[j][1].ToString().Split('-');
                                                    Time_IN_Get1 = NHandTime[1];
                                                }
                                                else
                                                {
                                                    Time_IN_Get1 = AutoDTable.Rows[j][7].ToString();
                                                }

                                                EmpdateIN = System.Convert.ToDateTime(Time_IN_Get1);


                                                if (EmpdateIN >= ShiftdateStartIN && EmpdateIN <= ShiftdateEndIN)
                                                {
                                                    if (ColumnName == "NH-")
                                                    {
                                                        AutoDTable.Rows[j][1] = "NH-" + string.Format("{0:hh:mm tt}", Time_IN_Get1);
                                                    }
                                                    else
                                                    {
                                                        AutoDTable.Rows[j][1] = string.Format("{0:hh:mm tt}", Time_IN_Get1);
                                                    }

                                                }
                                                else
                                                {
                                                    string GetRndpm = GetRandom(Convert.ToInt32(Random_No_Fixed), 15);
                                                    Random_No_Get = Convert.ToInt64(GetRndpm);
                                                    //Random_No_Get = GetRandom(Random_No_Fixed, 15);
                                                    Time_IN_Get1 = "08" + ":" + Random_No_Get;

                                                    if (ColumnName == "NH-")
                                                    {
                                                        AutoDTable.Rows[j][1] = "NH-" + string.Format("{0:hh:mm tt}", Time_IN_Get1);
                                                    }
                                                    else
                                                    {
                                                        AutoDTable.Rows[j][1] = string.Format("{0:hh:mm tt}", Time_IN_Get1);
                                                    }
                                                }



                                            }//End For
                                            if (ColumnName == "NH-")
                                            {
                                                string[] stringSeparators = new string[] { "NH-" };
                                                string[] NHandTime = AutoDTable.Rows[j][1].ToString().Split(stringSeparators, StringSplitOptions.None);

                                                Time_IN_Get1 = NHandTime[1];
                                            }
                                            else
                                            {
                                                Time_IN_Get1 = AutoDTable.Rows[j][1].ToString();
                                            }
                                            DateTime NewDT = Convert.ToDateTime(Time_IN_Get1);
                                            string Emp_Total_Work_Time_11;


                                            NewDT = NewDT.AddHours(8); //'It adds 8 hours from the Time IN Datetime.
                                            NewDT = NewDT.AddMinutes(Random_No_Get); //'It adds Random Minutes from the Time IN datetime.

                                            Time_Out_Update1 = String.Format("{0:hh:mm tt}", NewDT);
                                            Emp_Total_Work_Time_11 = "08" + ":" + Random_No_Get;

                                            if (ColumnName == "NH-")
                                            {
                                                AutoDTable.Rows[j][2] = "NH-" + Time_Out_Update1;
                                            }
                                            else
                                            {
                                                AutoDTable.Rows[j][2] = Time_Out_Update1;
                                            }
                                            if (ColumnName == "NH-")
                                            {
                                                AutoDTable.Rows[j][3] = "NH-" + Emp_Total_Work_Time_11;
                                            }
                                            else
                                            {
                                                AutoDTable.Rows[j][3] = Emp_Total_Work_Time_11;
                                            }
                                            //'end of 8 hours


                                        }
                                        else
                                        {
                                            SSQL = "Select * from Shift_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And ShiftDesc = 'SHIFT1'";
                                            Shift_DS = objdata.RptEmployeeMultipleDetails(SSQL);

                                            Shift_Check_blb = false;
                                            for (int k = 0; k < Shift_DS.Rows.Count; k++)
                                            {
                                                string a = Shift_DS.Rows[k]["StartIN_Days"].ToString();
                                                int b = Convert.ToInt16(a.ToString());
                                                Shift_Start_Time = date1.AddDays(b).ToString("yyyy/MM/dd") + " " + Shift_DS.Rows[k]["StartIN"].ToString();
                                                string a1 = Shift_DS.Rows[k]["EndIN_Days"].ToString();
                                                int b1 = Convert.ToInt16(a1.ToString());
                                                Shift_End_Time = date1.AddDays(b1).ToString("yyyy/MM/dd") + " " + Shift_DS.Rows[k]["EndIN"].ToString();

                                                ShiftdateStartIN = System.Convert.ToDateTime(Shift_Start_Time);
                                                ShiftdateEndIN = System.Convert.ToDateTime(Shift_End_Time);


                                                if (ColumnName == "NH-")
                                                {
                                                    string[] stringSeparators = new string[] { "NH-" };
                                                    string[] NHandTime = AutoDTable.Rows[j][1].ToString().Split(stringSeparators, StringSplitOptions.None);

                                                    Time_IN_Get1 = NHandTime[0];
                                                }
                                                else
                                                {
                                                    Time_IN_Get1 = AutoDTable.Rows[j][1].ToString();
                                                }
                                                EmpdateIN = System.Convert.ToDateTime(Time_IN_Get1);

                                                if (EmpdateIN >= ShiftdateStartIN && EmpdateIN <= ShiftdateEndIN)
                                                {
                                                    if (ColumnName == "NH-")
                                                    {
                                                        AutoDTable.Rows[j][1] = "NH-" + string.Format("{0:hh:mm tt}", Time_IN_Get1);
                                                    }
                                                    else
                                                    {
                                                        AutoDTable.Rows[j][1] = string.Format("{0:hh:mm tt}", Time_IN_Get1);
                                                    }

                                                }
                                                else
                                                {

                                                    string GetRndpm = GetRandom(Convert.ToInt32(Random_No_Fixed), 15);
                                                    Random_No_Get = Convert.ToInt64(GetRndpm);
                                                    //Random_No_Get = GetRandom(Random_No_Fixed, 15);
                                                    Time_IN_Get1 = "08" + ":" + Random_No_Get;

                                                    if (ColumnName == "NH-")
                                                    {
                                                        AutoDTable.Rows[j][1] = "NH-" + string.Format("{0:hh:mm tt}", Time_IN_Get1);
                                                    }
                                                    else
                                                    {
                                                        AutoDTable.Rows[j][1] = string.Format("{0:hh:mm tt}", Time_IN_Get1);
                                                    }


                                                }
                                            } //end For K
                                            if (ColumnName == "NH-")
                                            {
                                                string[] stringSeparators = new string[] { "NH-" };
                                                string[] NHandTime = AutoDTable.Rows[j][1].ToString().Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);
                                                Time_IN_Get1 = NHandTime[1];
                                            }
                                            else
                                            {
                                                Time_IN_Get1 = AutoDTable.Rows[j][1].ToString();
                                            }

                                            Adole_NoTime = Convert.ToInt16(Adole_Time[0]);
                                            int Adole_NoTime1 = Convert.ToInt16(Adole_Time[1]);
                                            DateTime NewDT = Convert.ToDateTime(Time_IN_Get1);
                                            string Emp_Total_Work_Time_11;
                                            NewDT = NewDT.AddHours(Adole_NoTime); //' 0 It adds 4 hours from the Time IN Datetime.
                                            NewDT = NewDT.AddMinutes(Adole_NoTime1); //56 It adds Random Minutes from the Time IN datetime.
                                            Time_Out_Update1 = String.Format("{0:hh:mm tt}", NewDT);

                                            DateTime MDate1 = Convert.ToDateTime(Time_IN_Get1.ToString());
                                            DateTime MDate2 = Convert.ToDateTime(Time_Out_Update1.ToString());

                                            TimeSpan ts1;

                                            ts1 = MDate2.Subtract(MDate1);

                                            Emp_Total_Work_Time_11 = ts1.Hours.ToString() + ":" + ts1.Minutes.ToString();


                                            if (ColumnName == "NH-")
                                            {
                                                AutoDTable.Rows[j][2] = "NH-" + Time_Out_Update1;
                                            }
                                            else
                                            {
                                                AutoDTable.Rows[j][2] = Time_Out_Update1;
                                            }
                                            if (ColumnName == "NH-")
                                            {
                                                AutoDTable.Rows[j][3] = "NH-" + Emp_Total_Work_Time_11;
                                            }
                                            else
                                            {
                                                AutoDTable.Rows[j][3] = Emp_Total_Work_Time_11;
                                            }


                                        }
                                    }

                                }

                            }

                        }


                    }


                    DataTable DataAlready = new DataTable();




                    SSQL = "select * from IFReport_DayAttendance where CompCode = '" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                    SSQL = SSQL + " and MachineID = '" + OT_Week_OFF_Machine_No + "'";
                    SSQL = SSQL + " and AttendanceDate='" + Date_Value_Str + "'";

                    DataAlready = objdata.RptEmployeeMultipleDetails(SSQL);

                    if (DataAlready.Rows.Count == 0)
                    {
                        SSQL = "insert into IFReport_DayAttendance(CompCode,LocCode,MachineID,ExistingCode,FirstName,DeptName,Shift,TimeIn,";
                        SSQL = SSQL + "TimeOut,Category,SubCategory,TotalMIN,GrandTOT,AttendanceDate,PrepBy,TypeName)values('" + SessionCcode + "',";
                        SSQL = SSQL + "'" + SessionLcode + "','" + OT_Week_OFF_Machine_No + "','" + ExistingCode + "','" + FirstName + "',";
                        SSQL = SSQL + "'" + DeptName + "','" + Final_Shift + "','" + AutoDTable.Rows[j][1].ToString() + "','" + AutoDTable.Rows[j][2].ToString() + "',";
                        SSQL = SSQL + "'" + Category + "','" + SubCategory + "','" + AutoDTable.Rows[j][3].ToString() + "',";
                        SSQL = SSQL + "'" + AutoDTable.Rows[j][3].ToString() + "','" + Date_Value_Str + "','User','" + TypeName + "')";
                        objdata.RptEmployeeMultipleDetails(SSQL);
                    }

                }
            }
            grid.DataSource = AutoDTable;
            grid.DataBind();
            string attachment = "attachment;filename=EMPLOYEE WISE DAY ATTENDANCE BETWEEN DATES.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);
            Response.Write("<table>");

            Response.Write("<tr Font-Bold='true' align='left'>");
            Response.Write("<td colspan='8'>");
            Response.Write("<a style=\"font-weight:bold\">EMPLOYEE WISE DAY ATTENDANCE BETWEEN DATES -" + Token_No_Display + "-" + Name_Display + "-" + Date1 + "TO" + Date2 + "</a>");


            Response.Write("</td>");
            Response.Write("</tr>");


            Response.Write("</table>");


            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();
        }
        catch (Exception ex)
        {

            return;
        }
    }
    //public void Fill_Emp_Day_Attd_Between_Dates()
    //{


    //    DataSet ds = new DataSet();
    //    DataTable dt = new DataTable();

    //    string EmpName;


    //    double Total_Time_get;
    //    // DataTable dt = new DataTable();
    //    DataTable dt1 = new DataTable();



    //    date1 = Convert.ToDateTime(Date1);
    //    //string dat = ;
    //    date2 = Convert.ToDateTime(Date2);

    //    int daycount = (int)((date2 - date1).TotalDays);
    //    int daysAdded = 0;

    //    while (daycount >= 0)
    //    {
    //        //DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded).ToShortDateString());
    //        ////string day1=string.Format("{MM/dd/yyyy}",date1.AddDays(daysAdded));
    //        //AutoDTable.Row.Add(Convert.ToString(dayy.ToShortDateString()));

    //        daycount -= 1;
    //        daysAdded += 1;
    //    }

    //    //AutoDTable.Columns.Add("Date");
    //    //AutoDTable.Columns.Add("1st IN");
    //    //AutoDTable.Columns.Add("1st OUT");
    //    ////AutoDTable.Columns.Add("1st Total Hours");
    //    ////AutoDTable.Columns.Add("2nd IN");
    //    ////AutoDTable.Columns.Add("2nd OUT");
    //    ////AutoDTable.Columns.Add("2nd Total Hours");
    //    //AutoDTable.Columns.Add("Final Total Hours");
    //    //AutoDTable.Columns.Add("Hours");
    //    //AutoDTable.Columns.Add("OT_Hours");

    //    EmpName = "";

    //    SSQL = "select MachineID,isnull(FirstName,'') + '.'+ isnull(MiddleInitial,'') as FirstName,ExistingCode,DeptName from ";
    //    if (status == "Approval")
    //    {
    //        SSQL = SSQL + "Employee_Mst";
    //    }
    //    else
    //    {
    //        SSQL = SSQL + "Employee_Mst_New_Emp";
    //    }

    //    SSQL = SSQL + " Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And IsActive='Yes'";

    //    if (EmpCode1 != "")
    //    {
    //        SSQL = SSQL + " And MachineID='" + EmpCode1 + "'";
    //    }
    //    if (Division != "-Select-")
    //    {
    //        SSQL = SSQL + " And Division='" + Division + "'";
    //    }

    //    // SSQL = SSQL + " Group by MachineID,FirstName,ExistingCode,DeptName order by MachineID";
    //    dt = objdata.RptEmployeeMultipleDetails(SSQL);

    //    if (dt.Rows.Count > 0)
    //    {

    //        EmpName = dt.Rows[0]["FirstName"].ToString();

    //    }

    //    intK = 0;

    //    int SNo = 1;

    //    for (int i = 0; i < dt.Rows.Count; i++)
    //    {



    //        string MachineID = dt.Rows[i]["MachineID"].ToString();

    //        //AutoDTable.Rows[intK]["SNo"] = SNo;
    //        //AutoDTable.Rows[intK]["EmployeeNo"] = dt.Rows[i]["MachineID"].ToString();
    //        //AutoDTable.Rows[intK]["ExistingCode"] = dt.Rows[i]["ExistingCode"].ToString();
    //        //AutoDTable.Rows[intK]["Name"] = dt.Rows[i]["FirstName"].ToString();
    //        //AutoDTable.Rows[intK]["DeptName"] = dt.Rows[i]["DeptName"].ToString();


    //        int count = 5;

    //        for (int j = 0; j < daysAdded; j++)
    //        {
    //            AutoDTable.NewRow();
    //            AutoDTable.Rows.Add();

    //            DateTime dayy = Convert.ToDateTime(date1.AddDays(j).ToShortDateString());

    //            Date1 = Convert.ToString(dayy.ToShortDateString());

    //            SSQL = "select TimeIN,TimeOUT,Total_Hrs1 from LogTime_Days where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
    //            SSQL = SSQL + " And MachineID='" + MachineID + "' And CONVERT(datetime,Attn_Date_Str,103) = CONVERT(datetime,'" + Convert.ToDateTime(Date1).ToString("dd/MM/yyyy") + "',103)";
    //            //CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(Date).ToString("dd/MM/yyyy")
    //            dt1 = objdata.RptEmployeeMultipleDetails(SSQL);

    //            if (dt1.Rows.Count > 0)
    //            {


    //                AutoDTable.Rows[intK]["Date"] = Date1;
    //                if (dt1.Rows[0]["TimeIN"].ToString() == "")
    //                {
    //                    AutoDTable.Rows[intK]["1st IN"] = 0;
    //                }
    //                else
    //                {
    //                    AutoDTable.Rows[intK]["1st IN"] = dt1.Rows[0]["TimeIN"].ToString();
    //                }
    //                if (dt1.Rows[0]["TimeOUT"].ToString() == "")
    //                {
    //                    AutoDTable.Rows[intK]["1st OUT"] = 0;
    //                }
    //                else
    //                {
    //                    AutoDTable.Rows[intK]["1st OUT"] = dt1.Rows[0]["TimeOUT"].ToString();
    //                }
    //                if (dt1.Rows[0]["Total_Hrs1"].ToString() == "")
    //                {
    //                    AutoDTable.Rows[intK]["1st Total Hours"] = "00:00";
    //                }
    //                else
    //                {
    //                    AutoDTable.Rows[intK]["1st Total Hours"] = dt1.Rows[0]["Total_Hrs1"].ToString();
    //                }
    //                AutoDTable.Rows[intK]["2nd IN"] = 0;
    //                AutoDTable.Rows[intK]["2nd OUT"] = 0;
    //                AutoDTable.Rows[intK]["2nd Total Hours"] = "00:00";

    //                AutoDTable.Rows[intK]["Final Total Hours"] = dt1.Rows[0]["Total_Hrs1"].ToString();

    //                //AutoDTable.Rows[intK]["OT_Hours"] = dt1.Rows[0]["OTHours"].ToString();
    //                intK = intK + 1;
    //            }
    //            else
    //            {


    //                AutoDTable.Rows[intK]["Date"] = Date1;
    //                AutoDTable.Rows[intK]["1st IN"] = 0;
    //                AutoDTable.Rows[intK]["1st OUT"] = 0;
    //                AutoDTable.Rows[intK]["1st Total Hours"] = "00:00"; ;
    //                AutoDTable.Rows[intK]["2nd IN"] = 0;
    //                AutoDTable.Rows[intK]["2nd OUT"] = 0;
    //                AutoDTable.Rows[intK]["2nd Total Hours"] = "00:00"; ;
    //                //AutoDTable.Rows[intK]["1st Total Hours"] = 0;
    //                AutoDTable.Rows[intK]["Final Total Hours"] = "00:00";
    //                //AutoDTable.Rows[intK]["OT_Hours"] = 0;
    //                intK = intK + 1;
    //            }

    //        }

    //        //AutoDTable.Rows[intK]["Total Days"] = daysAdded.ToString();



    //        SNo = SNo + 1;

    //    }

    //    grid.DataSource = AutoDTable;
    //    grid.DataBind();
    //    string attachment = "attachment;filename=EMPLOYEE WISE DAY ATTENDANCE BETWEEN DATES.xls";
    //    Response.ClearContent();
    //    Response.AddHeader("content-disposition", attachment);
    //    Response.ContentType = "application/ms-excel";
    //    grid.HeaderStyle.Font.Bold = true;
    //    System.IO.StringWriter stw = new System.IO.StringWriter();
    //    HtmlTextWriter htextw = new HtmlTextWriter(stw);
    //    grid.RenderControl(htextw);
    //    Response.Write("<table>");

    //    Response.Write("<tr Font-Bold='true' align='left'>");
    //    Response.Write("<td colspan='8'>");
    //    Response.Write("<a style=\"font-weight:bold\">EMPLOYEE WISE DAY ATTENDANCE BETWEEN DATES -" + EmpCode1 + "-" + EmpName + "-" + Date1 + "TO" + Date2 + "</a>");


    //    Response.Write("</td>");
    //    Response.Write("</tr>");


    //    Response.Write("</table>");


    //    Response.Write(stw.ToString());
    //    Response.End();
    //    Response.Clear();

    //}


    public void Fill_Emp_Day_Attd_Between_Dates()
    {

        try
        {
            date1 = Convert.ToDateTime(Date1);
            date2 = Convert.ToDateTime(Date2);
            if (date1 > date2)
                return;

            string iEmpDet = null;

            iEmpDet = EmpCode1;
            string Token_No_Display = "";
            string Name_Display = "";
            string T_Query = "";
            T_Query = "Select ExistingCode,FirstName from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And MachineID='" + EmpCode1 + "'";
            DataTable DT_To = new DataTable();
            DT_To = objdata.RptEmployeeMultipleDetails(T_Query);
            if (DT_To.Rows.Count != 0)
            {
                Token_No_Display = DT_To.Rows[0]["ExistingCode"].ToString();
                Name_Display = DT_To.Rows[0]["FirstName"].ToString();
            }

            long iRowVal = 0;

            int daysAdded = 0;
            int daycount = (int)((date2 - date1).TotalDays);

            iRowVal = daycount;

            int i = 0;
            while (daycount >= 0)
            {
                DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded));
                AutoDTable.NewRow();
                AutoDTable.Rows.Add();
                AutoDTable.Rows[i]["Date"] = Convert.ToString(dayy.ToShortDateString());
                daycount -= 1;
                daysAdded += 1;
                i++;
            }

            DataTable mLocalShift = new DataTable();
            DataTable mEmployeeDT = new DataTable();
            string Machine_ID_Str = null;

            SSQL = "";
            SSQL = "select ShiftDesc,StartIN,StartIN_Days,EndIN,EndIn_Days,StartOut,StartOut_Days,EndOut,EndOut_Days";
            SSQL = SSQL + " from Shift_Mst Where ShiftDesc Like '%Shift%'";
            SSQL = SSQL + " And CompCode='" + SessionCcode.ToString() + "'";
            SSQL = SSQL + " And LocCode='" + SessionLcode.ToString() + "'";

            mLocalShift = objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "Select MachineID_Encrypt,(FirstName + '.' + MiddleInitial) as FirstName from Employee_Mst where IsActive='Yes' And EmpNo='" + iEmpDet + "'";
            SSQL = SSQL + " And CompCode='" + SessionCcode.ToString() + "'";
            SSQL = SSQL + " And LocCode='" + SessionLcode.ToString() + "'";
            //if (SessionUserType == "2")
            //{
            //    SSQL = SSQL + " And IsNonAdmin='1'";
            //}
            //else if (SessionUserType != "2")
            //{
            //    SSQL = SSQL + " And IsNonAdmin='0'";
            //}



            mEmployeeDT = objdata.RptEmployeeMultipleDetails(SSQL);

            Machine_ID_Str = mEmployeeDT.Rows[0][0].ToString();
            EmpName = mEmployeeDT.Rows[0][1].ToString();


            string Date_Value_Str = null;
            string Date_Value_Str1 = null;

            string OT_Week_OFF_Machine_No = null;
            string Time_IN_Str = null;
            string Time_Out_Str = null;

            string Total_Time_get = "";
            string Total_Time_get_1 = "";
            Int32 j = 0;
            string time_Check_dbl = "";
            Int32 time_Check_dbl_1 = 0;
            for (int mGrdRow = 0; mGrdRow < AutoDTable.Rows.Count; mGrdRow++)
            {

                OT_Week_OFF_Machine_No = Machine_ID_Str;
                string aa = AutoDTable.Rows[mGrdRow]["Date"].ToString();
                DateTime Date_Value = Convert.ToDateTime(aa);
                DateTime Date_Value1 = Date_Value.AddDays(1);
                Date_Value_Str = string.Format(Date_Value.ToString("yyyy/MM/dd"));
                Date_Value_Str1 = string.Format(Date_Value1.ToString("yyyy/MM/dd"));

                string Emp_Total_Work_Time_1 = "00:00";
                string Final_OT_Work_Time_1 = "00:00";

                //Find Week OFF
                string Employee_Week_Name = "";
                string Assign_Week_Name = "";
                DataTable NFHDS = new DataTable();
                string qry_nfh = "";

                //Time In Query
                DataTable mLocalDS1 = new DataTable();
                time_Check_dbl = "";
                time_Check_dbl_1 = 0;
                SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                SSQL = SSQL + " And TimeIN >='" + Date_Value_Str + " " + "02:00' And TimeIN <='" + Date_Value_Str1 + " " + "02:00' Order by TimeIN ASC";
                mLocalDS = objdata.RptEmployeeMultipleDetails(SSQL);
                if (mLocalDS.Rows.Count <= 0)
                {
                    Time_IN_Str = "";
                }
                else
                {
                    //Time_IN_Str = mLocalDS.Tables(0).Rows(0)(0)
                }

                //Find Shift Name Start                
                string From_Time_Str = "";
                string To_Time_Str = "";
                string Final_InTime = "";
                string Final_OutTime = "";
                string Final_Shift = "";
                DataTable Shift_DS = new DataTable();
                Int32 K = 0;
                bool Shift_Check_blb = false;
                string Shift_Start_Time = null;
                string Shift_End_Time = null;
                string Employee_Time = "";
                DateTime ShiftdateStartIN = default(DateTime);
                DateTime ShiftdateEndIN = default(DateTime);
                DateTime EmpdateIN = default(DateTime);

                //Check With IN Time Shift
                if (mLocalDS.Rows.Count != 0)
                {
                    SSQL = "Select * from Shift_Mst Where CompCode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "' And ShiftDesc like '%SHIFT%'";
                    Shift_DS = objdata.RptEmployeeMultipleDetails(SSQL);
                    Shift_Check_blb = false;
                    Final_InTime = mLocalDS.Rows[0]["TimeIN"].ToString();
                    for (K = 0; K < Shift_DS.Rows.Count; K++)
                    {
                        string a = Shift_DS.Rows[K]["StartIN_Days"].ToString();
                        int b = Convert.ToInt16(a.ToString());
                        Shift_Start_Time = Convert.ToDateTime(Date_Value_Str).AddDays(b).ToShortDateString() + " " + Shift_DS.Rows[K]["StartIN"].ToString();
                        string a1 = Shift_DS.Rows[K]["EndIN_Days"].ToString();
                        int b1 = Convert.ToInt16(a1.ToString());
                        Shift_End_Time = Convert.ToDateTime(Date_Value_Str).AddDays(b1).ToShortDateString() + " " + Shift_DS.Rows[K]["EndIN"].ToString();

                        ShiftdateStartIN = Convert.ToDateTime(Shift_Start_Time.ToString());
                        ShiftdateEndIN = Convert.ToDateTime(Shift_End_Time.ToString());

                        EmpdateIN = Convert.ToDateTime(Final_InTime.ToString());




                        if (EmpdateIN >= ShiftdateStartIN & EmpdateIN <= ShiftdateEndIN)
                        {
                            Final_Shift = Shift_DS.Rows[K]["ShiftDesc"].ToString();
                            Shift_Check_blb = true;
                            break; // TODO: might not be correct. Was : Exit For
                        }
                    }
                    if (Shift_Check_blb == false)
                    {
                        //TimeOUT Get
                        SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                        SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                        SSQL = SSQL + " And TimeOUT >='" + Date_Value_Str + " " + "10:00' And TimeOUT <='" + Date_Value_Str1 + " " + "10:00' Order by TimeOUT Asc";
                    }
                    else
                    {
                        if (Final_Shift == "SHIFT1")
                        {
                            //TimeOUT Get
                            SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                            SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                            SSQL = SSQL + " And TimeOUT >='" + Date_Value_Str + " " + "06:30' And TimeOUT <='" + Date_Value_Str1 + " " + "03:00' Order by TimeOUT Asc";
                        }
                        else
                        {
                            //TimeOUT Get
                            SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                            SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                            SSQL = SSQL + " And TimeOUT >='" + Date_Value_Str + " " + "10:00' And TimeOUT <='" + Date_Value_Str1 + " " + "10:00' Order by TimeOUT Asc";
                        }
                    }
                }
                else
                {
                    //TimeOUT Get
                    SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                    SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                    SSQL = SSQL + " And TimeOUT >='" + Date_Value_Str + " " + "10:00' And TimeOUT <='" + Date_Value_Str1 + " " + "10:00' Order by TimeOUT Asc";
                }
                //Find Shift Name End
                mLocalDS1 = objdata.RptEmployeeMultipleDetails(SSQL);
                if (mLocalDS1.Rows.Count <= 0)
                {
                    Time_Out_Str = "";
                }
                else
                {
                    //Time_Out_Str = mLocalDS.Tables(0).Rows(0)(0)
                }
                if (mLocalDS.Rows.Count > 1)
                {
                    Final_OT_Work_Time_1 = "00:00";
                    for (int tin = 0; tin < mLocalDS.Rows.Count; tin++)
                    {
                        Time_IN_Str = mLocalDS.Rows[tin][0].ToString();
                        if (tin == 2)
                            break; // TODO: might not be correct. Was : Exit For
                        if (mLocalDS1.Rows.Count > tin)
                        {
                            Time_Out_Str = mLocalDS1.Rows[tin][0].ToString();
                        }
                        else if (mLocalDS1.Rows.Count > mLocalDS.Rows.Count)
                        {
                            Time_Out_Str = mLocalDS1.Rows[mLocalDS1.Rows.Count - 1][0].ToString();
                        }
                        else
                        {
                            Time_Out_Str = "";
                        }
                        if (tin == 0)
                        {
                            //First Time In And Time Out Update
                            if (mLocalDS.Rows.Count <= 0)
                            {
                                Time_IN_Str = "";
                                AutoDTable.Rows[mGrdRow][1] = Time_IN_Str;
                                //fg.set_TextMatrix(mGrdRow, 1, Time_IN_Str);
                            }
                            else
                            {
                                AutoDTable.Rows[mGrdRow][1] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[tin][0]);

                                //  fg.set_TextMatrix(mGrdRow, 1, string.Format("{0:hh:mm tt}", mLocalDS.Tables(0).Rows(tin)(0)));
                            }
                            if (string.IsNullOrEmpty(Time_Out_Str))
                            {
                                AutoDTable.Rows[mGrdRow][2] = Time_Out_Str;
                                //fg.set_TextMatrix(mGrdRow, 2, Time_Out_Str);
                            }
                            else
                            {
                                AutoDTable.Rows[mGrdRow][2] = string.Format("{0:hh:mm tt}", mLocalDS1.Rows[tin][0]);
                                // fg.set_TextMatrix(mGrdRow, 2, string.Format("{0:hh:mm tt}", mLocalDS1.Tables(0).Rows(tin)(0)));
                            }
                        }
                        else
                        {
                            //First Time In And Time Out Update
                            if (mLocalDS.Rows.Count <= 0)
                            {
                                AutoDTable.Rows[mGrdRow][4] = Time_IN_Str;
                                //fg.set_TextMatrix(mGrdRow, 4, Time_IN_Str);
                            }
                            else
                            {
                                AutoDTable.Rows[mGrdRow][4] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[tin][0]);

                                // fg.set_TextMatrix(mGrdRow, 4, string.Format("{0:hh:mm tt}", mLocalDS.Tables(0).Rows(tin)(0)));
                            }
                            if (string.IsNullOrEmpty(Time_Out_Str))
                            {
                                AutoDTable.Rows[mGrdRow][5] = Time_Out_Str;
                                // fg.set_TextMatrix(mGrdRow, 5, Time_Out_Str);
                            }
                            else
                            {
                                AutoDTable.Rows[mGrdRow][5] = string.Format("{0:hh:mm tt}", mLocalDS1.Rows[mLocalDS1.Rows.Count - 1][0]);

                                //fg.set_TextMatrix(mGrdRow, 5, string.Format("{0:hh:mm tt}", mLocalDS1.Tables(0).Rows(mLocalDS1.Tables(0).Rows.Count - 1)(0)));
                            }
                        }
                        //First Total Hours
                        if (tin == 0)
                        {
                            //Time Duration Get
                            time_Check_dbl = "";
                            if (string.IsNullOrEmpty(Time_IN_Str) | string.IsNullOrEmpty(Time_Out_Str))
                            {
                                time_Check_dbl = "";
                            }
                            else
                            {
                                DateTime date5 = System.Convert.ToDateTime(Time_IN_Str);
                                DateTime date6 = System.Convert.ToDateTime(Time_Out_Str);
                                TimeSpan ts = new TimeSpan();

                                ts = date6.Subtract(date5);
                                ts = date6.Subtract(date5);
                                Total_Time_get = ts.Hours.ToString();
                                //& ":" & Trim(ts.Minutes)
                                if (Left_Val(Total_Time_get, 1) == "-")
                                {
                                    date2 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);

                                    ts = date2.Subtract(date1);
                                    ts = date2.Subtract(date1);
                                    Total_Time_get = ts.Hours.ToString();

                                    //Total_Time_get = Right_Val(Total_Time_get, Len(Total_Time_get) - 1)
                                    time_Check_dbl = Total_Time_get + ":" + ts.Minutes;
                                }
                                else
                                {
                                    time_Check_dbl = Total_Time_get + ":" + ts.Minutes;
                                }
                            }

                            //Find Week OFF
                            NFHDS = null;
                            qry_nfh = "Select NFHDate from NFH_Mst where NFHDate=convert(varchar,'" + Date_Value_Str + "',103)";
                            NFHDS = objdata.RptEmployeeMultipleDetails(qry_nfh);
                            if (NFHDS.Rows.Count > 0)
                            {
                                AutoDTable.Rows[mGrdRow][3] = "NH / " + time_Check_dbl;
                                //  fg.set_TextMatrix(mGrdRow, 3, "NH / " + time_Check_dbl);
                            }
                            else
                            {
                                Employee_Week_Name = (Convert.ToDateTime(Date_Value_Str).DayOfWeek).ToString();
                                //Get Employee Week OF DAY
                                SSQL = "Select WeekOff from Employee_MST where MachineID_Encrypt='" + OT_Week_OFF_Machine_No + "' And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                                NFHDS = objdata.RptEmployeeMultipleDetails(SSQL);
                                if (NFHDS.Rows.Count <= 0)
                                {
                                    Assign_Week_Name = "";
                                }
                                else
                                {
                                    Assign_Week_Name = NFHDS.Rows[0][0].ToString();
                                }
                                if (Employee_Week_Name == Assign_Week_Name)
                                {
                                    AutoDTable.Rows[mGrdRow][3] = "WH / " + time_Check_dbl;
                                    //  fg.set_TextMatrix(mGrdRow, 3, "WH / " + time_Check_dbl);
                                }
                                else
                                {
                                    AutoDTable.Rows[mGrdRow][3] = time_Check_dbl;
                                    //fg.set_TextMatrix(mGrdRow, 3, time_Check_dbl);
                                }
                            }
                            // Second Total Hours
                        }
                        else
                        {
                            //Time Duration Get
                            time_Check_dbl = "";
                            if (string.IsNullOrEmpty(Time_IN_Str) | string.IsNullOrEmpty(Time_Out_Str))
                            {
                                time_Check_dbl = "";
                            }
                            else
                            {
                                DateTime date7 = System.Convert.ToDateTime(Time_IN_Str);
                                DateTime date8 = System.Convert.ToDateTime(Time_Out_Str);
                                TimeSpan ts = new TimeSpan();

                                ts = date8.Subtract(date7);
                                ts = date8.Subtract(date7);
                                Total_Time_get = ts.Hours.ToString();
                                //& ":" & Trim(ts.Minutes)
                                if (Left_Val(Total_Time_get, 1) == "-")
                                {
                                    date8 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);

                                    ts = date8.Subtract(date7);
                                    ts = date8.Subtract(date7);
                                    Total_Time_get = ts.Hours.ToString();

                                    //Total_Time_get = Right_Val(Total_Time_get, Len(Total_Time_get) - 1)
                                    time_Check_dbl = Total_Time_get + ":" + ts.Minutes;
                                }
                                else
                                {
                                    time_Check_dbl = Total_Time_get + ":" + ts.Minutes;
                                }
                            }
                            //Find Week OFF
                            NFHDS = null;
                            qry_nfh = "Select NFHDate from NFH_Mst where NFHDate='" + Date_Value_Str + "'";
                            NFHDS = objdata.RptEmployeeMultipleDetails(qry_nfh);
                            if (NFHDS.Rows.Count > 0)
                            {
                                AutoDTable.Rows[mGrdRow][6] = "NH / " + time_Check_dbl;
                                //fg.set_TextMatrix(mGrdRow, 6, "NH / " + time_Check_dbl);
                            }
                            else
                            {
                                Employee_Week_Name = (Convert.ToDateTime(Date_Value_Str).DayOfWeek).ToString();

                                //Get Employee Week OF DAY
                                SSQL = "Select WeekOff from Employee_MST where MachineID_Encrypt='" + OT_Week_OFF_Machine_No + "' And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";

                                // SSQL = "Select WeekOff from Shiftrrecording where MachineNo='" + iEmpDet + "'";     //OT_Week_OFF_Machine_No
                                NFHDS = objdata.RptEmployeeMultipleDetails(SSQL);
                                if (NFHDS.Rows.Count <= 0)
                                {
                                    Assign_Week_Name = "";
                                }
                                else
                                {
                                    Assign_Week_Name = NFHDS.Rows[0][0].ToString();
                                }
                                if (Employee_Week_Name == Assign_Week_Name)
                                {
                                    AutoDTable.Rows[mGrdRow][6] = "WH / " + time_Check_dbl;
                                    // fg.set_TextMatrix(mGrdRow, 6, "WH / " + time_Check_dbl);
                                }
                                else
                                {
                                    AutoDTable.Rows[mGrdRow][6] = time_Check_dbl;
                                    // fg.set_TextMatrix(mGrdRow, 6, time_Check_dbl);
                                }
                            }
                        }
                        //Final Total Time Get Start
                        Time_IN_Str = mLocalDS.Rows[tin][0].ToString();

                        if (mLocalDS1.Rows.Count > tin)
                        {
                            Time_Out_Str = mLocalDS1.Rows[tin][0].ToString();
                        }
                        else if (mLocalDS1.Rows.Count > mLocalDS.Rows.Count)
                        {
                            Time_Out_Str = mLocalDS1.Rows[mLocalDS1.Rows.Count - 1][0].ToString();
                        }
                        else
                        {
                            Time_Out_Str = "";
                        }
                        TimeSpan ts4Final = new TimeSpan();
                        ts4Final = Convert.ToDateTime(string.Format("{0:hh\\:mm}", Final_OT_Work_Time_1)).TimeOfDay;
                        if (mLocalDS.Rows.Count <= 0)
                        {
                            Time_IN_Str = "";
                        }
                        else
                        {
                            Time_IN_Str = mLocalDS.Rows[tin][0].ToString();
                        }
                        //Emp_Total_Work_Time
                        if (string.IsNullOrEmpty(Time_IN_Str) | string.IsNullOrEmpty(Time_Out_Str))
                        {
                            time_Check_dbl = time_Check_dbl;
                        }
                        else
                        {
                            DateTime date3Final = System.Convert.ToDateTime(Time_IN_Str);
                            DateTime date4Final = System.Convert.ToDateTime(Time_Out_Str);
                            TimeSpan ts1Final = new TimeSpan();
                            ts1Final = date4Final.Subtract(date3Final);
                            ts1Final = date4Final.Subtract(date3Final);
                            Total_Time_get = ts1Final.Hours.ToString();
                            //& ":" & Trim(ts.Minutes)
                            //ts4 = ts4.Add(ts1)
                            //OT Time Get
                            Final_OT_Work_Time_1 = ts4Final.Hours + ":" + ts4Final.Minutes;
                            Time_Minus_Value_Check = Final_OT_Work_Time_1.Split(':');
                            if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                Final_OT_Work_Time_1 = "00:00";
                            if (Left_Val(Final_OT_Work_Time_1, 1) == "0:" | Final_OT_Work_Time_1 == "0:-1" | Final_OT_Work_Time_1 == "0:-3")
                                Final_OT_Work_Time_1 = "00:00";
                            //MessageBox.Show(Emp_Total_Work_Time_1.ToString() + "")
                            if (Left_Val(Total_Time_get, 1) == "-")
                            {
                                date4Final = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                                ts1Final = date4Final.Subtract(date3Final);
                                ts1Final = date4Final.Subtract(date3Final);
                                ts4Final = ts4Final.Add(ts1Final);
                                Total_Time_get = ts1Final.Hours.ToString();
                                //& ":" & Trim(ts.Minutes)
                                time_Check_dbl = Total_Time_get;
                                Final_OT_Work_Time_1 = ts4Final.Hours + ":" + ts4Final.Minutes;
                                Time_Minus_Value_Check = Final_OT_Work_Time_1.Split(':');
                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    Final_OT_Work_Time_1 = "00:00";
                                if (Left_Val(Final_OT_Work_Time_1, 1) == "0:" | Final_OT_Work_Time_1 == "0:-1" | Final_OT_Work_Time_1 == "0:-3")
                                    Final_OT_Work_Time_1 = "00:00";
                            }
                            else
                            {
                                ts4Final = ts4Final.Add(ts1Final);
                                Final_OT_Work_Time_1 = ts4Final.Hours + ":" + ts4Final.Minutes;
                                Time_Minus_Value_Check = Final_OT_Work_Time_1.Split(':');
                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    Final_OT_Work_Time_1 = "00:00";
                                if (Left_Val(Final_OT_Work_Time_1, 1) == "0:" | Final_OT_Work_Time_1 == "0:-1" | Final_OT_Work_Time_1 == "0:-3")
                                    Final_OT_Work_Time_1 = "00:00";
                                time_Check_dbl = Total_Time_get;
                            }
                        }
                        //Final Total Time Get End
                    }
                    //Update Final Work Total Time
                    if (Final_OT_Work_Time_1 != "00:00")
                    {
                        //Find Week OFF
                        NFHDS = null;
                        qry_nfh = "Select NFHDate from NFH_Mst where NFHDate='" + Date_Value_Str + "'";
                        NFHDS = objdata.RptEmployeeMultipleDetails(qry_nfh);
                        if (NFHDS.Rows.Count > 0)
                        {
                            AutoDTable.Rows[mGrdRow][7] = "NH / " + Final_OT_Work_Time_1;
                            //fg.set_TextMatrix(mGrdRow, 6, "NH / " + time_Check_dbl);
                        }
                        else
                        {
                            Employee_Week_Name = (Convert.ToDateTime(Date_Value_Str).DayOfWeek).ToString();

                            //Get Employee Week OF DAY
                            SSQL = "Select WeekOff from Employee_MST where MachineID_Encrypt='" + OT_Week_OFF_Machine_No + "' And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";

                            // SSQL = "Select WeekOff from Shiftrrecording where MachineNo='" + iEmpDet + "'";     //OT_Week_OFF_Machine_No
                            NFHDS = objdata.RptEmployeeMultipleDetails(SSQL);
                            if (NFHDS.Rows.Count <= 0)
                            {
                                Assign_Week_Name = "";
                            }
                            else
                            {
                                Assign_Week_Name = NFHDS.Rows[0][0].ToString();
                            }
                            if (Employee_Week_Name == Assign_Week_Name)
                            {
                                AutoDTable.Rows[mGrdRow][7] = "WH / " + Final_OT_Work_Time_1;
                                // fg.set_TextMatrix(mGrdRow, 6, "WH / " + time_Check_dbl);
                            }
                            else
                            {
                                AutoDTable.Rows[mGrdRow][7] = Final_OT_Work_Time_1;
                                //AutoDTable.Rows[mGrdRow][6] = time_Check_dbl;
                                // fg.set_TextMatrix(mGrdRow, 6, time_Check_dbl);
                            }
                        }




                    }
                    else
                    {
                        AutoDTable.Rows[mGrdRow][7] = "";
                    }

                    //fg.set_TextMatrix(mGrdRow, 7, (Final_OT_Work_Time_1 != "00:00" ? Final_OT_Work_Time_1 : ""));
                }
                else
                {
                    time_Check_dbl = "";

                    if (mLocalDS.Rows.Count <= 0)
                    {
                        Time_IN_Str = "";
                        AutoDTable.Rows[mGrdRow][1] = Time_IN_Str;
                        //fg.set_TextMatrix(mGrdRow, 1, Time_IN_Str);
                    }
                    else
                    {
                        Time_IN_Str = mLocalDS.Rows[0][0].ToString();
                        AutoDTable.Rows[mGrdRow][1] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[0][0]);
                        //fg.set_TextMatrix(mGrdRow, 1, string.Format("{0:hh:mm tt}", mLocalDS.Tables[0].Rows[0][0]));
                    }
                    //Time Out Get
                    //If Machine_ID_Str = "iNMfGxLH3zBn/Sxh0+p9RQ==" Then
                    //    MessageBox.Show("" & "")
                    //End If
                    //SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" & Machine_ID_Str & "'"
                    //SSQL = SSQL & " And Compcode='" & iStr1(0) & "' And LocCode='" & iStr2(0) & "'"
                    //SSQL = SSQL & " And TimeOUT >='" & Date_Value_Str & " " & "10:00' And TimeOUT <='" & Date_Value_Str1 & " " & "10:00' Order by TimeOUT Asc"
                    //mLocalDS = ReturnMultipleValue(SSQL)
                    if (mLocalDS1.Rows.Count <= 0)
                    {
                        Time_Out_Str = "";
                        AutoDTable.Rows[mGrdRow][2] = Time_Out_Str;
                        //fg.set_TextMatrix(mGrdRow, 2, Time_Out_Str);
                    }
                    else
                    {
                        Time_Out_Str = mLocalDS1.Rows[0][0].ToString();
                        AutoDTable.Rows[mGrdRow][2] = string.Format("{0:hh:mm tt}", mLocalDS1.Rows[0][0]);
                        //fg.set_TextMatrix(mGrdRow, 2, string.Format("{0:hh:mm tt}", mLocalDS1.Tables(0).Rows(0)(0)));
                    }
                    for (int Z = 0; Z <= mLocalDS1.Rows.Count - 1; Z++)
                    {
                        if (Z == 0)
                        {
                            AutoDTable.Rows[mGrdRow][2] = string.Format("{0:hh:mm tt}", mLocalDS1.Rows[Z][0]);
                            //fg.set_TextMatrix(mGrdRow, 2, string.Format("{0:hh:mm tt}", mLocalDS1.Tables(0).Rows(Z)(0)));
                        }
                        else
                        {
                            AutoDTable.Rows[mGrdRow][5] = string.Format("{0:hh:mm tt}", mLocalDS1.Rows[Z][0]);
                            //fg.set_TextMatrix(mGrdRow, 5, string.Format("{0:hh:mm tt}", mLocalDS1.Tables(0).Rows(Z)(0)));
                        }
                    }
                    //Time Duration Get
                    if (string.IsNullOrEmpty(Time_IN_Str) | string.IsNullOrEmpty(Time_Out_Str))
                    {
                        time_Check_dbl = "";
                    }
                    else
                    {
                        date1 = System.Convert.ToDateTime(Time_IN_Str);
                        date2 = System.Convert.ToDateTime(Time_Out_Str);
                        TimeSpan ts = new TimeSpan();

                        ts = date2.Subtract(date1);
                        ts = date2.Subtract(date1);
                        Total_Time_get = ts.Hours.ToString();
                        //& ":" & Trim(ts.Minutes)
                        if (Left_Val(Total_Time_get, 1) == "-")
                        {
                            date2 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);

                            ts = date2.Subtract(date1);
                            ts = date2.Subtract(date1);
                            Total_Time_get = ts.Hours.ToString();

                            //Total_Time_get = Right_Val(Total_Time_get, Len(Total_Time_get) - 1)
                            time_Check_dbl = Total_Time_get + ":" + ts.Minutes;
                        }
                        else
                        {
                            time_Check_dbl = Total_Time_get + ":" + ts.Minutes;
                        }
                    }

                    //Find Week OFF
                    NFHDS = null;
                    qry_nfh = "Select NFHDate from NFH_Mst where NFHDate='" + Date_Value_Str + "'";
                    NFHDS = objdata.RptEmployeeMultipleDetails(qry_nfh);
                    if (NFHDS.Rows.Count > 0)
                    {
                        AutoDTable.Rows[mGrdRow][3] = "NH / " + time_Check_dbl;
                        AutoDTable.Rows[mGrdRow][7] = "NH / " + time_Check_dbl;

                        //fg.set_TextMatrix(mGrdRow, 3, "NH / " + time_Check_dbl);
                        //fg.set_TextMatrix(mGrdRow, 7, "NH / " + time_Check_dbl);
                    }
                    else
                    {
                        Employee_Week_Name = (Convert.ToDateTime(Date_Value_Str).DayOfWeek).ToString();
                        SSQL = "Select WeekOff from Employee_MST where MachineID_Encrypt='" + OT_Week_OFF_Machine_No + "' And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";

                        //SSQL = "Select WeekOff from Shiftrrecording where MachineNo='" + OT_Week_OFF_Machine_No + "'";
                        NFHDS = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (NFHDS.Rows.Count <= 0)
                        {
                            Assign_Week_Name = "";
                        }
                        else
                        {
                            Assign_Week_Name = NFHDS.Rows[0][0].ToString();
                        }
                        if (Employee_Week_Name == Assign_Week_Name)
                        {
                            AutoDTable.Rows[mGrdRow][3] = "WH / " + time_Check_dbl;
                            AutoDTable.Rows[mGrdRow][7] = "WH / " + time_Check_dbl;

                            //fg.set_TextMatrix(mGrdRow, 3, "WH / " + time_Check_dbl);
                            //fg.set_TextMatrix(mGrdRow, 7, "WH / " + time_Check_dbl);
                        }
                        else
                        {
                            AutoDTable.Rows[mGrdRow][3] = time_Check_dbl;
                            AutoDTable.Rows[mGrdRow][7] = time_Check_dbl;


                            //  fg.set_TextMatrix(mGrdRow, 3, time_Check_dbl);
                            // fg.set_TextMatrix(mGrdRow, 7, time_Check_dbl);
                        }
                        //fg.set_TextMatrix(mGrdRow, 3, time_Check_dbl)
                    }
                }




            }
            grid.DataSource = AutoDTable;
            grid.DataBind();
            string attachment = "attachment;filename=EMPLOYEE WISE DAY ATTENDANCE BETWEEN DATES.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);
            Response.Write("<table>");

            Response.Write("<tr Font-Bold='true' align='left'>");
            Response.Write("<td colspan='8'>");
            Response.Write("<a style=\"font-weight:bold\">EMPLOYEE WISE DAY ATTENDANCE BETWEEN DATES -" + Token_No_Display + "-" + Name_Display + "-" + Date1 + "TO" + Date2 + "</a>");


            Response.Write("</td>");
            Response.Write("</tr>");


            Response.Write("</table>");


            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();

        }
        catch (Exception ex)
        {

            return;
        }





    }


    private static string UTF8Decryption(string encryptpwd)
    {
        string decryptpwd = "0";
        BALDataAccess objdata_new = new BALDataAccess();

        string query = "Select * from Employee_Mst where MachineID_Encrypt='" + encryptpwd + "'";
        DataTable DT = new DataTable();
        DT = objdata_new.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            decryptpwd = DT.Rows[0]["MachineID"].ToString();
        }
        else
        {
            query = "Select * from Employee_Mst_New_Emp where MachineID_Encrypt='" + encryptpwd + "'";
            DT = objdata_new.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                decryptpwd = DT.Rows[0]["MachineID"].ToString();
            }
            else
            {
                decryptpwd = "0";
            }
        }

        //UTF8Encoding encodepwd = new UTF8Encoding();
        //Decoder Decode = encodepwd.GetDecoder();
        //byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
        //int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
        //char[] decoded_char = new char[charCount];
        //Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
        //decryptpwd = new String(decoded_char);
        return decryptpwd;
    }
    public static string GetRandom(int Min, int Max)
    {
        System.Random Generator = new System.Random();

        return Generator.Next(Min, Max).ToString();


    }
    public static string UTF8Encryption_OLD(string password)
    {
        string strmsg = string.Empty;
        byte[] encode = new byte[password.Length];
        encode = Encoding.UTF8.GetBytes(password);
        strmsg = Convert.ToBase64String(encode);
        return strmsg;
    }

    public string UTF8Encryption(string mvarPlanText)
    {
        string cipherText = "";
        try
        {
            string passPhrase = "Altius";
            string saltValue = "info@altius.co.in";
            string hashAlgorithm = "SHA1";
            string initVector = "@1B2c3D4e5F6g7H8";
            int passwordIterations = 2;
            int keySize = 256;
            byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
            byte[] saltValueBytes = Encoding.ASCII.GetBytes(saltValue);
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(mvarPlanText);
            PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, saltValueBytes, hashAlgorithm, passwordIterations);
            byte[] keyBytes = password.GetBytes(keySize / 8);
            RijndaelManaged symmetricKey = new RijndaelManaged();
            symmetricKey.Mode = CipherMode.CBC;
            ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes);
            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
            cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
            cryptoStream.FlushFinalBlock();
            byte[] cipherTextBytes = memoryStream.ToArray();
            memoryStream.Close();
            cryptoStream.Close();
            cipherText = Convert.ToBase64String(cipherTextBytes);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }

        return cipherText;
    }
}
