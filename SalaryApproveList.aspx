﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="SalaryApproveList.aspx.cs" Inherits="SalaryApproveList" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">



<script src="assets/js/master_list_jquery.min.js"></script>
<script src="assets/js/master_list_jquery-ui.min.js"></script>
<link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css"/>
 <script>
     $(document).ready(function() {
     $('#example').dataTable();
     });
 </script>


<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
                $('.select2').select2();
            }
        });
    };
</script>
<asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                <ContentTemplate>
<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Basic Update</a></li>
				<li class="active">Salary Approved List</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Salary Approved List</h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">Salary Approved List</h4>
                        </div>
                        <div class="panel-body">
                        <div class="form-group">
                        <!-- begin row -->
                          <div class="row">
                            <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Wages Type</label>
								 <asp:DropDownList runat="server" ID="ddlWages" class="form-control select2" style="width:100%;">
							 	 </asp:DropDownList>
							 	 <asp:RequiredFieldValidator ControlToValidate="ddlWages" InitialValue="-Select-" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                               </asp:RequiredFieldValidator>
								</div>
                               </div>
                           <!-- end col-4 -->
                           
                          <!-- begin col-4 -->
                          <div class="col-md-4">
                               <div class="form-group">
                                    <label class="col-md-12">Salary Type</label>
                                        <label class="checkbox-inline">
                                            <asp:CheckBox ID="chkNewWages" runat="server" AutoPostBack="true" 
                                        oncheckedchanged="chkNewWages_CheckedChanged"  />New Salary
                                        </label>
                                        <label class="checkbox-inline">
                                            <asp:CheckBox ID="chkOldWages" runat="server" AutoPostBack="true" 
                                        oncheckedchanged="chkOldWages_CheckedChanged"  />Salary Increment
                                        </label>
                                   
                                </div>
                          </div>
                          <!-- end col-4 -->
                           <!-- begin col-4 -->
                                <div class="col-md-4">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btnSearch" Text="Search" 
                                         ValidationGroup="Validate_Field" class="btn btn-success" 
                                         onclick="btnSearch_Click" />
									
								 </div>
                               </div>
                              <!-- end col-4 -->
                          </div>
                        <!-- end row -->
                        
                 
                             <!-- table start -->
					<div class="col-md-12">
					    <div class="row">
					        <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
			                    <HeaderTemplate>
                                    <table id="example" class="display table">
                                        <thead>
                                            <tr>
                                                <th>MachineID</th>
                                                <th>TokenNo</th>
                                                <th>EmpName</th>
                                                <th>DOJ</th>
                                                <th>Department</th>
                                                <th>Approve Date</th>
                                                <th>Old Salary</th>
                                                <th>New Salary</th>
                                              
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                    <td><%# Eval("MachineID")%></td>
                                    <td><%# Eval("ExistingCode")%></td>
                                    <td><%# Eval("FirstName")%></td>
                                    <td><%# Eval("DOJ")%></td>
                                    <td><%# Eval("DeptName")%></td>
                                    <td><%# Eval("Salary_Update_Date")%></td>
                                    <td><%# Eval("OLD_Salary")%></td>
                                    <td><%# Eval("New_Salary")%></td>
                                    
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater>
					    </div>
					</div>
					<!-- table End -->
                        
                        </div>
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
<!-- end #content -->
</ContentTemplate>
</asp:UpdatePanel>



</asp:Content>

