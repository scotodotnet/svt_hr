﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class ViewCivilAbstract : System.Web.UI.Page
{
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    ReportDocument rd = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();
    SqlConnection con;
    string str_month;
    string str_yr;
    string str_cate;
    string str_dept;
    string SessionCcode;
    string SessionLcode;
    string SessionAdmin;
    static string CmpName;
    static string Cmpaddress;
    string query;
    string fromdate;
    string ToDate;
    string salaryType;
    string Emp_ESI_Code;
    string SessionPayroll;

    string EmployeeType;
    string PayslipType;

    string PFTypeGet;

    string Left_Employee = "0";
    string Left_Date = "";
    string Get_Division_Name = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        //ReportDocument rd = new ReportDocument();
        SqlConnection con = new SqlConnection(constr);
        SessionAdmin = Session["Isadmin"].ToString();
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();

        str_cate = Request.QueryString["Cate"].ToString();
        str_dept = Request.QueryString["Depat"].ToString();
        str_month = Request.QueryString["Months"].ToString();
        str_yr = Request.QueryString["yr"].ToString();
        fromdate = Request.QueryString["fromdate"].ToString();
        ToDate = Request.QueryString["ToDate"].ToString();
        //salaryType = Request.QueryString["Salary"].ToString();

        EmployeeType = Request.QueryString["EmpType"].ToString();
        PayslipType = Request.QueryString["PayslipType"].ToString();

        Emp_ESI_Code = Request.QueryString["ESICode"].ToString();

        PFTypeGet = Request.QueryString["PFTypePost"].ToString();

        Left_Employee = Request.QueryString["Left_Emp"].ToString();
        Left_Date = Request.QueryString["Leftdate"].ToString();

        Load_DB();

        if (Emp_ESI_Code == "--Select--") { Emp_ESI_Code = ""; }

        int YR = 0;
        string report_head = "";
        if (str_month == "January")
        {
            YR = Convert.ToInt32(str_yr);
            YR = YR + 1;
        }
        else if (str_month == "February")
        {
            YR = Convert.ToInt32(str_yr);
            YR = YR + 1;
        }
        else if (str_month == "March")
        {
            YR = Convert.ToInt32(str_yr);
            YR = YR + 1;
        }
        else
        {
            YR = Convert.ToInt32(str_yr);
        }

        //salaryType = Request.QueryString["Salary"].ToString();
        DataTable dt = new DataTable();
        query = "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(query);
        //dt = objdata.Company_retrive(SessionCcode, SessionLcode);
        if (dt.Rows.Count > 0)
        {
            CmpName = dt.Rows[0]["Cname"].ToString();
            Cmpaddress = (dt.Rows[0]["Address1"].ToString() + ", " + dt.Rows[0]["Address2"].ToString() + ", " + dt.Rows[0]["Location"].ToString() + "-" + dt.Rows[0]["Pincode"].ToString());
        }
        Get_Division_Name = Request.QueryString["Division"].ToString();
        //Civil
        //query = "Select ED.DeptName as Department,MD.DeptName as DepartmentNm,sum(SD.WorkedDays) as WDay,sum(Cast(SD.OTHoursNew as decimal(18,2))) as OTHours," +
        //        " Sum(SD.RoundOffNetPay) as NetPay from [" + SessionPayroll + "]..SalaryDetails SD,Employee_Mst ED,Department_Mst MD "+
        //        " inner join MstEmployeeType ET on ET.EmpType=ED.Wages"+
        query = "Select ED.DeptName as Department,MD.DeptName as DepartmentNm,sum(SD.WorkedDays) as WDay," +
                "sum(Cast(SD.OTHoursNew as decimal(18,2))) as OTHours, Sum(SD.RoundOffNetPay) as NetPay" +
                " from Employee_Mst ED inner join Department_Mst MD on ED.DeptName COLLATE DATABASE_DEFAULT=MD.DeptName COLLATE DATABASE_DEFAULT" +
                " inner join [" + SessionPayroll + "]..SalaryDetails SD on ED.ExistingCode COLLATE DATABASE_DEFAULT=SD.ExisistingCode COLLATE DATABASE_DEFAULT" +
                " inner join MstEmployeeType ET on ET.EmpType COLLATE DATABASE_DEFAULT=ED.Wages COLLATE DATABASE_DEFAULT" +
                " where ED.EmpNo=SD.EmpNo" +
                " and ED.DeptCode=MD.DeptCode and SD.Ccode='" + SessionCcode + "' and SD.Lcode='" + SessionLcode + "'" +
                " and ED.CompCode='" + SessionCcode + "' and ED.LocCode='" + SessionLcode + "' and ET.EmpTypeCd='5'" +
                " and SD.Month='" + str_month + "' and SD.FromDate = convert(datetime,'" + fromdate + "', 105)" +
                " and SD.ToDate = Convert(Datetime,'" + ToDate + "', 105)" + //and SD.WagesType='" + salaryType + "'" +
                " and SD.FinancialYear='" + str_yr + "'";

        //Activate Employee Check
        //if (Left_Employee == "1")
        //{
        //    query = query + " and ED.ActivateMode='N'";
        //}
        //else
        //{
        //    query = query + " and ED.ActivateMode='Y'";
        //}

        //Activate Employee Check
        if (Left_Employee == "1") { query = query + " and ED.IsActive='No' and Month(convert(datetime,ED.DOR,105))=Month(convert(datetime,'" + Left_Date + "', 105)) And Year(convert(datetime,ED.DOR,105))=Year(convert(datetime,'" + Left_Date + "', 105))"; }
        else
        {
            if (Left_Date != "") { query = query + " and (convert(datetime,ED.DOR,105) > convert(datetime,'" + Left_Date + "', 105) Or ED.IsActive='Yes')"; }
        }

        //Add Division Condition
        if (Get_Division_Name != "" && Get_Division_Name != "-Select-")
        {
            query = query + " and ED.Division='" + Get_Division_Name + "'";
        }

        query = query + " group by ED.DeptName,MD.DeptName Order by MD.DeptName Asc";

        SqlCommand cmd = new SqlCommand(query, con);
        SqlDataAdapter sda = new SqlDataAdapter(cmd);
        DataSet ds1 = new DataSet();
        con.Open();
        sda.Fill(ds1);
        DataTable dt_1 = new DataTable();
        sda.Fill(dt_1);
        con.Close();
        if (ds1.Tables[0].Rows.Count > 0)
        {
            rd.Load(Server.MapPath("Payslip/CivilAbstract.rpt"));
            rd.SetDataSource(ds1.Tables[0]);

            rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
            rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
            rd.DataDefinition.FormulaFields["CompanyAddress1"].Text = "'" + Cmpaddress + "'";
            rd.DataDefinition.FormulaFields["ReportHead"].Text = "'" + "Departmentwise Abstract Wages Report From : " + fromdate + " To : " + ToDate + "'";
            rd.DataDefinition.FormulaFields["Division_Name_Str"].Text = "'" + "Division : " + Get_Division_Name + "'";

            rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");

            CrystalReportViewer1.ReportSource = rd;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No Data Found');", true);
        }
    }
    public void Load_DB()
    {
        //Get Database Name
        string query = "";
        DataTable dt_DB = new DataTable();
        query = "select CMS,Rights,Sales,Stores,Weaving,Maintaince,Production,Payroll from [SVT_Rights]..MstDBname";
        dt_DB = objdata.RptEmployeeMultipleDetails(query);
        if (dt_DB.Rows.Count > 0)
        {
            SessionPayroll = dt_DB.Rows[0]["Payroll"].ToString();
            //SessionRights = dt_DB.Rows[0]["Rights"].ToString();
        }
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        CrystalReportViewer1.Dispose();
    }
}
