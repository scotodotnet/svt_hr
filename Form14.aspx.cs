﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;

public partial class Form14 : System.Web.UI.Page
{
    DataTable dtdlocation = new DataTable();
    string fromdate;
    string todate;
    string Wages;
    BALDataAccess objdata = new BALDataAccess();
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    DataTable AutoDTable = new DataTable();
    DataTable DataCells = new DataTable();
    string SessionUserType;
    string SSQL = "";
    string mIpAddress_IN;
    string mIpAddress_OUT;
    DataTable dsEmployee = new DataTable();
    DateTime date1 = new DateTime();
    DateTime date2 = new DateTime();
    string RegNo;

    string Address1;
    string Address2;
    string PF_DOJ_Get_str;
    DateTime PF_DOJ_Get;

    string city;
    string Pincode;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report-FORM 14";
            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();

            fromdate = Request.QueryString["FromDate"].ToString();
            todate = Request.QueryString["ToDate"].ToString();
            string TempWages = Request.QueryString["WagesType"].ToString();
            Wages = TempWages.Replace("_", "&");




            System.Web.UI.WebControls.DataGrid grid =
                                 new System.Web.UI.WebControls.DataGrid();




            string Location_Full_Address_Join = "";
            DataTable dt = new DataTable();
            string Compname = "";
            SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "'";
            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt.Rows.Count != 0)
            {
                SessionCompanyName = dt.Rows[0]["CompName"].ToString();
            }



            SSQL = "Select * from Location_Mst where CompCode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
            dtdlocation = objdata.RptEmployeeMultipleDetails(SSQL);

            if (dtdlocation.Rows.Count > 0)
            {
                RegNo = dtdlocation.Rows[0]["Register_No"].ToString();
                Address1 = dtdlocation.Rows[0]["Add1"].ToString();
                Address2 = dtdlocation.Rows[0]["Add2"].ToString();
                city = dtdlocation.Rows[0]["City"].ToString();
                Pincode = dtdlocation.Rows[0]["Pincode"].ToString();

                if (Address1 != "")
                {
                    Location_Full_Address_Join = Location_Full_Address_Join + Address1;
                }
                if (Address2 != "")
                {
                    Location_Full_Address_Join = Location_Full_Address_Join + "," + Address2;
                }
                if (city != "")
                {
                    Location_Full_Address_Join = Location_Full_Address_Join + "," + city;
                }
                if (Pincode != "")
                {
                    Location_Full_Address_Join = Location_Full_Address_Join + "," + Pincode;
                }
            }
            else
            {
                RegNo = "";
            }

            date1 = Convert.ToDateTime(fromdate);
            date2 = Convert.ToDateTime(todate);

            //grid.DataSource = AutoDTable;
            //grid.DataBind();
            string attachment = "attachment;filename=Form14.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);

            Response.Write("<table border='1'; word-wrap:break-word;>");


            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='2'  border='0'>");
            Response.Write("</td>");

            Response.Write("<td colspan='5'>");
            Response.Write("<a style=\"font-weight:bold\">Form-14</a>");
            Response.Write("</td>");

            Response.Write("<td colspan='3'  border: 0;>");
            Response.Write("</td>");

            Response.Write("</tr>");

            Response.Write("<tr Font-Bold='true' align='center'>");

            Response.Write("<td colspan='2' BorderColor='White'>");
            Response.Write("</td>");

            Response.Write("<td colspan='5'>");
            Response.Write("<a style=\"font-weight:bold\">(REGISTER OF YOUNG PERSON WORKERS.)</a>");
            Response.Write("</td>");

            Response.Write("<td colspan='3' BorderColor='White'>");
            Response.Write("</td>");

            Response.Write("</tr>");

            Response.Write("<tr Font-Bold='true' align='center'>");

            Response.Write("<td colspan='2' BorderColor='White'>");
            Response.Write("</td>");

            Response.Write("<td colspan='5'>");
            Response.Write("<a style=\"font-weight:bold\">( Prescribed under Rule 86  of the factories'  Act 1948)</a>");
            Response.Write("</td>");

            Response.Write("<td colspan='3' BorderColor='White'>");
            Response.Write("</td>");

            Response.Write("</tr>");


            


            Response.Write("<tr Font-Bold='true' align='center'>");

            Response.Write("<td rowspan='2' colspan='2' valign='middle'>");
            Response.Write("<a style=\"font-weight:bold\">Name and address of the factory :</a>");
            Response.Write("</td>");

            Response.Write("<td Border=true  colspan='5' align='center'>");
            Response.Write("<a style=\"font-weight:bold\">" + SessionCompanyName + "</a>");
            Response.Write(" ");
            Response.Write("<a style=\"font-weight:bold\">" + SessionLcode + "</a>");
            Response.Write("</td>");

            Response.Write("<td Border=true  colspan='1' align='center'>");
            Response.Write("<a style=\"font-weight:bold\">Register No: " + RegNo + "</a>");
            Response.Write("</td>");

            Response.Write("<td Border=true  colspan='2' align='center'>");
            Response.Write("<a style=\"font-weight:bold\">Month & Year: " + date1.ToString("MMMM") + " " + date1.Year.ToString() + "</a>");
            Response.Write("</td>");

            Response.Write("</tr>");

            Response.Write("<tr Font-Bold='true' align='center'>");

            Response.Write("<td colspan='5'>");
            Response.Write("<a style=\"font-weight:bold\">" + Location_Full_Address_Join.ToString() + "</a>");
            Response.Write("</td>");

            Response.Write("<td colspan='3' BorderColor='White'>");
            Response.Write("</td>");

            Response.Write("</tr>");


            Response.Write("<tr align='center'>");

            Response.Write("<td Font-Bold='true' align='Middle'>");
            Response.Write("<a style=\"font-weight:bold\" align='center'>S.No</a>");
            Response.Write("</td>");


            Response.Write("<td Font-Bold='true' align='center' valign='middle'>");
            Response.Write("<a style=\"font-weight:bold;text-align:center;vertical-align:middle\">Name and residential adress of the worker</a>");
            Response.Write("</td>");

            
            Response.Write("<td Font-Bold='true' align='Middle'>");
            Response.Write("<a style=\"font-weight:bold\">Date of Birth / Age</a>");
            Response.Write("</td>");


            Response.Write("<td Font-Bold='true' align='Middle'>");
            Response.Write("<a style=\"font-weight:bold\">Father's Name</a>");
            Response.Write("</td>");

            Response.Write("<td Font-Bold='true' align='Middle'>");
            Response.Write("<a style=\"font-weight:bold\">Date of first employment</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">Nature of work on which being Employed</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">Serial number of certificate of fitness issued by certifying surgeon and its date</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">Token number giving reference to certificate</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">Letter Of group as in form No:11</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">Remarks</a>");
            Response.Write("</td>");

            Response.Write("</tr>");


            
            int dayCount = (int)((date2 - date1).TotalDays);

            //PF Date of Joining Condition Add
            SSQL = "";
            SSQL = "Select * from Employee_Mst where CompCode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
            SSQL = SSQL + " And Wages='" + Wages + "' And IsActive='Yes'";
            SSQL = SSQL + " And (CONVERT(DateTime,PFDOJ, 103) >= '" + date1.ToString("yyyy/MM/dd") + "'";
            SSQL = SSQL + " and (CONVERT(DateTime,PFDOJ, 103) <= '" + date2.ToString("yyyy/MM/dd") + "'))";
            //Check User Type
            if (SessionUserType == "2")
            {
                SSQL = SSQL + " And Eligible_PF='1'";
            }
            SSQL = SSQL + " And cast(convert(varchar(3),DATEDIFF(MONTH, BirthDate, GETDATE())/12) + '.' + ";
            SSQL = SSQL + " convert(varchar(2),DATEDIFF(MONTH, BirthDate, GETDATE()) % 12) as decimal(18,2)) <=18.0";
            SSQL = SSQL + "Order by ExistingCode Asc";



            //SSQL = "Select FirstName,Permanent_Dist,BirthDate,DATEDIFF(hour,BirthDate,GETDATE())/8766 AS Age,LastName,DOJ,DeptName,Designation,ExistingCode,MachineID,Address1";
            //SSQL = SSQL + " from Employee_Mst where CompCode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "' And Wages='" + Wages.ToString() + "' And IsActive='Yes'";
            ////SSQL = SSQL & " And DATEDIFF(hour,BirthDate,GETDATE())/8766 <=18 And Adolescent_Status='Pending'"
            ////PF Date of Joining Condition Add
            ////SSQL = SSQL & " And CAST(PFDOJ as datetime) between '" & Format(CDate(dtpFromDate.Text), "yyyy/MM/dd") & "' and '" & Format(CDate(dtpToDate.Text), "yyyy/MM/dd") & "'"
            //SSQL = SSQL + " And (CONVERT(DateTime,PFDOJ, 103) between CONVERT(DateTime, '" + date1.ToString("dd/MM/yyyy") + "', 103) and CONVERT(DateTime, '" + date1.ToString("dd/MM/yyyy") + "', 103))";
            //SSQL = SSQL + " And Adolescent='1'";
            //if (SessionUserType == "IF User")
            //{
            //    SSQL = SSQL + " And Eligible_PF='1'";
            //}
            //SSQL = SSQL + "Order by ExistingCode Asc";
           


            dsEmployee = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dsEmployee.Rows.Count > 0)
            {
                for (int i = 0; i < dsEmployee.Rows.Count; i++)
                {
                    Response.Write("<tr align='center'>");

                    Response.Write("<td>");
                    Response.Write("<a style=\"font-weight:bold\" align='center'>" + (i + 1).ToString() + "</a>");
                    Response.Write("</td>");

                    Response.Write("<td>");
                    Response.Write("<a style=\"font-weight:bold;text-align:center;vertical-align:middle\">" + dsEmployee.Rows[i]["FirstName"].ToString() + dsEmployee.Rows[i]["LastName"].ToString() + "," + dsEmployee.Rows[i]["Address1"].ToString() + "</a>");
                    Response.Write("</td>");

                    //Get Age
                    string a_query = "Select FLOOR((CAST (GetDate() AS INTEGER) - CAST(CONVERT(datetime,BirthDate,103)  AS INTEGER)) / 365.25) as Age_Dis from";
                    a_query = a_query + " Employee_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' And ExistingCode='" + dsEmployee.Rows[i]["ExistingCode"].ToString() + "'";
                    DataTable DT_Ag = new DataTable();
                    DT_Ag = objdata.RptEmployeeMultipleDetails(a_query);
                    string Age_Current = "";
                    if (DT_Ag.Rows.Count != 0)
                    {
                        Age_Current = DT_Ag.Rows[0]["Age_Dis"].ToString();
                    }

                    Response.Write("<td>");
                    Response.Write("<a style=\"font-weight:bold\">" + Convert.ToDateTime(dsEmployee.Rows[i]["BirthDate"].ToString()).ToString("dd/MM/yyyy") + ", " + Age_Current + " </a>");
                    Response.Write("</td>");

                    Response.Write("<td>");
                    Response.Write("<a style=\"font-weight:bold\">" + dsEmployee.Rows[i]["LastName"].ToString() + "</a>");
                    Response.Write("</td>");

                    Response.Write("<td>");
                    Response.Write("<a style=\"font-weight:bold\">" + Convert.ToDateTime(dsEmployee.Rows[i]["DOJ"].ToString()).ToString("dd/MM/yyyy") + "</a>");
                    Response.Write("</td>");

                    Response.Write("<td>");
                    Response.Write("<table>");
                    Response.Write("<tr><td>");
                    Response.Write("<a style=\"font-weight:bold\" style=\'word-wrap: break-word\'>" + dsEmployee.Rows[i]["DeptName"].ToString() + "</a>");
                    Response.Write("</td></tr>");
                    Response.Write("<tr><td>");
                    Response.Write("<a style=\"font-weight:bold\" style=\'word-wrap: break-word\'>" + dsEmployee.Rows[i]["Designation"].ToString() + "</a>");
                    Response.Write("</td></tr>");
                    Response.Write("</table>");
                    Response.Write("</td>");

                    //Response.Write("<td>");
                    //Response.Write("<a style=\"font-weight:bold\" style=\'word-wrap: break-word\'>" + dsEmployee.Rows[i]["DeptName"].ToString() + dsEmployee.Rows[i]["Designation"].ToString() + "</a>");
                    //Response.Write("</td>");

                   
                   

                    string MachineID = dsEmployee.Rows[i]["MachineID"].ToString();
                    DataTable dtdAdult = new DataTable();
                    string Certificate_No_Date_Join = "";

                    SSQL = "Select * from Adolcent_Emp_Det where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And MachineID='" + MachineID.ToString() + "'";
                    dtdAdult = objdata.RptEmployeeMultipleDetails(SSQL);
                    Response.Write("<td>");
                    if (dtdAdult.Rows.Count != 0)
                    {
                        for (int j = 0; j < dtdAdult.Rows.Count; j++)
                        {
                            if (j == 0) { Response.Write("<table>"); }
                            Certificate_No_Date_Join = dtdAdult.Rows[j]["Certificate_No"].ToString() + "/ " + dtdAdult.Rows[j]["Certificate_Date_Str"].ToString();
                            Response.Write("<tr><td>");
                            Response.Write("<a style=\"font-weight:bold\">" + Certificate_No_Date_Join + "</a>");
                            Response.Write("</td></tr>");
                            //Certificate_No_Date_Join = dtdAdult.Rows[j]["Certificate_No"].ToString();
                        }
                        Response.Write("</table>");
                    }
                    else
                    {
                        Response.Write("<a style=\"font-weight:bold\">" + Certificate_No_Date_Join + "</a>");
                    }
                    Response.Write("</td>");

                    //Response.Write("<td>");
                    //Response.Write("<a style=\"font-weight:bold\">" + Certificate_No_Date_Join + "</a>");
                    //Response.Write("</td>");

                    Response.Write("<td>");
                    Response.Write("<a style=\"font-weight:bold\">" + dsEmployee.Rows[i]["ExistingCode"].ToString() + "</a>");
                    Response.Write("</td>");

                    Response.Write("<td>");
                    Response.Write("<a style=\"font-weight:bold\">Day Shift</a>");
                    Response.Write("</td>");

                    Response.Write("<td>");
                    Response.Write("<a style=\"font-weight:bold\"></a>");
                    Response.Write("</td>");

                    Response.Write("</tr>");
                }
            }
            Response.Write("</table>");

            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();
        }
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        CrystalReportViewer1.Dispose();
    }

}
