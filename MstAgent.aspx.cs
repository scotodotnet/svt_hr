﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class MstAgent : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Agent Master";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");
        }

        Load_Data_Agent();
    }

    private void Load_Data_Agent()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from MstAgent";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater1.DataSource = DT;
        Repeater1.DataBind();
    }

    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {
        string query;
        DataTable DT = new DataTable();
      

        query = "select * from MstAgent where AgentID='" + e.CommandName.ToString() + "'"; 
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count > 0)
        {
            query = "delete from MstAgent where AgentID='" + e.CommandName.ToString() + "'"; 
            objdata.RptEmployeeMultipleDetails(query);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Agent Deleted Successfully...!');", true);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Data Found..!');", true);
        }
        Load_Data_Agent();
    }

    protected void GridEditEnquiryClick(object sender, CommandEventArgs e)
    {
        string query;
        DataTable DT = new DataTable();


        query = "select * from MstAgent where AgentID='" + e.CommandName.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count > 0)
        {
            txtAgentID.Value = DT.Rows[0]["AgentID"].ToString();
            txtAgentName.Text = DT.Rows[0]["AgentName"].ToString();
            txtMobileNo.Text = DT.Rows[0]["Mobile"].ToString();
            txtLocation.Text = DT.Rows[0]["Location"].ToString();
            txtCommission.Text = DT.Rows[0]["Commission"].ToString();
            txtAddress.Text = DT.Rows[0]["Address"].ToString();
            btnSave.Text = "Update";
        }
        else
        {
            txtAgentID.Value = "";
            txtAgentName.Text = "";
            txtMobileNo.Text = "";
            txtLocation.Text = "";
            txtCommission.Text = "0";
            btnSave.Text = "Save";
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SaveMode = "Insert";
        string query = "";
        DataTable DT = new DataTable();


        if (btnSave.Text == "Update")
        {
            SaveMode = "Update";

            query = "Update MstAgent set AgentName='" + txtAgentName.Text.ToUpper() + "',Mobile='" + txtMobileNo.Text + "',";
            query = query + "Location='" + txtLocation.Text + "',Commission='" + txtCommission.Text + "',Address='" + txtAddress.Text + "' where AgentID='" + txtAgentID.Value + "'";
            objdata.RptEmployeeMultipleDetails(query);

        }
        else
        {
            query = "select * from MstAgent where AgentName='" + txtAgentName.Text.ToUpper() + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);

            if (DT.Rows.Count > 0)
            {
                SaveMode = "Already";
            }
            else
            {
                SaveMode = "Insert";
                query = "Insert into MstAgent (AgentName,Mobile,Location,Commission,Address)";
                query = query + "values('" + txtAgentName.Text.ToUpper() + "','" + txtMobileNo.Text + "','" + txtLocation.Text + "','" + txtCommission.Text + "','" + txtAddress.Text + "')";
                objdata.RptEmployeeMultipleDetails(query);
            }
            
        }

        if (SaveMode == "Update")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Agent Updated Successfully...!');", true);
        }
        else if (SaveMode == "Insert")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Agent Saved Successfully...!');", true);
        }
        else if (SaveMode == "Already")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Agent Already Exists!');", true);
        }

        Load_Data_Agent();
        Clear_All_Field();
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }
    private void Clear_All_Field()
    {
        txtAgentID.Value = "";
        txtAgentName.Text = "";
        txtMobileNo.Text = "";
        txtLocation.Text = "";
        txtCommission.Text = "0";
        btnSave.Text = "Save";
        txtAddress.Text = "";
    }


}
