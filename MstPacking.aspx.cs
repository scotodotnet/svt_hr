﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class MstPacking : System.Web.UI.Page
{


    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SSQL = "";
    DataTable dt = new DataTable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();

        LoadGrid();

        if (!IsPostBack)
        {
            LoadEmpCat();
        }
    }

    private void LoadEmpCat()
    {
        SSQL = "";
        SSQL = "Select * from MstEmployeeType ";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlType.DataSource =dt;
        ddlType.DataTextField = "EmpType";
        ddlType.DataValueField = "EmpTypeCd";
        ddlType.DataBind();
        ddlType.Items.Insert(0,new ListItem("Select","0"));
    }

    private void LoadGrid()
    {
        SSQL = "";
        SSQL = "Select * from Mst_Packing";
        Repeater1.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater1.DataBind();
    }

    protected void ddlType_TextChanged(object sender, EventArgs e)
    {

    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (ddlType.SelectedIndex != 0 && txtPackWt.Text != string.Empty && txtRate.Text != string.Empty)
        {
            SSQL = "";
            SSQL = "Delete from Mst_Packing where EmpCatCode='" + ddlType.SelectedItem.Value+"'";
            objdata.RptEmployeeMultipleDetails(SSQL);
            SSQL = "";
            SSQL = "Insert into Mst_Packing values('"+ddlType.SelectedItem.Value+"','"+ddlType.SelectedItem.Text+"','"+txtPackWt.Text+"','"+txtRate.Text+"')";
            objdata.RptEmployeeMultipleDetails(SSQL);
            if (btnSave.Text == "Save")
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "Alert", "alert('Successfully Added');", true);
                btnClear_Click(sender, e);
            }else
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "Alert", "alert('Successfully Updated');", true);
                btnClear_Click(sender, e);
                btnSave.Text = "Save";
            }
            
            LoadGrid();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this,Page.GetType(),"Alert","alert('Please Check the Fields');",true);
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        ddlType.ClearSelection();
        txtPackWt.Text = string.Empty;
        txtRate.Text = string.Empty;
    }

    protected void btnEditIssueEntry_Grid_Command(object sender, CommandEventArgs e)
    {
        if (e.CommandName == "Edit")
        {
            DataTable dt = new DataTable();
            dt = objdata.RptEmployeeMultipleDetails("Select * from Mst_Packing where EmpCatName='" + e.CommandArgument.ToString() + "'");
            ddlType.SelectedValue = dt.Rows[0][0].ToString();
            txtPackWt.Text = dt.Rows[0][2].ToString();
            txtRate.Text = dt.Rows[0][3].ToString();
            btnSave.Text = "Update";
        }
    }

    protected void btnDeleteEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {
        if (e.CommandName == "Delete")
        {
            SSQL = "";
            SSQL = "Delete From Mst_Packing where EmpCatName='"+e.CommandArgument+"'";
            objdata.RptEmployeeMultipleDetails(SSQL);
            ScriptManager.RegisterStartupScript(this,this.GetType(),"Alert","alert('Category Deleted Successfully')",true);
            LoadGrid();
        }
    }
}