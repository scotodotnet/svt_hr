﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Data.OleDb;
using Payroll;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.IO;

public partial class AttendanceUpload : System.Web.UI.Page
{
    string SessionAdmin;
    string DepartmentCode;
    string Name_Upload;
    string Name_Upload1;
    BALDataAccess objdata = new BALDataAccess();
    AttenanceClass objatt = new AttenanceClass();
    string SessionCcode;
    string SessionLcode;
    string WagesType;
    string SessionUserType;
    string SessionEpay;
    string Query = "";

    string TempNFH = "0";
    string TempCL = "0";
    string TempThreeSide = "0";
    string TempFull = "0";
    string TempHome = "0";
    string TempOTNew = "";
    string TempWH_Work_Days = "0";
    string TempFixed_Days = "0";
    string TempNFH_Work_Days = "0";
    string TempNFH_Work_Manual = "0";
    string TempNFH_Work_Station = "0";
    string TempNFH_Count_Days = "0";
    string TempLBH_Count_Days = "0";

    SqlConnection con;
    String constr = ConfigurationManager.AppSettings["ConnectionString"];

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        SessionEpay = Session["SessionEpay"].ToString();
        con = new SqlConnection(constr);
        if (!IsPostBack)
        {
            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddlfinance.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }

        }
    }
    public void Load_TwoDates()
    {
        if (ddlMonths.SelectedValue != "-Select-" && ddlfinance.SelectedValue != "-Select-")
        {

            decimal Month_Total_days = 0;
            string Month_Last = "";
            string Year_Last = "0";
            string Temp_Years = "";
            string[] Years;
            string FromDate = "";
            string ToDate = "";

            Temp_Years = ddlfinance.SelectedValue;
            Years = Temp_Years.Split('-');
            if ((ddlMonths.SelectedValue == "January") || (ddlMonths.SelectedValue == "March") || (ddlMonths.SelectedValue == "May") || (ddlMonths.SelectedValue == "July") || (ddlMonths.SelectedValue == "August") || (ddlMonths.SelectedValue == "October") || (ddlMonths.SelectedValue == "December"))
            {
                Month_Total_days = 31;
            }
            else if ((ddlMonths.SelectedValue == "April") || (ddlMonths.SelectedValue == "June") || (ddlMonths.SelectedValue == "September") || (ddlMonths.SelectedValue == "November"))
            {
                Month_Total_days = 30;
            }

            else if (ddlMonths.SelectedValue == "February")
            {
                int yrs = (Convert.ToInt32(Years[0]) + 1);
                if ((yrs % 4) == 0)
                {
                    Month_Total_days = 29;
                }
                else
                {
                    Month_Total_days = 28;
                }
            }
            switch (ddlMonths.SelectedItem.Text)
            {
                case "January":
                    Month_Last = "01";
                    break;
                case "February":
                    Month_Last = "02";
                    break;
                case "March":
                    Month_Last = "03";
                    break;
                case "April":
                    Month_Last = "04";
                    break;
                case "May":
                    Month_Last = "05";
                    break;
                case "June":
                    Month_Last = "06";
                    break;
                case "July":
                    Month_Last = "07";
                    break;
                case "August":
                    Month_Last = "08";
                    break;
                case "September":
                    Month_Last = "09";
                    break;
                case "October":
                    Month_Last = "10";
                    break;
                case "November":
                    Month_Last = "11";
                    break;
                case "December":
                    Month_Last = "12";
                    break;
                default:
                    break;
            }

            if ((ddlMonths.SelectedValue == "January") || (ddlMonths.SelectedValue == "February") || (ddlMonths.SelectedValue == "March"))
            {
                Year_Last = Years[0];
            }
            else
            {
                Year_Last = Years[0];
            }
            FromDate = "01" + "/" + Month_Last + "/" + Year_Last;
            ToDate = Month_Total_days + "/" + Month_Last + "/" + Year_Last;

            txtFrom.Text = FromDate.ToString();
            txtTo.Text = ToDate.ToString();

        }
        else
        {
            txtFrom.Text = "";
            txtTo.Text = "";
        }


    }
    protected void ddlMonths_SelectedIndexChanged(object sender, EventArgs e)
    {
        //Load_TwoDates();
    }
    protected void ddlfinance_SelectedIndexChanged(object sender, EventArgs e)
    {
        //Load_TwoDates();
    }
    protected void btnUpload_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        try
        {

            if (ddlMonths.SelectedValue == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Month.');", true);
                //System.Windows.Forms.MessageBox.Show("Select the Month", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            //else if ((txtdays.Text.Trim() == "") || (txtdays.Text.Trim() == null))
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Days.');", true);
            //    //System.Windows.Forms.MessageBox.Show("Enter the Working days in a Month", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //    ErrFlag = true;
            //}
            //else if (Convert.ToInt32(txtdays.Text) == 0)
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the days Properly.');", true);
            //    //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            //    ErrFlag = true;
            //}
            else if (txtFrom.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the from Date Properly.');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (txtTo.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the To Date Properly.');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (txtNfh.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the To NFh Days Properly.');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            if (FileUpload.HasFile)
            {
                FileUpload.SaveAs(Server.MapPath("Upload/" + FileUpload.FileName));

            }
            if (!ErrFlag)
            {
                int Month_Mid_Total_Days_Count = 0;
                Month_Mid_Total_Days_Count = (int)((Convert.ToDateTime(txtTo.Text) - Convert.ToDateTime(txtFrom.Text)).TotalDays);
                Month_Mid_Total_Days_Count = Month_Mid_Total_Days_Count + 1;
                txtdays.Text = Month_Mid_Total_Days_Count.ToString();
                decimal total = (Convert.ToDecimal(txtdays.Text.Trim()));
                int total_check = 0;
                decimal days = 0;
                string from_date_check = txtFrom.Text.ToString();
                string to_date_check = txtTo.Text.ToString();
                from_date_check = from_date_check.Replace("/", "-");
                to_date_check = to_date_check.Replace("/", "-");
                txtFrom.Text = from_date_check;
                txtTo.Text = to_date_check;

                if (FileUpload.HasFile)
                {
                    FileUpload.SaveAs(Server.MapPath("Upload/" + FileUpload.FileName));

                }

                if ((ddlMonths.SelectedValue == "January") || (ddlMonths.SelectedValue == "March") || (ddlMonths.SelectedValue == "May") || (ddlMonths.SelectedValue == "July") || (ddlMonths.SelectedValue == "August") || (ddlMonths.SelectedValue == "October") || (ddlMonths.SelectedValue == "December"))
                {
                    days = 31;
                }
                else if ((ddlMonths.SelectedValue == "April") || (ddlMonths.SelectedValue == "June") || (ddlMonths.SelectedValue == "September") || (ddlMonths.SelectedValue == "November"))
                {
                    days = 30;
                }

                else if (ddlMonths.SelectedValue == "February")
                {
                    int yrs = (Convert.ToInt32(ddlfinance.SelectedValue) + 1);
                    if ((yrs % 4) == 0)
                    {
                        days = 29;
                    }
                    else
                    {
                        days = 28;
                    }
                }
                if (total > days)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the days properly.');", true);
                    ErrFlag = true;
                }
                if (!ErrFlag)
                {
                    string connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Server.MapPath("Upload/" + FileUpload.FileName) + ";" + "Extended Properties=Excel 8.0;";
                    // string connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + Server.MapPath("Upload/" + FileUpload.FileName) + ";" + "Extended Properties=\"Excel 12.0;IMEX=1\";";
                    OleDbConnection sSourceConnection = new OleDbConnection(connectionString);
                    //OleDbConnection sSourceConnection = new OleDbConnection(connectionString);
                    DataTable dts = new DataTable();

                    //sSourceConnection.Open();
                    //DataTable dtExcelSchema;
                    //dtExcelSchema = sSourceConnection.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                    //string SheetName = GettingSheetName(dtExcelSchema);
                    //sSourceConnection.Close();

                    using (sSourceConnection)
                    {
                        sSourceConnection.Open();

                        OleDbCommand command = new OleDbCommand("Select * FROM [Sheet1$];", sSourceConnection);
                        sSourceConnection.Close();

                        using (OleDbCommand cmd = sSourceConnection.CreateCommand())
                        {
                            command.CommandText = "Select * FROM [Sheet1$];";
                            sSourceConnection.Open();
                        }
                        using (OleDbDataReader dr = command.ExecuteReader())
                        {

                            if (dr.HasRows)
                            {

                            }

                        }
                        OleDbDataAdapter objDataAdapter = new OleDbDataAdapter();
                        objDataAdapter.SelectCommand = command;
                        DataSet ds = new DataSet();

                        objDataAdapter.Fill(ds);
                        DataTable dt = new DataTable();
                        dt = ds.Tables[0];
                        for (int i = 0; i < dt.Columns.Count; i++)
                        {
                            dt.Columns[i].ColumnName = dt.Columns[i].ColumnName.Replace(" ", string.Empty).ToString();

                        }
                        string constr = ConfigurationManager.AppSettings["ConnectionString"];

                        //For Weekly Wages
                        decimal Total_Hrw = 0;// Convert.ToDecimal(dt.Rows[j][3].ToString()) + Convert.ToDecimal(dt.Rows[j][5].ToString()) + Convert.ToDecimal(dt.Rows[j][7].ToString()) + Convert.ToDecimal(dt.Rows[j][9].ToString()) + Convert.ToDecimal(dt.Rows[j][11].ToString()) + Convert.ToDecimal(dt.Rows[j][13].ToString()) + Convert.ToDecimal(dt.Rows[j][15].ToString());
                        decimal Total_OTHrs = 0;
                        decimal Total_OT = 0;
                        if (ddlEmployeeType.SelectedItem.Text.ToUpper() == "WEEKLY".ToUpper() || ddlEmployeeType.SelectedItem.Text.ToUpper() == "WEEKLY-HINDI".ToUpper())
                        {
                            for (int j = 0; j < dt.Rows.Count; j++)
                            {

                                SqlConnection cn = new SqlConnection(constr);


                                //Get Employee Master Details
                                DataTable dt_Emp_Mst = new DataTable();
                                string MachineID = dt.Rows[j][1].ToString();
                                string Query_Emp_mst = "";
                                //Query_Emp_mst = "Select ED.FirstName as EmpName,ED.MachineID as MachineNo,ED.EmpNo,ED.ExistingCode as ExisistingCode,MD.DeptName as DepartmentNm, '2' as Wagestype,";
                                //Query_Emp_mst = Query_Emp_mst + " ED.MachineID as BiometricID from Employee_Mst ED inner Join Department_Mst MD on MD.DeptName=ED.DeptName";
                                //Query_Emp_mst = Query_Emp_mst + " where ED.CompCode='" + SessionCcode + "' ";
                                //Query_Emp_mst = Query_Emp_mst + " and ED.LocCode='" + SessionLcode + "' and ED.IsActive='Yes'";
                                //Query_Emp_mst = Query_Emp_mst + " and ED.MachineID='" + MachineID + "'";

                                Query_Emp_mst = "Select FirstName as EmpName,MachineID as MachineNo,EmpNo,ExistingCode as ExisistingCode,DeptName as DepartmentNm, '2' as Wagestype,";
                                Query_Emp_mst = Query_Emp_mst + " MachineID as BiometricID from Employee_Mst";
                                Query_Emp_mst = Query_Emp_mst + " where CompCode='" + SessionCcode + "' ";
                                Query_Emp_mst = Query_Emp_mst + " and LocCode='" + SessionLcode + "' and IsActive='Yes'";
                                Query_Emp_mst = Query_Emp_mst + " and MachineID='" + MachineID + "'";
                                if (SessionAdmin == "2")
                                {
                                    Query_Emp_mst = Query_Emp_mst + " and Eligible_PF='1'";
                                }
                                dt_Emp_Mst = objdata.RptEmployeeMultipleDetails(Query_Emp_mst);

                                //Select ED.FirstName as EmpName,ED.MachineID as MachineNo,ED.EmpNo,ED.ExistingCode as ExisistingCode,MD.DeptName,'2' as Wagestype,
                                //ED.MachineID as BiometricID from Employee_Mst ED inner Join Department_Mst MD on MD.DeptCode=ED.DeptCode
                                //where ED.CompCode='ESM'
                                //and ED.LocCode='UNIT I' and ED.IsActive='Yes' 
                                //and ED.MachineID='9';

                                if (dt_Emp_Mst.Rows.Count == 0)
                                {
                                    j = j + 2;
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Token No Not Found in Employee Master. The Row Number is " + j + "');", true);
                                    //System.Windows.Forms.MessageBox.Show("Enter the Machine ID. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                    ErrFlag = true;
                                }

                                string Department = dt_Emp_Mst.Rows[0]["DepartmentNm"].ToString();
                                string EmpNo = dt_Emp_Mst.Rows[0]["EmpNo"].ToString();

                                string ExistingCode = dt.Rows[j][1].ToString();
                                string FirstName = dt.Rows[j][2].ToString();
                                string WorkingDays = "0";
                                string MID_Present = "0";
                                if (SessionAdmin == "2")
                                {
                                    WorkingDays = dt.Rows[j][10].ToString();
                                    MID_Present = dt.Rows[j][12].ToString();
                                }
                                else
                                {
                                    WorkingDays = dt.Rows[j][17].ToString();
                                    MID_Present = dt.Rows[j][19].ToString();
                                }

                                //if (Department == "")
                                //{
                                //    j = j + 2;
                                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Department. The Row Number is " + j + "');", true);
                                //    //System.Windows.Forms.MessageBox.Show("Enter the Department. The Row number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                //    ErrFlag = true;
                                //}
                                if (MachineID == "")
                                {
                                    j = j + 2;
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Machine ID. The Row Number is " + j + "');", true);
                                    //System.Windows.Forms.MessageBox.Show("Enter the Machine ID. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                    ErrFlag = true;
                                }
                                //else if (EmpNo == "")
                                //{
                                //    j = j + 2;
                                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Employee Number. The Row Number is " + j + "');", true);
                                //    //System.Windows.Forms.MessageBox.Show("Enter the Employee Number. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                //    ErrFlag = true;
                                //}
                                else if (ExistingCode == "")
                                {
                                    j = j + 2;
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Existing Code. The Row Number is " + j + "');", true);
                                    //System.Windows.Forms.MessageBox.Show("Enter the Existing Code. The Roe Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                    ErrFlag = true;
                                }
                                else if (FirstName == "")
                                {
                                    j = j + 2;
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Name. The Row Number is " + j + "');", true);
                                    //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                    ErrFlag = true;
                                }
                                else if (WorkingDays == "")
                                {
                                    j = j + 2;
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Working Days. The Row Number is " + j + "');", true);
                                    //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                    ErrFlag = true;
                                }
                                //else if (Home == "")
                                //{
                                //    j = j + 2;
                                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Working Days. The Row Number is " + j + "');", true);
                                //    //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                //    ErrFlag = true;
                                //}
                                //else if (CL == "")
                                //{
                                //    j = j + 2;
                                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the CL Days. The Row Number is " + j + "');", true);
                                //    //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                //    ErrFlag = true;
                                //}
                                //else if (AbsentDays == "")
                                //{
                                //    j = j + 2;
                                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the N/FH Days properly. The Row Number is " + j + "');", true);
                                //    //System.Windows.Forms.MessageBox.Show("Enter the Days Properly. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                //    ErrFlag = true;
                                //}
                                //else if (OT_Days == "")
                                //{
                                //    j = j + 2;
                                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the OT Days. The Row Number is " + j + "');", true);
                                //    //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                //    ErrFlag = true;
                                //}
                                //else if (Full == "")
                                //{
                                //    j = j + 2;
                                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the F/N Shift Days. The Row Number is " + j + "');", true);
                                //    //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                //    ErrFlag = true;
                                //}
                                //else if (Home == "")
                                //{
                                //    j = j + 2;
                                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Canteen Days Minus. The Row Number is " + j + "');", true);
                                //    //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                //    ErrFlag = true;
                                //}
                                //else if ((Convert.ToDecimal(Half)) > Convert.ToDecimal(WorkingDays))
                                //{
                                //    j = j + 2;
                                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the O/N Days Properly. The Row Number is " + j + "');", true);
                                //    //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                //    ErrFlag = true;
                                //}
                                //else if ((Convert.ToDecimal(Full)) > Convert.ToDecimal(WorkingDays))
                                //{
                                //    j = j + 2;
                                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the O/N Days Properly. The Row Number is " + j + "');", true);
                                //    //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                //    ErrFlag = true;
                                //}
                                //else if ((Convert.ToDecimal(ThreeSided)) > Convert.ToDecimal(WorkingDays))
                                //{
                                //    j = j + 2;
                                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the ThreeSided Days Properly. The Row Number is " + j + "');", true);
                                //    //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                //    ErrFlag = true;
                                //}

                                else if ((Convert.ToDecimal(WorkingDays) + Convert.ToDecimal(txtNfh.Text)) > 31)
                                {
                                    j = j + 2;
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Days properly. The Row Number is " + j + "');", true);
                                    //System.Windows.Forms.MessageBox.Show("Enter the Days Properly. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                    ErrFlag = true;
                                }


                                //else if (weekoff == "")
                                //{
                                //    j = j + 2;
                                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Weekoff Days Properly');", true);
                                //    ErrFlag = true;
                                //}
                                cn.Open();
                                string qry_dpt = "Select DeptName as DepartmentNm from Department_Mst where DeptName = '" + Department + "'";
                                SqlCommand cmd_dpt = new SqlCommand(qry_dpt, cn);
                                SqlDataReader sdr_dpt = cmd_dpt.ExecuteReader();
                                if (sdr_dpt.HasRows)
                                {
                                }
                                else
                                {
                                    //j = j + 2;
                                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Department name not found in the department Details. The Row Number is " + j + "');", true);
                                    ////System.Windows.Forms.MessageBox.Show("Department Name not Found in Department Details. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                    //ErrFlag = true;
                                }
                                cn.Close();
                                cn.Open();
                                string qry_dpt1 = "Select ED.DeptCode as Department from Employee_Mst ED inner Join Department_Mst MSt on Mst.DeptName=ED.DeptName where Mst.DeptName = '" + Department + "' and ED.EmpNo='" + EmpNo + "'";
                                SqlCommand cmd_dpt1 = new SqlCommand(qry_dpt1, cn);
                                SqlDataReader sdr_dpt1 = cmd_dpt1.ExecuteReader();
                                if (sdr_dpt1.HasRows)
                                {
                                }
                                else
                                {
                                    //j = j + 2;
                                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Department name not found in the department Details. The Row Number is " + j + "');", true);
                                    ////System.Windows.Forms.MessageBox.Show("Department Name not Found in Department Details. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                    //ErrFlag = true;
                                }
                                cn.Close();
                                cn.Open();
                                //string Qry_SalaryType = "Select EmpNo from Officialprofile where WagesType ='" + rbsalary.SelectedValue + "'";
                                //SqlCommand cmd_wages = new SqlCommand(Qry_SalaryType, cn);
                                //SqlDataReader sdr_wages = cmd_wages.ExecuteReader();
                                //if (sdr_wages.HasRows)
                                //{
                                //}
                                //else
                                //{
                                //    j = j + 2;
                                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('This " + EmpNo + " is Wages Type is incorrect.');", true);
                                //    //System.Windows.Forms.MessageBox.Show("Department Name not Found in Department Details. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                //    ErrFlag = true;
                                //}
                                cn.Close();
                                cn.Open();
                                string qry_empNo = "Select EmpNo from Employee_Mst where EmpNo= '" + EmpNo + "' and ExistingCode = '" + ExistingCode + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                                SqlCommand cmd_Emp = new SqlCommand(qry_empNo, cn);
                                SqlDataReader sdr_Emp = cmd_Emp.ExecuteReader();
                                if (sdr_Emp.HasRows)
                                {
                                }
                                else
                                {
                                    j = j + 2;
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Employee Details Properly. The Row Number is " + j + "');", true);
                                    //System.Windows.Forms.MessageBox.Show("Enter the Employee Details Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                    ErrFlag = true;
                                }
                                cn.Close();
                                total = (Convert.ToDecimal(WorkingDays) + Convert.ToDecimal(txtNfh.Text.Trim()));
                                days = 0;
                                if ((ddlMonths.SelectedValue == "January") || (ddlMonths.SelectedValue == "March") || (ddlMonths.SelectedValue == "May") || (ddlMonths.SelectedValue == "July") || (ddlMonths.SelectedValue == "August") || (ddlMonths.SelectedValue == "October") || (ddlMonths.SelectedValue == "December"))
                                {
                                    days = 31;
                                }
                                else if ((ddlMonths.SelectedValue == "April") || (ddlMonths.SelectedValue == "June") || (ddlMonths.SelectedValue == "September") || (ddlMonths.SelectedValue == "November"))
                                {
                                    days = 30;
                                }
                                else if (ddlMonths.SelectedValue == "February")
                                {
                                    int yrs = (Convert.ToInt32(ddlfinance.SelectedValue) + 1);
                                    if ((yrs % 4) == 0)
                                    {
                                        days = 29;
                                    }
                                    else
                                    {
                                        days = 28;
                                    }
                                }
                                total_check = Convert.ToInt32(total);
                                if (total_check > Convert.ToInt32(days))
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the days properly.');", true);
                                    ErrFlag = true;
                                }
                                //total_check = Convert.ToInt32(total);
                                //if (total_check < Convert.ToInt32(txtdays.Text))
                                //{
                                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the days properly.');", true);
                                //    ErrFlag = true;
                                //}
                                decimal Att_sum = (Convert.ToDecimal(WorkingDays) + Convert.ToDecimal(txtNfh.Text.Trim()));
                                int Att_Sum_Check = 0;
                                Att_Sum_Check = Convert.ToInt32(Att_sum);
                                if (Convert.ToInt32(days) < Att_Sum_Check)
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Attenance Details Properly ...');", true);
                                    ErrFlag = true;
                                }

                                //if ((Convert.ToDateTime(txtTo.Text) + Convert.ToDateTime(txtFrom.Text)).TotalDays > 7)
                                //{
                                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Check the From Date and To Date ...');", true);
                                //    ErrFlag = true;
                                //}
                            }
                        }


                        //for monthly Wages
                        else
                        {
                            for (int j = 0; j < dt.Rows.Count; j++)
                            {

                                SqlConnection cn = new SqlConnection(constr);


                                //Get Employee Master Details
                                DataTable dt_Emp_Mst = new DataTable();
                                string MachineID = dt.Rows[j][0].ToString();
                                string Query_Emp_mst = "";
                                //Query_Emp_mst = "Select ED.FirstName as EmpName,ED.MachineID as MachineNo,ED.EmpNo,ED.ExistingCode as ExisistingCode,MD.DeptName as DepartmentNm, '2' as Wagestype,";
                                //Query_Emp_mst = Query_Emp_mst + " ED.MachineID as BiometricID from Employee_Mst ED inner Join Department_Mst MD on MD.DeptCode=ED.DeptCode";
                                //Query_Emp_mst = Query_Emp_mst + " where ED.CompCode='" + SessionCcode + "'";
                                //Query_Emp_mst = Query_Emp_mst + " and ED.LocCode='" + SessionLcode + "' and ED.IsActive='Yes'";
                                //Query_Emp_mst = Query_Emp_mst + " and ED.MachineID='" + MachineID + "'";

                                Query_Emp_mst = "Select FirstName as EmpName,MachineID as MachineNo,EmpNo,ExistingCode as ExisistingCode,DeptName as DepartmentNm, '2' as Wagestype,";
                                Query_Emp_mst = Query_Emp_mst + " MachineID as BiometricID from Employee_Mst ";
                                Query_Emp_mst = Query_Emp_mst + " where CompCode='" + SessionCcode + "'";
                                Query_Emp_mst = Query_Emp_mst + " and LocCode='" + SessionLcode + "' and IsActive='Yes'";
                                Query_Emp_mst = Query_Emp_mst + " and MachineID='" + MachineID + "'";

                                dt_Emp_Mst = objdata.RptEmployeeMultipleDetails(Query_Emp_mst);

                                //Select ED.FirstName as EmpName,ED.MachineID as MachineNo,ED.EmpNo,ED.ExistingCode as ExisistingCode,MD.DeptName,'2' as Wagestype,
                                //ED.MachineID as BiometricID from Employee_Mst ED inner Join Department_Mst MD on MD.DeptCode=ED.DeptCode
                                //where ED.CompCode='ESM'
                                //and ED.LocCode='UNIT I' and ED.IsActive='Yes' 
                                //and ED.MachineID='9';

                                if (dt_Emp_Mst.Rows.Count == 0)
                                {
                                    j = j + 2;
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Token No Not Found in Employee Master. The Row Number is " + j + "');", true);
                                    //System.Windows.Forms.MessageBox.Show("Enter the Machine ID. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                    ErrFlag = true;
                                }

                                string Department = dt_Emp_Mst.Rows[0]["DepartmentNm"].ToString();
                                string EmpNo = dt_Emp_Mst.Rows[0]["EmpNo"].ToString();

                                string ExistingCode = dt.Rows[j][1].ToString();
                                string FirstName = dt.Rows[j][2].ToString();
                                string WorkingDays = dt.Rows[j][3].ToString();
                                string CL = dt.Rows[j][4].ToString();
                                string AbsentDays = dt.Rows[j][5].ToString();
                                string OT_Days = dt.Rows[j][6].ToString();
                                string Full = dt.Rows[j][7].ToString();
                                string Home = dt.Rows[j][8].ToString();
                                string MID_Present = dt.Rows[j][10].ToString();
                                string weekoff = "0";//dt.Rows[j][9].ToString();


                                //if (Department == "")
                                //{
                                //    j = j + 2;
                                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Department. The Row Number is " + j + "');", true);
                                //    //System.Windows.Forms.MessageBox.Show("Enter the Department. The Row number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                //    ErrFlag = true;
                                //}
                                if (MachineID == "")
                                {
                                    j = j + 2;
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Machine ID. The Row Number is " + j + "');", true);
                                    //System.Windows.Forms.MessageBox.Show("Enter the Machine ID. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                    ErrFlag = true;
                                }
                                //else if (EmpNo == "")
                                //{
                                //    j = j + 2;
                                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Employee Number. The Row Number is " + j + "');", true);
                                //    //System.Windows.Forms.MessageBox.Show("Enter the Employee Number. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                //    ErrFlag = true;
                                //}
                                else if (ExistingCode == "")
                                {
                                    j = j + 2;
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Existing Code. The Row Number is " + j + "');", true);
                                    //System.Windows.Forms.MessageBox.Show("Enter the Existing Code. The Roe Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                    ErrFlag = true;
                                }
                                else if (FirstName == "")
                                {
                                    j = j + 2;
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Name. The Row Number is " + j + "');", true);
                                    //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                    ErrFlag = true;
                                }
                                else if (WorkingDays == "")
                                {
                                    j = j + 2;
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Working Days. The Row Number is " + j + "');", true);
                                    //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                    ErrFlag = true;
                                }
                                //else if (Home == "")
                                //{
                                //    j = j + 2;
                                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Working Days. The Row Number is " + j + "');", true);
                                //    //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                //    ErrFlag = true;
                                //}
                                else if (CL == "")
                                {
                                    j = j + 2;
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the CL Days. The Row Number is " + j + "');", true);
                                    //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                    ErrFlag = true;
                                }
                                else if (AbsentDays == "")
                                {
                                    j = j + 2;
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the N/FH Days properly. The Row Number is " + j + "');", true);
                                    //System.Windows.Forms.MessageBox.Show("Enter the Days Properly. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                    ErrFlag = true;
                                }
                                else if (OT_Days == "")
                                {
                                    j = j + 2;
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the OT Days. The Row Number is " + j + "');", true);
                                    //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                    ErrFlag = true;
                                }
                                else if (Full == "")
                                {
                                    j = j + 2;
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the F/N Shift Days. The Row Number is " + j + "');", true);
                                    //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                    ErrFlag = true;
                                }
                                else if (Home == "")
                                {
                                    j = j + 2;
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Canteen Days Minus. The Row Number is " + j + "');", true);
                                    //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                    ErrFlag = true;
                                }
                                //else if ((Convert.ToDecimal(Half)) > Convert.ToDecimal(WorkingDays))
                                //{
                                //    j = j + 2;
                                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the O/N Days Properly. The Row Number is " + j + "');", true);
                                //    //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                //    ErrFlag = true;
                                //}
                                //else if ((Convert.ToDecimal(Full)) > Convert.ToDecimal(WorkingDays))
                                //{
                                //    j = j + 2;
                                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the O/N Days Properly. The Row Number is " + j + "');", true);
                                //    //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                //    ErrFlag = true;
                                //}
                                //else if ((Convert.ToDecimal(ThreeSided)) > Convert.ToDecimal(WorkingDays))
                                //{
                                //    j = j + 2;
                                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the ThreeSided Days Properly. The Row Number is " + j + "');", true);
                                //    //System.Windows.Forms.MessageBox.Show("Enter the Name. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                //    ErrFlag = true;
                                //}

                                else if ((Convert.ToDecimal(WorkingDays) + Convert.ToDecimal(txtNfh.Text)) > 31)
                                {
                                    j = j + 2;
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Days properly. The Row Number is " + j + "');", true);
                                    //System.Windows.Forms.MessageBox.Show("Enter the Days Properly. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                    ErrFlag = true;
                                }


                                //else if (weekoff == "")
                                //{
                                //    j = j + 2;
                                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Weekoff Days Properly');", true);
                                //    ErrFlag = true;
                                //}
                                cn.Open();
                                string qry_dpt = "Select DeptName as DepartmentNm from Department_Mst where DeptName = '" + Department + "'";
                                SqlCommand cmd_dpt = new SqlCommand(qry_dpt, cn);
                                SqlDataReader sdr_dpt = cmd_dpt.ExecuteReader();
                                if (sdr_dpt.HasRows)
                                {
                                }
                                else
                                {
                                    j = j + 2;
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Department name not found in the department Details. The Row Number is " + j + "');", true);
                                    //System.Windows.Forms.MessageBox.Show("Department Name not Found in Department Details. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                    ErrFlag = true;
                                }
                                cn.Close();
                                cn.Open();
                                string qry_dpt1 = "Select ED.DeptCode as Department from Employee_Mst ED inner Join Department_Mst MSt on Mst.DeptCode=ED.DeptCode where Mst.DeptName = '" + Department + "' and ED.EmpNo='" + EmpNo + "'";
                                SqlCommand cmd_dpt1 = new SqlCommand(qry_dpt1, cn);
                                SqlDataReader sdr_dpt1 = cmd_dpt1.ExecuteReader();
                                if (sdr_dpt1.HasRows)
                                {
                                }
                                else
                                {
                                    j = j + 2;
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Department name not found in the department Details. The Row Number is " + j + "');", true);
                                    //System.Windows.Forms.MessageBox.Show("Department Name not Found in Department Details. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                    ErrFlag = true;
                                }
                                cn.Close();
                                cn.Open();
                                //string Qry_SalaryType = "Select EmpNo from Officialprofile where WagesType ='" + rbsalary.SelectedValue + "'";
                                //SqlCommand cmd_wages = new SqlCommand(Qry_SalaryType, cn);
                                //SqlDataReader sdr_wages = cmd_wages.ExecuteReader();
                                //if (sdr_wages.HasRows)
                                //{
                                //}
                                //else
                                //{
                                //    j = j + 2;
                                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('This " + EmpNo + " is Wages Type is incorrect.');", true);
                                //    //System.Windows.Forms.MessageBox.Show("Department Name not Found in Department Details. The Row Number is " + j, "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                //    ErrFlag = true;
                                //}
                                cn.Close();
                                cn.Open();
                                string qry_empNo = "Select EmpNo from Employee_Mst where EmpNo= '" + EmpNo + "' and ExistingCode = '" + ExistingCode + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                                SqlCommand cmd_Emp = new SqlCommand(qry_empNo, cn);
                                SqlDataReader sdr_Emp = cmd_Emp.ExecuteReader();
                                if (sdr_Emp.HasRows)
                                {
                                }
                                else
                                {
                                    j = j + 2;
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Employee Details Properly. The Row Number is " + j + "');", true);
                                    //System.Windows.Forms.MessageBox.Show("Enter the Employee Details Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                                    ErrFlag = true;
                                }
                                cn.Close();
                                total = (Convert.ToDecimal(WorkingDays) + Convert.ToDecimal(txtNfh.Text.Trim()) + Convert.ToDecimal(CL) + Convert.ToDecimal(AbsentDays));
                                days = 0;
                                if ((ddlMonths.SelectedValue == "January") || (ddlMonths.SelectedValue == "March") || (ddlMonths.SelectedValue == "May") || (ddlMonths.SelectedValue == "July") || (ddlMonths.SelectedValue == "August") || (ddlMonths.SelectedValue == "October") || (ddlMonths.SelectedValue == "December"))
                                {
                                    days = 31;
                                }
                                else if ((ddlMonths.SelectedValue == "April") || (ddlMonths.SelectedValue == "June") || (ddlMonths.SelectedValue == "September") || (ddlMonths.SelectedValue == "November"))
                                {
                                    days = 30;
                                }
                                else if (ddlMonths.SelectedValue == "February")
                                {
                                    int yrs = (Convert.ToInt32(ddlfinance.SelectedValue) + 1);
                                    if ((yrs % 4) == 0)
                                    {
                                        days = 29;
                                    }
                                    else
                                    {
                                        days = 28;
                                    }
                                }
                                total_check = Convert.ToInt32(total);
                                if (total_check > Convert.ToInt32(days))
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the days properly.');", true);
                                    ErrFlag = true;
                                }
                                //total_check = Convert.ToInt32(total);
                                //if (total_check < Convert.ToInt32(txtdays.Text))
                                //{
                                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the days properly.');", true);
                                //    ErrFlag = true;
                                //}
                                decimal Att_sum = (Convert.ToDecimal(WorkingDays) + Convert.ToDecimal(txtNfh.Text.Trim()) + Convert.ToDecimal(AbsentDays) + Convert.ToDecimal(CL));
                                int Att_Sum_Check = 0;
                                Att_Sum_Check = Convert.ToInt32(Att_sum);
                                if (Convert.ToInt32(days) < Att_Sum_Check)
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Attenance Details Properly ...');", true);
                                    ErrFlag = true;
                                }
                            }
                        }



                        if (!ErrFlag)
                        {
                            // for Weekly Wages
                            if (ddlEmployeeType.SelectedItem.Text.ToUpper() == "WEEKLY".ToUpper() || ddlEmployeeType.SelectedItem.Text == "WEEKLY-HINDI".ToUpper())
                            {
                                // DateTime MyDate = DateTime.ParseExact(txtFrom.Text, "dd-MM-yyyy", null);
                                // DateTime MyDate1 = DateTime.ParseExact(txtTo.Text, "dd-MM-yyyy", null);

                                //if ((MyDate1-MyDate).TotalDays>7|(MyDate1-my))


                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    decimal Total_Days = 0;
                                    Total_OT = 0;
                                    //Get Employee Master Details
                                    DataTable dt_Emp_Mst = new DataTable();
                                    string MachineID = dt.Rows[i][1].ToString();
                                    string Query_Emp_mst = "";
                                    string MID_Present = dt.Rows[i][12].ToString();

                                    string DayAT_1 = "0";
                                    string DayAT_2 = "0";
                                    string DayAT_3 = "0";
                                    string DayAT_4 = "0";
                                    string DayAT_5 = "0";
                                    string DayAT_6 = "0";
                                    string DayAT_7 = "0";

                                    string DayOT_1 = "0";
                                    string DayOT_2 = "0";
                                    string DayOT_3 = "0";
                                    string DayOT_4 = "0";
                                    string DayOT_5 = "0";
                                    string DayOT_6 = "0";
                                    string DayOT_7 = "0";

                                    //Query_Emp_mst = "Select ED.Wages, ED.FirstName as EmpName,ED.MachineID as MachineNo,ED.EmpNo,ED.ExistingCode as ExisistingCode,MD.DeptName as DepartmentNm, '2' as Wagestype,";
                                    //Query_Emp_mst = Query_Emp_mst + " ED.MachineID as BiometricID from Employee_Mst ED inner Join Department_Mst MD on MD.DeptName=ED.DeptName";
                                    //Query_Emp_mst = Query_Emp_mst + " where ED.CompCode='" + SessionCcode + "'";
                                    //Query_Emp_mst = Query_Emp_mst + " and ED.LocCode='" + SessionLcode + "' and ED.IsActive='Yes'";
                                    //Query_Emp_mst = Query_Emp_mst + " and ED.MachineID='" + MachineID + "'";


                                    Query_Emp_mst = "Select Wages, FirstName as EmpName,MachineID as MachineNo,EmpNo,ExistingCode as ExisistingCode,DeptName as DepartmentNm, '2' as Wagestype,";
                                    Query_Emp_mst = Query_Emp_mst + " MachineID as BiometricID from Employee_Mst ";
                                    Query_Emp_mst = Query_Emp_mst + " where CompCode='" + SessionCcode + "'";
                                    Query_Emp_mst = Query_Emp_mst + " and LocCode='" + SessionLcode + "' and IsActive='Yes'";
                                    Query_Emp_mst = Query_Emp_mst + " and MachineID='" + MachineID + "'";


                                    dt_Emp_Mst = objdata.RptEmployeeMultipleDetails(Query_Emp_mst);


                                    for (int nullchk = 3; nullchk < dt.Columns.Count; nullchk++)
                                    {
                                        if (dt.Rows[i][nullchk].ToString() == "" | dt.Rows[i][nullchk].ToString() == string.Empty | dt.Rows[i][nullchk].ToString() == null)
                                        {
                                            dt.Rows[i][nullchk] = "0";
                                        }
                                    }
                                    if (MachineID == "15119")
                                    {
                                        string dk = "0";
                                    }

                                    if (SessionAdmin != "2")
                                    {
                                        for (int otcnt = 4; otcnt < dt.Columns.Count - 1;)
                                        {
                                            if (dt.Rows[i][otcnt].ToString() != "0" | dt.Rows[i][otcnt].ToString() != string.Empty | dt.Rows[i][otcnt].ToString() != null)
                                            {
                                                if (Convert.ToDecimal(dt.Rows[i][otcnt].ToString()) >= 4)
                                                {
                                                    Total_OT = Total_OT + 1;
                                                }
                                            }
                                            otcnt += 2;
                                        }
                                    }
                                    else
                                    {
                                        Total_OT = 0;
                                    }


                                    if (SessionAdmin != "2")
                                    {
                                        Total_Hrw = Convert.ToDecimal(dt.Rows[i][3].ToString()) + Convert.ToDecimal(dt.Rows[i][5].ToString()) + Convert.ToDecimal(dt.Rows[i][7].ToString()) + Convert.ToDecimal(dt.Rows[i][9].ToString()) + Convert.ToDecimal(dt.Rows[i][11].ToString()) + Convert.ToDecimal(dt.Rows[i][13].ToString()) + Convert.ToDecimal(dt.Rows[i][15].ToString());
                                        Total_OTHrs = Convert.ToDecimal(dt.Rows[i][4].ToString()) + Convert.ToDecimal(dt.Rows[i][6].ToString()) + Convert.ToDecimal(dt.Rows[i][8].ToString()) + Convert.ToDecimal(dt.Rows[i][10].ToString()) + Convert.ToDecimal(dt.Rows[i][12].ToString()) + Convert.ToDecimal(dt.Rows[i][14].ToString()) + Convert.ToDecimal(dt.Rows[i][16].ToString());
                                    }
                                    else
                                    {

                                        if (dt.Rows[i][3].ToString() == "WH")
                                        {
                                            DayAT_1 = "0";
                                        }
                                        else
                                        {
                                            DayAT_1 = dt.Rows[i][3].ToString();
                                        }
                                        if (dt.Rows[i][4].ToString() == "WH")
                                        {
                                            DayAT_2 = "0";
                                        }
                                        else
                                        {
                                            DayAT_2 = dt.Rows[i][4].ToString();
                                        }
                                        if (dt.Rows[i][5].ToString() == "WH")
                                        {
                                            DayAT_3 = "0";
                                        }
                                        else
                                        {
                                            DayAT_3 = dt.Rows[i][5].ToString();
                                        }
                                        if (dt.Rows[i][6].ToString() == "WH")
                                        {
                                            DayAT_4 = "0";
                                        }
                                        else
                                        {
                                            DayAT_4 = dt.Rows[i][6].ToString();
                                        }
                                        if (dt.Rows[i][7].ToString() == "WH")
                                        {
                                            DayAT_5 = "0";
                                        }
                                        else
                                        {
                                            DayAT_5 = dt.Rows[i][7].ToString();
                                        }
                                        if (dt.Rows[i][8].ToString() == "WH")
                                        {
                                            DayAT_6 = "0";
                                        }
                                        else
                                        {
                                            DayAT_6 = dt.Rows[i][8].ToString();
                                        }
                                        if (dt.Rows[i][9].ToString() == "WH")
                                        {
                                            DayAT_7 = "0";
                                        }
                                        else
                                        {
                                            DayAT_7 = dt.Rows[i][9].ToString();
                                        }

                                        Total_Hrw = Convert.ToDecimal(DayAT_1.ToString()) + Convert.ToDecimal(DayAT_2.ToString()) + Convert.ToDecimal(DayAT_3.ToString()) + Convert.ToDecimal(DayAT_4.ToString()) + Convert.ToDecimal(DayAT_5.ToString()) + Convert.ToDecimal(DayAT_6.ToString()) + Convert.ToDecimal(DayAT_7.ToString());
                                    }

                                    Total_Days = Total_Hrw;


                                    SqlConnection cn = new SqlConnection(constr);
                                    cn.Open();
                                    string qry_dpt1 = "Select DeptCode as DepartmentCd from Department_Mst where DeptName = '" + dt_Emp_Mst.Rows[0]["DepartmentNm"].ToString() + "'";
                                    SqlCommand cmd_dpt1 = new SqlCommand(qry_dpt1, cn);
                                    DepartmentCode = Convert.ToString(cmd_dpt1.ExecuteScalar());
                                    string qry_emp = "Select AT.EmpNo from [" + SessionEpay + "]..AttenanceDetails AT inner JOin [" + SessionEpay + "]..SalaryDetails SD on SD.EmpNo=AT.EmpNo where AT.EmpNo='" + dt_Emp_Mst.Rows[0]["EmpNo"].ToString() + "' and " +
                                                     "AT.Months='" + ddlMonths.Text + "' and AT.FinancialYear='" + ddlfinance.SelectedValue + "' and AT.Ccode='" + SessionCcode + "' and AT.Lcode='" + SessionLcode + "' and SD.Month='" + ddlMonths.Text + "'" +
                                                     " and SD.FinancialYear='" + ddlfinance.SelectedValue + "' and SD.Process_Mode='1' and SD.Ccode='" + SessionCcode + "' and SD.Lcode='" + SessionLcode + "' and convert(datetime,AT.TODate,105) >= convert(datetime,'" + txtFrom.Text + "',105)";
                                    //string qry_emp = "Select EmpNo from AttenanceDetails where EmpNo = '" + dt.Rows[i][2].ToString() + "' and Months = '" + ddlMonths.Text + "' and FinancialYear = '" + ddlfinance.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                                    SqlCommand cmd_verify = new SqlCommand(qry_emp, cn);
                                    Name_Upload = Convert.ToString(cmd_verify.ExecuteScalar());
                                    string qry_emp1 = "Select EmpNo from [" + SessionEpay + "]..AttenanceDetails where EmpNo = '" + dt_Emp_Mst.Rows[0]["EmpNo"].ToString() + "' and Months = '" + ddlMonths.Text + "' and FinancialYear = '" + ddlfinance.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and convert(datetime,TODate,105) >= convert(datetime,'" + txtFrom.Text + "',105)";
                                    SqlCommand cmd_verify1 = new SqlCommand(qry_emp1, cn);
                                    Name_Upload1 = Convert.ToString(cmd_verify1.ExecuteScalar());
                                    if (dt_Emp_Mst.Rows[0]["Wages"].ToString().ToUpper() == "CIVIL".ToUpper().ToString())
                                    {
                                        WagesType = "1";
                                    }
                                    else
                                    {
                                        WagesType = "2";
                                    }
                                    //string qry_Type = "Select Wagestype from Officialprofile where EmpNo = '" + dt_Emp_Mst.Rows[0]["EmpNo"].ToString() + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                                    //SqlCommand cmd_verify2 = new SqlCommand(qry_Type, cn);
                                    //WagesType = Convert.ToString(cmd_verify2.ExecuteScalar());



                                    objatt.department = DepartmentCode;
                                    objatt.EmpNo = dt_Emp_Mst.Rows[0]["EmpNo"].ToString();
                                    objatt.Exist = dt_Emp_Mst.Rows[0]["ExisistingCode"].ToString();
                                    objatt.EmpName = dt_Emp_Mst.Rows[0]["EmpName"].ToString();
                                    if (SessionAdmin == "2")
                                    {
                                        objatt.days = dt.Rows[i][10].ToString();
                                    }
                                    else
                                    {
                                        objatt.days = dt.Rows[i][17].ToString();
                                    }


                                    objatt.Months = ddlMonths.Text;

                                    //if (dt.Rows[i][13].ToString().Trim() == "")
                                    //{
                                    objatt.TotalDays = (Convert.ToInt32(txtdays.Text)).ToString();
                                    //}
                                    //else
                                    //{
                                    //    objatt.TotalDays = dt.Rows[i][13].ToString();
                                    //}

                                    //objatt.TotalDays = (Convert.ToInt32(txtdays.Text)).ToString();


                                    objatt.finance = ddlfinance.SelectedValue;
                                    objatt.Ccode = SessionCcode;
                                    objatt.Lcode = SessionLcode;
                                    objatt.nfh = txtNfh.Text.Trim();
                                    objatt.Absent = "0";

                                    //if (dt.Rows[i][5].ToString().Trim() == "")      //   N/FH
                                    //{
                                    //    TempNFH = "0";
                                    //}
                                    //else
                                    //{
                                    //    TempNFH = dt.Rows[i][5].ToString();
                                    //}

                                    //if (dt.Rows[i][4].ToString().Trim() == "")
                                    //{
                                    //    TempCL = "0";
                                    //}

                                    //else
                                    //{
                                    //    TempCL = dt.Rows[i][4].ToString();
                                    //}
                                    //if (dt.Rows[i][5].ToString().Trim() == "")
                                    //{
                                    //    objatt.Absent = "0";
                                    //}

                                    //else
                                    //{
                                    //    objatt.Absent = dt.Rows[i][5].ToString();
                                    //}
                                    //if (dt.Rows[i][6].ToString().Trim() == "") //OT DAYS
                                    //{
                                    //    TempThreeSide = "0";
                                    //}

                                    //else
                                    //{
                                    //    TempThreeSide = dt.Rows[i][6].ToString();
                                    //}
                                    //if (dt.Rows[i][7].ToString().Trim() == "") { TempFull = "0"; } else { TempFull = dt.Rows[i][7].ToString(); }
                                    //if (dt.Rows[i][8].ToString().Trim() == "") { TempHome = "0"; } else { TempHome = dt.Rows[i][8].ToString(); }
                                    //if (dt.Rows[i][9].ToString().Trim() == "") { objatt.weekoff = "0"; } else { objatt.weekoff = dt.Rows[i][9].ToString(); }
                                    objatt.weekoff = "0";
                                    //if (dt.Rows[i][9].ToString().Trim() == "") { TempOTNew = "0"; } else { TempOTNew = dt.Rows[i][9].ToString(); }

                                    //objatt.HomeDays = dt.Rows[i][7].ToString();
                                    //objatt.Absent = dt.Rows[i][7].ToString();
                                    //objatt.weekoff = dt.Rows[i][8].ToString();
                                    //objatt.half = "0";
                                    //objatt.Full = "0";
                                    //objatt.ThreeSided = "0";



                                    //if (WagesType == "1")
                                    //{
                                    //    objatt.half = dt.Rows[i][10].ToString();
                                    //    objatt.Full = dt.Rows[i][11].ToString();
                                    //    objatt.ThreeSided = "0";
                                    //}
                                    //else if (WagesType == "3")
                                    //{
                                    //    objatt.half = "0";
                                    //    objatt.Full = "0";
                                    //    objatt.ThreeSided = dt.Rows[i][12].ToString();
                                    //}
                                    //else
                                    //{
                                    //    objatt.half = "0";
                                    //    objatt.Full = "0";
                                    //    objatt.ThreeSided = "0";
                                    //}

                                    objatt.workingdays = txtdays.Text;

                                    //if (dt.Rows[i][10].ToString().Trim() == "") { TempWH_Work_Days = "0"; } else { TempWH_Work_Days = dt.Rows[i][10].ToString(); }
                                    //if (dt.Rows[i][11].ToString().Trim() == "") { TempFixed_Days = "0"; } else { TempFixed_Days = dt.Rows[i][11].ToString(); }
                                    //if (dt.Rows[i][12].ToString().Trim() == "") { TempNFH_Work_Days = "0"; } else { TempNFH_Work_Days = dt.Rows[i][12].ToString(); }

                                    //if (dt.Rows[i][14].ToString().Trim() == "") { TempNFH_Work_Manual = "0"; } else { TempNFH_Work_Manual = dt.Rows[i][14].ToString(); }
                                    //if (dt.Rows[i][15].ToString().Trim() == "") { TempNFH_Work_Station = "0"; } else { TempNFH_Work_Station = dt.Rows[i][15].ToString(); }
                                    //if (dt.Rows[i][16].ToString().Trim() == "") { TempNFH_Count_Days = "0"; } else { TempNFH_Count_Days = dt.Rows[i][16].ToString(); }
                                    //if (dt.Rows[i][17].ToString().Trim() == "") { TempLBH_Count_Days = "0"; } else { TempLBH_Count_Days = dt.Rows[i][17].ToString(); }

                                    cn.Close();

                                    string TempDate = "getdate()";
                                    if (Name_Upload == dt.Rows[i][2].ToString())
                                    {
                                        //string Del = "Delete from AttenanceDetails where EmpNo = '" + dt.Rows[i][2].ToString() + "' and Months = '" + ddlMonths.Text + "' and FinancialYear = '" + ddlfinance.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                                        //cn.Open();
                                        //SqlCommand cmd_del = new SqlCommand(Del, cn);
                                        ////cn.Open();
                                        //cmd_del.ExecuteNonQuery();
                                        //cn.Close();
                                    }
                                    else if (Name_Upload1 == dt_Emp_Mst.Rows[0]["EmpNo"].ToString())
                                    {
                                        string Del = "Delete from [" + SessionEpay + "]..AttenanceDetails where EmpNo = '" + dt_Emp_Mst.Rows[0]["EmpNo"].ToString() + "' and Months = '" + ddlMonths.Text + "' and FinancialYear = '" + ddlfinance.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and convert(Datetime,FromDate,105) = Convert(Datetime,'" + txtFrom.Text.Trim() + "', 105)";
                                        cn.Open();
                                        SqlCommand cmd_del = new SqlCommand(Del, cn);
                                        cmd_del.ExecuteNonQuery();
                                        cn.Close();

                                        DateTime MyDate = DateTime.ParseExact(txtFrom.Text, "dd-MM-yyyy", null);
                                        DateTime MyDate1 = DateTime.ParseExact(txtTo.Text, "dd-MM-yyyy", null);

                                        if (SessionAdmin == "2")
                                        {
                                            Query = "";
                                            Query = Query + "Insert into [" + SessionEpay + "]..AttenanceDetails (DeptName,EmpNo,ExistingCode,Days,Months,FinancialYear,CreatedDate,";
                                            Query = Query + "TotalDays,Ccode,Lcode,WorkingDays,FromDate,ToDate,Modeval,";
                                            Query = Query + "OTHoursNew,Fixed_Work_Days,";
                                            Query = Query + "Day1_WH,Day2_WH,Day3_WH,Day4_WH,Day5_WH,Day6_WH,Day7_WH,ToTal_Hrs,Total_Days) values (";
                                            Query = Query + "'" + DepartmentCode + "','" + dt_Emp_Mst.Rows[0]["EmpNo"].ToString() + "','" + dt_Emp_Mst.Rows[0]["ExisistingCode"].ToString() + "',";
                                            Query = Query + "'" + dt.Rows[i][10].ToString() + "','" + ddlMonths.Text + "','" + ddlfinance.SelectedValue + "',convert(datetime," + TempDate + ",105),";
                                            Query = Query + "'" + txtdays.Text.ToString() + "','" + SessionCcode + "','" + SessionLcode + "','" + txtdays.Text + "',";
                                            Query = Query + "convert(datetime,'" + txtFrom.Text + "',105),convert(datetime,'" + txtTo.Text + "',105),'" + WagesType + "',";
                                            Query = Query + "'" + Total_OTHrs.ToString() + "','" + txtdays.Text + "','" + dt.Rows[i][3].ToString() + "','" + dt.Rows[i][4].ToString() + "','" + dt.Rows[i][5].ToString() + "','" + dt.Rows[i][6].ToString() + "','" + dt.Rows[i][7].ToString() + "','" + dt.Rows[i][8].ToString() + "',";
                                            Query = Query + "'" + dt.Rows[i][9].ToString() + "','" + Total_Hrw.ToString() + "','" + Total_Days + "')";
                                            objdata.RptEmployeeMultipleDetails(Query);
                                        }
                                        else
                                        {

                                            Query = "";
                                            Query = Query + "Insert into [" + SessionEpay + "]..AttenanceDetails (DeptName,EmpNo,ExistingCode,Days,Months,FinancialYear,CreatedDate,";
                                            Query = Query + "TotalDays,Ccode,Lcode,WorkingDays,FromDate,ToDate,Modeval,";
                                            Query = Query + "OTHoursNew,Fixed_Work_Days,";
                                            Query = Query + "Day1_WH,Day1_OT,Day2_WH,Day2_OT,Day3_WH,Day3_OT,Day4_WH,Day4_OT,Day5_WH,Day5_OT,Day6_WH,Day6_OT,Day7_WH,Day7_OT,ToTal_Hrs,ToTal_OT,Total_Days,MID_Present) values (";
                                            Query = Query + "'" + DepartmentCode + "','" + dt_Emp_Mst.Rows[0]["EmpNo"].ToString() + "','" + dt_Emp_Mst.Rows[0]["ExisistingCode"].ToString() + "',";
                                            Query = Query + "'" + dt.Rows[i][17].ToString() + "','" + ddlMonths.Text + "','" + ddlfinance.SelectedValue + "',convert(datetime," + TempDate + ",105),";
                                            Query = Query + "'" + txtdays.Text.ToString() + "','" + SessionCcode + "','" + SessionLcode + "','" + txtdays.Text + "',";
                                            Query = Query + "convert(datetime,'" + txtFrom.Text + "',105),convert(datetime,'" + txtTo.Text + "',105),'" + WagesType + "',";
                                            Query = Query + "'" + Total_OTHrs.ToString() + "','" + txtdays.Text + "','" + dt.Rows[i][3].ToString() + "','" + dt.Rows[i][4].ToString() + "','" + dt.Rows[i][5].ToString() + "','" + dt.Rows[i][6].ToString() + "','" + dt.Rows[i][7].ToString() + "','" + dt.Rows[i][8].ToString() + "',";
                                            Query = Query + "'" + dt.Rows[i][9].ToString() + "','" + dt.Rows[i][10].ToString() + "','" + dt.Rows[i][11].ToString() + "','" + dt.Rows[i][12].ToString() + "','" + dt.Rows[i][13].ToString() + "','" + dt.Rows[i][14].ToString() + "',";
                                            Query = Query + "'" + dt.Rows[i][15].ToString() + "','" + dt.Rows[i][16].ToString() + "','" + Total_Hrw.ToString() + "','" + Total_OT.ToString() + "','" + Total_Days + "','" + dt.Rows[i][19].ToString() + "')";
                                            objdata.RptEmployeeMultipleDetails(Query);
                                        }
                                        //objdata.Attenance_insert(objatt, MyDate, MyDate1, rbsalary.SelectedValue);
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Uploaded Successfully...');", true);
                                    }
                                    else
                                    {
                                        DateTime MyDate = DateTime.ParseExact(txtFrom.Text, "dd-MM-yyyy", null);
                                        DateTime MyDate1 = DateTime.ParseExact(txtTo.Text, "dd-MM-yyyy", null);

                                        if (dt_Emp_Mst.Rows[0]["EmpNo"].ToString() == "60531")
                                        {
                                            string s = "0";
                                        }

                                        if (SessionAdmin == "2")
                                        {
                                            Query = "";
                                            Query = Query + "Insert into [" + SessionEpay + "]..AttenanceDetails (DeptName,EmpNo,ExistingCode,Days,Months,FinancialYear,CreatedDate,";
                                            Query = Query + "TotalDays,Ccode,Lcode,WorkingDays,FromDate,ToDate,Modeval,";
                                            Query = Query + "OTHoursNew,Fixed_Work_Days,";
                                            Query = Query + "Day1_WH,Day2_WH,Day3_WH,Day4_WH,Day5_WH,Day6_WH,Day7_WH,ToTal_Hrs,Total_Days) values (";
                                            Query = Query + "'" + DepartmentCode + "','" + dt_Emp_Mst.Rows[0]["EmpNo"].ToString() + "','" + dt_Emp_Mst.Rows[0]["ExisistingCode"].ToString() + "',";
                                            Query = Query + "'" + dt.Rows[i][10].ToString() + "','" + ddlMonths.Text + "','" + ddlfinance.SelectedValue + "',convert(datetime," + TempDate + ",105),";
                                            Query = Query + "'" + txtdays.Text.ToString() + "','" + SessionCcode + "','" + SessionLcode + "','" + txtdays.Text + "',";
                                            Query = Query + "convert(datetime,'" + txtFrom.Text + "',105),convert(datetime,'" + txtTo.Text + "',105),'" + WagesType + "',";
                                            Query = Query + "'" + Total_OTHrs.ToString() + "','" + txtdays.Text + "','" + dt.Rows[i][3].ToString() + "','" + dt.Rows[i][4].ToString() + "','" + dt.Rows[i][5].ToString() + "','" + dt.Rows[i][6].ToString() + "','" + dt.Rows[i][7].ToString() + "','" + dt.Rows[i][8].ToString() + "',";
                                            Query = Query + "'" + dt.Rows[i][9].ToString() + "','" + Total_Hrw.ToString() + "','" + Total_Days + "')";
                                            objdata.RptEmployeeMultipleDetails(Query);
                                        }
                                        else
                                        {

                                            Query = "";
                                            Query = Query + "Insert into [" + SessionEpay + "]..AttenanceDetails (DeptName,EmpNo,ExistingCode,Days,Months,FinancialYear,CreatedDate,";
                                            Query = Query + "TotalDays,Ccode,Lcode,WorkingDays,FromDate,ToDate,Modeval,";
                                            Query = Query + "OTHoursNew,Fixed_Work_Days,";
                                            Query = Query + "Day1_WH,Day1_OT,Day2_WH,Day2_OT,Day3_WH,Day3_OT,Day4_WH,Day4_OT,Day5_WH,Day5_OT,Day6_WH,Day6_OT,Day7_WH,Day7_OT,ToTal_Hrs,ToTal_OT,Total_Days,MID_Present) values (";
                                            Query = Query + "'" + DepartmentCode + "','" + dt_Emp_Mst.Rows[0]["EmpNo"].ToString() + "','" + dt_Emp_Mst.Rows[0]["ExisistingCode"].ToString() + "',";
                                            Query = Query + "'" + dt.Rows[i][17].ToString() + "','" + ddlMonths.Text + "','" + ddlfinance.SelectedValue + "',convert(datetime," + TempDate + ",105),";
                                            Query = Query + "'" + txtdays.Text.ToString() + "','" + SessionCcode + "','" + SessionLcode + "','" + txtdays.Text + "',";
                                            Query = Query + "convert(datetime,'" + txtFrom.Text + "',105),convert(datetime,'" + txtTo.Text + "',105),'" + WagesType + "',";
                                            Query = Query + "'" + Total_OTHrs.ToString() + "','" + txtdays.Text + "','" + dt.Rows[i][3].ToString() + "','" + dt.Rows[i][4].ToString() + "','" + dt.Rows[i][5].ToString() + "','" + dt.Rows[i][6].ToString() + "','" + dt.Rows[i][7].ToString() + "','" + dt.Rows[i][8].ToString() + "',";
                                            Query = Query + "'" + dt.Rows[i][9].ToString() + "','" + dt.Rows[i][10].ToString() + "','" + dt.Rows[i][11].ToString() + "','" + dt.Rows[i][12].ToString() + "','" + dt.Rows[i][13].ToString() + "','" + dt.Rows[i][14].ToString() + "',";
                                            Query = Query + "'" + dt.Rows[i][15].ToString() + "','" + dt.Rows[i][16].ToString() + "','" + Total_Hrw.ToString() + "','" + Total_OT.ToString() + "','" + Total_Days + "','" + dt.Rows[i][19].ToString() + "')";
                                            objdata.RptEmployeeMultipleDetails(Query);
                                        }
                                        //objdata.Attenance_insert(objatt, MyDate, MyDate1, rbsalary.SelectedValue);
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Uploaded Successfully...');", true);
                                    }
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Uploaded Successfully...');", true);
                                    sSourceConnection.Close();
                                }
                            }
                            else if (ddlEmployeeType.SelectedItem.Text == "WEEKLY-HINDI".ToUpper())
                            {

                            }



                            else
                            {
                                //For monthly Wages
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    //Get Employee Master Details
                                    DataTable dt_Emp_Mst = new DataTable();
                                    string MachineID = dt.Rows[i][0].ToString();
                                    string Query_Emp_mst = "";
                                    Query_Emp_mst = "Select ED.Wages, ED.FirstName as EmpName,ED.MachineID as MachineNo,ED.EmpNo,ED.ExistingCode as ExisistingCode,MD.DeptName as DepartmentNm, '2' as Wagestype,";
                                    Query_Emp_mst = Query_Emp_mst + " ED.MachineID as BiometricID from Employee_Mst ED inner Join Department_Mst MD on MD.DeptCode=ED.DeptCode";
                                    Query_Emp_mst = Query_Emp_mst + " where ED.CompCode='" + SessionCcode + "'";
                                    Query_Emp_mst = Query_Emp_mst + " and ED.LocCode='" + SessionLcode + "' and ED.IsActive='Yes'";
                                    Query_Emp_mst = Query_Emp_mst + " and ED.MachineID='" + MachineID + "'";
                                    dt_Emp_Mst = objdata.RptEmployeeMultipleDetails(Query_Emp_mst);

                                    SqlConnection cn = new SqlConnection(constr);
                                    cn.Open();
                                    string qry_dpt1 = "Select DeptCode as DepartmentCd from Department_Mst where DeptName = '" + dt_Emp_Mst.Rows[0]["DepartmentNm"].ToString() + "'";
                                    SqlCommand cmd_dpt1 = new SqlCommand(qry_dpt1, cn);
                                    DepartmentCode = Convert.ToString(cmd_dpt1.ExecuteScalar());
                                    string qry_emp = "Select AT.EmpNo from [" + SessionEpay + "]..AttenanceDetails AT inner JOin [" + SessionEpay + "]..SalaryDetails SD on SD.EmpNo=AT.EmpNo where AT.EmpNo='" + dt_Emp_Mst.Rows[0]["EmpNo"].ToString() + "' and " +
                                                     "AT.Months='" + ddlMonths.Text + "' and AT.FinancialYear='" + ddlfinance.SelectedValue + "' and AT.Ccode='" + SessionCcode + "' and AT.Lcode='" + SessionLcode + "' and SD.Month='" + ddlMonths.Text + "'" +
                                                     " and SD.FinancialYear='" + ddlfinance.SelectedValue + "' and SD.Process_Mode='1' and SD.Ccode='" + SessionCcode + "' and SD.Lcode='" + SessionLcode + "' and convert(datetime,AT.TODate,105) >= convert(datetime,'" + txtFrom.Text + "',105)";
                                    //string qry_emp = "Select EmpNo from AttenanceDetails where EmpNo = '" + dt.Rows[i][2].ToString() + "' and Months = '" + ddlMonths.Text + "' and FinancialYear = '" + ddlfinance.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                                    SqlCommand cmd_verify = new SqlCommand(qry_emp, cn);
                                    Name_Upload = Convert.ToString(cmd_verify.ExecuteScalar());
                                    string qry_emp1 = "Select EmpNo from [" + SessionEpay + "]..AttenanceDetails where EmpNo = '" + dt_Emp_Mst.Rows[0]["EmpNo"].ToString() + "' and Months = '" + ddlMonths.Text + "' and FinancialYear = '" + ddlfinance.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and convert(datetime,TODate,105) >= convert(datetime,'" + txtFrom.Text + "',105)";
                                    SqlCommand cmd_verify1 = new SqlCommand(qry_emp1, cn);
                                    Name_Upload1 = Convert.ToString(cmd_verify1.ExecuteScalar());
                                    if (dt_Emp_Mst.Rows[0]["Wages"].ToString().ToUpper() == "CIVIL".ToUpper().ToString())
                                    {
                                        WagesType = "1";
                                    }
                                    else
                                    {
                                        WagesType = "2";
                                    }
                                    //string qry_Type = "Select Wagestype from Officialprofile where EmpNo = '" + dt_Emp_Mst.Rows[0]["EmpNo"].ToString() + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                                    //SqlCommand cmd_verify2 = new SqlCommand(qry_Type, cn);
                                    //WagesType = Convert.ToString(cmd_verify2.ExecuteScalar());



                                    objatt.department = DepartmentCode;
                                    objatt.EmpNo = dt_Emp_Mst.Rows[0]["EmpNo"].ToString();
                                    objatt.Exist = dt_Emp_Mst.Rows[0]["ExisistingCode"].ToString();
                                    objatt.EmpName = dt_Emp_Mst.Rows[0]["EmpName"].ToString();
                                    objatt.days = dt.Rows[i][3].ToString();

                                    objatt.Months = ddlMonths.Text;

                                    if (dt.Rows[i][13].ToString().Trim() == "")
                                    {
                                        objatt.TotalDays = (Convert.ToInt32(txtdays.Text)).ToString();
                                    }
                                    else
                                    {
                                        objatt.TotalDays = dt.Rows[i][13].ToString();
                                    }

                                    //objatt.TotalDays = (Convert.ToInt32(txtdays.Text)).ToString();


                                    objatt.finance = ddlfinance.SelectedValue;
                                    objatt.Ccode = SessionCcode;
                                    objatt.Lcode = SessionLcode;
                                    objatt.nfh = txtNfh.Text.Trim();
                                    objatt.Absent = "0";

                                    if (dt.Rows[i][5].ToString().Trim() == "")      //   N/FH
                                    {
                                        TempNFH = "0";
                                    }
                                    else
                                    {
                                        TempNFH = dt.Rows[i][5].ToString();
                                    }

                                    if (dt.Rows[i][4].ToString().Trim() == "")
                                    {
                                        TempCL = "0";
                                    }

                                    else
                                    {
                                        TempCL = dt.Rows[i][4].ToString();
                                    }
                                    //if (dt.Rows[i][5].ToString().Trim() == "")
                                    //{
                                    //    objatt.Absent = "0";
                                    //}

                                    //else
                                    //{
                                    //    objatt.Absent = dt.Rows[i][5].ToString();
                                    //}
                                    if (dt.Rows[i][6].ToString().Trim() == "") //OT DAYS
                                    {
                                        TempThreeSide = "0";
                                    }

                                    else
                                    {
                                        TempThreeSide = dt.Rows[i][6].ToString();
                                    }
                                    if (dt.Rows[i][7].ToString().Trim() == "") { TempFull = "0"; } else { TempFull = dt.Rows[i][7].ToString(); }
                                    if (dt.Rows[i][8].ToString().Trim() == "") { TempHome = "0"; } else { TempHome = dt.Rows[i][8].ToString(); }
                                    //if (dt.Rows[i][9].ToString().Trim() == "") { objatt.weekoff = "0"; } else { objatt.weekoff = dt.Rows[i][9].ToString(); }
                                    objatt.weekoff = "0";
                                    if (dt.Rows[i][9].ToString().Trim() == "") { TempOTNew = "0"; } else { TempOTNew = dt.Rows[i][9].ToString(); }

                                    //objatt.HomeDays = dt.Rows[i][7].ToString();
                                    //objatt.Absent = dt.Rows[i][7].ToString();
                                    //objatt.weekoff = dt.Rows[i][8].ToString();
                                    //objatt.half = "0";
                                    //objatt.Full = "0";
                                    //objatt.ThreeSided = "0";



                                    //if (WagesType == "1")
                                    //{
                                    //    objatt.half = dt.Rows[i][10].ToString();
                                    //    objatt.Full = dt.Rows[i][11].ToString();
                                    //    objatt.ThreeSided = "0";
                                    //}
                                    //else if (WagesType == "3")
                                    //{
                                    //    objatt.half = "0";
                                    //    objatt.Full = "0";
                                    //    objatt.ThreeSided = dt.Rows[i][12].ToString();
                                    //}
                                    //else
                                    //{
                                    //    objatt.half = "0";
                                    //    objatt.Full = "0";
                                    //    objatt.ThreeSided = "0";
                                    //}

                                    objatt.workingdays = txtdays.Text;

                                    if (dt.Rows[i][10].ToString().Trim() == "") { TempWH_Work_Days = "0"; } else { TempWH_Work_Days = dt.Rows[i][10].ToString(); }
                                    if (dt.Rows[i][11].ToString().Trim() == "") { TempFixed_Days = "0"; } else { TempFixed_Days = dt.Rows[i][11].ToString(); }
                                    if (dt.Rows[i][12].ToString().Trim() == "") { TempNFH_Work_Days = "0"; } else { TempNFH_Work_Days = dt.Rows[i][12].ToString(); }

                                    if (dt.Rows[i][14].ToString().Trim() == "") { TempNFH_Work_Manual = "0"; } else { TempNFH_Work_Manual = dt.Rows[i][14].ToString(); }
                                    if (dt.Rows[i][15].ToString().Trim() == "") { TempNFH_Work_Station = "0"; } else { TempNFH_Work_Station = dt.Rows[i][15].ToString(); }
                                    if (dt.Rows[i][16].ToString().Trim() == "") { TempNFH_Count_Days = "0"; } else { TempNFH_Count_Days = dt.Rows[i][16].ToString(); }
                                    if (dt.Rows[i][17].ToString().Trim() == "") { TempLBH_Count_Days = "0"; } else { TempLBH_Count_Days = dt.Rows[i][17].ToString(); }

                                    cn.Close();

                                    string TempDate = "getdate()";
                                    if (Name_Upload == dt.Rows[i][2].ToString())
                                    {
                                        //string Del = "Delete from AttenanceDetails where EmpNo = '" + dt.Rows[i][2].ToString() + "' and Months = '" + ddlMonths.Text + "' and FinancialYear = '" + ddlfinance.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                                        //cn.Open();
                                        //SqlCommand cmd_del = new SqlCommand(Del, cn);
                                        ////cn.Open();
                                        //cmd_del.ExecuteNonQuery();
                                        //cn.Close();
                                    }
                                    else if (Name_Upload1 == dt_Emp_Mst.Rows[0]["EmpNo"].ToString())
                                    {
                                        string Del = "Delete from [" + SessionEpay + "]..AttenanceDetails where EmpNo = '" + dt_Emp_Mst.Rows[0]["EmpNo"].ToString() + "' and Months = '" + ddlMonths.Text + "' and FinancialYear = '" + ddlfinance.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and convert(Datetime,FromDate,105) = Convert(Datetime,'" + txtFrom.Text.Trim() + "', 105)";
                                        cn.Open();
                                        SqlCommand cmd_del = new SqlCommand(Del, cn);
                                        cmd_del.ExecuteNonQuery();
                                        cn.Close();
                                        DateTime MyDate = DateTime.ParseExact(txtFrom.Text, "dd-MM-yyyy", null);
                                        DateTime MyDate1 = DateTime.ParseExact(txtTo.Text, "dd-MM-yyyy", null);

                                        Query = "";
                                        Query = Query + "Insert into [" + SessionEpay + "]..AttenanceDetails (DeptName,EmpNo,ExistingCode,Days,Months,FinancialYear,CreatedDate,";
                                        Query = Query + "TotalDays,Ccode,Lcode,NFh,WorkingDays,CL,AbsentDays,weekoff,FromDate,ToDate,Modeval,home,";
                                        Query = Query + "halfNight,FullNight,ThreeSided,OTHoursNew,Fixed_Work_Days,WH_Work_Days,NFH_Work_Days,";
                                        Query = Query + "NFH_Work_Days_Incentive,NFH_Work_Days_Statutory,AEH,LBH) values (";
                                        Query = Query + "'" + DepartmentCode + "','" + dt_Emp_Mst.Rows[0]["EmpNo"].ToString() + "','" + dt_Emp_Mst.Rows[0]["ExisistingCode"].ToString() + "',";
                                        Query = Query + "'" + dt.Rows[i][3].ToString() + "','" + ddlMonths.Text + "','" + ddlfinance.SelectedValue + "',convert(datetime," + TempDate + ",105),";
                                        Query = Query + "'" + dt.Rows[i][13].ToString() + "','" + SessionCcode + "','" + SessionLcode + "','" + TempNFH + "','" + txtdays.Text + "','" + TempCL + "',";
                                        Query = Query + "'0','0',convert(datetime,'" + txtFrom.Text + "',105),convert(datetime,'" + txtTo.Text + "',105),'" + WagesType + "','" + TempHome + "',";
                                        Query = Query + "'0','" + TempFull + "','" + TempThreeSide + "','" + TempOTNew + "','" + TempFixed_Days + "','" + TempWH_Work_Days + "','" + TempNFH_Work_Days + "',";
                                        Query = Query + "'" + TempNFH_Work_Manual + "','" + TempNFH_Work_Station + "','" + TempNFH_Count_Days + "','" + TempLBH_Count_Days + "')";
                                        objdata.RptEmployeeMultipleDetails(Query);
                                        //objdata.Attenance_insert(objatt, MyDate, MyDate1, rbsalary.SelectedValue);
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Uploaded Successfully...');", true);
                                    }
                                    else
                                    {
                                        DateTime MyDate = DateTime.ParseExact(txtFrom.Text, "dd-MM-yyyy", null);
                                        DateTime MyDate1 = DateTime.ParseExact(txtTo.Text, "dd-MM-yyyy", null);
                                        Query = "";
                                        Query = Query + "Insert into [" + SessionEpay + "]..AttenanceDetails (DeptName,EmpNo,ExistingCode,Days,Months,FinancialYear,CreatedDate,";
                                        Query = Query + "TotalDays,Ccode,Lcode,NFh,WorkingDays,CL,AbsentDays,weekoff,FromDate,ToDate,Modeval,home,";
                                        Query = Query + "halfNight,FullNight,ThreeSided,OTHoursNew,Fixed_Work_Days,WH_Work_Days,NFH_Work_Days,";
                                        Query = Query + "NFH_Work_Days_Incentive,NFH_Work_Days_Statutory,AEH,LBH) values (";
                                        Query = Query + "'" + DepartmentCode + "','" + dt_Emp_Mst.Rows[0]["EmpNo"].ToString() + "','" + dt_Emp_Mst.Rows[0]["ExisistingCode"].ToString() + "',";
                                        Query = Query + "'" + dt.Rows[i][3].ToString() + "','" + ddlMonths.Text + "','" + ddlfinance.SelectedValue + "',convert(datetime," + TempDate + ",105),";
                                        Query = Query + "'" + dt.Rows[i][13].ToString() + "','" + SessionCcode + "','" + SessionLcode + "','" + TempNFH + "','" + txtdays.Text + "','" + TempCL + "',";
                                        Query = Query + "'0','0',convert(datetime,'" + txtFrom.Text + "',105),convert(datetime,'" + txtTo.Text + "',105),'" + WagesType + "','" + TempHome + "',";
                                        Query = Query + "'0','" + TempFull + "','" + TempThreeSide + "','" + TempOTNew + "','" + TempFixed_Days + "','" + TempWH_Work_Days + "','" + TempNFH_Work_Days + "',";
                                        Query = Query + "'" + TempNFH_Work_Manual + "','" + TempNFH_Work_Station + "','" + TempNFH_Count_Days + "','" + TempLBH_Count_Days + "')";
                                        objdata.RptEmployeeMultipleDetails(Query);

                                        //objdata.Attenance_insert(objatt, MyDate, MyDate1, rbsalary.SelectedValue);
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Uploaded Successfully...');", true);
                                    }
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Uploaded Successfully...');", true);
                                    sSourceConnection.Close();
                                }

                                //System.Windows.Forms.MessageBox.Show("Uploaded SuccessFully", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);

                            }
                        }
                        if (ErrFlag == true)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Your File Not Upload...');", true);
                            //System.Windows.Forms.MessageBox.Show("Your File Not Uploaded", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Upload Correct File Format...');", true);
            //System.Windows.Forms.MessageBox.Show("Please Upload Correct File Format", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        }
    }
    protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        string StaffLabour = "";

        if (ddlCategory.SelectedValue == "1")
        {
            StaffLabour = "1";
        }
        else if (ddlCategory.SelectedValue == "2")
        {
            StaffLabour = "2";
        }
        else
        {
            StaffLabour = "0";
        }


        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlEmployeeType.Items.Clear();
        if (SessionAdmin == "2")
        {
            query = "Select * from MstEmployeeType where IF_='1' and EmpCategory='" + StaffLabour + "'";
        }
        else
        {
            query = "select EmpType from MstEmployeeType where EmpCategory='" + StaffLabour + "'";
        }

        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlEmployeeType.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpType"] = "-Select-";
        dr["EmpType"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlEmployeeType.DataTextField = "EmpType";
        ddlEmployeeType.DataValueField = "EmpType";
        ddlEmployeeType.DataBind();
    }
    protected void btnDownload_Click(object sender, EventArgs e)
    {
        try
        {
            if (ddlCategory.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category');", true);
            }
            else if (ddlCategory.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category');", true);
            }

            else if (ddlEmployeeType.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Type');", true);
            }
            else if (ddlEmployeeType.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Type');", true);
            }
            else
            {
                string staff_lab = "";
                string Employee_Type = "";
                if (ddlCategory.SelectedValue == "1")
                {
                    staff_lab = "S";
                }
                else if (ddlCategory.SelectedValue == "2")
                {
                    staff_lab = "L";
                }
                Employee_Type = ddlEmployeeType.SelectedValue.ToString();
                DataTable dt = new DataTable();

                Query = "";
                Query = Query + "select EM.EmpNo,(EM.FirstName) as EmpName,(EM.MachineID) as MachineNo,(EM.ExistingCode) as ExisistingCode,(DM.DeptName) as DepartmentNm from Employee_Mst EM inner join Department_Mst DM on EM.DeptCode=DM.DeptCode ";
                Query = Query + "where EM.LocCode='UNIT I' and EM.CompCode='GTPL' and EM.IsActive='Yes' ";
                Query = Query + "and EM.Wages='" + ddlEmployeeType.SelectedItem.Text.ToString() + "'";
                Query = Query + " Order by EM.ExistingCode Asc";
                dt = objdata.RptEmployeeMultipleDetails(Query);
                gvEmp.DataSource = dt;
                gvEmp.DataBind();

                //foreach (GridViewRow gvsalDed in gvEmp.Rows)
                //{
                //    Label lbl_EmpNo = (Label)gvsalDed.FindControl("lblEmpNo");
                //    Label lbl_Advance = (Label)gvsalDed.FindControl("lblAdvance");
                //    string qry_ot = "";
                //    DataTable Advance_Mst_DT = new DataTable();
                //    DataTable Advance_Pay_DT = new DataTable();
                //    string Advance_Amt = "0.00";
                //    string Balance_Amt = "0";
                //    string Monthly_Paid_Amt = "0";
                //    string Advance_Paid_Amt = "0";
                //    if (ddlEmployeeType.SelectedItem.Text != "CIVIL")
                //    {
                //        qry_ot = "Select * from [" + SessionEpay + "]..AdvancePayment where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                //        qry_ot = qry_ot + " And EmpNo='" + lbl_EmpNo.Text.Trim() + "' And Completed='N'";
                //        Advance_Mst_DT = objdata.RptEmployeeMultipleDetails(qry_ot);
                //        if (Advance_Mst_DT.Rows.Count != 0)
                //        {
                //            //get Balance Amt
                //            string constr = ConfigurationManager.AppSettings["ConnectionString"];
                //            SqlConnection cn = new SqlConnection(constr);
                //            cn.Open();
                //            qry_ot = "Select isnull(sum(Amount),0) as Paid_Amt from [" + SessionEpay + "]..Advancerepayment where EmpNo='" + lbl_EmpNo.Text.Trim() + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' And AdvanceId='" + Advance_Mst_DT.Rows[0]["ID"] + "'";
                //            SqlCommand cmd_wages = new SqlCommand(qry_ot, cn);
                //            Advance_Paid_Amt = (cmd_wages.ExecuteScalar()).ToString();
                //            Balance_Amt = (Convert.ToDecimal(Advance_Mst_DT.Rows[0]["Amount"].ToString()) - Convert.ToDecimal(Advance_Paid_Amt)).ToString();
                //            Monthly_Paid_Amt = Advance_Mst_DT.Rows[0]["MonthlyDeduction"].ToString();
                //            if (Convert.ToDecimal(Monthly_Paid_Amt.ToString()) >= Convert.ToDecimal(Balance_Amt.ToString()))
                //            {
                //                Advance_Amt = (Math.Round(Convert.ToDecimal(Balance_Amt), 2, MidpointRounding.AwayFromZero)).ToString();
                //            }
                //            else
                //            {
                //                Advance_Amt = (Math.Round(Convert.ToDecimal(Monthly_Paid_Amt), 2, MidpointRounding.AwayFromZero)).ToString();
                //            }
                //            cn.Close();
                //        }
                //        else
                //        {
                //            Advance_Amt = "0.00";
                //        }


                //    }
                //    else
                //    {
                //        Advance_Amt = "0.00";
                //    }

                //    lbl_Advance.Text = Advance_Amt;



                //}

                if (dt.Rows.Count > 0)
                {
                    string attachment = "attachment;filename=Attendance.xls";
                    Response.ClearContent();
                    Response.AddHeader("content-disposition", attachment);
                    Response.ContentType = "application/ms-excel";

                    StringWriter stw = new StringWriter();
                    HtmlTextWriter htextw = new HtmlTextWriter(stw);
                    gvEmp.RenderControl(htextw);
                    //gvDownload.RenderControl(htextw);
                    //Response.Write("Contract Details");
                    Response.Write(stw.ToString());
                    Response.End();
                    Response.Clear();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Downloaded Successfully');", true);

                }


            }
        }
        catch (Exception)
        {

            throw;
        }
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
}
