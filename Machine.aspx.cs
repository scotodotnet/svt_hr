﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Text;
using System.IO;

public partial class Machine : System.Web.UI.Page
{

    BALDataAccess objdata = new BALDataAccess();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string[] Time_Minus_Value_Check;
    public zkemkeeper.CZKEMClass axCZKEM1 = new zkemkeeper.CZKEMClass();
    private int iMachineNumber = 1;
    
    
    string LocCode;
    
    string SSQL = "";
    
    string SessionUserType;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionUserName = Session["Usernmdisplay"].ToString();
            SessionUserID = Session["UserId"].ToString();
            SessionRights = Session["Rights"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
        }
        sample();
        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Download Clear";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("DownloadClear"));
            //li.Attributes.Add("class", "droplink active open");
            if (SessionUserType != "1")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert('Your are Not Authorized');", true);
                Response.Redirect("AttendDashBoard.aspx");
            }
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string Serial = "";
        string serial_ENCODE = "";
        string Table = "Mst_Machine";
        if (txtSerial.Text != "")
        {
            Serial = txtSerial.Text;
            serial_ENCODE = UTF8Encryption(Serial);

            if (Serial != serial_ENCODE)
            {
                SSQL = "";
                SSQL = "Insert Into "+UTF8Encryption(Table).ToString().Split('=').FirstOrDefault() +" values('"+serial_ENCODE+"')";
                objdata.RptEmployeeMultipleDetails(SSQL);
                txtSerial.Text = string.Empty;
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert('Data is Not Valid!');", true);
                txtSerial.Focus();
            }
        }else
        {
            ScriptManager.RegisterStartupScript(this,this.GetType(),"Alert","alert('Please Enter the Serial Code!');",true);
            txtSerial.Focus();
        }
    }
    private static string UTF8Encryption(string serial)
    {
        string strmsg = string.Empty;
        byte[] encode = new byte[serial.Length];
        encode = Encoding.UTF8.GetBytes(serial);
        strmsg = Convert.ToBase64String(encode);
        return strmsg;
    }
    public void sample()
    {
        string samp = "Mst_Machine",s;
        s=UTF8Encryption(samp);
        s = UTF8Decryption(s);
    }
    public string UTF8Encryption_NEW(string mvarPlanText)
    {
        string cipherText = "";
        try
        {
            string passPhrase = "Altius";
            string saltValue = "info@altius.co.in";
            string hashAlgorithm = "SHA1";
            string initVector = "@1B2c3D4e5F6g7H8";
            int passwordIterations = 2;
            int keySize = 256;
            byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
            byte[] saltValueBytes = Encoding.ASCII.GetBytes(saltValue);
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(mvarPlanText);
            PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, saltValueBytes, hashAlgorithm, passwordIterations);
            byte[] keyBytes = password.GetBytes(keySize / 8);
            RijndaelManaged symmetricKey = new RijndaelManaged();
            symmetricKey.Mode = CipherMode.CBC;
            ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes);
            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
            cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
            cryptoStream.FlushFinalBlock();
            byte[] cipherTextBytes = memoryStream.ToArray();
            memoryStream.Close();
            cryptoStream.Close();
            cipherText = Convert.ToBase64String(cipherTextBytes);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }

        return cipherText;
    }
    private static string UTF8Decryption(string encryptpwd)
    {
        string decryptpwd = string.Empty;
        UTF8Encoding encodepwd = new UTF8Encoding();
        Decoder Decode = encodepwd.GetDecoder();
        byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
        int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
        char[] decoded_char = new char[charCount];
        Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
        decryptpwd = new String(decoded_char);
        return decryptpwd;
    }
}