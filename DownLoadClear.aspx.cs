﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using Org.BouncyCastle.Utilities;
using System.IO;


public partial class DownLoadClear : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string[] Time_Minus_Value_Check;
    public zkemkeeper.CZKEMClass axCZKEM1 = new zkemkeeper.CZKEMClass();
    private int iMachineNumber = 1;
    private bool bIsConnected = false;
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;
    string LocCode;
    bool Errflag;
    bool Berrflag;
    bool Connect = false;
    bool Check_Download_Clear_Error = false;
    DataTable mLocalDS = new DataTable();
    string SSQL = "";
    bool isPresent = false;
    string SessionUserType;
    DateTime Shift_Start_Time, Emp_Time_IN;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionUserName = Session["Usernmdisplay"].ToString();
            SessionUserID = Session["UserId"].ToString();
            SessionRights = Session["Rights"].ToString();
            SessionUserType = Session["Isadmin"].ToString();

            con = new SqlConnection(constr);

        }

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Download Clear";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("DownloadClear"));
            //li.Attributes.Add("class", "droplink active open");
            if (SessionUserType == "1")
            {
                Div_Wages.Visible = true;
            }
            else
            {
                Div_Wages.Visible = false;
            }

            IPAddress();
            lblDwnCmpltd.Text = "";
            Load_WagesType();
        }
        //Load_EmpNo();
    }

    private void Load_WagesType()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlWages.Items.Clear();
        query = "Select * from MstEmployeeType";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlWages.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpType"] = "-Select-";
        dr["EmpType"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlWages.DataTextField = "EmpType";
        ddlWages.DataValueField = "EmpType";
        ddlWages.DataBind();
    }

    protected void btnDocumnet_Click(object sender, EventArgs e)
    {
        DataTable dt1 = new DataTable();


        SSQL = "SELECT *FROM Employee_Mst where Adhar_No <>'' or Voter_ID <>'' or DL_No <>'' or Passport_No <> '' or";
        SSQL = SSQL + " Ration_Card <>'' or PanCard<>'' or Smart_Card <>'' or Other_Card_No <>'' ";
        SSQL = SSQL + " and LocCode='" + SessionLcode + "' and CompCode='" + SessionCcode + "' ";
        dt1 = objdata.RptEmployeeMultipleDetails(SSQL);

        for (int i = 0; i < dt1.Rows.Count; i++)
        {

            if (dt1.Rows[i]["Adhar_No"].ToString() != "")
            {

                SSQL = "insert into Employee_Doc_Mst (CompCode,LocCode,EmpNo,ExistingCode,DocType,DocNo,Doc_Img,Created_By,Created_Date) ";
                SSQL = SSQL + "Values ('" + SessionCcode + "','" + SessionLcode + "','" + dt1.Rows[i]["MachineID"] + "','" + dt1.Rows[i]["ExistingCode"] + "',";
                SSQL = SSQL + " 'Adhar Card','" + dt1.Rows[i]["Adhar_No"].ToString() + "','','" + SessionUserName + "',GetDate())";
                objdata.RptEmployeeMultipleDetails(SSQL);
            }
            if (dt1.Rows[i]["Voter_ID"].ToString() != "")
            {

                SSQL = "insert into Employee_Doc_Mst (CompCode,LocCode,EmpNo,ExistingCode,DocType,DocNo,Doc_Img,Created_By,Created_Date) ";
                SSQL = SSQL + "Values ('" + SessionCcode + "','" + SessionLcode + "','" + dt1.Rows[i]["MachineID"] + "','" + dt1.Rows[i]["ExistingCode"] + "',";
                SSQL = SSQL + " 'Voter Card','" + dt1.Rows[i]["Voter_ID"].ToString() + "','','" + SessionUserName + "',GetDate())";
                objdata.RptEmployeeMultipleDetails(SSQL);
            }

            if (dt1.Rows[i]["DL_No"].ToString() != "")
            {

                SSQL = "insert into Employee_Doc_Mst (CompCode,LocCode,EmpNo,ExistingCode,DocType,DocNo,Doc_Img,Created_By,Created_Date) ";
                SSQL = SSQL + "Values ('" + SessionCcode + "','" + SessionLcode + "','" + dt1.Rows[i]["MachineID"] + "','" + dt1.Rows[i]["ExistingCode"] + "',";
                SSQL = SSQL + " 'Driving Licence','" + dt1.Rows[i]["DL_No"].ToString() + "','','" + SessionUserName + "',GetDate())";
                objdata.RptEmployeeMultipleDetails(SSQL);
            }

            if (dt1.Rows[i]["Passport_No"].ToString() != "")
            {

                SSQL = "insert into Employee_Doc_Mst (CompCode,LocCode,EmpNo,ExistingCode,DocType,DocNo,Doc_Img,Created_By,Created_Date) ";
                SSQL = SSQL + "Values ('" + SessionCcode + "','" + SessionLcode + "','" + dt1.Rows[i]["MachineID"] + "','" + dt1.Rows[i]["ExistingCode"] + "',";
                SSQL = SSQL + " 'Passport','" + dt1.Rows[i]["Passport_No"].ToString() + "','','" + SessionUserName + "',GetDate())";
                objdata.RptEmployeeMultipleDetails(SSQL);
            }

            if (dt1.Rows[i]["Ration_Card"].ToString() != "")
            {

                SSQL = "insert into Employee_Doc_Mst (CompCode,LocCode,EmpNo,ExistingCode,DocType,DocNo,Doc_Img,Created_By,Created_Date) ";
                SSQL = SSQL + "Values ('" + SessionCcode + "','" + SessionLcode + "','" + dt1.Rows[i]["MachineID"] + "','" + dt1.Rows[i]["ExistingCode"] + "',";
                SSQL = SSQL + " 'Ration Card','" + dt1.Rows[i]["Ration_Card"].ToString() + "','','" + SessionUserName + "',GetDate())";
                objdata.RptEmployeeMultipleDetails(SSQL);
            }

            if (dt1.Rows[i]["PanCard"].ToString() != "")
            {

                SSQL = "insert into Employee_Doc_Mst (CompCode,LocCode,EmpNo,ExistingCode,DocType,DocNo,Doc_Img,Created_By,Created_Date) ";
                SSQL = SSQL + "Values ('" + SessionCcode + "','" + SessionLcode + "','" + dt1.Rows[i]["MachineID"] + "','" + dt1.Rows[i]["ExistingCode"] + "',";
                SSQL = SSQL + " 'Pan Card','" + dt1.Rows[i]["PanCard"].ToString() + "','','" + SessionUserName + "',GetDate())";
                objdata.RptEmployeeMultipleDetails(SSQL);
            }
            if (dt1.Rows[i]["Smart_Card"].ToString() != "")
            {

                SSQL = "insert into Employee_Doc_Mst (CompCode,LocCode,EmpNo,ExistingCode,DocType,DocNo,Doc_Img,Created_By,Created_Date) ";
                SSQL = SSQL + "Values ('" + SessionCcode + "','" + SessionLcode + "','" + dt1.Rows[i]["MachineID"] + "','" + dt1.Rows[i]["ExistingCode"] + "',";
                SSQL = SSQL + " 'Smart Card','" + dt1.Rows[i]["Smart_Card"].ToString() + "','','" + SessionUserName + "',GetDate())";
                objdata.RptEmployeeMultipleDetails(SSQL);
            }

            if (dt1.Rows[i]["Other_Card_No"].ToString() != "")
            {

                SSQL = "insert into Employee_Doc_Mst (CompCode,LocCode,EmpNo,ExistingCode,DocType,DocNo,Doc_Img,Created_By,Created_Date) ";
                SSQL = SSQL + "Values ('" + SessionCcode + "','" + SessionLcode + "','" + dt1.Rows[i]["MachineID"] + "','" + dt1.Rows[i]["ExistingCode"] + "',";
                SSQL = SSQL + " 'Others','" + dt1.Rows[i]["Other_Card_No"].ToString() + "','','" + SessionUserName + "',GetDate())";
                objdata.RptEmployeeMultipleDetails(SSQL);
            }

        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Data Saved Successfully');", true);

    }

    public void IPAddress()
    {
        DataTable dtdsupp = new DataTable();
        string query = "";

        ddlIPAddress.Items.Clear();
        query = "Select (IPAddress + ' | ' + IPMode) as IPAddress from IPAddress_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlIPAddress.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["IPAddress"] = "-Select-";
        dr["IPAddress"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlIPAddress.DataTextField = "IPAddress";
        ddlIPAddress.DataValueField = "IPAddress";
        ddlIPAddress.DataBind();
    }

    public void Bin()
    {
        if (ddlIPAddress.SelectedValue.Trim() == "")
        {
            // MessageBox.Show("IP and Port cannot be null", "Error");
            return;
        }
        int idwErrorCode = 0;


        int port = 4370;


        if (Errflag == true)
        {
            lblDwnCmpltd.Text = "DOWNLOAD PROCESSING.......";

            Errflag = true;
        }

        string[] IP_Add_Spilit = ddlIPAddress.Text.Split(new string[] { " | " }, StringSplitOptions.None);
        string Final_IP_Address = IP_Add_Spilit[0].ToString();
        bIsConnected = axCZKEM1.Connect_Net(Final_IP_Address, Convert.ToInt32(port));
        if (bIsConnected == true)
        {

            iMachineNumber = 1;//In fact,when you are using the tcp/ip communication,this parameter will be ignored,that is any integer will all right.Here we use 1.
            axCZKEM1.RegEvent(iMachineNumber, 65535);//Here you can register the realtime events that you want to be triggered(the parameters 65535 means registering all)
        }
        else
        {
            axCZKEM1.GetLastError(ref idwErrorCode);
            // MessageBox.Show("Unable to connect the device,ErrorCode=" + idwErrorCode.ToString(), "Error");

            //System.Threading.Thread.Sleep(3000);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Unable to connect the device');", true);
            Connect = true;


        }
        //Cursor = Cursors.Default;
    }

    private double IIf(bool p, object p_2, int p_3)
    {
        throw new NotImplementedException();
    }

    public string Left_Val(string Value, int Length)
    {

        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }

    public string Right_Val(string Value, int Length)
    {

        int i = 0;
        i = 0;
        if (Value.Length >= Length)
        {
            //i = Value.Length - Length
            return Value.Substring(Value.Length - Length, Length);
        }
        else
        {
            return Value;
        }
    }

    private string Strings(int p)
    {
        throw new NotImplementedException();
    }

    protected void btndownload_Click(object sender, EventArgs e)
    {
        try
        {
            Errflag = true;
            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ProgressBarShow();", true);
            if (bIsConnected == false)
            {
                //MessageBox.Show("Please connect the device first", "Error");
                //return
            }
            string sdwEnrollNumber = "";
            int idwVerifyMode = 0;
            int idwInOutMode = 0;
            int idwYear = 0;
            int idwMonth = 0;
            int idwDay = 0;
            int idwHour = 0;
            int idwMinute = 0;
            int idwSecond = 0;
            int idwWorkcode = 0;
            int idwErrorCode = 0;
            //bool download_completed;
            Bin();
            string[] IP_Add_Spilit = ddlIPAddress.Text.Split(new string[] { " | " }, StringSplitOptions.None);
            string Final_IP_Address = IP_Add_Spilit[0].ToString();

            axCZKEM1.EnableDevice(iMachineNumber, false);//disable the device
            if (axCZKEM1.ReadGeneralLogData(iMachineNumber))//read all the attendance records to the memory
            {
                while (axCZKEM1.SSR_GetGeneralLogData(iMachineNumber, out sdwEnrollNumber, out idwVerifyMode,
                           out idwInOutMode, out idwYear, out idwMonth, out idwDay, out idwHour, out idwMinute, out idwSecond, ref idwWorkcode))//get records from the memory
                {
                    if (sdwEnrollNumber == "Mjc0Mg==")
                    {
                        sdwEnrollNumber = "Mjc0Mg==";
                    }
                    string Mode = "";
                    lblDwnCmpltd.Text = "Download Processing....";
                    DataTable IP_Da = new DataTable();
                    string IP_check = "select IPMode from IPAddress_Mst where IpAddress='" + Final_IP_Address + "' and  CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";

                    IP_Da = objdata.RptEmployeeMultipleDetails(IP_check);
                    if (IP_Da.Rows.Count == 0)
                    {
                    }
                    else
                    {
                        Mode = IP_Da.Rows[0]["IPMode"].ToString();
                    }
                    DateTime date = new DateTime();
                    date = Convert.ToDateTime(idwYear.ToString() + "-" + idwMonth.ToString() + "-" + idwDay.ToString() + " " + idwHour.ToString() + ":" + idwMinute.ToString() + ":" + idwSecond.ToString());
                    string Query = "";
                    string Chck_Date = idwYear.ToString() + "/" + idwMonth.ToString() + "/" + idwDay.ToString();

                    //Encry Code
                    //string strmsg = string.Empty;
                    //byte[] encode = new byte[sdwEnrollNumber.Length];
                    //encode = Encoding.UTF8.GetBytes(sdwEnrollNumber);
                    //sdwEnrollNumber = Convert.ToBase64String(encode);
                    sdwEnrollNumber = UTF8Encryption(sdwEnrollNumber);

                    if (Mode == "IN")
                    {
                        Query = "delete from LogTime_IN where MachineID='" + sdwEnrollNumber + "' and TimeIN='" + date.ToString("yyyy/MM/dd HH:mm:ss") + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                        SqlCommand cmd_del = new SqlCommand(Query, con);
                        con.Open();
                        cmd_del.ExecuteNonQuery();
                        con.Close();

                        Query = "insert into LogTime_IN(CompCode,LocCode,IPAddress,MachineID,TimeIN)values('" + SessionCcode + "','" + SessionLcode + "','" + Final_IP_Address + "','" + sdwEnrollNumber + "','" + date.ToString("yyyy/MM/dd HH:mm:ss") + "')";
                        SqlCommand cmd_upd1 = new SqlCommand(Query, con);
                        con.Open();
                        cmd_upd1.ExecuteNonQuery();
                        con.Close();
                    }

                    if (Mode == "OUT")
                    {
                        Query = "delete from LogTime_OUT where MachineID='" + sdwEnrollNumber + "' and TimeOUT='" + date.ToString("yyyy/MM/dd HH:mm:ss") + "' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "'";
                        SqlCommand cmd_del = new SqlCommand(Query, con);
                        con.Open();
                        cmd_del.ExecuteNonQuery();
                        con.Close();

                        Query = "insert into LogTime_OUT(CompCode,LocCode,IPAddress,MachineID,TimeOUT)values('" + SessionCcode + "','" + SessionLcode + "','" + Final_IP_Address + "','" + sdwEnrollNumber + "','" + date.ToString("yyyy/MM/dd HH:mm:ss") + "')";
                        SqlCommand cmd_upd1 = new SqlCommand(Query, con);
                        con.Open();
                        cmd_upd1.ExecuteNonQuery();
                        con.Close();
                    }
                    if (Mode == "LUNCH")
                    {
                        Query = "delete from LogTime_Lunch where MachineID='" + sdwEnrollNumber + "' and TimeIN='" + date.ToString("yyyy/MM/dd HH:mm:ss") + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                        SqlCommand cmd_del = new SqlCommand(Query, con);
                        con.Open();
                        cmd_del.ExecuteNonQuery();
                        con.Close();

                        Query = "insert into LogTime_Lunch(CompCode,LocCode,IPAddress,MachineID,TimeIN)values('" + SessionCcode + "','" + SessionLcode + "','" + Final_IP_Address + "','" + sdwEnrollNumber + "','" + date.ToString("yyyy/MM/dd HH:mm:ss") + "')";
                        SqlCommand cmd_upd1 = new SqlCommand(Query, con);
                        con.Open();
                        cmd_upd1.ExecuteNonQuery();
                        con.Close();
                    }
                    if (Mode == "LUNCH IN")
                    {
                        Query = "delete from LogTimeLunch_IN where MachineID='" + sdwEnrollNumber + "' and TimeIN='" + date.ToString("yyyy/MM/dd HH:mm:ss") + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                        SqlCommand cmd_del = new SqlCommand(Query, con);
                        con.Open();
                        cmd_del.ExecuteNonQuery();
                        con.Close();

                        Query = "insert into LogTimeLunch_IN(CompCode,LocCode,IPAddress,MachineID,TimeIN)values('" + SessionCcode + "','" + SessionLcode + "','" + Final_IP_Address + "','" + sdwEnrollNumber + "','" + date.ToString("yyyy/MM/dd HH:mm:ss") + "')";
                        SqlCommand cmd_upd1 = new SqlCommand(Query, con);
                        con.Open();
                        cmd_upd1.ExecuteNonQuery();
                        con.Close();
                    }
                    if (Mode == "LUNCH OUT")
                    {
                        Query = "delete from LogTimeLunch_OUT where MachineID='" + sdwEnrollNumber + "' and TimeOUT='" + date.ToString("yyyy/MM/dd HH:mm:ss") + "' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "'";
                        SqlCommand cmd_del = new SqlCommand(Query, con);
                        con.Open();
                        cmd_del.ExecuteNonQuery();
                        con.Close();

                        Query = "insert into LogTimeLunch_OUT(CompCode,LocCode,IPAddress,MachineID,TimeOUT)values('" + SessionCcode + "','" + SessionLcode + "','" + Final_IP_Address + "','" + sdwEnrollNumber + "','" + date.ToString("yyyy/MM/dd HH:mm:ss") + "')";
                        SqlCommand cmd_upd1 = new SqlCommand(Query, con);
                        con.Open();
                        cmd_upd1.ExecuteNonQuery();
                        con.Close();
                    }

                    if (Mode == "HOSTEL LUNCH")
                    {
                        Query = "delete from LogTimeHostel_Lunch where MachineID='" + sdwEnrollNumber + "' and TimeIN='" + date.ToString("yyyy/MM/dd HH:mm:ss") + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                        SqlCommand cmd_del = new SqlCommand(Query, con);
                        con.Open();
                        cmd_del.ExecuteNonQuery();
                        con.Close();

                        string Datestr = date.ToString("dd/MM/yyyy");
                        string BreakForm = Datestr + " 07:30";
                        string BreakTo = Datestr + " 10:00";
                        string LunchFrom = Datestr + " 11:30";
                        string LunchTo = Datestr + " 15:00";
                        string DinnerFrom = Datestr + " 18:30";
                        string DinnerTo = Datestr + " 23:00";
                        DateTime BFo = Convert.ToDateTime(BreakForm);
                        DateTime BTo = Convert.ToDateTime(BreakTo);
                        DateTime LFo = Convert.ToDateTime(LunchFrom);
                        DateTime LTo = Convert.ToDateTime(LunchTo);
                        DateTime DFo = Convert.ToDateTime(DinnerFrom);
                        DateTime DTo = Convert.ToDateTime(DinnerTo);
                        if (BFo <= date && BTo >= date)
                        {
                            Query = "insert into LogTimeHostel_Lunch(CompCode,LocCode,IPAddress,MachineID,TimeIN,Status)";
                            Query = Query + "values('" + SessionCcode + "','" + SessionLcode + "','" + Final_IP_Address + "','" + sdwEnrollNumber + "','" + date.ToString("yyyy/MM/dd HH:mm:ss") + "','BreakFast')";
                            SqlCommand cmd_upd1 = new SqlCommand(Query, con);
                            con.Open();
                            cmd_upd1.ExecuteNonQuery();
                            con.Close();
                        }
                        else if (LFo <= date && LTo >= date)
                        {
                            Query = "insert into LogTimeHostel_Lunch(CompCode,LocCode,IPAddress,MachineID,TimeIN,Status)";
                            Query = Query + "values('" + SessionCcode + "','" + SessionLcode + "','" + Final_IP_Address + "','" + sdwEnrollNumber + "','" + date.ToString("yyyy/MM/dd HH:mm:ss") + "','Lunch')";
                            SqlCommand cmd_upd1 = new SqlCommand(Query, con);
                            con.Open();
                            cmd_upd1.ExecuteNonQuery();
                            con.Close();
                        }
                        else if (DFo <= date && DTo >= date)
                        {
                            Query = "insert into LogTimeHostel_Lunch(CompCode,LocCode,IPAddress,MachineID,TimeIN,Status)";
                            Query = Query + "values('" + SessionCcode + "','" + SessionLcode + "','" + Final_IP_Address + "','" + sdwEnrollNumber + "','" + date.ToString("yyyy/MM/dd HH:mm:ss") + "','Dinner')";
                            SqlCommand cmd_upd1 = new SqlCommand(Query, con);
                            con.Open();
                            cmd_upd1.ExecuteNonQuery();
                            con.Close();
                        }
                        else
                        {
                            Query = "insert into LogTimeHostel_Lunch(CompCode,LocCode,IPAddress,MachineID,TimeIN,Status)";
                            Query = Query + "values('" + SessionCcode + "','" + SessionLcode + "','" + Final_IP_Address + "','" + sdwEnrollNumber + "','" + date.ToString("yyyy/MM/dd HH:mm:ss") + "','NotMatch')";
                            SqlCommand cmd_upd1 = new SqlCommand(Query, con);
                            con.Open();
                            cmd_upd1.ExecuteNonQuery();
                            con.Close();
                        }

                    }

                    if (Mode == "SALARY")
                    {
                        Query = "delete from LogTime_SALARY where MachineID='" + sdwEnrollNumber + "' and Payout='" + date.ToString("yyyy/MM/dd HH:mm:ss") + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                        SqlCommand cmd_del = new SqlCommand(Query, con);
                        con.Open();
                        cmd_del.ExecuteNonQuery();
                        con.Close();

                        Query = "insert into LogTime_SALARY(CompCode,LocCode,IPAddress,MachineID,Payout)values('" + SessionCcode + "','" + SessionLcode + "','" + Final_IP_Address + "','" + sdwEnrollNumber + "','" + date.ToString("yyyy/MM/dd HH:mm:ss") + "')";
                        SqlCommand cmd_upd1 = new SqlCommand(Query, con);
                        con.Open();
                        cmd_upd1.ExecuteNonQuery();
                        con.Close();
                    }

                    if (Mode == "BONUS")
                    {
                        Query = "delete from LogTime_BONUS where MachineID='" + sdwEnrollNumber + "' and Bonusout='" + date.ToString("yyyy/MM/dd HH:mm:ss") + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                        SqlCommand cmd_del = new SqlCommand(Query, con);
                        con.Open();
                        cmd_del.ExecuteNonQuery();
                        con.Close();

                        Query = "insert into LogTime_BONUS(CompCode,LocCode,IPAddress,MachineID,Bonusout)values('" + SessionCcode + "','" + SessionLcode + "','" + Final_IP_Address + "','" + sdwEnrollNumber + "','" + date.ToString("yyyy/MM/dd HH:mm:ss") + "')";
                        SqlCommand cmd_upd1 = new SqlCommand(Query, con);
                        con.Open();
                        cmd_upd1.ExecuteNonQuery();
                        con.Close();
                    }

                    if (Mode == "WORKINGHOUSE")
                    {
                        Query = "delete from WorkingHouse_IN where MachineID='" + sdwEnrollNumber + "' and TimeIN='" + date.ToString("yyyy/MM/dd HH:mm:ss") + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                        SqlCommand cmd_del = new SqlCommand(Query, con);
                        con.Open();
                        cmd_del.ExecuteNonQuery();
                        con.Close();

                        Query = "insert into WorkingHouse_IN(CompCode,LocCode,IPAddress,MachineID,TimeIN)values('" + SessionCcode + "','" + SessionLcode + "','" + Final_IP_Address + "','" + sdwEnrollNumber + "','" + date.ToString("yyyy/MM/dd HH:mm:ss") + "')";
                        SqlCommand cmd_upd1 = new SqlCommand(Query, con);
                        con.Open();
                        cmd_upd1.ExecuteNonQuery();
                        con.Close();
                    }

                    // StoreDays(sdwEnrollNumber, date);

                }
            }
            else
            {
                Check_Download_Clear_Error = true;
                // Cursor = Cursors.Default;
                axCZKEM1.GetLastError(ref idwErrorCode);
                if (idwErrorCode != 0)
                {
                    //MessageBox.Show("Reading data from terminal failed,ErrorCode: " + idwErrorCode.ToString(), "Error");
                }
                else
                {
                    //MessageBox.Show("No data from terminal returns!", "Error");
                }
            }
            axCZKEM1.EnableDevice(iMachineNumber, true);//enable the device
            if (Errflag == true && Connect == false)
            {

                lblDwnCmpltd.Text = "DOWNLOAD COMPLETED....";
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ProgressBarHide();", true);
                ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "ProgressBarHide();", true);
                ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "SaveMsgAlert('DOWNLOAD COMPLETED....');", true);

                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('DOWNLOAD COMPLETED....');", true);
                Errflag = true;
            }
            else
            {
                Check_Download_Clear_Error = true;
                lblDwnCmpltd.Text = "Machine Can't Ping";
                // ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ProgressBarHide();", true);
                ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "ProgressBarHide();", true);
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Machine Can't Ping....');", true);
                Errflag = true;
            }

        }

        catch (Exception ex)
        {
            Check_Download_Clear_Error = true;
            throw new Exception("Error: " + ex.Message);
        }
    }

    private static string UTF8Encryption_OLD(string password)
    {
        string strmsg = string.Empty;
        byte[] encode = new byte[password.Length];
        encode = Encoding.UTF8.GetBytes(password);
        strmsg = Convert.ToBase64String(encode);
        return strmsg;
    }

    private static string UTF8Decryption(string encryptpwd)
    {
        string decryptpwd = string.Empty;
        UTF8Encoding encodepwd = new UTF8Encoding();
        Decoder Decode = encodepwd.GetDecoder();
        byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
        int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
        char[] decoded_char = new char[charCount];
        Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
        decryptpwd = new String(decoded_char);
        return decryptpwd;
    }

    protected void btndownloadClear_Click(object sender, EventArgs e)
    {
        //if (TxtCompcode.Text == "")
        //{
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Give Company Name');", true);
        //    Errflag = true;
        //}
        //if (ddlLocationCode.SelectedItem.Text == "")
        //{
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Give Location Name');", true);
        //    Errflag = true;
        //}
        if (ddlIPAddress.SelectedItem.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Give IP Address');", true);
            Errflag = true;
        }
        Check_Download_Clear_Error = false;
        btndownload_Click(sender, e);

        if (Check_Download_Clear_Error == false)
        {

            TFT_Machine_Attn_Log_Clear();
            ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "ProgressBarHide();", true);
            ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "SaveMsgAlert('DOWNLOAD CLEARED SUCCESSFULLY....');", true);
        }

    }

    public void TFT_Machine_Attn_Log_Clear()
    {
        bool bConn = false;
        int idwErrorCode = 0;
        int iMachineNumber = 0;
        int port = 4370;
        //the serial number of the device.After connecting the device ,this value will be changed.
        string[] IP_Add_Spilit = ddlIPAddress.Text.Split(new string[] { " | " }, StringSplitOptions.None);
        string Final_IP_Address = IP_Add_Spilit[0].ToString();
        bConn = axCZKEM1.Connect_Net(Final_IP_Address, Convert.ToInt32(port));

        //mdiMain.AxSB100PC1.SetIPAddress(Trim(iStr3(0)), CLng("5005"), CLng("0"))
        if (bConn == false)
        {
            //MessageBox.Show("Not connected.", "Altius", MessageBoxButtons.OK, MessageBoxIcon.Stop)
            return;
        }


        iMachineNumber = 1;
        axCZKEM1.EnableDevice(iMachineNumber, false);

        //disable the device
        if (axCZKEM1.ClearGLog(iMachineNumber) == true)
        {
            axCZKEM1.RefreshData(iMachineNumber);
            //the data in the device should be refreshed
            //MsgBox("All att Logs have been cleared from teiminal!", MsgBoxStyle.Information, "Success")
        }
        else
        {
            axCZKEM1.GetLastError(ref idwErrorCode);
            //MsgBox("Operation failed,ErrorCode=" & idwErrorCode, MsgBoxStyle.Exclamation, "Error")
        }

        //enable the device
        axCZKEM1.EnableDevice(iMachineNumber, true);
    }

    protected void btnConversion_Click(object sender, EventArgs e)
    {

        string Attn_Date_Str1 = txtFromDate.Text;
        string Attn_Date_Str2 = txtToDate.Text;
        bool ErrFlag_1 = false;
        //Delete Exisiting Date Record Start
        DateTime Date1 = Convert.ToDateTime(Attn_Date_Str1);
        DateTime Date2 = Convert.ToDateTime(Attn_Date_Str2);

        int daycount = (int)((Date2 - Date1).TotalDays);
        int daysAdded = daycount + 1;
        //if (txtTokenNumber.Text == "")
        //{
        //    if (daycount != 0)
        //    {
        //        if (SessionUserType != "1")
        //        {
        //            ErrFlag_1 = true;
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('You do not have rights to convert multiple date..');", true);
        //            ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "ProgressBarHide();", true);
        //        }
        //    }
        //}
        if (!ErrFlag_1)
        {
            for (int ik = 0; (ik <= (daysAdded - 1)); ik++)
            {
                string Date_Value_Str = "";
                Date_Value_Str = Convert.ToDateTime(Attn_Date_Str1).AddDays(ik).ToShortDateString();
                SSQL = "Delete from LogTime_Days where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And Attn_Date_Str='" + Date_Value_Str + "'";
                if (txtTokenNumber.Text != "")
                {
                    SSQL = SSQL + " And ExistingCode='" + txtTokenNumber.Text + "'";
                }
                if (ddlWages.SelectedValue != "-Select-")
                {
                    SSQL = SSQL + " And Wages='" + ddlWages.SelectedItem.Text + "'";
                }
                objdata.RptEmployeeMultipleDetails(SSQL);
            }
            //Delete Exisiting Date Record End

            DataTable Emp_DS = new DataTable();
            long Emp_Machine;
            string Search_Machine_ID = "";
            if (txtFromDate.Text != "" && txtToDate.Text != "")
            {
                SSQL = "Select * from Employee_MST where Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                SSQL = SSQL + " And (IsActive='Yes' or CONVERT(DATETIME,DOR,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(Attn_Date_Str1).ToString("dd/MM/yyyy") + "',103))";
                if (txtTokenNumber.Text != "")
                {
                    SSQL = SSQL + " And ExistingCode='" + txtTokenNumber.Text + "'";
                }
                if (ddlWages.SelectedValue != "-Select-")
                {
                    SSQL = SSQL + " And Wages='" + ddlWages.SelectedItem.Text + "'";
                }
                // And IsActive='Yes'";
                Emp_DS = objdata.RptEmployeeMultipleDetails(SSQL);

                if (Emp_DS.Rows.Count == 0)
                {
                    //Pending Employee Save to Days
                    SSQL = "Select * from Employee_Mst_New_Emp where Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    SSQL = SSQL + " And (IsActive='Yes' or CONVERT(DATETIME,DOR,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(Attn_Date_Str1).ToString("dd/MM/yyyy") + "',103))";
                    if (txtTokenNumber.Text != "")
                    {
                        SSQL = SSQL + " And ExistingCode='" + txtTokenNumber.Text + "'";
                    }
                    if (ddlWages.SelectedValue != "-Select-")
                    {
                        SSQL = SSQL + " And Wages='" + ddlWages.SelectedItem.Text + "'";
                    }
                    //And IsActive='Yes'";
                    Emp_DS = objdata.RptEmployeeMultipleDetails(SSQL);

                    if (Emp_DS.Rows.Count == 0)
                    {
                    }
                    else
                    {
                        for (int i = 0; i < Emp_DS.Rows.Count; i++)
                        {
                            Search_Machine_ID = Emp_DS.Rows[i]["MachineID"].ToString();
                            Get_Working_Days_Save_DB_Pending_Employee(Search_Machine_ID, Attn_Date_Str1, Attn_Date_Str2);
                        }
                        //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('CONVERSION COMPLETED....');", true);
                        //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ProgressBarHide();", true);
                        //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('CONVERSION COMPLETED..');", true);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('CONVERSION COMPLETED..');", true);

                        //ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "ProgressBarHide();", true);
                        //ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "SaveMsgAlert('CONVERSION COMPLETED..');", true);
                    }
                }
                else
                {
                    //Approved Employee Save to Days
                    for (int i = 0; i < Emp_DS.Rows.Count; i++)
                    {
                        Search_Machine_ID = Emp_DS.Rows[i]["MachineID"].ToString();
                        Get_Working_Days_Save_DB(Search_Machine_ID, Attn_Date_Str1, Attn_Date_Str2);

                    }

                    //Pending Employee Save to Days
                    SSQL = "Select * from Employee_Mst_New_Emp where Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    SSQL = SSQL + " And (IsActive='Yes' or CONVERT(DATETIME,DOR,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(Attn_Date_Str1).ToString("dd/MM/yyyy") + "',103))";
                    if (txtTokenNumber.Text != "")
                    {
                        SSQL = SSQL + " And ExistingCode='" + txtTokenNumber.Text + "'";
                    }
                    if (ddlWages.SelectedValue != "-Select-")
                    {
                        SSQL = SSQL + " And Wages='" + ddlWages.SelectedItem.Text + "'";
                    }
                    //And IsActive='Yes'";
                    Emp_DS = objdata.RptEmployeeMultipleDetails(SSQL);

                    if (Emp_DS.Rows.Count == 0)
                    {
                    }
                    else
                    {
                        for (int i = 0; i < Emp_DS.Rows.Count; i++)
                        {
                            Search_Machine_ID = Emp_DS.Rows[i]["MachineID"].ToString();
                            Get_Working_Days_Save_DB_Pending_Employee(Search_Machine_ID, Attn_Date_Str1, Attn_Date_Str2);
                        }
                    }
                    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('CONVERSION COMPLETED....');", true);
                    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ProgressBarHide();", true);
                    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('CONVERSION COMPLETED..');", true);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('CONVERSION COMPLETED..');", true);
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "ProgressBarHide();", true);
                    //ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "SaveMsgAlert('CONVERSION COMPLETED..');", true);

                }
            }
            else
            {
                //ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "ProgressBarHide();", true);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter From Date and To Date');", true);
            }
        }
        //else
        //{
        //    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "ProgressBarHide();", true);
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('CONVERSION COMPLETED..');", true);
        //}

    }

    public void Get_Working_Days_Save_DB(string Get_Machine_ID, string Get_Punching_Date1, string Get_Punching_Date2)
    {
        DateTime Date1 = Convert.ToDateTime(Get_Punching_Date1);
        DateTime Date2 = Convert.ToDateTime(Get_Punching_Date2);

        int daycount = (int)((Date2 - Date1).TotalDays);
        int daysAdded = daycount + 1;
        //Late IN Variable
        double Late_time_Check_dbl = 0;
        double Deduction_Hrs = 0;
        string Deduction_Min = "0";
        string Late_Total_Time_get = "";
        string Deduction_Total_Hrs = "00:00";

        int intI;
        int intK;
        int intCol;
        string Fin_Year = "";
        string Months_Full_Str = "";
        string Date_Col_Str = "";
        int Month_Int = 1;
        string Spin_Machine_ID_Str = "";

        string[] Time_Minus_Value_Check;

        DataTable mLocalDS = new DataTable();

        DataTable mLocalDS1 = new DataTable();

        //Attn_Flex Col Add Var
        string[] Att_Date_Check;
        string Att_Already_Date_Check;
        string Att_Year_Check = "";
        int Month_Name_Change;
        string halfPresent = "0";
        int EPay_Total_Days_Get = 0;
        intCol = 4;
        Month_Name_Change = 1;


        EPay_Total_Days_Get = 1;

        intI = 2;
        intK = 1;

        bool ErrFlag = false;


        DataTable Emp_DS = new DataTable();

        SSQL = "Select * from Employee_Mst where CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " And MachineID='" + Get_Machine_ID + "'";
        Emp_DS = objdata.RptEmployeeMultipleDetails(SSQL);

        if (Emp_DS.Rows.Count == 0)
        {
            SSQL = "Select * from Employee_Mst_New_Emp where CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And MachineID='" + Get_Machine_ID + "'";
            Emp_DS = objdata.RptEmployeeMultipleDetails(SSQL);

            if (Emp_DS.Rows.Count == 0)
            {
            }
        }

        if (Emp_DS.Rows.Count > 0)
        {
            ErrFlag = false;
        }
        else
        {
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            for (int intRow = 0; intRow < Emp_DS.Rows.Count; intRow++)
            {
                intK = 1;

                string Emp_Total_Work_Time_1 = "00:00";
                //Get Employee Week OF DAY
                DataTable DS_WH = new DataTable();
                string Emp_WH_Day;
                string DOJ_Date_Str;
                string MachineID1 = Emp_DS.Rows[intRow]["MachineID"].ToString();

                string Query = " Select * from Employee_Mst where MachineID='" + MachineID1 + "' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' ";
                DS_WH = objdata.RptEmployeeMultipleDetails(Query);
                if (DS_WH.Rows.Count != 0)
                {
                    Emp_WH_Day = DS_WH.Rows[0]["WeekOff"].ToString();
                    DOJ_Date_Str = DS_WH.Rows[0]["DOJ"].ToString();
                }
                else
                {
                    Emp_WH_Day = "";
                    DOJ_Date_Str = "";
                }

                int colIndex = intK;
                string DayofWeekName = "";
                decimal Weekoff_Count = 0;
                isPresent = false;
                bool isWHPresent = false;
                decimal Present_Count = 0;
                decimal Present_WH_Count = 0;
                int Appsent_Count = 0;
                decimal Final_Count = 0;
                decimal Total_Days_Count = 0;
                string Already_Date_Check;
                int Days_Insert_RowVal = 0;
                decimal FullNight_Shift_Count = 0;
                int NFH_Days_Count = 0;
                decimal NFH_Days_Present_Count = 0;
                Already_Date_Check = "";
                string Year_Check = "";
                string Time_IN_Str = "";
                string Time_Out_Str = "";
                int g = 1;

                double OT_Time;

                OT_Time = 0;

                string INTime = "";
                string OUTTime = "";

                for (intCol = 0; (intCol <= (daysAdded - 1)); intCol++)
                {

                    Emp_Total_Work_Time_1 = "00:00";
                    string Machine_ID_Str = "";
                    string OT_Week_OFF_Machine_No;
                    string Date_Value_Str = "";
                    string Total_Time_get = "";
                    string Final_OT_Work_Time_1 = "";
                    //DataSet mLocalDS = new DataSet();

                    //DataTable mLocalDS = new DataTable();

                    decimal Month_WH_Count = 0;
                    int j = 0;
                    double time_Check_dbl = 0;
                    string Date_Value_Str1 = "";
                    string Employee_Shift_Name_DB = "No Shift";
                    isPresent = false;
                    isWHPresent = false;

                    //Shift Change Check Variable Declaration Start
                    DateTime InTime_Check = new DateTime();
                    DateTime InToTime_Check = new DateTime();
                    TimeSpan InTime_TimeSpan;
                    string From_Time_Str = "";
                    string To_Time_Str = "";
                    //DataSet DS_InTime = new DataSet();
                    DataTable DS_Time = new DataTable();
                    //DataSet DS_Time = new DataSet();
                    DataTable DS_InTime = new DataTable();
                    DateTime EmpdateIN_Change = new DateTime();
                    DataTable InsertData = new DataTable();
                    string Final_InTime = "";
                    string Final_OutTime = "";
                    string Final_Shift = "";

                    int K = 0;
                    bool Shift_Check_blb = false;
                    string Shift_Start_Time_Change;
                    string Shift_End_Time_Change;
                    string Employee_Time_Change = "";
                    DateTime ShiftdateStartIN_Change = new DateTime();
                    DateTime ShiftdateEndIN_Change = new DateTime();

                    //Shift Change Check Variable Declaration End
                    string Employee_Punch = "";
                    Spin_Machine_ID_Str = Emp_DS.Rows[intRow]["MachineID"].ToString();

                    if (Spin_Machine_ID_Str == "6")
                    {
                        Spin_Machine_ID_Str = "6";
                    }

                    Machine_ID_Str = UTF8Encryption(Emp_DS.Rows[intRow]["MachineID"].ToString());
                    //Machine_ID_Str = Emp_DS.Rows[intRow]["MachineID_Encrypt"].ToString();
                    OT_Week_OFF_Machine_No = Emp_DS.Rows[intRow]["MachineID"].ToString();
                    Date_Value_Str = Convert.ToDateTime(Get_Punching_Date1).AddDays(intCol).ToShortDateString();
                    Date_Value_Str1 = Convert.ToDateTime(Date_Value_Str).AddDays(1).ToShortDateString();

                    string Out_Punch_Date = Convert.ToDateTime(Get_Punching_Date1).AddDays(-1).ToShortDateString();
                    //TimeIN Get

                    SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                    SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    SSQL = SSQL + " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).ToString("yyyy/MM/dd") + " " + "02:30' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";
                    mLocalDS = objdata.RptEmployeeMultipleDetails(SSQL);



                    //TimeOUT Get
                    SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                    SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    SSQL = SSQL + " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).ToString("yyyy/MM/dd") + " " + "09:00' Order by TimeOUT desc";
                    mLocalDS1 = objdata.RptEmployeeMultipleDetails(SSQL);

                    if (mLocalDS.Rows.Count == 0 && mLocalDS1.Rows.Count == 0)
                    {
                        //Here Out Punch Check
                        SSQL = OutPunch_Get(Machine_ID_Str, Date_Value_Str, Date_Value_Str1, Out_Punch_Date);
                        mLocalDS1 = objdata.RptEmployeeMultipleDetails(SSQL);
                    }

                    //Shift Change Check Start
                    if (mLocalDS.Rows.Count != 0)
                    {
                        InTime_Check = Convert.ToDateTime(mLocalDS.Rows[0]["TimeIN"].ToString());
                        InToTime_Check = InTime_Check.AddHours(3);

                        InTime_Check = Convert.ToDateTime(string.Format(Convert.ToString(InTime_Check), "HH:mm:ss"));
                        InToTime_Check = Convert.ToDateTime(string.Format(Convert.ToString(InToTime_Check), "HH:mm:ss"));

                        //Old Time check error for 24 format

                        string TimeIN = InTime_Check.ToString();
                        string[] TimeIN_Split = TimeIN.Split(' ');
                        string TimeIN1 = TimeIN_Split[1].ToString();
                        string[] final = TimeIN1.Split(':');
                        string Final_IN = "";
                        if (Convert.ToInt32(InTime_Check.Hour) >= 10)
                        {
                            if (Convert.ToInt32(InTime_Check.Minute) >= 10)
                            {
                                Final_IN = InTime_Check.Hour + ":" + InTime_Check.Minute;
                            }
                            else
                            {
                                Final_IN = InTime_Check.Hour + ":0" + InTime_Check.Minute;
                            }
                        }
                        else
                        {
                            if (Convert.ToInt32(InTime_Check.Minute) >= 10)
                            {
                                Final_IN = "0" + InTime_Check.Hour + ":" + InTime_Check.Minute;
                            }
                            else
                            {
                                Final_IN = "0" + InTime_Check.Hour + ":0" + InTime_Check.Minute;
                            }
                        }


                        string TimeOUT = InToTime_Check.ToString();
                        string[] TimeOUT_Split = TimeOUT.Split(' ');
                        string TimeOUT1 = TimeOUT_Split[1].ToString();
                        string[] final1 = TimeOUT1.Split(':');
                        string Final_IN1 = "";
                        if (Convert.ToInt32(InToTime_Check.Hour) >= 10)
                        {
                            if (Convert.ToInt32(InToTime_Check.Minute) >= 10)
                            {
                                Final_IN1 = InToTime_Check.Hour + ":" + InToTime_Check.Minute;
                            }
                            else
                            {
                                Final_IN1 = InToTime_Check.Hour + ":0" + InToTime_Check.Minute;
                            }
                        }
                        else
                        {
                            if (Convert.ToInt32(InToTime_Check.Minute) >= 10)
                            {
                                Final_IN1 = "0" + InToTime_Check.Hour + ":" + InToTime_Check.Minute;
                            }
                            else
                            {
                                Final_IN1 = "0" + InToTime_Check.Hour + ":0" + InToTime_Check.Minute;
                            }
                        }
                        string Final_IN_Date = InTime_Check.Date.ToString("dd/MM/yyyy");
                        string Final_IN_Date1 = InToTime_Check.Date.ToString("dd/MM/yyyy");
                        //string[] Night_Time_Check = Final_IN.Split(':');
                        ////Two Hours OutTime Check
                        //if (Night_Time_Check[0] == "00")
                        //{
                        //    SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                        //            " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN + "' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN1 + "' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Asc";
                        //}
                        //else
                        //{
                        //    SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                        //            " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN + "' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN1 + "' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Asc";
                        //}
                        SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                                    " And TimeOUT >='" + Convert.ToDateTime(Final_IN_Date).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN + "' And TimeOUT <='" + Convert.ToDateTime(Final_IN_Date1).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN1 + "' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Asc";
                        DS_Time = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (DS_Time.Rows.Count != 0)
                        {
                            //After Out In Time Check
                            DateTime OUT_Winth_InTime_Check = Convert.ToDateTime(DS_Time.Rows[0]["TimeOUT"].ToString());
                            if (Convert.ToInt32(OUT_Winth_InTime_Check.Hour) >= 10)
                            {
                                if (Convert.ToInt32(OUT_Winth_InTime_Check.Minute) >= 10)
                                {
                                    Final_IN1 = OUT_Winth_InTime_Check.Hour + ":" + OUT_Winth_InTime_Check.Minute;
                                }
                                else
                                {
                                    Final_IN1 = OUT_Winth_InTime_Check.Hour + ":0" + OUT_Winth_InTime_Check.Minute;
                                }
                            }
                            else
                            {
                                if (Convert.ToInt32(OUT_Winth_InTime_Check.Minute) >= 10)
                                {
                                    Final_IN1 = "0" + OUT_Winth_InTime_Check.Hour + ":" + OUT_Winth_InTime_Check.Minute;
                                }
                                else
                                {
                                    Final_IN1 = "0" + OUT_Winth_InTime_Check.Hour + ":0" + OUT_Winth_InTime_Check.Minute;
                                }
                            }
                            string After_Final_Out_Date1 = OUT_Winth_InTime_Check.Date.ToString("dd/MM/yyyy");

                            if (After_Final_Out_Date1 == Date_Value_Str1)
                            {
                                SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'" +
                                       " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";
                            }
                            else
                            {
                                SSQL = "Select TimeIN from LogTime_IN where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                                " And TimeIN >='" + Convert.ToDateTime(After_Final_Out_Date1).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN1 + "' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";
                            }
                            DS_InTime = objdata.RptEmployeeMultipleDetails(SSQL);
                            if (DS_InTime.Rows.Count != 0)
                            {
                                //DataSet Shift_DS_Change = new DataSet();

                                DataTable Shift_DS_Change = new DataTable();

                                Final_InTime = DS_InTime.Rows[0]["TimeIN"].ToString();
                                //Check With IN Time Shift
                                SSQL = "Select * from Shift_Mst Where ShiftDesc like '%SHIFT%'";
                                SSQL = SSQL + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                Shift_DS_Change = objdata.RptEmployeeMultipleDetails(SSQL);
                                Shift_Check_blb = false;
                                for (int k = 0; k < Shift_DS_Change.Rows.Count; k++)
                                {
                                    Shift_Start_Time_Change = Date_Value_Str + " " + Shift_DS_Change.Rows[k]["StartIN"].ToString();
                                    if (Shift_DS_Change.Rows[k]["EndIN_Days"].ToString() == "1")
                                    {
                                        Shift_End_Time_Change = Date_Value_Str1 + " " + Shift_DS_Change.Rows[k]["EndIN"].ToString();
                                    }
                                    else
                                    {
                                        Shift_End_Time_Change = Date_Value_Str + " " + Shift_DS_Change.Rows[k]["EndIN"].ToString();
                                    }

                                    ShiftdateStartIN_Change = System.Convert.ToDateTime(Shift_Start_Time_Change);
                                    ShiftdateEndIN_Change = System.Convert.ToDateTime(Shift_End_Time_Change);

                                    if (EmpdateIN_Change >= ShiftdateStartIN_Change && EmpdateIN_Change <= ShiftdateEndIN_Change)
                                    {
                                        Final_Shift = Shift_DS_Change.Rows[k]["ShiftDesc"].ToString();
                                        Shift_Check_blb = true;
                                        break;
                                    }
                                    ShiftdateStartIN_Change = System.Convert.ToDateTime(Shift_Start_Time_Change);
                                    ShiftdateEndIN_Change = System.Convert.ToDateTime(Shift_End_Time_Change);
                                    EmpdateIN_Change = System.Convert.ToDateTime(Final_InTime);
                                    if (EmpdateIN_Change >= ShiftdateStartIN_Change && EmpdateIN_Change <= ShiftdateEndIN_Change)
                                    {
                                        Final_Shift = Shift_DS_Change.Rows[k]["ShiftDesc"].ToString();
                                        Shift_Check_blb = true;
                                        break;
                                    }
                                }
                                if (Shift_Check_blb == true)
                                {

                                    //IN Time Query Update
                                    string Querys = "";
                                    Querys = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'" +
                                    " And TimeIN >'" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN1 + ":59:00" + "' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";
                                    mLocalDS = objdata.RptEmployeeMultipleDetails(Querys);

                                    if (Final_Shift.ToUpper() == "SHIFT2")
                                    {
                                        Querys = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'" +
                                        " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "13:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "03:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT desc";
                                    }
                                    else
                                    {
                                        Querys = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'" +
                                                " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "17:40' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT desc";

                                    }
                                    mLocalDS1 = objdata.RptEmployeeMultipleDetails(Querys);
                                }
                                else
                                {
                                    //Get Employee In Time
                                    SSQL = "Select TimeIN from LogTime_IN where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                                    " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN1 + "' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";
                                    mLocalDS = objdata.RptEmployeeMultipleDetails(SSQL);
                                    //TimeOUT Get
                                    SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'" +
                                           " And TimeOUT >'" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN1 + "' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT desc";
                                    mLocalDS1 = objdata.RptEmployeeMultipleDetails(SSQL);
                                }
                            }
                            else
                            {
                                //Get Employee In Time
                                SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'" +
                                       " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";
                                mLocalDS = objdata.RptEmployeeMultipleDetails(SSQL);

                                //TimeOUT Get
                                //SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'" +
                                //      " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Asc";
                                SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                                " And TimeOUT >'" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN1 + "' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "23:55" + "' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT desc";

                                mLocalDS1 = objdata.RptEmployeeMultipleDetails(SSQL);
                                if (mLocalDS1.Rows.Count == 0)
                                {
                                    SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                                    " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN + "' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN1 + "' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT desc";

                                    mLocalDS1 = objdata.RptEmployeeMultipleDetails(SSQL);
                                }

                            }
                        }
                        else
                        {
                            //Get Employee In Time
                            SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'" +
                                   " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:30' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";
                            mLocalDS = objdata.RptEmployeeMultipleDetails(SSQL);

                            //TimeOUT Get
                            SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'" +
                                  " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "09:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT desc";
                            mLocalDS1 = objdata.RptEmployeeMultipleDetails(SSQL);
                        }
                    }
                    //Shift Change Check End 


                    if (mLocalDS.Rows.Count <= 0)
                    {
                        Time_IN_Str = "";
                        INTime = "";
                    }
                    else
                    {
                        DateTime datetime1 = Convert.ToDateTime(mLocalDS.Rows[0][0].ToString());
                        INTime = datetime1.ToString("hh:mm tt");
                        //INTime = string.Format("{0:hh:mm tt}", mLocalDS.Rows[0][0].ToString());
                    }

                    if (mLocalDS1.Rows.Count <= 0)
                    {
                        Time_Out_Str = "";
                        OUTTime = "";
                    }
                    else
                    {
                        DateTime datetime = Convert.ToDateTime(mLocalDS1.Rows[0][0].ToString());
                        OUTTime = datetime.ToString("hh:mm tt");
                        //OUTTime = string.Format(Convert.ToString(datetime), "HH:mm:ss tt");
                        //OUTTime = string.Format("{0:hh:mm tt}", mLocalDS1.Rows[0][0].ToString());

                    }

                    if (mLocalDS.Rows.Count == 0)
                    {
                        Employee_Punch = "";
                    }
                    else
                    {
                        Employee_Punch = mLocalDS.Rows[0][0].ToString();
                    }
                    if (mLocalDS.Rows.Count > 1)
                    {

                        for (int tin = 0; tin < mLocalDS.Rows.Count; tin++)
                        {
                            if (tin == 0)
                            {
                                Time_IN_Str = mLocalDS.Rows[tin][0].ToString();
                                if (mLocalDS1.Rows.Count > tin)
                                {
                                    Time_Out_Str = mLocalDS1.Rows[tin][0].ToString();
                                }
                                else if (mLocalDS1.Rows.Count > mLocalDS.Rows.Count)
                                {
                                    Time_Out_Str = mLocalDS1.Rows[mLocalDS1.Rows.Count - 1][0].ToString();
                                }
                                else
                                {
                                    Time_Out_Str = "";
                                }
                                TimeSpan ts4 = new TimeSpan();
                                ts4 = Convert.ToDateTime(string.Format("{0:hh\\:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
                                if (mLocalDS.Rows.Count <= 0)
                                {
                                    Time_IN_Str = "";
                                }
                                else
                                {
                                    Time_IN_Str = mLocalDS.Rows[tin][0].ToString();
                                }
                                if (Time_IN_Str == "" || Time_Out_Str == "")
                                {
                                    time_Check_dbl = time_Check_dbl;
                                }
                                else
                                {

                                    DateTime date1 = System.Convert.ToDateTime(Time_IN_Str);
                                    DateTime date2 = System.Convert.ToDateTime(Time_Out_Str);
                                    TimeSpan ts = new TimeSpan();
                                    ts = date2.Subtract(date1);

                                    //& ":" & Trim(ts.Minutes)
                                    Total_Time_get = (ts.Hours).ToString();
                                    ts4 = ts4.Add(ts);

                                    //OT Time Get
                                    Emp_Total_Work_Time_1 = (ts4.Hours) + ":" + (ts4.Minutes);
                                    Emp_Total_Work_Time_1 = (ts4.Hours) + ":" + (ts4.Minutes);

                                    if (Left_Val(ts4.Minutes.ToString(), 1) == "-" || Left_Val(ts4.Hours.ToString(), 1) == "-" || Emp_Total_Work_Time_1 == "0:-1")
                                    {
                                        Emp_Total_Work_Time_1 = "00:00";

                                    }
                                    if (Left_Val(Total_Time_get, 1) == "-")
                                    {
                                        date2 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                                        ts = date2.Subtract(date1);
                                        ts = date2.Subtract(date1);
                                        Total_Time_get = (ts.Hours).ToString();
                                        time_Check_dbl = double.Parse(Total_Time_get);
                                        Emp_Total_Work_Time_1 = (ts.Hours) + ":" + (ts.Minutes).ToString();
                                        Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                                        if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" || Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                        {
                                            Emp_Total_Work_Time_1 = "00:00";
                                        }
                                        if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" || Emp_Total_Work_Time_1 == "0:1" || Emp_Total_Work_Time_1 == "0:-3")
                                        {
                                            Emp_Total_Work_Time_1 = "00:00";
                                        }

                                    }
                                    else
                                    {
                                        //time_Check_dbl = time_Check_dbl + double.Parse(Total_Time_get);
                                        time_Check_dbl = ts4.Hours;
                                    }
                                }
                            }

                            //break;

                        }//For End
                    }
                    else
                    {
                        TimeSpan ts4 = new TimeSpan();
                        ts4 = Convert.ToDateTime(string.Format("{0:hh\\:mm}", Emp_Total_Work_Time_1)).TimeOfDay;

                        if (mLocalDS.Rows.Count <= 0)
                        {
                            Time_IN_Str = "";
                        }
                        else
                        {
                            Time_IN_Str = mLocalDS.Rows[0][0].ToString();
                        }
                        for (int tout = 0; tout < mLocalDS1.Rows.Count; tout++)
                        {
                            if (mLocalDS1.Rows.Count <= 0)
                            {
                                Time_Out_Str = "";
                            }
                            else
                            {
                                Time_Out_Str = mLocalDS1.Rows[0][0].ToString();
                            }

                            //break;

                        }
                        //Emp_Total_Work_Time
                        if (Time_IN_Str == "" || Time_Out_Str == "")
                        {
                            time_Check_dbl = 0;
                        }
                        else
                        {
                            DateTime date1 = System.Convert.ToDateTime(Time_IN_Str);
                            DateTime date2 = System.Convert.ToDateTime(Time_Out_Str);
                            TimeSpan ts = new TimeSpan();
                            ts = date2.Subtract(date1);
                            ts = date2.Subtract(date1);
                            Total_Time_get = (ts.Hours).ToString();
                            ts4 = ts4.Add(ts);
                            //OT Time Get
                            Emp_Total_Work_Time_1 = (ts4.Hours) + ":" + (ts4.Minutes);
                            Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                            if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" || Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                            {
                                Emp_Total_Work_Time_1 = "00:00";
                            }
                            if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" || Emp_Total_Work_Time_1 == "0:-1" || Emp_Total_Work_Time_1 == "0:-3")
                            {
                                Emp_Total_Work_Time_1 = "00:00";
                            }
                            if (Left_Val(Total_Time_get, 1) == "-")
                            {

                                date2 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                                ts = date2.Subtract(date1);
                                ts = date2.Subtract(date1);
                                Total_Time_get = ts.Hours.ToString();
                                time_Check_dbl = double.Parse(Total_Time_get);
                                Emp_Total_Work_Time_1 = (ts.Hours) + ":" + (ts.Minutes);
                                Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" || Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";

                                    time_Check_dbl = 0;
                                }
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" || Emp_Total_Work_Time_1 == "0:-1" || Emp_Total_Work_Time_1 == "0:-3")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";

                                    time_Check_dbl = 0;
                                }
                            }
                            else
                            {
                                time_Check_dbl = double.Parse(Total_Time_get);
                            }
                        }
                    }
                    //Emp_Total_Work_Time




                    //Emp_Total_Work_Time
                    string Emp_Total_Work_Time = "";
                    string Final_OT_Work_Time = "00:00";
                    string Final_OT_Work_Time_Val = "00:00";
                    Emp_Total_Work_Time = Emp_Total_Work_Time_1;
                    //Find Week OFF
                    string Employee_Week_Name = "";
                    string Assign_Week_Name = "";
                    //mLocalDS = Nothing
                    Employee_Week_Name = Convert.ToDateTime(Date_Value_Str).DayOfWeek.ToString();
                    SSQL = "Select WeekOff from Shiftrrecording where MachineNo='" + OT_Week_OFF_Machine_No + "'";
                    mLocalDS = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (mLocalDS.Rows.Count <= 0)
                    {
                        Assign_Week_Name = "";
                    }
                    else
                    {
                        //Assign_Week_Name = mLocalDS.Tables(0).Rows(0)(0)
                        Assign_Week_Name = "";
                    }

                    //NFH Days Check Start
                    bool NFH_Day_Check = false;
                    DateTime NFH_Date = new DateTime();
                    DateTime DOJ_Date_Format = new DateTime();
                    String qry_nfh = "Select NFHDate from NFH_Mst where NFHDate=convert(varchar,'" + Convert.ToDateTime(Date_Value_Str).ToString("yyyy/MM/dd") + "',103)";
                    mLocalDS = objdata.RptEmployeeMultipleDetails(qry_nfh);


                    if (Employee_Week_Name.ToUpper() == Assign_Week_Name.ToUpper())
                    {
                        //Skip
                    }
                    else
                    {
                        //Get Employee Work Time
                        Double Calculate_Attd_Work_Time = 0;
                        Double Calculate_Attd_Work_Time_half = 0;
                        SSQL = "Select * from Employee_MST where MachineID='" + OT_Week_OFF_Machine_No + "' and Calculate_Work_Hours <> '' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'"; // And IsActive='Yes'";
                        mLocalDS = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (mLocalDS.Rows.Count <= 0)
                        {
                            Calculate_Attd_Work_Time = 7;
                        }
                        else
                        {
                            if (mLocalDS.Rows[0]["Calculate_Work_Hours"].ToString() != "")
                            {
                                Calculate_Attd_Work_Time = Convert.ToDouble(mLocalDS.Rows[0]["Calculate_Work_Hours"]);
                            }
                            else
                            {
                                Calculate_Attd_Work_Time = 0;
                            }
                            if (Calculate_Attd_Work_Time == 0)
                            {
                                Calculate_Attd_Work_Time = 7;
                            }
                            else
                            {
                                Calculate_Attd_Work_Time = 7;
                            }
                        }
                        Calculate_Attd_Work_Time_half = 4.0;



                        string Wages_Name_Check = "";
                        DataTable MDS_DS = new DataTable();
                        SSQL = "Select * from Employee_MST where MachineID='" + OT_Week_OFF_Machine_No + "' and CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                        MDS_DS = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (MDS_DS.Rows.Count != 0)
                        {
                            Wages_Name_Check = MDS_DS.Rows[0]["Wages"].ToString();
                        }
                        if (((Wages_Name_Check).ToUpper() == ("STAFF")) || ((Wages_Name_Check).ToUpper() == ("SECURITY").ToUpper()) || ((Wages_Name_Check).ToUpper() == ("FITTER & ELECTRICIANS").ToUpper()) || ((Wages_Name_Check).ToUpper() == ("DRIVERS").ToUpper()))
                        {
                            Calculate_Attd_Work_Time = 7;
                        }
                        else
                        {
                            Calculate_Attd_Work_Time = 7;
                        }


                        halfPresent = "0"; //Half Days Calculate Start
                        if (time_Check_dbl >= Calculate_Attd_Work_Time_half && time_Check_dbl < Calculate_Attd_Work_Time)
                        {
                            isPresent = true;
                            halfPresent = "1";
                        }
                        //Half Days Calculate End
                        if (time_Check_dbl >= Calculate_Attd_Work_Time)
                        {
                            isPresent = true;
                            halfPresent = "0";
                        }
                    }

                    //Shift Check Code Start
                    DataTable Shift_Ds = new DataTable();
                    string Start_IN = "";
                    string End_In = "";
                    string Shift_Name_Store = "";
                    bool Shift_Check_blb_Check = false;

                    DateTime ShiftdateStartIN_Check = new DateTime();
                    DateTime ShiftdateEndIN_Check = new DateTime();
                    DateTime EmpdateIN_Check = new DateTime();
                    //if( isPresent == true)
                    //{
                    //    Shift_Check_blb_Check = false;
                    //    if((Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("STAFF")) || (Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper()== ("Watch & Ward")))
                    //    {
                    //        Employee_Shift_Name_DB = "GENERAL";
                    //    }
                    //    else
                    //    {

                    //        //Shift Master Check Code Start
                    //        SSQL = "Select * from Shift_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And ShiftDesc <> 'GENERAL' Order by ShiftDesc Asc";
                    //        Shift_Ds = objdata.RptEmployeeMultipleDetails(SSQL);
                    //        if(Shift_Ds.Rows.Count != 0 )
                    //        {
                    //            SSQL = "Select * from Shift_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And ShiftDesc like '%SHIFT%'";
                    //            Shift_Ds = objdata.RptEmployeeMultipleDetails(SSQL);
                    //            Shift_Check_blb = false;
                    //            for(K = 0;K < Shift_Ds.Rows.Count;K++)
                    //            {
                    //                Start_IN = Convert.ToDateTime(Date_Value_Str).AddDays(Convert.ToDouble(Shift_Ds.Rows[K]["StartIN_Days"])).ToString("dd/MM/yyyy") + " " + Shift_Ds.Rows[K]["StartIN"].ToString();
                    //                End_In = Convert.ToDateTime(Date_Value_Str).AddDays(Convert.ToDouble(Shift_Ds.Rows[K]["EndIN_Days"])).ToString("dd/MM/yyyy") + " " + Shift_Ds.Rows[K]["EndIN"].ToString();

                    //                ShiftdateStartIN_Check = Convert.ToDateTime(Start_IN);
                    //                ShiftdateEndIN_Check = Convert.ToDateTime(End_In);
                    //                EmpdateIN_Check = Convert.ToDateTime(Employee_Punch);
                    //                if(EmpdateIN_Check >= ShiftdateStartIN_Check && EmpdateIN_Check <= ShiftdateEndIN_Check)
                    //                {
                    //                    Employee_Shift_Name_DB = Shift_Ds.Rows[K]["ShiftDesc"].ToString();
                    //                    Shift_Check_blb_Check = true ;
                    //                    break;
                    //                }
                    //            }
                    //            if(Shift_Check_blb_Check == false)
                    //            {
                    //                Employee_Shift_Name_DB = "No Shift";
                    //            }
                    //        }
                    //        else
                    //        {
                    //            Employee_Shift_Name_DB = "No Shift";
                    //        }
                    //        //Shift Master Check Code End

                    //    }
                    //}
                    //else
                    //{
                    //    Shift_Name_Store = "No Shift";
                    //}



                    if (Time_IN_Str != "")
                    {
                        Shift_Check_blb_Check = false;
                        //if ((Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("STAFF")) || (Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("Watch & Ward").ToUpper()) || (Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() ==("Manager").ToUpper()))
                        //{
                        //    Employee_Shift_Name_DB = "GENERAL";
                        //}
                        //else
                        //{

                        //Shift Master Check Code Start
                        //SSQL = "Select * from Shift_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And ShiftDesc <> 'GENERAL' Order by ShiftDesc Asc";

                        SSQL = "Select * from Shift_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                        // if ((Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("STAFF")) || (Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("SECURITY").ToUpper()) || (Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("FITTER & ELECTRICIANS").ToUpper()) || (Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("DRIVERS").ToUpper()))
                        if ((Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("MONTHLY - STAFF") && Emp_DS.Rows[intRow]["ShiftType"].ToString().ToUpper() == ("GENERAL").ToUpper()) || (Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("WEEKLY").ToUpper() && Emp_DS.Rows[intRow]["ShiftType"].ToString().ToUpper() == ("GENERAL").ToUpper()) || (Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("CLEANING").ToUpper() && Emp_DS.Rows[intRow]["ShiftType"].ToString().ToUpper() == ("GENERAL").ToUpper()) || (Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("MIXING").ToUpper() && Emp_DS.Rows[intRow]["ShiftType"].ToString().ToUpper() == ("GENERAL").ToUpper()) || (Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("WEEKLY-HINDI").ToUpper() && Emp_DS.Rows[intRow]["ShiftType"].ToString().ToUpper() == ("GENERAL").ToUpper()))
                        {
                            SSQL = SSQL + " And ShiftDesc = 'GENERAL'";
                        }
                        else
                        {
                            SSQL = SSQL + " And ShiftDesc <> 'GENERAL'";
                        }
                        SSQL = SSQL + " Order by ShiftDesc Asc";
                        Shift_Ds = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (Shift_Ds.Rows.Count != 0)
                        {
                            SSQL = "Select * from Shift_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'"; //And ShiftDesc like '%SHIFT%'";
                                                                                                                                       //  if ((Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("STAFF")) || (Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("SECURITY").ToUpper()) || (Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("FITTER & ELECTRICIANS").ToUpper()) || (Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("DRIVERS").ToUpper()))
                            if ((Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("MONTHLY - STAFF") && Emp_DS.Rows[intRow]["ShiftType"].ToString().ToUpper() == ("GENERAL").ToUpper()) || (Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("WEEKLY").ToUpper() && Emp_DS.Rows[intRow]["ShiftType"].ToString().ToUpper() == ("GENERAL").ToUpper()) || (Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("CLEANING").ToUpper() && Emp_DS.Rows[intRow]["ShiftType"].ToString().ToUpper() == ("GENERAL").ToUpper()) || (Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("MIXING").ToUpper() && Emp_DS.Rows[intRow]["ShiftType"].ToString().ToUpper() == ("GENERAL").ToUpper()) || (Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("WEEKLY-HINDI").ToUpper() && Emp_DS.Rows[intRow]["ShiftType"].ToString().ToUpper() == ("GENERAL").ToUpper()))
                            {
                                SSQL = SSQL + " And ShiftDesc = 'GENERAL'";
                            }
                            else
                            {
                                SSQL = SSQL + " And ShiftDesc like '%SHIFT%'";
                            }
                            Shift_Ds = objdata.RptEmployeeMultipleDetails(SSQL);
                            Shift_Check_blb = false;
                            for (K = 0; K < Shift_Ds.Rows.Count; K++)
                            {
                                Start_IN = Convert.ToDateTime(Date_Value_Str).AddDays(Convert.ToDouble(Shift_Ds.Rows[K]["StartIN_Days"])).ToString("dd/MM/yyyy") + " " + Shift_Ds.Rows[K]["StartIN"].ToString();
                                End_In = Convert.ToDateTime(Date_Value_Str).AddDays(Convert.ToDouble(Shift_Ds.Rows[K]["EndIN_Days"])).ToString("dd/MM/yyyy") + " " + Shift_Ds.Rows[K]["EndIN"].ToString();

                                ShiftdateStartIN_Check = Convert.ToDateTime(Start_IN);
                                ShiftdateEndIN_Check = Convert.ToDateTime(End_In);
                                EmpdateIN_Check = Convert.ToDateTime(Employee_Punch);
                                if (EmpdateIN_Check >= ShiftdateStartIN_Check && EmpdateIN_Check <= ShiftdateEndIN_Check)
                                {
                                    Employee_Shift_Name_DB = Shift_Ds.Rows[K]["ShiftDesc"].ToString();
                                    Shift_Check_blb_Check = true;
                                    break;
                                }
                            }
                            if (Shift_Check_blb_Check == false)
                            {
                                Employee_Shift_Name_DB = "No Shift";
                            }
                        }
                        else
                        {
                            Employee_Shift_Name_DB = "No Shift";
                        }
                        //Shift Master Check Code End

                        //}
                    }
                    else
                    {
                        Shift_Name_Store = "No Shift";
                        Employee_Shift_Name_DB = "No Shift";
                    }

                    //Shift Check Code End



                    string ShiftType = "";
                    string Pre_Abs = "";
                    if (isPresent == true)
                    {

                        if (Employee_Shift_Name_DB != "No Shift")
                        {
                            ShiftType = "Proper";

                            if (halfPresent == "1")
                            {
                                Pre_Abs = "Half Day";
                            }
                            else
                            {
                                Pre_Abs = "Present";
                            }

                        }
                        else
                        {
                            ShiftType = "Mismatch";

                            if (halfPresent == "1")
                            {
                                Pre_Abs = "Half Day";
                            }
                            else
                            {
                                Pre_Abs = "Present";
                            }

                        }

                    }
                    else
                    {

                        if (INTime == "" && OUTTime == "")
                        {
                            ShiftType = "Leave";
                            Employee_Shift_Name_DB = "Leave";
                            Pre_Abs = "Leave";

                        }
                        else if (INTime == "")
                        {
                            if (OUTTime != "")
                            {
                                ShiftType = "Improper";
                            }
                            Pre_Abs = "Absent";

                        }
                        else if (OUTTime == "")
                        {
                            if (INTime != "")
                            {
                                ShiftType = "Improper";
                            }
                            Pre_Abs = "Absent";

                        }
                        else if (INTime != "" && OUTTime != "")
                        {
                            ShiftType = "Absent";
                            Pre_Abs = "Absent";

                        }
                    }
                    Deduction_Total_Hrs = "00:00";
                    Late_time_Check_dbl = 0;
                    Deduction_Min = "0";
                    Late_Total_Time_get = "0";

                    //Mill Incentive Days Check
                    string MID_Shift_Str = "";
                    string MID_Present = "0";
                    string MID_ShortName = "";
                    //string MID_Shift_Check = "";  

                    if (ShiftType == "Improper" && INTime != "" && Employee_Shift_Name_DB != "No Shift")
                    {
                        SSQL = "";
                        SSQL = "Select * from MstMillIncentiveDay where CCode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                        SSQL = SSQL + " and Convert(date,Date,103)=Convert(date,'" + Date_Value_Str + "',103) and Shift='" + Employee_Shift_Name_DB + "'";
                        DataTable dt_MID = new DataTable();
                        dt_MID = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (dt_MID.Rows.Count > 0)
                        {
                            MID_Shift_Str = dt_MID.Rows[0]["Shift"].ToString();
                            MID_ShortName = dt_MID.Rows[0]["ShortName"].ToString();
                            if (dt_MID.Rows[0]["Present"].ToString() == "Present")
                            {
                                MID_Present = "1.0";
                            }
                            else if (dt_MID.Rows[0]["Present"].ToString() == "Half Present")
                            {
                                MID_Present = "0.5";
                            }
                            else
                            {
                                MID_Present = "0.0";
                            }
                        }
                        else
                        {
                            MID_Shift_Str = "";
                            MID_Present = "0";
                            MID_ShortName = "";
                        }
                    }


                    //Late Time Checking 
                    if (Employee_Shift_Name_DB != "No Shift")
                    {
                        if (Employee_Shift_Name_DB != "Leave")
                        {
                            if (INTime != "" && OUTTime != "")
                            {
                                TimeSpan ts = new TimeSpan();
                                string[] Sp_Total_Hr;
                                bool Errflag_In = false;
                                int Minus_check = 0;
                                if (MachineID1 == "543")
                                {
                                    MachineID1 = "543";
                                }
                                double Total_Minutes;
                                DataTable da_Shift = new DataTable();
                                DataTable TempDt = new DataTable();

                                SSQL = "select * from Shift_Mst where ShiftDesc='" + Employee_Shift_Name_DB + "' and  CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                da_Shift = objdata.RptEmployeeMultipleDetails(SSQL);
                                if (da_Shift.Rows.Count != 0)
                                {
                                    Shift_Start_Time = Convert.ToDateTime(da_Shift.Rows[0]["StartTime"]);
                                    Emp_Time_IN = Convert.ToDateTime(INTime);
                                }

                                if (Emp_Time_IN > Shift_Start_Time)
                                {
                                    Total_Minutes = Emp_Time_IN.Subtract(Shift_Start_Time).TotalMinutes;
                                    //Get LateIN deduction using Emp TimeIN for Particular shift
                                    SSQL = "";
                                    SSQL = "Select isnull(Deduction,'0') as Deduction from MstLateIN where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                                    SSQL = SSQL + " and CAST(" + Total_Minutes + " AS decimal)>=CAST(StartMin AS decimal) and CAST(" + Total_Minutes + "  AS decimal)<=CAST(EndMin AS decimal)";
                                    TempDt = objdata.RptEmployeeMultipleDetails(SSQL);
                                    if (TempDt.Rows.Count > 0)
                                    {

                                        Deduction_Min = TempDt.Rows[0]["Deduction"].ToString();
                                        DateTime date1 = System.Convert.ToDateTime(Deduction_Min);
                                        DateTime date2 = System.Convert.ToDateTime(Emp_Total_Work_Time_1);


                                        ts = date2.Subtract(date1);
                                        ts = date2.Subtract(date1);
                                        Late_Total_Time_get = ts.Hours.ToString();
                                        if (Left_Val(Late_Total_Time_get, 1) == "-")
                                        {
                                            date1 = System.Convert.ToDateTime(Time_IN_Str).AddDays(0);
                                            date2 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                                            ts = date2.Subtract(date1);
                                            ts = date2.Subtract(date1);
                                            Late_Total_Time_get = ts.Hours.ToString();
                                            Late_time_Check_dbl = double.Parse(Total_Time_get);
                                            Deduction_Total_Hrs = (ts.Hours) + ":" + (ts.Minutes);
                                            Sp_Total_Hr = Deduction_Total_Hrs.Split(':');
                                            Minus_check = Convert.ToInt32(Sp_Total_Hr[1]);

                                        }
                                        else
                                        {
                                            Late_time_Check_dbl = double.Parse(Late_Total_Time_get);
                                            Deduction_Total_Hrs = (ts.Hours) + ":" + (ts.Minutes);
                                            Sp_Total_Hr = Deduction_Total_Hrs.Split(':');
                                            Minus_check = Convert.ToInt32(Sp_Total_Hr[1]);
                                        }
                                        Errflag_In = true;
                                    }
                                    else
                                    {
                                        Deduction_Hrs = 0;

                                    }

                                    if (Errflag_In == true)
                                    {

                                        string Minus_Val = "";

                                        Sp_Total_Hr = Deduction_Total_Hrs.Split(':');
                                        Minus_check = Convert.ToInt32(Sp_Total_Hr[1]);
                                        DataTable Da_New = new DataTable();
                                        if (Deduction_Min == "00:30")
                                        {
                                            Minus_check = 30;
                                        }
                                        else if (Deduction_Min == "01:00")
                                        {
                                            Minus_check = 45;
                                        }
                                        else
                                        {

                                        }
                                        SSQL = "";
                                        SSQL = "Select isnull(Deduction,'0') as Deduction from MstLateIN where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                                        SSQL = SSQL + " and CAST(" + Minus_check + " AS decimal)>=CAST(StartMin AS decimal) and CAST(" + Minus_check + "  AS decimal)<=CAST(EndMin AS decimal)";
                                        Da_New = objdata.RptEmployeeMultipleDetails(SSQL);
                                        if (Da_New.Rows.Count != 0)
                                        {
                                            Minus_Val = Da_New.Rows[0][0].ToString();

                                        }
                                        if (Minus_Val == "00:30")
                                        {
                                            Deduction_Total_Hrs = (ts.Hours) + "." + 05;
                                            Deduction_Min = Minus_Val;
                                        }
                                        else if (Minus_Val == "01:00")
                                        {
                                            Deduction_Total_Hrs = (Late_time_Check_dbl + Convert.ToDouble(1)).ToString();
                                            Deduction_Min = Minus_Val;
                                        }
                                        else
                                        {
                                            Deduction_Total_Hrs = Convert.ToDouble(Late_time_Check_dbl).ToString();
                                        }


                                    }
                                    else
                                    {

                                        string Minus_Val = "";
                                        DateTime date1 = System.Convert.ToDateTime(INTime);
                                        DateTime date2 = System.Convert.ToDateTime(OUTTime);
                                        ts = date2.Subtract(date1);
                                        ts = date2.Subtract(date1);
                                        Late_Total_Time_get = ts.Hours.ToString();
                                        if (Left_Val(Late_Total_Time_get, 1) == "-")
                                        {
                                            date1 = System.Convert.ToDateTime(Time_IN_Str).AddDays(0);
                                            date2 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                                            ts = date2.Subtract(date1);
                                            Late_Total_Time_get = ts.Hours.ToString();
                                            Late_time_Check_dbl = double.Parse(Total_Time_get);
                                            Deduction_Total_Hrs = (ts.Hours) + ":" + (ts.Minutes);
                                            Sp_Total_Hr = Deduction_Total_Hrs.Split(':');
                                            Minus_check = Convert.ToInt32(Sp_Total_Hr[1]);

                                        }
                                        else
                                        {
                                            Late_time_Check_dbl = double.Parse(Late_Total_Time_get);
                                            Deduction_Total_Hrs = (ts.Hours) + ":" + (ts.Minutes);
                                            Sp_Total_Hr = Deduction_Total_Hrs.Split(':');
                                            Minus_check = Convert.ToInt32(Sp_Total_Hr[1]);
                                        }
                                        DataTable Da_New = new DataTable();
                                        if (Deduction_Min == "00:30")
                                        {
                                            Minus_check = 30;
                                        }
                                        else if (Deduction_Min == "01:00")
                                        {
                                            Minus_check = 45;
                                        }
                                        else
                                        {

                                        }
                                        SSQL = "";
                                        SSQL = "Select isnull(Deduction,'0') as Deduction from MstLateIN where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                                        SSQL = SSQL + " and CAST(" + Minus_check + " AS decimal)>=CAST(StartMin AS decimal) and CAST(" + Minus_check + "  AS decimal)<=CAST(EndMin AS decimal)";
                                        Da_New = objdata.RptEmployeeMultipleDetails(SSQL);
                                        if (Da_New.Rows.Count != 0)
                                        {
                                            Minus_Val = Da_New.Rows[0][0].ToString();

                                        }
                                        if (Minus_Val == "00:30")
                                        {
                                            Deduction_Total_Hrs = (ts.Hours) + "." + 05;
                                            if (Errflag_In == true)
                                            {
                                                Deduction_Min = Minus_Val;
                                            }
                                        }
                                        else if (Minus_Val == "01:00")
                                        {
                                            Deduction_Total_Hrs = (Late_time_Check_dbl + Convert.ToDouble(1)).ToString();
                                            if (Errflag_In == true)
                                            {
                                                Deduction_Min = Minus_Val;
                                            }
                                        }
                                        else
                                        {
                                            Deduction_Total_Hrs = Convert.ToDouble(Late_time_Check_dbl).ToString();
                                        }



                                    }

                                }
                                else
                                {
                                    string Minus_Val = "";
                                    DataTable Da_New = new DataTable();
                                    DateTime date1 = System.Convert.ToDateTime(INTime);
                                    DateTime date2 = System.Convert.ToDateTime(OUTTime);
                                    date1 = System.Convert.ToDateTime(Time_IN_Str).AddDays(0);
                                    date2 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                                    ts = date2.Subtract(date1);
                                    Late_Total_Time_get = ts.Hours.ToString();
                                    Late_time_Check_dbl = double.Parse(Total_Time_get);
                                    Deduction_Total_Hrs = (ts.Hours) + ":" + (ts.Minutes);
                                    Sp_Total_Hr = Deduction_Total_Hrs.Split(':');
                                    Minus_check = Convert.ToInt32(Sp_Total_Hr[1]);

                                    SSQL = "";
                                    SSQL = "Select isnull(Deduction,'0') as Deduction from MstLateIN where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                                    SSQL = SSQL + " and CAST(" + Minus_check + " AS decimal)>=CAST(StartMin AS decimal) and CAST(" + Minus_check + "  AS decimal)<=CAST(EndMin AS decimal)";
                                    Da_New = objdata.RptEmployeeMultipleDetails(SSQL);
                                    if (Da_New.Rows.Count != 0)
                                    {
                                        Minus_Val = Da_New.Rows[0][0].ToString();

                                    }
                                    if (Minus_Val == "00:30")
                                    {
                                        Deduction_Total_Hrs = (ts.Hours) + "." + 05;
                                        Deduction_Min = "0";

                                    }
                                    else if (Minus_Val == "01:00")
                                    {
                                        Deduction_Total_Hrs = (Late_time_Check_dbl + Convert.ToDouble(1)).ToString();
                                        Deduction_Min = "0";

                                    }
                                    else
                                    {
                                        Deduction_Total_Hrs = Convert.ToDouble(Late_time_Check_dbl).ToString();
                                        Deduction_Min = "0";
                                    }
                                }
                            }
                            else
                            {
                                Late_Total_Time_get = Total_Time_get;
                                Late_time_Check_dbl = time_Check_dbl;
                                Deduction_Total_Hrs = "00:00";
                            }
                        }
                    }
                    if (Deduction_Total_Hrs == "00:00")
                    {
                        Deduction_Total_Hrs = "0";
                    }
                    if (Employee_Shift_Name_DB == "No Shift")
                    {
                        Deduction_Total_Hrs = Total_Time_get;
                        Late_Total_Time_get = Total_Time_get;
                        Deduction_Min = "0";
                    }

                    //if (Deduction_Total_Hrs == "")
                    //{
                    //    Deduction_Total_Hrs = "0";
                    //}
                    //if (Late_Total_Time_get == "")
                    //{
                    //    Late_Total_Time_get = "0";
                    //}
                    //Week OF Day Check
                    bool Check_Week_Off_DOJ = false;
                    string Attn_Date_Day = DateTime.Parse(Date_Value_Str).DayOfWeek.ToString();
                    if ((Emp_WH_Day).ToUpper() == (Attn_Date_Day).ToUpper())
                    {

                        //'Check DOJ Date to Report Date
                        DateTime Week_Off_Date;
                        DateTime WH_DOJ_Date_Format;

                        //  if (((Emp_DS.Rows[intRow]["Wages"].ToString().Trim().ToUpper()) == ("STAFF").ToUpper()) || ((Emp_DS.Rows[intRow]["Wages"].ToString().Trim().ToUpper()) == ("SECURITY").ToUpper()) || ((Emp_DS.Rows[intRow]["Wages"].ToString().Trim().ToUpper()) == ("FITTER & ELECTRICIANS").ToUpper()) || ((Emp_DS.Rows[intRow]["Wages"].ToString().Trim().ToUpper()) == ("DRIVERS").ToUpper()))
                        if ((Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("MONTHLY - STAFF") && Emp_DS.Rows[intRow]["ShiftType"].ToString().ToUpper() == ("GENERAL").ToUpper()) || (Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("WEEKLY").ToUpper() && Emp_DS.Rows[intRow]["ShiftType"].ToString().ToUpper() == ("GENERAL").ToUpper()) || (Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("CLEANING").ToUpper() && Emp_DS.Rows[intRow]["ShiftType"].ToString().ToUpper() == ("GENERAL").ToUpper()) || (Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("MIXING").ToUpper() && Emp_DS.Rows[intRow]["ShiftType"].ToString().ToUpper() == ("GENERAL").ToUpper()) || (Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("WEEKLY-HINDI").ToUpper() && Emp_DS.Rows[intRow]["ShiftType"].ToString().ToUpper() == ("GENERAL").ToUpper()))
                        {
                            if (DOJ_Date_Str == "")
                            {
                                Week_Off_Date = Convert.ToDateTime(Date_Value_Str); //Report Date
                                
                                WH_DOJ_Date_Format = Convert.ToDateTime(DOJ_Date_Str);
                                if (WH_DOJ_Date_Format <= Week_Off_Date)
                                {
                                    Check_Week_Off_DOJ = true;
                                }
                                else
                                {
                                    Check_Week_Off_DOJ = false;
                                }
                            }
                            else
                            {
                                Check_Week_Off_DOJ = true;
                            }
                        }
                        else
                        {
                            Check_Week_Off_DOJ = true;
                        }

                        Month_WH_Count = Month_WH_Count + 1;
                        if (isPresent == true)
                        {
                            if (NFH_Day_Check == false)
                            {
                                if (halfPresent == "1")
                                {
                                    Present_WH_Count = Convert.ToDecimal(Convert.ToDouble(Present_WH_Count) + (0.5));
                                }
                                else if (halfPresent == "0")
                                {
                                    Present_WH_Count = Present_WH_Count + 1;
                                }
                            }
                        }

                    }

                    if (isPresent == true)
                    {
                        if (halfPresent == "1")
                        {
                            Present_Count = Convert.ToDecimal(Convert.ToDouble(Present_Count) + (0.5));
                        }
                        else if (halfPresent == "0")
                        {
                            Present_Count = Present_Count + 1;
                        }
                    }
                    else
                    {
                        Appsent_Count = Appsent_Count + 1;
                    }
                    //'colIndex += shiftCount
                    //intK += 1

                    if (INTime == "" || OUTTime == "")
                    {
                        Deduction_Total_Hrs = "0";
                        Late_Total_Time_get = "0";
                    }

                    DataTable mDataSet = new DataTable();
                    //Update Employee Worked_Days
                    SSQL = "Select * from LogTime_Days where MachineID='" + Get_Machine_ID + "' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And Attn_Date_Str='" + Date_Value_Str + "'";
                    mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (mDataSet.Rows.Count != 0)
                    {
                        SSQL = "Delete from LogTime_Days where MachineID='" + Get_Machine_ID + "' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And Attn_Date_Str='" + Date_Value_Str + "'";
                        objdata.RptEmployeeMultipleDetails(SSQL);
                    }
                    SSQL = "Insert Into LogTime_Days(CompCode,LocCode,MachineID,ExistingCode,FirstName,LastName,DeptName,";
                    SSQL = SSQL + "Designation,DOJ,Wages,Present,Wh_Check,Wh_Count,Wh_Present_Count,Shift,Total_Hrs,Attn_Date_Str,Attn_Date,";
                    SSQL = SSQL + "TimeIN,TimeOUT,Total_Hrs1,TypeName,Present_Absent,Division,Late_Hrs,Late_Total_Hrs,Late_Deducation,MID_Shift,MID_Present,MID_ShortName)";
                    SSQL = SSQL + " Values('" + SessionCcode + "','" + SessionLcode + "','" + Get_Machine_ID + "','" + Emp_DS.Rows[intRow]["ExistingCode"] + "',";
                    SSQL = SSQL + "'" + Emp_DS.Rows[intRow]["FirstName"] + "','" + Emp_DS.Rows[intRow]["LastName"] + "',";
                    SSQL = SSQL + "'" + Emp_DS.Rows[intRow]["DeptName"] + "','" + Emp_DS.Rows[intRow]["Designation"] + "',";
                    SSQL = SSQL + "'" + Emp_DS.Rows[intRow]["DOJ"] + "','" + Emp_DS.Rows[intRow]["Wages"] + "','" + Present_Count + "','" + Check_Week_Off_DOJ + "',";
                    SSQL = SSQL + "'" + Month_WH_Count + "','" + Present_WH_Count + "','" + Employee_Shift_Name_DB + "','" + time_Check_dbl + "','" + Date_Value_Str + "','" + Convert.ToDateTime(Date_Value_Str).ToString("yyyy/MM/dd") + "',";
                    SSQL = SSQL + "'" + INTime + "','" + OUTTime + "','" + Emp_Total_Work_Time_1 + "','" + ShiftType + "','" + Pre_Abs + "','" + Emp_DS.Rows[intRow]["Division"].ToString() + "','" + Late_Total_Time_get + "','" + Deduction_Total_Hrs + "','" + Deduction_Min + "',";
                    SSQL = SSQL + "'" + MID_Shift_Str + "','" + MID_Present + "','" + MID_ShortName + "')";
                    objdata.RptEmployeeMultipleDetails(SSQL);

                    Present_Count = 0;
                    Check_Week_Off_DOJ = false;
                    Month_WH_Count = 0;
                    Present_WH_Count = 0;
                    Employee_Shift_Name_DB = "No Shift";
                    //'Employee_Shift_Name_DB
                    //'Final_Count = Present_Count
                    //'Month_WH_Count
                    //'Present_WH_Count
                    //'time_Check_dbl
                }

                intI += 1;
                Total_Days_Count = 0;
                Final_Count = 0;
                Appsent_Count = 0;
                Present_Count = 0;
                //Month_WH_Count = 0;
                Present_WH_Count = 0;
                NFH_Days_Count = 0;
                NFH_Days_Present_Count = 0;
            }
        }
    }

    public void Get_Working_Days_Save_DB_Pending_Employee(string Get_Machine_ID, string Get_Punching_Date1, string Get_Punching_Date2)
    {
        DateTime Date1 = Convert.ToDateTime(Get_Punching_Date1);
        DateTime Date2 = Convert.ToDateTime(Get_Punching_Date2);

        int daycount = (int)((Date2 - Date1).TotalDays);
        int daysAdded = daycount + 1;

        int intI;
        int intK;
        int intCol;
        string Fin_Year = "";
        string Months_Full_Str = "";
        string Date_Col_Str = "";
        int Month_Int = 1;
        string Spin_Machine_ID_Str = "";

        string[] Time_Minus_Value_Check;

        DataTable mLocalDS = new DataTable();

        DataTable mLocalDS1 = new DataTable();

        //Attn_Flex Col Add Var
        string[] Att_Date_Check;
        string Att_Already_Date_Check;
        string Att_Year_Check = "";
        int Month_Name_Change;
        string halfPresent = "0";
        int EPay_Total_Days_Get = 0;
        intCol = 4;
        Month_Name_Change = 1;


        EPay_Total_Days_Get = 1;

        intI = 2;
        intK = 1;

        bool ErrFlag = false;


        DataTable Emp_DS = new DataTable();

        SSQL = "Select * from Employee_Mst where CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " And MachineID='" + Get_Machine_ID + "'";
        Emp_DS = objdata.RptEmployeeMultipleDetails(SSQL);

        if (Emp_DS.Rows.Count == 0)
        {
            SSQL = "Select * from Employee_Mst_New_Emp where CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And MachineID='" + Get_Machine_ID + "'";
            Emp_DS = objdata.RptEmployeeMultipleDetails(SSQL);

            if (Emp_DS.Rows.Count == 0)
            {
            }
        }

        if (Emp_DS.Rows.Count > 0)
        {
            ErrFlag = false;
        }
        else
        {
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            for (int intRow = 0; intRow < Emp_DS.Rows.Count; intRow++)
            {
                intK = 1;

                string Emp_Total_Work_Time_1 = "00:00";
                //Get Employee Week OF DAY
                DataTable DS_WH = new DataTable();
                string Emp_WH_Day;
                string DOJ_Date_Str;
                string MachineID1 = Emp_DS.Rows[intRow]["MachineID"].ToString();

                string Query = " Select * from Employee_Mst_New_Emp where MachineID='" + MachineID1 + "' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' ";
                DS_WH = objdata.RptEmployeeMultipleDetails(Query);
                if (DS_WH.Rows.Count != 0)
                {
                    Emp_WH_Day = DS_WH.Rows[0]["WeekOff"].ToString();
                    DOJ_Date_Str = DS_WH.Rows[0]["DOJ"].ToString();
                }
                else
                {
                    Emp_WH_Day = "";
                    DOJ_Date_Str = "";
                }

                int colIndex = intK;
                string DayofWeekName = "";
                decimal Weekoff_Count = 0;
                isPresent = false;
                bool isWHPresent = false;
                decimal Present_Count = 0;
                decimal Present_WH_Count = 0;
                int Appsent_Count = 0;
                decimal Final_Count = 0;
                decimal Total_Days_Count = 0;
                string Already_Date_Check;
                int Days_Insert_RowVal = 0;
                decimal FullNight_Shift_Count = 0;
                int NFH_Days_Count = 0;
                decimal NFH_Days_Present_Count = 0;
                Already_Date_Check = "";
                string Year_Check = "";
                string Time_IN_Str = "";
                string Time_Out_Str = "";
                int g = 1;

                double OT_Time;

                OT_Time = 0;

                string INTime = "";
                string OUTTime = "";

                for (intCol = 0; (intCol <= (daysAdded - 1)); intCol++)
                {

                    Emp_Total_Work_Time_1 = "00:00";
                    string Machine_ID_Str = "";
                    string OT_Week_OFF_Machine_No;
                    string Date_Value_Str = "";
                    string Total_Time_get = "";
                    string Final_OT_Work_Time_1 = "";
                    //DataSet mLocalDS = new DataSet();

                    //DataTable mLocalDS = new DataTable();

                    decimal Month_WH_Count = 0;
                    int j = 0;
                    double time_Check_dbl = 0;
                    string Date_Value_Str1 = "";
                    string Employee_Shift_Name_DB = "No Shift";
                    isPresent = false;
                    isWHPresent = false;

                    //Shift Change Check Variable Declaration Start
                    DateTime InTime_Check = new DateTime();
                    DateTime InToTime_Check = new DateTime();
                    TimeSpan InTime_TimeSpan;
                    string From_Time_Str = "";
                    string To_Time_Str = "";
                    //DataSet DS_InTime = new DataSet();
                    DataTable DS_Time = new DataTable();
                    //DataSet DS_Time = new DataSet();
                    DataTable DS_InTime = new DataTable();
                    DateTime EmpdateIN_Change = new DateTime();
                    DataTable InsertData = new DataTable();
                    string Final_InTime = "";
                    string Final_OutTime = "";
                    string Final_Shift = "";

                    int K = 0;
                    bool Shift_Check_blb = false;
                    string Shift_Start_Time_Change;
                    string Shift_End_Time_Change;
                    string Employee_Time_Change = "";
                    DateTime ShiftdateStartIN_Change = new DateTime();
                    DateTime ShiftdateEndIN_Change = new DateTime();

                    //Shift Change Check Variable Declaration End
                    string Employee_Punch = "";
                    Spin_Machine_ID_Str = Emp_DS.Rows[intRow]["MachineID"].ToString();

                    if (Spin_Machine_ID_Str == "305140")
                    {
                        Spin_Machine_ID_Str = "305140";
                    }

                    Machine_ID_Str = UTF8Encryption(Emp_DS.Rows[intRow]["MachineID"].ToString());
                    //Machine_ID_Str = Emp_DS.Rows[intRow]["MachineID_Encrypt"].ToString();
                    OT_Week_OFF_Machine_No = Emp_DS.Rows[intRow]["MachineID"].ToString();
                    Date_Value_Str = Convert.ToDateTime(Get_Punching_Date1).AddDays(intCol).ToShortDateString();
                    Date_Value_Str1 = Convert.ToDateTime(Date_Value_Str).AddDays(1).ToShortDateString();
                    string Out_Punch_Date = Convert.ToDateTime(Get_Punching_Date1).AddDays(-1).ToShortDateString();

                    //TimeIN Get

                    SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                    SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    SSQL = SSQL + " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";
                    mLocalDS = objdata.RptEmployeeMultipleDetails(SSQL);



                    //TimeOUT Get
                    SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                    SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    SSQL = SSQL + " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT Asc";
                    mLocalDS1 = objdata.RptEmployeeMultipleDetails(SSQL);

                    if (mLocalDS.Rows.Count == 0 && mLocalDS1.Rows.Count == 0)
                    {
                        //Here Out Punch Check
                        SSQL = OutPunch_Get(Machine_ID_Str, Date_Value_Str, Date_Value_Str1, Out_Punch_Date);
                        mLocalDS1 = objdata.RptEmployeeMultipleDetails(SSQL);
                    }

                    //Shift Change Check Start
                    if (mLocalDS.Rows.Count != 0)
                    {
                        InTime_Check = Convert.ToDateTime(mLocalDS.Rows[0]["TimeIN"].ToString());
                        InToTime_Check = InTime_Check.AddHours(2);

                        InTime_Check = Convert.ToDateTime(string.Format(Convert.ToString(InTime_Check), "HH:mm:ss"));
                        InToTime_Check = Convert.ToDateTime(string.Format(Convert.ToString(InToTime_Check), "HH:mm:ss"));

                        //Old Time check error for 24 format

                        string TimeIN = InTime_Check.ToString();
                        string[] TimeIN_Split = TimeIN.Split(' ');
                        string TimeIN1 = TimeIN_Split[1].ToString();
                        string[] final = TimeIN1.Split(':');
                        string Final_IN = "";
                        if (Convert.ToInt32(InTime_Check.Hour) >= 10)
                        {
                            if (Convert.ToInt32(InTime_Check.Minute) >= 10)
                            {
                                Final_IN = InTime_Check.Hour + ":" + InTime_Check.Minute;
                            }
                            else
                            {
                                Final_IN = InTime_Check.Hour + ":0" + InTime_Check.Minute;
                            }
                        }
                        else
                        {
                            if (Convert.ToInt32(InTime_Check.Minute) >= 10)
                            {
                                Final_IN = "0" + InTime_Check.Hour + ":" + InTime_Check.Minute;
                            }
                            else
                            {
                                Final_IN = "0" + InTime_Check.Hour + ":0" + InTime_Check.Minute;
                            }
                        }


                        string TimeOUT = InToTime_Check.ToString();
                        string[] TimeOUT_Split = TimeOUT.Split(' ');
                        string TimeOUT1 = TimeOUT_Split[1].ToString();
                        string[] final1 = TimeOUT1.Split(':');
                        string Final_IN1 = "";
                        if (Convert.ToInt32(InToTime_Check.Hour) >= 10)
                        {
                            if (Convert.ToInt32(InToTime_Check.Minute) >= 10)
                            {
                                Final_IN1 = InToTime_Check.Hour + ":" + InToTime_Check.Minute;
                            }
                            else
                            {
                                Final_IN1 = InToTime_Check.Hour + ":0" + InToTime_Check.Minute;
                            }
                        }
                        else
                        {
                            if (Convert.ToInt32(InToTime_Check.Minute) >= 10)
                            {
                                Final_IN1 = "0" + InToTime_Check.Hour + ":" + InToTime_Check.Minute;
                            }
                            else
                            {
                                Final_IN1 = "0" + InToTime_Check.Hour + ":0" + InToTime_Check.Minute;
                            }
                        }
                        string Final_IN_Date = InTime_Check.Date.ToString("dd/MM/yyyy");
                        string Final_IN_Date1 = InToTime_Check.Date.ToString("dd/MM/yyyy");
                        //Two Hours OutTime Check
                        //SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                        //        " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN + "' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN1 + "' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Asc";
                        //DS_Time = objdata.RptEmployeeMultipleDetails(SSQL);
                        SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                                    " And TimeOUT >='" + Convert.ToDateTime(Final_IN_Date).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN + "' And TimeOUT <='" + Convert.ToDateTime(Final_IN_Date1).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN1 + "' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Asc";
                        DS_Time = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (DS_Time.Rows.Count != 0)
                        {
                            //After Out In Time Check
                            DateTime OUT_Winth_InTime_Check = Convert.ToDateTime(DS_Time.Rows[0]["TimeOUT"].ToString());
                            if (Convert.ToInt32(OUT_Winth_InTime_Check.Hour) >= 10)
                            {
                                if (Convert.ToInt32(OUT_Winth_InTime_Check.Minute) >= 10)
                                {
                                    Final_IN1 = OUT_Winth_InTime_Check.Hour + ":" + OUT_Winth_InTime_Check.Minute;
                                }
                                else
                                {
                                    Final_IN1 = OUT_Winth_InTime_Check.Hour + ":0" + OUT_Winth_InTime_Check.Minute;
                                }
                            }
                            else
                            {
                                if (Convert.ToInt32(OUT_Winth_InTime_Check.Minute) >= 10)
                                {
                                    Final_IN1 = "0" + OUT_Winth_InTime_Check.Hour + ":" + OUT_Winth_InTime_Check.Minute;
                                }
                                else
                                {
                                    Final_IN1 = "0" + OUT_Winth_InTime_Check.Hour + ":0" + OUT_Winth_InTime_Check.Minute;
                                }
                            }
                            string After_Final_Out_Date1 = OUT_Winth_InTime_Check.Date.ToString("dd/MM/yyyy");

                            if (After_Final_Out_Date1 == Date_Value_Str1)
                            {
                                SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'" +
                                       " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";
                            }
                            else
                            {
                                SSQL = "Select TimeIN from LogTime_IN where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                                " And TimeIN >='" + Convert.ToDateTime(After_Final_Out_Date1).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN1 + "' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";
                            }
                            //SSQL = "Select TimeIN from LogTime_IN where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                            //" And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN1 + "' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";
                            DS_InTime = objdata.RptEmployeeMultipleDetails(SSQL);
                            if (DS_InTime.Rows.Count != 0)
                            {
                                //DataSet Shift_DS_Change = new DataSet();

                                DataTable Shift_DS_Change = new DataTable();

                                Final_InTime = DS_InTime.Rows[0]["TimeIN"].ToString();
                                //Check With IN Time Shift
                                SSQL = "Select * from Shift_Mst Where ShiftDesc like '%SHIFT%'";
                                SSQL = SSQL + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                Shift_DS_Change = objdata.RptEmployeeMultipleDetails(SSQL);
                                Shift_Check_blb = false;
                                for (int k = 0; k < Shift_DS_Change.Rows.Count; k++)
                                {
                                    Shift_Start_Time_Change = Date_Value_Str + " " + Shift_DS_Change.Rows[k]["StartIN"].ToString();
                                    if (Shift_DS_Change.Rows[k]["EndIN_Days"].ToString() == "1")
                                    {
                                        Shift_End_Time_Change = Date_Value_Str1 + " " + Shift_DS_Change.Rows[k]["EndIN"].ToString();
                                    }
                                    else
                                    {
                                        Shift_End_Time_Change = Date_Value_Str + " " + Shift_DS_Change.Rows[k]["EndIN"].ToString();
                                    }

                                    ShiftdateStartIN_Change = System.Convert.ToDateTime(Shift_Start_Time_Change);
                                    ShiftdateEndIN_Change = System.Convert.ToDateTime(Shift_End_Time_Change);

                                    if (EmpdateIN_Change >= ShiftdateStartIN_Change && EmpdateIN_Change <= ShiftdateEndIN_Change)
                                    {
                                        Final_Shift = Shift_DS_Change.Rows[k]["ShiftDesc"].ToString();
                                        Shift_Check_blb = true;
                                        break;
                                    }
                                    ShiftdateStartIN_Change = System.Convert.ToDateTime(Shift_Start_Time_Change);
                                    ShiftdateEndIN_Change = System.Convert.ToDateTime(Shift_End_Time_Change);
                                    EmpdateIN_Change = System.Convert.ToDateTime(Final_InTime);
                                    if (EmpdateIN_Change >= ShiftdateStartIN_Change && EmpdateIN_Change <= ShiftdateEndIN_Change)
                                    {
                                        Final_Shift = Shift_DS_Change.Rows[k]["ShiftDesc"].ToString();
                                        Shift_Check_blb = true;
                                        break;
                                    }
                                }
                                if (Shift_Check_blb == true)
                                {

                                    //IN Time Query Update
                                    string Querys = "";
                                    Querys = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'" +
                                    " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN1 + "' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";
                                    mLocalDS = objdata.RptEmployeeMultipleDetails(Querys);

                                    if (Final_Shift.ToUpper() == "SHIFT2")
                                    {
                                        Querys = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'" +
                                        " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "13:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "03:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Asc";
                                    }
                                    else
                                    {
                                        Querys = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'" +
                                                " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "17:40' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Asc";

                                    }
                                    mLocalDS1 = objdata.RptEmployeeMultipleDetails(Querys);
                                }
                                else
                                {
                                    //Get Employee In Time
                                    SSQL = "Select TimeIN from LogTime_IN where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                                    " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN1 + "' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";
                                    mLocalDS = objdata.RptEmployeeMultipleDetails(SSQL);
                                    //TimeOUT Get
                                    SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'" +
                                           " And TimeOUT >'" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN1 + "' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Asc";
                                    mLocalDS1 = objdata.RptEmployeeMultipleDetails(SSQL);
                                }
                            }
                            else
                            {
                                //Get Employee In Time
                                SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'" +
                                       " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";
                                mLocalDS = objdata.RptEmployeeMultipleDetails(SSQL);

                                //TimeOUT Get
                                //SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'" +
                                //      " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Asc";
                                SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + UTF8Encryption(Spin_Machine_ID_Str) + "'" +
                                " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN + "' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + Final_IN1 + "' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Asc";

                                mLocalDS1 = objdata.RptEmployeeMultipleDetails(SSQL);

                            }
                        }
                        else
                        {
                            //Get Employee In Time
                            SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'" +
                                   " And TimeIN >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeIN ASC";
                            mLocalDS = objdata.RptEmployeeMultipleDetails(SSQL);

                            //TimeOUT Get
                            SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'" +
                                  " And TimeOUT >='" + Convert.ToDateTime(Date_Value_Str).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + Convert.ToDateTime(Date_Value_Str1).AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' and  CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "' Order by TimeOUT Asc";
                            mLocalDS1 = objdata.RptEmployeeMultipleDetails(SSQL);
                        }
                    }
                    //Shift Change Check End 


                    if (mLocalDS.Rows.Count <= 0)
                    {
                        Time_IN_Str = "";
                        INTime = "";
                    }
                    else
                    {
                        DateTime datetime1 = Convert.ToDateTime(mLocalDS.Rows[0][0].ToString());
                        INTime = datetime1.ToString("hh:mm tt");
                        //INTime = string.Format("{0:hh:mm tt}", mLocalDS.Rows[0][0].ToString());
                    }

                    if (mLocalDS1.Rows.Count <= 0)
                    {
                        Time_Out_Str = "";
                        OUTTime = "";
                    }
                    else
                    {
                        DateTime datetime = Convert.ToDateTime(mLocalDS1.Rows[0][0].ToString());
                        OUTTime = datetime.ToString("hh:mm tt");
                        //OUTTime = string.Format(Convert.ToString(datetime), "HH:mm:ss tt");
                        //OUTTime = string.Format("{0:hh:mm tt}", mLocalDS1.Rows[0][0].ToString());

                    }


                    if (mLocalDS.Rows.Count == 0)
                    {
                        Employee_Punch = "";
                    }
                    else
                    {
                        Employee_Punch = mLocalDS.Rows[0][0].ToString();
                    }
                    if (mLocalDS.Rows.Count > 1)
                    {
                        for (int tin = 0; tin < mLocalDS.Rows.Count; tin++)
                        {
                            Time_IN_Str = mLocalDS.Rows[tin][0].ToString();
                            if (mLocalDS1.Rows.Count > tin)
                            {
                                Time_Out_Str = mLocalDS1.Rows[tin][0].ToString();
                            }
                            else if (mLocalDS1.Rows.Count > mLocalDS.Rows.Count)
                            {
                                Time_Out_Str = mLocalDS1.Rows[mLocalDS1.Rows.Count - 1][0].ToString();
                            }
                            else
                            {
                                Time_Out_Str = "";
                            }
                            TimeSpan ts4 = new TimeSpan();
                            ts4 = Convert.ToDateTime(string.Format("{0:hh\\:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
                            if (mLocalDS.Rows.Count <= 0)
                            {
                                Time_IN_Str = "";
                            }
                            else
                            {
                                Time_IN_Str = mLocalDS.Rows[tin][0].ToString();
                            }
                            if (Time_IN_Str == "" || Time_Out_Str == "")
                            {
                                time_Check_dbl = time_Check_dbl;
                            }
                            else
                            {

                                DateTime date1 = System.Convert.ToDateTime(Time_IN_Str);
                                DateTime date2 = System.Convert.ToDateTime(Time_Out_Str);
                                TimeSpan ts = new TimeSpan();
                                ts = date2.Subtract(date1);

                                //& ":" & Trim(ts.Minutes)
                                Total_Time_get = (ts.Hours).ToString();
                                ts4 = ts4.Add(ts);

                                //OT Time Get
                                Emp_Total_Work_Time_1 = (ts4.Hours) + ":" + (ts4.Minutes);
                                Emp_Total_Work_Time_1 = (ts4.Hours) + ":" + (ts4.Minutes);

                                if (Left_Val(ts4.Minutes.ToString(), 1) == "-" || Left_Val(ts4.Hours.ToString(), 1) == "-" || Emp_Total_Work_Time_1 == "0:-1")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";

                                }
                                if (Left_Val(Total_Time_get, 1) == "-")
                                {
                                    date2 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                                    ts = date2.Subtract(date1);
                                    ts = date2.Subtract(date1);
                                    Total_Time_get = (ts.Hours).ToString();
                                    time_Check_dbl = double.Parse(Total_Time_get);
                                    Emp_Total_Work_Time_1 = (ts.Hours) + ":" + (ts.Minutes).ToString();
                                    Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                                    if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" || Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    {
                                        Emp_Total_Work_Time_1 = "00:00";
                                    }
                                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" || Emp_Total_Work_Time_1 == "0:1" || Emp_Total_Work_Time_1 == "0:-3")
                                    {
                                        Emp_Total_Work_Time_1 = "00:00";
                                    }

                                }
                                else
                                {
                                    time_Check_dbl = time_Check_dbl + double.Parse(Total_Time_get);
                                }
                            }

                            //break;

                        }//For End
                    }
                    else
                    {
                        TimeSpan ts4 = new TimeSpan();
                        ts4 = Convert.ToDateTime(string.Format("{0:hh\\:mm}", Emp_Total_Work_Time_1)).TimeOfDay;

                        if (mLocalDS.Rows.Count <= 0)
                        {
                            Time_IN_Str = "";
                        }
                        else
                        {
                            Time_IN_Str = mLocalDS.Rows[0][0].ToString();
                        }
                        for (int tout = 0; tout < mLocalDS1.Rows.Count; tout++)
                        {
                            if (mLocalDS1.Rows.Count <= 0)
                            {
                                Time_Out_Str = "";
                            }
                            else
                            {
                                Time_Out_Str = mLocalDS1.Rows[0][0].ToString();
                            }

                            //break;

                        }
                        //Emp_Total_Work_Time
                        if (Time_IN_Str == "" || Time_Out_Str == "")
                        {
                            time_Check_dbl = 0;
                        }
                        else
                        {
                            DateTime date1 = System.Convert.ToDateTime(Time_IN_Str);
                            DateTime date2 = System.Convert.ToDateTime(Time_Out_Str);
                            TimeSpan ts = new TimeSpan();
                            ts = date2.Subtract(date1);
                            ts = date2.Subtract(date1);
                            Total_Time_get = (ts.Hours).ToString();
                            ts4 = ts4.Add(ts);
                            //OT Time Get
                            Emp_Total_Work_Time_1 = (ts4.Hours) + ":" + (ts4.Minutes);
                            Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                            if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" || Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                            {
                                Emp_Total_Work_Time_1 = "00:00";
                            }
                            if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" || Emp_Total_Work_Time_1 == "0:-1" || Emp_Total_Work_Time_1 == "0:-3")
                            {
                                Emp_Total_Work_Time_1 = "00:00";
                            }
                            if (Left_Val(Total_Time_get, 1) == "-")
                            {

                                date2 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                                ts = date2.Subtract(date1);
                                ts = date2.Subtract(date1);
                                Total_Time_get = ts.Hours.ToString();
                                time_Check_dbl = double.Parse(Total_Time_get);
                                Emp_Total_Work_Time_1 = (ts.Hours) + ":" + (ts.Minutes);
                                Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" || Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";

                                    time_Check_dbl = 0;
                                }
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" || Emp_Total_Work_Time_1 == "0:-1" || Emp_Total_Work_Time_1 == "0:-3")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";

                                    time_Check_dbl = 0;
                                }
                            }
                            else
                            {
                                time_Check_dbl = double.Parse(Total_Time_get);
                            }
                        }
                    }
                    //Emp_Total_Work_Time

                    //Emp_Total_Work_Time
                    string Emp_Total_Work_Time = "";
                    string Final_OT_Work_Time = "00:00";
                    string Final_OT_Work_Time_Val = "00:00";
                    Emp_Total_Work_Time = Emp_Total_Work_Time_1;
                    //Find Week OFF
                    string Employee_Week_Name = "";
                    string Assign_Week_Name = "";
                    //mLocalDS = Nothing
                    Employee_Week_Name = Convert.ToDateTime(Date_Value_Str).DayOfWeek.ToString();
                    SSQL = "Select WeekOff from Shiftrrecording where MachineNo='" + OT_Week_OFF_Machine_No + "'";
                    mLocalDS = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (mLocalDS.Rows.Count <= 0)
                    {
                        Assign_Week_Name = "";
                    }
                    else
                    {
                        //Assign_Week_Name = mLocalDS.Tables(0).Rows(0)(0)
                        Assign_Week_Name = "";
                    }

                    //NFH Days Check Start
                    bool NFH_Day_Check = false;
                    DateTime NFH_Date = new DateTime();
                    DateTime DOJ_Date_Format = new DateTime();
                    String qry_nfh = "Select NFHDate from NFH_Mst where NFHDate=convert(varchar,'" + Convert.ToDateTime(Date_Value_Str).ToString("yyyy/MM/dd") + "',103)";
                    mLocalDS = objdata.RptEmployeeMultipleDetails(qry_nfh);
                    //If mLocalDS.Tables(0).Rows.Count > 0 Then
                    //    'Date OF Joining Check Start
                    //    If DOJ_Date_Str <> "" Then
                    //        NFH_Date = mLocalDS.Tables(0).Rows(0)("NFHDate")
                    //        DOJ_Date_Format = DOJ_Date_Str
                    //        If DOJ_Date_Format.Date < NFH_Date.Date Then
                    //            'isPresent = True
                    //            NFH_Day_Check = True
                    //            NFH_Days_Count = NFH_Days_Count + 1
                    //        Else
                    //            'Skip
                    //       End If
                    //    Else
                    //        'isPresent = True
                    //        NFH_Day_Check = True
                    //        NFH_Days_Count = NFH_Days_Count + 1
                    //    End If
                    //    'Date OF Joining Check End
                    //End If
                    //NFH Days Check End

                    if (Employee_Week_Name.ToUpper() == Assign_Week_Name.ToUpper())
                    {
                        //Skip
                    }
                    else
                    {
                        //Get Employee Work Time
                        Double Calculate_Attd_Work_Time = 0;
                        Double Calculate_Attd_Work_Time_half = 0;
                        SSQL = "Select * from Employee_Mst_New_Emp where MachineID='" + OT_Week_OFF_Machine_No + "' and Calculate_Work_Hours <> '' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'"; // And IsActive='Yes'";
                        mLocalDS = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (mLocalDS.Rows.Count <= 0)
                        {
                            Calculate_Attd_Work_Time = 7;
                        }
                        else
                        {
                            if (mLocalDS.Rows[0]["Calculate_Work_Hours"].ToString() != "")
                            {
                                Calculate_Attd_Work_Time = Convert.ToDouble(mLocalDS.Rows[0]["Calculate_Work_Hours"]);
                            }
                            else
                            {
                                Calculate_Attd_Work_Time = 0;
                            }
                            if (Calculate_Attd_Work_Time == 0)
                            {
                                Calculate_Attd_Work_Time = 7;
                            }
                            else
                            {
                                Calculate_Attd_Work_Time = 7;
                            }
                        }
                        Calculate_Attd_Work_Time_half = 4.0;



                        string Wages_Name_Check = "";
                        DataTable MDS_DS = new DataTable();
                        SSQL = "Select * from Employee_MST where MachineID='" + OT_Week_OFF_Machine_No + "' and CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                        MDS_DS = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (MDS_DS.Rows.Count != 0)
                        {
                            Wages_Name_Check = MDS_DS.Rows[0]["Wages"].ToString();
                        }
                        if (((Wages_Name_Check).ToUpper() == ("STAFF")) || ((Wages_Name_Check).ToUpper() == ("SECURITY").ToUpper()) || ((Wages_Name_Check).ToUpper() == ("FITTER & ELECTRICIANS").ToUpper()) || ((Wages_Name_Check).ToUpper() == ("DRIVERS").ToUpper()))
                        {
                            Calculate_Attd_Work_Time = 8;
                        }
                        else
                        {
                            Calculate_Attd_Work_Time = 8;
                        }

                        halfPresent = "0"; //Half Days Calculate Start
                        if (time_Check_dbl >= Calculate_Attd_Work_Time_half && time_Check_dbl < Calculate_Attd_Work_Time)
                        {
                            isPresent = true;
                            halfPresent = "1";
                        }
                        //Half Days Calculate End
                        if (time_Check_dbl >= Calculate_Attd_Work_Time)
                        {
                            isPresent = true;
                            halfPresent = "0";
                        }
                    }

                    //Shift Check Code Start
                    DataTable Shift_Ds = new DataTable();
                    string Start_IN = "";
                    string End_In = "";
                    string Shift_Name_Store = "";
                    bool Shift_Check_blb_Check = false;

                    DateTime ShiftdateStartIN_Check = new DateTime();
                    DateTime ShiftdateEndIN_Check = new DateTime();
                    DateTime EmpdateIN_Check = new DateTime();
                    //if( isPresent == true)
                    //{
                    //    Shift_Check_blb_Check = false;
                    //    if((Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("STAFF")) || (Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper()== ("Watch & Ward")))
                    //    {
                    //        Employee_Shift_Name_DB = "GENERAL";
                    //    }
                    //    else
                    //    {

                    //        //Shift Master Check Code Start
                    //        SSQL = "Select * from Shift_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And ShiftDesc <> 'GENERAL' Order by ShiftDesc Asc";
                    //        Shift_Ds = objdata.RptEmployeeMultipleDetails(SSQL);
                    //        if(Shift_Ds.Rows.Count != 0 )
                    //        {
                    //            SSQL = "Select * from Shift_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And ShiftDesc like '%SHIFT%'";
                    //            Shift_Ds = objdata.RptEmployeeMultipleDetails(SSQL);
                    //            Shift_Check_blb = false;
                    //            for(K = 0;K < Shift_Ds.Rows.Count;K++)
                    //            {
                    //                Start_IN = Convert.ToDateTime(Date_Value_Str).AddDays(Convert.ToDouble(Shift_Ds.Rows[K]["StartIN_Days"])).ToString("dd/MM/yyyy") + " " + Shift_Ds.Rows[K]["StartIN"].ToString();
                    //                End_In = Convert.ToDateTime(Date_Value_Str).AddDays(Convert.ToDouble(Shift_Ds.Rows[K]["EndIN_Days"])).ToString("dd/MM/yyyy") + " " + Shift_Ds.Rows[K]["EndIN"].ToString();

                    //                ShiftdateStartIN_Check = Convert.ToDateTime(Start_IN);
                    //                ShiftdateEndIN_Check = Convert.ToDateTime(End_In);
                    //                EmpdateIN_Check = Convert.ToDateTime(Employee_Punch);
                    //                if(EmpdateIN_Check >= ShiftdateStartIN_Check && EmpdateIN_Check <= ShiftdateEndIN_Check)
                    //                {
                    //                    Employee_Shift_Name_DB = Shift_Ds.Rows[K]["ShiftDesc"].ToString();
                    //                    Shift_Check_blb_Check = true ;
                    //                    break;
                    //                }
                    //            }
                    //            if(Shift_Check_blb_Check == false)
                    //            {
                    //                Employee_Shift_Name_DB = "No Shift";
                    //            }
                    //        }
                    //        else
                    //        {
                    //            Employee_Shift_Name_DB = "No Shift";
                    //        }
                    //        //Shift Master Check Code End

                    //    }
                    //}
                    //else
                    //{
                    //    Shift_Name_Store = "No Shift";
                    //}



                    if (Time_IN_Str != "")
                    {
                        Shift_Check_blb_Check = false;
                        //if ((Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("STAFF")) || (Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("Watch & Ward").ToUpper()) || (Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("Manager").ToUpper()))
                        //{
                        //    Employee_Shift_Name_DB = "GENERAL";
                        //}
                        //else
                        //{

                        //Shift Master Check Code Start
                        //SSQL = "Select * from Shift_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And ShiftDesc <> 'GENERAL' Order by ShiftDesc Asc";
                        SSQL = "Select * from Shift_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                        //  if ((Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("STAFF")) || (Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("SECURITY").ToUpper()) || (Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("FITTER & ELECTRICIANS").ToUpper()) || (Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("DRIVERS").ToUpper()))
                        if ((Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("MONTHLY - STAFF") && Emp_DS.Rows[intRow]["ShiftType"].ToString().ToUpper() == ("GENERAL").ToUpper()) || (Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("WEEKLY").ToUpper() && Emp_DS.Rows[intRow]["ShiftType"].ToString().ToUpper() == ("GENERAL").ToUpper()) || (Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("CLEANING").ToUpper() && Emp_DS.Rows[intRow]["ShiftType"].ToString().ToUpper() == ("GENERAL").ToUpper()) || (Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("MIXING").ToUpper() && Emp_DS.Rows[intRow]["ShiftType"].ToString().ToUpper() == ("GENERAL").ToUpper()) || (Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("WEEKLY-HINDI").ToUpper() && Emp_DS.Rows[intRow]["ShiftType"].ToString().ToUpper() == ("GENERAL").ToUpper()))
                        {
                            SSQL = SSQL + " And ShiftDesc = 'GENERAL'";
                        }
                        else
                        {
                            SSQL = SSQL + " And ShiftDesc <> 'GENERAL'";
                        }
                        SSQL = SSQL + " Order by ShiftDesc Asc";
                        Shift_Ds = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (Shift_Ds.Rows.Count != 0)
                        {
                            SSQL = "Select * from Shift_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'"; //And ShiftDesc like '%SHIFT%'";
                            // if ((Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("STAFF")) || (Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("SECURITY").ToUpper()) || (Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("FITTER & ELECTRICIANS").ToUpper()) || (Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("DRIVERS").ToUpper()))
                            if ((Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("MONTHLY - STAFF") && Emp_DS.Rows[intRow]["ShiftType"].ToString().ToUpper() == ("GENERAL").ToUpper()) || (Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("WEEKLY").ToUpper() && Emp_DS.Rows[intRow]["ShiftType"].ToString().ToUpper() == ("GENERAL").ToUpper()) || (Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("CLEANING").ToUpper() && Emp_DS.Rows[intRow]["ShiftType"].ToString().ToUpper() == ("GENERAL").ToUpper()) || (Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("MIXING").ToUpper() && Emp_DS.Rows[intRow]["ShiftType"].ToString().ToUpper() == ("GENERAL").ToUpper()) || (Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("WEEKLY-HINDI").ToUpper() && Emp_DS.Rows[intRow]["ShiftType"].ToString().ToUpper() == ("GENERAL").ToUpper()))
                            {
                                SSQL = SSQL + " And ShiftDesc = 'GENERAL'";
                            }
                            else
                            {
                                SSQL = SSQL + " And ShiftDesc like '%SHIFT%'";
                            }
                            Shift_Ds = objdata.RptEmployeeMultipleDetails(SSQL);
                            Shift_Check_blb = false;
                            for (K = 0; K < Shift_Ds.Rows.Count; K++)
                            {
                                Start_IN = Convert.ToDateTime(Date_Value_Str).AddDays(Convert.ToDouble(Shift_Ds.Rows[K]["StartIN_Days"])).ToString("dd/MM/yyyy") + " " + Shift_Ds.Rows[K]["StartIN"].ToString();
                                End_In = Convert.ToDateTime(Date_Value_Str).AddDays(Convert.ToDouble(Shift_Ds.Rows[K]["EndIN_Days"])).ToString("dd/MM/yyyy") + " " + Shift_Ds.Rows[K]["EndIN"].ToString();

                                ShiftdateStartIN_Check = Convert.ToDateTime(Start_IN);
                                ShiftdateEndIN_Check = Convert.ToDateTime(End_In);
                                EmpdateIN_Check = Convert.ToDateTime(Employee_Punch);
                                if (EmpdateIN_Check >= ShiftdateStartIN_Check && EmpdateIN_Check <= ShiftdateEndIN_Check)
                                {
                                    Employee_Shift_Name_DB = Shift_Ds.Rows[K]["ShiftDesc"].ToString();
                                    Shift_Check_blb_Check = true;
                                    break;
                                }
                            }
                            if (Shift_Check_blb_Check == false)
                            {
                                Employee_Shift_Name_DB = "No Shift";
                            }
                        }
                        else
                        {
                            Employee_Shift_Name_DB = "No Shift";
                        }
                        //Shift Master Check Code End

                        //}
                    }
                    else
                    {
                        Shift_Name_Store = "No Shift";
                        Employee_Shift_Name_DB = "No Shift";
                    }

                    //Shift Check Code End



                    string ShiftType = "";
                    string Pre_Abs = "";
                    if (isPresent == true)
                    {

                        if (Employee_Shift_Name_DB != "No Shift")
                        {
                            ShiftType = "Proper";

                            if (halfPresent == "1")
                            {
                                Pre_Abs = "Half Day";
                            }
                            else
                            {
                                Pre_Abs = "Present";
                            }

                        }
                        else
                        {
                            ShiftType = "Mismatch";

                            if (halfPresent == "1")
                            {
                                Pre_Abs = "Half Day";
                            }
                            else
                            {
                                Pre_Abs = "Present";
                            }

                        }

                    }
                    else
                    {

                        if (INTime == "" && OUTTime == "")
                        {
                            ShiftType = "Leave";
                            Employee_Shift_Name_DB = "Leave";
                            Pre_Abs = "Leave";

                        }
                        else if (INTime == "")
                        {
                            if (OUTTime != "")
                            {
                                ShiftType = "Improper";
                            }
                            Pre_Abs = "Absent";

                        }
                        else if (OUTTime == "")
                        {
                            if (INTime != "")
                            {
                                ShiftType = "Improper";
                            }
                            Pre_Abs = "Absent";

                        }
                        else if (INTime != "" && OUTTime != "")
                        {
                            ShiftType = "Absent";
                            Pre_Abs = "Absent";

                        }
                    }





                    //Week OF Day Check
                    bool Check_Week_Off_DOJ = false;
                    string Attn_Date_Day = DateTime.Parse(Date_Value_Str).DayOfWeek.ToString();
                    if ((Emp_WH_Day).ToUpper() == (Attn_Date_Day).ToUpper())
                    {

                        //'Check DOJ Date to Report Date
                        DateTime Week_Off_Date;
                        DateTime WH_DOJ_Date_Format;

                        //if (((Emp_DS.Rows[intRow]["Wages"].ToString().Trim().ToUpper()) == ("STAFF").ToUpper()) || ((Emp_DS.Rows[intRow]["Wages"].ToString().Trim().ToUpper()) == ("SECURITY").ToUpper()) || ((Emp_DS.Rows[intRow]["Wages"].ToString().Trim().ToUpper()) == ("FITTER & ELECTRICIANS").ToUpper()) || ((Emp_DS.Rows[intRow]["Wages"].ToString().Trim().ToUpper()) == ("DRIVERS").ToUpper()))
                        if ((Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("MONTHLY - STAFF") && Emp_DS.Rows[intRow]["ShiftType"].ToString().ToUpper() == ("GENERAL").ToUpper()) || (Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("WEEKLY").ToUpper() && Emp_DS.Rows[intRow]["ShiftType"].ToString().ToUpper() == ("GENERAL").ToUpper()) || (Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("CLEANING").ToUpper() && Emp_DS.Rows[intRow]["ShiftType"].ToString().ToUpper() == ("GENERAL").ToUpper()) || (Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("MIXING").ToUpper() && Emp_DS.Rows[intRow]["ShiftType"].ToString().ToUpper() == ("GENERAL").ToUpper()) || (Emp_DS.Rows[intRow]["Wages"].ToString().ToUpper() == ("WEEKLY-HINDI").ToUpper() && Emp_DS.Rows[intRow]["ShiftType"].ToString().ToUpper() == ("GENERAL").ToUpper()))
                        {
                            if (DOJ_Date_Str == "")
                            {
                                Week_Off_Date = Convert.ToDateTime(Date_Value_Str); //Report Date
                                WH_DOJ_Date_Format = Convert.ToDateTime(DOJ_Date_Str);
                                if (WH_DOJ_Date_Format <= Week_Off_Date)
                                {
                                    Check_Week_Off_DOJ = true;
                                }
                                else
                                {
                                    Check_Week_Off_DOJ = false;
                                }
                            }
                            else
                            {
                                Check_Week_Off_DOJ = true;
                            }
                        }
                        else
                        {
                            Check_Week_Off_DOJ = true;
                        }

                        Month_WH_Count = Month_WH_Count + 1;
                        if (isPresent == true)
                        {
                            if (NFH_Day_Check == false)
                            {
                                if (halfPresent == "1")
                                {
                                    Present_WH_Count = Convert.ToDecimal(Convert.ToDouble(Present_WH_Count) + (0.5));
                                }
                                else if (halfPresent == "0")
                                {
                                    Present_WH_Count = Present_WH_Count + 1;
                                }
                            }
                        }

                    }

                    if (isPresent == true)
                    {
                        if (halfPresent == "1")
                        {
                            Present_Count = Convert.ToDecimal(Convert.ToDouble(Present_Count) + (0.5));
                        }
                        else if (halfPresent == "0")
                        {
                            Present_Count = Present_Count + 1;
                        }
                    }
                    else
                    {
                        Appsent_Count = Appsent_Count + 1;
                    }
                    //'colIndex += shiftCount
                    //intK += 1

                    DataTable mDataSet = new DataTable();
                    //Update Employee Worked_Days
                    SSQL = "Select * from LogTime_Days where MachineID='" + Get_Machine_ID + "' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And Attn_Date_Str='" + Date_Value_Str + "'";
                    mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (mDataSet.Rows.Count != 0)
                    {
                        SSQL = "Delete from LogTime_Days where MachineID='" + Get_Machine_ID + "' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And Attn_Date_Str='" + Date_Value_Str + "'";
                        objdata.RptEmployeeMultipleDetails(SSQL);
                    }
                    SSQL = "Insert Into LogTime_Days(CompCode,LocCode,MachineID,ExistingCode,FirstName,LastName,DeptName,";
                    SSQL = SSQL + "Designation,DOJ,Wages,Present,Wh_Check,Wh_Count,Wh_Present_Count,Shift,Total_Hrs,Attn_Date_Str,Attn_Date,";
                    SSQL = SSQL + "TimeIN,TimeOUT,Total_Hrs1,TypeName,Present_Absent,Division)";
                    SSQL = SSQL + " Values('" + SessionCcode + "','" + SessionLcode + "','" + Get_Machine_ID + "','" + Emp_DS.Rows[intRow]["ExistingCode"] + "',";
                    SSQL = SSQL + "'" + Emp_DS.Rows[intRow]["FirstName"] + "','" + Emp_DS.Rows[intRow]["LastName"] + "',";
                    SSQL = SSQL + "'" + Emp_DS.Rows[intRow]["DeptName"] + "','" + Emp_DS.Rows[intRow]["Designation"] + "',";
                    SSQL = SSQL + "'" + Emp_DS.Rows[intRow]["DOJ"] + "','" + Emp_DS.Rows[intRow]["Wages"] + "','" + Present_Count + "','" + Check_Week_Off_DOJ + "',";
                    SSQL = SSQL + "'" + Month_WH_Count + "','" + Present_WH_Count + "','" + Employee_Shift_Name_DB + "','" + time_Check_dbl + "','" + Date_Value_Str + "','" + Convert.ToDateTime(Date_Value_Str).ToString("yyyy/MM/dd") + "',";
                    SSQL = SSQL + "'" + INTime + "','" + OUTTime + "','" + Emp_Total_Work_Time_1 + "','" + ShiftType + "','" + Pre_Abs + "','" + Emp_DS.Rows[intRow]["Division"].ToString() + "')";
                    objdata.RptEmployeeMultipleDetails(SSQL);

                    Present_Count = 0;
                    Check_Week_Off_DOJ = false;
                    Month_WH_Count = 0;
                    Present_WH_Count = 0;
                    Employee_Shift_Name_DB = "No Shift";
                    //'Employee_Shift_Name_DB
                    //'Final_Count = Present_Count
                    //'Month_WH_Count
                    //'Present_WH_Count
                    //'time_Check_dbl
                }

                intI += 1;
                Total_Days_Count = 0;
                Final_Count = 0;
                Appsent_Count = 0;
                Present_Count = 0;
                //Month_WH_Count = 0;
                Present_WH_Count = 0;
                NFH_Days_Count = 0;
                NFH_Days_Present_Count = 0;
            }
        }

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        string SS = "";
        DataTable DT = new DataTable();

        SS = "Select Distinct MachineID from Employee_Mst_New_Emp";
        DT = objdata.RptEmployeeMultipleDetails(SS);

        for (int i = 0; i < DT.Rows.Count; i++)
        {
            SS = "Update Employee_Mst_New_Emp set MachineID_Encrypt_New='" + UTF8Encryption(DT.Rows[i]["MachineID"].ToString()) + "'";
            SS = SS + " where MachineID='" + DT.Rows[i]["MachineID"].ToString() + "'";
            objdata.RptEmployeeMultipleDetails(SS);
        }

        SS = "Select Distinct MachineID from Employee_Mst";
        DT = objdata.RptEmployeeMultipleDetails(SS);

        for (int i = 0; i < DT.Rows.Count; i++)
        {
            SS = "Update Employee_Mst set MachineID_Encrypt_New='" + UTF8Encryption(DT.Rows[i]["MachineID"].ToString()) + "'";
            SS = SS + " where MachineID='" + DT.Rows[i]["MachineID"].ToString() + "'";
            objdata.RptEmployeeMultipleDetails(SS);
        }

        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Convertion Completed...');", true);

        //string machineid = "";
        //machineid = UTF8Encryption(txtTokenNumber.Text);
        //txtFromDate.Text = machineid;

    }

    public string OutPunch_Get(string MachineID_Out, string OUT_Date_Str, string OUT_Date_Str1, string Time_Previous_Date)
    {
        string query = "";
        string SSQL = "";
        DataTable mLocalDS_New = new DataTable();
        DataTable mDataSet_New = new DataTable();

        SSQL = "Select MachineID,Max(TimeOUT) as [TimeOUT] from LogTime_OUT where MachineID='" + MachineID_Out + "'";
        SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " And TimeOUT >='" + Convert.ToDateTime(OUT_Date_Str).ToString("yyyy/MM/dd") + " " + "08:00' And TimeOUT <='" + Convert.ToDateTime(OUT_Date_Str1).ToString("yyyy/MM/dd") + " " + "10:00'";
        SSQL = SSQL + " Group By MachineID";
        SSQL = SSQL + " Order By Max(TimeOUT)";
        mLocalDS_New = objdata.RptEmployeeMultipleDetails(SSQL);

        if (mLocalDS_New.Rows.Count == 0)
        {
            query = "";
            return query;
        }
        else
        {
            for (int iTabRow = 0; iTabRow < mLocalDS_New.Rows.Count; iTabRow++)
            {
                string Machine_ID_Check = "";
                string Time_IN_Check_Shift = "";
                string Time_OUT_Check = "";

                Machine_ID_Check = mLocalDS_New.Rows[iTabRow]["MachineID"].ToString();
                SSQL = "Select MachineID,TimeIN from LogTime_IN where MachineID='" + Machine_ID_Check + "' And Compcode='" + SessionCcode + "'";
                SSQL = SSQL + " And LocCode='" + SessionLcode + "'";
                SSQL = SSQL + " And TimeIN >='" + Convert.ToDateTime(OUT_Date_Str).ToString("yyyy/MM/dd") + " " + "02:00'";
                SSQL = SSQL + " And TimeIN <='" + Convert.ToDateTime(OUT_Date_Str1).ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN Desc";
                mDataSet_New = objdata.RptEmployeeMultipleDetails(SSQL);
                if (mDataSet_New.Rows.Count == 0)
                {
                    Time_IN_Check_Shift = "";
                    Time_OUT_Check = mLocalDS_New.Rows[iTabRow]["TimeOUT"].ToString();
                    //Check Night Shift IN Time
                    //2nd Shift Check
                    string Shift_Start_Time = OUT_Date_Str + " " + "06:00";
                    string Shift_End_Time = OUT_Date_Str + " " + "10:00";
                    string Employee_Time = "";
                    DateTime ShiftdateStartIN;
                    DateTime ShiftdateEndIN;
                    DateTime EmpdateIN;

                    Employee_Time = Time_OUT_Check;
                    ShiftdateStartIN = System.Convert.ToDateTime(Shift_Start_Time);
                    ShiftdateEndIN = System.Convert.ToDateTime(Shift_End_Time);
                    EmpdateIN = System.Convert.ToDateTime(Employee_Time);

                    if ((EmpdateIN >= ShiftdateStartIN) && (EmpdateIN <= ShiftdateEndIN))
                    {
                        //Check Previous Day Night Shift In Time
                        SSQL = "Select MachineID,TimeIN from LogTime_IN where MachineID='" + Machine_ID_Check + "' And Compcode='" + SessionCcode + "'";
                        SSQL = SSQL + " And LocCode='" + SessionLcode + "'";
                        SSQL = SSQL + " And TimeIN >='" + Convert.ToDateTime(Time_Previous_Date).ToString("yyyy/MM/dd") + " " + "18:00'";
                        SSQL = SSQL + " And TimeIN <='" + Convert.ToDateTime(OUT_Date_Str).ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN Desc";
                        mDataSet_New = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (mDataSet_New.Rows.Count <= 0)
                        {
                            SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + MachineID_Out + "'";
                            SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                            SSQL = SSQL + " And TimeOUT >='" + Convert.ToDateTime(OUT_Date_Str).ToString("yyyy/MM/dd") + " " + "08:00' And TimeOUT <='" + Convert.ToDateTime(OUT_Date_Str1).ToString("yyyy/MM/dd") + " " + "10:00'";
                            SSQL = SSQL + " Order by TimeOUT";
                            query = SSQL;
                            return query;
                        }
                        else
                        {
                            query = "";
                            return query;
                        }
                    }
                    else
                    {
                        //Check With Next Date
                        Shift_Start_Time = OUT_Date_Str1 + " " + "07:00";
                        Shift_End_Time = OUT_Date_Str1 + " " + "09:45";
                        Employee_Time = Time_OUT_Check;

                        ShiftdateStartIN = System.Convert.ToDateTime(Shift_Start_Time);
                        ShiftdateEndIN = System.Convert.ToDateTime(Shift_End_Time);
                        EmpdateIN = System.Convert.ToDateTime(Employee_Time);

                        if ((EmpdateIN >= ShiftdateStartIN) && (EmpdateIN <= ShiftdateEndIN))
                        {
                            SSQL = "Select MachineID,TimeIN from LogTime_IN where MachineID='" + Machine_ID_Check + "' And Compcode='" + SessionCcode + "'";
                            SSQL = SSQL + " And LocCode='" + SessionLcode + "'";
                            SSQL = SSQL + " And TimeIN >='" + Convert.ToDateTime(OUT_Date_Str1).ToString("yyyy/MM/dd") + " " + "06:00' And TimeIN <='" + Convert.ToDateTime(OUT_Date_Str1).ToString("yyyy/MM/dd") + " " + "09:45' Order by TimeIN Desc";
                            mDataSet_New = objdata.RptEmployeeMultipleDetails(SSQL);
                            if (mDataSet_New.Rows.Count <= 0)
                            {
                                //Check With Current Date Night Shift
                                SSQL = "Select MachineID,TimeIN from LogTime_IN where MachineID='" + Machine_ID_Check + "' And Compcode='" + SessionCcode + "'";
                                SSQL = SSQL + " And LocCode='" + SessionLcode + "'";
                                SSQL = SSQL + " And TimeIN >='" + Convert.ToDateTime(OUT_Date_Str).ToString("yyyy/MM/dd") + " " + "18:00' And TimeIN <='" + Convert.ToDateTime(OUT_Date_Str1).ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN Desc";
                                mDataSet_New = objdata.RptEmployeeMultipleDetails(SSQL);
                                if (mDataSet_New.Rows.Count <= 0)
                                {
                                    SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + MachineID_Out + "'";
                                    SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                    SSQL = SSQL + " And TimeOUT >='" + Convert.ToDateTime(OUT_Date_Str).ToString("yyyy/MM/dd") + " " + "08:00' And TimeOUT <='" + Convert.ToDateTime(OUT_Date_Str1).ToString("yyyy/MM/dd") + " " + "10:00'";
                                    SSQL = SSQL + " Order by TimeOUT";
                                    query = SSQL;
                                    return query;
                                }
                                else
                                {
                                    query = "";
                                    return query;
                                }
                            }
                            else
                            {
                                query = "";
                                return query;
                            }
                        }
                        else
                        {
                            SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + MachineID_Out + "'";
                            SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                            SSQL = SSQL + " And TimeOUT >='" + Convert.ToDateTime(OUT_Date_Str).ToString("yyyy/MM/dd") + " " + "08:00' And TimeOUT <='" + Convert.ToDateTime(OUT_Date_Str1).ToString("yyyy/MM/dd") + " " + "10:00'";
                            SSQL = SSQL + " Order by TimeOUT";
                            query = SSQL;
                            return query;
                        }
                    }

                }
                else
                {
                    query = "";
                    return query;
                }
            }
        }

        return query;
    }


    public string UTF8Encryption(string mvarPlanText)
    {
        string cipherText = "";
        try
        {
            string passPhrase = "Altius";
            string saltValue = "info@altius.co.in";
            string hashAlgorithm = "SHA1";
            string initVector = "@1B2c3D4e5F6g7H8";
            int passwordIterations = 2;
            int keySize = 256;
            byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
            byte[] saltValueBytes = Encoding.ASCII.GetBytes(saltValue);
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(mvarPlanText);
            PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, saltValueBytes, hashAlgorithm, passwordIterations);
            byte[] keyBytes = password.GetBytes(keySize / 8);
            RijndaelManaged symmetricKey = new RijndaelManaged();
            symmetricKey.Mode = CipherMode.CBC;
            ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes);
            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
            cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
            cryptoStream.FlushFinalBlock();
            byte[] cipherTextBytes = memoryStream.ToArray();
            memoryStream.Close();
            cryptoStream.Close();
            cipherText = Convert.ToBase64String(cipherTextBytes);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }

        return cipherText;
    }
}
