﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="LeaveDetails.aspx.cs" Inherits="LeaveDetails" Title="" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">



<script src="assets/js/master_list_jquery.min.js"></script>
<script src="assets/js/master_list_jquery-ui.min.js"></script>
<link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css"/>
 <script>
     $(document).ready(function() {
     $('#example').dataTable();
     $('.datepicker').datepicker({
         format: "dd/mm/yyyy",
         autoclose: true
     });
     });
 </script>
<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
                $('.datepicker').datepicker({
                    format: "dd/mm/yyyy",
                    autoclose: true
                });
            }
        });
    };
</script>
<asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                <ContentTemplate>
<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Manual</a></li>
				<li class="active">Manual Leave</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Manual Leave </h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">Manual Leave</h4>
                        </div>
                        <div class="panel-body">
                        <div class="form-group">
                        <!-- begin row -->
                          <div class="row">
                          <!-- begin col-4 -->
                            <div class="col-md-4">
							   <div class="form-group">
								<label>Existing Code</label>
								<asp:TextBox runat="server" ID="txtExistingCode" class="form-control" 
                                       AutoPostBack="true" ontextchanged="txtExistingCode_TextChanged"></asp:TextBox>
								</div>
                             </div>
                           <!-- end col-4 -->
                           <!-- begin col-4 -->
                           <div class="col-md-4">
                           <div class="form-group">
                            <label>Machine ID</label>
                            <asp:DropDownList runat="server" ID="txtMachineID" class="form-control select2" 
                                   AutoPostBack="true" style="width:100%;" 
                                   onselectedindexchanged="txtMachineID_SelectedIndexChanged">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ControlToValidate="txtMachineID" InitialValue="-Select-" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                           </asp:RequiredFieldValidator>
                            </div>
                           </div>
                           <!-- end col-4 -->
                           
                           <!-- begin col-4 -->
                            <div class="col-md-4">
							   <div class="form-group">
								<label>Employee Name</label>
								<asp:Label runat="server" ID="txtEmpName" class="form-control" BackColor="#F3F3F3"></asp:Label>
								</div>
                             </div>
                           <!-- end col-4 -->
                           
                          </div>
                        <!-- end row -->
                       <!-- begin row -->
                          <div class="row">
                             <!-- begin col-4 -->
                                 <div class="col-md-4">
									<div class="form-group">
										<label>From Date</label>
										<asp:TextBox runat="server" ID="txtFromDate" class="form-control datepicker" 
                                            placeholder="dd/MM/YYYY" AutoPostBack="true" 
                                            ontextchanged="txtFromDate_TextChanged"></asp:TextBox>
										<asp:RequiredFieldValidator ControlToValidate="txtFromDate" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                           </asp:RequiredFieldValidator>
									</div>
                                  </div>
                               <!-- end col-4 -->
                               <!-- begin col-4 -->
                                 <div class="col-md-4">
									<div class="form-group">
										<label>To Date</label>
										<asp:TextBox runat="server" ID="txtToDate" class="form-control datepicker" 
                                            placeholder="dd/MM/YYYY" AutoPostBack="true" 
                                            ontextchanged="txtToDate_TextChanged"></asp:TextBox>
										<asp:RequiredFieldValidator ControlToValidate="txtToDate" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                        </asp:RequiredFieldValidator>
									</div>
                                  </div>
                               <!-- end col-4 -->
                                <!-- begin col-4 -->
                                 <div class="col-md-4">
									<div class="form-group">
										<label>Total Days</label>
										<asp:TextBox runat="server" ID="txtTotDays" Text="0" class="form-control" Enabled="false"></asp:TextBox>
									</div>
                                  </div>
                               <!-- end col-4 -->
                          </div>
                       <!-- end row -->  
                       <!-- begin row -->
                          <div class="row">
                           <!-- begin col-4 -->
                           <div class="col-md-4">
                           <div class="form-group">
                            <label>Leave Type</label>
                            <asp:DropDownList runat="server" ID="ddlLeaveType" class="form-control select2" style="width:100%;">
                            <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
                            <asp:ListItem Value="Casual Leave">Casual Leave</asp:ListItem>
                            <asp:ListItem Value="Long Leave">Long Leave</asp:ListItem>
                            <asp:ListItem Value="Medical Leave">Medical Leave</asp:ListItem>
                            <asp:ListItem Value="Sick Leave">Sick Leave</asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ControlToValidate="ddlLeaveType" InitialValue="-Select-" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                           </asp:RequiredFieldValidator>
                            </div>
                           </div>
                           <!-- end col-4 -->
                          <!-- begin col-4 -->
                                 <div class="col-md-4">
									<div class="form-group">
										<label>Leave Desc</label>
										<asp:TextBox runat="server" ID="txtLeaveDesc" class="form-control" TextMode="MultiLine"></asp:TextBox>
									</div>
                                  </div>
                               <!-- end col-4 -->
                          </div>
                       <!-- end row -->
                        <div class="row">
                         <div class="col-md-4"></div>
                           <!-- begin col-4 -->
                                <div class="col-md-4">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btnSave" Text="Save" class="btn btn-success" 
                                         ValidationGroup="Validate_Field" onclick="btnSave_Click" />
									<asp:Button runat="server" id="btnClear" Text="Clear" class="btn btn-danger" 
                                         onclick="btnClear_Click" />
								 </div>
                               </div>
                              <!-- end col-4 -->
                              <div class="col-md-4"></div>
                         </div>
                        <!-- end row --> 
                          
                          <!-- table start -->
					<div class="col-md-12">
					    <div class="row">
					        <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
			                    <HeaderTemplate>
                                    <table id="example" class="display table">
                                        <thead>
                                            <tr>
                                                <th>Machine ID</th>
                                                <th>Token No</th>
                                                <th>Emp Name</th>
                                                <th>From Date</th>
                                                <th>To Date</th>
                                                <th>Total Days</th>
                                                <th>Leave Status</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                    <td><%# Eval("Machine_No")%></td>
                                    <td><%# Eval("ExistingCode")%></td>
                                    <td><%# Eval("EmpName")%></td>
                                    <td><%# Eval("FromDate")%></td>
                                    <td><%# Eval("ToDate")%></td>
                                    <td><%# Eval("TotalDays")%></td>
                                    <td><%# Eval("LeaveStatus")%></td>
                                   
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater>
					    </div>
					</div>
					<!-- table End -->
                        
                        </div>
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
<!-- end #content -->
</ContentTemplate>
</asp:UpdatePanel>


</asp:Content>

