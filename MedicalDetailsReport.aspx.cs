﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.IO;
using System.Data.SqlClient;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.Collections.Generic;
using System.IO;

public partial class MedicalDetailsReport : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;

    string SSQL;

    string mvarUserType;
    DataTable mDataSet = new DataTable();
    DataTable dt = new DataTable();
    BALDataAccess objdata = new BALDataAccess();
    DataTable AutoDataTable = new DataTable();
    String CurrentYear1;
    static int CurrentYear;
    string RptName;
    string Division;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {



            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Report-Salary Cover Summary Report";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("SalaryProcess"));
                //li.Attributes.Add("class", "droplink active open");

               
            }



        }
    }
    protected void btnDetails_Click(object sender, EventArgs e)
    {
        RptName = "Details";
        ResponseHelper.Redirect("MedicalDetailsDisplay.aspx?FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&RptName=" + RptName, "_blank", "");
    }
    protected void btnAbstract_Click(object sender, EventArgs e)
    {
        RptName = "Abstract";
        ResponseHelper.Redirect("MedicalDetailsDisplay.aspx?FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&RptName=" + RptName, "_blank", "");
    }
    protected void btnConsolidate_Click(object sender, EventArgs e)
    {
        RptName = "Consolidate";
        ResponseHelper.Redirect("MedicalDetailsDisplay.aspx?FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&RptName=" + RptName, "_blank", "");
    }
}
