﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Break_Master : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SSQL;
    string SessionEpay;
    string SessionPayroll;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        SessionEpay = Session["SessionEpay"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Wages Rate";

            LoadWages();

        }
        Load_OLD();
    }

    private void Load_OLD()
    {
        SSQL = "Select * from ["+SessionEpay+"]..MstLunch where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        RepeaterLunch.DataSource= objdata.RptEmployeeMultipleDetails(SSQL);
        RepeaterLunch.DataBind();

        SSQL = "";
        SSQL = "Select * from [" + SessionEpay + "]..MstOT where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        RepeaterOT.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        RepeaterOT.DataBind();

    }

    private void LoadWages()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlWages.Items.Clear();
        ddlWage1.Items.Clear();
        query = "Select *from MstEmployeeType";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlWages.DataSource = dtdsupp;
        ddlWage1.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpType"] = "-Select-";
        dr["EmpType"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlWages.DataTextField = "EmpType";
        ddlWage1.DataTextField = "EmpType";
        ddlWages.DataValueField = "EmpType";
        ddlWage1.DataValueField = "EmpType";
        ddlWages.DataBind();
        ddlWage1.DataBind();

    }

    protected void btnLuch_Click(object sender, EventArgs e)
    {
        if(ddlWages.SelectedItem.Text=="-Select-" | txtLunch.Text == "")
        {
             ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please Check the Lunch Fields!!!');", true);
        }else
        {
            if(btnLuch.Text == "Update")
            {
                btnLuch.Text = "Save";

                SSQL = "";
                SSQL = "Delete from [" + SessionEpay + "]..MstLunch where Wages='" + ddlWages.SelectedItem.Text + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);

                SSQL = "";
                SSQL = "Insert into [" + SessionEpay + "]..MstLunch values('" + SessionCcode + "','" + SessionLcode + "','" + ddlWages.SelectedItem.Text + "','" + txtLunch.Text + "')";
                objdata.RptEmployeeMultipleDetails(SSQL);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Wages Updated Successfully!!!');", true);
                btnLunchClr_Click(sender,e);
                Load_OLD();
            }else
            {
                SSQL = "";
                SSQL = "Insert into [" + SessionEpay + "]..MstLunch values('" + SessionCcode + "','" + SessionLcode + "','" + ddlWages.SelectedItem.Text + "','" + txtLunch.Text + "')";
                objdata.RptEmployeeMultipleDetails(SSQL);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Wages Saved Successfully!!!');", true);
                btnLunchClr_Click(sender, e);
                Load_OLD();
            }
        }
    }

    
    protected void btnLunchClr_Click(object sender, EventArgs e)
    {
        ddlWages.ClearSelection();
        txtLunch.Text = "";
    }

    protected void btnOT_Click(object sender, EventArgs e)
    {
        if (ddlWage1.SelectedItem.Text == "-Select-" | txtOT.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please Check the OT Fields!!!');", true);
        }
        else
        {
            if (btnOT.Text == "Update")
            {
                btnOT.Text = "Save";

                SSQL = "";
                SSQL = "Delete from [" + SessionEpay + "]..MstOT where Wages='" + ddlWage1.SelectedItem.Text + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);

                SSQL = "";
                SSQL = "Insert into [" + SessionEpay + "]..MstOT values('" + SessionCcode + "','" + SessionLcode + "','" + ddlWage1.SelectedItem.Text + "','" + txtOT.Text + "')";
                objdata.RptEmployeeMultipleDetails(SSQL);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Wages Updated Successfully!!!');", true);
                btnOTClr_Click(sender,e);
                Load_OLD();
            }
            else
            {
                SSQL = "";
                SSQL = "Insert into [" + SessionEpay + "]..MstOT values('" + SessionCcode + "','" + SessionLcode + "','" + ddlWage1.SelectedItem.Text + "','" + txtOT.Text + "')";
                objdata.RptEmployeeMultipleDetails(SSQL);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Wages Saved Successfully!!!');", true);
                btnOTClr_Click(sender, e);
                Load_OLD();
            }
        }
    }

    protected void btnOTClr_Click(object sender, EventArgs e)
    {
        ddlWage1.ClearSelection();
        txtOT.Text = "";
    }

    protected void btnApprvEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {
        if (e.CommandArgument.ToString() == "Lunch_Edit")
        {
            SSQL = "Select * from [" + SessionEpay + "]..MstLunch where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and Wages='" + e.CommandName + "'";
            DataTable dt = new DataTable();
            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt.Rows.Count > 0)
            {
                ddlWages.SelectedItem.Text = dt.Rows[0]["Wages"].ToString();
                txtLunch.Text = dt.Rows[0]["Luch"].ToString();
                btnLuch.Text = "Update";
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No records Found!!!');", true);
            }
        }

        if (e.CommandArgument.ToString() == "OT_Edit")
        {
            SSQL = "Select * from [" + SessionEpay + "]..MstOT where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and Wages='" + e.CommandName + "'";
            DataTable dt = new DataTable();
            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt.Rows.Count > 0)
            {
                ddlWage1.SelectedItem.Text = dt.Rows[0]["Wages"].ToString();
                txtOT.Text = dt.Rows[0]["OT"].ToString();
                btnOT.Text = "Update";
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No records Found!!!');", true);
            }
        }
    }

    protected void btnCancelEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {
       
        if(e.CommandArgument.ToString() == "Lunch_Delete")
        {
            SSQL = "";
            SSQL = "Delete from [" + SessionEpay + "]..MstLunch where Wages='" + e.CommandName + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Details Deleted Successfully!!!');", true);
            Load_OLD();
        }

        if (e.CommandArgument.ToString() == "OT_Delete")
        {
            SSQL = "";
            SSQL = "Delete from [" + SessionEpay + "]..MstOT where Wages='" + e.CommandName + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Details Deleted Successfully!!!');", true);
            Load_OLD();
        }
    }
}