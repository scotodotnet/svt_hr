﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PayrollMaster.master" AutoEventWireup="true" CodeFile="Wages_Rate.aspx.cs" Inherits="Wages_Rate" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


<script src="assets/js/master_list_jquery.min.js"></script>
<script src="assets/js/master_list_jquery-ui.min.js"></script>
<link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css"/>
<%--<script src="assets/js/form-wizards.demo.min.js"></script>--%>
 <script type="text/javascript">
     $(document).ready(function() {
         $('#example').dataTable();
      
     });
	</script>
<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.select2').select2();
                $('#example').dataTable();
            }
        });
    };
</script>
<asp:UpdatePanel ID="UpdatePanel5" runat="server">
 <ContentTemplate>
<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Wages Entry</a></li>
				<li class="active">Wages Rate Entry</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Day Wages Salary</h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">Day Wages Salary</h4>
                        </div>
                        <div class="panel-body">
                         <div class="row"> 
			            <div class="col-md-12">
                          <h2 align="center">Day Wages Salary</h2>
                          </div>
                          </div>
                         
                        <!-- begin row -->
			            <div class="row"> 
			            <div class="col-md-2">
			            <div class="form-group">
                          <label>Wages Type</label>
                          <asp:DropDownList ID="ddlWages" runat="server"  class="form-control select2" style="width:100%" OnSelectedIndexChanged="ddlWages_SelectedIndexChanged" AutoPostBack="true">
                          </asp:DropDownList>
                        </div>
			            </div>
			            
			              <div class="row"> 
			             <!-- begin col-3 -->
			            <div class="col-md-1">
			            <div class="form-group">
                          <label>Day1</label>
                          <asp:TextBox ID="txtDay1" runat="server" CssClass="form-control" ></asp:TextBox>
                        </div>
			            </div>
			            
			            <div class="col-md-1">
			            <div class="form-group">
                          <label>Day2</label>
                          <asp:TextBox ID="txtDay2" runat="server" CssClass="form-control" ></asp:TextBox>
                        </div>
			            </div>
			            
			             <div class="col-md-1">
			            <div class="form-group">
                          <label>Day3</label>
                          <asp:TextBox ID="txtDay3" runat="server" CssClass="form-control" ></asp:TextBox>
                        </div>
			            </div>
			              <div class="col-md-1">
			            <div class="form-group">
                          <label>Day4</label>
                          <asp:TextBox ID="txtDay4" runat="server" CssClass="form-control" ></asp:TextBox>
                        </div>
			            </div>
			            <div class="col-md-1">
			            <div class="form-group">
                          <label>Day5</label>
                          <asp:TextBox ID="txtDay5" runat="server" CssClass="form-control" ></asp:TextBox>
                        </div>
			            </div>
			            <div class="col-md-1">
			            <div class="form-group">
                          <label>Day6</label>
                          <asp:TextBox ID="txtDay6" runat="server" CssClass="form-control" ></asp:TextBox>
                        </div>
			            </div>
			             <div class="col-md-1">
			            <div class="form-group">
                          <label>Day7</label>
                          <asp:TextBox ID="txtDay7" runat="server" CssClass="form-control" ></asp:TextBox>
                        </div>
			            </div>
			            <br />
			            <div class="col-md-2">
			            <div align="center" class="form-group">
                            <asp:Button ID="btnSave" runat="server" CssClass="btn btn-success" align="center" OnClick="btnSave_Click" Text="Save" />
                        
                            <asp:Button ID="btnClr" runat="server" CssClass="btn btn-danger" align="center" OnClick="btnClr_Click" Text="Clear" />
                        </div>
			            </div>
			            
                       <!-- end col-3 -->
                        </div>
			            
			            
                        </div>
                     
                 					
					    <div class="row"> 
			            <div class="col-md-12">
                          <h2 align="center">Lunch Amount</h2>
                          </div>
                          </div>
                          
					 <div class="row"> 
			            <div class="col-md-3">
			            <div class="form-group">
                          <label>Wages</label>
                          <asp:DropDownList ID="ddlwageLunch" runat="server"  class="form-control select2" AutoPostBack="true" 
                                style="width:100%" onselectedindexchanged="ddlwageLunch_SelectedIndexChanged">
                          </asp:DropDownList>
                        </div>
			            </div>
			            
			             <div class="col-md-2">
			            <div class="form-group">
                          <label>Lunch Rate</label>
                          <asp:TextBox ID="txtLunch" runat="server" CssClass="form-control" ></asp:TextBox>
                        </div>
			            </div>
			            <br />
			              <div class="col-md-2">
			            <div class="form-group">
                            <asp:Button ID="btnLuch" runat="server" CssClass="btn btn-success"  Text="Save" 
                                onclick="btnLuch_Click" />
                        
                            <asp:Button ID="btnLunchClr" runat="server" CssClass="btn btn-danger" 
                                Text="Clear" onclick="btnLunchClr_Click" />
                        </div>
			            </div>
			            
                        </div>
					
					
					 <div class="row"> 
			            <div class="col-md-12">
                          <h2 align="center">OT Lunch Amount</h2>
                          </div>
                          </div>
					
					
					 <div class="row"> 
			            <div class="col-md-3">
			            <div class="form-group">
                          <label>Wages</label>
                          <asp:DropDownList ID="ddlWageOT" runat="server"  class="form-control select2" AutoPostBack="true" 
                                style="width:100%" onselectedindexchanged="ddlWageOT_SelectedIndexChanged">
                          </asp:DropDownList>
                        </div>
			            </div>
			               <div class="col-md-2">
			            <div class="form-group">
                          <label>OT Rate</label>
                          <asp:TextBox ID="txtOT" runat="server" CssClass="form-control" ></asp:TextBox>
                        </div>
			            </div>
			            <br />
			             <div class="col-md-2">
			            <div class="form-group">
                            <asp:Button ID="btnOT" runat="server" CssClass="btn btn-success"  Text="Save" 
                                onclick="btnOT_Click" />
                        
                            <asp:Button ID="btnOTClr" runat="server" CssClass="btn btn-danger"  Text="Clear" />
                        </div>
			            </div>
			            
                        </div>
                        
                        
                        <div class="row"> 
			            <div class="col-md-12">
                          <h2 align="center">Masthiri Incentive</h2>
                          </div>
                          </div>
                        
                         <div class="col-md-3">
			            <div class="form-group">
                          <label>Wages Type</label>
                          <asp:DropDownList ID="ddlWagesMasthiri" runat="server"  
                                class="form-control select2" style="width:100%"  AutoPostBack="true" 
                                onselectedindexchanged="ddlWagesMasthiri_SelectedIndexChanged">
                          </asp:DropDownList>
                        </div>
			            </div>
			            			        
			            <div class="col-md-2">
			            <div class="form-group">
                          <label>Amount</label>
                          <asp:TextBox ID="txtAmountMasthiri" runat="server" CssClass="form-control" ></asp:TextBox>
                        </div>
			            </div>
			            <br />
			             <div class="col-md-2">
			            <div class="form-group">
                            <asp:Button ID="btnMasthiriSave" runat="server" CssClass="btn btn-success"  
                                Text="Save" onclick="btnMasthiriSave_Click" />
                        
                            <asp:Button ID="btnMasthiriClr" runat="server" CssClass="btn btn-danger"  
                                Text="Clear" onclick="btnMasthiriClr_Click" />
                        </div>
			            </div>
                      
                                         
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
            
         
        </div>
<!-- end #content -->

</ContentTemplate>
</asp:UpdatePanel>


</asp:Content>


