<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="DeptIncentive.aspx.cs" Inherits="DeptIncentive" Title=""  %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


<script src="assets/js/master_list_jquery.min.js"></script>
<script src="assets/js/master_list_jquery-ui.min.js"></script>
<link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
    $(document).ready(function() {
        $('#example').dataTable();

    });
</script>



<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                
                $('.select2').select2();
                $('.datepicker').datepicker({
                    format: "dd/mm/yyyy",
                    autoclose: true
                });
                $('#example').dataTable();
            }
        });
    };
</script>


<asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                <ContentTemplate>
<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Salary Process</a></li>
				<li class="active">Department Incentive</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Department Incentive</h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">Department Incentive</h4>
                        </div>
                        <div class="panel-body">
                        <div class="form-group">
                        <!-- begin row -->
                          <div class="row">
                            <!-- begin col-4 -->
                              <div class="col-md-3">
								<div class="form-group">
								 <label>Month</label>
								 <asp:DropDownList runat="server" ID="ddlMonth" class="form-control select2" 
                                        style="width:100%;" AutoPostBack="true" 
                                        onselectedindexchanged="ddlMonth_SelectedIndexChanged">
								 <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
								 <asp:ListItem Value="Jan">Jan</asp:ListItem>
								 <asp:ListItem Value="Feb">Feb</asp:ListItem>
								 <asp:ListItem Value="Mar">Mar</asp:ListItem>
								 <asp:ListItem Value="Apr">Apr</asp:ListItem>
								 <asp:ListItem Value="May">May</asp:ListItem>
								 <asp:ListItem Value="Jun">Jun</asp:ListItem>
								 <asp:ListItem Value="Jul">Jul</asp:ListItem>
								 <asp:ListItem Value="Aug">Aug</asp:ListItem>
								 <asp:ListItem Value="Sep">Sep</asp:ListItem>
								 <asp:ListItem Value="Oct">Oct</asp:ListItem>
								 <asp:ListItem Value="Nov">Nov</asp:ListItem>
								 <asp:ListItem Value="Dec">Dec</asp:ListItem>
							 	 </asp:DropDownList>
							 	  <asp:RequiredFieldValidator ControlToValidate="ddlMonth" InitialValue="-Select-" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                  </asp:RequiredFieldValidator>
								</div>
                               </div>
                           <!-- end col-4 -->
                             <!-- begin col-4 -->
                              <div class="col-md-3">
								<div class="form-group">
								 <label>Fin. Year</label>
								 <asp:DropDownList runat="server" ID="ddlFinyear" class="form-control select2" 
                                        style="width:100%;" AutoPostBack="true" 
                                        onselectedindexchanged="ddlFinyear_SelectedIndexChanged">
							 	 </asp:DropDownList>
							 	 <asp:RequiredFieldValidator ControlToValidate="ddlFinyear" InitialValue="-Select-" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                  </asp:RequiredFieldValidator>
								</div>
                               </div>
                           <!-- end col-4 -->
                            <!-- begin col-4 -->
                              <div class="col-md-3">
								<div class="form-group">
								 <label>Wages Type</label>
								 <asp:DropDownList runat="server" ID="ddlWages" class="form-control select2" 
                                        style="width:100%;" AutoPostBack="true" onselectedindexchanged="ddlWages_SelectedIndexChanged">
							 	 </asp:DropDownList>
							 	 <asp:RequiredFieldValidator ControlToValidate="ddlWages" InitialValue="-Select-" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                  </asp:RequiredFieldValidator>
								</div>
                               </div>
                           <!-- end col-4 -->
                            <!-- begin col-4 -->
                                <div class="col-md-3">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btnLoadAll" Text="Load All Employee" 
                                         ValidationGroup="Validate_Field" class="btn btn-success" 
                                         onclick="btnLoadAll_Click" />
									
								 </div>
                               </div>
                              <!-- end col-4 -->
                         </div>
                        <!-- end row -->
                       <!-- begin row -->
                          <div class="row">
                            <!-- begin col-4 -->
                           <div class="col-md-4">
                            <div class="form-group">
                           <label>Token No</label>
                           <asp:DropDownList runat="server" ID="ddlTokenNo" class="form-control select2" 
                                    AutoPostBack="true" onselectedindexchanged="ddlTokenNo_SelectedIndexChanged">
                           </asp:DropDownList>
                            </div>
                           </div>
                           <!-- end col-4 -->
                           <!-- begin col-4 -->
                            <div class="col-md-4">
							   <div class="form-group">
								<label>Machine ID</label>
								<asp:Label runat="server" ID="txtMachineID" class="form-control" BackColor="#F3F3F3"></asp:Label>
								</div>
                             </div>
                           <!-- end col-4 -->
                          <!-- begin col-4 -->
                            <div class="col-md-4">
							   <div class="form-group">
								<label>Employee Name</label>
								<asp:Label runat="server" ID="txtEmpName" class="form-control" BackColor="#F3F3F3"></asp:Label>
								</div>
                             </div>
                           <!-- end col-4 -->
                           </div>
                        <!-- end row -->
                        <!-- begin row -->
                          <div class="row">
                          <!-- begin col-4 -->
                                 <div class="col-md-4">
									<div class="form-group">
										<label>Department</label>
								     <asp:Label runat="server" ID="txtDepartment" class="form-control" BackColor="#F3F3F3"></asp:Label>
								    </div>
                                  </div>
                               <!-- end col-4 -->
                              <!-- begin col-4 -->
                                <div class="col-md-3">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btnAdd" Text="ADD" class="btn btn-success" 
                                         onclick="btnAdd_Click" />
									
								 </div>
                               </div>
                              <!-- end col-4 -->
                          </div>
                        <!-- end row -->
                       
                         <!-- begin row -->  
                        <div class="row">
                         <div class="col-md-4"></div>
                           <!-- begin col-4 -->
                                <div class="col-md-4">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btnSave" Text="Save" CssClass="btn btn-success" OnClick="btnSave_Click" />
									
									<asp:Button runat="server" id="btnClear" Text="Clear" class="btn btn-danger" 
                                         onclick="btnClear_Click" />
								 </div>
                               </div>
                              <!-- end col-4 -->
                              <div class="col-md-4"></div>
                         </div>
                        <!-- end row --> 
                        
                        <div class="col-md-12">
                        <div class="row">
                        
                         <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
			                    <HeaderTemplate>
                                    <table id="example" class="display table">
                                        <thead>
                                            <tr>
                                            <th>TokenNo</th>
                                                <th>EmpName</th>
                                                <th>Department</th>
                                                <th>Designation</th>
                                                
                                                
                                                <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>  <td><%# Eval("TokenNo")%></td>
                                        <td><%# Eval("EmpName")%></td>
                                        <td><%# Eval("DeptName")%></td>
                                      <td><%# Eval("Designation")%></td>
                                       
                                        <td>
                                                     <asp:LinkButton ID="btnDeleteEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                        Text="" OnCommand="GridDeleteEnquiryClick" CommandArgument='<%# Eval("MachineID")%>'
                                                                                                          CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Employee details?');">
                                                    </asp:LinkButton>

                                                </td>
                                       
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater>
                        
                        <%--<div style="overflow:scroll">
                        <asp:GridView ID="GVModule" runat="server" AutoGenerateColumns="false"
                         CssClass="gvv display table">
                        <Columns>
                        <asp:TemplateField>
                        <HeaderTemplate>TokenNo</HeaderTemplate>
                        <ItemTemplate>
                        <asp:Label id="TokenNo" runat="server" Text='<%# Eval("TokenNo") %>'/>
                        </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField>
                        <HeaderTemplate>EmpName</HeaderTemplate>
				                            <ItemTemplate>
				                                <asp:Label id="EmpName" runat="server" Text='<%# Eval("EmpName") %>'/>
				                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
				                        <HeaderTemplate>D1</HeaderTemplate>
				                            <ItemTemplate>
				                                <asp:CheckBox id="chkD1" runat="server"  />
				                            </ItemTemplate>
				                        </asp:TemplateField>
				                       <asp:TemplateField>
				                        <HeaderTemplate>D2</HeaderTemplate>
				                            <ItemTemplate>
				                                <asp:CheckBox id="chkD2" runat="server"/>
				                            </ItemTemplate>
				                        </asp:TemplateField>
				                        <asp:TemplateField>
				                        <HeaderTemplate>D3</HeaderTemplate>
				                            <ItemTemplate>
				                                <asp:CheckBox id="chkD3" runat="server"/>
				                            </ItemTemplate>
				                        </asp:TemplateField>
				                        <asp:TemplateField>
				                        <HeaderTemplate>D4</HeaderTemplate>
				                            <ItemTemplate>
				                                <asp:CheckBox id="chkD4" runat="server"/>
				                            </ItemTemplate>
				                        </asp:TemplateField>
				                        <asp:TemplateField>
				                        <HeaderTemplate>D5</HeaderTemplate>
				                            <ItemTemplate>
				                                <asp:CheckBox id="chkD5" runat="server"/>
				                            </ItemTemplate>
				                        </asp:TemplateField>
				                        <asp:TemplateField>
				                        <HeaderTemplate>D6</HeaderTemplate>
				                            <ItemTemplate>
				                                <asp:CheckBox id="chkD6" runat="server"/>
				                            </ItemTemplate>
				                        </asp:TemplateField>
				                        <asp:TemplateField>
				                        <HeaderTemplate>D7</HeaderTemplate>
				                            <ItemTemplate>
				                                <asp:CheckBox id="chkD7" runat="server"/>
				                            </ItemTemplate>
				                        </asp:TemplateField>
				                        <asp:TemplateField>
				                        <HeaderTemplate>D8</HeaderTemplate>
				                            <ItemTemplate>
				                                <asp:CheckBox id="chkD8" runat="server"/>
				                            </ItemTemplate>
				                        </asp:TemplateField>
				                        <asp:TemplateField>
				                        <HeaderTemplate>D9</HeaderTemplate>
				                            <ItemTemplate>
				                                <asp:CheckBox id="chkD9" runat="server"/>
				                            </ItemTemplate>
				                        </asp:TemplateField>
				                        <asp:TemplateField>
				                        <HeaderTemplate>D10</HeaderTemplate>
				                            <ItemTemplate>
				                                <asp:CheckBox id="chkD10" runat="server"/>
				                            </ItemTemplate>
				                        </asp:TemplateField>
				                         <asp:TemplateField>
				                        <HeaderTemplate>D11</HeaderTemplate>
				                            <ItemTemplate>
				                                <asp:CheckBox id="chkD11" runat="server"/>
				                            </ItemTemplate>
				                        </asp:TemplateField>
				                       <asp:TemplateField>
				                        <HeaderTemplate>D12</HeaderTemplate>
				                            <ItemTemplate>
				                                <asp:CheckBox id="chkD12" runat="server"/>
				                            </ItemTemplate>
				                        </asp:TemplateField>
				                        <asp:TemplateField>
				                        <HeaderTemplate>D13</HeaderTemplate>
				                            <ItemTemplate>
				                                <asp:CheckBox id="chkD13" runat="server"/>
				                            </ItemTemplate>
				                        </asp:TemplateField>
				                        <asp:TemplateField>
				                        <HeaderTemplate>D14</HeaderTemplate>
				                            <ItemTemplate>
				                                <asp:CheckBox id="chkD14" runat="server"/>
				                            </ItemTemplate>
				                        </asp:TemplateField>
				                        <asp:TemplateField>
				                        <HeaderTemplate>D15</HeaderTemplate>
				                            <ItemTemplate>
				                                <asp:CheckBox id="chkD15" runat="server"/>
				                            </ItemTemplate>
				                        </asp:TemplateField>
				                        <asp:TemplateField>
				                        <HeaderTemplate>D16</HeaderTemplate>
				                            <ItemTemplate>
				                                <asp:CheckBox id="chkD16" runat="server"/>
				                            </ItemTemplate>
				                        </asp:TemplateField>
				                        <asp:TemplateField>
				                        <HeaderTemplate>D17</HeaderTemplate>
				                            <ItemTemplate>
				                                <asp:CheckBox id="chkD17" runat="server"/>
				                            </ItemTemplate>
				                        </asp:TemplateField>
				                        <asp:TemplateField>
				                        <HeaderTemplate>D18</HeaderTemplate>
				                            <ItemTemplate>
				                                <asp:CheckBox id="chkD18" runat="server"/>
				                            </ItemTemplate>
				                        </asp:TemplateField>
				                        <asp:TemplateField>
				                        <HeaderTemplate>D19</HeaderTemplate>
				                            <ItemTemplate>
				                                <asp:CheckBox id="chkD19" runat="server"/>
				                            </ItemTemplate>
				                        </asp:TemplateField>
				                        <asp:TemplateField>
				                        <HeaderTemplate>D20</HeaderTemplate>
				                            <ItemTemplate>
				                                <asp:CheckBox id="chkD20" runat="server"/>
				                            </ItemTemplate>
				                        </asp:TemplateField>
				                         <asp:TemplateField>
				                        <HeaderTemplate>D21</HeaderTemplate>
				                            <ItemTemplate>
				                                <asp:CheckBox id="chkD21" runat="server"/>
				                            </ItemTemplate>
				                        </asp:TemplateField>
				                       <asp:TemplateField>
				                        <HeaderTemplate>D22</HeaderTemplate>
				                            <ItemTemplate>
				                                <asp:CheckBox id="chkD22" runat="server"/>
				                            </ItemTemplate>
				                        </asp:TemplateField>
				                        <asp:TemplateField>
				                        <HeaderTemplate>D23</HeaderTemplate>
				                            <ItemTemplate>
				                                <asp:CheckBox id="chkD23" runat="server"/>
				                            </ItemTemplate>
				                        </asp:TemplateField>
				                        <asp:TemplateField>
				                        <HeaderTemplate>D24</HeaderTemplate>
				                            <ItemTemplate>
				                                <asp:CheckBox id="chkD24" runat="server"/>
				                            </ItemTemplate>
				                        </asp:TemplateField>
				                        <asp:TemplateField>
				                        <HeaderTemplate>D25</HeaderTemplate>
				                            <ItemTemplate>
				                                <asp:CheckBox id="chkD25" runat="server"/>
				                            </ItemTemplate>
				                        </asp:TemplateField>
				                        <asp:TemplateField>
				                        <HeaderTemplate>D26</HeaderTemplate>
				                            <ItemTemplate>
				                                <asp:CheckBox id="chkD26" runat="server"/>
				                            </ItemTemplate>
				                        </asp:TemplateField>
				                        <asp:TemplateField>
				                        <HeaderTemplate>D27</HeaderTemplate>
				                            <ItemTemplate>
				                                <asp:CheckBox id="chkD27" runat="server"/>
				                            </ItemTemplate>
				                        </asp:TemplateField>
				                        <asp:TemplateField>
				                        <HeaderTemplate>D28</HeaderTemplate>
				                            <ItemTemplate>
				                                <asp:CheckBox id="chkD28" runat="server"/>
				                            </ItemTemplate>
				                        </asp:TemplateField>
				                        <asp:TemplateField>
				                        <HeaderTemplate>D29</HeaderTemplate>
				                            <ItemTemplate>
				                                <asp:CheckBox id="chkD29" runat="server"/>
				                            </ItemTemplate>
				                        </asp:TemplateField>
				                        <asp:TemplateField>
				                        <HeaderTemplate>D30</HeaderTemplate>
				                            <ItemTemplate>
				                                <asp:CheckBox id="chkD30" runat="server"/>
				                            </ItemTemplate>
				                        </asp:TemplateField>
				                        <asp:TemplateField>
				                        <HeaderTemplate>D31</HeaderTemplate>
				                            <ItemTemplate>
				                                <asp:CheckBox id="chkD31" runat="server"/>
				                            </ItemTemplate>
				                        </asp:TemplateField>
				                        <asp:TemplateField>
				                        <HeaderTemplate>TotalDays</HeaderTemplate>
				                            <ItemTemplate>
				                                <asp:Label id="TotalDays" runat="server" Text='<%# Eval("TotalDays") %>' />
				                            </ItemTemplate>
				                        </asp:TemplateField>
				                        <asp:TemplateField>
				                        <HeaderTemplate>MachineID</HeaderTemplate>
				                            <ItemTemplate>
				                                <asp:Label id="MachineID" runat="server" Text='<%# Eval("MachineID") %>' />
				                            </ItemTemplate>
				                        </asp:TemplateField>
                        </Columns>
                        </asp:GridView>
                        </div>--%>
                        
                        </div>
                        </div>
                  
              
                       
                    
                        
             
                        
                        </div>
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
<!-- end #content -->

</ContentTemplate>

</asp:UpdatePanel>


</asp:Content>

