﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class RptPayEmployeeDet : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    System.Globalization.CultureInfo culterInfo = new System.Globalization.CultureInfo("en-GB");
    string SessionAdmin;
    string Stafflabour;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string Emp_ESI_Code;
    string SSQL;
    String CurrentYear1;
    static int CurrentYear;

    System.Web.UI.WebControls.DataGrid grid =
                            new System.Web.UI.WebControls.DataGrid();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        string ss = Session["UserId"].ToString();
        if (SessionAdmin == "1")
        {
           // PanelOnrole.Visible = true;
        }
        else
        {
           // PanelOnrole.Visible = false;
        }
        if (!IsPostBack)
        {
           // DropDwonCategory();
            alldropdownadd();
            //ESICode_load();
            Load_Division_Name();
        }
        //lblComany.Text = SessionCcode + " - " + SessionLcode;
        //lblusername.Text = Session["Usernmdisplay"].ToString();

       
      //  IFUserType();
    }


    private void Load_Division_Name()
    {
        string query = "";
        DataTable DIV_DT = new DataTable();
        query = "Select '0' as Row_Num,'-----Select----' as Division union ";
        query = query + "Select '1' as Row_Num,Division from Division_Master";
        query = query + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        query = query + " Order by Row_Num,Division Asc";
        DIV_DT = objdata.RptEmployeeMultipleDetails(query);
        ddlDivision.DataSource = DIV_DT;
        ddlDivision.DataTextField = "Division";
        ddlDivision.DataValueField = "Division";
        ddlDivision.DataBind();
    }
    public void alldropdownadd()
    {

        //Year Add

        CurrentYear1 = DateTime.Now.Year.ToString();
        CurrentYear = Convert.ToUInt16(CurrentYear1.ToString());
        //if (DateTime.Now.Month >= 1 && DateTime.Now.Month <= 3)
        //{
        //    CurrentYear = CurrentYear - 1;
        //}
        ddlFromYear.Items.Add("----Select----");
        ddlToYear.Items.Add("----Select----");
        ddlMonth.Items.Add("----Select----");
        ddlSortBy.Items.Add("----Select----");
        for (int i = 0; i < 50; i++)
        {

            string tt = CurrentYear1;
            ddlFromYear.Items.Add(tt.ToString());
            ddlToYear.Items.Add(tt.ToString());
            CurrentYear = CurrentYear - 1;
            string cy = Convert.ToString(CurrentYear);
            CurrentYear1 = cy;
        }
        //int currentYear = Utility.GetFinancialYear;
        //DataTable DT_Yr = new DataTable();
        //DT_Yr = objdata.RptEmployeeMultipleDetails("SELECT YEAR(getdate()) as Current_Year");
        //int DB_Current_Year = Convert.ToInt32(DT_Yr.Rows[0][0].ToString());

       
        //for (int i = 1950; i <= DB_Current_Year; i++)
        //{
        //    ddlFromYear.Items.Add(i.ToString());
        //    ddlToYear.Items.Add(i.ToString());
        //    //currentYear = currentYear - 1;
        //}
        //Month Add
        System.Globalization.DateTimeFormatInfo mfi = new
        System.Globalization.DateTimeFormatInfo();
        string strMonthName = "";
        for (int i = 0; i <= 11; i++)
        {
            strMonthName = mfi.GetAbbreviatedMonthName(i + 1).ToString();
            ddlMonth.Items.Add(strMonthName);
        }

        //Qualification
        DataTable dtQuali = new DataTable();
        SSQL = "Select '-----Select----' as Qualification  union   ";
        SSQL =SSQL +" Select Distinct (Qualification) from Employee_Mst where LocCode='"+SessionLcode+"'";
        dtQuali = objdata.RptEmployeeMultipleDetails (SSQL);
        ddlQualification.DataSource = dtQuali;
        ddlQualification.DataTextField = "Qualification";
        ddlQualification.DataValueField = "Qualification";
        ddlQualification.DataBind();

        //Designation
        DataTable dtDesign = new DataTable();

      //  SSQL = "Select '------Select--------' DesignName, 0 as DesignationNo union";
        SSQL = "Select '-----Select----' as Designation, '0' as DesignationNo union  ";
        SSQL = SSQL + " Select DesignName as Designation  ,1 as DesignationNo from Designation_Mst  Order by DesignationNo,Designation ASC";
        dtDesign = objdata.RptEmployeeMultipleDetails(SSQL);
        for (int i = 0; i <= dtDesign.Rows.Count - 1; i++)
        {
            dtDesign.Rows[i][1] = i;
        }
        ddlDesignation.DataSource = dtDesign;
        ddlDesignation.DataTextField = "Designation";
        ddlDesignation.DataValueField = "DesignationNo";
        ddlDesignation.DataBind();

        //Sortby Add
        ddlSortBy.Items.Add("Employee No");
        ddlSortBy.Items.Add("Employee Name");
        ddlSortBy.Items.Add("Machine No");
        ddlSortBy.Items.Add("Exisisting Code");
        ddlSortBy.Items.Add("Date of Joining");
        ddlSortBy.Items.Add("De-Activate Date");
        ddlSortBy.Items.Add("Date of Birth");
        ddlSortBy.Items.Add("Qualification");
        ddlSortBy.Items.Add("Department");
        ddlSortBy.Items.Add("Designation");
        ddlSortBy.Items.Add("Employee Type");


    }
    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dtempty = new DataTable();
        ddlDept.DataSource = dtempty;
        ddlDept.DataBind();
        //griddept.DataSource = dtempty;
        //griddept.DataBind();
        if (ddlRptCategory.SelectedValue == "1")
        {
            Stafflabour = "S";
        }
        else if (ddlRptCategory.SelectedValue == "2")
        {
            Stafflabour = "L";
        }
        else
        {
            Stafflabour = "0";
        }
        DataTable dtDip = new DataTable();
        SSQL = "Select '0' as DeptCode,'-----Select----' as DeptName union ";
        SSQL = SSQL + " Select DeptCode,DeptName  from Department_Mst ";
        dtDip = objdata.RptEmployeeMultipleDetails (SSQL);
        if (dtDip.Rows.Count > 1)
        {
            ddlDept.DataSource = dtDip;
            ddlDept.DataTextField = "DeptName";
            ddlDept.DataValueField = "DeptCode";
            ddlDept.DataBind();
        }
        else
        {
            DataTable dt = new DataTable();
            ddlDept.DataSource = dtempty;
            ddlDept.DataBind();
        }
        //Employee Type
        DataTable dtemp = new DataTable();
        SSQL = "Select '0' as EmpTypeCd,'-----Select----' as EmpType union ";
        SSQL = SSQL + " Select EmpTypeCd,EmpType from MstEmployeeType where EmpCategory='" + ddlRptCategory.SelectedValue + "' ";
        if (SessionUserType == "2")
        {
            SSQL = SSQL + " And EmpType ='CIVIL' ";
        }
        dtemp = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlEmpType.DataSource = dtemp;
        ddlEmpType.DataTextField = "EmpType";
        ddlEmpType.DataValueField = "EmpTypeCd";
        ddlEmpType.DataBind();

        //Clear All DropDown Box
        ddlDept.SelectedIndex = 0;
        ddlFromYear.SelectedIndex = 0;
        ddlToYear.SelectedIndex = 0;
        ddlMonth.SelectedIndex = 0;
        ddlQualification.SelectedIndex = 0;
        ddlDesignation.SelectedIndex = 0;
        ddlEmpType.SelectedIndex = 0;
        ddlSortBy.SelectedIndex = 0;
        //Result_Panel.Visible = false;
    }
    protected void btnBonusView_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;

        string Query_Val = "";


        if (ddlRptCategory.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Employee Category');", true);
            ErrFlag = true;
        }
        //Sabapathy

        if (ddlSortBy.SelectedIndex == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Sort By');", true);
            ErrFlag = true;
        }

        //if (ddlrptReport.SelectedIndex == 0)
        //{
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Export Type');", true);
        //    ErrFlag = true;
        //}





        if (!ErrFlag)
        {
            btnClick_Click(sender, e);

            string attachment = "attachment; filename=DepartmentwiseEmployee.doc";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            StringWriter stw = new StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            if (chkDeActive.Checked == true)
            {
                GridViewdeactiveded.RenderControl(htextw);
            }
            else
            {
                griddept.RenderControl(htextw);
            }
            Response.Write("<table><tr Font-Bold='true'><td colspan='13' align='center'>EMPLOYEE DETAILS</td></tr></table>");
            Response.Write(stw.ToString());
            Response.End();
        }
    }

    protected void btnBonusExcelView_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;

        string Query_Val = "";


        if (ddlRptCategory.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Employee Category');", true);
            ErrFlag = true;
        }
        //Sabapathy

        if (ddlSortBy.SelectedIndex == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Sort By');", true);
            ErrFlag = true;
        }

        //if (ddlrptReport.SelectedIndex == 0)
        //{
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Export Type');", true);
        //     ErrFlag = true;
        //}






        if (!ErrFlag)
        {
            btnClick_Click(sender, e);

            string attachment = "attachment; filename=EmployeeDetailsReport.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";

            StringWriter stw = new StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            if (chkDeActive.Checked == true)
            {
                GridViewdeactiveded.RenderControl(htextw);
            }
            else
            {

                griddept.RenderControl(htextw);
            }
            Response.Write("<table><tr Font-Bold='true'><td colspan='13' align='center'>EMPLOYEE DETAILS</td></tr></table>");
            Response.Write(stw.ToString());

            Response.End();
        }
    }

    protected void btnBonusPDFView_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;

        string Query_Val = "";


        if (ddlRptCategory.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Employee Category');", true);
            ErrFlag = true;
        }
        //Sabapathy

        if (ddlSortBy.SelectedIndex == 0)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Sort By');", true);
            ErrFlag = true;
        }

        //if (ddlrptReport.SelectedIndex == 0)
        //{
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Export Type');", true);
        //     ErrFlag = true;
        //}






        if (!ErrFlag)
        {
            btnClick_Click(sender, e);

            string attachment = "attachment; filename=DepartmentwiseEmployee.pdf";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/pdf";
            StringWriter stw = new StringWriter();

            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            if (chkDeActive.Checked == true)
            {
                GridViewdeactiveded.RenderControl(htextw);
            }
            else
            {
                griddept.RenderControl(htextw);
            }
            Document document = new Document(PageSize.A4, 10f, 10f, 10f, 10f);


            PdfWriter.GetInstance(document, Response.OutputStream);
            document.Open();
            StringReader str = new StringReader(stw.ToString());
            HTMLWorker htmlworker = new HTMLWorker(document);
            htmlworker.Parse(str);
            document.Close();
            Response.Write(document);
            Response.End();
        }
    }

  
    protected void btnClick_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        string Query_Val = "";
        string Category = "0";
        string DOBandDOJ = "";
        string SortFieldName = "";
        string tbl_From_Year = "";
        string tbl_To_Year = "";
        int tbl_Month = 0;
        try
        {
            if (ddlRptCategory.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Employee Category');", true);
                //System.Windows.Forms.MessageBox.Show("Select Employee Category", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            //Sabapathy

            if (ddlSortBy.SelectedIndex == 0)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Sort By');", true);
                //System.Windows.Forms.MessageBox.Show("Select Employee Category", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                //Category
                if (ddlRptCategory.SelectedValue == "1") { Category = "STAFF"; }
                if (ddlRptCategory.SelectedValue == "2") { Category = "LABOUR"; }

                //DateTime currentdate = Convert.ToDateTime(DateTime.Now.ToString("dd/MM/yyyy"));
                //string tdate = currentdate.ToString("dd/MM/yyyy");
              Query_Val = "";
              Query_Val = "Select ED.MachineID,ED.ExistingCode,ED.FirstName";
              Query_Val = Query_Val + " ,ED.Division,ED.SubCatName,convert(varchar,ED.BirthDate,106) as DOB, ";


              Query_Val = Query_Val + "  DATEDIFF(YY, ED.BirthDate, GETDATE()) -  CASE WHEN RIGHT(CONVERT(varchar, GETDATE(), 12), 4) >= ";
              Query_Val = Query_Val + "  RIGHT(CONVERT(varchar, ED.BirthDate, 12), 4) THEN 0 ELSE 1 END AS Age, ";

              //Query_Val = Query_Val + " (case when ED.IsActive = 'Yes' then (convert(varchar(3),DATEDIFF(MONTH, ED.BirthDate,'" + tdate + "')/12) + '.' + convert(varchar(2),DATEDIFF(MONTH, ED.BirthDate, '" + tdate + "') % 12))";
              //Query_Val = Query_Val + " else (convert(varchar(3),DATEDIFF(MONTH, ED.BirthDate, '" + tdate + "')/12) + '.' + convert(varchar(2),DATEDIFF(MONTH, ED.BirthDate, '" + tdate + "') % 12)) end";
              //Query_Val = Query_Val + " ) as Age,";
                
                
              Query_Val = Query_Val + "   ED.Gender,convert(varchar,ED.DOJ,106) as DOJ,ED.DeptName,ED.Designation";
              Query_Val = Query_Val + " ,ED.Qualification,ED.EmployeeMobile,ED.OTEligible,ED.Wages,ED.EmpLevel";
              Query_Val = Query_Val + " ,Case isnull(ED.Eligible_PF,1) When  1 then 'Yes' When  2 then 'No' end as Eligible_PF";
              Query_Val = Query_Val + "  ,ED.PFNo,ED.PFDOJ,Case isnull(ED.Eligible_ESI,1) When  1 then 'Yes' When  2 then 'No' end as Eligible_ESI,ED.ESINo,ED.ESIDOJ";
              Query_Val = Query_Val + " ,ED.UAN,ED.PF_Code,ED.ESICode,ED.RoomNo,ED.Vehicles_Type as Vehicle_Type,ED.BusRoute,ED.BusNo";
              Query_Val = Query_Val + ", convert(varchar,ED.DOR,106) as Deactivedate,ED.Reason,ED.WeekOff,ED.FormIObtained";
              Query_Val = Query_Val + " ,Case isnull(ED.Salary_Through,1) When  1 then 'Cash' When  2 then 'Bank' Else '' End as Salarythrough";
              Query_Val = Query_Val + "  ,ED.BankName,ED.IFSC_Code,ED.BranchCode,ED.AccountNo";
              Query_Val = Query_Val + " ,ED.BaseSalary,ED.VPF,ED.MaritalStatus,ED.Nationality,ED.Religion,ED.Height,ED.Weight,ED.Community";
              Query_Val = Query_Val + " ,ED.Handicapped,ED.Handicapped_Reason,ED.BloodGroup,ED.RecuritmentThro,ED.RecuriterName";
              Query_Val = Query_Val + ",ED.RecuritmentUnit,ED.RecutersMob,ED.ExistingEmpNo,ED.ExistingEmpName,ED.ReferalType,ED.AgentName";
              Query_Val = Query_Val + ",ED.RefParentsName,ED.RefParentsMobile";
              Query_Val = Query_Val + ",ED.CommAmount as CommAmt,ED.LeaveFrom,ED.LeaveTo,ED.Festival1,ED.Days1,ED.LeaveFrom2,ED.LeaveTo2,ED.Festival2,ED.Days2";
              Query_Val = Query_Val + ",ED.Certificate,ED.Nominee,ED.NomineeRelation,ED.FamilyDetails as FatherName,ED.ParentsPhone";
              Query_Val = Query_Val + ",ED.MotnerName as MotherName,ED.parentsMobile as ParentsMobile,ED.GuardianName,ED.GuardianMobile";
              Query_Val = Query_Val + ",ED.Address1,ED.Taluk_Perm,ED.Permanent_Dist,ED.StateName,ED.OtherState";
              Query_Val = Query_Val + ",ED.Taluk_Present,ED.Present_Dist,ED.Address2";
              Query_Val = Query_Val + ",ED.IDMark1,ED.IDMark2";
              Query_Val = Query_Val + ",'' as Adhar_Card ,''as Voter_Card,'' as Ration_Card,'' as Pan_Card,'' as Driving_Licence,'' as Smart_Card ";
              Query_Val = Query_Val + ",'' as Bank_Pass_Book,'' as Passport  from Employee_Mst ED inner join MstEmployeeType ME on ED.Wages=ME.EmpType";
              Query_Val = Query_Val + " Where ED.CatName = '" + Category + "' and ED.CompCode='" + SessionCcode + "'";
              Query_Val = Query_Val + " and ED.LocCode='" + SessionLcode + "'";

                if (SessionUserType == "2")
                {
                    Query_Val = Query_Val + " and ME.EmpTypeCd !='5' and ED.Eligible_PF='1'";
                }



                //Gender
                if (rbtngender.SelectedIndex == 1) { Query_Val = Query_Val + " and ED.Gender='Male'"; }
                if (rbtngender.SelectedIndex == 2) { Query_Val = Query_Val + " and ED.Gender='Female'"; }
                //PF GRADE
                //if (rppfgrade.SelectedIndex == 1) { Query_Val = Query_Val + " and ED.NonPFGrade='2'"; }
                //if (rppfgrade.SelectedIndex == 2) { Query_Val = Query_Val + " and ED.NonPFGrade='1'"; }
                //Department
                if (ddlDept.SelectedIndex != 0) { Query_Val = Query_Val + " and ED.DeptName = '" + ddlDept.SelectedItem.Text.ToString() + "'"; }
                //Date of Joining and Date of Birth
                tbl_From_Year = ddlFromYear.SelectedItem.Text.ToString();
                tbl_To_Year = ddlToYear.SelectedItem.Text.ToString();
                tbl_Month = ddlMonth.SelectedIndex;
                if (RdbDOJDOB.SelectedIndex == 0) { DOBandDOJ = "ED.DOJ"; }
                if (RdbDOJDOB.SelectedIndex == 1) { DOBandDOJ = "ED.BirthDate"; }
                if (DOBandDOJ != "")
                {
                    if (ddlFromYear.SelectedIndex != 0 && ddlToYear.SelectedIndex != 0) { Query_Val = Query_Val + " and Year(" + DOBandDOJ + ") >= '" + tbl_From_Year + "' And Year(" + DOBandDOJ + ") <= '" + tbl_To_Year + "'"; }
                    if (ddlFromYear.SelectedIndex != 0 && ddlMonth.SelectedIndex != 0) { Query_Val = Query_Val + " and Year(" + DOBandDOJ + ") = '" + tbl_From_Year + "' And Month(" + DOBandDOJ + ") = '" + tbl_Month + "'"; }
                    if (ddlFromYear.SelectedIndex != 0 && ddlMonth.SelectedIndex == 0 && ddlToYear.SelectedIndex == 0) { Query_Val = Query_Val + " and Year(" + DOBandDOJ + ") = '" + tbl_From_Year + "'"; }
                    if (ddlFromYear.SelectedIndex == 0 && ddlMonth.SelectedIndex != 0 && ddlToYear.SelectedIndex == 0) { Query_Val = Query_Val + " and Month(" + DOBandDOJ + ") = '" + tbl_Month + "'"; }
                }
                //Qualification
                if (ddlQualification.SelectedIndex != 0) { Query_Val = Query_Val + " and ED.Qualification = '" + ddlQualification.SelectedItem.Text.ToString() + "'"; }
                //Designation
                if (ddlDesignation.SelectedIndex != 0) { Query_Val = Query_Val + " and ED.Designation = '" + ddlDesignation.SelectedItem.Text.ToString() + "'"; }
                //Employee Type
                if (ddlEmpType.SelectedIndex != 0) { Query_Val = Query_Val + " and ME.EmpType = '" + ddlEmpType.SelectedItem.Text.ToString() + "'"; }

                //Active Check
                if (chkDeActive.Checked == false) { Query_Val = Query_Val + " and ED.IsActive = 'Yes'"; }

                //Deactive Check
                if (chkDeActive.Checked == true) { Query_Val = Query_Val + " and ED.IsActive = 'No'"; }

                //Add Division Condition
                if (ddlDivision.Text.ToString() != "" && ddlDivision.Text.ToString() != "-----Select----")
                {
                    Query_Val = Query_Val + " and ED.Division='" + ddlDivision.Text.ToString() + "'";
                }

                //Deactive Date Check
                if (chkDeActive.Checked == true)
                {

                    string Deactive_From_date = txtDeActFrom.Text.ToString();
                    string Deactive_To_Date = txtDeActTo.Text.ToString();
                    DateTime Deactive_From_Date_DB_Format;
                    DateTime Deactive_To_Date_DB_Format;
                    if (Deactive_From_date.ToString() != "" && Deactive_To_Date.ToString() != "")
                    {
                        Deactive_From_Date_DB_Format = DateTime.ParseExact(txtDeActFrom.Text, "dd-MM-yyyy", null);
                        Deactive_To_Date_DB_Format = DateTime.ParseExact(txtDeActTo.Text, "dd-MM-yyyy", null);
                        Query_Val = Query_Val + " and ED.DOR >= convert(datetime, '" + Deactive_From_Date_DB_Format + "', 105)" +
                        " and ED.DOR <= convert(datetime, '" + Deactive_To_Date_DB_Format + "', 105)";
                    }
                }


                //Set Order by
                SortFieldName = "";
                if (ddlSortBy.SelectedValue == "Employee No") { SortFieldName = "ED.EmpNo"; }
                if (ddlSortBy.SelectedValue == "Employee Name") { SortFieldName = "ED.FirstName"; }
                if (ddlSortBy.SelectedValue == "Machine No") { SortFieldName = "ED.MachineID"; }
                if (ddlSortBy.SelectedValue == "Exisisting Code") { SortFieldName = "ED.ExistingCode"; }
                if (ddlSortBy.SelectedValue == "Date of Joining") { SortFieldName = "ED.DOJ"; }
                if (ddlSortBy.SelectedValue == "De-Activate Date") { SortFieldName = "ED.DOR"; }
                if (ddlSortBy.SelectedValue == "Date of Birth") { SortFieldName = "ED.BirthDate"; }
                if (ddlSortBy.SelectedValue == "Qualification") { SortFieldName = "ED.Qualification"; }
                if (ddlSortBy.SelectedValue == "Department") { SortFieldName = "ED.DeptCode"; }
                if (ddlSortBy.SelectedValue == "Designation") { SortFieldName = "ED.Designation"; }
                if (ddlSortBy.SelectedValue == "Employee Type") { SortFieldName = "ME.EmpType"; }
                //Order by
                if (SortFieldName != "") { Query_Val = Query_Val + " Order by " + SortFieldName + " Asc"; }


                //Final Result
                //Result_Panel.Visible = true;
                DataTable dt = new DataTable();
                DataTable dt1 = new DataTable();
                DataTable dt2 = new DataTable(); 

                dt = objdata.RptEmployeeMultipleDetails(Query_Val);
                string doctype = "";

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    

                        //string s =  dt.Rows[i]["DOB"].ToString(); 
                        //DateTime dob = Convert.ToDateTime(s);
                        //DateTime currentdate = Convert.ToDateTime(DateTime.Now);
                        //TimeSpan time = currentdate.Subtract(dob);
                        //int total = (time.Days) / 365;
                        //string age= total.ToString();
                        //dt.Rows[i]["Age"] = age;
                    


                    Query_Val = "Select Distinct DocType  from Employee_Doc_Mst ";
                    dt1 = objdata.RptEmployeeMultipleDetails(Query_Val);
                    for (int iRow = 0; iRow < dt1.Rows.Count; iRow++)
                    {
                        doctype = dt1.Rows[iRow]["DocType"].ToString();
                        Query_Val = "Select * from Employee_Doc_Mst where CompCode='"+SessionCcode +"' and LocCode='"+SessionLcode +"'  ";
                        Query_Val = Query_Val + " and DocType='"+ doctype +"' and EmpNo='"+dt.Rows[i]["MachineID"].ToString()+"' ";
                        dt2 = objdata.RptEmployeeMultipleDetails(Query_Val);
                        if (dt2.Rows.Count != 0)
                        {
                            if(doctype =="Adhar Card")
                            {
                                dt.Rows[i]["Adhar_Card"]=dt2.Rows[0]["DocNo"].ToString();
                            }
                            if(doctype =="Voter Card")
                            {
                                dt.Rows[i]["Voter_Card"]=dt2.Rows[0]["DocNo"].ToString();
                            }
                            if(doctype =="Ration Card")
                            {
                                dt.Rows[i]["Ration_Card"]=dt2.Rows[0]["DocNo"].ToString();
                            }
                            if(doctype =="Pan Card")
                            {
                                dt.Rows[i]["Pan_Card"]=dt2.Rows[0]["DocNo"].ToString();
                            }
                            if(doctype =="Driving Licence")
                            {
                                dt.Rows[i]["Driving_Licence"]=dt2.Rows[0]["DocNo"].ToString();
                            }
                            if(doctype =="Smart Card")
                            {
                                dt.Rows[i]["Smart_Card"]=dt2.Rows[0]["DocNo"].ToString();
                            }
                            if(doctype =="Bank Pass Book")
                            {
                                dt.Rows[i]["Bank_Pass_Book"]=dt2.Rows[0]["DocNo"].ToString();
                            }
                             if(doctype =="Passport")
                            {
                                dt.Rows[i]["Passport"]=dt2.Rows[0]["DocNo"].ToString();
                            }
                        }

                    }
                }
                griddept.DataSource = dt;
                griddept.DataBind();
                GridViewdeactiveded.DataSource = dt;
                GridViewdeactiveded.DataBind();
            }
            //Sabapathy END



        }
        catch (Exception ex)
        { }
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
    
}
