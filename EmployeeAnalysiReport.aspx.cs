﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;
public partial class EmployeeAnalysiReport : System.Web.UI.Page
{
    string ShiftType1 = "";
    string Date = "";
    string Status = "";


    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string ss = "";

    ReportDocument report = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();

   
    string Division;
    string Date1_str;
    string Date2_str;
   


    string SSQL = "";

    
    int shiftCount;
    DateTime fromdate;
    DateTime todate;
    int dayCount;
    System.DateTime iDate;
    int daysAdded = 0;
   
    
    int grand;
    DataTable mLocalDS = new DataTable();
    

    DataTable AutoDTable = new DataTable();
    DataTable DataCell = new DataTable();
    DataTable dt = new DataTable();

   
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Employee Analysis Report";
            }
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
           
            Division = Request.QueryString["Division"].ToString();
            Date1_str = Request.QueryString["FromDate"].ToString();
            Date2_str = Request.QueryString["ToDate"].ToString();
            Status = Request.QueryString["Status"].ToString();
            LoadData();
        }
    }

    private void LoadData()
    {
        SSQL = "";
        SSQL = "Select MachineID,FirstName,IsActive,OTEligible,CatName,StdWrkHrs,Wages,DeptName,WeekOff from Employee_Mst where IsActive='Yes' and CompCode='" + SessionCcode+"' and LocCode='"+SessionLcode+"'";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt.Rows.Count > 0)
        {

            AutoDTable.Columns.Add("MachineID");
            AutoDTable.Columns.Add("Dept");
            AutoDTable.Columns.Add("Name");
            //AutoDTable.Columns.Add("TimeIN");
            //AutoDTable.Columns.Add("TimeOUT");
            AutoDTable.Columns.Add("Improper");
            AutoDTable.Columns.Add("ShiftMismatch");
            AutoDTable.Columns.Add("Absent");
            AutoDTable.Columns.Add("Present");
            AutoDTable.Columns.Add("TotalShift");
            //AutoDTable.Columns.Add("TotalHrs");
            AutoDTable.Columns.Add("OTHrs");


            //For Ramalinga Spinning mills By Selva
            //STAFF
            string STAFFImproper_Count = "0";
            string STAFFMismatch_Count = "0";
            string STAFFAbsent_Count = "0";
            string STAFFPresent_Count = "0";
            string STAFFTotalOT = "0";

            //MONTHLY
            string MONTHLYImproper_Count = "0";
            string MONTHLYMismatch_Count = "0";
            string MONTHLYAbsent_Count = "0";
            string MONTHLYPresent_Count = "0";
            string MONTHLYTotalOT = "0";

            //MONTHLY II
            string MONTHLYIIImproper_Count = "0";
            string MONTHLYIIMismatch_Count = "0";
            string MONTHLYIIAbsent_Count = "0";
            string MONTHLYIIPresent_Count = "0";
            string MONTHLYIITotalOT = "0";

            //MONTHLY I
            string MONTHLYIImproper_Count = "0";
            string MONTHLYIMismatch_Count = "0";
            string MONTHLYIAbsent_Count = "0";
            string MONTHLYIPresent_Count = "0";
            string MONTHLYITotalOT = "0";

            //FN NB
            string FNNBImproper_Count = "0";
            string FNNBMismatch_Count = "0";
            string FNNBAbsent_Count = "0";
            string FNNBPresent_Count = "0";
            string FNNBTotalOT = "0";

            //For Thenpandi Spinning mill data

            //string TBImproper_Count = "0";
            //string TBMismatch_Count = "0";
            //string TBAbsent_Count = "0";
            //string TBPresent_Count = "0";
            //string TBTotalOT = "0";

            //string TGImproper_Count = "0";
            //string TGMismatch_Count = "0";
            //string TGAbsent_Count = "0";
            //string TGPresent_Count = "0";
            //string TGTotalOT = "0";

            //string HBImproper_Count = "0";
            //string HBMismatch_Count = "0";
            //string HBAbsent_Count = "0";
            //string HBPresent_Count = "0";
            //string HBTotalOT = "0";

            //string HGImproper_Count = "0";
            //string HGMismatch_Count = "0";
            //string HGAbsent_Count = "0";
            //string HGPresent_Count = "0";
            //string HGTotalOT = "0";

            //For Thenpandi OT
            //int TBOT = 0, TGOT = 0, HBOT = 0, HGOT = 0;
            //int TAB = 0, TMis = 0, TOT = 0, Timp = 0, TP = 0;

            //For Ramalinga OT
            int STAFFOT = 0, MOT = 0, MIIOT = 0, MIOT = 0, FNNBOT = 0;

            int TAB = 0, TMis = 0, TOT = 0, Timp = 0, TP = 0;


            string OThours = "0"; //Edit by Narmatha
            int OTPRESENT = 0;
            string OTEligible = "";//By Selva
            string IsActive = "";//By Selva

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string staffwages = "";
                string Wages = "";

                int Absent = 0;
                int Present = 0;
                int Imp = 0;
                int Mis = 0;
                int Shift = 0;
                int OT = 0;


                string Machineid = "";
                string Deptname = "";
                string workinghours = "";
                string stdwrkhrs = "";

                

                DataTable Attn_Check_DT = new DataTable();

                SSQL = "";
                SSQL = "Select DeptName,Wages,Wh_Present_Count,Shift,Total_Hrs,Attn_Date_Str,TypeName,Present_Absent from LogTime_Days";
                SSQL = SSQL + " where MachineID='" + dt.Rows[i]["MachineID"] + "'";// and Convert(datetime,Attn_Date,103)>=Convert(datetime,"+Convert.ToDateTime(Date1_str.ToString()).ToString()+",103)";
                // SSQL = SSQL + " and Convert(datetime,Attn_Date,103)<=Convert(datetime,"+Convert.ToDateTime(Date2_str.ToString())+",103)";
                SSQL = SSQL + " And CONVERT(DATETIME, Attn_Date_Str,103)>= CONVERT(DATETIME, '" + Convert.ToDateTime(Date1_str).ToString("dd/MM/yyyy") + "', 103)";
                SSQL = SSQL + " And CONVERT(DATETIME, Attn_Date_Str,103)<= CONVERT(DATETIME, '" + Convert.ToDateTime(Date2_str).ToString("dd/MM/yyyy") + "', 103)";
                SSQL = SSQL + " and CompCode='" + SessionCcode + "' ANd LocCode='" + SessionLcode + "' order by Attn_Date ASC";
                Attn_Check_DT = objdata.RptEmployeeMultipleDetails(SSQL);

                AutoDTable.NewRow();
                AutoDTable.Rows.Add();

                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["MachineID"] = dt.Rows[i]["MachineID"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Name"] = dt.Rows[i]["FirstName"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Dept"] = dt.Rows[i]["DeptName"].ToString();


                if (Attn_Check_DT.Rows.Count > 0)
                {

                    for (int j = 0; j < Attn_Check_DT.Rows.Count; j++)
                    {
                        Machineid = dt.Rows[i]["MachineID"].ToString();
                        staffwages = dt.Rows[i]["CatName"].ToString();
                        Deptname = dt.Rows[i]["DeptName"].ToString();
                        stdwrkhrs = dt.Rows[i]["StdWrkHrs"].ToString();
                        Wages = dt.Rows[i]["Wages"].ToString();

                        OTEligible = dt.Rows[i]["OTEligible"].ToString();//By Selva
                        IsActive = dt.Rows[i]["IsActive"].ToString();//By Selva
                        workinghours = Attn_Check_DT.Rows[j]["Total_Hrs"].ToString();

                        if (stdwrkhrs == "" || stdwrkhrs == "0")
                        {
                            stdwrkhrs = "8";
                        }

                        if (Machineid == "503")
                        {
                            Machineid = "503";
                        }
                        if (i == 141)
                        {
                            var ij =1;
                        }

                        if (Attn_Check_DT.Rows[j]["Present_Absent"].ToString() == "Leave")
                        {
                            if (dt.Rows[i]["WeekOff"].ToString() == (Convert.ToDateTime(Attn_Check_DT.Rows[j]["Attn_Date_Str"].ToString()).DayOfWeek.ToString()).ToString())
                            {
                                //AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Absent"] = "WeekOff";
                                //AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Present"] = "-";
                                //AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TimeIN"] = "-";
                                //AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TimeOUT"] = "-";
                                //AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Improper"] = "-";
                                //AutoDTable.Rows[AutoDTable.Rows.Count - 1]["ShiftMismatch"] = "-";
                                //AutoDTable.Rows[AutoDTable.Rows.Count - 1]["WeekOff"] = "-";
                                //AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TotalHrs"] = "0";
                                //AutoDTable.Rows[AutoDTable.Rows.Count - 1]["OTHrs"] = "0";

                                //For Thenpandi
                                //if (dt.Rows[i]["Wages"].ToString() == "TAMIL BOYS")
                                //{
                                //    TBAbsent_Count = (Convert.ToInt32(TBAbsent_Count) + Convert.ToInt32(1)).ToString();
                                //}
                                //if (dt.Rows[i]["Wages"].ToString() == "TAMIL GIRLS")
                                //{
                                //    TGAbsent_Count = (Convert.ToInt32(TGAbsent_Count) + Convert.ToInt32(1)).ToString();
                                //}
                                //if (dt.Rows[i]["Wages"].ToString() == "HINDI BOYS")
                                //{
                                //    HBAbsent_Count = (Convert.ToInt32(HBAbsent_Count) + Convert.ToInt32(1)).ToString();
                                //}
                                //if (dt.Rows[i]["Wages"].ToString() == "HINDI GIRLS")
                                //{
                                //    HGAbsent_Count = (Convert.ToInt32(HGAbsent_Count) + Convert.ToInt32(1)).ToString();
                                //}


                                //For Ramalinga
                                if (dt.Rows[i]["Wages"].ToString() == "STAFF")
                                {
                                    STAFFAbsent_Count = (Convert.ToInt32(STAFFAbsent_Count) + Convert.ToInt32(1)).ToString();
                                }
                                if (dt.Rows[i]["Wages"].ToString() == "MONTHLY")
                                {
                                    MONTHLYAbsent_Count = (Convert.ToInt32(MONTHLYAbsent_Count) + Convert.ToInt32(1)).ToString();
                                }
                                if (dt.Rows[i]["Wages"].ToString() == "MONTHLY II")
                                {
                                    MONTHLYIIAbsent_Count = (Convert.ToInt32(MONTHLYIIAbsent_Count) + Convert.ToInt32(1)).ToString();
                                }
                                if (dt.Rows[i]["Wages"].ToString() == "MONTHLY I")
                                {
                                    MONTHLYIAbsent_Count = (Convert.ToInt32(MONTHLYIAbsent_Count) + Convert.ToInt32(1)).ToString();
                                }
                                if (dt.Rows[i]["Wages"].ToString() == "FN NB")
                                {
                                    FNNBAbsent_Count = (Convert.ToInt32(FNNBAbsent_Count) + Convert.ToInt32(1)).ToString();
                                }

                            }
                            else
                            {
                                //AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Absent"] = "Absent";
                                //AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Present"] = "-";
                                //AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TimeIN"] = "-";
                                //AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TimeOUT"] = "-";
                                //AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Improper"] = "-";
                                //AutoDTable.Rows[AutoDTable.Rows.Count - 1]["ShiftMismatch"] = "-";
                                //AutoDTable.Rows[AutoDTable.Rows.Count - 1]["WeekOff"] = "-";
                                //AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TotalHrs"] = "0";
                                //AutoDTable.Rows[AutoDTable.Rows.Count - 1]["OTHrs"] = "0";

                                //For Thenpandi
                                //if (dt.Rows[i]["Wages"].ToString() == "TAMIL BOYS")
                                //{
                                //    TBAbsent_Count = (Convert.ToInt32(TBAbsent_Count) + Convert.ToInt32(1)).ToString();
                                //}
                                //if (dt.Rows[i]["Wages"].ToString() == "TAMIL GIRLS")
                                //{
                                //    TGAbsent_Count = (Convert.ToInt32(TGAbsent_Count) + Convert.ToInt32(1)).ToString();
                                //}
                                //if (dt.Rows[i]["Wages"].ToString() == "HINDI BOYS")
                                //{
                                //    HBAbsent_Count = (Convert.ToInt32(HBAbsent_Count) + Convert.ToInt32(1)).ToString();
                                //}
                                //if (dt.Rows[i]["Wages"].ToString() == "HINDI GIRLS")
                                //{
                                //    HGAbsent_Count = (Convert.ToInt32(HGAbsent_Count) + Convert.ToInt32(1)).ToString();
                                //}


                                //For Ramalinga
                                if (dt.Rows[i]["Wages"].ToString() == "STAFF")
                                {
                                    STAFFAbsent_Count = (Convert.ToInt32(STAFFAbsent_Count) + Convert.ToInt32(1)).ToString();
                                }
                                if (dt.Rows[i]["Wages"].ToString() == "MONTHLY")
                                {
                                    MONTHLYAbsent_Count = (Convert.ToInt32(MONTHLYAbsent_Count) + Convert.ToInt32(1)).ToString();
                                }
                                if (dt.Rows[i]["Wages"].ToString() == "MONTHLY II")
                                {
                                    MONTHLYIIAbsent_Count = (Convert.ToInt32(MONTHLYIIAbsent_Count) + Convert.ToInt32(1)).ToString();
                                }
                                if (dt.Rows[i]["Wages"].ToString() == "MONTHLY I")
                                {
                                    MONTHLYIAbsent_Count = (Convert.ToInt32(MONTHLYIAbsent_Count) + Convert.ToInt32(1)).ToString();
                                }
                                if (dt.Rows[i]["Wages"].ToString() == "FN NB")
                                {
                                    FNNBAbsent_Count = (Convert.ToInt32(FNNBAbsent_Count) + Convert.ToInt32(1)).ToString();
                                }
                            }
                            Absent = Absent + 1;
                        }
                        else
                        {
                            if (Attn_Check_DT.Rows[j]["TypeName"].ToString() == "Improper")
                            {
                                //AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Improper"] = "Improper";
                                //AutoDTable.Rows[AutoDTable.Rows.Count - 1]["ShiftMismatch"] = "-";
                                //AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Absent"] = "-";
                                //AutoDTable.Rows[AutoDTable.Rows.Count - 1]["WeekOff"] = dt.Rows[i]["Wh_Present_Count"].ToString();
                                //AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Present"] = "-";
                                //AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TimeIN"] = dt.Rows[i]["TimeIN"].ToString();
                                //AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TimeOUT"] = dt.Rows[i]["TimeOUT"].ToString();
                                //AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TotalHrs"] = "0";
                                //AutoDTable.Rows[AutoDTable.Rows.Count - 1]["OTHrs"] = "0";

                                //For Thenpandi
                                //if (dt.Rows[i]["Wages"].ToString() == "TAMIL BOYS")
                                //{
                                //    TBImproper_Count = (Convert.ToInt32(TBImproper_Count) + Convert.ToInt32(1)).ToString();
                                //    TBPresent_Count = (Convert.ToInt32(TBPresent_Count) + Convert.ToInt32(1)).ToString();
                                //}
                                //if (dt.Rows[i]["Wages"].ToString() == "TAMIL GIRLS")
                                //{
                                //    TGImproper_Count = (Convert.ToInt32(TGImproper_Count) + Convert.ToInt32(1)).ToString();
                                //    TGPresent_Count = (Convert.ToInt32(TGPresent_Count) + Convert.ToInt32(1)).ToString();
                                //}
                                //if (dt.Rows[i]["Wages"].ToString() == "HINDI BOYS")
                                //{
                                //    HBImproper_Count = (Convert.ToInt32(HBImproper_Count) + Convert.ToInt32(1)).ToString();
                                //    HBPresent_Count = (Convert.ToInt32(HBPresent_Count) + Convert.ToInt32(1)).ToString();
                                //}
                                //if (dt.Rows[i]["Wages"].ToString() == "HINDI GIRLS")
                                //{
                                //    HGImproper_Count = (Convert.ToInt32(HGImproper_Count) + Convert.ToInt32(1)).ToString();
                                //    HGPresent_Count = (Convert.ToInt32(HGPresent_Count) + Convert.ToInt32(1)).ToString();
                                //}


                                //For Ramalinga
                                if (dt.Rows[i]["Wages"].ToString() == "STAFF")
                                {
                                    STAFFImproper_Count = (Convert.ToInt32(STAFFImproper_Count) + Convert.ToInt32(1)).ToString();
                                    STAFFPresent_Count = (Convert.ToInt32(STAFFPresent_Count) + Convert.ToInt32(1)).ToString();
                                }
                                if (dt.Rows[i]["Wages"].ToString() == "MONTHLY")
                                {
                                    MONTHLYImproper_Count = (Convert.ToInt32(MONTHLYImproper_Count) + Convert.ToInt32(1)).ToString();
                                    MONTHLYPresent_Count = (Convert.ToInt32(MONTHLYPresent_Count) + Convert.ToInt32(1)).ToString();
                                }
                                if (dt.Rows[i]["Wages"].ToString() == "MONTHLY II")
                                {
                                    MONTHLYIIImproper_Count = (Convert.ToInt32(MONTHLYIIImproper_Count) + Convert.ToInt32(1)).ToString();
                                    MONTHLYIIPresent_Count = (Convert.ToInt32(MONTHLYIIPresent_Count) + Convert.ToInt32(1)).ToString();
                                }
                                if (dt.Rows[i]["Wages"].ToString() == "MONTHLY I")
                                {
                                    MONTHLYIImproper_Count = (Convert.ToInt32(MONTHLYIImproper_Count) + Convert.ToInt32(1)).ToString();
                                    MONTHLYIPresent_Count = (Convert.ToInt32(MONTHLYIPresent_Count) + Convert.ToInt32(1)).ToString();
                                }
                                if (dt.Rows[i]["Wages"].ToString() == "FN NB")
                                {
                                    FNNBImproper_Count = (Convert.ToInt32(FNNBImproper_Count) + Convert.ToInt32(1)).ToString();
                                    FNNBPresent_Count = (Convert.ToInt32(FNNBPresent_Count) + Convert.ToInt32(1)).ToString();
                                }
                                Present = Present + 1;
                                Imp = Imp + 1;
                            }
                            else if (Attn_Check_DT.Rows[j]["TypeName"].ToString() == "Mismatch")
                            {
                                //AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Improper"] = "Mismatch";
                                //AutoDTable.Rows[AutoDTable.Rows.Count - 1]["ShiftMismatch"] = "1";
                                //AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Absent"] = "-";
                                //AutoDTable.Rows[AutoDTable.Rows.Count - 1]["WeekOff"] = dt.Rows[i]["Wh_Present_Count"].ToString();
                                //AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Present"] = dt.Rows[i]["Shift"].ToString();
                                //AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TimeIN"] = dt.Rows[i]["TimeIN"].ToString();
                                //AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TimeOUT"] = dt.Rows[i]["TimeOUT"].ToString();

                                Present = Present + 1;
                                Mis = Mis + 1;

                                if (staffwages == "STAFF" || Wages == "FITTER & ELECTRICIANS" || Wages == "SECURITY" || Wages == "DRIVERS")
                                {
                                    if (dt.Rows[i]["OTEligible"].ToString().Trim().ToUpper() == "1")
                                    {
                                        //OT salary for Labour by Narmatha
                                        string workinghours_New = (Convert.ToDecimal(workinghours)).ToString();
                                        decimal check = 0;
                                        check = Convert.ToDecimal(workinghours.ToString());
                                        check = (Math.Round(check, 0, MidpointRounding.AwayFromZero));
                                        //By Selva v
                                        if (Convert.ToDecimal(check) >= Convert.ToDecimal(stdwrkhrs))
                                        {
                                            OThours = (Convert.ToDecimal(workinghours_New) - Convert.ToDecimal(stdwrkhrs)).ToString();
                                            workinghours = (Convert.ToDecimal(workinghours_New) - Convert.ToDecimal(OThours)).ToString();
                                            OThours = Math.Round(Convert.ToDecimal(OThours), 0, MidpointRounding.AwayFromZero).ToString();

                                            OT = OT + Convert.ToInt32(OThours);
                                        }
                                    }
                                }
                                else
                                {
                                    string workinghours_New = "0";
                                    workinghours_New = (Convert.ToDecimal(workinghours)).ToString();

                                    //By Selva v
                                    if ((OTEligible == "1" && IsActive == "Yes") && (Convert.ToDecimal(workinghours_New) >= 8))
                                    {

                                        OThours = (Convert.ToDecimal(workinghours_New) - Convert.ToDecimal(stdwrkhrs)).ToString();
                                        workinghours = (Convert.ToDecimal(workinghours_New) - Convert.ToDecimal(OThours)).ToString();
                                        OThours = Math.Round(Convert.ToDecimal(OThours), 0, MidpointRounding.AwayFromZero).ToString();
                                        OT = OT + Convert.ToInt32(OThours);


                                    }
                                    //By Selva ^

                                    if (Attn_Check_DT.Rows[j]["Shift"].ToString().Trim().ToUpper() == "SHIFT3")
                                    {
                                        //By Selva v
                                        if ((OTEligible == "1" && IsActive == "Yes") && (Convert.ToDecimal(workinghours_New) >= 7))
                                        {
                                            OThours = (Convert.ToDecimal(workinghours_New) - Convert.ToDecimal(7)).ToString();
                                            workinghours = (Convert.ToDecimal(workinghours_New) - Convert.ToDecimal(OThours)).ToString();
                                            //AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TotalHrs"] = workinghours_New;
                                            OThours = Math.Round(Convert.ToDecimal(OThours), 0, MidpointRounding.AwayFromZero).ToString();
                                            //AutoDTable.Rows[AutoDTable.Rows.Count - 1]["OTHrs"] = OThours;
                                            OT = OT + Convert.ToInt32(OThours);


                                        }
                                        //By Selva ^
                                        //else
                                        //{
                                        //    OThours = "0";
                                        //    workinghours = workinghours_New.ToString();
                                        //    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TotalHrs"] = workinghours_New;
                                        //    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["OTHrs"] = "0";
                                        //}
                                    }
                                }

                                //For Thenpandi
                                //if (dt.Rows[i]["Wages"].ToString() == "TAMIL BOYS")
                                //{
                                //    TBMismatch_Count = (Convert.ToInt32(TBMismatch_Count) + Convert.ToInt32(1)).ToString();
                                //    TBPresent_Count = (Convert.ToInt32(TBPresent_Count) + Convert.ToInt32(1)).ToString();
                                //    TBTotalOT = (Convert.ToInt32(TBTotalOT) + Convert.ToInt32(OThours)).ToString();
                                //    if (Convert.ToInt32(OThours) > 0)
                                //    {
                                //        TBOT = TBOT + 1;
                                //    }
                                //}
                                //if (dt.Rows[i]["Wages"].ToString() == "TAMIL GIRLS")
                                //{
                                //    TGMismatch_Count = (Convert.ToInt32(TGMismatch_Count) + Convert.ToInt32(1)).ToString();
                                //    TGPresent_Count = (Convert.ToInt32(TGPresent_Count) + Convert.ToInt32(1)).ToString();
                                //    OThours = Math.Round(Convert.ToDecimal(OThours), 0, MidpointRounding.AwayFromZero).ToString();
                                //    TGTotalOT = (Convert.ToInt32(TGTotalOT) + Convert.ToInt32(OThours)).ToString();
                                //    if (Convert.ToInt32(OThours) > 0)
                                //    {
                                //        TGOT = TGOT + 1;
                                //    }
                                //}
                                //if (dt.Rows[i]["Wages"].ToString() == "HINDI BOYS")
                                //{
                                //    HBMismatch_Count = (Convert.ToInt32(HBMismatch_Count) + Convert.ToInt32(1)).ToString();
                                //    HBPresent_Count = (Convert.ToInt32(HBPresent_Count) + Convert.ToInt32(1)).ToString();
                                //    OThours = Math.Round(Convert.ToDecimal(OThours), 0, MidpointRounding.AwayFromZero).ToString();
                                //    HBTotalOT = (Convert.ToInt32(HBTotalOT) + Convert.ToInt32(OThours)).ToString();
                                //    if (Convert.ToInt32(OThours) > 0)
                                //    {
                                //        HBOT = HBOT + 1;
                                //    }
                                //}
                                //if (dt.Rows[i]["Wages"].ToString() == "HINDI GIRLS")
                                //{
                                //    HGMismatch_Count = (Convert.ToInt32(HGMismatch_Count) + Convert.ToInt32(1)).ToString();
                                //    HGPresent_Count = (Convert.ToInt32(HGPresent_Count) + Convert.ToInt32(1)).ToString();
                                //    OThours = Math.Round(Convert.ToDecimal(OThours), 0, MidpointRounding.AwayFromZero).ToString();
                                //    HGTotalOT = (Convert.ToInt32(HGTotalOT) + Convert.ToInt32(OThours)).ToString();
                                //    if (Convert.ToInt32(OThours) > 0)
                                //    {
                                //        HGOT = HGOT + 1;
                                //    }
                                //}


                                //For Ramalinga
                                if (dt.Rows[i]["Wages"].ToString() == "STAFF")
                                {
                                    STAFFMismatch_Count = (Convert.ToInt32(STAFFMismatch_Count) + Convert.ToInt32(1)).ToString();
                                    STAFFPresent_Count = (Convert.ToInt32(STAFFPresent_Count) + Convert.ToInt32(1)).ToString();
                                    STAFFTotalOT = (Convert.ToInt32(STAFFTotalOT) + Convert.ToInt32(OThours)).ToString();
                                    if (Convert.ToInt32(OThours) > 0)
                                    {
                                        STAFFOT = STAFFOT + 1;
                                    }
                                }
                                if (dt.Rows[i]["Wages"].ToString() == "MONTHLY")
                                {
                                    MONTHLYMismatch_Count = (Convert.ToInt32(MONTHLYMismatch_Count) + Convert.ToInt32(1)).ToString();
                                    MONTHLYPresent_Count = (Convert.ToInt32(MONTHLYPresent_Count) + Convert.ToInt32(1)).ToString();
                                    OThours = Math.Round(Convert.ToDecimal(OThours), 0, MidpointRounding.AwayFromZero).ToString();
                                    MONTHLYTotalOT = (Convert.ToInt32(MONTHLYTotalOT) + Convert.ToInt32(OThours)).ToString();
                                    if (Convert.ToInt32(OThours) > 0)
                                    {
                                        MOT = MOT + 1;
                                    }
                                }
                                if (dt.Rows[i]["Wages"].ToString() == "MONTHLY II")
                                {
                                    MONTHLYIIMismatch_Count = (Convert.ToInt32(MONTHLYIIMismatch_Count) + Convert.ToInt32(1)).ToString();
                                    MONTHLYIIPresent_Count = (Convert.ToInt32(MONTHLYIIPresent_Count) + Convert.ToInt32(1)).ToString();
                                    OThours = Math.Round(Convert.ToDecimal(OThours), 0, MidpointRounding.AwayFromZero).ToString();
                                    MONTHLYIITotalOT = (Convert.ToInt32(MONTHLYIITotalOT) + Convert.ToInt32(OThours)).ToString();
                                    if (Convert.ToInt32(OThours) > 0)
                                    {
                                        MIIOT = MIIOT + 1;
                                    }
                                }
                                if (dt.Rows[i]["Wages"].ToString() == "MONTHLY I")
                                {
                                    MONTHLYIMismatch_Count = (Convert.ToInt32(MONTHLYIMismatch_Count) + Convert.ToInt32(1)).ToString();
                                    MONTHLYIPresent_Count = (Convert.ToInt32(MONTHLYIPresent_Count) + Convert.ToInt32(1)).ToString();
                                    OThours = Math.Round(Convert.ToDecimal(OThours), 0, MidpointRounding.AwayFromZero).ToString();
                                    MONTHLYITotalOT = (Convert.ToInt32(MONTHLYITotalOT) + Convert.ToInt32(OThours)).ToString();
                                    if (Convert.ToInt32(OThours) > 0)
                                    {
                                        MIOT = MIOT + 1;
                                    }
                                }
                                if (dt.Rows[i]["Wages"].ToString() == "FN NB")
                                {
                                    FNNBMismatch_Count = (Convert.ToInt32(FNNBMismatch_Count) + Convert.ToInt32(1)).ToString();
                                    FNNBPresent_Count = (Convert.ToInt32(FNNBPresent_Count) + Convert.ToInt32(1)).ToString();
                                    OThours = Math.Round(Convert.ToDecimal(OThours), 0, MidpointRounding.AwayFromZero).ToString();
                                    FNNBTotalOT = (Convert.ToInt32(FNNBTotalOT) + Convert.ToInt32(OThours)).ToString();
                                    if (Convert.ToInt32(OThours) > 0)
                                    {
                                        FNNBOT = FNNBOT + 1;
                                    }
                                }
                            }
                            else
                            {

                                Present = Present + 1;
                                Shift = Shift + 1;

                                //AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Improper"] = "-";
                                //AutoDTable.Rows[AutoDTable.Rows.Count - 1]["ShiftMismatch"] = "-";
                                //AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Absent"] = "-";
                                //AutoDTable.Rows[AutoDTable.Rows.Count - 1]["WeekOff"] = dt.Rows[i]["Wh_Present_Count"].ToString();
                                //AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Present"] = dt.Rows[i]["Shift"].ToString();
                                //AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TimeIN"] = dt.Rows[i]["TimeIN"].ToString();
                                //AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TimeOUT"] = dt.Rows[i]["TimeOUT"].ToString();

                                if (staffwages == "STAFF" || Wages == "FITTER & ELECTRICIANS" || Wages == "SECURITY" || Wages == "DRIVERS")
                                {
                                    if (dt.Rows[i]["OTEligible"].ToString().Trim().ToUpper() == "1")
                                    {
                                        //OT salary for Labour by Narmatha
                                        string workinghours_New = (Convert.ToDecimal(workinghours)).ToString();
                                        decimal check = 0;
                                        check = Convert.ToDecimal(workinghours.ToString());
                                        check = (Math.Round(check, 0, MidpointRounding.AwayFromZero));
                                        //By Selva v
                                        if (Convert.ToDecimal(check) >= Convert.ToDecimal(stdwrkhrs))
                                        {
                                            OThours = (Convert.ToDecimal(workinghours_New) - Convert.ToDecimal(stdwrkhrs)).ToString();
                                            workinghours = (Convert.ToDecimal(workinghours_New) - Convert.ToDecimal(OThours)).ToString();
                                            OThours = Math.Round(Convert.ToDecimal(OThours), 0, MidpointRounding.AwayFromZero).ToString();
                                            //AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TotalHrs"] = workinghours_New;
                                            //AutoDTable.Rows[AutoDTable.Rows.Count - 1]["OTHrs"] = OThours;
                                            OT = OT + Convert.ToInt32(OThours);
                                        }
                                    }
                                }
                                else
                                {
                                    string workinghours_New = "0";
                                    workinghours_New = (Convert.ToDecimal(workinghours)).ToString();

                                    //By Selva v
                                    if ((OTEligible == "1" && IsActive == "Yes") && (Convert.ToDecimal(workinghours_New) >= 8))
                                    {
                                        OThours = (Convert.ToDecimal(workinghours_New) - Convert.ToDecimal(stdwrkhrs)).ToString();
                                        workinghours = (Convert.ToDecimal(workinghours_New) - Convert.ToDecimal(OThours)).ToString();
                                        // AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TotalHrs"] = workinghours_New;
                                        OThours = Math.Round(Convert.ToDecimal(OThours), 0, MidpointRounding.AwayFromZero).ToString();
                                        // AutoDTable.Rows[AutoDTable.Rows.Count - 1]["OTHrs"] = OThours;
                                        OT = OT + Convert.ToInt32(OThours);

                                    }
                                    //By Selva ^
                                    //else
                                    //{
                                    //    OThours = "0";
                                    //    workinghours = Convert.ToDecimal(workinghours_New).ToString();/// - Convert.ToDecimal(OThours)).ToString();
                                    //    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TotalHrs"] = workinghours_New;
                                    //    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["OTHrs"] = "0";
                                    //}

                                    if (Attn_Check_DT.Rows[j]["Shift"].ToString().Trim().ToUpper() == "SHIFT3")
                                    {
                                        //By Selva v
                                        if ((OTEligible == "1" && IsActive == "Yes") && (Convert.ToDecimal(workinghours_New) >= 7))
                                        {
                                            OThours = (Convert.ToDecimal(workinghours_New) - Convert.ToDecimal(7)).ToString();
                                            workinghours = (Convert.ToDecimal(workinghours_New) - Convert.ToDecimal(OThours)).ToString();
                                            //AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TotalHrs"] = workinghours_New;
                                            OThours = Math.Round(Convert.ToDecimal(OThours), 0, MidpointRounding.AwayFromZero).ToString();
                                            //AutoDTable.Rows[AutoDTable.Rows.Count - 1]["OTHrs"] = OThours;
                                            OT = OT + Convert.ToInt32(OThours);
                                        }
                                        //By Selva ^
                                        //else

                                    }
                                }

                                //For Thenpandi
                                //if (dt.Rows[i]["Wages"].ToString() == "TAMIL BOYS")
                                //{

                                //    TBPresent_Count = (Convert.ToInt32(TBPresent_Count) + Convert.ToInt32(1)).ToString();
                                //    OThours = Math.Round(Convert.ToDecimal(OThours), 0, MidpointRounding.AwayFromZero).ToString();
                                //    TBTotalOT = (Convert.ToInt32(TBTotalOT) + Convert.ToInt32(OThours)).ToString();
                                //    if (Convert.ToInt32(OThours) > 0)
                                //    {
                                //        TBOT = TBOT + 1;
                                //    }
                                //}
                                //if (dt.Rows[i]["Wages"].ToString() == "TAMIL GIRLS")
                                //{

                                //    TGPresent_Count = (Convert.ToInt32(TGPresent_Count) + Convert.ToInt32(1)).ToString();
                                //    OThours = Math.Round(Convert.ToDecimal(OThours), 0, MidpointRounding.AwayFromZero).ToString();
                                //    TGTotalOT = (Convert.ToInt32(TGTotalOT) + Convert.ToInt32(OThours)).ToString();
                                //    if (Convert.ToInt32(OThours) > 0)
                                //    {
                                //        TGOT = TGOT + 1;
                                //    }
                                //}
                                //if (dt.Rows[i]["Wages"].ToString() == "HINDI BOYS")
                                //{

                                //    HBPresent_Count = (Convert.ToInt32(HBPresent_Count) + Convert.ToInt32(1)).ToString();
                                //    OThours = Math.Round(Convert.ToDecimal(OThours), 0, MidpointRounding.AwayFromZero).ToString();
                                //    HBTotalOT = (Convert.ToInt32(HBTotalOT) + Convert.ToInt32(OThours)).ToString();
                                //    if (Convert.ToInt32(OThours) > 0)
                                //    {
                                //        HBOT = HBOT + 1;
                                //    }
                                //}
                                //if (dt.Rows[i]["Wages"].ToString() == "HINDI GIRLS")
                                //{

                                //    HGPresent_Count = (Convert.ToInt32(HGPresent_Count) + Convert.ToInt32(1)).ToString();
                                //    OThours = Math.Round(Convert.ToDecimal(OThours), 0, MidpointRounding.AwayFromZero).ToString();
                                //    HGTotalOT = (Convert.ToInt32(HGTotalOT) + Convert.ToInt32(OThours)).ToString();
                                //    if (Convert.ToInt32(OThours) > 0)
                                //    {
                                //        HGOT = HGOT + 1;
                                //    }
                                //}

                                //For Ramalinga
                                if (dt.Rows[i]["Wages"].ToString() == "STAFF")
                                {

                                    STAFFPresent_Count = (Convert.ToInt32(STAFFPresent_Count) + Convert.ToInt32(1)).ToString();
                                    OThours = Math.Round(Convert.ToDecimal(OThours), 0, MidpointRounding.AwayFromZero).ToString();
                                    STAFFTotalOT = (Convert.ToInt32(STAFFTotalOT) + Convert.ToInt32(OThours)).ToString();
                                    if (Convert.ToInt32(OThours) > 0)
                                    {
                                        STAFFOT = STAFFOT + 1;
                                    }
                                }
                                if (dt.Rows[i]["Wages"].ToString() == "MONTHLY")
                                {

                                    MONTHLYPresent_Count = (Convert.ToInt32(MONTHLYPresent_Count) + Convert.ToInt32(1)).ToString();
                                    OThours = Math.Round(Convert.ToDecimal(OThours), 0, MidpointRounding.AwayFromZero).ToString();
                                    MONTHLYTotalOT = (Convert.ToInt32(MONTHLYTotalOT) + Convert.ToInt32(OThours)).ToString();
                                    if (Convert.ToInt32(OThours) > 0)
                                    {
                                        MOT = MOT + 1;
                                    }
                                }
                                if (dt.Rows[i]["Wages"].ToString() == "MONTHLY II")
                                {

                                    MONTHLYIIPresent_Count = (Convert.ToInt32(MONTHLYIIPresent_Count) + Convert.ToInt32(1)).ToString();
                                    OThours = Math.Round(Convert.ToDecimal(OThours), 0, MidpointRounding.AwayFromZero).ToString();
                                    MONTHLYIITotalOT = (Convert.ToInt32(MONTHLYIITotalOT) + Convert.ToInt32(OThours)).ToString();
                                    if (Convert.ToInt32(OThours) > 0)
                                    {
                                        MIIOT = MIIOT + 1;
                                    }
                                }
                                if (dt.Rows[i]["Wages"].ToString() == "MONTHLY I")
                                {

                                    MONTHLYIPresent_Count = (Convert.ToInt32(MONTHLYIPresent_Count) + Convert.ToInt32(1)).ToString();
                                    OThours = Math.Round(Convert.ToDecimal(OThours), 0, MidpointRounding.AwayFromZero).ToString();
                                    MONTHLYITotalOT = (Convert.ToInt32(MONTHLYITotalOT) + Convert.ToInt32(OThours)).ToString();
                                    if (Convert.ToInt32(OThours) > 0)
                                    {
                                        MIOT = MIOT + 1;
                                    }
                                }
                                if (dt.Rows[i]["Wages"].ToString() == "FN NB")
                                {
                                    FNNBPresent_Count = (Convert.ToInt32(FNNBPresent_Count) + Convert.ToInt32(1)).ToString();
                                    OThours = Math.Round(Convert.ToDecimal(OThours), 0, MidpointRounding.AwayFromZero).ToString();
                                    FNNBTotalOT = (Convert.ToInt32(FNNBTotalOT) + Convert.ToInt32(OThours)).ToString();
                                    if (Convert.ToInt32(OThours) > 0)
                                    {
                                        FNNBOT = FNNBOT + 1;
                                    }
                                }
                            }
                        }
                    }
                }
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Improper"] = Imp.ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["ShiftMismatch"] = Mis.ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Absent"] = Absent.ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Present"] = Present.ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TotalShift"] = Shift.ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["OTHrs"] = OT.ToString();

                TOT = Convert.ToInt32(STAFFOT) + Convert.ToInt32(MONTHLYTotalOT) + Convert.ToInt32(MONTHLYIITotalOT) + Convert.ToInt32(MONTHLYITotalOT) + Convert.ToInt32(FNNBTotalOT);
                TP = Convert.ToInt32(STAFFPresent_Count) + Convert.ToInt32(MONTHLYPresent_Count) + Convert.ToInt32(MONTHLYIIPresent_Count) + Convert.ToInt32(MONTHLYIPresent_Count) + Convert.ToInt32(FNNBPresent_Count);
                TAB = Convert.ToInt32(STAFFAbsent_Count) + Convert.ToInt32(MONTHLYAbsent_Count) + Convert.ToInt32(MONTHLYIIAbsent_Count) + Convert.ToInt32(MONTHLYIAbsent_Count) + Convert.ToInt32(FNNBAbsent_Count);
                Timp = Convert.ToInt32(STAFFImproper_Count) + Convert.ToInt32(MONTHLYImproper_Count) + Convert.ToInt32(MONTHLYIIImproper_Count) + Convert.ToInt32(MONTHLYIImproper_Count) + Convert.ToInt32(FNNBImproper_Count);
                TMis = Convert.ToInt32(STAFFMismatch_Count) + Convert.ToInt32(MONTHLYMismatch_Count) + Convert.ToInt32(MONTHLYIIMismatch_Count) + Convert.ToInt32(MONTHLYIMismatch_Count) + Convert.ToInt32(FNNBMismatch_Count);
                OTPRESENT = STAFFOT + MOT + MIIOT + MIOT + FNNBOT;
            }
            
            if (AutoDTable.Rows.Count > 0)
            {
                report.Load(Server.MapPath("crystal/EmployeeAnalysisReport.rpt"));

                report.DataDefinition.FormulaFields["FromDT"].Text = "'" + Date1_str + "'";
                report.DataDefinition.FormulaFields["ToDT"].Text = "'" + Date2_str + "'";


                //For Thenpandi
                //report.DataDefinition.FormulaFields["TBImproper"].Text = "'" + TBImproper_Count + "'";
                //report.DataDefinition.FormulaFields["TBMismatch"].Text = "'" + TBMismatch_Count + "'";
                //report.DataDefinition.FormulaFields["TBAbsent"].Text = "'" + TBAbsent_Count + "'";
                //report.DataDefinition.FormulaFields["TBPresent"].Text = "'" + TBOT + "'";
                //report.DataDefinition.FormulaFields["TBTotalOT"].Text = "'" + TBTotalOT + "'";

                //report.DataDefinition.FormulaFields["TGImproper"].Text = "'" + TGImproper_Count + "'";
                //report.DataDefinition.FormulaFields["TGMismatch"].Text = "'" + TGMismatch_Count + "'";
                //report.DataDefinition.FormulaFields["TGAbsent"].Text = "'" + TGAbsent_Count + "'";
                //report.DataDefinition.FormulaFields["TGPresent"].Text = "'" + TGOT + "'";
                //report.DataDefinition.FormulaFields["TGTotalOT"].Text = "'" + TGTotalOT + "'";

                //report.DataDefinition.FormulaFields["HBImproper"].Text = "'" + HBImproper_Count + "'";
                //report.DataDefinition.FormulaFields["HBMismatch"].Text = "'" + HBMismatch_Count + "'";
                //report.DataDefinition.FormulaFields["HBAbsent"].Text = "'" + HBAbsent_Count + "'";
                //report.DataDefinition.FormulaFields["HBPresent"].Text = "'" + HBOT + "'";
                //report.DataDefinition.FormulaFields["HBTotalOT"].Text = "'" + HBTotalOT + "'";

                //report.DataDefinition.FormulaFields["HGImproper"].Text = "'" + HGImproper_Count + "'";
                //report.DataDefinition.FormulaFields["HGMismatch"].Text = "'" + HGMismatch_Count + "'";
                //report.DataDefinition.FormulaFields["HGAbsent"].Text = "'" + HGAbsent_Count + "'";
                //report.DataDefinition.FormulaFields["HGPresent"].Text = "'" + HGOT + "'";
                //report.DataDefinition.FormulaFields["HGTotalOT"].Text = "'" + HGTotalOT + "'";

                //For Ramalinga
                report.DataDefinition.FormulaFields["STAFFImproper"].Text = "'" + STAFFImproper_Count + "'";
                report.DataDefinition.FormulaFields["STAFFMismatch"].Text = "'" + STAFFMismatch_Count + "'";
                report.DataDefinition.FormulaFields["STAFFAbsent"].Text = "'" + STAFFAbsent_Count + "'";
                report.DataDefinition.FormulaFields["STAFFPresent"].Text = "'" + STAFFOT + "'";
                report.DataDefinition.FormulaFields["STAFFTotalOT"].Text = "'" + STAFFTotalOT + "'";

                report.DataDefinition.FormulaFields["MONTHLYImproper"].Text = "'" + MONTHLYImproper_Count + "'";
                report.DataDefinition.FormulaFields["MONTHLYMismatch"].Text = "'" + MONTHLYMismatch_Count + "'";
                report.DataDefinition.FormulaFields["MONTHLYAbsent"].Text = "'" + MONTHLYAbsent_Count + "'";
                report.DataDefinition.FormulaFields["MONTHLYPresent"].Text = "'" + MOT + "'";
                report.DataDefinition.FormulaFields["MONTHLYTotalOT"].Text = "'" + MONTHLYTotalOT + "'";

                report.DataDefinition.FormulaFields["MONTHLYIIImproper"].Text = "'" + MONTHLYIIImproper_Count + "'";
                report.DataDefinition.FormulaFields["MONTHLYIIMismatch"].Text = "'" + MONTHLYIIMismatch_Count + "'";
                report.DataDefinition.FormulaFields["MONTHLYIIAbsent"].Text = "'" + MONTHLYIIAbsent_Count + "'";
                report.DataDefinition.FormulaFields["MONTHLYIIPresent"].Text = "'" + MIIOT + "'";
                report.DataDefinition.FormulaFields["MONTHLYIITotalOT"].Text = "'" + MONTHLYIITotalOT + "'";

                report.DataDefinition.FormulaFields["MONTHLYIImproper"].Text = "'" + MONTHLYIImproper_Count + "'";
                report.DataDefinition.FormulaFields["MONTHLYIMismatch"].Text = "'" + MONTHLYIMismatch_Count + "'";
                report.DataDefinition.FormulaFields["MONTHLYIAbsent"].Text = "'" + MONTHLYIAbsent_Count + "'";
                report.DataDefinition.FormulaFields["MONTHLYIPresent"].Text = "'" + MIOT + "'";
                report.DataDefinition.FormulaFields["MONTHLYITotalOT"].Text = "'" + MONTHLYITotalOT + "'";

                report.DataDefinition.FormulaFields["FNNBImproper"].Text = "'" + FNNBImproper_Count + "'";
                report.DataDefinition.FormulaFields["FNNBMismatch"].Text = "'" + FNNBMismatch_Count + "'";
                report.DataDefinition.FormulaFields["FNNBAbsent"].Text = "'" + FNNBAbsent_Count + "'";
                report.DataDefinition.FormulaFields["FNNBPresent"].Text = "'" + FNNBOT + "'";
                report.DataDefinition.FormulaFields["FNNBTotalOT"].Text = "'" + FNNBTotalOT + "'";

                report.DataDefinition.FormulaFields["TImp"].Text = "'" + Timp + "'";
                report.DataDefinition.FormulaFields["TMis"].Text = "'" + TMis + "'";
                report.DataDefinition.FormulaFields["TAB"].Text = "'" + TAB + "'";
                report.DataDefinition.FormulaFields["TP"].Text = "'" + OTPRESENT + "'";
                report.DataDefinition.FormulaFields["TOT"].Text = "'" + TOT + "'";

                report.Database.Tables[0].SetDataSource(AutoDTable);
                report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                CrystalReportViewer1.ReportSource = report;
            }
        }
    }
}