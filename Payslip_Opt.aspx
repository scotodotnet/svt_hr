﻿<%@ Page Language="C#" MasterPageFile="~/PayrollMaster.master" AutoEventWireup="true" CodeFile="Payslip_Opt.aspx.cs" Inherits="Payslip_Opt" Title="Payslip Report View" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:UpdatePanel ID="SalPay" runat="server">
                 <ContentTemplate>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
        if (sender._postBackSettings.panelsToUpdate != null) {
            $('.select2').select2();
                $('.datepicker').datepicker({
                    format: "dd/mm/yyyy",
                    autoclose: true
                });
            }
        });
    };
</script>


 <!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Report</a></li>
				<li class="active">Pay Slip</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Pay Slip</h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">Pay Slip</h4>
                        </div>
                        <div class="panel-body">
                        <div class="form-group">
                        <!-- begin row -->
                        <div class="row">
                         <!-- begin col-4 -->
                              <div class="col-md-2">
								<div class="form-group">
								 <label>Category</label>
								 <asp:DropDownList runat="server" ID="ddlcategory" class="form-control select2" 
                                        style="width:100%;" AutoPostBack="true" 
                                        onselectedindexchanged="ddlcategory_SelectedIndexChanged">
								 <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
								 <asp:ListItem Value="STAFF">STAFF</asp:ListItem>
								 <asp:ListItem Value="LABOUR">LABOUR</asp:ListItem>
								</asp:DropDownList>
								<asp:RequiredFieldValidator ControlToValidate="ddlcategory" InitialValue="-Select-" Display="Dynamic"  ValidationGroup="Validate_PayField" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <asp:RequiredFieldValidator ControlToValidate="ddlcategory" InitialValue="-Select-" Display="Dynamic"  ValidationGroup="Validate_ExpField" class="form_error" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <asp:RequiredFieldValidator ControlToValidate="ddlcategory" InitialValue="-Select-" Display="Dynamic"  ValidationGroup="Validate_LeaveField" class="form_error" ID="RequiredFieldValidator7" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <asp:RequiredFieldValidator ControlToValidate="ddlcategory" InitialValue="-Select-" Display="Dynamic"  ValidationGroup="Validate_ManField" class="form_error" ID="RequiredFieldValidator10" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
								</div>
                               </div>
                           <!-- end col-4 -->
                            <!-- begin col-4 -->
                              <div class="col-md-3">
								<div class="form-group">
								 <label>Employee Type</label>
								 <asp:DropDownList runat="server" ID="txtEmployeeType" class="form-control select2" style="width:100%;">
								 </asp:DropDownList>
								 <asp:RequiredFieldValidator ControlToValidate="txtEmployeeType" InitialValue="0" Display="Dynamic"  ValidationGroup="Validate_PayField" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <asp:RequiredFieldValidator ControlToValidate="txtEmployeeType" InitialValue="0" Display="Dynamic"  ValidationGroup="Validate_ExpField" class="form_error" ID="RequiredFieldValidator6" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <asp:RequiredFieldValidator ControlToValidate="txtEmployeeType" InitialValue="0" Display="Dynamic"  ValidationGroup="Validate_LeaveField" class="form_error" ID="RequiredFieldValidator8" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                 <asp:RequiredFieldValidator ControlToValidate="txtEmployeeType" InitialValue="0" Display="Dynamic"  ValidationGroup="Validate_ManField" class="form_error" ID="RequiredFieldValidator11" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
								</div>
                               </div>
                           <!-- end col-4 -->
                           <!-- begin col-4 -->
                              <div class="col-md-3">
								<div class="form-group">
								 <label>Division</label>
								 <asp:DropDownList runat="server" ID="txtDivision" class="form-control select2" style="width:100%;">
								 </asp:DropDownList>
								</div>
                               </div>
                           <!-- end col-4 -->
                           <!-- begin col-4 -->
                              <div class="col-md-4" runat="server" id="IF_Dept_Hide">
								<div class="form-group">
								 <label>Department</label>
								 <asp:DropDownList runat="server" ID="ddldept" class="form-control select2" style="width:100%;">
								 </asp:DropDownList>
								</div>
                               </div>
                           <!-- end col-4 -->
                        </div>
                    <!-- end row -->
                       <!-- begin row -->
                        <div class="row">
                         <!-- begin col-4 -->
                              <div class="col-md-4" runat="server" id="IF_FromDate_Hide">
								<div class="form-group">
								 <label>FromDate</label>
								  <asp:TextBox ID="txtfrom" runat="server" class="form-control datepicker" placeholder="dd/MM/YYYY"></asp:TextBox>
                                   <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                        TargetControlID="txtfrom" ValidChars="0123456789/">
                                   </cc1:FilteredTextBoxExtender>
								</div>
                               </div>
                           <!-- end col-4 -->
                           <!-- begin col-4 -->
                              <div class="col-md-4" runat="server" id="IF_ToDate_Hide">
								<div class="form-group">
								 <label>ToDate</label>
								 <asp:TextBox ID="txtTo" runat="server" class="form-control datepicker" placeholder="dd/MM/YYYY"></asp:TextBox>
                                   <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                        TargetControlID="txtTo" ValidChars="0123456789/">
                                   </cc1:FilteredTextBoxExtender>
								</div>
                               </div>
                           <!-- end col-4 -->
                            <!-- begin col-4 -->
                              <div class="col-md-2">
								<div class="form-group">
								 <label>Month</label>
								 <asp:DropDownList runat="server" ID="ddlMonths" class="form-control select2" style="width:100%;">
							 	 </asp:DropDownList>
							 	 <asp:RequiredFieldValidator ControlToValidate="ddlMonths" InitialValue="0" Display="Dynamic"  ValidationGroup="Validate_PayField" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <asp:RequiredFieldValidator ControlToValidate="ddlMonths" InitialValue="0" Display="Dynamic"  ValidationGroup="Validate_ExpField" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <asp:RequiredFieldValidator ControlToValidate="ddlMonths" InitialValue="0" Display="Dynamic"  ValidationGroup="Validate_LeaveField" class="form_error" ID="RequiredFieldValidator9" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <asp:RequiredFieldValidator ControlToValidate="ddlMonths" InitialValue="0" Display="Dynamic"  ValidationGroup="Validate_ManField" class="form_error" ID="RequiredFieldValidator12" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
								</div>
                               </div>
                           <!-- end col-4 -->
                            <!-- begin col-4 -->
                              <div class="col-md-2">
								<div class="form-group">
								 <label>Fin. Year</label>
								 <asp:DropDownList runat="server" ID="ddlFinance" class="form-control select2" style="width:100%;">
							 	 </asp:DropDownList>
								</div>
                               </div>
                               <div class="col-md-5" runat="server" visible="false" id="IF_Report_Type">
								<div class="form-group">
								<label>Payslip Format Type</label>
                               <asp:RadioButtonList ID="rdbPayslipIFFormat" runat="server" RepeatColumns="2" 
                                                        TabIndex="3" Width="220" Visible="false" class="form-control" runat="server">
                                         <asp:ListItem Selected="true" Text="Cover" Value="0"></asp:ListItem>
                                         <asp:ListItem Selected="False" Text="Sign List" Value="2"></asp:ListItem>
                                      </asp:RadioButtonList>
                                      </div>
                                    </div>
                           <!-- end col-4 -->
                        </div>
                       <!-- end row -->
                        <!-- begin row -->
                        <div class="row">
                        <!-- begin col-4 -->
                              <div class="col-md-3" runat="server" id="IF_Salary_Through_Hide">
								<div class="form-group">
								 <label>Salary Through</label>
								    <asp:RadioButtonList ID="RdbCashBank" runat="server" RepeatColumns="3" class="form-control">
                                         <asp:ListItem Selected="true" Text="ALL" style="padding-right:40px" Value="0"></asp:ListItem>
                                         <asp:ListItem Selected="False" Text="Cash" style="padding-right:40px" Value="1"></asp:ListItem>
                                         <asp:ListItem Selected="False" Text="Bank" Value="2"></asp:ListItem>
                                     </asp:RadioButtonList>
								 </div>
                               </div>
                           <!-- end col-4 -->
                           <!-- begin col-4 -->
                              <div class="col-md-4" runat="server" id="IF_PF_NON_PF_Hide">
								<div class="form-group">
								 <label>PF/Non PF</label>
								
								    <asp:RadioButtonList ID="RdbPFNonPF" runat="server" RepeatColumns="3" class="form-control">
                                          <asp:ListItem Selected="true" Text="ALL" style="padding-right:40px" Value="0"></asp:ListItem>
                                          <asp:ListItem Selected="False" Text="PF" style="padding-right:40px" Value="1"></asp:ListItem>
                                          <asp:ListItem Selected="False" Text="Non PF" Value="2"></asp:ListItem>
                                    </asp:RadioButtonList>
                                    <asp:RadioButtonList ID="RdpIFPF" runat="server" RepeatColumns="1" 
                                                                    TabIndex="3" Width="180" Visible="false">
                                                                   <asp:ListItem Selected="true" Text="PF" Value="1"></asp:ListItem>
                                                                    </asp:RadioButtonList>
								 </div>
                               </div>
                           <!-- end col-4 -->
                           <!-- begin col-4 -->
                              <div class="col-md-5" runat="server" id="IF_rdbPayslipFormat_Hide">
								<div class="form-group">
								 <label>Payslip Format Type</label>
								
								    <asp:RadioButtonList ID="rdbPayslipFormat" runat="server" RepeatColumns="5" class="form-control">
                                          <asp:ListItem Selected="true" Text="Type1" style="padding-right:10px" Value="0"></asp:ListItem>
                                          <asp:ListItem Selected="False" Text="Type2" style="padding-right:10px" Value="1"></asp:ListItem>
                                          <asp:ListItem Selected="False" Text="Type3" style="padding-right:10px" Value="2"></asp:ListItem>
                                          <asp:ListItem Selected="False" Text="Type4" style="padding-right:10px" Value="3"></asp:ListItem>
                                          <asp:ListItem Selected="False" Text="CheckList" Value="4"></asp:ListItem>
                                     </asp:RadioButtonList>
                                      
								 </div>
                               </div>
                           <!-- end col-4 -->
                         </div>
                       <!-- end row -->
                       <!-- begin row -->
                        <div class="row">
                          <!-- begin col-4 -->
                              <div class="col-md-4" runat="server" id="IF_Left_Employee_Hide">
								<div class="form-group">
								  <asp:CheckBox ID="ChkLeft" runat="server" />LeftEmployee
								 <asp:TextBox ID="txtLeftDate" runat="server" class="form-control datepicker"></asp:TextBox>
								</div>
                               </div>
                           <!-- end col-4 -->
                           <!-- begin col-4 -->
                              <div class="col-md-5" runat="server" id="IF_State_Hide">
								<div class="form-group">
								 <label>State</label>
								
								    <asp:RadioButtonList ID="RdbOtherState" runat="server" RepeatColumns="3" class="form-control">
                                          <asp:ListItem Selected="true" Text="All" style="padding-right:40px" Value="0"></asp:ListItem>
                                          <asp:ListItem Selected="False" Text="Other State" style="padding-right:40px" Value="1"></asp:ListItem>
                                          <asp:ListItem Selected="False" Text="Non Other State" Value="2"></asp:ListItem>
                                     </asp:RadioButtonList>
								 </div>
                               </div>
                           <!-- end col-4 -->
                           <!-- begin col-4 -->
                                 <div class="col-md-3">
									<div class="form-group" style="margin-top: 10%;">
                                <asp:CheckBox ID="chkExment" runat="server" /> Exempted Staff
                                </div>
                                  </div>
                               <!-- end col-4 -->
                        </div>
                       <!-- end row -->
                       <div class="row">
                            <div class="col-md-4">
								<div class="form-group">
								 <label>Bank Name</label>
								 <asp:DropDownList runat="server" ID="txtBankName" class="form-control select2" style="width:100%;">
							 	 </asp:DropDownList>
								</div>
                               </div>
                       </div>
                       
                           <!-- begin row -->  
                        <div class="row">
                        <div class="col-md-2"></div>
                           <!-- begin col-4 -->
                                <div class="col-md-10">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btnSearch" Text="Payslip" class="btn btn-success" ValidationGroup="Validate_PayField" OnClick="btnSearch_Click"/>
									<asp:Button runat="server" id="btnExport" Text="Salary Details" class="btn btn-success" ValidationGroup="Validate_ExpField" OnClick="btnExport_Click" Visible="false"/>
									<%--<asp:Button runat="server" id="btnAllSalary" Text="All Salary Details" class="btn btn-success" OnClick="btnAllSalary_Click" Visible="false"/>--%>
									<asp:Button runat="server" id="btnBankSalary" Text="Bank Salary" class="btn btn-success" onclick="btnBankSalary_Click"/>
									<asp:Button runat="server" id="btnOtherBankSalary" Text="Other Bank Salary" class="btn btn-success" onclick="btnOtherBankSalary_Click"/>
									<asp:Button runat="server" id="btnCivilAbstract" Text="Civil Abstract" class="btn btn-success" OnClick="btnCivilAbstract_Click" Visible="false"/>
									<asp:Button runat="server" id="btnLeaveWages" Text="NFH Worked Wages" class="btn btn-success" ValidationGroup="Validate_LeaveField" OnClick="btnLeaveWages_Click" Visible="false"/>
									<asp:Button runat="server" id="BtnDeptManDays" Text="Department Mandays Wages" class="btn btn-success" onclick="BtnDeptManDays_Click" ValidationGroup="Validate_ManField"  />
									<%--<asp:Button runat="server" id="btnConfirm" Text="Confirmation" class="btn btn-success" OnClick="btnConfirm_Click" Visible="false"/>--%>
									
						    	 </div>
                               </div>
                              <!-- end col-4 -->
                            
                         </div>
                        <!-- end row --> 
                       <asp:Panel ID="IF_Leave_Credit" runat="server" Visible="false">
                       <legend>Leave Credit Report</legend>
                        <!-- begin row -->
                        <div class="row">
                        <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label> Year</label>
								 <asp:DropDownList runat="server" ID="txtLCYear" class="form-control select2" style="width:100%;">
							 	 </asp:DropDownList>
								</div>
                               </div>
                           <!-- end col-4 -->
                             <!-- begin col-4 -->
                                <div class="col-md-7">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btnLCReport" Text="LC Monthwise" class="btn btn-success" OnClick="btnLCReport_Click" Visible="false"/>
									<asp:Button runat="server" id="btnLCYearTotal" Text="LC upto date" class="btn btn-success" OnClick="btnLCYearTotal_Click"/>
									<asp:Button runat="server" id="btnLCDetReport" Text="LC History" class="btn btn-success" OnClick="btnLCDetReport_Click" Visible="false"/>
									
						    	 </div>
                               </div>
                              <!-- end col-4 -->
                        </div>
                       <!-- end row -->
                       </asp:Panel>
                       
                       <asp:Panel runat="server" ID="PnlTrStaff" Visible="false">
                        <!--  STAFF SALARY Details GridView Start  -->
                                            <tr id="TrStaff" runat="server" visible="false">
                                                <td colspan="4">
                                                    <asp:GridView ID="GVStaff" runat="server" AutoGenerateColumns="false" 
                                                        onselectedindexchanged="gvSalary_SelectedIndexChanged">
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Si. No</HeaderTemplate>
                                                                <ItemTemplate><%# Container.DataItemIndex + 1   %></ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Department</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbldept" runat="server" Text='<%# Eval("DepartmentNm") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Designation</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDesignation" runat="server" Text='<%# Eval("Designation") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Token No</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblExist" runat="server" Text='<%# Eval("ExisistingCode") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Name</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblname" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Working Days</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorkingDays" runat="server" Text='<%# Eval("WorkedDays") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>NFH</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblNFH" runat="server" Text='<%# Eval("NFh") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>W.H</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWH_Work_Days" runat="server" Text='<%# Eval("WH_Work_Days") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>H.Allwd</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblcl" runat="server" text='<%# Eval("CL") %>' ></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>LeaveCredit</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblLeaveCredit" runat="server" text='<%# Eval("LeaveCredit") %>' ></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>LOPDays</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblLOPDays" runat="server" Text='<%# Eval("LOPDays") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>LOPAmount</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblLOPAmount" runat="server" Text='<%# Eval("Losspay") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Fixed Salary</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblFixed" runat="server" Text='<%# Eval("Base") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Earned Stipend</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblBase" runat="server" Text='<%# Eval("BasicandDA") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Basic And DA</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblBasicAndDA" runat="server" Text='<%# Eval("BasicAndDANew") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>HRA</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblHRA" runat="server" Text='<%# Eval("BasicHRA") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Conv.Allow</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblConvAllow" runat="server" Text='<%# Eval("ConvAllow") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Edu.Allow</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblEduAllow" runat="server" Text='<%# Eval("EduAllow") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Medi.Allow</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblMediAllow" runat="server" Text='<%# Eval("MediAllow") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>RAI</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblBasicRAI" runat="server" Text='<%# Eval("BasicRAI") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Washing Allow</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWashingAllow" runat="server" Text='<%# Eval("WashingAllow") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Allowance 1</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblall1" runat="server" Text='<%# Eval("allowances1") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Allowance 2</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblall2" runat="server" Text='<%# Eval("allowances2") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Allowance 3</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblall3" runat="server" Text='<%# Eval("allowances3") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Allowance 4</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblall4" runat="server" Text='<%# Eval("allowances4") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Allowance 5</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblall5" runat="server" Text='<%# Eval("allowances5") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Total Earning</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTotalEarning" runat="server" Text='<%# Eval("GrossEarnings") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>P.F</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblpf" runat="server" Text='<%# Eval("ProvidentFund") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>ESI</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblESI" runat="server" Text='<%# Eval("ESI") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Advance1</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblAdvance" runat="server" Text='<%# Eval("Advance") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Deduction 1</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblded1" runat="server" Text='<%# Eval("Deduction1") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Deduction 2</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDed2" runat="server" Text='<%# Eval("Deduction2") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Canteen</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDed3" runat="server" Text='<%# Eval("Deduction3") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>TDS</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDed4" runat="server" Text='<%# Eval("Deduction4") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Deduction 5</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDed5" runat="server" Text='<%# Eval("Deduction5") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Total Deduction</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbltotded" runat="server" Text='<%# Eval("TotalDeductions") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Roundoff</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblRoundoff" runat="server" Text='<%# Eval("Roundoff") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField> 
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Net Amount</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblnet" runat="server" Text='<%# Eval("NetPay") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Signature</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="LBLSIGNATURE" runat="server" Text=""></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <!--  STAFF SALARY Details GridView End  -->
                       </asp:Panel>
                       <asp:Panel runat="server" ID="PnltrSub_Staff" Visible="false">
                        <!--  SUB-STAFF SALARY Details GridView Start  -->
                                            <tr id="trSub_Staff" runat="server" visible="false">
                                                <td colspan="4">
                                                    <asp:GridView ID="GVSub_Staff" runat="server" AutoGenerateColumns="false">
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Si. No</HeaderTemplate>
                                                                <ItemTemplate><%# Container.DataItemIndex + 1   %></ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Department</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbldept" runat="server" Text='<%# Eval("DepartmentNm") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Designation</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDesignation" runat="server" Text='<%# Eval("Designation") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Token No</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblExist" runat="server" Text='<%# Eval("ExisistingCode") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Name</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblname" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Working Days</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorkingDays" runat="server" Text='<%# Eval("WorkedDays") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>NFH</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblNFH" runat="server" Text='<%# Eval("NFh") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>OT Days</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblcl" runat="server" text='<%# Eval("ThreeSided") %>' ></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>OT Hours</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblOTHoursNew" runat="server" text='<%# Eval("OTHoursNew") %>' ></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>H.Allwd</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblcl" runat="server" text='<%# Eval("CL") %>' ></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Fixed Salary</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblFixed" runat="server" Text='<%# Eval("Base") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Earned Stipend</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblBase" runat="server" Text='<%# Eval("BasicandDA") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Basic And DA</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblBasicAndDA" runat="server" Text='<%# Eval("BasicAndDANew") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>HRA</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblHRA" runat="server" Text='<%# Eval("BasicHRA") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Conv.Allow</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblConvAllow" runat="server" Text='<%# Eval("ConvAllow") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Edu.Allow</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblEduAllow" runat="server" Text='<%# Eval("EduAllow") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Medi.Allow</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblMediAllow" runat="server" Text='<%# Eval("MediAllow") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>RAI</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblBasicRAI" runat="server" Text='<%# Eval("BasicRAI") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Washing Allow</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWashingAllow" runat="server" Text='<%# Eval("WashingAllow") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>OT Days Amt</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblOtDaysAmt" runat="server" Text='<%# Eval("ThreesidedAmt") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>OT Hours Amt</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblOthoursAmt" runat="server" Text='<%# Eval("OTHoursAmtNew") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Day Incentive</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDayIncentive" runat="server" Text='<%# Eval("DayIncentive") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Spinning Allow</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblall1" runat="server" Text='<%# Eval("allowances1") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Allowance 2</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblall2" runat="server" Text='<%# Eval("allowances2") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Allowance 3</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblall3" runat="server" Text='<%# Eval("allowances3") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Allowance 4</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblall4" runat="server" Text='<%# Eval("allowances4") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Allowance 5</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblall5" runat="server" Text='<%# Eval("allowances5") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Total Earning</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTotalEarning" runat="server" Text='<%# Eval("GrossEarnings") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>P.F</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblpf" runat="server" Text='<%# Eval("ProvidentFund") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>ESI</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblESI" runat="server" Text='<%# Eval("ESI") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Advance1</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblAdvance" runat="server" Text='<%# Eval("Advance") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Deduction 1</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblded1" runat="server" Text='<%# Eval("Deduction1") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Deduction 2</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDed2" runat="server" Text='<%# Eval("Deduction2") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Canteen</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDed3" runat="server" Text='<%# Eval("Deduction3") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Advance2</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDed4" runat="server" Text='<%# Eval("Deduction4") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Deduction 5</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDed5" runat="server" Text='<%# Eval("Deduction5") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Total Deduction</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbltotded" runat="server" Text='<%# Eval("TotalDeductions") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Roundoff</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblRoundoff" runat="server" Text='<%# Eval("Roundoff") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Net Amount</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblnet" runat="server" Text='<%# Eval("NetPay") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Signature</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="LBLSIGNATURE" runat="server" Text=""></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <!--  SUB-STAFF SALARY Details GridView End  -->
                       </asp:Panel>
                       
                       <asp:Panel runat="server" ID="PnltrRegular" Visible="false">
                         <!--  REGULAR SALARY Details GridView Start  -->
                                            <tr id="trRegular" runat="server" visible="false">
                                                <td colspan="4">
                                                    <asp:GridView ID="GVRegular" runat="server" AutoGenerateColumns="false" 
                                                        onselectedindexchanged="gvSalary_SelectedIndexChanged">
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Si. No</HeaderTemplate>
                                                                <ItemTemplate><%# Container.DataItemIndex + 1   %></ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Department</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbldept" runat="server" Text='<%# Eval("DepartmentNm") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Designation</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDesignation" runat="server" Text='<%# Eval("Designation") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Token No</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblExist" runat="server" Text='<%# Eval("ExisistingCode") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Name</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblname" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Working Days</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorkingDays" runat="server" Text='<%# Eval("WorkedDays") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>NFH</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblNFH" runat="server" Text='<%# Eval("NFh") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>OT Days</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblcl" runat="server" text='<%# Eval("ThreeSided") %>' ></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>H.Allwd</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblcl" runat="server" text='<%# Eval("CL") %>' ></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Fixed Salary</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblFixed" runat="server" Text='<%# Eval("Base") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Earned Stipend</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblBase" runat="server" Text='<%# Eval("BasicandDA") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Basic And DA</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblBasicAndDA" runat="server" Text='<%# Eval("BasicAndDANew") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>HRA</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblHRA" runat="server" Text='<%# Eval("BasicHRA") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Conv.Allow</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblConvAllow" runat="server" Text='<%# Eval("ConvAllow") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Edu.Allow</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblEduAllow" runat="server" Text='<%# Eval("EduAllow") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Medi.Allow</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblMediAllow" runat="server" Text='<%# Eval("MediAllow") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>RAI</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblBasicRAI" runat="server" Text='<%# Eval("BasicRAI") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Washing Allow</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWashingAllow" runat="server" Text='<%# Eval("WashingAllow") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>OT Days Amt</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblOtDaysAmt" runat="server" Text='<%# Eval("ThreesidedAmt") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Day Incentive</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDayIncentive" runat="server" Text='<%# Eval("DayIncentive") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Spinning Allow</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblall1" runat="server" Text='<%# Eval("allowances1") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Allowance 2</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblall2" runat="server" Text='<%# Eval("allowances2") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Allowance 3</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblall3" runat="server" Text='<%# Eval("allowances3") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Allowance 4</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblall4" runat="server" Text='<%# Eval("allowances4") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Allowance 5</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblall5" runat="server" Text='<%# Eval("allowances5") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Total Earning</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTotalEarning" runat="server" Text='<%# Eval("GrossEarnings") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>P.F</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblpf" runat="server" Text='<%# Eval("ProvidentFund") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>ESI</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblESI" runat="server" Text='<%# Eval("ESI") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Advance1</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblAdvance" runat="server" Text='<%# Eval("Advance") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Deduction 1</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblded1" runat="server" Text='<%# Eval("Deduction1") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Deduction 2</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDed2" runat="server" Text='<%# Eval("Deduction2") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Canteen</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDed3" runat="server" Text='<%# Eval("Deduction3") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Advance2</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDed4" runat="server" Text='<%# Eval("Deduction4") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Deduction 5</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDed5" runat="server" Text='<%# Eval("Deduction5") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Total Deduction</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbltotded" runat="server" Text='<%# Eval("TotalDeductions") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Roundoff</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblRoundoff" runat="server" Text='<%# Eval("Roundoff") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Net Amount</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblnet" runat="server" Text='<%# Eval("NetPay") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Signature</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="LBLSIGNATURE" runat="server" Text=""></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <!--  REGULAR SALARY Details GridView End  -->
                       </asp:Panel>
                       
                       <asp:Panel runat="server" ID="PnlTrHostel" Visible="false">
                        <!--Hostel Salary Details GridView Start-->
                                            <tr id="TrHostel" runat="server" visible="false">
                                                <td colspan="4">
                                                    <asp:GridView ID="GVHostel" runat="server" AutoGenerateColumns="false" 
                                                        onselectedindexchanged="gvSalary_SelectedIndexChanged">
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Si. No</HeaderTemplate>
                                                                <ItemTemplate><%# Container.DataItemIndex + 1   %></ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField Visible="false">
                                                                <HeaderTemplate>EmpNo</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblEmpNo" runat="server" Text='<%# Eval("EmpNo") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Department</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbldept" runat="server" Text='<%# Eval("DepartmentNm") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Designation</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDesignation" runat="server" Text='<%# Eval("Designation") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Ticket No</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblExist" runat="server" Text='<%# Eval("ExisistingCode") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Name</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblname" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Working Days</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorkingDays" runat="server" Text='<%# Eval("WorkedDays") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>NFH</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblNFH" runat="server" Text='<%# Eval("NFh") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>OT Days</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblcl" runat="server" text='<%# Eval("ThreeSided") %>' ></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>H.Allwd</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblcl" runat="server" text='<%# Eval("CL") %>' ></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Fixed Salary</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblFixed" runat="server" Text='<%# Eval("Base") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Earned Stipend</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblBase" runat="server" Text='<%# Eval("BasicandDA") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Basic And DA</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblBasicAndDA" runat="server" Text='<%# Eval("BasicAndDANew") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>HRA</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblHRA" runat="server" Text='<%# Eval("BasicHRA") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Conv.Allow</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblConvAllow" runat="server" Text='<%# Eval("ConvAllow") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Edu.Allow</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblEduAllow" runat="server" Text='<%# Eval("EduAllow") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Medi.Allow</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblMediAllow" runat="server" Text='<%# Eval("MediAllow") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>RAI</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblBasicRAI" runat="server" Text='<%# Eval("BasicRAI") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Washing Allow</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWashingAllow" runat="server" Text='<%# Eval("WashingAllow") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>OT Days Amt</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblOtDaysAmt" runat="server" Text='<%# Eval("ThreesidedAmt") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Spinning Allow</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblall1" runat="server" Text='<%# Eval("allowances1") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Allowance 2</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblall2" runat="server" Text='<%# Eval("allowances2") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Allowance 3</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblall3" runat="server" Text='<%# Eval("allowances3") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Allowance 4</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblall4" runat="server" Text='<%# Eval("allowances4") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Allowance 5</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblall5" runat="server" Text='<%# Eval("allowances5") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Total Earning</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTotalEarning" runat="server" Text='<%# Eval("GrossEarnings") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>P.F</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblpf" runat="server" Text='<%# Eval("ProvidentFund") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>ESI</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblESI" runat="server" Text='<%# Eval("ESI") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Advance1</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblAdvance" runat="server" Text='<%# Eval("Advance") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Canteen</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblded1" runat="server" Text='<%# Eval("Deduction1") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Deduction 2</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDed2" runat="server" Text='<%# Eval("Deduction2") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Advance2</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDed3" runat="server" Text='<%# Eval("Deduction3") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Guest Food</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDed4" runat="server" Text='<%# Eval("Deduction4") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Deduction 5</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDed5" runat="server" Text='<%# Eval("Deduction5") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Total Deduction</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbltotded" runat="server" Text='<%# Eval("TotalDeductions") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>                                                            
                                                            <%--<asp:TemplateField>
                                                                <HeaderTemplate>OT Hours</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblOThours" runat="server" Text=""></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>OT Amount</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblOT" runat="server" Text='<%# Eval("OverTime") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Roundoff</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblRoundoff" runat="server" Text='<%# Eval("Roundoff") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField> 
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Net Amount</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblnet" runat="server" Text='<%# Eval("NetPay") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Signature</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="LBLSIGNATURE" runat="server" Text=""></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <!--Hostel Salary Details GridView End-->
                       </asp:Panel>
                       
                       
                       <asp:Panel runat="server" ID="PnlTrCivil" Visible="false">
                        <!--CIVIL Salary Details GridView Start-->
                                            <tr id="TrCivil" runat="server" visible="false">
                                                <td colspan="4">
                                                    <asp:GridView ID="GVCivil" runat="server" AutoGenerateColumns="false" 
                                                        onselectedindexchanged="gvSalary_SelectedIndexChanged">
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Si. No</HeaderTemplate>
                                                                <ItemTemplate><%# Container.DataItemIndex + 1   %></ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Department</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbldept" runat="server" Text='<%# Eval("DepartmentNm") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Designation</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDesignation" runat="server" Text='<%# Eval("Designation") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Ticket No</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblExist" runat="server" Text='<%# Eval("ExisistingCode") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Name</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblname" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Working Days</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorkingDays" runat="server" Text='<%# Eval("WorkedDays") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>NFH</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblNFH" runat="server" Text='<%# Eval("NFh") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>OT Hours</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblOTHoursNew" runat="server" text='<%# Eval("OTHoursNew") %>' ></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Fixed Salary</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblFixed" runat="server" Text='<%# Eval("Base") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>OT Hours Salary</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblOTFixed" runat="server" Text='<%# Eval("OTFixed") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Earned Stipend</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblBase" runat="server" Text='<%# Eval("BasicandDA") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>OT Hours Amt</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblOthoursAmt" runat="server" Text='<%# Eval("OTHoursAmtNew") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Attn. Incentive</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblAttnIncentive" runat="server" Text='<%# Eval("DayIncentive") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Allowance 1</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblall1" runat="server" Text='<%# Eval("allowances1") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Allowance 2</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblall2" runat="server" Text='<%# Eval("allowances2") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Allowance 3</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblall3" runat="server" Text='<%# Eval("allowances3") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Allowance 4</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblall4" runat="server" Text='<%# Eval("allowances4") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Allowance 5</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblall5" runat="server" Text='<%# Eval("allowances5") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Total Earning</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTotalEarning" runat="server" Text='<%# Eval("GrossEarnings") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Advance1</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblAdvance" runat="server" Text='<%# Eval("Advance") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Deduction 1</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblded1" runat="server" Text='<%# Eval("Deduction1") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Deduction 2</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDed2" runat="server" Text='<%# Eval("Deduction2") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Deduction 3</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDed3" runat="server" Text='<%# Eval("Deduction3") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Deduction 4</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDed4" runat="server" Text='<%# Eval("Deduction4") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Deduction 5</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDed5" runat="server" Text='<%# Eval("Deduction5") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Total Deduction</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbltotded" runat="server" Text='<%# Eval("TotalDeductions") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Roundoff</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblRoundoff" runat="server" Text='<%# Eval("Roundoff") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField> 
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Net Amount</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblnet" runat="server" Text='<%# Eval("NetPay") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Signature</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="LBLSIGNATURE" runat="server" Text=""></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <!--CIVIL Salary Details GridView End-->
                       </asp:Panel>
                       
                       <asp:Panel runat="server" Visible="false" ID="Pnlgv">
                        <tr id="gv" runat="server" visible="false">
                                                <td colspan="4">
                                                    <asp:GridView ID="gvSalary" runat="server" AutoGenerateColumns="false" 
                                                        onselectedindexchanged="gvSalary_SelectedIndexChanged">
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    Si. No</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <%# Container.DataItemIndex + 1   %></ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                        <HeaderTemplate>Emp No</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblEmpNo" runat="server" Text='<%# Eval("EmpNo") %>'></asp:Label>
                                                                        </ItemTemplate>    
                                                                    </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    Ticket No</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblExist" runat="server" Text='<%# Eval("ExisistingCode") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    OldID</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblOld" runat="server" Text='<%# Eval("OldID") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    Department</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbldept" runat="server" Text='<%# Eval("DepartmentNm") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    Name</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblname" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%--<asp:TemplateField>
                                                                        <HeaderTemplate>Designation</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDesignation" runat="server" Text='<%# Eval("Designation") %>' ></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>--%>
                                                            <%--<asp:TemplateField>
                                                                        <HeaderTemplate>Department</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbldepartment" runat="server" Text='<%# Eval("DepartmentNm") %>' ></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>--%>
                                                            <%--<asp:TemplateField>
                                                                        <HeaderTemplate>Totalworkingdays</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbltotalworkingdays" runat="server" Text='<%# Eval("Totalworkingdays") %>' ></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>--%>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    Working Days</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblWorkingDays" runat="server" Text='<%# Eval("WorkedDays") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%--<asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    NFH</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblNFH" runat="server" Text='<%# Eval("NFh") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>
                                                            <%--<asp:TemplateField>
                                                                <HeaderTemplate>Total Days</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTotal" runat="server" Text='<%# Eval("tot") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>CL</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblcl" runat="server" text='<%# Eval("CL") %>' ></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Week Off</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblweek" runat="server" Text='<%# Eval("Weekoff") %>'></asp:Label> 
                                                                </ItemTemplate>
                                                                
                                                            </asp:TemplateField>
                                                            <%--<asp:TemplateField>
                                                                <HeaderTemplate>Home Town Days</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblcl" runat="server" text='<%# Eval("HomeDays") %>' ></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    OT Hours</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblOThours" runat="server" Text=""></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    Fixed Salary</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblFixed" runat="server" Text='<%# Eval("Base") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    Fixed OT Salary</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblFixedOT" runat="server" Text='<%# Eval("PFS") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    Base</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblBase" runat="server" Text='<%# Eval("BasicandDA") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%--<asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    FDA</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblFDA" runat="server" Text='<%# Eval("FDA") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    VDA</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblvda" runat="server" Text='<%# Eval("VDA") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    HRA</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblHRA" runat="server" Text='<%# Eval("HRA") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>
                                                            <%--<asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    TotalEarnings</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbltot" runat="server" Text='<%# Eval("GrossEarnings") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    PF Earnings</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblPFEarnings" runat="server" Text='<%# Eval("PFEarnings") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    P.F</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblpf" runat="server" Text='<%# Eval("ProvidentFund") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    ESI</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblESI" runat="server" Text='<%# Eval("ESI") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%--<asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    Union</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblUnion" runat="server" Text='<%# Eval("Unioncg") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    Advance1</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblAdvance" runat="server" Text='<%# Eval("Advance") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    Allowance 1</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblall1" runat="server" Text='<%# Eval("allowances1") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    Allowance 2</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblall2" runat="server" Text='<%# Eval("allowances2") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    Allowance 3</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblall3" runat="server" Text='<%# Eval("allowances3") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    Allowance 4</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblall4" runat="server" Text='<%# Eval("allowances4") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    Allowance 5</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblall5" runat="server" Text='<%# Eval("allowances5") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%--<asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    O/N SHIFT</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblall5" runat="server" Text='<%# Eval("HalfNightAmt") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    F/N SHIFT</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblall5" runat="server" Text='<%# Eval("FullNightAmt") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    SPINNING AMT</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblall5" runat="server" Text='<%# Eval("SpinningAmt") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    DAY INCENTIVE</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblall5" runat="server" Text='<%# Eval("DayIncentive") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    THREE SIDED</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblall5" runat="server" Text='<%# Eval("ThreesidedAmt") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Deduction 1</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblded1" runat="server" Text='<%# Eval("Deduction1") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Deduction 2</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDed2" runat="server" Text='<%# Eval("Deduction2") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Deduction 3</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDed3" runat="server" Text='<%# Eval("Deduction3") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Deduction 4</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDed4" runat="server" Text='<%# Eval("Deduction4") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    Deduction 5</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDed5" runat="server" Text='<%# Eval("Deduction5") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    LOP</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbllop" runat="server" Text='<%# Eval("Losspay") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            
                                                            <%--<asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    Stamp</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblstamp" runat="server" Text='<%# Eval("Stamp") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Total Deduction</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbltotded" runat="server" Text='<%# Eval("TotalDeductions") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Net Salary</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblNetSalary" runat="server" Text='<%# Eval("NetSalary") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>OT</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblOT" runat="server" Text='<%# Eval("OverTime") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%--<asp:TemplateField>
                                                                        <HeaderTemplate>Round off</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblround" runat="server"  Text='<%# Eval("Roundoff") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>--%>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Net Amount</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblnet" runat="server" Text='<%# Eval("NetPay") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Signature</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="LBLSIGNATURE" runat="server" Text=""></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                       </asp:Panel>
                       
                       <asp:Panel runat="server" ID="PnlBankSalary" Visible="false">
                          <!--  BANK SALARY Details GridView End  -->
                                            <tr id="BankSalary" runat="server" visible="false"> 
                                                <td colspan="4">
                                                    <asp:GridView ID="BankSalaryGridView" runat="server" AutoGenerateColumns="false">
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>S. No</HeaderTemplate>
                                                                <ItemTemplate><%# Container.DataItemIndex + 1   %></ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Token No</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTOkenNo" runat="server" Text='<%# Eval("ExisistingCode") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>NAME</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblname" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>ACCOUNT NO</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblAccountNo" runat="server" Text='<%# Eval("AccountNo") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>NET SALARY</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblNetSalary" runat="server" Text='<%# Eval("NetSalary") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>REMARKS</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblRemarks" runat="server" Text=""></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </td>
                                            </tr> <!--  BANK SALARY Details GridView End  -->
                                            
                       </asp:Panel>
                       
                       <asp:Panel runat="server" ID="Panel1" Visible="false">
                          <!--  Labour BANK SALARY Others Details GridView End  -->
                                            <tr id="Tr1" runat="server" visible="false"> 
                                                <td colspan="4">
                                                    <asp:GridView ID="OtherBankSalaryGridView" runat="server" AutoGenerateColumns="false">
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>S. No</HeaderTemplate>
                                                                <ItemTemplate><%# Container.DataItemIndex + 1   %></ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>Token No</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblTOkenNo" runat="server" Text='<%# Eval("ExisistingCode") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>NAME</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblname" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>ACCOUNT NO</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblAccountNo" runat="server" Text='<%# Eval("AccountNo") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>NET SALARY</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblNetSalary" runat="server" Text='<%# Eval("Bank_Others") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>REMARKS</HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblRemarks" runat="server" Text=""></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </td>
                                            </tr> <!--  BANK SALARY Details GridView End  -->
                                            
                       </asp:Panel>
                       
                        </div>
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
<!-- end #content -->



 </ContentTemplate>
                 <Triggers>
                    <asp:PostBackTrigger ControlID="btnBankSalary" />
                    <asp:PostBackTrigger ControlID="btnOtherBankSalary" />
                    
                 </Triggers>
              </asp:UpdatePanel>  

</asp:Content>

