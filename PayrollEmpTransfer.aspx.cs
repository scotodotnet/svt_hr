﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class PayrollEmpTransfer : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string Query = "";
    string SessionPayroll;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        Load_DB();
        if (!IsPostBack)
        {
            Load_TransferTokenNo();
            Load_Unit();
        }
    }
    public void Load_DB()
    {
        //Get Database Name
        string query = "";
        DataTable dt_DB = new DataTable();
        query = "select CMS,Rights,Sales,Stores,Weaving,Maintaince,Production,Payroll from [SVT_Rights]..MstDBname";
        dt_DB = objdata.RptEmployeeMultipleDetails(query);
        if (dt_DB.Rows.Count > 0)
        {
            SessionPayroll = dt_DB.Rows[0]["Payroll"].ToString();
            //SessionRights = dt_DB.Rows[0]["Rights"].ToString();
        }
    }
    public void Load_TransferTokenNo()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlTokenNo.Items.Clear();
        query = "select ExistingCode from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And IsActive='Yes'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlTokenNo.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["ExistingCode"] = "-Select-";
        dr["ExistingCode"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlTokenNo.DataTextField = "ExistingCode";
        ddlTokenNo.DataValueField = "ExistingCode";
        ddlTokenNo.DataBind();
    }

    private void Load_Unit()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        DataTable DT = new DataTable();
        ddlUnit.Items.Clear();
        query = "Select LocCode from Location_Mst where CompCode='" + SessionCcode + "'";
        query = query + " And LocCode!='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlUnit.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["LocCode"] = "-Select-";
        dr["LocCode"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlUnit.DataTextField = "LocCode";
        ddlUnit.DataValueField = "LocCode";
        ddlUnit.DataBind();

    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string query = "";
        bool ErrFlag = false;
        DataTable DT = new DataTable();
        DataTable dt1 = new DataTable();

        query = "Select *from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + ddlUnit.SelectedItem.Text + "'";
        query = query + " And ExistingCode='" + txtNewTokenNo.Text + "' And EmpNo='" + txtNewMachineID.Text + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            query = "Select MachineID,MachineID_Encrypt from Employee_Mst where ExistingCode='" + ddlTokenNo.SelectedItem.Text + "'";
            query = query + " and CompCode='" + SessionCcode + "'and LocCode='" + SessionLcode + "'";
            dt1 = objdata.RptEmployeeMultipleDetails(query);
            string Empno = dt1.Rows[0]["MachineID"].ToString();
            string MachineEncrypt = dt1.Rows[0]["MachineID_Encrypt"].ToString();

            DataTable dt2 = new DataTable();
            query = "Select MachineID,MachineID_Encrypt from Employee_Mst where ExistingCode='" + txtNewTokenNo.Text + "'";
            query = query + " and CompCode='" + SessionCcode + "'and LocCode='" + ddlUnit.SelectedItem.Text + "'";
            dt2 = objdata.RptEmployeeMultipleDetails(query);
            string MachineEncrypt_New = dt2.Rows[0]["MachineID_Encrypt"].ToString();


            //Epay
            //Advancepayment1 
            query = " update [" + SessionPayroll + "]..AdvancePayment set EmpNo='" + txtNewMachineID.Text + "',ExistNo='" + txtNewTokenNo.Text + "',Lcode='" + ddlUnit.SelectedItem.Text + "'";
            query = query + " where ExistNo='" + ddlTokenNo.SelectedItem.Text + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' ";
            objdata.RptEmployeeMultipleDetails(query);

            //Advancerepayment2 
            query = " update [" + SessionPayroll + "]..Advancerepayment set EmpNo='" + txtNewMachineID.Text + "',Lcode='" + ddlUnit.SelectedItem.Text + "' ";
            query = query + " where EmpNo='" + Empno + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' ";
            objdata.RptEmployeeMultipleDetails(query);

            //AttenanceDetails3 
            query = " update [" + SessionPayroll + "]..AttenanceDetails set EmpNo='" + txtNewMachineID.Text + "',ExistingCode='" + txtNewTokenNo.Text + "',Lcode='" + ddlUnit.SelectedItem.Text + "'";
            query = query + " where  ExistingCode='" + ddlTokenNo.SelectedItem.Text + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' ";
            objdata.RptEmployeeMultipleDetails(query);

            //BonusDetails4 
            query = " update [" + SessionPayroll + "]..Bonus_Details set MachineID='" + txtNewMachineID.Text + "',ExisistingCode='" + txtNewTokenNo.Text + "',Lcode='" + ddlUnit.SelectedItem.Text + "'";
            query = query + " where  ExisistingCode='" + ddlTokenNo.SelectedItem.Text + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' ";
            objdata.RptEmployeeMultipleDetails(query);

            //Bonus_Details_Last_Year5 
            query = " update [" + SessionPayroll + "]..Bonus_Details_Last_Year set ExisistingCode='" + txtNewTokenNo.Text + "',Lcode='" + ddlUnit.SelectedItem.Text + "'";
            query = query + " where  ExisistingCode='" + ddlTokenNo.SelectedItem.Text + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' ";
            objdata.RptEmployeeMultipleDetails(query);

            //BonusCalculation6 
            query = " update [" + SessionPayroll + "]..BonusCalculation set EmpCode='" + txtNewMachineID.Text + "'";
            query = query + " where  EmpCode='" + Empno + "' ";
            objdata.RptEmployeeMultipleDetails(query);

            //BonusForAll7 
            query = " update [" + SessionPayroll + "]..BonusForAll set EmpCode='" + txtNewMachineID.Text + "',Lcode='" + ddlUnit.SelectedItem.Text + "'";
            query = query + " where  EmpCode='" + Empno + "'and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' ";
            objdata.RptEmployeeMultipleDetails(query);

            //eAlert Civil Incent Days Det8 
            query = " update [" + SessionPayroll + "]..eAlert_Civil_Incent_Days_Det set MachineID='" + txtNewMachineID.Text + "',ExisistingCode='" + txtNewTokenNo.Text + "',EmpNo='" + txtNewMachineID.Text + "',Lcode='" + ddlUnit.SelectedItem.Text + "'";
            query = query + " where ExisistingCode='" + ddlTokenNo.SelectedItem.Text + "'and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' ";
            objdata.RptEmployeeMultipleDetails(query);


            //eAlert_Deduction_Det9
            query = " update [" + SessionPayroll + "]..eAlert_Deduction_Det set MachineID='" + txtNewMachineID.Text + "',ExisistingCode='" + txtNewTokenNo.Text + "',Lcode='" + ddlUnit.SelectedItem.Text + "'";
            query = query + " where   ExisistingCode='" + ddlTokenNo.SelectedItem.Text + "'and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' ";
            objdata.RptEmployeeMultipleDetails(query);


            //EL_CL_Det10
            query = " update [" + SessionPayroll + "]..EL_CL_Det set TokenNo ='" + txtNewTokenNo.Text + "',Lcode='" + ddlUnit.SelectedItem.Text + "'";
            query = query + " where   TokenNo='" + ddlTokenNo.SelectedItem.Text + "'and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' ";
            objdata.RptEmployeeMultipleDetails(query);


            //Hostel_Incr_Days_Dummy11
            query = " update [" + SessionPayroll + "]..Hostel_Incr_Days_Dummy set Token_No ='" + txtNewTokenNo.Text + "',Lcode='" + ddlUnit.SelectedItem.Text + "'";
            query = query + " where   Token_No='" + ddlTokenNo.SelectedItem.Text + "'and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' ";
            objdata.RptEmployeeMultipleDetails(query);

            //Hostel_Total_Work_Days12
            query = " update [" + SessionPayroll + "]..Hostel_Total_Work_Days set Token_No ='" + txtNewTokenNo.Text + "',EmpNo='" + txtNewMachineID.Text + "',Lcode='" + ddlUnit.SelectedItem.Text + "'";
            query = query + " where   Token_No='" + ddlTokenNo.SelectedItem.Text + "'and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' ";
            objdata.RptEmployeeMultipleDetails(query);


            //HostelIncentive_data13
            query = " update [" + SessionPayroll + "]..HostelIncentive_data set EmpNo='" + txtNewMachineID.Text + "',Lcode='" + ddlUnit.SelectedItem.Text + "'";
            query = query + " where   EmpNo='" + Empno + "'and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' ";
            objdata.RptEmployeeMultipleDetails(query);

            //MstLeaveCredit14
            query = " update [" + SessionPayroll + "]..MstLeaveCredit set EmpNo='" + txtNewMachineID.Text + "',ExisistingCode='" + txtNewTokenNo.Text + "',BiometricID='" + txtNewMachineID.Text + "',Lcode='" + ddlUnit.SelectedItem.Text + "'";
            query = query + " where   ExisistingCode='" + ddlTokenNo.SelectedItem.Text + "'and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' ";
            objdata.RptEmployeeMultipleDetails(query);

            //SalaryDetails15
            query = " update [" + SessionPayroll + "]..SalaryDetails set EmpNo='" + txtNewMachineID.Text + "',ExisistingCode='" + txtNewTokenNo.Text + "',MachineNo='" + txtNewMachineID.Text + "',Lcode='" + ddlUnit.SelectedItem.Text + "'";
            query = query + " where   ExisistingCode='" + ddlTokenNo.SelectedItem.Text + "'and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' ";
            objdata.RptEmployeeMultipleDetails(query);

            //SalaryDetails_Bonus16
            query = " update [" + SessionPayroll + "]..SalaryDetails_Bonus set MachineID='" + txtNewMachineID.Text + "',ExisistingCode='" + txtNewTokenNo.Text + "',Lcode='" + ddlUnit.SelectedItem.Text + "' ";
            query = query + " where   ExisistingCode='" + ddlTokenNo.SelectedItem.Text + "'and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' ";
            objdata.RptEmployeeMultipleDetails(query);


            //SalaryDetails_Voucher17
            query = " update [" + SessionPayroll + "]..SalaryDetails_Voucher set ExisistingCode='" + txtNewTokenNo.Text + "',Lcode='" + ddlUnit.SelectedItem.Text + "' ";
            query = query + " where   ExisistingCode='" + ddlTokenNo.SelectedItem.Text + "'and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' ";
            objdata.RptEmployeeMultipleDetails(query);

            //Wages_Increment_Det18
            query = " update [" + SessionPayroll + "]..Wages_Increment_Det set EmpNo='" + txtNewMachineID.Text + "',Token_No='" + txtNewTokenNo.Text + "',BiometricID='" + txtNewMachineID.Text + "',Lcode='" + ddlUnit.SelectedItem.Text + "'";
            query = query + " where   Token_No='" + ddlTokenNo.SelectedItem.Text + "'and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' ";
            objdata.RptEmployeeMultipleDetails(query);

            //Worker_Incentive19
            query = " update [" + SessionPayroll + "]..Worker_Incentive set EmpNo='" + txtNewMachineID.Text + "',Lcode='" + ddlUnit.SelectedItem.Text + "'";
            query = query + " where   EmpNo='" + Empno + "'and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' ";
            objdata.RptEmployeeMultipleDetails(query);


            //------------------------------------------------

            //Spay

            //CommisionRecovery_Mst1
            query = " update CommisionRecovery_Mst set TokenNo='" + txtNewTokenNo.Text + "',LocCode='" + ddlUnit.SelectedItem.Text + "'";
            query = query + " where TokenNo='" + ddlTokenNo.SelectedItem.Text + "'and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' ";
            objdata.RptEmployeeMultipleDetails(query);

            //CommisionVoucher_Mst2
            query = " update CommisionVoucher_Mst set TokenNo='" + txtNewTokenNo.Text + "',LocCode='" + ddlUnit.SelectedItem.Text + "'";
            query = query + " where   TokenNo='" + ddlTokenNo.SelectedItem.Text + "'and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' ";
            objdata.RptEmployeeMultipleDetails(query);

            //Commission_Transaction_Ledger3
            query = " update Commission_Transaction_Ledger set Token_No='" + txtNewTokenNo.Text + "',LocCode='" + ddlUnit.SelectedItem.Text + "'";
            query = query + " where   Token_No='" + ddlTokenNo.SelectedItem.Text + "'and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' ";
            objdata.RptEmployeeMultipleDetails(query);


            //Day_Attn_Record_Det4
            query = " update Day_Attn_Record_Det set ExistingCode='" + txtNewTokenNo.Text + "',MachineID='" + txtNewMachineID.Text + "',LocCode='" + ddlUnit.SelectedItem.Text + "'";
            query = query + " where   ExistingCode='" + ddlTokenNo.SelectedItem.Text + "'and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' ";
            objdata.RptEmployeeMultipleDetails(query);


            //Day_Attn_Record_Det_Mail5
            query = " update Day_Attn_Record_Det_Mail set ExistingCode='" + txtNewTokenNo.Text + "',MachineID='" + txtNewMachineID.Text + "',LocCode='" + ddlUnit.SelectedItem.Text + "'";
            query = query + " where   ExistingCode='" + ddlTokenNo.SelectedItem.Text + "'and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' ";
            objdata.RptEmployeeMultipleDetails(query);


            //  Day_Attn_Record_Det_Saved6
            query = " update Day_Attn_Record_Det_Saved set ExistingCode='" + txtNewTokenNo.Text + "',MachineID='" + txtNewMachineID.Text + "',LocCode='" + ddlUnit.SelectedItem.Text + "'";
            query = query + " where   ExistingCode='" + ddlTokenNo.SelectedItem.Text + "'and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' ";
            objdata.RptEmployeeMultipleDetails(query);


            //  Employee_Mst_Status7
            query = " update Employee_Mst_Status set Token_No='" + txtNewTokenNo.Text + "',MachineID='" + txtNewMachineID.Text + "',LocCode='" + ddlUnit.SelectedItem.Text + "'";
            query = query + " where   Token_No='" + ddlTokenNo.SelectedItem.Text + "'and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' ";
            objdata.RptEmployeeMultipleDetails(query);

            //  ExperienceDetails8
            query = " update ExperienceDetails set ExistingCode='" + txtNewTokenNo.Text + "',MachineID='" + txtNewMachineID.Text + "',EmpNo='" + txtNewMachineID.Text + "',LocCode='" + ddlUnit.SelectedItem.Text + "'";
            query = query + " where   ExistingCode='" + ddlTokenNo.SelectedItem.Text + "'and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' ";
            objdata.RptEmployeeMultipleDetails(query);

            // IFReport_DayAttendance9
            query = " update IFReport_DayAttendance set ExistingCode='" + txtNewTokenNo.Text + "',MachineID='" + txtNewMachineID.Text + "',LocCode='" + ddlUnit.SelectedItem.Text + "' ";
            query = query + " where   ExistingCode='" + ddlTokenNo.SelectedItem.Text + "'and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' ";
            objdata.RptEmployeeMultipleDetails(query);

            // Leave_History10
            query = " update Leave_History set ExistingCode='" + txtNewTokenNo.Text + "',Machine_No='" + txtNewMachineID.Text + "',EmpNo='" + txtNewMachineID.Text + "',LocCode='" + ddlUnit.SelectedItem.Text + "'  ";
            query = query + " where   ExistingCode='" + ddlTokenNo.SelectedItem.Text + "'and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' ";
            objdata.RptEmployeeMultipleDetails(query);

            // Leave_Register_Mst11
            query = " update Leave_Register_Mst set ExistingCode='" + txtNewTokenNo.Text + "',Machine_No='" + txtNewMachineID.Text + "',EmpNo='" + txtNewMachineID.Text + "',LocCode='" + ddlUnit.SelectedItem.Text + "' ";
            query = query + " where   ExistingCode='" + ddlTokenNo.SelectedItem.Text + "'and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' ";
            objdata.RptEmployeeMultipleDetails(query);

            // Log_ImproperDays12
            query = " update Log_ImproperDays set ExistingCode='" + txtNewTokenNo.Text + "',MachineID='" + txtNewMachineID.Text + "',LocCode='" + ddlUnit.SelectedItem.Text + "' ";
            query = query + " where   ExistingCode='" + ddlTokenNo.SelectedItem.Text + "'and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' ";
            objdata.RptEmployeeMultipleDetails(query);

            // LogTime_Days13
            query = " update LogTime_Days set ExistingCode='" + txtNewTokenNo.Text + "',MachineID='" + txtNewMachineID.Text + "',LocCode='" + ddlUnit.SelectedItem.Text + "' ";
            query = query + " where   ExistingCode='" + ddlTokenNo.SelectedItem.Text + "'and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' ";
            objdata.RptEmployeeMultipleDetails(query);

            // ManAttn_Details14
            query = " update ManAttn_Details set ExistingCode='" + txtNewTokenNo.Text + "',Machine_No='" + txtNewMachineID.Text + "',EmpNo='" + txtNewMachineID.Text + "',LocCode='" + ddlUnit.SelectedItem.Text + "'  ";
            query = query + " where   ExistingCode='" + ddlTokenNo.SelectedItem.Text + "'and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' ";
            objdata.RptEmployeeMultipleDetails(query);

            // Medical_GatePass15
            query = " update Medical_GatePass set TokenID='" + txtNewTokenNo.Text + "',MachineID='" + txtNewMachineID.Text + "',Lcode='" + ddlUnit.SelectedItem.Text + "' ";
            query = query + " where   TokenID='" + ddlTokenNo.SelectedItem.Text + "'and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' ";
            objdata.RptEmployeeMultipleDetails(query);

            // MedicalDetails16
            query = " update MedicalDetails set TokenID='" + txtNewTokenNo.Text + "',MachineID='" + txtNewMachineID.Text + "',Lcode='" + ddlUnit.SelectedItem.Text + "' ";
            query = query + " where   TokenID='" + ddlTokenNo.SelectedItem.Text + "'and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' ";
            objdata.RptEmployeeMultipleDetails(query);

            // Salary_Update_Det17
            query = " update Salary_Update_Det set ExistingCode='" + txtNewTokenNo.Text + "',MachineID='" + txtNewMachineID.Text + "' ,LocCode='" + ddlUnit.SelectedItem.Text + "'";
            query = query + " where   ExistingCode='" + ddlTokenNo.SelectedItem.Text + "'and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' ";
            objdata.RptEmployeeMultipleDetails(query);

            // SalaryAck_Details18
            query = " update SalaryAck_Details set ExistingCode='" + txtNewTokenNo.Text + "',MachineID='" + txtNewMachineID.Text + "',EmpNo='" + txtNewMachineID.Text + "',LocCode='" + ddlUnit.SelectedItem.Text + "'  ";
            query = query + " where  ExistingCode='" + ddlTokenNo.SelectedItem.Text + "'and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' ";
            objdata.RptEmployeeMultipleDetails(query);


            // ShiftChange_Mst19
            query = " update ShiftChange_Mst set ExistingCode='" + txtNewTokenNo.Text + "',Machine_No='" + txtNewMachineID.Text + "',EmpNo='" + txtNewMachineID.Text + "' ,LocCode='" + ddlUnit.SelectedItem.Text + "' ";
            query = query + " where  ExistingCode='" + ddlTokenNo.SelectedItem.Text + "'and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' ";
            objdata.RptEmployeeMultipleDetails(query);

            //ShiftReport20
            query = " update ShiftReport set MachineNo='" + txtNewMachineID.Text + "' ,Lcode='" + ddlUnit.SelectedItem.Text + "'";
            query = query + " where  MachineNo='" + Empno + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
            objdata.RptEmployeeMultipleDetails(query);

            //SpinIncentiveDetPart21
            query = " update SpinIncentiveDetPart set TokenNo='" + txtNewTokenNo.Text + "',MachineID='" + txtNewMachineID.Text + "',LocCode='" + ddlUnit.SelectedItem.Text + "' ";
            query = query + " where  TokenNo='" + ddlTokenNo.SelectedItem.Text + "'and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' ";
            objdata.RptEmployeeMultipleDetails(query);

            // Training_Level_Change22
            query = " update Training_Level_Change set ExistingCode='" + txtNewTokenNo.Text + "',MachineID='" + txtNewMachineID.Text + "',EmpNo='" + txtNewMachineID.Text + "',Lcode='" + ddlUnit.SelectedItem.Text + "' ";
            query = query + " where  ExistingCode='" + ddlTokenNo.SelectedItem.Text + "'and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' ";
            objdata.RptEmployeeMultipleDetails(query);


            // UnpaidVoucher_Salary_Confirm23
            query = " update UnpaidVoucher_Salary_Confirm set ExistingCode='" + txtNewTokenNo.Text + "',EmpNo='" + txtNewMachineID.Text + "',LocCode='" + ddlUnit.SelectedItem.Text + "' ";
            query = query + " where  ExistingCode='" + ddlTokenNo.SelectedItem.Text + "'and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' ";
            objdata.RptEmployeeMultipleDetails(query);


            // Weekly_OTHours24
            query = " update Weekly_OTHours set ExistingCode='" + txtNewTokenNo.Text + "',MachineID='" + txtNewMachineID.Text + "',LocCode='" + ddlUnit.SelectedItem.Text + "' ";
            query = query + " where  ExistingCode='" + ddlTokenNo.SelectedItem.Text + "'and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' ";
            objdata.RptEmployeeMultipleDetails(query);

            // LogTime_BONUS25
            query = " update LogTime_BONUS set MachineID='" + MachineEncrypt_New + "',LocCode='" + ddlUnit.SelectedItem.Text + "' ";
            query = query + " where  MachineID='" + MachineEncrypt + "'and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' ";
            objdata.RptEmployeeMultipleDetails(query);


            // LogTime_IN26
            query = " update LogTime_IN set MachineID='" + MachineEncrypt_New + "',LocCode='" + ddlUnit.SelectedItem.Text + "' ";
            query = query + " where  MachineID='" + MachineEncrypt + "'and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' ";
            objdata.RptEmployeeMultipleDetails(query);

            // LogTime_OUT27
            query = " update LogTime_OUT set MachineID='" + MachineEncrypt_New + "',LocCode='" + ddlUnit.SelectedItem.Text + "' ";
            query = query + " where  MachineID='" + MachineEncrypt + "'and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' ";
            objdata.RptEmployeeMultipleDetails(query);

            // LogTime_SALARY28
            query = " update LogTime_SALARY set MachineID='" + MachineEncrypt_New + "' ,LocCode='" + ddlUnit.SelectedItem.Text + "'";
            query = query + " where  MachineID='" + MachineEncrypt + "'and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' ";
            objdata.RptEmployeeMultipleDetails(query);

            // LogTimeHostel_Lunch29
            query = " update LogTimeHostel_Lunch set MachineID='" + MachineEncrypt_New + "',LocCode='" + ddlUnit.SelectedItem.Text + "' ";
            query = query + " where  MachineID='" + MachineEncrypt + "'and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' ";
            objdata.RptEmployeeMultipleDetails(query);


            //LogTimeLunch_IN30
            query = " update LogTimeLunch_IN set MachineID='" + MachineEncrypt_New + "',LocCode='" + ddlUnit.SelectedItem.Text + "' ";
            query = query + " where  MachineID='" + MachineEncrypt + "'and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' ";
            objdata.RptEmployeeMultipleDetails(query);

            //LogTimeLunch_OUT31
            query = " update LogTimeLunch_OUT set MachineID='" + MachineEncrypt_New + "',LocCode='" + ddlUnit.SelectedItem.Text + "' ";
            query = query + " where  MachineID='" + MachineEncrypt + "'and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' ";
            objdata.RptEmployeeMultipleDetails(query);


            query = "update Employee_Mst Set IsActive='No' where ExistingCode='" + ddlTokenNo.SelectedItem.Text + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' ";
            objdata.RptEmployeeMultipleDetails(query);


        }
        else
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Token No does not Exists..Register the Employee Details first.!');", true);

        }


    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        ddlTokenNo.SelectedValue="-Select-";
        ddlUnit.SelectedValue = "-Select-";
        txtNewTokenNo.Text = ""; txtNewMachineID.Text = "";
    }

}
