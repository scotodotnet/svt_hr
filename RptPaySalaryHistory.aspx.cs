﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

using Payroll;
using Payroll.Data;
using Payroll.Configuration;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

public partial class RptPaySalaryHistory : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    DateTime TransDate;
    string SessionAdmin;
    string Stafflabour;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string SessionUserName;
    string SessionUserID;
    string SessionRights;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        if (!IsPostBack)
        {
            Load_Department();
            Load_EmpNo();
        }
    }

    private void Load_Department()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddldept.Items.Clear();
        query = "Select *from Department_Mst";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddldept.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["DeptCode"] = "0";
        dr["DeptName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddldept.DataTextField = "DeptName";
        ddldept.DataValueField = "DeptCode";
        ddldept.DataBind();
    }

    private void Load_EmpNo()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtEmployeeNo.Items.Clear();
        query = "Select CONVERT(varchar(10), EmpNo) as EmpNo from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        query = query + " And IsActive='Yes' And DeptName='" + ddldept.SelectedItem.Text.ToString() + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtEmployeeNo.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpNo"] = "0";
        dr["EmpNo"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtEmployeeNo.DataTextField = "EmpNo";
        txtEmployeeNo.DataValueField = "EmpNo";
        txtEmployeeNo.DataBind();


        txtEmployeeName.Items.Clear();
        query = "Select CONVERT(varchar(10), EmpNo) as EmpNo,FirstName from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        query = query + " And IsActive='Yes' And DeptName='" + ddldept.SelectedItem.Text.ToString() + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtEmployeeName.DataSource = dtdsupp;
        DataRow dr1 = dtdsupp.NewRow();
        dr1["EmpNo"] = "0";
        dr1["FirstName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr1, 0);
        txtEmployeeName.DataTextField = "FirstName";
        txtEmployeeName.DataValueField = "EmpNo";
        txtEmployeeName.DataBind();

        txtTokenNo.Items.Clear();
        query = "Select ExistingCode from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        query = query + " And IsActive='Yes' And DeptName='" + ddldept.SelectedItem.Text.ToString() + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtTokenNo.DataSource = dtdsupp;
        DataRow dr2 = dtdsupp.NewRow();
        dr2["ExistingCode"] = "0";
        dr2["ExistingCode"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr2, 0);
        txtTokenNo.DataTextField = "ExistingCode";
        txtTokenNo.DataValueField = "ExistingCode";
        txtTokenNo.DataBind();
    }

    protected void ddldept_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_EmpNo();
    }

    protected void txtEmployeeNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (txtEmployeeNo.SelectedValue != "0")
        {
            txtEmployeeName.SelectedValue = txtEmployeeNo.SelectedValue;

            string checkTokenNo = txtEmployeeName.SelectedValue.ToString();
            DataTable Token_DT = new DataTable();
            string query = "";
            query = "Select ExistingCode from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            query = query + " And IsActive='Yes' And DeptName='" + ddldept.SelectedItem.Text.ToString() + "'";
            query = query + " And EmpNo='" + txtEmployeeNo.SelectedValue + "'";
            Token_DT = objdata.RptEmployeeMultipleDetails(query);
            if (Token_DT.Rows.Count.ToString() != "0")
            {
                txtTokenNo.SelectedValue = Token_DT.Rows[0]["ExistingCode"].ToString();
                //txtTokenSearch.Text = Token_DT.Rows[0]["ExisistingCode"].ToString();
            }
        }
        else
        {
            txtEmployeeNo.SelectedValue = "0";
            txtEmployeeName.SelectedValue = "0";
            txtTokenNo.SelectedValue = "0";
        }
    }

    protected void txtEmployeeName_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (txtEmployeeName.SelectedValue != "0")
        {
            txtEmployeeNo.SelectedValue = txtEmployeeName.SelectedValue;

            string checkTokenNo = txtEmployeeName.SelectedValue.ToString();
            DataTable Token_DT = new DataTable();
            string query = "";
            query = "Select ExistingCode from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            query = query + " And IsActive='Yes' And DeptName='" + ddldept.SelectedItem.Text.ToString() + "'";
            query = query + " And EmpNo='" + txtEmployeeNo.SelectedValue + "'";
            Token_DT = objdata.RptEmployeeMultipleDetails(query);
            if (Token_DT.Rows.Count.ToString() != "0")
            {
                txtTokenNo.SelectedValue = Token_DT.Rows[0]["ExistingCode"].ToString();
                //txtTokenSearch.Text = Token_DT.Rows[0]["ExisistingCode"].ToString();
            }
        }
        else
        {
            txtEmployeeNo.SelectedValue = "0";
            txtEmployeeName.SelectedValue = "0";
            txtTokenNo.SelectedValue = "0";
        }
    }

    protected void txtTokenNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (txtTokenNo.SelectedValue != "0")
        {
            //txtEmployeeNo.SelectedValue = txtEmployeeName.SelectedValue;

            string checkTokenNo = txtEmployeeName.SelectedValue.ToString();
            DataTable Token_DT = new DataTable();
            string query = "";
            query = "Select EmpNo from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            query = query + " And IsActive='Yes' And DeptName='" + ddldept.SelectedItem.Text.ToString() + "'";
            query = query + " And ExistingCode='" + txtTokenNo.SelectedValue + "'";
            Token_DT = objdata.RptEmployeeMultipleDetails(query);
            if (Token_DT.Rows.Count.ToString() != "0")
            {
                txtEmployeeName.SelectedValue = Token_DT.Rows[0]["EmpNo"].ToString();
                txtEmployeeNo.SelectedValue = Token_DT.Rows[0]["EmpNo"].ToString();
                //txtTokenSearch.Text = Token_DT.Rows[0]["ExisistingCode"].ToString();
            }
        }
        else
        {
            txtEmployeeNo.SelectedValue = "0";
            txtEmployeeName.SelectedValue = "0";
            txtTokenNo.SelectedValue = "0";
        }
    }

    protected void btnReportView_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        if ((txtTokenNo.SelectedValue == "") || (txtTokenNo.SelectedValue == "0"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Token No...');", true);
            ErrFlag = true;
        }
        //else if (txtFromDate.Text.Trim() == "")
        //{
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the from Date Properly.');", true);
        //    //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        //    ErrFlag = true;
        //}
        //else if (txtToDate.Text.Trim() == "")
        //{
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the To Date Properly.');", true);
        //    //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        //    ErrFlag = true;
        //}

        if (!ErrFlag)
        {
            if (ddlcategory.SelectedValue == "STAFF")
            {
                Stafflabour = "STAFF";
            }
            else if (ddlcategory.SelectedValue == "LABOUR")
            {
                Stafflabour = "LABOUR";
            }
            string EmpNo_Search = txtEmployeeNo.SelectedValue.ToString();
            string Token_No_Search = txtTokenNo.SelectedValue.ToString();
            string Employee_Type_Search = "";
            string Employee_Type_Code = "";
            //Get Employee Type
            string query = "";
            DataTable Token_DT = new DataTable();
            query = "Select ET.EmpTypeCd,ET.EmpType from Employee_Mst ED inner join MstEmployeeType ET on ED.Wages=ET.EmpType ";
            query = query + " where ED.CompCode='" + SessionCcode + "' and ED.LocCode='" + SessionLcode + "'";
            query = query + " and ED.EmpNo='" + EmpNo_Search + "'";
            Token_DT = objdata.RptEmployeeMultipleDetails(query);
            if (Token_DT.Rows.Count != 0)
            {
                Employee_Type_Code = Token_DT.Rows[0]["EmpTypeCd"].ToString();
                Employee_Type_Search = Token_DT.Rows[0]["EmpType"].ToString();
            }
            else
            {
                Employee_Type_Code = "";
                Employee_Type_Search = "";
            }

            ResponseHelper.Redirect("ViewReport.aspx?Cate=" + Stafflabour + "&Depat=" + ddldept.SelectedValue + "&Months=" + "" + "&yr=" + "2014" + "&fromdate=" + txtFromDate.Text.ToString() + "&ToDate=" + txtToDate.Text.ToString() + "&Salary=" + "" + "&CashBank=" + "" + "&ESICode=" + "" + "&EmpTypeCd=" + Employee_Type_Code + "&EmpType=" + Employee_Type_Search + "&PayslipType=" + "" + "&PFTypePost=" + "" + "&Left_Emp=" + "" + "&Leftdate=" + "" + "&EmpNo_Search=" + EmpNo_Search + "&Report_Type=Employee_Salary_History", "_blank", "");

        }


    }

    protected void btnTokenSearch_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        if (txtTokenSearch.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Enter the Token No....!');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Department Properly....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            DataTable Token_DT = new DataTable();
            string query = "";
            query = "Select ExistingCode as ExisistingCode,FirstName as EmpName,EmpNo,CatName as StafforLabor,DeptName as Department,DeptCode from Employee_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
            query = query + " and ExistingCode='" + txtTokenSearch.Text + "'";
            Token_DT = objdata.RptEmployeeMultipleDetails(query);
            if (Token_DT.Rows.Count != 0)
            {
                if (Token_DT.Rows[0]["StafforLabor"].ToString().ToUpper() == "STAFF".ToString().ToUpper())
                {
                    ddlcategory.SelectedValue = "STAFF";
                }
                else if (Token_DT.Rows[0]["StafforLabor"].ToString().ToUpper() == "LABOUR".ToString().ToUpper())
                {
                    ddlcategory.SelectedValue = "LABOUR";
                }
                //ddlcategory_SelectedIndexChanged(sender, e);

                DataTable Dept_DT = new DataTable();
                query = "Select *from Department_Mst where DeptName='" + Token_DT.Rows[0]["Department"].ToString() + "'";
                Dept_DT = objdata.RptEmployeeMultipleDetails(query);

                if (Dept_DT.Rows.Count != 0)
                {
                    ddldept.SelectedValue = Dept_DT.Rows[0]["DeptCode"].ToString();
                    ddldept_SelectedIndexChanged(sender, e);
                }
                txtTokenNo.SelectedValue = Token_DT.Rows[0]["ExisistingCode"].ToString();
                txtEmployeeNo.SelectedValue = Token_DT.Rows[0]["EmpNo"].ToString();
                txtEmployeeName.SelectedValue = Token_DT.Rows[0]["EmpNo"].ToString();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('This Token No Not Fount in Database....!');", true);
                //System.Windows.Forms.MessageBox.Show("Select the Department Properly....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
        }
    }

}
