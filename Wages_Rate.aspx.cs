﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Wages_Rate : System.Web.UI.Page
{

    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SSQL;
    string SessionEpay;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        SessionEpay = Session["SessionEpay"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Wages Rate";
           
            LoadWages();
           
        }
       // Load_OLD();
    }

  

    private void LoadWages()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlWages.Items.Clear();
        ddlwageLunch.Items.Clear();
        ddlWageOT.Items.Clear();
        ddlWagesMasthiri.Items.Clear();
        query = "Select *from MstEmployeeType";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlWages.DataSource = dtdsupp;
        ddlwageLunch.DataSource = dtdsupp;
        ddlWageOT.DataSource = dtdsupp;
        ddlWagesMasthiri.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpType"] = "-Select-";
        dr["EmpType"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlWages.DataTextField = "EmpType";
        ddlwageLunch.DataTextField = "EmpType";
        ddlWageOT.DataTextField = "EmpType";
        ddlWages.DataValueField = "EmpType";
        ddlwageLunch.DataValueField = "EmpType";
        ddlWageOT.DataValueField = "EmpType";
        ddlWagesMasthiri.DataTextField = "EmpType";
        ddlWagesMasthiri.DataValueField = "EmpType";
        ddlWagesMasthiri.DataBind();
        ddlWageOT.DataBind();
        ddlWages.DataBind();
        ddlwageLunch.DataBind();
      
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        bool ErrFlg = false;

        if (ddlWages.SelectedItem.Text == "-Select-" || ddlWages.SelectedItem.Value == "-Select-")
        {
            ddlWages.Focus();
            ErrFlg = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please Select the Wages Type');", true);
            return;
        }

        if (!ErrFlg)
        {
            string Query = "";
            Query = "Delete from [" + SessionEpay + "]..Wages_Amt where Wages='" + ddlWages.SelectedItem.Text + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";

            objdata.RptEmployeeMultipleDetails(Query);

            Query = "Insert into [" + SessionEpay + "]..Wages_Amt values('" + SessionCcode + "','" + SessionLcode + "','" + ddlWages.SelectedItem.Text.ToUpper() + "','" + txtDay1.Text + "','" + txtDay2.Text + "','" + txtDay3.Text + "','" + txtDay4.Text + "','" + txtDay5.Text + "','" + txtDay6.Text + "','" + txtDay7.Text + "');";
            objdata.RptEmployeeMultipleDetails(Query);
            btnClr_Click(sender, e);
           // Load_OLD();
        }
    }

    protected void btnClr_Click(object sender, EventArgs e)
    {
        ddlWages.ClearSelection();
        txtDay1.Text = "";
        txtDay2.Text = "";
        txtDay3.Text = "";
        txtDay4.Text = "";
        txtDay5.Text = "";
        txtDay6.Text = "";
        txtDay7.Text = "";
    }



    protected void ddlWages_SelectedIndexChanged(object sender, EventArgs e)
    {
        if(ddlWages.SelectedItem.Text!= "-Select-")
        {
            SSQL = "Select * from [" + SessionEpay + "]..Wages_Amt where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Wages='" + ddlWages.SelectedItem.Text + "'";
            DataTable dt = new DataTable();
            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt.Rows.Count > 0)
            {
                txtDay1.Text = dt.Rows[0]["Day1"].ToString();
                txtDay2.Text = dt.Rows[0]["Day2"].ToString();
                txtDay3.Text = dt.Rows[0]["Day3"].ToString();
                txtDay4.Text = dt.Rows[0]["Day4"].ToString();
                txtDay5.Text = dt.Rows[0]["Day5"].ToString();
                txtDay6.Text = dt.Rows[0]["Day6"].ToString();
                txtDay7.Text = dt.Rows[0]["Day7"].ToString();
            }
            else
            {
                txtDay1.Text = "0";
                txtDay2.Text = "0";
                txtDay3.Text = "0";
                txtDay4.Text = "0";
                txtDay5.Text = "0";
                txtDay6.Text = "0";
                txtDay7.Text = "0";
            }
        }
    }
    protected void btnLuch_Click(object sender, EventArgs e)
    {
        if (ddlwageLunch.SelectedItem.Text == "-Select-" | txtLunch.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please Check the Lunch Fields!!!');", true);
        }
        else
        {
            if (btnLuch.Text == "Update")
            {
                btnLuch.Text = "Save";

                SSQL = "";
                SSQL = "Delete from [" + SessionEpay + "]..MstLunch where Wages='" + ddlWages.SelectedItem.Text + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);

                SSQL = "";
                SSQL = "Insert into [" + SessionEpay + "]..MstLunch values('" + SessionCcode + "','" + SessionLcode + "','" + ddlWages.SelectedItem.Text + "','" + txtLunch.Text + "')";
                objdata.RptEmployeeMultipleDetails(SSQL);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Wages Updated Successfully!!!');", true);
                btnLunchClr_Click(sender, e);
                
            }
            else
            {
                SSQL = "";
                SSQL = "Insert into [" + SessionEpay + "]..MstLunch values('" + SessionCcode + "','" + SessionLcode + "','" + ddlWages.SelectedItem.Text + "','" + txtLunch.Text + "')";
                objdata.RptEmployeeMultipleDetails(SSQL);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Wages Saved Successfully!!!');", true);
                btnLunchClr_Click(sender, e);
                
            }
        }
    }
    protected void btnLunchClr_Click(object sender, EventArgs e)
    {
        ddlwageLunch.ClearSelection();
        txtLunch.Text = "";
    }
    protected void ddlwageLunch_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlwageLunch.SelectedItem.Text != "-Select-")
        {
            SSQL = "Select * from [" + SessionEpay + "]..MstLunch where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and Wages='" + ddlwageLunch.SelectedItem.Text + "'";
            DataTable dt = new DataTable();
            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt.Rows.Count > 0)
            {
                txtLunch.Text = dt.Rows[0]["Luch"].ToString();
                
            }
            else
            {
                txtLunch.Text = "0";
            }
        }
    }
    protected void ddlWageOT_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlWageOT.SelectedItem.Text != "-Select-")
        {
            SSQL = "Select * from [" + SessionEpay + "]..MstOT where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and Wages='" + ddlWageOT.SelectedItem.Text + "'";
            DataTable dt = new DataTable();
            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt.Rows.Count > 0)
            {
                txtOT.Text = dt.Rows[0]["OT"].ToString();

            }
            else
            {
                txtOT.Text = "0";
            }
        }
    }
   
        protected void btnOT_Click(object sender, EventArgs e)
    {
        if (ddlWageOT.SelectedItem.Text == "-Select-" | txtOT.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please Check the OT Fields!!!');", true);
        }
        else
        {
     
                SSQL = "";
                SSQL = "Delete from [" + SessionEpay + "]..MstOT where Wages='" + ddlWageOT.SelectedItem.Text + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);

                SSQL = "";
                SSQL = "Insert into [" + SessionEpay + "]..MstOT values('" + SessionCcode + "','" + SessionLcode + "','" + ddlWageOT.SelectedItem.Text + "','" + txtOT.Text + "')";
                objdata.RptEmployeeMultipleDetails(SSQL);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Wages Updated Successfully!!!');", true);
                btnOTClr_Click(sender,e);
            
        
        }
    }
        protected void btnOTClr_Click(object sender, EventArgs e)
        {
            ddlWageOT.ClearSelection();
            txtOT.Text = "";
        }
        protected void ddlWagesMasthiri_SelectedIndexChanged(object sender, EventArgs e)
        {
            string SSQL = "";
            if (ddlWagesMasthiri.SelectedItem.Text != "-Select-")
            {
                SSQL = "Select * from [" + SessionEpay + "]..Masthiri_Inc where Wages='" + ddlWagesMasthiri.SelectedItem.Text + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                DataTable Masthiri_Inc_DT = new DataTable();
                Masthiri_Inc_DT = objdata.RptEmployeeMultipleDetails(SSQL);
                if (Masthiri_Inc_DT.Rows.Count > 0)
                {
                    txtAmountMasthiri.Text = Masthiri_Inc_DT.Rows[0]["Amt"].ToString();
                }
                else
                {
                    txtAmountMasthiri.Text = "0";
                }
            }
        }
        protected void btnMasthiriSave_Click(object sender, EventArgs e)
        {
            bool ErrFlg = false;

            if (ddlWagesMasthiri.SelectedItem.Text == "-Select-" || ddlWagesMasthiri.SelectedItem.Value == "-Select-")
            {
                ddlWagesMasthiri.Focus();
                ErrFlg = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please Select the Wages Type');", true);
                return;
            }

            if (!ErrFlg)
            {
                string Query = "";
                Query = "Delete from [" + SessionEpay + "]..Masthiri_Inc where Wages='" + ddlWagesMasthiri.SelectedItem.Text + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";

                objdata.RptEmployeeMultipleDetails(Query);

                Query = "Insert into [" + SessionEpay + "]..Masthiri_Inc values('" + SessionCcode + "','" + SessionLcode + "','" + ddlWagesMasthiri.SelectedItem.Text.ToUpper() + "','" + txtAmountMasthiri.Text + "');";
                objdata.RptEmployeeMultipleDetails(Query);

            }
        }
        protected void btnMasthiriClr_Click(object sender, EventArgs e)
        {
            ddlWagesMasthiri.ClearSelection();
            txtAmountMasthiri.Text = "";
        }
}