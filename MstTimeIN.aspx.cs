﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
public partial class MstTimeIN : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights; string SessionAdmin;
    string SSQL;
    bool ErrFlag = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
      if(!IsPostBack)
        Load_OlD();
    }

    private void Load_OlD()
    {
        SSQL = "";
        SSQL = "Select * from MstTimeIN where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        DataTable TimeIn_DT = new DataTable();
        TimeIn_DT = objdata.RptEmployeeMultipleDetails(SSQL);
        if (TimeIn_DT.Rows.Count > 0)
        {
            txtTIStartMnts1.Text = TimeIn_DT.Rows[0][2].ToString();
            txtTIEndMnts1.Text = TimeIn_DT.Rows[0][3].ToString();
            txtTIStartMnts2.Text = TimeIn_DT.Rows[1][2].ToString();
            txtTIEndMnts2.Text = TimeIn_DT.Rows[1][3].ToString();
            txtTITotalMnts1.Text = TimeIn_DT.Rows[0][4].ToString();
            txtTITotalMnts2.Text = TimeIn_DT.Rows[1][4].ToString();
        }
        else
        {
            txtTIStartMnts1.Text = "0";
            txtTIEndMnts1.Text = "0";
            txtTIStartMnts2.Text = "0";
            txtTIEndMnts2.Text = "0";
            txtTITotalMnts1.Text = "0";
            txtTITotalMnts2.Text = "0";
        }

        SSQL = "";
        SSQL = "Select * from MstTimeOT where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        DataTable OTTime = new DataTable();
        OTTime = objdata.RptEmployeeMultipleDetails(SSQL);
        if (OTTime.Rows.Count > 0)
        {
            txtOTStartMnts1.Text = OTTime.Rows[0][2].ToString();
            txtOTEndMnts1.Text = OTTime.Rows[0][3].ToString();
            txtOTStartMnts2.Text = OTTime.Rows[1][2].ToString();
            txtOTEndMnts2.Text = OTTime.Rows[1][3].ToString();
            txtOTTotalMnts1.Text = OTTime.Rows[0][4].ToString();
            txtOTTotalMnts2.Text = OTTime.Rows[1][4].ToString();
        }
        else
        {
            txtOTStartMnts1.Text = "0";
            txtOTEndMnts1.Text = "0";
            txtOTStartMnts2.Text = "0";
            txtOTEndMnts2.Text = "0";
            txtOTTotalMnts1.Text = "0";
            txtOTTotalMnts2.Text = "0";
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (txtTIStartMnts1.Text == "" || txtTIStartMnts2.Text == ""|| txtTIEndMnts1.Text == "" || txtTIEndMnts2.Text == "" ||  txtTITotalMnts1.Text == "" || txtTITotalMnts2.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please Check the Time Fields....');", true);
            return;
        }
        if (!ErrFlag)
        {
            SSQL = "";
            SSQL = "Delete From MstTimeIN where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "";
            SSQL = "Insert Into MstTimeIN values('" + SessionCcode + "','" + SessionLcode + "','" + txtTIStartMnts1.Text + "','" + txtTIEndMnts1.Text + "','" + txtTITotalMnts1.Text + "');";
            objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "";
            SSQL = "Insert Into MstTimeIN values('" + SessionCcode + "','" + SessionLcode + "','" + txtTIStartMnts2.Text + "','" + txtTIEndMnts2.Text + "','" + txtTITotalMnts2.Text + "');";
            objdata.RptEmployeeMultipleDetails(SSQL);

            Load_OlD();
        }
    }

    protected void btnClr_Click(object sender, EventArgs e)
    {
        txtTIStartMnts1.Text = "0";
        txtTIEndMnts1.Text = "0";
        txtTIStartMnts2.Text = "0";
        txtTIEndMnts2.Text = "0";
        txtTITotalMnts1.Text = "0";
        txtTITotalMnts2.Text = "0";
    }

    protected void btnOTSave_Click(object sender, EventArgs e)
    {
        if (txtOTStartMnts1.Text == "" || txtOTStartMnts2.Text == "" || txtOTEndMnts1.Text == "" || txtOTEndMnts2.Text == "" || txtOTTotalMnts1.Text == "" || txtOTTotalMnts2.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please Check the OT Fields....');", true);
            return;
        }
        if (!ErrFlag)
        {
            SSQL = "";
            SSQL = "Delete From MstTimeOT where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "";
            SSQL = "Insert Into MstTimeOT values('" + SessionCcode + "','" + SessionLcode + "','" + txtOTStartMnts1.Text + "','" + txtOTEndMnts1.Text + "','" + txtOTTotalMnts1.Text + "');";
            objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "";
            SSQL = "Insert Into MstTimeOT values('" + SessionCcode + "','" + SessionLcode + "','" + txtOTStartMnts2.Text + "','" + txtOTEndMnts2.Text + "','" + txtOTTotalMnts2.Text + "');";
            objdata.RptEmployeeMultipleDetails(SSQL);

            Load_OlD();
        }
    }

    protected void btnOTClr_Click(object sender, EventArgs e)
    {
        txtOTStartMnts1.Text = "0";
        txtOTEndMnts1.Text = "0";
        txtOTStartMnts2.Text = "0";
        txtOTEndMnts2.Text = "0";
        txtOTTotalMnts1.Text = "0";
        txtOTTotalMnts2.Text = "0";
    }
}