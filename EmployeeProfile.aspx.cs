﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.IO;
using System.Data.SqlClient;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.Collections.Generic;
using System.IO;



public partial class EmployeeProfile : System.Web.UI.Page
{

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;

    string SSQL;
    //string mvarUserType = "IF User";
    string mvarUserType;
    DataTable mDataSet = new DataTable();
    DataTable dt = new DataTable();
    BALDataAccess objdata = new BALDataAccess();
    DataTable AutoDataTable = new DataTable();
    string iEmpDet = "";
    //char[] delimiters = new char[] { "-",">" };

    string[] delimiters = { "-", ">" };

    string mUnitName = "";
    string strPhotoPath = "";
    FileStream fs;
    BinaryReader br;
    DataRow drow;
    string Empcode;
    DataSet ds = new DataSet();
    string Division;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Report-Employee Full Report";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("SalaryProcess"));
                //li.Attributes.Add("class", "droplink active open");
            }


            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();

            Division = Request.QueryString["Division"].ToString();

            if (SessionUserType == "2")
            {
                NonAdminGetEmpProfileTable();
            }
            else
            {

                GetEmpProfileTable();
            }

            ds.Tables.Add(AutoDataTable);
            ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("crystal/EmployeeProfile.rpt"));

            report.Database.Tables[0].SetDataSource(ds.Tables[0]);

            if (Division != "-Select-")
            {
                report.DataDefinition.FormulaFields["Division_Name_Str"].Text = "'" + "Division:" + "'" + "'" + Division + "'";
            }
            else
            {
                report.DataDefinition.FormulaFields["Division_Name_Str"].Text = "'" + "Division:" + "'";
            }
            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;


        }
    }


    public void NonAdminGetEmpProfileTable()
    {
        AutoDataTable.Columns.Add("CompanyName");
        AutoDataTable.Columns.Add("LocationName");
        AutoDataTable.Columns.Add("EmpNo");
        AutoDataTable.Columns.Add("CatName");
        AutoDataTable.Columns.Add("SubCatName");
        AutoDataTable.Columns.Add("TypeName");
        AutoDataTable.Columns.Add("ExistingCode");
        AutoDataTable.Columns.Add("MachineID");
        AutoDataTable.Columns.Add("FirstName");
        AutoDataTable.Columns.Add("LastName");
        AutoDataTable.Columns.Add("Gender");
        AutoDataTable.Columns.Add("DeptName");
        AutoDataTable.Columns.Add("Designation");
        AutoDataTable.Columns.Add("img", System.Type.GetType("System.Byte[]"));


        SSQL = "";
        SSQL = "select isnull(EmpNo,'') as [EmpNo]";
        SSQL += ",isnull(CatName,'') as [CatName]";
        SSQL += ",isnull(SubCatName,'') as [SubCatName]";
        SSQL += ",isnull(TypeName,'') as [TypeName]";
        SSQL += ",isnull(ExistingCode,'') as [ExistingCode]";
        SSQL += ",isnull(MachineID,'') as [MachineID]";
        SSQL += ",isnull(FirstName,'') as [FirstName]";
        SSQL += ",isnull(MiddleInitial,'') as [LastName]";
        SSQL += ",isnull(Gender,'') as [Gender]";
        SSQL += ",isnull(DeptName,'') as [DeptName]";
        SSQL += ",isnull(Designation,'') as [Designation]";
        SSQL += " from employee_mst";
        SSQL += " Where CompCode='" + SessionCcode + "'";
        SSQL += " and LocCode='" + SessionLcode + "' And IsActive='Yes' and Eligible_PF ='1";

        if (Division != "-Select-")
        {
            SSQL += " and Division='" + Division + "'";
        }
        mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);

        if (mDataSet.Rows.Count > 0)
        {

            for (int iRow = 0; iRow < mDataSet.Rows.Count; iRow++)
            {


                DataTable DT_Photo = new DataTable();
                string SS = "Select *from Photo_Path_Det";
                DT_Photo = objdata.RptEmployeeMultipleDetails(SS);

                string PhotoDet = "";
                string UNIT_Folder = "";
                if (DT_Photo.Rows.Count != 0)
                {
                    PhotoDet = DT_Photo.Rows[0]["Photo_Path"].ToString();
                }
                //if (SessionLcode.ToUpper() == "UNIT I") { UNIT_Folder = "Emp_Scan/UNIT_I/"; }
                //if (SessionLcode.ToUpper() == "UNIT II") { UNIT_Folder = "Emp_Scan/UNIT_II/"; }
                //if (SessionLcode.ToUpper() == "UNIT III") { UNIT_Folder = "Emp_Scan/UNIT_III/"; }
                //if (SessionLcode.ToUpper() == "UNIT IV") { UNIT_Folder = "//SABA-PC/To Share/Narmatha/Emp_Scan/UNIT_IV/"; } //UNIT_Folder = "Emp_Scan/UNIT_IV/"; }

                if (SessionLcode.ToUpper() == "UNIT I") { UNIT_Folder = PhotoDet + "/UNIT_I/"; }
                if (SessionLcode.ToUpper() == "UNIT II") { UNIT_Folder = PhotoDet + "/UNIT_II/"; }
                if (SessionLcode.ToUpper() == "UNIT III") { UNIT_Folder = PhotoDet + "/UNIT_III/"; }
                if (SessionLcode.ToUpper() == "UNIT IV") { UNIT_Folder = PhotoDet + "/UNIT_IV/"; } //UNIT_Folder = "Emp_Scan/UNIT_IV/"; }


                byte[] imageByteData = new byte[0];
                string mid = mDataSet.Rows[iRow]["MachineID"].ToString();
                // string path = "D:/Photo/" + mid + ".jpg";

                string path = UNIT_Folder + "/Photos/" + mid + ".jpg";

                if (System.IO.File.Exists(path))
                {
                    imageByteData = System.IO.File.ReadAllBytes(path);

                }

                else
                {
                    string filepath = HttpContext.Current.Request.MapPath("~/assets/images/No_Image.jpg");

                    imageByteData = System.IO.File.ReadAllBytes(filepath);

                }

                AutoDataTable.NewRow();
                AutoDataTable.Rows.Add();



                AutoDataTable.Rows[iRow]["CompanyName"] = SessionCcode;
                AutoDataTable.Rows[iRow]["LocationName"] = SessionLcode;
                AutoDataTable.Rows[iRow]["EmpNo"] = mDataSet.Rows[iRow]["EmpNo"].ToString();
                AutoDataTable.Rows[iRow]["CatName"] = mDataSet.Rows[iRow]["CatName"].ToString();

                AutoDataTable.Rows[iRow]["SubCatName"] = mDataSet.Rows[iRow]["SubCatName"].ToString();
                AutoDataTable.Rows[iRow]["TypeName"] = mDataSet.Rows[iRow]["TypeName"].ToString();
                AutoDataTable.Rows[iRow]["ExistingCode"] = mDataSet.Rows[iRow]["ExistingCode"].ToString();
                AutoDataTable.Rows[iRow]["MachineID"] = mDataSet.Rows[iRow]["MachineID"].ToString();

                AutoDataTable.Rows[iRow]["FirstName"] = mDataSet.Rows[iRow]["FirstName"].ToString();
                AutoDataTable.Rows[iRow]["LastName"] = mDataSet.Rows[iRow]["LastName"].ToString();
                AutoDataTable.Rows[iRow]["Gender"] = mDataSet.Rows[iRow]["Gender"].ToString();
                AutoDataTable.Rows[iRow]["DeptName"] = mDataSet.Rows[iRow]["DeptName"].ToString();
                AutoDataTable.Rows[iRow]["Designation"] = mDataSet.Rows[iRow]["Designation"].ToString();
                AutoDataTable.Rows[iRow]["img"] = imageByteData;
            }
        }



    }

    public void GetEmpProfileTable()
    {
        AutoDataTable.Columns.Add("CompanyName");
        AutoDataTable.Columns.Add("LocationName");
        AutoDataTable.Columns.Add("EmpNo");
        AutoDataTable.Columns.Add("CatName");
        AutoDataTable.Columns.Add("SubCatName");
        AutoDataTable.Columns.Add("TypeName");
        AutoDataTable.Columns.Add("ExistingCode");
        AutoDataTable.Columns.Add("MachineID");
        AutoDataTable.Columns.Add("FirstName");
        AutoDataTable.Columns.Add("LastName");
        AutoDataTable.Columns.Add("Gender");
        AutoDataTable.Columns.Add("DeptName");
        AutoDataTable.Columns.Add("Designation");
        AutoDataTable.Columns.Add("img", System.Type.GetType("System.Byte[]"));


        SSQL = "";
        SSQL = "select isnull(EmpNo,'') as [EmpNo]";
        SSQL += ",isnull(CatName,'') as [CatName]";
        SSQL += ",isnull(SubCatName,'') as [SubCatName]";
        SSQL += ",isnull(TypeName,'') as [TypeName]";
        SSQL += ",isnull(ExistingCode,'') as [ExistingCode]";
        SSQL += ",isnull(MachineID,'') as [MachineID]";
        SSQL += ",isnull(FirstName,'') as [FirstName]";
        SSQL += ",isnull(MiddleInitial,'') as [LastName]";
        SSQL += ",isnull(Gender,'') as [Gender]";
        SSQL += ",isnull(DeptName,'') as [DeptName]";
        SSQL += ",isnull(Designation,'') as [Designation]";
        SSQL += " from employee_mst";
        SSQL += " Where CompCode='" + SessionCcode + "'";
        SSQL += " and LocCode='" + SessionLcode + "' And IsActive='Yes'";

        if (Division != "-Select-")
        {
            SSQL += " and Division='" + Division + "'";
        }
        mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);

        if (mDataSet.Rows.Count > 0)
        {

            for (int iRow = 0; iRow < mDataSet.Rows.Count; iRow++)
            {
                DataTable DT_Photo = new DataTable();
                string SS = "Select * from Photo_Path_Det";
                DT_Photo = objdata.RptEmployeeMultipleDetails(SS);

                string PhotoDet = "";
                string UNIT_Folder = "";
                if (DT_Photo.Rows.Count != 0)
                {
                    PhotoDet = DT_Photo.Rows[0]["Photo_Path"].ToString();
                }
                //if (SessionLcode.ToUpper() == "UNIT I") { UNIT_Folder = "Emp_Scan/UNIT_I/"; }
                //if (SessionLcode.ToUpper() == "UNIT II") { UNIT_Folder = "Emp_Scan/UNIT_II/"; }
                //if (SessionLcode.ToUpper() == "UNIT III") { UNIT_Folder = "Emp_Scan/UNIT_III/"; }
                //if (SessionLcode.ToUpper() == "UNIT IV") { UNIT_Folder = "//SABA-PC/To Share/Narmatha/Emp_Scan/UNIT_IV/"; } //UNIT_Folder = "Emp_Scan/UNIT_IV/"; }

                if (SessionLcode.ToUpper() == "UNIT I") { UNIT_Folder = PhotoDet + "/UNIT_I/"; }
                if (SessionLcode.ToUpper() == "UNIT II") { UNIT_Folder = PhotoDet + "/UNIT_II/"; }
                if (SessionLcode.ToUpper() == "UNIT III") { UNIT_Folder = PhotoDet + "/UNIT_III/"; }
                if (SessionLcode.ToUpper() == "UNIT IV") { UNIT_Folder = PhotoDet + "/UNIT_IV/"; } //UNIT_Folder = "Emp_Scan/UNIT_IV/"; }


                byte[] imageByteData = new byte[0];
                string mid = mDataSet.Rows[iRow]["MachineID"].ToString();
                // string path = "D:/Photo/" + mid + ".jpg";

                string path = UNIT_Folder + "/Photos/" + mid + ".jpg";

                if (System.IO.File.Exists(path))
                {
                    imageByteData = System.IO.File.ReadAllBytes(path);
                }

                else
                {
                    string filepath = HttpContext.Current.Request.MapPath("~/assets/images/No_Image.jpg");

                    imageByteData = System.IO.File.ReadAllBytes(filepath);
                }


                AutoDataTable.NewRow();
                AutoDataTable.Rows.Add();



                AutoDataTable.Rows[iRow]["CompanyName"] = SessionCcode;
                AutoDataTable.Rows[iRow]["LocationName"] = SessionLcode;
                AutoDataTable.Rows[iRow]["EmpNo"] = mDataSet.Rows[iRow]["EmpNo"].ToString();
                AutoDataTable.Rows[iRow]["CatName"] = mDataSet.Rows[iRow]["CatName"].ToString();

                AutoDataTable.Rows[iRow]["SubCatName"] = mDataSet.Rows[iRow]["SubCatName"].ToString();
                AutoDataTable.Rows[iRow]["TypeName"] = mDataSet.Rows[iRow]["TypeName"].ToString();
                AutoDataTable.Rows[iRow]["ExistingCode"] = mDataSet.Rows[iRow]["ExistingCode"].ToString();
                AutoDataTable.Rows[iRow]["MachineID"] = mDataSet.Rows[iRow]["MachineID"].ToString();

                AutoDataTable.Rows[iRow]["FirstName"] = mDataSet.Rows[iRow]["FirstName"].ToString();
                AutoDataTable.Rows[iRow]["LastName"] = mDataSet.Rows[iRow]["LastName"].ToString();
                AutoDataTable.Rows[iRow]["Gender"] = mDataSet.Rows[iRow]["Gender"].ToString();
                AutoDataTable.Rows[iRow]["DeptName"] = mDataSet.Rows[iRow]["DeptName"].ToString();
                AutoDataTable.Rows[iRow]["Designation"] = mDataSet.Rows[iRow]["Designation"].ToString();
                AutoDataTable.Rows[iRow]["img"] = imageByteData;
            }
        }
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        CrystalReportViewer1.Dispose();
    }
}
