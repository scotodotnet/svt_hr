﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;

public partial class Form12 : System.Web.UI.Page
{
    DataTable dtdlocation = new DataTable();
    string fromdate;
    string todate;
    string Wages;
    BALDataAccess objdata = new BALDataAccess();
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    DataTable AutoDTable = new DataTable();
    DataTable DataCells = new DataTable();
    string SessionUserType;
    string SSQL = "";
    string mIpAddress_IN;
    string mIpAddress_OUT;
    DataTable dsEmployee = new DataTable();
    DateTime date1 = new DateTime();
    DateTime date2 = new DateTime();
    string RegNo;
   
    string Address1;
    string Address2;
    string city;
    string Pincode;
    string PF_DOJ_Get_str;
    DateTime PF_DOJ_Get;



    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report-FORM 12";
            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();

            fromdate = Request.QueryString["FromDate"].ToString();
            todate = Request.QueryString["ToDate"].ToString();
            string TempWages = Request.QueryString["WagesType"].ToString();
            Wages = TempWages.Replace("_", "&");

            //DataTable dtIPaddress = new DataTable();
            //dtIPaddress = objdata.BelowFourHours(SessionCcode.ToString(), SessionLcode.ToString());


            //if (dtIPaddress.Rows.Count > 0)
            //{
            //    for (int i = 0; i < dtIPaddress.Rows.Count; i++)
            //    {
            //        if (dtIPaddress.Rows[i]["IPMode"].ToString() == "IN")
            //        {
            //            mIpAddress_IN = dtIPaddress.Rows[i]["IPAddress"].ToString();
            //        }
            //        else if (dtIPaddress.Rows[i]["IPMode"].ToString() == "OUT")
            //        {
            //            mIpAddress_OUT = dtIPaddress.Rows[i]["IPAddress"].ToString();
            //        }
            //    }
            //}


            System.Web.UI.WebControls.DataGrid grid =
                                 new System.Web.UI.WebControls.DataGrid();


            string Location_Full_Address_Join = "";
            DataTable dt = new DataTable();
            string Compname = "";
            SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "'";
            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt.Rows.Count != 0)
            {
                SessionCompanyName = dt.Rows[0]["CompName"].ToString();
            }



            SSQL = "Select * from Location_Mst where CompCode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
            dtdlocation = objdata.RptEmployeeMultipleDetails(SSQL);

            if (dtdlocation.Rows.Count > 0)
            {
                RegNo = dtdlocation.Rows[0]["Register_No"].ToString();
                Address1 = dtdlocation.Rows[0]["Add1"].ToString();
                Address2 = dtdlocation.Rows[0]["Add2"].ToString();
                city = dtdlocation.Rows[0]["City"].ToString();
                Pincode = dtdlocation.Rows[0]["Pincode"].ToString();

                if (Address1 != "")
                {
                    Location_Full_Address_Join = Location_Full_Address_Join + Address1;
                }
                if (Address2 != "")
                {
                    Location_Full_Address_Join = Location_Full_Address_Join + "," + Address2;
                }
                if (city != "")
                {
                    Location_Full_Address_Join = Location_Full_Address_Join + "," + city;
                }
                if (Pincode != "")
                {
                    Location_Full_Address_Join = Location_Full_Address_Join + "," + Pincode;
                }
            }
            else
            {
                RegNo = "";
            }

            //grid.DataSource = AutoDTable;
            //grid.DataBind();
            string attachment = "attachment;filename=Form12.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);

            Response.Write("<table border='1'; word-wrap:break-word;>");


            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='3'  border='0'>");
            Response.Write("</td>");

            Response.Write("<td colspan='5'>");
            Response.Write("<a style=\"font-weight:bold\">Form-12</a>");
            Response.Write("</td>");

            Response.Write("<td colspan='3'  border: 0;>");
            Response.Write("</td>");

            Response.Write("</tr>");

            Response.Write("<tr Font-Bold='true' align='center'>");

            Response.Write("<td colspan='3' BorderColor='White'>");
            Response.Write("</td>");

            Response.Write("<td colspan='5'>");
            Response.Write("<a style=\"font-weight:bold\">(Register of adult workers and young persons.)</a>");
            Response.Write("</td>");

            Response.Write("<td colspan='3' BorderColor='White'>");
            Response.Write("</td>");

            Response.Write("</tr>");


            Response.Write("<tr Font-Bold='true' align='center'>");

            Response.Write("<td rowspan='2' colspan='3' valign='middle'>");
            Response.Write("<a style=\"font-weight:bold\">Name and address of the factory :</a>");
            Response.Write("</td>");

            Response.Write("<td Border=true  colspan='5' align='center'>");
            Response.Write("<a style=\"font-weight:bold\">" + SessionCompanyName + "</a>");
            Response.Write(" ");
            Response.Write("<a style=\"font-weight:bold\">" + SessionLcode + "</a>");
            Response.Write("</td>");

            Response.Write("<td Border=true  colspan='1' align='center'>");
            Response.Write("<a style=\"font-weight:bold\">Register No: " + RegNo + "</a>");
            Response.Write("</td>");

            Response.Write("<td Border=true  colspan='2' align='center'>");
            Response.Write("<a style=\"font-weight:bold\">Month & Year: " + date1.ToString("MMMM") + " " + date1.Year.ToString() + "</a>");
            Response.Write("</td>");

            Response.Write("</tr>");

            Response.Write("<tr Font-Bold='true' align='center'>");

            Response.Write("<td colspan='5'>");
            Response.Write("<a style=\"font-weight:bold\">" + Location_Full_Address_Join.ToString() + "</a>");
            Response.Write("</td>");

            Response.Write("<td colspan='3' BorderColor='White'>");
            Response.Write("</td>");

            Response.Write("</tr>");


            Response.Write("<tr align='center'>");

            Response.Write("<td Font-Bold='true' align='Middle'>");
            Response.Write("<a style=\"font-weight:bold\" align='center'>S.No</a>");
            Response.Write("</td>");


            Response.Write("<td Font-Bold='true' align='center' valign='middle'>");
            Response.Write("<a style=\"font-weight:bold;text-align:center;vertical-align:middle\">Name and residential adress of the worker</a>");
            Response.Write("</td>");

            Response.Write("<td Font-Bold='true' align='Middle'>");
            Response.Write("<a style=\"font-weight:bold\">Father's Name</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">Date of first entry into Service</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\" style=\'word-wrap: break-word\'>Nature of work in which employed</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">Letter of group as in Form No:11</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">Number of relay, if working in shifts</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">Serial number of certificate of fitness issued by certifying surgeon and its date</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">Token number with reference to certificate of fitness number and date of certificate, if an adolescent</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\"> Number and date of certificate, if an adolocent.</a>");
            Response.Write("</td>");

            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">Remarks</a>");
            Response.Write("</td>");

            Response.Write("</tr>");

           
            date1 = Convert.ToDateTime(fromdate);
            date2 = Convert.ToDateTime(todate);
            int dayCount = (int)((date2 - date1).TotalDays);

            //PF Date of Joining Condition Add
            SSQL = "";
            SSQL = "Select * from Employee_Mst where CompCode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
            SSQL = SSQL + " And Wages='" + Wages + "' And IsActive='Yes'";
            SSQL = SSQL + " And (CONVERT(DateTime,PFDOJ, 103) >= '" + date1.ToString("yyyy/MM/dd") + "'";
            SSQL = SSQL + " and (CONVERT(DateTime,PFDOJ, 103) <= '" + date2.ToString("yyyy/MM/dd") + "'))";
            //Check User Type
            if (SessionUserType == "2")
            {
                SSQL = SSQL + " And Eligible_PF='1'";
            }
            SSQL = SSQL + "Order by ExistingCode Asc";



            //SSQL = "Select * from Employee_Mst where CompCode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
            //SSQL = SSQL + " And Wages='" + Wages + "' And IsActive='Yes'";
            ////Check User Type
            //if (SessionUserType == "IF User")
            //{
            //    SSQL = SSQL + " And Eligible_PF='1'";
            //}
            //SSQL = SSQL + "Order by ExistingCode Asc";

            dsEmployee = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dsEmployee.Rows.Count > 0)
            {
                for (int i = 0; i < dsEmployee.Rows.Count; i++)
                {
                    Response.Write("<tr align='center'>");

                    Response.Write("<td>");
                    Response.Write("<a style=\"font-weight:bold\" align='center'>" + (i + 1).ToString() + "</a>");
                    Response.Write("</td>");


                    Response.Write("<td>");
                    Response.Write("<a style=\"font-weight:bold;text-align:center;vertical-align:middle\">" + dsEmployee.Rows[i]["FirstName"].ToString() + dsEmployee.Rows[i]["LastName"].ToString() + "," + dsEmployee.Rows[i]["Address1"].ToString() + "</a>");
                    Response.Write("</td>");

                    Response.Write("<td>");
                    Response.Write("<a style=\"font-weight:bold\">" + dsEmployee.Rows[i]["LastName"].ToString() + "</a>");
                    Response.Write("</td>");

                    Response.Write("<td>");
                    Response.Write("<a style=\"font-weight:bold\">" + Convert.ToDateTime(dsEmployee.Rows[i]["DOJ"].ToString()).ToString("dd/MM/yyyy") + "</a>");
                    Response.Write("</td>");

                    Response.Write("<td>");
                    Response.Write("<table>");
                    Response.Write("<tr><td>");
                    Response.Write("<a style=\"font-weight:bold\" style=\'word-wrap: break-word\'>" + dsEmployee.Rows[i]["DeptName"].ToString() + "</a>");
                    Response.Write("</td></tr>");
                    Response.Write("<tr><td>");
                    Response.Write("<a style=\"font-weight:bold\" style=\'word-wrap: break-word\'>" + dsEmployee.Rows[i]["Designation"].ToString() + "</a>");
                    Response.Write("</td></tr>");
                    Response.Write("</table>");
                    Response.Write("</td>");

                    Response.Write("<td>");
                    Response.Write("<a style=\"font-weight:bold\"></a>");
                    Response.Write("</td>");

                    Response.Write("<td>");
                    Response.Write("<a style=\"font-weight:bold\"></a>");
                    Response.Write("</td>");

                    Response.Write("<td>");
                    Response.Write("<a style=\"font-weight:bold\"></a>");
                    Response.Write("</td>");

                    Response.Write("<td>");
                    Response.Write("<a style=\"font-weight:bold\">" + dsEmployee.Rows[i]["ExistingCode"].ToString() + "</a>");
                    Response.Write("</td>");

                    string MachineID = dsEmployee.Rows[i]["MachineID"].ToString();
                    DataTable dtdAdult = new DataTable();
                    string Certificate_No_Date_Join = "";

                    SSQL = "Select * from Adolcent_Emp_Det where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And MachineID='" + MachineID.ToString() + "'";
                    dtdAdult = objdata.RptEmployeeMultipleDetails(SSQL);
                    Response.Write("<td>");
                    if (dtdAdult.Rows.Count != 0)
                    {
                        for (int j = 0; j < dtdAdult.Rows.Count; j++)
                        {
                            if (j == 0) { Response.Write("<table>"); }
                            Certificate_No_Date_Join = dtdAdult.Rows[j]["Certificate_No"].ToString() + "/ " + dtdAdult.Rows[j]["Certificate_Date_Str"].ToString();
                            Response.Write("<tr><td>");
                            Response.Write("<a style=\"font-weight:bold\">" + Certificate_No_Date_Join + "</a>");
                            Response.Write("</td></tr>");
                            //if ((Certificate_No_Date_Join != ""))
                            //{
                            //    Certificate_No_Date_Join = (Certificate_No_Date_Join + "\n");
                            //}
                            //Certificate_No_Date_Join = Certificate_No_Date_Join + dtdAdult.Rows[j]["Certificate_No"].ToString() + "/ " + dtdAdult.Rows[j]["Certificate_Date_Str"].ToString();
                        }
                        Response.Write("</table>");
                    }
                    else
                    {
                        Response.Write("<a style=\"font-weight:bold\">" + Certificate_No_Date_Join + "</a>");
                    }
                    Response.Write("</td>");
                    //Response.Write("<td>");
                    //Response.Write("<a style=\"font-weight:bold\">" + Certificate_No_Date_Join + "</a>");
                    //Response.Write("</td>");

                    Response.Write("<td>");
                    Response.Write("<a style=\"font-weight:bold\"></a>");
                    Response.Write("</td>");

                    Response.Write("</tr>");

                }

            }
            Response.Write("</table>");
          
            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();
        }
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        CrystalReportViewer1.Dispose();
    }


}

   
        
    
