﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Globalization;

public partial class LongLeaveDaysReport : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string Division;
    string LeaveDays;

   

    BALDataAccess objdata = new BALDataAccess();

    string SSQL = "";
    DataTable AutoDTable = new DataTable();
    DataTable DataCell = new DataTable();
    DataTable dt = new DataTable();
    string FromDate;
    string ToDate;
  
    string WagesType;
    
     System.Web.UI.WebControls.DataGrid grid =
                            new System.Web.UI.WebControls.DataGrid();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {

            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report- Long Leave";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("ManualEntry"));
                //li.Attributes.Add("class", "droplink active open");
            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            Division = Request.QueryString["Division"].ToString();
            LeaveDays = Request.QueryString["LeaveDays"].ToString();
            string TempWages = Request.QueryString["Wages"].ToString();
            WagesType = TempWages.Replace("_", "&");
            //WagesType = Request.QueryString["Wages"].ToString();

            //////FromDate = Request.QueryString["FromDate"].ToString();
            //////ToDate = Request.QueryString["ToDate"].ToString();

            if (SessionUserType == "2")
            {
                // NonAdminBetweenDates();
            }

            else
            {
                AdminLongLeave();
            }
        }
    }

    public void AdminLongLeave()
    {
        int remainday=Convert.ToInt32(LeaveDays);
        
        DateTime Now = DateTime.Today;
        var Todat = Now.AddDays(-1);
        var fromdat = Todat.AddDays(-remainday);

        DataCell.Columns.Add("SNo");
        DataCell.Columns.Add("TokenNo");
        DataCell.Columns.Add("EmpName");
        DataCell.Columns.Add("Dept");
        DataCell.Columns.Add("Designation");
        DataCell.Columns.Add("Leave_Days");
        DataCell.Columns.Add("ContactNo");
        DataCell.Columns.Add("Taluk");
        DataCell.Columns.Add("District");
        DataCell.Columns.Add("Remarks");

        SSQL = "Select LD.ExistingCode,LD.FirstName,LD.DeptName,LD.Designation,EM.EmployeeMobile as ContactNo,";
        SSQL = SSQL + " EM.Taluk_Perm ,EM.Present_Dist,count(LD.Present) as Absent,LD.Present_Absent from ";
        SSQL = SSQL + " LogTime_Days LD  inner Join Employee_Mst EM on LD.ExistingCode=EM.ExistingCode ";
        SSQL = SSQL + " where CONVERT(DATETIME,Attn_Date_Str,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(fromdat).ToString("dd/MM/yyyy") + "',103) ";
        SSQL = SSQL + " and CONVERT(DATETIME,Attn_Date_Str,103)<= CONVERT(DATETIME,'" + Convert.ToDateTime(Todat).ToString("dd/MM/yyyy") + "',103) ";
        SSQL = SSQL + " and LD.Wages='"+WagesType +"' and LD.CompCode ='"+SessionCcode +"' and LD.LocCode ='"+SessionLcode+"' and ";
        SSQL = SSQL + " EM.CompCode ='"+SessionCcode +"' and Em.LocCode ='"+SessionLcode +"' ";
        SSQL = SSQL + " group by LD.ExistingCode,LD.FirstName,LD.DeptName,LD.Designation,EM.EmployeeMobile , ";
        SSQL = SSQL + " EM.Taluk_Perm ,EM.Present_Dist,LD.Present_Absent having SUM(LD.Present)='0.0' order by Absent desc";
        AutoDTable = objdata.RptEmployeeMultipleDetails(SSQL);


        SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "' ";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        string name = dt.Rows[0]["CompName"].ToString();

        if (AutoDTable.Rows.Count != 0)
        {
            int sno=1;
            for (int i = 0; i < AutoDTable.Rows.Count; i++)
            {
                DataCell.NewRow();
                DataCell.Rows.Add();

                DataCell.Rows[i]["SNo"] = sno;
                DataCell.Rows[i]["TokenNo"] = AutoDTable.Rows[i]["ExistingCode"].ToString();
                DataCell.Rows[i]["EmpName"] = AutoDTable.Rows[i]["FirstName"].ToString();
                DataCell.Rows[i]["Dept"] = AutoDTable.Rows[i]["DeptName"].ToString();
                DataCell.Rows[i]["Designation"] = AutoDTable.Rows[i]["Designation"].ToString();
                DataCell.Rows[i]["Leave_Days"] = LeaveDays;
                DataCell.Rows[i]["ContactNo"] = AutoDTable.Rows[i]["ContactNo"].ToString();
                DataCell.Rows[i]["Taluk"] = AutoDTable.Rows[i]["Taluk_Perm"].ToString();
                DataCell.Rows[i]["District"] = AutoDTable.Rows[i]["Present_Dist"].ToString();
                DataCell.Rows[i]["Remarks"] = "";

            }

            grid.DataSource = AutoDTable;
            grid.DataBind();
            string attachment = "attachment;filename=DAY ATTENDANCE BETWEEN DATES.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);
            Response.Write("<table>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='10'>");
            Response.Write("<a style=\"font-weight:bold\">" + name + "-" + SessionLcode +"</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='10'>");
            Response.Write("<a style=\"font-weight:bold\">Long Leave Details</a>");
            Response.Write("</td>");
            Response.Write("</tr>");

            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='10'>");
            Response.Write("<a> </a>");

            Response.Write("</td>");
            Response.Write("</tr>");
           
            Response.Write("</table>");
            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Records Found');", true);
        }
    }

}
