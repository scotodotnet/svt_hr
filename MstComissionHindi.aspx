﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="MstComissionHindi.aspx.cs" Inherits="MstComissionHindi" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<script src="assets/js/master_list_jquery.min.js"></script>
<script src="assets/js/master_list_jquery-ui.min.js"></script>
<link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
     $(document).ready(function() {
         $('#example').dataTable();
      
     });
</script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.select2').select2();
                $('#example').dataTable();
            }
        });
    };
</script>


<!-- begin #content -->
<div id="content" class="content">
    <ol class="breadcrumb pull-right">
	    <li class="active">Commission Master</li>
	</ol>
	<h1 class="page-header">Hindi Group Commission</h1>
	<div class="row">
        <div class="col-md-12">
		    <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">Hindi Group Commission</h4>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                     
                          <!-- begin row -->
			            <div class="row"> 
                           
			            <div class="col-md-3">
			            <div class="form-group">
                          <label>Wages Type</label>
                          <asp:DropDownList ID="ddlWages1" runat="server"  class="form-control select2" style="width:100%" OnSelectedIndexChanged="ddlWages1_SelectedIndexChanged" AutoPostBack="true">
                          </asp:DropDownList>
                        </div>
			            </div>
                             <!-- begin col-3 -->
			            <div class="col-md-2">
			            <div class="form-group">
                          <label>Amount</label>
                          <asp:TextBox ID="txtAmount" runat="server" CssClass="form-control" ></asp:TextBox>
                        </div>
			            </div>
                       <!-- end col-3 -->
                        </div>
                        <!-- end row -->
                        
                          <!-- begin row -->
			            <div class="row"> 
			            
                        </div>
                        <!-- end row -->
                          
                             <!-- begin row -->
			            <div class="row"> 
			             <!-- begin col-3 -->
			            <div class="col-md-2">
			            <div class="form-group">
                            <asp:Button ID="btnCommissionSv" runat="server" CssClass="btn btn-success" OnClick="btnCommissionSv_Click" Text="Save" />
                        
                            <asp:Button ID="btnCommissionClr" runat="server" CssClass="btn btn-danger" OnClick="btnCommissionClr_Click" Text="Clear" />
                        </div>
			            </div>
                       <!-- end col-3 -->
                        </div>
                        <!-- end row -->
                      <%-- <div class="panel">
                         
                        <h5>Hostel / Regular Basic Incentive Text</h5>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <asp:RadioButtonList ID="txtRdpHos_Rg_Incent_Txt" runat="server" RepeatColumns="3" class="form-control" RepeatDirection="Horizontal">
                                        <asp:ListItem Value="1" Text="Attendance" style="padding:5px;"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Production" style="padding:5px;"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Punctuality" style="padding:5px;"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                            <div class="col-md-4">
								<div class="form-group">
								    <br />
									<asp:Button runat="server" id="BtnHos_Rg_Incen" Text="Save" class="btn btn-success" />
						    	 </div>
                            </div>
                        </div>
                       
                       
                         <div class="row">
                            <div class="col-md-4">
							    <div class="form-group">
								    <label>Department Incentive Amount</label>
								    <asp:TextBox runat="server" ID="txtDeptIncAmt" Text="0.00" class="form-control" style="width:100%;"></asp:TextBox>
								</div>
                            </div>
                             <div class="col-md-4">
								<div class="form-group">
								    <br />
									<asp:Button runat="server" id="BtnDeptIncAmt" Text="Save" class="btn btn-success" />
						    	 </div>
                            </div>
                        </div>
                        </div>--%>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</asp:Content>

