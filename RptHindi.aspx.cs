﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using System.Data.Sql;
using System.Data.SqlClient;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;

public partial class HindiBoysCommision : System.Web.UI.Page
{
    string ShiftType1 = "";
    string Date = "";
    string Status = "";
    string fmDate = "";
    string Todate = "";
    string Wages = "";
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string SessionUserName;
    string SessionUserID;
    string SessionRights;
    string ss = "";
    string FromDate = "";
    string ToDate = "";


    ReportDocument report = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();

    string SSQL = "";

    DataTable AutoDTable = new DataTable();
    string Shift = "";

    DateTime date1;
    DateTime Date2 = new DateTime();

    string HGrpName;


    System.Web.UI.WebControls.DataGrid grid =
                           new System.Web.UI.WebControls.DataGrid();

    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Report-Hindi Group Commission";
                HGrpName = Request.QueryString["GroupName"].ToString();
                SessionCcode = Session["Ccode"].ToString();
                SessionLcode = Session["Lcode"].ToString();
                SessionUserName = Session["Usernmdisplay"].ToString();
                SessionUserID = Session["UserId"].ToString();
                SessionRights = Session["Rights"].ToString();
                SessionUserType = Session["Isadmin"].ToString();
                FromDate = Request.QueryString["FromDate"].ToString();
                ToDate = Request.QueryString["ToDate"].ToString();
            }
            Report();
        }
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
    public void Report()
    {
        //string SSQL = "";
        //SSQL = "Select * from HindiGroupCommission ";

        //if (HGrpName != "ALL")
        //{
        //    SSQL = SSQL + "where GroupName='" + HGrpName + "'";

        //}
        //    DataTable EmpDt = new DataTable();
        //    EmpDt = objdata.RptEmployeeMultipleDetails(SSQL);
        //    if (EmpDt.Rows.Count > 0)
        //    {
        //        AutoDTable.Columns.Add("GroupName");
        //        AutoDTable.Columns.Add("TotalDays");
        //        AutoDTable.Columns.Add("PerDay");
        //        AutoDTable.Columns.Add("GrandTotal");

        //        for (int EmpRw = 0; EmpRw < EmpDt.Rows.Count; EmpRw++)
        //        {
        //            string GroupName = "";
        //            Decimal PerDay = 0;
        //            Decimal GrandTotal = 0;
        //            Decimal TotalDays = 0;

        //            AutoDTable.NewRow();
        //            AutoDTable.Rows.Add();

        //            AutoDTable.Rows[AutoDTable.Rows.Count - 1]["GroupName"] = EmpDt.Rows[EmpRw]["GroupName"];
        //            AutoDTable.Rows[AutoDTable.Rows.Count - 1]["PerDay"] = EmpDt.Rows[EmpRw]["PerDay"];

        //            SSQL = "Select GroupName,PerDay,TotalDays from HindiGroupCommission";
        //            DataTable Group_DT = new DataTable();

        //            Group_DT = objdata.RptEmployeeMultipleDetails(SSQL);
        //            if (Group_DT.Rows.Count > 0)
        //            {
        //                if (Group_DT.Rows[0]["GroupName"].ToString() != "")
        //                {
        //                    GroupName = (Group_DT.Rows[0]["GroupName"].ToString());
        //                }



        //                //for (int i = 0; i < EmpDt.Rows.Count; i++)
        //                //{
        //                //    if (Group_DT.Rows[0]["GroupName"].ToString() != "")
        //                //    {
        //                //        GroupName = (Group_DT.Rows[1]["GroupName"].ToString());
        //                //    }
        //                else
        //                {
        //                    GroupName = "";
        //                }
        //            }
        //            //}
        //            else
        //            {
        //                GroupName = "";
        //            }
        //            if (Group_DT.Rows.Count > 0)
        //            {
        //                if (Group_DT.Rows[0]["PerDay"].ToString() != "")
        //                {
        //                    PerDay = Convert.ToDecimal(Group_DT.Rows[0]["PerDay"].ToString());
        //                }
        //                else
        //                {
        //                    PerDay = 0;
        //                }
        //            }
        //            else
        //            {
        //                PerDay = 0;
        //            }

        //            if (Group_DT.Rows.Count > 0)
        //            {
        //                TotalDays = Convert.ToDecimal(Group_DT.Rows[0]["TotalDays"]);

        //            }

        //            if (PerDay > 0)
        //            {

        //                GrandTotal = Convert.ToDecimal(PerDay * TotalDays);
        //                GrandTotal = Math.Round(Convert.ToDecimal(GrandTotal), 2, MidpointRounding.AwayFromZero);


        //                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["GroupName"] = EmpDt.Rows[EmpRw]["GroupName"];
        //                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TotalDays"] = EmpDt.Rows[EmpRw]["TotalDays"];
        //                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["PerDay"] = EmpDt.Rows[EmpRw]["PerDay"];
        //                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["GrandTotal"] = EmpDt.Rows[EmpRw]["GrandTotal"];

        //                ////  AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Group Name"] = EmpDt.Rows[EmpRw]["GroupName"];
        //                //  AutoDTable.Rows[AutoDTable.Rows.Count - 1]["GroupName"] = EmpDt.Rows[EmpRw]["GroupName"];
        //                //  // AutoDTable.Rows[AutoDTable.Rows.Count - 1]["GroupName"] = GroupName;
        //                //  AutoDTable.Rows[AutoDTable.Rows.Count - 1]["PerDay"] = PerDay;

        //                //  AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TotalDays"] = TotalDays;

        //                //  AutoDTable.Rows[AutoDTable.Rows.Count - 1]["GrandTotal"] = GrandTotal;
        //            }
        //        }
        //    }


        SSQL = "";
        SSQL = "Select ROW_NUMBER()  OVER (ORDER BY  EM.Hindi_Category) As [S.NO],EM.Hindi_Category as [NAME OF GROUP],Sum(LD.Present) as TOTALDAYS,'' as [PER DAY],'' as [GRAND TOTAL]";
        SSQL = SSQL + " from Employee_Mst EM inner join LogTime_Days LD on LD.MachineID=EM.MachineID";
        //SSQL = SSQL + " inner join MstCommissionHindi HG on HG.GroupName=EM.Hindi_Category";

       
        SSQL = SSQL + " where Convert(datetime,LD.Attn_Date,103)>=Convert(datetime,'" + FromDate.ToString() + "',103)";
        SSQL = SSQL + " and Convert(datetime,LD.Attn_Date,1030)<=Convert(datetime,'" + ToDate.ToString() + "',103)";
        SSQL = SSQL + " and EM.CompCode='" + SessionCcode + "' and EM.LocCode='" + SessionLcode + "' and LD.CompCode='" + SessionCcode + "' and LD.LocCode='" + SessionLcode + "'";
        //SSQL = SSQL + " and HG.Ccode='" + SessionCcode + "' and HG.Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " and EM.Hindi_Category!='' and EM.Hindi_Category!='NULL' and  EM.Hindi_Category!='Select'";
        if (HGrpName.ToString() != "ALL")
        {
            SSQL = SSQL + " and EM.Hindi_Category='" + HGrpName.ToString() + "'";
        }
        SSQL = SSQL + " group by EM.Hindi_Category order by EM.Hindi_Category asc";

        AutoDTable = objdata.RptEmployeeMultipleDetails(SSQL);

        if (AutoDTable != null && AutoDTable.Rows.Count > 0)
        {
            for(int Row = 0; Row < AutoDTable.Rows.Count; Row++)
            {
                string Amt = "0";
                SSQL = "";
                SSQL = "Select isnull(sum(Amt),'0') from MstCommissionHindi where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " and GroupName='" + AutoDTable.Rows[Row]["NAME OF GROUP"] + "'";
                con = new SqlConnection(constr);
                con.Open();
                SqlCommand cmd = new SqlCommand(SSQL,con);
                Amt = cmd.ExecuteScalar().ToString();
                con.Close();

                AutoDTable.Rows[Row]["PER DAY"] = Amt;
                AutoDTable.Rows[Row]["GRAND TOTAL"] = Math.Round(Convert.ToDecimal(Convert.ToDecimal(AutoDTable.Rows[Row]["PER DAY"])*Convert.ToDecimal(AutoDTable.Rows[Row]["TOTALDAYS"])), 2, MidpointRounding.AwayFromZero).ToString();

            }
        }


        if (AutoDTable.Rows.Count > 0)
        {
            grid.DataSource = AutoDTable;
            grid.DataBind();

            SSQL = "";
            SSQL = "Select Cname,LCode,Location,Address1 from [" + SessionRights + "]..AdminRights";
            SSQL = SSQL + " where Ccode='" + SessionCcode + "'";
            DataTable Comp_Dt = new DataTable();
            Comp_Dt = objdata.RptEmployeeMultipleDetails(SSQL);

            string CompName = "";
            string LName = "";
            string Address = "";

            if (Comp_Dt != null && Comp_Dt.Rows.Count > 0)
            {
                CompName = Comp_Dt.Rows[0]["Cname"].ToString();
                LName= Comp_Dt.Rows[0]["LCode"].ToString()+ "-"+Comp_Dt.Rows[0]["Location"].ToString();
                Address = Comp_Dt.Rows[0]["Address1"].ToString();
            }

            string attachment = "attachment;filename=Hindi Group Commission Report.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);
            Response.Write("<table>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan="+AutoDTable.Columns.Count+"> ");
            Response.Write("<a style=\"font-weight:bold\">"+CompName+"</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan=" + AutoDTable.Columns.Count + "> ");
            Response.Write("<a style=\"font-weight:bold\">" + LName + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan=" + AutoDTable.Columns.Count + ">");
            Response.Write("<a style=\"font-weight:bold\">HINDI GROUP COMMISSION REPORT -" + FromDate + "-" + ToDate + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("</table>");
            Response.Write(stw.ToString());
            Response.Write("<table border=1>");
            Response.Write("<tr>");
            Response.Write("<td colspan=2 align='center'> ");
            Response.Write("<a style=\"font-weight:bold\">TOTAL</a>");
            Response.Write("</td>");
            Response.Write("<td> =Sum(C5:C" + Convert.ToInt32(AutoDTable.Rows.Count + 4) + ")");
            Response.Write("</td>");
            Response.Write("<td> =Sum(D5:D" + Convert.ToInt32(AutoDTable.Rows.Count + 4) + ")");
            Response.Write("</td>");
            Response.Write("<td> =Sum(E5:E" + Convert.ToInt32(AutoDTable.Rows.Count + 4) + ")");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("</table>");
            Response.End();
            Response.Clear();
        }
    }
}






