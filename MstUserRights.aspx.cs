﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Security.Cryptography;
using System.IO;
using System.Text;

using Altius.BusinessAccessLayer.BALDataAccess;

public partial class MstUserRights : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionCcode;
    string SessionLcode;
    string SessionCompanyName;
    string SessionAdmin;
    string SessionLocationName;
    string SessionRights;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionRights = Session["Rights"].ToString();
        //SessionCompanyName = Session["CompanyName"].ToString();
        //SessionLocationName = Session["LocationName"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: User Form Rights";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");
            Module_Name_MenuName_And_UserName_Add();
            txtModuleName_SelectedIndexChanged(sender, e);
            Load_Module_Form_Details();
        }
    }

    private void Module_Name_MenuName_And_UserName_Add()
    {
        //User Name Add
        DataTable dtUser = new DataTable();
        string query = "";
        query = "Select * from [" + SessionRights + "]..MstUsers where CompCode='" + SessionCcode + "' And LocationCode='" + SessionLcode + "' And UserCode <> 'Scoto' And IsAdmin <> '1' order by UserCode Asc";
        dtUser = objdata.RptEmployeeMultipleDetails(query);
        txtUserName.DataSource = dtUser;
        txtUserName.DataTextField = "UserCode";
        txtUserName.DataValueField = "UserCode";
        txtUserName.DataBind();

        //Module Name Add
        DataTable dtcate = new DataTable();
        query = "Select * from [" + SessionRights + "]..Module_List"; //where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' order by ModuleName Asc";
        dtcate = objdata.RptEmployeeMultipleDetails(query);
        txtModuleName.DataSource = dtcate;
        txtModuleName.DataTextField = "ModuleName";
        txtModuleName.DataValueField = "ModuleID";
        txtModuleName.DataBind();

        ////Menu Name Add
        //DataTable dtMenu = new DataTable();
        //query = "Select * from [" + SessionRights + "]..Company_Module_MenuHead_Rights where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' order by MenuName Asc";
        //dtMenu = objdata.RptEmployeeMultipleDetails(query);
        //txtMenuName.DataSource = dtMenu;
        //txtMenuName.DataTextField = "MenuName";
        //txtMenuName.DataValueField = "MenuID";
        //txtMenuName.DataBind();
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        //GVModule.Rows[1].FindControl("chkAdd"). = "true";

        string query = "";
        DataTable DT_Check = new DataTable();
        DataTable qry_dt = new DataTable();
        string SaveMode = "";
        bool ErrFlag = false;

        string Module_ID_Encrypt = "0";
        string Module_ID = "0";
        string Module_Name = "0";

        string Menu_ID_Encrypt = "0";
        string Menu_ID = "0";
        string Menu_Name = "0";

        string UserName = "";

        //Module_ID = Decrypt(txtModuleName.SelectedValue.ToString()).ToString();
        Module_Name = txtModuleName.SelectedItem.ToString();
        Module_ID_Encrypt = txtModuleName.SelectedValue.ToString();

        //Menu_ID = Decrypt(txtMenuName.SelectedValue.ToString()).ToString();
        Menu_Name = txtMenuName.SelectedItem.ToString();
        Menu_ID_Encrypt = txtMenuName.SelectedValue.ToString();

        UserName = txtUserName.SelectedValue.ToString();

        query = "Delete from [" + SessionRights + "]..Company_Module_User_Rights where ModuleID='" + Module_ID_Encrypt + "' And MenuID='" + Menu_ID_Encrypt + "' And UserName='" + UserName + "'";
        query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        objdata.RptEmployeeMultipleDetails(query);

        foreach (GridViewRow gvsal in GVModule.Rows)
        {
            SaveMode = "Insert";


            string Form_ID_Encrypt = "0";
            string Form_ID = "0";
            string Form_Name = "0";

            CheckBox ChkSelect_chk = (CheckBox)gvsal.FindControl("chkSelect");
            string AddCheck = "0";
            string ModifyCheck = "0";
            string DeleteCheck = "0";
            string ViewCheck = "0";
            string ApproveCheck = "0";
            string PrintCheck = "0";


            //CheckBox ChkModify = (CheckBox)gvsal.FindControl("chkModify");
            //CheckBox ChkDelete = (CheckBox)gvsal.FindControl("chkDelete");
            //CheckBox ChkView = (CheckBox)gvsal.FindControl("chkView");
            //CheckBox ChkApprove = (CheckBox)gvsal.FindControl("chkApprove");
            //CheckBox ChkPrint = (CheckBox)gvsal.FindControl("chkPrintout");

            if (ChkSelect_chk.Checked == true) { AddCheck = "1"; }
            //if (ChkModify.Checked == true) { ModifyCheck = "1"; }
            //if (ChkDelete.Checked == true) { DeleteCheck = "1"; }
            //if (ChkView.Checked == true) { ViewCheck = "1"; }
            //if (ChkApprove.Checked == true) { ApproveCheck = "1"; }
            //if (ChkPrint.Checked == true) { PrintCheck = "1"; }

            Label FormID_lbl = (Label)gvsal.FindControl("FormID");
            Form_ID = FormID_lbl.Text.ToString();
            Form_Name = gvsal.Cells[1].Text.ToString();
            Form_ID_Encrypt = Form_ID.ToString();

            //if (ChkSelect_chk.Checked == true || ChkModify.Checked == true || ChkDelete.Checked == true || ChkView.Checked == true || ChkApprove.Checked == true || ChkPrint.Checked == true)
            if (ChkSelect_chk.Checked == true)
            {
                //Get Menu LIST
                string Menu_LI_ID = "";
                string Form_LI_ID = "";
                DataTable dtID = new DataTable();
                query = "Select * from [" + SessionRights + "]..Company_Module_MenuHead_Rights where ModuleID='" + Module_ID_Encrypt + "' And MenuID='" + Menu_ID_Encrypt + "'";
                query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                dtID = objdata.RptEmployeeMultipleDetails(query);
                if (dtID.Rows.Count != 0)
                {
                    Menu_LI_ID = dtID.Rows[0]["Menu_LI_ID"].ToString();
                }
                //Get Form LIST ID
                query = "Select * from [" + SessionRights + "]..Company_Module_Menu_Form_Rights where ModuleID='" + Module_ID_Encrypt + "' And MenuID='" + Menu_ID_Encrypt + "' And FormID='" + Form_ID_Encrypt + "'";
                query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                dtID = objdata.RptEmployeeMultipleDetails(query);
                if (dtID.Rows.Count != 0)
                {
                    Form_LI_ID = dtID.Rows[0]["Form_LI_ID"].ToString();
                }

                //Insert User Rights
                query = "Insert Into [" + SessionRights + "]..Company_Module_User_Rights(CompCode,LocCode,ModuleID,ModuleName,MenuID,MenuName,Menu_LI_ID,FormID,FormName,Form_LI_ID,UserName,AddRights) Values('" + SessionCcode + "','" + SessionLcode + "','" + Module_ID_Encrypt + "',";
                query = query + " '" + Module_Name + "','" + Menu_ID_Encrypt + "','" + Menu_Name + "','" + Menu_LI_ID + "','" + Form_ID_Encrypt + "','" + Form_Name + "','" + Form_LI_ID + "','" + UserName + "','" + AddCheck + "')";
                objdata.RptEmployeeMultipleDetails(query);
            }
        }

        if (SaveMode == "Insert")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('User Rights Details Saved Successfully');", true);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('User Rights Details Not Saved Properly...');", true);
        }

    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    private void Clear_All_Field()
    {
        chkAll.Checked = false;
        Load_Module_Form_Details();
    }

    protected void btnView_Click(object sender, EventArgs e)
    {

        Load_Module_Form_Details();

    }


    private void Load_Module_Form_Details()
    {

        string query = "";
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;

        string Module_ID_Encrypt = "";
        Module_ID_Encrypt = txtModuleName.SelectedValue.ToString();

        string Menu_ID_Encrypt = "";
        Menu_ID_Encrypt = txtMenuName.SelectedValue.ToString();

        if (Menu_ID_Encrypt == "") { Menu_ID_Encrypt = "0"; }

        DataTable DT = new DataTable();
        query = "Select FormID,FormName from [" + SessionRights + "]..Module_Menu_Form_List where ModuleID='" + Module_ID_Encrypt + "' And MenuID='" + Menu_ID_Encrypt + "'";
       // query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' order by FormID Asc";
        DT = objdata.RptEmployeeMultipleDetails(query);
        GVModule.DataSource = DT;
        GVModule.DataBind();

        if (GVModule.Rows.Count != 0)
        {
            GVPanel.Visible = true;
        }
        else
        {
            GVPanel.Visible = false;
        }
        Load_Module_Form_Rights();

    }

    private void Load_Module_Form_Rights()
    {
        chkAll.Visible = true;
        foreach (GridViewRow gvsal in GVModule.Rows)
        {
            string ModuleName = txtModuleName.SelectedItem.ToString();
            //string ModuleID = Decrypt(txtModuleName.SelectedValue.ToString()).ToString();
            string ModuleID_Encrypt = txtModuleName.SelectedValue.ToString();

            string MenuName = txtMenuName.SelectedItem.ToString();
            //string MenuID = Decrypt(txtMenuName.SelectedValue.ToString()).ToString();
            string MenuID_Encrypt = txtMenuName.SelectedValue.ToString();

            string UserName = txtUserName.SelectedItem.ToString();

            string FormName = "";
            string FormID = "0";
            string FormID_Encrypt = "0";

            Label FormID_lbl = (Label)gvsal.FindControl("FormID");
            FormID = FormID_lbl.Text.ToString();
            FormName = gvsal.Cells[1].Text.ToString();
            FormID_Encrypt = FormID.ToString();
            //Get Company Module Rights
            string query = "";
            DataTable DT = new DataTable();
            query = "Select * from [" + SessionRights + "]..Company_Module_User_Rights where ModuleID='" + ModuleID_Encrypt + "' And MenuID='" + MenuID_Encrypt + "' And FormID='" + FormID_Encrypt + "' And UserName='" + UserName + "'";
            query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                //User Rights Update in Grid
                //((CheckBox)gvsal.FindControl("chkSelect")).Checked = true;

                //User Rights Update in Grid
                if (DT.Rows[0]["AddRights"].ToString() == "1") { ((CheckBox)gvsal.FindControl("chkSelect")).Checked = true; }
                //if (DT.Rows[0]["ModifyRights"].ToString() == "1") { ((CheckBox)gvsal.FindControl("chkModify")).Checked = true; }
                //if (DT.Rows[0]["DeleteRights"].ToString() == "1") { ((CheckBox)gvsal.FindControl("chkDelete")).Checked = true; }
                //if (DT.Rows[0]["ViewRights"].ToString() == "1") { ((CheckBox)gvsal.FindControl("chkView")).Checked = true; }
                //if (DT.Rows[0]["ApproveRights"].ToString() == "1") { ((CheckBox)gvsal.FindControl("chkApprove")).Checked = true; }
                //if (DT.Rows[0]["PrintRights"].ToString() == "1") { ((CheckBox)gvsal.FindControl("chkPrintout")).Checked = true; }
            }
        }
    }

    private string Encrypt(string clearText)
    {
        string EncryptionKey = "MAKV2SPBNI99212";
        byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(clearBytes, 0, clearBytes.Length);
                    cs.Close();
                }
                clearText = Convert.ToBase64String(ms.ToArray());
            }
        }
        return clearText;
    }

    private string Decrypt(string cipherText)
    {
        string EncryptionKey = "MAKV2SPBNI99212";
        byte[] cipherBytes = Convert.FromBase64String(cipherText);
        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(cipherBytes, 0, cipherBytes.Length);
                    cs.Close();
                }
                cipherText = Encoding.Unicode.GetString(ms.ToArray());
            }
        }
        return cipherText;
    }

    protected void txtModuleName_SelectedIndexChanged(object sender, EventArgs e)
    {
        //Menu Name Add
        string query = "";
        DataTable dtMenu = new DataTable();
        if (txtModuleName.SelectedValue != "")
        {
            query = "Select * from [" + SessionRights + "]..Module_MenuHead_List where ModuleID='" + txtModuleName.SelectedValue + "' order by MenuName Asc"; //where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' ";
            dtMenu = objdata.RptEmployeeMultipleDetails(query);
            txtMenuName.DataSource = dtMenu;
            txtMenuName.DataTextField = "MenuName";
            txtMenuName.DataValueField = "MenuID";
            txtMenuName.DataBind();
            btnView_Click(sender, e);
        }
    }

    protected void txtMenuName_SelectedIndexChanged(object sender, EventArgs e)
    {
        //Menu Name Add
        if (txtMenuName.SelectedValue != "")
        {
            btnView_Click(sender, e);
        }
    }

    protected void txtUserName_SelectedIndexChanged(object sender, EventArgs e)
    {
        //Menu Name Add
        if (txtUserName.SelectedValue != "")
        {
            btnView_Click(sender, e);
        }
    }

    protected void chkAll_CheckedChanged(object sender, EventArgs e)
    {
        foreach (GridViewRow gvsal in GVModule.Rows)
        {
            //User Rights Update in Grid
            if (chkAll.Checked == true)
            {
                ((CheckBox)gvsal.FindControl("chkSelect")).Checked = true;
                //((CheckBox)gvsal.FindControl("chkModify")).Checked = true;
                //((CheckBox)gvsal.FindControl("chkDelete")).Checked = true;
                //((CheckBox)gvsal.FindControl("chkView")).Checked = true;
                //((CheckBox)gvsal.FindControl("chkApprove")).Checked = true;
                //((CheckBox)gvsal.FindControl("chkPrintout")).Checked = true;
            }
            else
            {
                ((CheckBox)gvsal.FindControl("chkSelect")).Checked = false;
                //((CheckBox)gvsal.FindControl("chkModify")).Checked = false;
                //((CheckBox)gvsal.FindControl("chkDelete")).Checked = false;
                //((CheckBox)gvsal.FindControl("chkView")).Checked = false;
                //((CheckBox)gvsal.FindControl("chkApprove")).Checked = false;
                //((CheckBox)gvsal.FindControl("chkPrintout")).Checked = false;
            }
        }
    }
}
