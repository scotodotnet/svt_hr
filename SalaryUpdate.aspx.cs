﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class SalaryUpdate : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SSQL = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();


        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Salary Update";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");
            Initial_Data_Referesh();
            Load_WagesType();
            Load_Data_EmpDet();
        }
        Load_Data();
    }


    private void Initial_Data_Referesh()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("MachineID", typeof(string)));
        dt.Columns.Add(new DataColumn("ExistingCode", typeof(string)));
        dt.Columns.Add(new DataColumn("FirstName", typeof(string)));
        dt.Columns.Add(new DataColumn("DOJ", typeof(string)));
        dt.Columns.Add(new DataColumn("DeptName", typeof(string)));
        dt.Columns.Add(new DataColumn("Payroll_EmpNo", typeof(string)));
        dt.Columns.Add(new DataColumn("OLD_Salary", typeof(string)));
        dt.Columns.Add(new DataColumn("New_Salary", typeof(string)));

        Repeater1.DataSource = dt;
        Repeater1.DataBind();

        ViewState["ItemTable"] = Repeater1.DataSource;

        //dt = Repeater1.DataSource;
    }

    private void Load_Data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];

        Repeater1.DataSource = dt;
        Repeater1.DataBind();

    }

    protected void GridApproveEnquiryClick(object sender, CommandEventArgs e)
    {
    }
    protected void GridCancelEnquiryClick(object sender, CommandEventArgs e)
    {
        DataTable DT = new DataTable();

        string MachineID = e.CommandName.ToString();
        string ExstCode = e.CommandArgument.ToString();
        string[] Existing = ExstCode.Split(',');
        string ExisitingCode = Existing[1].ToString();

        SSQL = "Select * from Salary_Update_Det where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " And Approve_Status='Pending' and Wages='" + ddlWages.SelectedItem.Text + "'";
        SSQL = SSQL + " And MachineID='" + MachineID + "' And ExistingCode='" + ExisitingCode + "'";
        DT = objdata.RptEmployeeMultipleDetails(SSQL);

        if (DT.Rows.Count != 0)
        {
            SSQL = "Delete from Salary_Update_Det where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And Approve_Status='Pending' and Wages='" + ddlWages.SelectedItem.Text + "'";
            SSQL = SSQL + " And MachineID='" + MachineID + "' And ExistingCode='" + ExisitingCode + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Salary Update Details Deleted Successfully.!');", true);
            ddlWages_SelectedIndexChanged(sender, e);
        }
    }
    private void Load_WagesType()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlWages.Items.Clear();
        query = "Select *from MstEmployeeType";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlWages.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpType"] = "-Select-";
        dr["EmpType"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlWages.DataTextField = "EmpType";
        ddlWages.DataValueField = "EmpType";
        ddlWages.DataBind();
    }

    private void Load_Data_EmpDet()
    {
        string query = "";
        DataTable DT = new DataTable();

        //Repeater1.DataSource = DT;
        //Repeater1.DataBind();

        txtMachineID.Items.Clear();
        query = "Select CONVERT(varchar(10), EmpNo) as EmpNo from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        query = query + " And Wages='" + ddlWages.SelectedItem.Text + "'";
        query = query + " And IsActive='Yes'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        txtMachineID.DataSource = DT;
        DataRow dr = DT.NewRow();
        dr["EmpNo"] = "-Select-";
        dr["EmpNo"] = "-Select-";
        DT.Rows.InsertAt(dr, 0);
        txtMachineID.DataTextField = "EmpNo";
        txtMachineID.DataValueField = "EmpNo";
        txtMachineID.DataBind();
    }

    protected void txtMachineID_SelectedIndexChanged(object sender, EventArgs e)
    {
        string query;
        DataTable DT = new DataTable();

        query = "Select * from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        query = query + " And EmpNo='" + txtMachineID.SelectedItem.Text + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            txtTokenNo.Text = DT.Rows[0]["ExistingCode"].ToString();
            txtEmpName.Text = DT.Rows[0]["FirstName"].ToString();
            txtDepartment.Text = DT.Rows[0]["DeptName"].ToString();
            txtDOJ.Text = Convert.ToDateTime(DT.Rows[0]["DOJ"]).ToString("dd/MM/yyyy");
            txtOldSalary.Text = DT.Rows[0]["BaseSalary"].ToString();
            
        }
        else
        {
            txtMachineID.SelectedValue = "-Select-";
            txtEmpName.Text = "";
            txtTokenNo.Text = "";
            txtDepartment.Text = "";
            txtDOJ.Text = "";
        }
        //Get_Current_Salary();
    }

    protected void ddlWages_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Data_EmpDet();

        bool ErrFlag = false;
        DataTable DT = new DataTable();
        string Salary_Type = "";
        string Approve_Status = "";

       
        DataTable Sal_Ds = new DataTable();
        SSQL = "Select * from Salary_Update_Det where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " And Approve_Status='Pending' and Wages='" + ddlWages.SelectedItem.Text + "'";
      //  SSQL = SSQL + " And Salary_Type='" + Salary_Type + "'";
        SSQL = SSQL + " Order by ExistingCode Asc";
        Sal_Ds = objdata.RptEmployeeMultipleDetails(SSQL);

        Repeater1.DataSource = Sal_Ds;
        Repeater1.DataBind();
        ViewState["ItemTable"] = Sal_Ds;
    }

    public void Get_Current_Salary()
    {
        string Payroll_Emp_No = "";
        DataTable Base_DS=new DataTable();
       
        //SSQL = "Select * from [" + SessionPayroll + "]..EmployeeDetails where BiometricID='" + txtMachineID.SelectedItem.Text + "' And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ExisistingCode='" + txtTokenNo.Text + "'";
        //Base_DS = objdata.RptEmployeeMultipleDetails(SSQL);
        //if (Base_DS.Rows.Count!=0)
        //{
        //    Payroll_Emp_No = Base_DS.Rows[0]["EmpNo"].ToString();
        //}

        ////Get Base Salary
        //SSQL = "Select cast(Base as decimal(18,2)) as Base_Salary from [" + SessionPayroll + "]..SalaryMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And EmpNo='" + Payroll_Emp_No + "'";
        //Base_DS = objdata.RptEmployeeMultipleDetails(SSQL);
        //if(Base_DS.Rows.Count!=0 )
        //{
        //    txtOldSalary.Text = Base_DS.Rows[0]["Base_Salary"].ToString();
        //}
        //else
        //{
        //    txtOldSalary.Text = "0.00";
        //}


        SSQL = "Select * from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " And IsActive='Yes' And Wages='" + ddlWages.SelectedItem.Text + "' And ExistingCode='" + txtTokenNo.Text + "'";
        Base_DS = objdata.RptEmployeeMultipleDetails(SSQL);

        if (Base_DS.Rows.Count != 0)
        {
            txtOldSalary.Text = Base_DS.Rows[0]["BaseSalary"].ToString();
        }
        else
        {
            txtOldSalary.Text = "0.00";
        }
    }

    protected void chkNewWages_CheckedChanged(object sender, EventArgs e)
    {
         if(chkNewWages.Checked == true)
         {
             chkOldWages.Checked=false;
             txtOldSalary.Text="0.00";
         }
    }

    protected void chkOldWages_CheckedChanged(object sender, EventArgs e)
    {
          if(chkOldWages.Checked==true)
          {
              chkNewWages.Checked=false;
              Get_Current_Salary();
          }
          //Get_Current_Salary();
    }

    private void Clear_All_Field()
    {
        ddlWages.SelectedValue = "-Select-";
        txtMachineID.SelectedValue = "-Select-";
        txtEmpName.Text = "";
        txtTokenNo.Text = "";
        txtDepartment.Text = "";
        txtDOJ.Text = "";
        txtNewSalary.Text = ""; txtOldSalary.Text = "";
        chkOldWages.Checked = false;
        chkNewWages.Checked = false;
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        bool ErrFlag=false;
        string Payroll_Emp_No = "";
        DataTable Base_DS =new DataTable();
        DataTable mDataSet=new DataTable();

        if(chkNewWages.Checked==false && chkOldWages.Checked==false)
        {
            ErrFlag=true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select Salary Type.!');", true);
        }

        if(txtNewSalary.Text=="0" || txtNewSalary.Text=="0.0" || txtNewSalary.Text=="0.00" || txtNewSalary.Text=="")
        {
            ErrFlag=true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Salary Amount');", true);
        }
       
        //SSQL = "Select * from [" + SessionPayroll + "]..EmployeeDetails where BiometricID='" + txtMachineID.SelectedItem.Text + "' And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ExisistingCode='" + txtTokenNo.Text + "'";
        //Base_DS = objdata.RptEmployeeMultipleDetails(SSQL);
        //if(Base_DS.Rows.Count !=0)
        //{
          //  Payroll_Emp_No = Base_DS.Rows[0]["EmpNo"].ToString();
        //}
        //else
        //{
        //    ErrFlag=true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Employee Not Save in Payroll Database...');", true);
        //}

        if(!ErrFlag)
        {
        SSQL = "Select * from Salary_Update_Det where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' And Approve_Status <> 'Approve' And ExistingCode='" + txtTokenNo.Text + "'";
        mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);

        if(mDataSet.Rows.Count!=0)
        {
            ErrFlag=true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Employee Already Added. You have to Delete this Employee First...');", true);
        }

            if(!ErrFlag)
            {

                string Salary_Type="";
        
                if(chkNewWages.Checked==true)
                {
                    Salary_Type = "New";
                }
                else
                {
                    Salary_Type = "Increment";
                }
                Payroll_Emp_No = txtMachineID.SelectedItem.Text;
                //Insert Salary Update
                SSQL = "Insert Into Salary_Update_Det(CompCode,LocCode,Wages,ExistingCode,MachineID,FirstName,DOJ,DeptName,";
                SSQL = SSQL + " Salary_Type,OLD_Salary,New_Salary,Payroll_EmpNo,Approve_Status,Reason) Values('" + SessionCcode + "',";
                SSQL = SSQL + " '" + SessionLcode + "','" + ddlWages.SelectedItem.Text + "','" + txtTokenNo.Text + "','" + txtMachineID.SelectedItem.Text + "',";
                SSQL = SSQL + " '" + txtEmpName.Text + "','" + txtDOJ.Text + "','" + txtDepartment.Text + "','" + Salary_Type + "',";
                SSQL = SSQL + " '" + txtOldSalary.Text + "','" + txtNewSalary.Text + "','" + Payroll_Emp_No + "','Pending','')";
                objdata.RptEmployeeMultipleDetails(SSQL);

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Employee Salary Added Successfully..');", true);

                //Clear_All_Field();
                txtMachineID.SelectedValue = "-Select-";
                txtEmpName.Text = "";
                txtTokenNo.Text = "";
                txtDepartment.Text = "";
                txtDOJ.Text = "";
                txtNewSalary.Text = ""; txtOldSalary.Text = "";
                ddlWages_SelectedIndexChanged(sender, e);
            }
        }

    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
        ddlWages_SelectedIndexChanged(sender, e);
    }

    protected void txtTokenNo_TextChanged(object sender, EventArgs e)
    {
        string query;
        DataTable DT = new DataTable();

        query = "Select * from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        query = query + " And IsActive='Yes' And Wages='" + ddlWages.SelectedItem.Text + "' And ExistingCode='" + txtTokenNo.Text + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            txtMachineID.SelectedValue = DT.Rows[0]["EmpNo"].ToString();
            txtEmpName.Text = DT.Rows[0]["FirstName"].ToString();
            txtDepartment.Text = DT.Rows[0]["DeptName"].ToString();
            txtDOJ.Text = Convert.ToDateTime(DT.Rows[0]["DOJ"]).ToString("dd/MM/yyyy");
            txtOldSalary.Text = DT.Rows[0]["BaseSalary"].ToString();
        }
        else
        {
            txtMachineID.SelectedValue = "-Select-";
            txtEmpName.Text = "";
            txtDepartment.Text = "";
            txtDOJ.Text = "";
            txtOldSalary.Text = "";
            //txtTokenNo.Text = "";
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Token No not Data found.!');", true);
        }
    }

}
