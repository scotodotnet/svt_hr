﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class MstShiftType : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights; string SessionAdmin;
    string SSQL;
    bool ErrFlag = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        if (!IsPostBack)
        {
            Load_ID();
        }
        Load_OlD();
        txtShiftCd.ReadOnly = true;
    }

    private void Load_ID()
    {
        Int32 ID = 0;
        SSQL = "";
        SSQL = "Select Max(ShiftTypeCd) from MstShiftType where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        DataTable ID_dt = new DataTable();
        ID_dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (ID_dt.Rows.Count > 0)
        {
            if (ID_dt.Rows[0][0] == null || ID_dt.Rows[0][0].ToString() == string.Empty)
            {
                ID = 1;
            }
            else
            {
                ID = 1 + Convert.ToInt32(ID_dt.Rows[0][0].ToString());
            }
        }
        else
        {
            ID = 1;
        }
        txtShiftCd.Text = (ID).ToString();
    }

    private void Load_OlD()
    {
        SSQL = "";
        SSQL = "Select * from MstShiftType where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        Repeater1.DataSource=objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater1.DataBind();
    }

    protected void btnApprvEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {
        if (e.CommandArgument.ToString() == "Edit")
        {
            SSQL = "";
            SSQL = "Select * from MstShiftType where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and ShiftTypeCd='" + e.CommandName.ToString() + "'";
            DataTable dt = new DataTable();
            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt.Rows.Count > 0)
            {
                txtShiftCd.Text = dt.Rows[0]["ShiftTypeCd"].ToString();
                txtShiftName.Text = dt.Rows[0]["ShiftTypeName"].ToString();
                btnSave.Text = "Update";
            }
        }
        if (e.CommandArgument.ToString() == "Delete")
        {
            SSQL = "";
            SSQL = "Delete from MstShiftType where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and ShiftTypeCd='" + e.CommandName.ToString() + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Details Deleted Succesfully!!!');", true);
            Load_OlD();
            Load_ID();
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (txtShiftCd.Text == "" || txtShiftName.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please Check the Fields!!!');", true);
            return;
        }else
        {
            if (btnSave.Text == "Update")
            {
                SSQL = "";
                SSQL = "Delete From MstShiftType where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and ShiftTypeCd='" + txtShiftCd.Text + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);
                btnSave.Text = "Save";
            }
            if (btnSave.Text == "Save")
            {
                SSQL = "";
                SSQL = "Select * from MstShiftType where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and ShiftTypeCd='" + txtShiftName.Text.ToUpper() + "'";
                DataTable chk = new DataTable();
                chk = objdata.RptEmployeeMultipleDetails(SSQL);
                if (chk.Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Category Alredy Inserted!!!');", true);
                    btnClr_Click(sender, e);
                }
                else
                {
                    SSQL = "";
                    SSQL = "Insert Into MstShiftType values('" + SessionCcode + "','" + SessionLcode + "','" + txtShiftCd.Text + "','" + txtShiftName.Text.ToUpper() + "')";
                    objdata.RptEmployeeMultipleDetails(SSQL);
                    Load_OlD();
                    btnClr_Click(sender, e);
                }
            }
            Load_ID();
        }
    }

    protected void btnClr_Click(object sender, EventArgs e)
    {
        txtShiftName.Text = "";
    }
}