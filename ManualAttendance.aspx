<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="ManualAttendance.aspx.cs" Inherits="ManualAttendance" Title="" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script src="assets/js/master_list_jquery.min.js"></script>
    <script src="assets/js/master_list_jquery-ui.min.js"></script>
    <link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css" />

    <script>
        $(document).ready(function () {
            $('#example').dataTable();

            $('.datepicker').datepicker({
                format: "dd/mm/yyyy",
                autoclose: true
            });
        });
    </script>
    </script>
 <script type="text/javascript">
     function SaveMsgAlert(msg) {
         swal(msg);
     }
 </script>

    <script type="text/javascript">
        function ProgressBarShow() {
            $('#Download_loader').show();
        }
    </script>

    <script type="text/javascript">
        function ProgressBarHide() {
            $('#Download_loader').hide();
        }
    </script>

    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('#example').dataTable();
                    $('.select2').select2();
                    $('.datepicker').datepicker({
                        format: "dd/mm/yyyy",
                        autoclose: true
                    });
                }
            });
        };
    </script>
    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
        <ContentTemplate>
            <!-- begin #content -->
            <div id="content" class="content">
                <!-- begin breadcrumb -->
                <ol class="breadcrumb pull-right">
                    <li><a href="javascript:;">Manual</a></li>
                    <li class="active">Manual Attendance</li>
                </ol>
                <!-- end breadcrumb -->
                <!-- begin page-header -->
                <h1 class="page-header">Manual Attendance </h1>
                <!-- end page-header -->

                <!-- begin row -->
                <div class="row">
                    <!-- begin col-12 -->
                    <div class="col-md-12">
                        <!-- begin panel -->
                        <div class="panel panel-inverse">
                            <div class="panel-heading">
                                <div class="panel-heading-btn">
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                                </div>
                                <h4 class="panel-title">Manual Attendance</h4>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <!-- begin row -->
                                    <div class="row">
                                        <!-- begin col-4 -->
                                        <div class="form-group col-md-2">
                                            <asp:CheckBox ID="chkapproval" runat="server" Checked="true"
                                                OnCheckedChanged="chkapproval_CheckedChanged" AutoPostBack="true" />
                                            Approval
												
                                        </div>
                                        <div class="form-group col-md-2">
                                            <asp:CheckBox ID="chkPending" runat="server"
                                                OnCheckedChanged="chkPending_CheckedChanged" AutoPostBack="true" />
                                            Pending
                                        </div>


                                        <div class="col-md-6">
                                        </div>
                                        <!-- end col-12 -->
                                    </div>
                                    <!-- end row -->

                                    <!-- begin row -->
                                    <div class="row">
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Token No</label>
                                                <asp:DropDownList runat="server" ID="txtExistingCode" class="form-control select2"
                                                    Style="width: 100%;" AutoPostBack="true"
                                                    OnSelectedIndexChanged="txtExistingCode_SelectedIndexChanged">
                                                </asp:DropDownList>
                                                <asp:HiddenField ID="txtMachineID" runat="server" />
                                                <asp:RequiredFieldValidator ControlToValidate="txtExistingCode" InitialValue="-Select-" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Employee Name</label>
                                                <asp:Label runat="server" ID="txtEmpName" class="form-control" BackColor="#F3F3F3"></asp:Label>

                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Department</label>
                                                <asp:Label runat="server" ID="txtDepartment" class="form-control" BackColor="#F3F3F3"></asp:Label>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                    </div>
                                    <!-- end row -->

                                    <div class="row">
                                        <!-- begin col-4 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>ShiftWise</label>
                                                <asp:RadioButtonList ID="RdbShiftWise" runat="server" class="form-control"
                                                    RepeatDirection="Horizontal"
                                                    OnSelectedIndexChanged="RdbShiftWise_SelectedIndexChanged" AutoPostBack="true">
                                                    <asp:ListItem Value="1" Text="Yes" style="padding-right: 40px" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Value="2" Text="No"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-3" runat="server" id="div_ShiftType">
                                            <div class="form-group">
                                                <label>Shift Type</label>
                                                <asp:DropDownList runat="server" ID="ddlShiftType" class="form-control select2" Style="width: 100%">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ControlToValidate="ddlShiftType" InitialValue="-Select-" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-4" runat="server" id="div_ShiftDate">
                                            <div class="form-group">
                                                <label>Shift Date</label>
                                                <asp:TextBox runat="server" ID="txtShiftDate" class="form-control datepicker" AutoComplete="off" placeholder="dd/MM/YYYY"></asp:TextBox>
                                                <asp:RequiredFieldValidator ControlToValidate="txtShiftDate" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Attnd.Type</label>
                                                <asp:DropDownList runat="server" ID="ddlAttndType" class="form-control select2" Style="width: 100%">
                                                    <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
                                                    <asp:ListItem Value="P">P</asp:ListItem>
                                                    <asp:ListItem Value="H">H</asp:ListItem>
                                                    <asp:ListItem Value="A">A</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ControlToValidate="ddlAttndType" InitialValue="-Select-" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator8" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                                <asp:TextBox runat="server" ID="txtOTHrs" class="form-control" Visible="false"></asp:TextBox>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                    </div>

                                    <!-- begin row -->
                                    <asp:Panel ID="Panel1" runat="server" Visible="false">
                                        <div class="row">
                                            <!-- begin col-4 -->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>IN Date</label>
                                                    <asp:TextBox runat="server" ID="txtINDate" class="form-control datepicker" AutoComplete="off" placeholder="dd/MM/YYYY"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ControlToValidate="txtINDate" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator41" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                    </asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                            <!-- end col-4 -->
                                            <!-- begin col-4 -->
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label>Time IN</label>
                                                    <asp:TextBox runat="server" ID="txtTimeIN" class="form-control" AutoComplete="off"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ControlToValidate="txtTimeIN" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                    </asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                            <!-- end col-4 -->
                                            <!-- begin col-4 -->
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>OUT Date</label>
                                                    <asp:TextBox runat="server" ID="txtOUTDate" class="form-control datepicker" AutoComplete="off" placeholder="dd/MM/YYYY"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ControlToValidate="txtOUTDate" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator6" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                    </asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                            <!-- end col-4 -->
                                            <!-- begin col-4 -->
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <label>Time OUT</label>
                                                    <asp:TextBox runat="server" ID="txtTimeOUT" class="form-control" AutoComplete="off"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ControlToValidate="txtTimeOUT" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator7" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                    </asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                            <!-- end col-4 -->
                                        </div>
                                    </asp:Panel>
                                    <!-- end row -->
                                    <!-- begin row -->
                                    <div class="row">
                                        <div class="col-md-4"></div>
                                        <!-- begin col-4 -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <br />
                                                <asp:Button runat="server" ID="btnSave" Text="Save" class="btn btn-success"
                                                    ValidationGroup="Validate_Field" OnClick="btnSave_Click" />
                                                <asp:Button runat="server" ID="btnClear" Text="Clear" class="btn btn-danger"
                                                    OnClick="btnClear_Click" />
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <div class="col-md-4"></div>
                                    </div>
                                    <!-- end row -->

                                    <!-- begin row -->
                                    <div class="row">

                                        <!-- begin col-3 -->
                                        <div class="col-md-3" runat="server" visible="false">
                                            <div class="form-group">
                                                <label>Department</label>
                                                <asp:DropDownList ID="ddlDepartment" runat="server"
                                                    class="form-control select2" Style="width: 100%;"
                                                    OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged" AutoPostBack="true">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ControlToValidate="ddlDepartment" InitialValue="-Select-" Display="Dynamic" ValidationGroup="Validate_DownField" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Employee Type</label>
                                                <asp:DropDownList ID="ddlEmployeeType" runat="server"
                                                    class="form-control select2" Style="width: 100%; text-transform: uppercase"
                                                    OnSelectedIndexChanged="ddlEmployeeType_SelectedIndexChanged" AutoPostBack="true">
                                                </asp:DropDownList>

                                            </div>

                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Van No</label>
                                                <asp:DropDownList ID="ddlVanNo" runat="server"
                                                    class="form-control select2" Style="width: 100%;"
                                                    OnSelectedIndexChanged="ddlVanNo_SelectedIndexChanged" AutoPostBack="true">
                                                </asp:DropDownList>

                                            </div>
                                        </div>

                                        <!-- end col-3 -->
                                        <!-- begin col-3 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <br />
                                                <asp:FileUpload ID="FileUpload" runat="server" />

                                                <%--<label>Designation</label><span class="mandatory">*</span>--%>
                                                <asp:DropDownList ID="ddlDesignation" Visible="false" runat="server" class="form-control select2" Style="width: 100%;">
                                                </asp:DropDownList>

                                            </div>
                                        </div>
                                        <!-- end col-3 -->
                                        <!-- begin col-3 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <br />
                                                <asp:Button runat="server" ID="btnDownLoad" Text="Download"
                                                    class="btn btn-warning" ValidationGroup="Validate_DownField"
                                                    OnClick="btnDownLoad_Click" />
                                                <asp:Button runat="server" ID="btnUpload" Text="Upload" class="btn btn-success"
                                                    OnClick="btnUpload_Click" OnClientClick="ProgressBarShow();" />
                                            </div>
                                        </div>
                                        <!-- end col-3 -->
                                        <!-- begin col-3 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                            </div>
                                        </div>
                                        <!-- end col-3 -->
                                    </div>
                                    <div id="Download_loader" style="display: none" />
                                </div>
                                <!-- end row -->

                            </div>
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
            </div>
            <!-- end #content -->
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnDownLoad" />
            <asp:PostBackTrigger ControlID="btnUpload" />
        </Triggers>
    </asp:UpdatePanel>

</asp:Content>

