﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Globalization;

public partial class RptLateIN : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string Division;
    string Status;
    double Final_Count;
    string TokenNo;


    double Present_Count;
    double Absent_Count;
    decimal Fixed_Work_Days;
    decimal Month_WH_Count;
    double Present_WH_Count;
    double NFH_Days_Count;
    double NFH_Days_Present_Count;

    BALDataAccess objdata = new BALDataAccess();

    string SSQL = "";
    DataTable AutoDTable = new DataTable();
    DataTable mEmployeeDT = new DataTable();
    DataTable MEmployeeDS = new DataTable();
    DateTime date1;
    DateTime Date2 = new DateTime();
    int intK;
    string FromDate = null;
    string ToDate;
    string Shift;
    string Date_Value_Str;
    string Date_value_str1;
    string[] Time_Minus_Value_Check;
    string WagesType;
    double leaveCount;


    System.Web.UI.WebControls.DataGrid grid =
                            new System.Web.UI.WebControls.DataGrid();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }else
        {
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report- Late IN Between Dates";
            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            Status = Request.QueryString["Status"].ToString();
            Division = Request.QueryString["Division"].ToString();

            string TempWages = Request.QueryString["Wages"].ToString();
            WagesType = TempWages.Replace("_", "&");
            Shift = Request.QueryString["Shift"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            
            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();
            DataTable TempDt = new DataTable();
           
            AutoDTable.Columns.Add("MachineID");
            AutoDTable.Columns.Add("FirstName");
            AutoDTable.Columns.Add("DeptName");
            
            date1 = Convert.ToDateTime(FromDate);
            string dat = ToDate;
            Date2 = Convert.ToDateTime(dat);

            int daycount = (int)((Date2 - date1).TotalDays);
            int daysAdded = 0;

            while (daycount >= 0)
            {
                DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded).ToShortDateString());
                AutoDTable.Columns.Add(Convert.ToString(dayy.ToShortDateString()));

                daycount -= 1;
                daysAdded += 1;
            }

            AutoDTable.Columns.Add("Total Minutes");

            SSQL = "Select MachineID,FirstName,DeptName from Employee_mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
            if (WagesType != "-Select-")
            {
                SSQL = SSQL + " and Wages='" + WagesType + "'";
            }
            dt1 = objdata.RptEmployeeMultipleDetails(SSQL);


            for (int EmpNoRw = 0; EmpNoRw < dt1.Rows.Count; EmpNoRw++)
            {
                
                AutoDTable.NewRow();
                AutoDTable.Rows.Add();

                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["MachineID"] = dt1.Rows[EmpNoRw]["MachineID"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["FirstName"] = dt1.Rows[EmpNoRw]["FirstName"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["DeptName"] = dt1.Rows[EmpNoRw]["DeptName"].ToString();

                //Total calculation for Each Emp
                double Total_Deduction_Hrs = 0;
                double Total_Deduction_Min = 0;

                daycount = (int)((Date2 - date1).TotalDays);
                daysAdded = 0;

                while (daycount >= 0)
                {
                    //Deduction for each date
                    double Deduction_Hrs = 0;
                    double Deduction_Min = 0;

                    DateTime Shift_Start_Time,Emp_Time_IN;
                    DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded).ToShortDateString());
                    double Total_Minutes;

                    SSQL = "Select LD.MachineID,LD.FirstName,LD.DeptName,LD.Present,LD.Attn_Date,LD.TimeIN,LD.Shift,SM.StartTime from LogTime_Days LD";
                    SSQL = SSQL + " Inner Join Shift_Mst SM on SM.ShiftDesc=LD.Shift";
                    SSQL = SSQL + " where LD.CompCode='" + SessionCcode + "' and LD.LocCode='" + SessionLcode + "' and SM.CompCode='" + SessionCcode + "' and SM.LocCode='" + SessionLcode + "'";
                    if (Shift.ToString().ToUpper() != "ALL")
                    {
                        SSQL = SSQL + " and LD.Shift='" + Shift + "'";
                    }
                    SSQL = SSQL + " and Convert(datetime,LD.Attn_Date,103)= Convert(datetime,'" + dayy.ToString("dd/MM/yyyy") + "',103) and LD.MachineID='" + dt1.Rows[EmpNoRw]["MachineID"] + "'";
                    SSQL = SSQL + " and LD.TypeName!='Leave' and LD.Present_Absent!='Leave' and LD.Present_Absent!='Absent' and LD.Shift!='No Shift'";

                    dt = objdata.RptEmployeeMultipleDetails(SSQL);

                    if (dt.Rows.Count > 0)
                    {
                        Shift_Start_Time = Convert.ToDateTime(dt.Rows[0]["StartTime"]);
                        Emp_Time_IN = Convert.ToDateTime(dt.Rows[0]["TimeIN"]);

                        if (Emp_Time_IN > Shift_Start_Time)
                        {
                            Total_Minutes = Emp_Time_IN.Subtract(Shift_Start_Time).TotalMinutes;

                            //Get LateIN deduction using Emp TimeIN for Particular shift
                            SSQL = "";
                            SSQL = "Select isnull(Deduction,'0') as Deduction from MstLateIN where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                            SSQL = SSQL + " and CAST(" + Total_Minutes + " AS decimal)>=CAST(StartMin AS decimal) and CAST(" + Total_Minutes + "  AS decimal)<=CAST(EndMin AS decimal)";

                            TempDt = objdata.RptEmployeeMultipleDetails(SSQL);

                            if (dt1.Rows[EmpNoRw]["MachineID"].ToString() == "503")
                            {
                                string s = "1";
                            }
                            if (TempDt.Rows.Count > 0)
                            {
                                if (Convert.ToDouble(TempDt.Rows[0]["Deduction"].ToString()) > 0)
                                {
                                    Deduction_Hrs = Convert.ToDouble(TempDt.Rows[0]["Deduction"]);
                                    Total_Deduction_Hrs = Total_Deduction_Hrs + Deduction_Hrs;
                                }
                                else
                                {
                                    Deduction_Hrs = 0;
                                }
                            }
                            else
                            {
                                Deduction_Hrs = 0;
                            }
                        }
                    }

                    //Set color for differ
                    if (Deduction_Hrs > 0)
                    {
                        Deduction_Hrs = Convert.ToDouble(Deduction_Hrs / 60);

                        AutoDTable.Rows[AutoDTable.Rows.Count - 1][dayy.ToString("dd/MM/yyyy")] = Deduction_Hrs.ToString();
                    }
                    else
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1][dayy.ToString("dd/MM/yyyy")] = "";

                    daycount -= 1;
                    daysAdded += 1;
                }
                
                //Convert Minutes to Hour if the Minutes is >= 60
                if (Total_Deduction_Hrs > 0)
                {
                    Total_Deduction_Hrs = Convert.ToDouble(Total_Deduction_Hrs / 60);
                    

                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Total Minutes"] = Total_Deduction_Hrs.ToString();
                }
                else
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Total Minutes"] = "";
            }

            // for delete the row if the Total Minutes in 0
            for (int rw = AutoDTable.Rows.Count - 1; rw >= 0; rw--)
            {
                DataRow dr = AutoDTable.Rows[rw];
                if (dr["Total Minutes"].ToString() == "" | dr["Total Minutes"].ToString() == "0" | dr["Total Minutes"].ToString() == string.Empty)
                    dr.Delete();
            }
            AutoDTable.AcceptChanges();
            
            if (AutoDTable.Rows.Count != 0)
            {
                grid.DataSource = AutoDTable;
                grid.DataBind();
                string attachment = "attachment;filename=LATE IN BETWEEN DATES.xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";
                grid.HeaderStyle.Font.Bold = true;
                System.IO.StringWriter stw = new System.IO.StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);
                grid.RenderControl(htextw);
                Response.Write("<table>");
                Response.Write("<tr Font-Bold='true' align='left'>");
                Response.Write("<td colspan='10'>");
                Response.Write("<a style=\"font-weight:bold\">LATE IN BETWEEN DATES REPORT -" + Session["Lcode"].ToString() + "-" + FromDate + "-" + ToDate + "</a>");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");
                Response.Write(stw.ToString());
                Response.End();
                Response.Clear();
            }
        }
    }
}