﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
public partial class Pay_Basic_Det_Mst : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string Query = "";
    string SessionPayroll;
    string SessionUserType;
    string SessionRights;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionRights = Session["Rights"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        Load_DB();
        if (!IsPostBack)
        {
            Load_WagesType();
            Load_Data();

            if (SessionUserType == "2")
            {
                
            }
            else
            {
                IFUser_Fields_Hide();
            }
        }
    }
    public void IFUser_Fields_Hide()
    {
        lblHead.Visible = true;
        lblCateName.Visible = true;
        ddlcategoryPF.Visible = true;
        lblEMpType.Visible = true;
        txtEmployeeType.Visible = true;
        lblDays.Visible = true;
        txtPfDays.Visible = true;
        btnPFsave.Visible = true;
        btnPFclear.Visible = true;
    }
    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_WagesType();
    }
    private void Load_WagesType()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtEmployeeType.Items.Clear();
        string Category_Str = "0";
        if (ddlcategoryPF.SelectedItem.Text == "STAFF")
        {
            Category_Str = "1";
        }
        else if (ddlcategoryPF.SelectedItem.Text == "LABOUR")
        {
            Category_Str = "2";
        }
        query = "select EmpTypeCd,EmpType from MstEmployeeType where EmpCategory='" + Category_Str + "'";

        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtEmployeeType.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpTypeCd"] = "0";
        dr["EmpType"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtEmployeeType.DataTextField = "EmpType";
        txtEmployeeType.DataValueField = "EmpTypeCd";
        txtEmployeeType.DataBind();
    }
    public void Load_DB()
    {
        //Get Database Name
        string query = "";
        DataTable dt_DB = new DataTable();
        query = "select CMS,Rights,Sales,Stores,Weaving,Maintaince,Production,Payroll from [SVT_Rights]..MstDBname";
        dt_DB = objdata.RptEmployeeMultipleDetails(query);
        if (dt_DB.Rows.Count > 0)
        {
            SessionPayroll = dt_DB.Rows[0]["Payroll"].ToString();
            //SessionRights = dt_DB.Rows[0]["Rights"].ToString();
        }
    }

    protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dtdsupp = new DataTable();
        ddlEmployeeType.Items.Clear();
        Query = "Select EmpTypeCd,EmpType from MstEmployeeType where EmpCategory='" + ddlCategory.SelectedValue + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(Query);
        ddlEmployeeType.DataSource = dtdsupp;
        //DataRow dr = dtdsupp.NewRow();
        //dr["EmpType"] = "-Select-";
        //dr["EmpTypeCd"] = "-Select-";
        //dtdsupp.Rows.InsertAt(dr, 0);
        ddlEmployeeType.DataTextField = "EmpType";
        ddlEmployeeType.DataValueField = "EmpTypeCd";
        ddlEmployeeType.DataBind();
        ddlEmployeeType_SelectedIndexChanged(sender, e);
    }
    protected void ddlEmployeeType_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Data();
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            String MsgFlag = "Insert";
            DataTable dt = new DataTable();
            Query = "select * from [" + SessionPayroll + "]..MstBasicDet where Category='" + ddlCategory.SelectedValue + "' and EmployeeType='" + ddlEmployeeType.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
            dt = objdata.RptEmployeeMultipleDetails(Query);
            if (dt.Rows.Count > 0)
            {
                Query = "delete from [" + SessionPayroll + "]..MstBasicDet where Category='" + ddlCategory.SelectedValue + "' and EmployeeType='" + ddlEmployeeType.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                dt = objdata.RptEmployeeMultipleDetails(Query);
                MsgFlag = "Update";
            }

            Query = "";
            Query = Query + "Insert into [" + SessionPayroll + "]..MstBasicDet(Ccode,Lcode,Category,EmployeeType,BasicDA,HRA,ConvAllow,EduAllow,MediAllow,";
            Query = Query + "RAI,WashingAllow,Spl_Allowance,Uniform,Vehicle_Maintenance,Communication,Journ_Perid_Paper,Incentives)Values('" + SessionCcode + "','" + SessionLcode + "',";
            Query = Query + "'" + ddlCategory.SelectedValue + "','" + ddlEmployeeType.SelectedValue + "','" + txtBasic.Text + "','" + txtHRA.Text + "','0','0','0',";
            Query = Query + "'0','0','0','0','0','0','0','0')";
            objdata.RptEmployeeMultipleDetails(Query);
            Clear();
            Load_Data();
            if (MsgFlag == "Insert")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Update Successfully');", true);
            }
        }
        catch (Exception Ex)
        {

        }
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear();
    }
    public void Clear()
    {
        txtBasic.Text = "0";
        //txtSpl.Text = "0";
        txtHRA.Text = "0";
        //txtvehicle.Text = "0";
        //txtCommunication.Text = "0";
        //txtJornal.Text = "0";
        //txtUniform.Text = "0";
        //txtConv.Text = "0";
        //txtEdu.Text = "0";
        //txtMedi.Text = "0";
        //txtWash.Text = "0";
        //txtIncentive.Text = "0";
        //txtRAI.Text = "0";
        //txtMedi.Text = "0";
        //ddlCategory.SelectedValue = "0";
        //ddlEmployeeType.SelectedValue = "0";
    }

    public void Load_Data()
    {
        DataTable dt = new DataTable();
        Query = "select * from [" + SessionPayroll + "]..MstBasicDet where Category='" + ddlCategory.SelectedValue + "' and EmployeeType='" + ddlEmployeeType.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(Query);
        if (dt.Rows.Count > 0)
        {
            txtBasic.Text = dt.Rows[0]["BasicDA"].ToString();
            //txtSpl.Text = dt.Rows[0]["Spl_Allowance"].ToString();
            txtHRA.Text = dt.Rows[0]["HRA"].ToString();
            //txtvehicle.Text = dt.Rows[0]["Vehicle_Maintenance"].ToString();
            //txtCommunication.Text = dt.Rows[0]["Communication"].ToString();
            //txtJornal.Text = dt.Rows[0]["Journ_Perid_Paper"].ToString();
            //txtUniform.Text = dt.Rows[0]["Uniform"].ToString();
            //txtConv.Text = dt.Rows[0]["ConvAllow"].ToString();
            //txtEdu.Text = dt.Rows[0]["EduAllow"].ToString();
            ////txtMedi.Text = dt.Rows[0][""].ToString();
            //txtWash.Text = dt.Rows[0]["WashingAllow"].ToString();
            //txtIncentive.Text = dt.Rows[0]["Incentives"].ToString();
            //txtRAI.Text = dt.Rows[0]["RAI"].ToString();
            //txtMedi.Text = dt.Rows[0]["MediAllow"].ToString();
        }
        else
        {
            txtBasic.Text = "0";
            //txtSpl.Text = "0";
            txtHRA.Text = "0";
            //txtvehicle.Text = "0";
            //txtCommunication.Text = "0";
            //txtJornal.Text = "0";
            //txtUniform.Text = "0";
            //txtConv.Text = "0";
            //txtEdu.Text = "0";
            //txtMedi.Text = "0";
            //txtWash.Text = "0";
            //txtIncentive.Text = "0";
            //txtRAI.Text = "0";
            //txtMedi.Text = "0";
        }

    }
    public void Load_Data_PF()
    {
        DataTable dt = new DataTable();
        Query = "select * from [" + SessionPayroll + "]..MstPFdaysCalc where Category='" + ddlcategoryPF.SelectedValue + "' and EmployeeType='" + txtEmployeeType.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(Query);
        if (dt.Rows.Count > 0)
        {
            txtPfDays.Text = dt.Rows[0]["PFdays"].ToString();
        }
        else 
        {
            txtPfDays.Text = "0.0";
        }
    }
    protected void btnPFsave_Click(object sender, EventArgs e)
    {
        try
        {
            String MsgFlag = "Insert";
            DataTable dt = new DataTable();
            Query = "select * from [" + SessionPayroll + "]..MstPFdaysCalc where Category='" + ddlcategoryPF.SelectedValue + "' and EmployeeType='" + txtEmployeeType.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
            dt = objdata.RptEmployeeMultipleDetails(Query);
            if (dt.Rows.Count > 0)
            {
                Query = "delete from [" + SessionPayroll + "]..MstPFdaysCalc where Category='" + ddlcategoryPF.SelectedValue + "' and EmployeeType='" + txtEmployeeType.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                dt = objdata.RptEmployeeMultipleDetails(Query);
                MsgFlag = "Update";
            }
            Query = "";
            Query = Query + "insert into [" + SessionPayroll + "]..MstPFdaysCalc(Ccode,Lcode,Category,EmployeeType,PFdays)";
            Query = Query + "values('" + SessionCcode + "','" + SessionLcode + "','" + ddlcategoryPF.SelectedValue + "','" + txtEmployeeType.SelectedValue + "','" + txtPfDays.Text + "')";
            objdata.RptEmployeeMultipleDetails(Query);
            txtPfDays.Text = "0.0";
            if (MsgFlag == "Insert")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Update Successfully');", true);
            }


        }
        catch (Exception)
        {
            
            throw;
        }
    }
    protected void btnPFclear_Click(object sender, EventArgs e)
    {

    }
    protected void txtEmployeeType_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Data_PF();
    }
}