﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="MstUserCreation.aspx.cs" Inherits="MstUserCreation" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<script src="assets/js/master_list_jquery.min.js"></script>
<script src="assets/js/master_list_jquery-ui.min.js"></script>
<link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css"/>
 <script>
     $(document).ready(function() {
     $('#example').dataTable();
     $('.select2').select2();
     });
	</script>


<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
                $('.select2').select2();
            }
        });
    };
</script>

<asp:UpdatePanel>
<ContentTemplate>
<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Master</a></li>
				<li class="active">User Creation</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">User Creation </h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">User Creation</h4>
                        </div>
                        <div class="panel-body">
                        <!-- begin row -->
                          <div class="row">
                           <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								  <label>User ID</label>
								 <asp:TextBox ID="txtUserID" MaxLength="30" class="form-control" runat="server"></asp:TextBox>
                                 <asp:RequiredFieldValidator ControlToValidate="txtUserID" ValidationGroup="Validate_Field" class="form_error" ID="validate1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                 </asp:RequiredFieldValidator>
								</div>
                               </div>
                              <!-- end col-4 -->
                              <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								  <label>Full Name</label>
								   <asp:TextBox ID="txtUserFullName" class="form-control" runat="server"></asp:TextBox>
							       <asp:RequiredFieldValidator ControlToValidate="txtUserFullName" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                   </asp:RequiredFieldValidator>
								</div>
                               </div>
                              <!-- end col-4 -->
                              
                             <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								  <label>User Type</label>
								  <asp:DropDownList ID="txtUserType" runat="server" class="form-control select2">
                                    <asp:ListItem Value="1" Text="Admin"></asp:ListItem>
                                    <asp:ListItem Value="2" Text="IF User"></asp:ListItem>
                                    <asp:ListItem Value="3" Text="Normal User"></asp:ListItem>
                                    <asp:ListItem Value="4" Text="MD"></asp:ListItem>
                                    <%--<asp:ListItem Value="5" Text="Hospital"></asp:ListItem>--%>
                                    
                                 </asp:DropDownList>
								</div>
                               </div>
                              <!-- end col-4 -->
                              </div>
                        <!-- end row -->
                         <div class="row">
                               <!-- begin col-4 -->
                               <div class="col-md-4">
								<div class="form-group">
								  <label>New Password</label>
								   <asp:TextBox ID="txtNewPassword" class="form-control" runat="server" TextMode="Password"></asp:TextBox>
					            <asp:RequiredFieldValidator ControlToValidate="txtNewPassword" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
								 </div>
                               </div>
                               <!-- end col-4 -->
                             <!-- begin col-4 -->
                               <div class="col-md-4">
								<div class="form-group">
								  <label>Conform Password</label>
								    <asp:TextBox ID="txtConformPassword" class="form-control" runat="server" TextMode="Password"></asp:TextBox>
					                <asp:RequiredFieldValidator ControlToValidate="txtConformPassword" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                    </asp:RequiredFieldValidator>
								 </div>
                               </div>
                               <!-- end col-4 -->
                               <!-- begin col-2 -->
                              <div class="col-md-2">
								<div class="form-group">
								  <label>Location</label>
								  <asp:DropDownList ID="ddlUnit" runat="server" class="form-control select2">
                                  </asp:DropDownList>
								</div>
                               </div>
                              <!-- end col-2 -->   
                               <!-- begin col-2 -->
                                <div class="col-md-2">
                                <div class="form-group">
                                <br />
                                <asp:Button runat="server" id="btnUnitChange" Text="Load" class="btn btn-warning"  OnClick="btnUnitChange_Click"  />
                                </div>  
                                 </div>  
                                <!-- end col-2 -->              
                         </div>
                         <div class="row">
                         <div class="col-md-4"></div>
                           <!-- begin col-4 -->
                                <div class="col-md-4">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" ID="btnSave" Text="Save" class="btn btn-success" ValidationGroup="Validate_Field" OnClick="btnSave_Click" />
									<asp:Button runat="server" id="btnCancel" Text="Cancel" class="btn btn-danger" OnClick="btnCancel_Click"/>
								 </div>
                               </div>
                              <!-- end col-4 -->
                              <div class="col-md-4"></div>
                         </div>
                        <!-- end row -->
                        
                          <!-- table start -->
                    <div class="col-md-12">
		                <div class="row">
		                    <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
		                        <HeaderTemplate>
                                    <table id="example" class="display table">
                                        <thead>
                                            <tr>
                                                <th>User ID</th>
                                                <th>Name</th>
                                                <th>Type</th>
                                                <%--<th>Password</th>--%>
                                                <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Eval("UserCode")%></td>
                                        <td><%# Eval("UserName")%></td>
                                        <td><%# Eval("IsAdmin")%></td>
                                        <%--<td><%# Eval("NewPassword")%></td>--%>
                                        <td>
                                            <asp:LinkButton ID="btnEditGrid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                                                Text="" OnCommand="GridEditClick" CommandArgument="Edit" CommandName='<%# Eval("UserCode")%>'>
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                Text="" OnCommand="GridDeleteClick" CommandArgument="Delete" CommandName='<%# Eval("UserCode")%>' 
                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this new user details?');">
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>
		                    </asp:Repeater>		                    
		                </div>
		            </div>
                    <!-- table End -->
                        
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
<!-- end #content -->
</ContentTemplate>
</asp:UpdatePanel>

</asp:Content>

