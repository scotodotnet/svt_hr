<%@ Page Language="C#" MasterPageFile="~/PayrollMaster.master" AutoEventWireup="true" CodeFile="SalaryCalcPay.aspx.cs" Inherits="SalaryCalcPay" Title="" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
        if (sender._postBackSettings.panelsToUpdate != null) {
            $('.select2').select2();
                $('.datepicker').datepicker({
                    format: "dd/mm/yyyy",
                    autoclose: true
                });
            }
        });
    };
</script>
 <script type="text/javascript">
     function SaveMsgAlert(msg) {
         swal(msg);
     }
</script>

<script type="text/javascript">
    function ProgressBarShow() {
        $('#Download_loader').show();
    }
</script>

<script type="text/javascript">
    function ProgressBarHide() {
        $('#Download_loader').hide();
    }
</script>
<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Salary Process</a></li>
				<li class="active">Salary Calculation</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Salary Calculation</h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
              <asp:UpdatePanel ID="SalPay" runat="server">
                 <ContentTemplate>
                   <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">Salary Calculation</h4>
                        </div>
                        <div class="panel-body">
                        <div class="form-group">
                       <!-- begin row -->
                        <div class="row">
                        <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Fin. Year</label>
								 <asp:DropDownList runat="server" ID="ddlFinYear" class="form-control select2" AutoPostBack="true"
                                        style="width:100%;" onselectedindexchanged="ddlFinYear_SelectedIndexChanged">
							 	 </asp:DropDownList>
								</div>
                               </div>
                           <!-- end col-4 -->
                           <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Month</label>
								 <asp:DropDownList runat="server" ID="ddlMonth" class="form-control select2" AutoPostBack="true"
                                        style="width:100%;" onselectedindexchanged="ddlMonth_SelectedIndexChanged">
								 <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
								 <asp:ListItem Value="January">January</asp:ListItem>
								 <asp:ListItem Value="February">February</asp:ListItem>
								 <asp:ListItem Value="March">March</asp:ListItem>
								 <asp:ListItem Value="April">April</asp:ListItem>
								 <asp:ListItem Value="May">May</asp:ListItem>
								 <asp:ListItem Value="June">June</asp:ListItem>
								 <asp:ListItem Value="July">July</asp:ListItem>
								 <asp:ListItem Value="August">August</asp:ListItem>
								 <asp:ListItem Value="September">September</asp:ListItem>
								 <asp:ListItem Value="October">October</asp:ListItem>
								 <asp:ListItem Value="November">November</asp:ListItem>
								 <asp:ListItem Value="December">December</asp:ListItem>
							 	 </asp:DropDownList>
								</div>
                               </div>
                           <!-- end col-4 -->
                            <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								<br />
								 <asp:FileUpload ID="FileUpload" runat="server" />
								</div>
								</div>
								 <!-- end col-4 -->
                        </div>
                       <!-- end row -->
                       <!-- begin row -->
                        <div class="row">
                          <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Category</label>
								 <asp:DropDownList runat="server" ID="ddlCategory" class="form-control select2" AutoPostBack="true"
                                        style="width:100%;" onselectedindexchanged="ddlCategory_SelectedIndexChanged">
								 <asp:ListItem Value="0">-Select-</asp:ListItem>
								 <asp:ListItem Value="1">Staff</asp:ListItem>
								 <asp:ListItem Value="2">Labour</asp:ListItem>
								</asp:DropDownList>
								</div>
                               </div>
                           <!-- end col-4 -->
                           <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Employee Type</label>
								 <asp:DropDownList runat="server" ID="ddlEmployeeType" class="form-control select2" AutoPostBack="true"
                                        style="width:100%;" onselectedindexchanged="ddlEmployeeType_SelectedIndexChanged">
								 </asp:DropDownList>
								</div>
                               </div>
                           <!-- end col-4 -->
                           <!-- begin col-4 -->
                                 <div class="col-md-4">
									<div class="form-group">
										<label>Transfer Date</label>
										<asp:TextBox runat="server" ID="txtTransDate" class="form-control datepicker"></asp:TextBox>
									</div>
                                  </div>
                               <!-- end col-4 -->
                        </div>
                       <!-- end row -->
                       <!-- begin row -->
                        <div class="row">
                        <!-- begin col-4 -->
                                 <div class="col-md-4">
									<div class="form-group">
										<label>From Date</label>
										<asp:TextBox runat="server" ID="txtFromDate" class="form-control datepicker"></asp:TextBox>
									</div>
                                  </div>
                               <!-- end col-4 -->
                               <!-- begin col-4 -->
                                 <div class="col-md-4">
									<div class="form-group">
										<label>To Date</label>
										<asp:TextBox runat="server" ID="txtToDate" class="form-control datepicker"></asp:TextBox>
									</div>
                                  </div>
                               <!-- end col-4 -->
                                <!-- begin col-4 -->
                          <div class="col-md-4" runat="server" id="IF_Token_No">
                               <div class="form-group">
                                     
                                            <label>TokenNo</label>
                                        
                                       <asp:TextBox runat="server" ID="txtTokenNo" class="form-control" Enabled="false"></asp:TextBox>
                                    </div>
                                </div>
                                 <!-- end col-4 -->
                             </div>
                       <!-- end row -->
                        <!-- begin row -->  
                        <div class="row">
                         <!-- begin col-4 -->
                          <div class="col-md-4" runat="server" id="IF_Civil_Inc">
                               <div class="form-group">
                                   <div class="col-md-8">
                                        <label class="checkbox-inline">
                                            <asp:CheckBox ID="ChkCivilIncentive" runat="server" oncheckedchanged="ChkCivilIncentive_CheckedChanged" AutoPostBack="true" />Civil Incentive
                                        </label>
                                       
                                    </div>
                                </div>
                             </div>
                           <!-- end col-4 -->
                           <div class="col-md-4">
                           <div class="form-group">
                           </div>
                           </div>
                           <div class="col-md-4" runat="server" id="IF_Token_Checkbox">
                              <div class="form-group">
										
										<asp:CheckBox ID="ChkTokenNoSalaryprocess" runat="server" Text="Token No" Font-Bold="true"
                                                                        oncheckedchanged="ChkTokenNoSalaryprocess_CheckedChanged" BorderStyle="Dashed" AutoPostBack="true" />  
									</div>
                              </div>
                         </div>
                       <!-- end row -->
                       <!-- begin row -->  
                        <div class="row">
                         <div class="col-md-2"></div>
                           <!-- begin col-4 -->
                                <div class="col-md-8">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btnUpload" Text="Upload" class="btn btn-success" 
                                         onclick="btnUpload_Click"  OnClientClick="ProgressBarShow();" />
									<asp:Button runat="server" id="btnSalCalc" Text="Salary Calculate" 
                                         class="btn btn-success" onclick="btnSalCalc_Click"  OnClientClick="ProgressBarShow();" />
                                         
                                         
                                <asp:Button ID="btnSalDown" runat="server" class="btn btn-warning" 
                                         Text="Salary Download" onclick="btnSalDown_Click" />
								 </div>
                               </div>
                              <!-- end col-4 -->
                              <div class="col-md-4"></div>
                         </div>
                        <!-- end row --> 
                        <div class="row">
                       <asp:Panel ID="PanelCivilIncenDate" runat="server" Visible="false">
                        <!-- begin col-4 -->
                                 <div class="col-md-4">
									<div class="form-group">
										<label>Civil From Date</label>
										<asp:TextBox runat="server" ID="txtIstWeekDate" class="form-control datepicker"></asp:TextBox>
									</div>
                                  </div>
                               <!-- end col-4 -->
                               <!-- begin col-4 -->
                                 <div class="col-md-4">
									<div class="form-group">
										<label>Civil To Date</label>
										<asp:TextBox runat="server" ID="txtLastWeekDate" class="form-control datepicker"></asp:TextBox>
									</div>
                                  </div>
                               <!-- end col-4 -->
                       </asp:Panel>
                       </div>
                        </div>
                        
                        <div class="form-group">
                          <div class="row">
                              <asp:GridView ID="gvEmp" runat="server" AutoGenerateColumns="false" >
                                                                    <Columns>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>DeptName</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="DeptName" runat="server" Text='<%# Eval("DepartmentNm") %>' ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <%--<asp:TemplateField>
                                                                            <HeaderTemplate>MachineID</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="MachineID" runat="server" Text='<%# Eval("MachineNo") %>' ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>--%>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>EmpNo</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="lblEmpNo" runat="server" Text='<%# Eval("EmpNo") %>' ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>ExistingCode</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="ExistingCode" runat="server" Text='<%# Eval("ExisistingCode") %>' ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>FirstName</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="FirstName" runat="server" Text='<%# Eval("EmpName") %>' ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>Allowance 3</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="Days" runat="server" Text="0.00" ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>Allowance 4</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="CL" runat="server" Text="0.00" ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>Allowance 5</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="lblHomeTown" runat="server" Text="0.00" ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>Deduction 3</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="lblAbsent" runat="server" Text="0.00" ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>Deduction 4</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblweekoff" runat="server" Text="0.00" ></asp:Label>
                                                                             </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>Deduction 5</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblDed5" runat="server" Text="0.00" ></asp:Label>
                                                                             </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>Advance</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblAdvance" runat="server" Text='<%# Eval("advance") %>'  ></asp:Label>
                                                                             </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>DedOthers1</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblDedOthers1" runat="server" Text="0.00" ></asp:Label>
                                                                             </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>DedOthers2</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblDedOthers2" runat="server" Text="0.00" ></asp:Label>
                                                                             </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                          </div>
                        </div>
                        <div id="Download_loader" style="display:none"/></div>
                        </div>
                           
                    </div>
                    <!-- end panel -->
                </div>
                 </ContentTemplate>
                 <Triggers>
                    <asp:PostBackTrigger ControlID="btnSalDown" />
                    <asp:PostBackTrigger ControlID="btnUpload" />
                 </Triggers>
              </asp:UpdatePanel>  
			    
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
<!-- end #content -->




</asp:Content>

