﻿<%@ Page Language="C#" MasterPageFile="~/PayrollMaster.master" AutoEventWireup="true" CodeFile="BonusProcess.aspx.cs" Inherits="BonusProcess" Title="" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
        if (sender._postBackSettings.panelsToUpdate != null) {
            $('.select2').select2();
                $('.datepicker').datepicker({
                    format: "dd/mm/yyyy",
                    autoclose: true
                });
            }
        });
    };
</script>
 <script type="text/javascript">
     function SaveMsgAlert(msg) {
         swal(msg);
     }
</script>

<script type="text/javascript">
    function ProgressBarShow() {
        $('#Download_loader').show();
    }
</script>

<script type="text/javascript">
    function ProgressBarHide() {
        $('#Download_loader').hide();
    }
</script>

 <!-- begin #content -->
    
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Bonus</a></li>
				<li class="active">Bonus Process</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Bonus Process</h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                 <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                  <ContentTemplate>
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">Bonus Process</h4>
                        </div>
                        <div class="panel-body">
                        <div class="form-group">
                       <!-- begin row -->
                        <div class="row">
                        <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Category</label>
								<asp:DropDownList runat="server" ID="ddlCategory" class="form-control select2" AutoPostBack="true"
                                        style="width:100%;" onselectedindexchanged="ddlCategory_SelectedIndexChanged1">
								 <asp:ListItem Value="0">-Select-</asp:ListItem>
								 <asp:ListItem Value="1">Staff</asp:ListItem>
								 <asp:ListItem Value="2">Labour</asp:ListItem>
								</asp:DropDownList>
								</div>
                               </div>
                           <!-- end col-4 -->
                           <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Employee Type</label>
								 <asp:DropDownList runat="server" ID="ddlEmployeeType" class="form-control select2" style="width:100%;">
								 </asp:DropDownList>
								</div>
                               </div>
                           <!-- end col-4 -->
                            <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Bonus Year</label>
								 <asp:DropDownList runat="server" ID="ddlFinance" class="form-control select2" style="width:100%;">
								 </asp:DropDownList>
								</div>
                               </div>
                           <!-- end col-4 -->
                        </div>
                       <!-- end row -->
                     
                      
                       <!-- begin row -->  
                        <div class="row">
                         <div class="col-md-4"></div>
                           <!-- begin col-4 -->
                                <div class="col-md-4">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btnBonusProcess" Text="Bonus Process" 
                                         class="btn btn-success" onclick="btnBonusProcess_Click" OnClientClick="ProgressBarShow();"/>
						    	 </div>
                               </div>
                              <!-- end col-4 -->
                              <div class="col-md-4"></div>
                         </div>
                        <!-- end row --> 
                            <asp:panel runat="server" visible="false">
                     <legend>Bonus Report</legend>
                        
                     <!-- begin row -->
                        <div class="row">
                        <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Category</label>
								 <asp:DropDownList runat="server" ID="ddlcategory_Rpt" class="form-control select2" style="width:100%;" AutoPostBack="true"
								  onselectedindexchanged="ddlcategory_Rpt_SelectedIndexChanged">
								 <asp:ListItem Value="0">-Select-</asp:ListItem>
								 <asp:ListItem Value="1">Staff</asp:ListItem>
								 <asp:ListItem Value="2">Labour</asp:ListItem>
								</asp:DropDownList>
								</div>
                               </div>
                           <!-- end col-4 -->
                           <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Employee Type</label>
								 <asp:DropDownList runat="server" ID="ddlRptEmpType" class="form-control select2" style="width:100%;">
								 </asp:DropDownList>
								</div>
                               </div>
                           <!-- end col-4 -->
                            <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Bonus Year</label>
								 <asp:DropDownList runat="server" ID="txtBonusYear" class="form-control select2" style="width:100%;">
								 </asp:DropDownList>
								</div>
                               </div>
                           <!-- end col-4 -->
                        </div>
                       <!-- end row -->
                       <!-- begin row -->
                        <div class="row">
                        <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Salary Through</label>
								
								    <asp:RadioButtonList ID="RdbCashBank" runat="server" RepeatColumns="3" class="form-control">
                                         <asp:ListItem Selected="true" Text="ALL" style="padding-right:40px" Value="0"></asp:ListItem>
                                         <asp:ListItem Selected="False" Text="Cash" style="padding-right:40px" Value="1"></asp:ListItem>
                                         <asp:ListItem Selected="False" Text="Bank" Value="2"></asp:ListItem>
                                     </asp:RadioButtonList>
								 </div>
                               </div>
                           <!-- end col-4 -->
                           <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Division</label>
								 <asp:DropDownList runat="server" ID="ddlDivision" class="form-control select2" style="width:100%;">
								 </asp:DropDownList>
								</div>
                               </div>
                           <!-- end col-4 -->
                            <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>PF/Non PF</label>
								
								    <asp:RadioButtonList ID="RdbPFNonPF" runat="server" RepeatColumns="3" class="form-control">
                                          <asp:ListItem Selected="true" Text="ALL" style="padding-right:40px" Value="0"></asp:ListItem>
                                          <asp:ListItem Selected="False" Text="PF" style="padding-right:40px" Value="1"></asp:ListItem>
                                          <asp:ListItem Selected="False" Text="Non PF" Value="2"></asp:ListItem>
                                     </asp:RadioButtonList>
								 </div>
                               </div>
                           <!-- end col-4 -->
                        </div>
                       <!-- end row -->
                         <!-- begin row -->
                        <div class="row">
                         <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Active Mode</label>
								
								    <asp:RadioButtonList ID="RdbLeftType" runat="server" RepeatColumns="3" class="form-control">
                                         <asp:ListItem Selected="true" Text="ALL" style="padding-right:40px" Value="1"></asp:ListItem>
                                         <asp:ListItem Selected="False" Text="OnRoll" style="padding-right:40px" Value="2"></asp:ListItem>
                                         <asp:ListItem Selected="False" Text="Left" Value="3"></asp:ListItem>
                                     </asp:RadioButtonList>
								 </div>
                               </div>
                           <!-- end col-4 -->
                            <!-- begin col-6 -->
                              <div class="col-md-8">
								<div class="form-group">
								 <label>Report Type</label>
								  <asp:RadioButtonList ID="rdbPayslipFormat" runat="server" RepeatColumns="6">
                                         <asp:ListItem Selected="true" Text="Individual" style="padding-right:30px" Value="1"></asp:ListItem>
                                         <asp:ListItem Selected="False" Text="Recociliation" style="padding-right:30px" Value="2"></asp:ListItem>
                                         <asp:ListItem Selected="False" Text="Checklist" style="padding-right:30px" Value="3"></asp:ListItem>
                                         <asp:ListItem Selected="False" Text="Signlist" style="padding-right:30px" Value="4"></asp:ListItem>
                                         <asp:ListItem Selected="False" Text="Blank Signlist" style="padding-right:30px" Value="5"></asp:ListItem>
                                         <asp:ListItem Selected="False" Text="Cover"  Value="6"></asp:ListItem>
                                     </asp:RadioButtonList>
								 </div>
                               </div>
                           <!-- end col-6 -->
                             <tr id="GridView" runat="server" visible="false">
                                                                <td colspan="5">
                                                                    <asp:GridView ID="GridExcelView" runat="server" AutoGenerateColumns="true">
                                                                    </asp:GridView>
                                                                </td>
                                                            </tr>
                        </div>
                         <!-- end row -->
                         
                             <!-- begin row -->  
                        <div class="row">
                         <div class="col-md-4"></div>
                           <!-- begin col-4 -->
                                <div class="col-md-4">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btnBonusView" Text="Report View" 
                                         class="btn btn-success" onclick="btnBonusView_Click" />
									<asp:Button runat="server" id="btnBonusExcelView" Text="Excel View" 
                                         class="btn btn-success" onclick="btnBonusExcelView_Click" />
						    	 </div>
                               </div>
                              <!-- end col-4 -->
                              <div class="col-md-4"></div>
                         </div>
                                </asp:panel>
                          <%--<div id="Download_loader" style="display:none"/></div>--%>

                        <!-- end row --> 
                        </div>
                        </div>
                        
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
                      </ContentTemplate>
                     <Triggers>
                         <asp:PostBackTrigger ControlID="btnBonusProcess" />
                     </Triggers>
                      </asp:UpdatePanel>
            </div>
            <!-- end row -->
        </div>
<!-- end #content -->

</asp:Content>

