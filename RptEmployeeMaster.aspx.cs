﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
public partial class RptEmployeeMaster : System.Web.UI.Page
{
    string ShiftType1 = "";
    string Date = "";
    string Status = "";
    string fmDate = "";
    string Todate = "";
    string Wages = "";

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string ss = "";

    BALDataAccess objdata = new BALDataAccess();

    string SSQL = "";

    DataTable AutoDTable = new DataTable();
    string Shift = "";

    DateTime date1;
    DateTime Date2 = new DateTime();


    System.Web.UI.WebControls.DataGrid grid =
                           new System.Web.UI.WebControls.DataGrid();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Report-Employee Master";
            }
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            Status = Request.QueryString["Status"].ToString();

            Report();
        }
    }

    private void Report()
    {
        string TableName = "";
        if (Status == "Pending")
        {
            TableName = "Employee_Mst_New_Emp";
        }
        else
        {
            TableName = "Employee_Mst";
        }

        SSQL = "";
        SSQL = "Select MachineID,FirstName,Convert(varchar,DOJ,103) as DateOfJoining,DeptName,OTEligible,FixedSalary,EligibleMasthiri_Inc as Masthiri_Incentive,";
        SSQL = SSQL + "ShiftType_New,Hindi_Category,Alllowance1 as Bus_Amt,BaseSalary,Wages,IsActive from " + TableName;

        AutoDTable = objdata.RptEmployeeMultipleDetails(SSQL);

        if (AutoDTable.Rows.Count > 0)
        {
            grid.DataSource = AutoDTable;
            grid.DataBind();
            string attachment = "attachment;filename=EMPLOYEE MASTER.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);
            Response.Write("<table>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='6'>");
            Response.Write("<a style=\"font-weight:bold\">EMPLOYEE MASTER -" + SessionCcode + "-" + SessionLcode +"</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("</table>");
            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();
        }

    }
}