﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;
public partial class PayrollAttendance_New : System.Web.UI.Page
{

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string Division;
    DataTable Log_DS = new DataTable();

    BALDataAccess objdata = new BALDataAccess();


    DataTable AutoDTable = new DataTable();
    DataTable mEmployeeDT = new DataTable();
    DataTable MEmployeeDS = new DataTable();
    DataTable mDataSet = new DataTable();
    DataTable NFH_DS = new DataTable();
    DataTable NFH_Type_Ds = new DataTable();
    DataTable WH_DS = new DataTable();

    DateTime NFH_Date = new DateTime();
    DateTime DOJ_Date_Format = new DateTime();
    string qry_nfh = "";
    string SSQL = "";
    DateTime Week_Off_Date = new DateTime();
    DateTime WH_DOJ_Date_Format = new DateTime();
    Boolean Check_Week_Off_DOJ = false;
    double NFH_Present_Check = 0;
    decimal NFH_Days_Count = 0;
    decimal AEH_NFH_Days_Count = 0;
    decimal LBH_NFH_Days_Count = 0;
    decimal NFH_Days_Present_Count = 0;
    decimal WH_Count = 0;
    decimal WH_Present_Count = 0;
    decimal Present_Days_Count = 0;
    int Fixed_Work_Days = 0;
    decimal Spinning_Incentive_Days = 0;

    decimal NFH_Double_Wages_Checklist = 0;
    decimal NFH_Double_Wages_Statutory = 0;
    decimal NFH_Double_Wages_Manual = 0;
    decimal NFH_WH_Days_Mins = 0;
    decimal NFH_Single_Wages = 0;
    int Month_Mid_Total_Days_Count;
    string NFH_Type_Str = "";
    string NFH_Name_Get_Str = "";
    string NFH_Dbl_Wages_Statutory_Check = "";
    string Emp_WH_Day = "";
    string DOJ_Date_Str = "";


    string NFH_Date_P_Date = "";
    DateTime date1;
    DateTime date2;
    DateTime Date2 = new DateTime();
    int intK;
    string FromDate;
    string ToDate;
    string Date_Value_Str;
    string Date_value_str1;
    string[] Time_Minus_Value_Check;
    string WagesType;
    string WagesValue;

    System.Web.UI.WebControls.DataGrid grid =
                            new System.Web.UI.WebControls.DataGrid();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {

            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report- Payroll Attendance ";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("ManualEntry"));
                //li.Attributes.Add("class", "droplink active open");
            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();

            WagesValue = Request.QueryString["Wages"].ToString();
            Division = Request.QueryString["Division"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["Todate"].ToString();
            //WagesType = WagesValue;

            //if(WagesValue =="1")
            //{
            //    WagesType="STAFF";
            //}
            //else if(WagesValue =="2")
            //{
            //     WagesType="SUB-STAFF";
            //}
            // else if  (WagesValue =="3")
            //{
            //      WagesType="REGULAR";
            //}
            //  else if (WagesValue =="4")
            //{
            //       WagesType="HOSTEL";
            //}
            //  else if (WagesValue =="5")
            //{
            //     WagesType="CIVIL";
            //}
            //  else if (WagesValue =="6")
            //{
            //      WagesType="Watch & Ward";
            //}
            //  else if (WagesValue =="7")
            //{
            //       WagesType="MANAGER";
            //}
            //  else if (WagesValue =="9")
            //{
            //       WagesType="STAFF";
            //}
            //  else if (WagesValue =="10")
            //{
            //       WagesType="OTHERS";
            //}


            mEmployeeDT.Columns.Add("Machine ID");
            mEmployeeDT.Columns.Add("Token No");
            mEmployeeDT.Columns.Add("EmpName");
            mEmployeeDT.Columns.Add("Days");
            mEmployeeDT.Columns.Add("H.Allowed");
            mEmployeeDT.Columns.Add("N/FH");
            mEmployeeDT.Columns.Add("OT Days");
            mEmployeeDT.Columns.Add("SPG Allow");
            mEmployeeDT.Columns.Add("Canteen Days Minus");
            mEmployeeDT.Columns.Add("OT Hours");
            mEmployeeDT.Columns.Add("W.H");
            mEmployeeDT.Columns.Add("Fixed W.Days");
            mEmployeeDT.Columns.Add("NFH W.Days");
            mEmployeeDT.Columns.Add("Total Month Days");
            mEmployeeDT.Columns.Add("NFH Worked Days");
            mEmployeeDT.Columns.Add("NFH D.W Statutory");
            mEmployeeDT.Columns.Add("AEH");
            mEmployeeDT.Columns.Add("LBH");
            mEmployeeDT.Columns.Add("MID Incentive Days");

            if (SessionUserType == "2")
            {
                NonAdminPayrollAttn();
            }
            else
            {

                PayrollAttn();

            }
        }
    }
    public void NonAdminPayrollAttn()
    {

        try
        {
            int intI;
            SSQL = "";
            SSQL = "Select LTD.MachineID,LTD.ExistingCode,EM.FirstName,sum(LTD.Present) as Days,";
            SSQL = SSQL + " EM.DOJ,EM.Weekoff from LogTime_Days LTD inner join Employee_Mst EM on";
            SSQL = SSQL + " EM.CompCode=LTD.CompCode And EM.LocCode=LTD.LocCode And LTD.MachineID=EM.MachineID";
            SSQL = SSQL + " where LTD.CompCode='" + SessionCcode + "' And LTD.LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And EM.CompCode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And EM.Wages='" + WagesValue + "' And (EM.IsActive='Yes' or CONVERT(DATETIME,EM.DOR,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103))";
            SSQL = SSQL + " and CONVERT(DATETIME,LTD.Attn_Date_Str,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103)";
            SSQL = SSQL + " And  CONVERT(DATETIME,LTD.Attn_Date_Str,103)<= CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103) ";
            SSQL = SSQL + " And LTD.Present!='0.0' and EM.Eligible_PF='1' ";
            if (Division != "-Select-")
            {
                SSQL = SSQL + " And EM.Division = '" + Division + "'";
            }
            //If Trim(txtDivision.Text) <> "" Then SSQL = SSQL + " And EM.Division = '" + txtDivision.Text + "'";

            SSQL = SSQL + " Group by LTD.MachineID,LTD.ExistingCode,EM.FirstName,EM.DOJ,EM.Weekoff ";
            SSQL = SSQL + " Order by LTD.ExistingCode Asc";

            Log_DS = objdata.RptEmployeeMultipleDetails(SSQL);

            intI = 2;
            for (int i = 0; i < Log_DS.Rows.Count; i++)
            {

                // 'Final Output Variable Declaration
                NFH_Days_Count = 0;
                AEH_NFH_Days_Count = 0;
                LBH_NFH_Days_Count = 0;
                NFH_Days_Present_Count = 0;
                WH_Count = 0;
                WH_Present_Count = 0;
                Present_Days_Count = Convert.ToDecimal(Log_DS.Rows[i]["Days"]);

                if (Present_Days_Count > 21)
                {
                    Present_Days_Count = 21;
                }

                Fixed_Work_Days = 0;
                Spinning_Incentive_Days = 0;

                NFH_Double_Wages_Checklist = 0;
                NFH_Double_Wages_Statutory = 0;
                NFH_Double_Wages_Manual = 0;
                NFH_WH_Days_Mins = 0;
                NFH_Single_Wages = 0;

                NFH_Type_Str = "";
                NFH_Name_Get_Str = "";
                NFH_Dbl_Wages_Statutory_Check = "";
                Emp_WH_Day = "";
                DOJ_Date_Str = "";

                DOJ_Date_Str = Log_DS.Rows[i]["DOJ"].ToString();
                Emp_WH_Day = Log_DS.Rows[i]["WeekOff"].ToString();

                //  'NFH Check Start
                string Machineid = Log_DS.Rows[i]["MachineID"].ToString();
                if (Machineid == "1886")
                {
                    Machineid = "1886";
                }

                qry_nfh = "Select NFHDate from NFH_Mst where  CONVERT(DATETIME,DateStr,103) >=  CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103) ";
                qry_nfh = qry_nfh + " And  CONVERT(DATETIME,DateStr,103) <=  CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103) ";
                NFH_DS = objdata.RptEmployeeMultipleDetails(qry_nfh);
                if (NFH_DS.Rows.Count > 0)
                {

                    //  'Date OF Joining Check Start
                    for (int k = 0; k < NFH_DS.Rows.Count; k++)
                    {
                        NFH_Date = Convert.ToDateTime(NFH_DS.Rows[k]["NFHDate"]);
                        // 'NFH Day Present Check

                        NFH_Date_P_Date = String.Format(Convert.ToString(NFH_Date), "yyyy/MM/dd");

                        SSQL = "";
                        SSQL = "Select * from LogTime_Days where Attn_Date=CONVERT(DATETIME,'" + Convert.ToDateTime(NFH_Date_P_Date).ToString("dd/MM/yyyy") + "',103)  ";
                        SSQL = SSQL + "  And MachineID='" + Log_DS.Rows[i]["MachineID"] + "' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";

                        mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (mDataSet.Rows.Count != 0)
                        {

                            NFH_Present_Check = Convert.ToDouble(mDataSet.Rows[0]["Present"]);
                        }
                        else
                        {

                            NFH_Present_Check = 0;

                        }
                        //'Get NFh Type

                        SSQL = "Select * from NFH_Mst where CONVERT(DATETIME,DateStr,103) >=  CONVERT(DATETIME,'" + Convert.ToDateTime(NFH_Date_P_Date).ToString("dd/MM/yyyy") + "',103) ";
                        NFH_Type_Ds = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (NFH_Type_Ds.Rows.Count != 0)
                        {

                            NFH_Type_Str = NFH_Type_Ds.Rows[0]["NFH_Type"].ToString();
                            NFH_Dbl_Wages_Statutory_Check = NFH_Type_Ds.Rows[0]["Dbl_Wages_Statutory"].ToString();
                            NFH_Name_Get_Str = NFH_Type_Ds.Rows[0]["Form25DisplayText"].ToString();

                        }
                        else
                        {
                            NFH_Type_Str = "";
                            NFH_Dbl_Wages_Statutory_Check = "";
                        }
                        if (DOJ_Date_Str != "")
                        {

                            DOJ_Date_Format = Convert.ToDateTime(DOJ_Date_Str);
                            if (DOJ_Date_Format.Date < NFH_Date.Date)
                            {
                                if (NFH_Type_Str == "WH Minus")
                                {
                                    NFH_WH_Days_Mins = NFH_WH_Days_Mins + 1;

                                }
                                else
                                {

                                    NFH_Days_Present_Count = NFH_Days_Present_Count + Convert.ToDecimal(NFH_Present_Check);
                                    if (NFH_Type_Str == "Double Wages Checklist")
                                    {

                                        NFH_Double_Wages_Checklist = NFH_Double_Wages_Checklist + Convert.ToDecimal(NFH_Present_Check);

                                        if (NFH_Dbl_Wages_Statutory_Check == "Yes")
                                        {
                                            NFH_Double_Wages_Statutory = NFH_Double_Wages_Statutory + Convert.ToDecimal(NFH_Present_Check);

                                        }
                                    }

                                    if (NFH_Type_Str == "Double Wages Manual")
                                    {
                                        NFH_Double_Wages_Manual = NFH_Double_Wages_Manual + Convert.ToDecimal(NFH_Present_Check);
                                    }

                                    NFH_Days_Count = NFH_Days_Count + 1;
                                    if (NFH_Name_Get_Str == "AEH")
                                    {
                                        AEH_NFH_Days_Count = AEH_NFH_Days_Count + 1;
                                    }
                                    if (NFH_Name_Get_Str == "LBH")
                                    {
                                        LBH_NFH_Days_Count = LBH_NFH_Days_Count + 1;
                                    }

                                }
                            }
                        }




                        else
                        {
                            NFH_Days_Count = NFH_Days_Count + 1;

                            if (NFH_Name_Get_Str == "AEH")
                            {
                                AEH_NFH_Days_Count = AEH_NFH_Days_Count + 1;

                            }
                            if (NFH_Name_Get_Str == "LBH")
                            {
                                LBH_NFH_Days_Count = LBH_NFH_Days_Count + 1;

                            }

                            if (NFH_Type_Str == "WH Minus")
                            {
                                NFH_WH_Days_Mins = NFH_WH_Days_Mins + 1;
                            }
                            else
                            {
                                NFH_Days_Present_Count = NFH_Days_Present_Count + Convert.ToDecimal(NFH_Present_Check);

                                if (NFH_Type_Str == "Double Wages Checklist")
                                {
                                    NFH_Double_Wages_Checklist = NFH_Double_Wages_Checklist + Convert.ToDecimal(NFH_Present_Check);
                                    if (NFH_Dbl_Wages_Statutory_Check == "Yes")
                                    {
                                        NFH_Double_Wages_Statutory = NFH_Double_Wages_Statutory + Convert.ToDecimal(NFH_Present_Check);
                                    }
                                }
                                if (NFH_Type_Str == "Double Wages Manual")
                                {
                                    NFH_Double_Wages_Manual = NFH_Double_Wages_Manual + Convert.ToDecimal(NFH_Present_Check);
                                }
                            }



                        }
                    }
                }// NFH Check END


                //'Week of Check

                Check_Week_Off_DOJ = false;

                SSQL = "Select Present,Attn_Date from LogTime_Days where MachineID='" + Log_DS.Rows[i]["MachineID"] + "' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                SSQL = SSQL + " And upper(DATENAME(weekday,Attn_Date))=upper('" + Emp_WH_Day + "')";
                SSQL = SSQL + " And Attn_Date >= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + " And Attn_Date <=CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103)";
                WH_DS = objdata.RptEmployeeMultipleDetails(SSQL);
                for (int j = 0; j < WH_DS.Rows.Count; j++)
                {

                    Week_Off_Date = Convert.ToDateTime(WH_DS.Rows[j]["Attn_Date"]);
                    if (WagesType.ToUpper() == "STAFF".ToUpper() || WagesType.ToUpper() == "Watch & Ward".ToUpper() || WagesType.ToUpper() == "Manager".ToUpper())
                    {
                        if (DOJ_Date_Str != "")
                        {

                            WH_DOJ_Date_Format = Convert.ToDateTime(DOJ_Date_Str);
                            if (WH_DOJ_Date_Format.Date <= Week_Off_Date.Date)
                            {

                                Check_Week_Off_DOJ = true;
                            }
                            else
                            {
                                Check_Week_Off_DOJ = false;
                            }
                        }
                        else
                        {
                            Check_Week_Off_DOJ = true;
                        }
                    }
                    else
                    {
                        Check_Week_Off_DOJ = true;
                    }

                    if (Check_Week_Off_DOJ == true)
                    {

                        WH_Count = WH_Count + 1;
                        //'NFH Day Check
                        // String.Format(Convert.ToString(dtime), "yyyy/MM/dd");

                        string NFH_Date_WH_Date = Convert.ToDateTime(Week_Off_Date).AddDays(0).ToString("yyyy/MM/dd");
                        SSQL = "Select * from NFH_Mst where CONVERT(DATETIME,DateStr,103) >=  CONVERT(DATETIME,'" + Convert.ToDateTime(NFH_Date_WH_Date).ToString("dd/MM/yyyy") + "',103) ";
                        mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (mDataSet.Rows.Count > 0)
                        {
                            if (mDataSet.Rows.Count != 0)
                            {
                                NFH_Type_Str = mDataSet.Rows[0]["NFH_Type"].ToString();
                                NFH_Dbl_Wages_Statutory_Check = NFH_Type_Ds.Rows[0]["Dbl_Wages_Statutory"].ToString();
                            }
                            else
                            {
                                NFH_Type_Str = "";
                                NFH_Dbl_Wages_Statutory_Check = "";
                            }
                            if (NFH_Type_Str == "WH Minus")
                            {
                                WH_Present_Count = WH_Present_Count + Convert.ToDecimal(WH_DS.Rows[j]["Present"].ToString());
                            }


                        }
                        else
                        {
                            WH_Present_Count = WH_Present_Count + Convert.ToDecimal(WH_DS.Rows[j]["Present"].ToString());
                        }
                    }
                }




                //'Spinning Incentive Check
                if (WagesValue.ToUpper() == "REGULAR".ToUpper() || WagesValue.ToUpper() == "HOSTEL".ToUpper())
                {
                    //'Check Spinning Days Start
                    Boolean Check_Spinning_Eligible = false;
                    DataTable DS_Spinning_Incv = new DataTable();
                    // 'Spinning Wages Check
                    int Month_Int = 1;
                    string Months_Full_Str = "";
                    string Fin_Year = "";
                    string Spin_Wages = WagesType;
                    string Spin_Machine_ID_Str = Log_DS.Rows[i]["MachineID"].ToString();

                    string[] date = FromDate.Split('/');
                    string mon = date[1];
                    Month_Int = Convert.ToInt32(mon.ToString());
                    if (Month_Int >= 4)
                    {
                        Fin_Year = date[2] + "-" + (Convert.ToInt32(date[2]) + 1);
                    }
                    else
                    {
                        Fin_Year = (Convert.ToInt32(date[2]) - 1) + "-" + date[2];
                    }


                    if (Month_Int == 01)
                    {
                        Months_Full_Str = "January";
                    }

                    else if (Month_Int == 02)
                    {
                        Months_Full_Str = "February";
                    }
                    else if (Month_Int == 03)
                    {
                        Months_Full_Str = "March";
                    }
                    else if (Month_Int == 04)
                    {
                        Months_Full_Str = "April";
                    }
                    else if (Month_Int == 05)
                    {
                        Months_Full_Str = "May";
                    }
                    else if (Month_Int == 06)
                    {
                        Months_Full_Str = "June";
                    }
                    else if (Month_Int == 07)
                    {
                        Months_Full_Str = "July";
                    }
                    else if (Month_Int == 08)
                    {
                        Months_Full_Str = "August";
                    }
                    else if (Month_Int == 09)
                    {
                        Months_Full_Str = "September";
                    }
                    else if (Month_Int == 10)
                    {
                        Months_Full_Str = "October";
                    }
                    else if (Month_Int == 11)
                    {
                        Months_Full_Str = "November";
                    }
                    else if (Month_Int == 12)
                    {
                        Months_Full_Str = "December";
                    }

                    Months_Full_Str = Left_Val(Months_Full_Str, 3);
                    SSQL = "Select * from SpinIncentiveDetPart where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And Months='" + Months_Full_Str + "' And FinYear='" + Fin_Year + "' And Wages='" + Spin_Wages + "' And MachineID='" + Spin_Machine_ID_Str + "'";
                    DS_Spinning_Incv = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (DS_Spinning_Incv.Rows.Count != 0)
                    {
                        Check_Spinning_Eligible = true;
                    }
                    else
                    {
                        Check_Spinning_Eligible = false;
                    }
                    if (Check_Spinning_Eligible == true)
                    {
                        if (WagesType.ToUpper() == "HOSTEL".ToUpper())
                        {
                            Spinning_Incentive_Days = Convert.ToDecimal(Log_DS.Rows[i]["Days"].ToString());
                        }
                        else
                        {
                            if (WagesType.ToUpper() == "REGULAR".ToUpper())
                            {
                                SSQL = "Select isnull(sum(Present),0) as Days from LogTime_Days where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                SSQL = SSQL + " And MachineID='" + Log_DS.Rows[i]["MachineID"] + "'";
                                SSQL = SSQL + " And Attn_Date >= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103)";
                                SSQL = SSQL + " And Attn_Date <= CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103)";
                                SSQL = SSQL + " And (Shift='SHIFT2' or Shift='SHIFT3')";
                                mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);

                                if (mDataSet.Rows.Count != 0)
                                {
                                    Spinning_Incentive_Days = Convert.ToDecimal(mDataSet.Rows[0]["Days"].ToString());
                                }
                                else
                                {
                                    Spinning_Incentive_Days = 0;
                                }
                            }
                            else
                            {
                                Spinning_Incentive_Days = 0;
                            }

                        }

                    }

                    else
                    {
                        Spinning_Incentive_Days = 0;

                    }
                }
                else
                {
                    Spinning_Incentive_Days = 0;

                }



                //'Total Days Get

                date1 = Convert.ToDateTime(FromDate);
                // string dd =string.Format("{dd/MM/yyyy}",TxtTo.Text);
                date2 = Convert.ToDateTime(ToDate);
                int dayCount = (int)((date2 - date1).TotalDays);
                int Total_Days_Count = dayCount;
                Total_Days_Count = Total_Days_Count + 1;
                Month_Mid_Total_Days_Count = 0;

                // 'Check DOJ Date to Report Date
                DateTime Report_Date = new DateTime();
                DateTime DOJ_Date_Format_Check = new DateTime();
                if (WagesValue.ToUpper() == "STAFF".ToUpper() || WagesValue.ToUpper() == "Watch & Ward".ToUpper() || WagesValue.ToUpper() == "Manager".ToUpper())
                {
                    if (DOJ_Date_Str != "")
                    {
                        Report_Date = Convert.ToDateTime(FromDate.ToString()); //'Report Date
                        DOJ_Date_Format_Check = Convert.ToDateTime(DOJ_Date_Str);
                        if (DOJ_Date_Format_Check <= Report_Date)
                        {
                            Month_Mid_Total_Days_Count = Total_Days_Count;
                        }
                        else
                        {
                            Month_Mid_Total_Days_Count = dayCount;
                            Month_Mid_Total_Days_Count = Month_Mid_Total_Days_Count + 1;
                        }

                    }
                    else
                    {
                        Month_Mid_Total_Days_Count = Total_Days_Count;
                    }

                }
                else
                {

                }

                if (WagesValue.ToUpper() == "STAFF".ToUpper() || WagesValue.ToUpper() == "Watch & Ward".ToUpper() || WagesValue.ToUpper() == "Manager".ToUpper())
                {
                    Fixed_Work_Days = Month_Mid_Total_Days_Count - Convert.ToInt32(WH_Count);
                    Present_Days_Count = Present_Days_Count - WH_Present_Count;
                    Present_Days_Count = Present_Days_Count - NFH_Days_Present_Count;
                    WH_Present_Count = WH_Present_Count + NFH_Days_Present_Count;
                }
                else
                {
                    Fixed_Work_Days = 0;
                    Present_Days_Count = Present_Days_Count - WH_Present_Count;
                    Present_Days_Count = Present_Days_Count - NFH_Days_Present_Count;
                    WH_Present_Count = WH_Present_Count;
                }
                if (Present_Days_Count != 0 && NFH_Days_Count != 0 && NFH_WH_Days_Mins != 0)
                {
                    double NFH_Week_Off_Minus_Days = Convert.ToDouble(NFH_WH_Days_Mins);
                    double NFH_Final_Disp_Days = 0;
                    if (NFH_Week_Off_Minus_Days <= Convert.ToDouble(WH_Present_Count))
                    {
                        WH_Present_Count = WH_Present_Count - Convert.ToDecimal(NFH_Week_Off_Minus_Days);
                    }
                    else
                    {
                        // 'NFH Days Minus for Working Days
                        double Balance_NFH_Days_Minus_IN_WorkDays = 0;

                        Balance_NFH_Days_Minus_IN_WorkDays = NFH_Week_Off_Minus_Days - Convert.ToDouble(WH_Present_Count);
                        Present_Days_Count = Present_Days_Count - Convert.ToDecimal(Balance_NFH_Days_Minus_IN_WorkDays);
                        WH_Present_Count = 0;
                    }
                }



                string NFH_Doubale_Wages_Attn_Inct = (NFH_Double_Wages_Checklist + NFH_Double_Wages_Manual + NFH_WH_Days_Mins).ToString();

                if (Present_Days_Count != 0)
                {


                    mEmployeeDT.NewRow();
                    mEmployeeDT.Rows.Add();
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Machine ID"] = Log_DS.Rows[i]["MachineID"].ToString();
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Token No"] = Log_DS.Rows[i]["ExistingCode"].ToString();
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["EmpName"] = Log_DS.Rows[i]["FirstName"].ToString();
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Days"] = Present_Days_Count;
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["H.Allowed"] = "0";
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["N/FH"] = NFH_Days_Count;

                    if (WagesValue.ToUpper() == "STAFF".ToUpper() || WagesValue.ToUpper() == "Watch & Ward".ToUpper() || WagesValue.ToUpper() == "Manager".ToUpper())
                    {
                        mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["OT Days"] = "0";
                        mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["SPG Allow"] = "0";
                        mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Canteen Days Minus"] = "0";
                        mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["OT Hours"] = "0";
                        mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["W.H"] = WH_Present_Count;
                        mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Fixed W.Days"] = Fixed_Work_Days;
                    }
                    else
                    {
                        mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["OT Days"] = WH_Present_Count;
                        mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["SPG Allow"] = Spinning_Incentive_Days;
                        mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Canteen Days Minus"] = "0";
                        mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["OT Hours"] = "0";
                        mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["W.H"] = "0";
                        mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Fixed W.Days"] = "0";
                    }



                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["NFH W.Days"] = NFH_Double_Wages_Checklist;
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Total Month Days"] = Month_Mid_Total_Days_Count;
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["NFH Worked Days"] = NFH_Doubale_Wages_Attn_Inct;
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["NFH D.W Statutory"] = NFH_Double_Wages_Statutory;
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["AEH"] = AEH_NFH_Days_Count;
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["LBH"] = LBH_NFH_Days_Count;
                    intI = intI + 1;
                }



                NFH_Days_Count = 0;
                AEH_NFH_Days_Count = 0;
                LBH_NFH_Days_Count = 0;
                NFH_Double_Wages_Checklist = 0;
                NFH_Double_Wages_Statutory = 0;
                NFH_Double_Wages_Manual = 0;
                NFH_WH_Days_Mins = 0;


            }


        }

        catch (Exception ex)
        {

        }


        UploadDataTableToExcel(mEmployeeDT);

    }

    public void PayrollAttn()
    {

        try
        {
            int intI;
            SSQL = "";
            SSQL = "Select LTD.MachineID,LTD.ExistingCode,EM.FirstName,sum(LTD.Present) as Days,SUM(LTD.MID_Present) as MID_InctDays,";
            SSQL = SSQL + " EM.DOJ,EM.Weekoff from LogTime_Days LTD inner join Employee_Mst EM on";
            SSQL = SSQL + " EM.CompCode=LTD.CompCode And EM.LocCode=LTD.LocCode And LTD.MachineID=EM.MachineID";
            SSQL = SSQL + " where LTD.CompCode='" + SessionCcode + "' And LTD.LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And EM.CompCode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And EM.Wages='" + WagesValue + "' And (EM.IsActive='Yes' or CONVERT(DATETIME,EM.DOR,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103))";
            SSQL = SSQL + " and CONVERT(DATETIME,LTD.Attn_Date_Str,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103)";
            SSQL = SSQL + " And  CONVERT(DATETIME,LTD.Attn_Date_Str,103)<= CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103) ";
            //SSQL = SSQL + " And (LTD.Present!='0.0'  or FixedSalary='Yes' ) ";
            SSQL = SSQL + " and  (LTD.MID_present!='0.0' or FixedSalary='Yes')";
            if (Division != "-Select-")
            {
                SSQL = SSQL + " And EM.Division = '" + Division + "'";
            }
            //If Trim(txtDivision.Text) <> "" Then SSQL = SSQL + " And EM.Division = '" + txtDivision.Text + "'";

            SSQL = SSQL + " Group by LTD.MachineID,LTD.ExistingCode,EM.FirstName,EM.DOJ,EM.Weekoff ";
            SSQL = SSQL + " Order by LTD.ExistingCode Asc";

            Log_DS = objdata.RptEmployeeMultipleDetails(SSQL);

            intI = 2;
            for (int i = 0; i < Log_DS.Rows.Count; i++)
            {

                // 'Final Output Variable Declaration
                NFH_Days_Count = 0;
                AEH_NFH_Days_Count = 0;
                LBH_NFH_Days_Count = 0;
                NFH_Days_Present_Count = 0;
                WH_Count = 0;
                WH_Present_Count = 0;
                Present_Days_Count = Convert.ToDecimal(Log_DS.Rows[i]["Days"]);


                Fixed_Work_Days = 0;
                Spinning_Incentive_Days = 0;

                NFH_Double_Wages_Checklist = 0;
                NFH_Double_Wages_Statutory = 0;
                NFH_Double_Wages_Manual = 0;
                NFH_WH_Days_Mins = 0;
                NFH_Single_Wages = 0;

                NFH_Type_Str = "";
                NFH_Name_Get_Str = "";
                NFH_Dbl_Wages_Statutory_Check = "";
                Emp_WH_Day = "";
                DOJ_Date_Str = "";

                DOJ_Date_Str = Log_DS.Rows[i]["DOJ"].ToString();
                Emp_WH_Day = Log_DS.Rows[i]["WeekOff"].ToString();

                //  'NFH Check Start
                string Machineid = Log_DS.Rows[i]["MachineID"].ToString();
                if (Machineid == "1886")
                {
                    Machineid = "1886";
                }

                qry_nfh = "Select NFHDate from NFH_Mst where  CONVERT(DATETIME,DateStr,103) >=  CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103) ";
                qry_nfh = qry_nfh + " And  CONVERT(DATETIME,DateStr,103) <=  CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103) ";
                NFH_DS = objdata.RptEmployeeMultipleDetails(qry_nfh);
                if (NFH_DS.Rows.Count > 0)
                {

                    //  'Date OF Joining Check Start
                    for (int k = 0; k < NFH_DS.Rows.Count; k++)
                    {
                        NFH_Date = Convert.ToDateTime(NFH_DS.Rows[k]["NFHDate"]);
                        // 'NFH Day Present Check

                        NFH_Date_P_Date = String.Format(Convert.ToString(NFH_Date), "yyyy/MM/dd");

                        SSQL = "";
                        SSQL = "Select * from LogTime_Days where Attn_Date=CONVERT(DATETIME,'" + Convert.ToDateTime(NFH_Date_P_Date).ToString("dd/MM/yyyy") + "',103)  ";
                        SSQL = SSQL + "  And MachineID='" + Log_DS.Rows[i]["MachineID"] + "' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";

                        mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (mDataSet.Rows.Count != 0)
                        {

                            NFH_Present_Check = Convert.ToDouble(mDataSet.Rows[0]["Present"]);
                        }
                        else
                        {

                            NFH_Present_Check = 0;

                        }
                        //'Get NFh Type

                        SSQL = "Select * from NFH_Mst where CONVERT(DATETIME,DateStr,103) >=  CONVERT(DATETIME,'" + Convert.ToDateTime(NFH_Date_P_Date).ToString("dd/MM/yyyy") + "',103) ";
                        NFH_Type_Ds = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (NFH_Type_Ds.Rows.Count != 0)
                        {

                            NFH_Type_Str = NFH_Type_Ds.Rows[0]["NFH_Type"].ToString();
                            NFH_Dbl_Wages_Statutory_Check = NFH_Type_Ds.Rows[0]["Dbl_Wages_Statutory"].ToString();
                            NFH_Name_Get_Str = NFH_Type_Ds.Rows[0]["Form25DisplayText"].ToString();

                        }
                        else
                        {
                            NFH_Type_Str = "";
                            NFH_Dbl_Wages_Statutory_Check = "";
                        }
                        if (DOJ_Date_Str != "")
                        {

                            DOJ_Date_Format = Convert.ToDateTime(DOJ_Date_Str);
                            if (DOJ_Date_Format.Date < NFH_Date.Date)
                            {
                                if (NFH_Type_Str == "WH Minus")
                                {
                                    NFH_WH_Days_Mins = NFH_WH_Days_Mins + 1;

                                }
                                else
                                {

                                    NFH_Days_Present_Count = NFH_Days_Present_Count + Convert.ToDecimal(NFH_Present_Check);
                                    if (NFH_Type_Str == "Double Wages Checklist")
                                    {

                                        NFH_Double_Wages_Checklist = NFH_Double_Wages_Checklist + Convert.ToDecimal(NFH_Present_Check);

                                        if (NFH_Dbl_Wages_Statutory_Check == "Yes")
                                        {
                                            NFH_Double_Wages_Statutory = NFH_Double_Wages_Statutory + Convert.ToDecimal(NFH_Present_Check);

                                        }
                                    }

                                    if (NFH_Type_Str == "Double Wages Manual")
                                    {
                                        NFH_Double_Wages_Manual = NFH_Double_Wages_Manual + Convert.ToDecimal(NFH_Present_Check);
                                    }

                                    NFH_Days_Count = NFH_Days_Count + 1;
                                    if (NFH_Name_Get_Str == "AEH")
                                    {
                                        AEH_NFH_Days_Count = AEH_NFH_Days_Count + 1;
                                    }
                                    if (NFH_Name_Get_Str == "LBH")
                                    {
                                        LBH_NFH_Days_Count = LBH_NFH_Days_Count + 1;
                                    }

                                }
                            }
                        }




                        else
                        {
                            NFH_Days_Count = NFH_Days_Count + 1;

                            if (NFH_Name_Get_Str == "AEH")
                            {
                                AEH_NFH_Days_Count = AEH_NFH_Days_Count + 1;

                            }
                            if (NFH_Name_Get_Str == "LBH")
                            {
                                LBH_NFH_Days_Count = LBH_NFH_Days_Count + 1;

                            }

                            if (NFH_Type_Str == "WH Minus")
                            {
                                NFH_WH_Days_Mins = NFH_WH_Days_Mins + 1;
                            }
                            else
                            {
                                NFH_Days_Present_Count = NFH_Days_Present_Count + Convert.ToDecimal(NFH_Present_Check);

                                if (NFH_Type_Str == "Double Wages Checklist")
                                {
                                    NFH_Double_Wages_Checklist = NFH_Double_Wages_Checklist + Convert.ToDecimal(NFH_Present_Check);
                                    if (NFH_Dbl_Wages_Statutory_Check == "Yes")
                                    {
                                        NFH_Double_Wages_Statutory = NFH_Double_Wages_Statutory + Convert.ToDecimal(NFH_Present_Check);
                                    }
                                }
                                if (NFH_Type_Str == "Double Wages Manual")
                                {
                                    NFH_Double_Wages_Manual = NFH_Double_Wages_Manual + Convert.ToDecimal(NFH_Present_Check);
                                }
                            }



                        }
                    }
                }// NFH Check END


                //'Week of Check

                Check_Week_Off_DOJ = false;

                SSQL = "Select Present,Attn_Date from LogTime_Days where MachineID='" + Log_DS.Rows[i]["MachineID"] + "' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                SSQL = SSQL + " And upper(DATENAME(weekday,Attn_Date))=upper('" + Emp_WH_Day + "')";
                SSQL = SSQL + " And Attn_Date >= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + " And Attn_Date <=CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103)";
                WH_DS = objdata.RptEmployeeMultipleDetails(SSQL);
                for (int j = 0; j < WH_DS.Rows.Count; j++)
                {

                    Week_Off_Date = Convert.ToDateTime(WH_DS.Rows[j]["Attn_Date"]);
                    if (WagesType.ToUpper() == "STAFF".ToUpper() || WagesType.ToUpper() == "Watch & Ward".ToUpper() || WagesType.ToUpper() == "Manager".ToUpper())
                    {
                        if (DOJ_Date_Str != "")
                        {

                            WH_DOJ_Date_Format = Convert.ToDateTime(DOJ_Date_Str);
                            if (WH_DOJ_Date_Format.Date <= Week_Off_Date.Date)
                            {

                                Check_Week_Off_DOJ = true;
                            }
                            else
                            {
                                Check_Week_Off_DOJ = false;
                            }
                        }
                        else
                        {
                            Check_Week_Off_DOJ = true;
                        }
                    }
                    else
                    {
                        Check_Week_Off_DOJ = true;
                    }

                    if (Check_Week_Off_DOJ == true)
                    {

                        WH_Count = WH_Count + 1;
                        //'NFH Day Check
                        // String.Format(Convert.ToString(dtime), "yyyy/MM/dd");

                        string NFH_Date_WH_Date = Convert.ToDateTime(Week_Off_Date).AddDays(0).ToString("yyyy/MM/dd");
                        SSQL = "Select * from NFH_Mst where CONVERT(DATETIME,DateStr,103) >=  CONVERT(DATETIME,'" + Convert.ToDateTime(NFH_Date_WH_Date).ToString("dd/MM/yyyy") + "',103) ";
                        mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (mDataSet.Rows.Count > 0)
                        {
                            if (mDataSet.Rows.Count != 0)
                            {
                                NFH_Type_Str = mDataSet.Rows[0]["NFH_Type"].ToString();
                                NFH_Dbl_Wages_Statutory_Check = NFH_Type_Ds.Rows[0]["Dbl_Wages_Statutory"].ToString();
                            }
                            else
                            {
                                NFH_Type_Str = "";
                                NFH_Dbl_Wages_Statutory_Check = "";
                            }
                            if (NFH_Type_Str == "WH Minus")
                            {
                                WH_Present_Count = WH_Present_Count + Convert.ToDecimal(WH_DS.Rows[j]["Present"].ToString());
                            }


                        }
                        else
                        {
                            WH_Present_Count = WH_Present_Count + Convert.ToDecimal(WH_DS.Rows[j]["Present"].ToString());
                        }
                    }
                }


                //'Spinning Incentive Check
                if (WagesValue.ToUpper() == "REGULAR".ToUpper() || WagesValue.ToUpper() == "HOSTEL".ToUpper())
                {
                    //'Check Spinning Days Start
                    Boolean Check_Spinning_Eligible = false;
                    DataTable DS_Spinning_Incv = new DataTable();
                    // 'Spinning Wages Check
                    int Month_Int = 1;
                    string Months_Full_Str = "";
                    string Fin_Year = "";
                    string Spin_Wages = WagesType;
                    string Spin_Machine_ID_Str = Log_DS.Rows[i]["MachineID"].ToString();

                    string[] date = FromDate.Split('/');
                    string mon = date[1];
                    Month_Int = Convert.ToInt32(mon.ToString());
                    if (Month_Int >= 4)
                    {
                        Fin_Year = date[2] + "-" + (Convert.ToInt32(date[2]) + 1);
                    }
                    else
                    {
                        Fin_Year = (Convert.ToInt32(date[2]) - 1) + "-" + date[2];
                    }


                    if (Month_Int == 01)
                    {
                        Months_Full_Str = "January";
                    }

                    else if (Month_Int == 02)
                    {
                        Months_Full_Str = "February";
                    }
                    else if (Month_Int == 03)
                    {
                        Months_Full_Str = "March";
                    }
                    else if (Month_Int == 04)
                    {
                        Months_Full_Str = "April";
                    }
                    else if (Month_Int == 05)
                    {
                        Months_Full_Str = "May";
                    }
                    else if (Month_Int == 06)
                    {
                        Months_Full_Str = "June";
                    }
                    else if (Month_Int == 07)
                    {
                        Months_Full_Str = "July";
                    }
                    else if (Month_Int == 08)
                    {
                        Months_Full_Str = "August";
                    }
                    else if (Month_Int == 09)
                    {
                        Months_Full_Str = "September";
                    }
                    else if (Month_Int == 10)
                    {
                        Months_Full_Str = "October";
                    }
                    else if (Month_Int == 11)
                    {
                        Months_Full_Str = "November";
                    }
                    else if (Month_Int == 12)
                    {
                        Months_Full_Str = "December";
                    }

                    Months_Full_Str = Left_Val(Months_Full_Str, 3);
                    SSQL = "Select * from SpinIncentiveDetPart where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And Months='" + Months_Full_Str + "' And FinYear='" + Fin_Year + "' And Wages='" + Spin_Wages + "' And MachineID='" + Spin_Machine_ID_Str + "'";
                    DS_Spinning_Incv = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (DS_Spinning_Incv.Rows.Count != 0)
                    {
                        Check_Spinning_Eligible = true;
                    }
                    else
                    {
                        Check_Spinning_Eligible = false;
                    }
                    if (Check_Spinning_Eligible == true)
                    {
                        if (WagesType.ToUpper() == "HOSTEL".ToUpper())
                        {
                            Spinning_Incentive_Days = Convert.ToDecimal(Log_DS.Rows[i]["Days"].ToString());
                        }
                        else
                        {
                            if (WagesType.ToUpper() == "REGULAR".ToUpper())
                            {
                                SSQL = "Select isnull(sum(Present),0) as Days from LogTime_Days where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                SSQL = SSQL + " And MachineID='" + Log_DS.Rows[i]["MachineID"] + "'";
                                SSQL = SSQL + " And Attn_Date >= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103)";
                                SSQL = SSQL + " And Attn_Date <= CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103)";
                                SSQL = SSQL + " And (Shift='SHIFT2' or Shift='SHIFT3')";
                                mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);

                                if (mDataSet.Rows.Count != 0)
                                {
                                    Spinning_Incentive_Days = Convert.ToDecimal(mDataSet.Rows[0]["Days"].ToString());
                                }
                                else
                                {
                                    Spinning_Incentive_Days = 0;
                                }
                            }
                            else
                            {
                                Spinning_Incentive_Days = 0;
                            }

                        }

                    }

                    else
                    {
                        Spinning_Incentive_Days = 0;

                    }
                }
                else
                {
                    Spinning_Incentive_Days = 0;

                }



                //'Total Days Get

                date1 = Convert.ToDateTime(FromDate);
                // string dd =string.Format("{dd/MM/yyyy}",TxtTo.Text);
                date2 = Convert.ToDateTime(ToDate);
                int dayCount = (int)((date2 - date1).TotalDays);
                int Total_Days_Count = dayCount;
                Total_Days_Count = Total_Days_Count + 1;
                Month_Mid_Total_Days_Count = 0;

                // 'Check DOJ Date to Report Date
                DateTime Report_Date = new DateTime();
                DateTime DOJ_Date_Format_Check = new DateTime();
                if (WagesValue.ToUpper() == "STAFF".ToUpper() || WagesValue.ToUpper() == "Watch & Ward".ToUpper() || WagesValue.ToUpper() == "Manager".ToUpper())
                {
                    if (DOJ_Date_Str != "")
                    {
                        Report_Date = Convert.ToDateTime(FromDate.ToString()); //'Report Date
                        DOJ_Date_Format_Check = Convert.ToDateTime(DOJ_Date_Str);
                        if (DOJ_Date_Format_Check <= Report_Date)
                        {
                            Month_Mid_Total_Days_Count = Total_Days_Count;
                        }
                        else
                        {
                            //Month_Mid_Total_Days_Count = dayCount;
                            Month_Mid_Total_Days_Count = (int)((date2 - DOJ_Date_Format_Check).TotalDays);
                            Month_Mid_Total_Days_Count = Month_Mid_Total_Days_Count + 1;
                        }

                    }
                    else
                    {
                        Month_Mid_Total_Days_Count = Total_Days_Count;
                    }

                }
                else
                {

                }

                if (WagesValue.ToUpper() == "STAFF".ToUpper() || WagesValue.ToUpper() == "Watch & Ward".ToUpper() || WagesValue.ToUpper() == "Manager".ToUpper())
                {
                    Fixed_Work_Days = Month_Mid_Total_Days_Count - Convert.ToInt32(WH_Count);
                    Present_Days_Count = Present_Days_Count - WH_Present_Count;
                    Present_Days_Count = Present_Days_Count - NFH_Days_Present_Count;
                    WH_Present_Count = WH_Present_Count + NFH_Days_Present_Count;
                }
                else
                {
                    Fixed_Work_Days = 0;
                    Present_Days_Count = Present_Days_Count - WH_Present_Count;
                    Present_Days_Count = Present_Days_Count - NFH_Days_Present_Count;
                    WH_Present_Count = WH_Present_Count;
                }
                if (Present_Days_Count != 0 && NFH_Days_Count != 0 && NFH_WH_Days_Mins != 0)
                {
                    double NFH_Week_Off_Minus_Days = Convert.ToDouble(NFH_WH_Days_Mins);
                    double NFH_Final_Disp_Days = 0;
                    if (NFH_Week_Off_Minus_Days <= Convert.ToDouble(WH_Present_Count))
                    {
                        WH_Present_Count = WH_Present_Count - Convert.ToDecimal(NFH_Week_Off_Minus_Days);
                    }
                    else
                    {
                        // 'NFH Days Minus for Working Days
                        double Balance_NFH_Days_Minus_IN_WorkDays = 0;

                        Balance_NFH_Days_Minus_IN_WorkDays = NFH_Week_Off_Minus_Days - Convert.ToDouble(WH_Present_Count);
                        Present_Days_Count = Present_Days_Count - Convert.ToDecimal(Balance_NFH_Days_Minus_IN_WorkDays);
                        WH_Present_Count = 0;
                    }
                }



                string NFH_Doubale_Wages_Attn_Inct = (NFH_Double_Wages_Checklist + NFH_Double_Wages_Manual + NFH_WH_Days_Mins).ToString();

                //if (Present_Days_Count != 0)
                //{


                mEmployeeDT.NewRow();
                mEmployeeDT.Rows.Add();
                mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Machine ID"] = Log_DS.Rows[i]["MachineID"].ToString();
                mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Token No"] = Log_DS.Rows[i]["ExistingCode"].ToString();
                mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["EmpName"] = Log_DS.Rows[i]["FirstName"].ToString();
                mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Days"] = Present_Days_Count;
                mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["H.Allowed"] = "0";
                mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["N/FH"] = NFH_Days_Count;

                if (WagesValue.ToUpper() == "STAFF".ToUpper() || WagesValue.ToUpper() == "Watch & Ward".ToUpper() || WagesValue.ToUpper() == "Manager".ToUpper())
                {
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["OT Days"] = "0";
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["SPG Allow"] = "0";
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Canteen Days Minus"] = "0";
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["OT Hours"] = "0";
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["W.H"] = WH_Present_Count;
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Fixed W.Days"] = Fixed_Work_Days;
                }
                else
                {
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["OT Days"] = WH_Present_Count;
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["SPG Allow"] = Spinning_Incentive_Days;
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Canteen Days Minus"] = "0";
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["OT Hours"] = "0";
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["W.H"] = "0";
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Fixed W.Days"] = "0";
                }



                mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["NFH W.Days"] = NFH_Double_Wages_Checklist;
                mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Total Month Days"] = Month_Mid_Total_Days_Count;
                mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["NFH Worked Days"] = NFH_Doubale_Wages_Attn_Inct;
                mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["NFH D.W Statutory"] = NFH_Double_Wages_Statutory;
                mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["AEH"] = AEH_NFH_Days_Count;
                mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["LBH"] = LBH_NFH_Days_Count;
                mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["MID Incentive Days"] = Log_DS.Rows[i]["MID_InctDays"].ToString();
                intI = intI + 1;
                //}



                NFH_Days_Count = 0;
                AEH_NFH_Days_Count = 0;
                LBH_NFH_Days_Count = 0;
                NFH_Double_Wages_Checklist = 0;
                NFH_Double_Wages_Statutory = 0;
                NFH_Double_Wages_Manual = 0;
                NFH_WH_Days_Mins = 0;


            }


        }




        catch (Exception ex)
        {

        }


        UploadDataTableToExcel(mEmployeeDT);

    }
    protected void UploadDataTableToExcel(DataTable dtRecords)
    {
        string XlsPath = Server.MapPath(@"~/Add_data/EmployeePayroll.xls");
        string attachment = string.Empty;
        if (XlsPath.IndexOf("\\") != -1)
        {
            string[] strFileName = XlsPath.Split(new char[] { '\\' });
            attachment = "attachment; filename=" + strFileName[strFileName.Length - 1];
        }
        else
            attachment = "attachment; filename=" + XlsPath;
        try
        {
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/vnd.ms-excel";
            string tab = string.Empty;

            foreach (DataColumn datacol in dtRecords.Columns)
            {
                Response.Write(tab + datacol.ColumnName);
                tab = "\t";
            }
            Response.Write("\n");

            foreach (DataRow dr in dtRecords.Rows)
            {
                tab = "";
                for (int j = 0; j < dtRecords.Columns.Count; j++)
                {
                    Response.Write(tab + Convert.ToString(dr[j]));
                    tab = "\t";
                }

                Response.Write("\n");
            }
            Response.End();
        }
        catch (Exception ex)
        {
            //Response.Write(ex.Message);
        }
    }
    public string Left_Val(string Value, int Length)
    {
        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }
}