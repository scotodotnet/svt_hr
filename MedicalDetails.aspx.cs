﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class MedicalDetails : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights; string SessionTransNo;
    string SSQL; string SessionUserType;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
            
        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Medical Details";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");

            Initial_Data_Referesh();
            Load_Data_GatePassID();
            Load_Trans_ID();

            if (Session["TransID"] == null)
            {
                
            }
            else
            {
                SessionTransNo = Session["TransID"].ToString();
                txtTransID.Text = SessionTransNo;
                btnSearch_Click(sender, e);
            }

            Load_Data_TokenID();
            Load_Data_DoctorName();
        }
        Load_OLD_data();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        //Search DirectPurchase
        string query = "";
        DataTable Main_DT = new DataTable();
        query = "Select * from MedicalDetails where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And TransID='" + txtTransID.Text + "'";
        Main_DT = objdata.RptEmployeeMultipleDetails(query);
        if (Main_DT.Rows.Count != 0)
        {
            txtDate.Text = Main_DT.Rows[0]["TransDate"].ToString();
            ddlGPOUTNo.SelectedValue = Main_DT.Rows[0]["GatePassID"].ToString();
            ddlGPOUTNo_SelectedIndexChanged(sender, e);

            //JobWork_Main_Sub Table Load

            DataTable dt = new DataTable();
            query = "Select * from MedicalDetails where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And TransID='" + txtTransID.Text + "'";
            dt = objdata.RptEmployeeMultipleDetails(query);
            ViewState["ItemTable"] = dt;
            Repeater1.DataSource = dt;
            Repeater1.DataBind();
            

            btnSave.Text = "Update";
        }
        else
        {
            Clear_All_Field();
        }
    }

    private void Load_OLD_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }

    private void Initial_Data_Referesh()
    {
        DataTable dt = new DataTable();

        dt.Columns.Add(new DataColumn("TokenID", typeof(string)));
        dt.Columns.Add(new DataColumn("MachineID", typeof(string)));
        dt.Columns.Add(new DataColumn("Reason", typeof(string)));
        dt.Columns.Add(new DataColumn("MedicalDetails", typeof(string)));
        dt.Columns.Add(new DataColumn("Amount", typeof(string)));
        dt.Columns.Add(new DataColumn("DoctorName", typeof(string)));
        dt.Columns.Add(new DataColumn("Remarks", typeof(string)));


        Repeater1.DataSource = dt;
        Repeater1.DataBind();
        ViewState["ItemTable"] = Repeater1.DataSource;

    }

    private void Load_Data_GatePassID()
    {
        string query = "";
        DataTable DT = new DataTable();

        //Repeater1.DataSource = DT;
        //Repeater1.DataBind();

        ddlGPOUTNo.Items.Clear();
       // query = "Select Distinct TransID from Medical_GatePass where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        //query = query + " And TransID NOT IN ";
       // query = query + "(Select GatePassID from MedicalDetails where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "')";
        if (Session["TransID"] == null)
        {
            query = "Select Distinct Cast(t1.TransID as varchar(20)) as TransID from Medical_GatePass t1 where t1.Ccode='" + SessionCcode + "' And t1.Lcode='" + SessionLcode + "'";
            query = query + " And t1.TokenID NOT IN ";
            query = query + "(Select TokenID from MedicalDetails where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "')";
        }
        else
        {
            query = "Select GatePassID as TransID from MedicalDetails where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And TransID='" + Session["TransID"].ToString() + "'";
            query = query + " Union ";
            query = query + "Select Distinct Cast(t1.TransID as varchar(20)) as TransID from Medical_GatePass t1 where t1.Ccode='" + SessionCcode + "' And t1.Lcode='" + SessionLcode + "'";
            query = query + " And t1.TokenID NOT IN ";
            query = query + "(Select TokenID from MedicalDetails where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "')";
        }
        DT = objdata.RptEmployeeMultipleDetails(query);
        ddlGPOUTNo.DataSource = DT;
        DataRow dr = DT.NewRow();
        dr["TransID"] = "-Select-";
        dr["TransID"] = "-Select-";
        DT.Rows.InsertAt(dr, 0);
        ddlGPOUTNo.DataTextField = "TransID";
        ddlGPOUTNo.DataValueField = "TransID";
        ddlGPOUTNo.DataBind();
    }

    private void Load_Data_TokenID()
    {
        string query = "";
        DataTable DT = new DataTable();

        //Repeater1.DataSource = DT;
        //Repeater1.DataBind();

        ddlTokenNo.Items.Clear();
        string GP_ID = "0";
        if (ddlGPOUTNo.SelectedItem.Text == "-Select-")
        {
            GP_ID = "0";
        }
        else
        {
            GP_ID = ddlGPOUTNo.SelectedItem.Text;
        }
        if (Session["TransID"] == null)
        {
            query = "Select Distinct TokenID from Medical_GatePass where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            query = query + " And TransID='" + GP_ID + "'";
            query = query + " And TokenID NOT IN ";
            query = query + "(Select TokenID from MedicalDetails where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And GatePassID='" + GP_ID + "')";

        }
        else
        {
            query = "Select TokenID from MedicalDetails where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And GatePassID='" + ddlGPOUTNo.SelectedItem.Text + "'"; 
            query = query + " Union ";
            query = query + "Select Distinct TokenID from Medical_GatePass where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            query = query + " And TransID='" + ddlGPOUTNo.SelectedItem.Text + "'";
            query = query + " And TokenID NOT IN ";
            query = query + "(Select TokenID from MedicalDetails where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And GatePassID='" + ddlGPOUTNo.SelectedItem.Text + "')";
        }

        DT = objdata.RptEmployeeMultipleDetails(query);
        ddlTokenNo.DataSource = DT;
        DataRow dr = DT.NewRow();
        dr["TokenID"] = "-Select-";
        dr["TokenID"] = "-Select-";
        DT.Rows.InsertAt(dr, 0);
        ddlTokenNo.DataTextField = "TokenID";
        ddlTokenNo.DataValueField = "TokenID";
        ddlTokenNo.DataBind();
    }

    private void Load_Data_DoctorName()
    {
        string query = "";
        DataTable DT = new DataTable();

        ddlDoctorName.Items.Clear();

        query = "Select DoctorName from MstDoctor";
        DT = objdata.RptEmployeeMultipleDetails(query);
        ddlDoctorName.DataSource = DT;
        DataRow dr = DT.NewRow();
        dr["DoctorName"] = "-Select-";
        dr["DoctorName"] = "-Select-";
        DT.Rows.InsertAt(dr, 0);
        ddlDoctorName.DataTextField = "DoctorName";
        ddlDoctorName.DataValueField = "DoctorName";
        ddlDoctorName.DataBind();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        DataTable dt1 = new DataTable();
        DataTable DT_Check = new DataTable();
        bool ErrFlag = false;


        //check with Item Details Add with Grid
        DT_Check = (DataTable)ViewState["ItemTable"];
        if (DT_Check.Rows.Count == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You have to add atleast one Employee Details..');", true);
        }

        DataTable dt_Admin  = new DataTable();
        SSQL = "Select Salary,Manual,Medical from MstAdminRights where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " And UserCode='" + SessionUserType + "'";
        dt_Admin = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt_Admin.Rows.Count != 0)
        {
            if (dt_Admin.Rows[0]["Medical"].ToString() == "Yes")
            {
                DateTime CurrDate = Convert.ToDateTime(txtDate.Text);
                if (txtGPOUTDate.Value != "")
                {
                    DateTime GPOUT_Date = Convert.ToDateTime(txtGPOUTDate.Value);

                    int daycount = (int)((CurrDate - GPOUT_Date).TotalDays)+1;
                    
                    if(daycount>2)
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('GatePass OUT has been more than two days!');", true);

                    }
                }
            }
        }


        if (!ErrFlag)
        {
            SSQL = "Select *from MedicalDetails where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " And TransID='" + txtTransID.Text + "'";
            dt1 = objdata.RptEmployeeMultipleDetails(SSQL);

            if (dt1.Rows.Count != 0)
            {
                SSQL = "Delete from MedicalDetails where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " And TransID='" + txtTransID.Text + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);
            }

            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["ItemTable"];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                SSQL = "Insert into MedicalDetails(Ccode,Lcode,TransID,TransDate,GatePassID,TokenID,MachineID,Reason,MedicalDetails,Amount,DoctorName,Remarks)values(";
                SSQL = SSQL + "'" + SessionCcode + "','" + SessionLcode + "','" + txtTransID.Text + "','" + txtDate.Text + "',";
                SSQL = SSQL + "'" + ddlGPOUTNo.SelectedItem.Text + "','" + dt.Rows[i]["TokenID"].ToString() + "','" + dt.Rows[i]["MachineID"].ToString() + "','" + dt.Rows[i]["Reason"].ToString() + "',";
                SSQL = SSQL + "'" + dt.Rows[i]["MedicalDetails"].ToString() + "','" + dt.Rows[i]["Amount"].ToString() + "','" + dt.Rows[i]["DoctorName"].ToString() + "','" + dt.Rows[i]["Remarks"].ToString() + "')";
                objdata.RptEmployeeMultipleDetails(SSQL);
            }

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Medical Details Saved Successfully..!');", true);
            Clear_All_Field();
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    protected void ddlGPOUTNo_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        string query = "Select Distinct TransDate from Medical_GatePass where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        query = query + " And TransID='" + ddlGPOUTNo.SelectedItem.Text + "'";
        dt = objdata.RptEmployeeMultipleDetails(query);

        if (dt.Rows.Count != 0)
        {
            txtGPOUTDate.Value = dt.Rows[0]["TransDate"].ToString();
        }
        else
        {
            txtGPOUTDate.Value = "";
        }


        Load_Data_TokenID();
       
    }

    protected void ddlTokenNo_SelectedIndexChanged(object sender, EventArgs e)
    {

        string query;
        DataTable DT = new DataTable();
        if (ddlTokenNo.SelectedItem.Text != "-Select-")
        {
            query = "Select * from Medical_GatePass where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            query = query + " And TransID='" + ddlGPOUTNo.SelectedItem.Text + "' And TokenID='" + ddlTokenNo.SelectedItem.Text + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);

            if (DT.Rows.Count != 0)
            {
                //txtTokenNo.Text = DT.Rows[0]["TokenID"].ToString();
                txtMachineID.Text = DT.Rows[0]["MachineID"].ToString();
                txtReason.Text = DT.Rows[0]["Reason"].ToString();
            }
            else
            {
                ddlTokenNo.SelectedValue = "-Select-";
                txtMachineID.Text = "";
                txtReason.Text = "";
            }
        }
        else
        {
            ddlGPOUTNo.SelectedValue = "-Select-";
            ddlTokenNo.SelectedValue = "-Select-";
            txtMachineID.Text = "";
            txtReason.Text = "";
        }
    }

    private void Clear_All_Field()
    {
        Session.Remove("TransID");
        txtTransID.Text = "";
        txtDate.Text = "";
        ddlGPOUTNo.SelectedValue = "-Select-"; txtGPOUTDate.Value = "";
        ddlTokenNo.SelectedValue = "-Select-";
        txtMachineID.Text = ""; txtReason.Text = "";
        txtAmount.Text = ""; txtDetails.Text = "";
        txtRemarks.Text = "";
        ddlDoctorName.SelectedValue = "-Select-";

        Initial_Data_Referesh();
        Load_Data_GatePassID();
        Load_Data_TokenID();

        

        btnSave.Text = "Save";
    }

    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i]["TokenID"].ToString() == e.CommandName.ToString())
            {
                dt.Rows.RemoveAt(i);
                dt.AcceptChanges();
            }
        }
        ViewState["ItemTable"] = dt;
        Load_OLD_data();
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        DataRow dr = null;
        string query = "";

        if (!ErrFlag)
        {

            //string ValuationType = qry_dt.Rows[0]["ValuationType"].ToString();
            // check view state is not null  
            if (ViewState["ItemTable"] != null)
            {
                //get datatable from view state   
                dt = (DataTable)ViewState["ItemTable"];

                //check Item Already add or not
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["TokenID"].ToString().ToUpper() == ddlTokenNo.SelectedValue.ToString().ToUpper())
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This TokenNo Already Added..');", true);
                    }
                }
                if (!ErrFlag)
                {
                    dr = dt.NewRow();
                    dr["TokenID"] = ddlTokenNo.SelectedValue;
                    dr["MachineID"] = txtMachineID.Text;
                    dr["Reason"] = txtReason.Text;
                    dr["MedicalDetails"] = txtDetails.Text;
                    dr["Amount"] = txtAmount.Text;
                    dr["DoctorName"] = ddlDoctorName.SelectedItem.Text;
                    dr["Remarks"] = txtRemarks.Text;

                    dt.Rows.Add(dr);
                    ViewState["ItemTable"] = dt;
                    Repeater1.DataSource = dt;
                    Repeater1.DataBind();


                    ddlTokenNo.SelectedValue = "-Select-";
                    txtMachineID.Text = ""; txtReason.Text = "";
                    txtAmount.Text = ""; txtDetails.Text = "";
                    txtRemarks.Text = "";
                    ddlDoctorName.SelectedValue = "-Select-";
                }
            }
            else
            {
                dr = dt.NewRow();
                dr["TokenID"] = ddlTokenNo.SelectedValue;
                dr["MachineID"] = txtMachineID.Text;
                dr["Reason"] = txtReason.Text;
                dr["MedicalDetails"] = txtDetails.Text;
                dr["Amount"] = txtAmount.Text;
                dr["DoctorName"] = ddlDoctorName.SelectedItem.Text;
                dr["Remarks"] = txtRemarks.Text;    

                dt.Rows.Add(dr);
                ViewState["ItemTable"] = dt;
                Repeater1.DataSource = dt;
                Repeater1.DataBind();


                ddlTokenNo.SelectedValue = "-Select-";
                txtMachineID.Text = ""; txtReason.Text = "";
                txtAmount.Text = ""; txtDetails.Text = "";
                ddlDoctorName.SelectedValue = "-Select-";
                txtRemarks.Text = "";
            }
        }
    }

    private void Load_Trans_ID()
    {
        string query = "";
        DataTable DT_T = new DataTable();
        query = "Select * from MedicalDetails where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' order by TransID Desc";
        DT_T = objdata.RptEmployeeMultipleDetails(query);
        if (DT_T.Rows.Count != 0)
        {
            string Last_Trans_ID = DT_T.Rows[0]["TransID"].ToString();
            string Final_Trans_ID = (Convert.ToDecimal(Last_Trans_ID) + Convert.ToDecimal(1)).ToString();
            txtTransID.Text = Final_Trans_ID.ToString();
        }
        else
        {
            txtTransID.Text = "1";
        }
    }
}
