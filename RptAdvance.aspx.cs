﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Payroll;
using Payroll.Configuration;
using Payroll.Data;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class RptAdvance : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string Query;
    string mydate;
    string SessionPayroll;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
       
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        Load_DB();
        if (!IsPostBack)
        {

            Load_WagesCode();
           // ddlWagesType_SelectedIndexChanged(sender, e);
        }
        Load_Data();
         
    }
    public void Load_DB()
    {
        //Get Database Name
        string query = "";
        DataTable dt_DB = new DataTable();
        query = "select CMS,Rights,Sales,Stores,Weaving,Maintaince,Production,Payroll from [SVT_Rights]..MstDBname";
        dt_DB = objdata.RptEmployeeMultipleDetails(query);
        if (dt_DB.Rows.Count > 0)
        {
            SessionPayroll = dt_DB.Rows[0]["Payroll"].ToString();
            //SessionRights = dt_DB.Rows[0]["Rights"].ToString();
        }
    }
    public void Load_WagesCode()
    {
        DataTable dtdsupp = new DataTable();
        ddlWagesType.Items.Clear();
        Query = "Select EmpTypeCd,EmpType from MstEmployeeType  ";
        dtdsupp = objdata.RptEmployeeMultipleDetails(Query);
        ddlWagesType.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpType"] = "-Select-";
        dr["EmpTypeCd"] = "0";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlWagesType.DataTextField = "EmpType";
        ddlWagesType.DataValueField = "EmpTypeCd";
        ddlWagesType.DataBind();
    }
    protected void ddlWagesType_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dtdsupp = new DataTable();
        ddlMacineId.Items.Clear();
        Query = "Select convert(varchar(10), EmpNo)as EmpNo from Employee_Mst where Wages='" + ddlWagesType.SelectedItem.Text + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' And IsActive='Yes'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(Query);
        ddlMacineId.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpNo"] = "-Select-";
        dr["EmpNo"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlMacineId.DataTextField = "EmpNo";
        ddlMacineId.DataValueField = "EmpNo";
        ddlMacineId.DataBind();
    }
    private void clr()
    {
        //lblEmpName1.Text = "";

        txtDesignation.Text = "";
        txtDepartment.Text = "";
        txtDOJ.Text = "";
        DataTable dtempty = new DataTable();
        Repeater1.DataSource = dtempty;
        Repeater1.DataBind();

        Repeater1.Visible = false;
       
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
       
        string dpt = "";
        DataTable dt = new DataTable();
        clr();

        if (((txtExist.Text == "")))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Employee Number or Existing Number...!');", true);
            //System.Windows.Forms.MessageBox.Show("Enter the Employee Number or Existing Number...!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            //dt = objdata.AdvanceSalary_EmpNo(txtemp.Text);
            if (txtExist.Text != "")
            {

                Query=" Select ED.EmpNo,ED.ExistingCode  as ExisistingCode,ED.FirstName as  EmpName,ED.Designation,Convert(varchar,ED.DOJ,105) as Dateofjoining,";
                Query = Query + " ED.BaseSalary as Base,MDP.DeptName as DepartmentNm ,ED.DeptName as Department from Employee_Mst ED ";
                Query = Query + " inner join Department_Mst MDP on MDP.DeptCode=ED.DeptCode";
                Query = Query + " where ED.ExistingCode='"+txtExist.Text+"' and ED.IsActive='Yes'";
                Query = Query + " and ED.CompCode='"+SessionCcode +"' and ED.LocCode='"+ SessionLcode +"'";

                dt = objdata.RptEmployeeMultipleDetails(Query);
            }
            if (dt.Rows.Count > 0)
            {



                Repeater1.Visible = true;
             

                txtExist.Text = dt.Rows[0]["ExisistingCode"].ToString();
                ddlMacineId.SelectedValue = dt.Rows[0]["EmpNo"].ToString();
                txtEmpName.Text = dt.Rows[0]["EmpNo"].ToString();
                txtDOJ.Text = dt.Rows[0]["Dateofjoining"].ToString();

                txtDepartment.Text = dt.Rows[0]["DepartmentNm"].ToString();


                txtDesignation.Text = dt.Rows[0]["Designation"].ToString();
                DataTable dt1 = new DataTable();
                Query=" Select EmpNo,ID,Amount,Convert(varchar,TransDate,105) as CreateDate, Case Completed when 'N' then 'NO' Else 'YES' ";
                Query = Query + " End as Completed,ReductionMonth,BalanceAmount,convert(varchar,Modifieddate,105) as Modifieddate ";
                Query = Query + " from [" + SessionPayroll + "]..AdvancePayment where EmpNo='" + dt.Rows[0]["EmpNo"].ToString() + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' Order By Convert(datetime,";
                Query = Query + " TransDate,105) ";

                dt1 = objdata.RptEmployeeMultipleDetails(Query);
                Repeater1.DataSource = dt1;
                Repeater1.DataBind();

            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Employee Number Not Found....!');", true);
                //System.Windows.Forms.MessageBox.Show("Employee Number Not Found....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);

            }
        }
        
    }
    protected void ddlMacineId_SelectedIndexChanged(object sender, EventArgs e)
    {
        bool ErrFlag = false;
       
        clr();
        if (ddlMacineId.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Code....!');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Employee Code....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            Repeater1.Visible = true;
            DataTable dt = new DataTable();
            Query=" Select ED.EmpNo,ED.ExistingCode as ExisistingCode,ED.FirstName as EmpName,ED.Designation,";
            Query = Query + " Convert(varchar,ED.DOJ,105) as Dateofjoining,ED.BaseSalary as Base, MDP.DeptName as DepartmentNm ";
            Query = Query + " from Employee_Mst ED  inner join Department_Mst MDP on MDP.DeptCode=ED.DeptCode";
            Query = Query + " where ED.EmpNo='" + ddlMacineId.SelectedValue + "' and ED.IsActive ='Yes' and ED.CompCode='"+SessionCcode +"' and ED.LocCode='"+SessionLcode +"'";

            dt = objdata.RptEmployeeMultipleDetails(Query);
            if (dt.Rows.Count > 0)
            {
                
                txtExist.Text = dt.Rows[0]["ExisistingCode"].ToString();

                //txtemp.Text = dt.Rows[0]["EmpNo"].ToString();
                txtEmpName.Text = dt.Rows[0]["EmpName"].ToString();
                txtDOJ.Text = dt.Rows[0]["Dateofjoining"].ToString();
                txtDepartment.Text = dt.Rows[0]["DepartmentNm"].ToString();
                txtDesignation.Text = dt.Rows[0]["Designation"].ToString();

                DataTable dt1 = new DataTable();
                Query = " Select EmpNo,ID,Amount,Convert(varchar,TransDate,105) as CreateDate, Case Completed when 'N' then 'NO' Else 'YES' ";
                Query = Query + " End as Completed,ReductionMonth,BalanceAmount,convert(varchar,Modifieddate,105) as Modifieddate ";
                Query = Query + " from [" + SessionPayroll + "]..AdvancePayment where EmpNo='" + dt.Rows[0]["EmpNo"].ToString() + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' Order By Convert(datetime,";
                Query = Query + " TransDate,105) ";

                dt1 = objdata.RptEmployeeMultipleDetails(Query);
                Repeater1.DataSource = dt1;
                Repeater1.DataBind();

               
            }
        }
        else
        {
            Repeater1.Visible = false;
        }
    }
    public void Load_Data()
    {
        if (txtExist.Text != "")
        {
            DataTable dtDisplay = new DataTable();

            Query = " Select EmpNo,ID,Amount,Convert(varchar,TransDate,105) as CreateDate, Case Completed when 'N' then 'NO' Else 'YES' ";
            Query = Query + " End as Completed,ReductionMonth,BalanceAmount,convert(varchar,Modifieddate,105) as Modifieddate ";
            Query = Query + " from [" + SessionPayroll + "]..AdvancePayment where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' ";

            Query = Query + " and ExistNo ='" + txtExist.Text + "' ";



            Query = Query + "Order By Convert(datetime, TransDate,105) ";
            dtDisplay = objdata.RptEmployeeMultipleDetails(Query);

            //dtdDisplay = objdata.EmployeeDisplay(SessionCcode,SessionLcode);
            if (dtDisplay.Rows.Count > 0)
            {
                Repeater1.DataSource = dtDisplay;
                Repeater1.DataBind();
            }

        }
        else
        {
            DataTable dtempty = new DataTable();
            Repeater1.DataSource = dtempty;
            Repeater1.DataBind();
        }
    }
    protected void btnAll_Emp_Advance_Click(object sender, EventArgs e)
    {
        string RptName = "";
        if (ddlWagesType.SelectedItem.Text != "-Select-" && txtExist.Text== "")
        {
            RptName = "AllEmployeeAdvance";

            ResponseHelper.Redirect("AdvancePayReportDisplay.aspx?Wages=" + ddlWagesType.SelectedItem.Text + "&RptNam=" + RptName, "_blank", "");

        }
    }
    protected void GridEditEntryClick(object sender, CommandEventArgs e)
    {
        string argval = e.CommandName.ToString();
        string RptName = "";

        if (ddlWagesType.SelectedItem.Text != "-Select-" && txtExist.Text != "")
        {
            RptName = "SingleEmployeeAdvance";

            ResponseHelper.Redirect("AdvancePayReportDisplay.aspx?Wages=" + ddlWagesType.SelectedItem.Text + "&id=" + argval + "&RptNam=" + RptName + "&ExistingCode=" + txtExist.Text, "_blank", "");

        }
       
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        clr();
       txtEmpName.Text = "";
        txtExist.Text = "";
        DataTable dtempty = new DataTable();
       
        ddlMacineId.DataSource = dtempty;
        ddlMacineId.DataBind();
    }

}
