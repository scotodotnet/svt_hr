﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class MstDeptartment : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        
        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Master";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");
            Load_Department();
        }
        
        Load_Data_Dept();

        Load_Data_Designtion();
        Load_Data_WagesType();
        //Load_Data_Division();
        Load_Data_Taluk();
        Load_Data_District();
        Load_Data_State();
        //Load_Data_Level();
    }

    private void Load_Department()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlDepartment.Items.Clear();
        query = "Select *from Department_Mst";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlDepartment.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["DeptCode"] = "0";
        dr["DeptName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlDepartment.DataTextField = "DeptName";
        ddlDepartment.DataValueField = "DeptCode";
        ddlDepartment.DataBind();
    }

    private void Load_Data_Dept()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from Department_Mst";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater1.DataSource = DT;
        Repeater1.DataBind();
    }

    private void Load_Data_Designtion()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from Designation_Mst";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater2.DataSource = DT;
        Repeater2.DataBind();
    }

    private void Load_Data_WagesType()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select EmpTypeCd,EmpType,(case EmpCategory when '1' then 'STAFF' When '2' then 'LABOUR' else 'End User' end) as EmpCategory from MstEmployeeType";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater3.DataSource = DT;
        Repeater3.DataBind();
    }

    //private void Load_Data_Division()
    //{
    //    string query = "";
    //    DataTable DT = new DataTable();
    //    query = "Select * from Division_Master where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
    //    DT = objdata.RptEmployeeMultipleDetails(query);
    //    Repeater4.DataSource = DT;
    //    Repeater4.DataBind();
    //}

    private void Load_Data_Taluk()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from MstTaluk";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater5.DataSource = DT;
        Repeater5.DataBind();
    }

    private void Load_Data_District()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from MstDistrict";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater6.DataSource = DT;
        Repeater6.DataBind();
    }

    private void Load_Data_State()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from MstState";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater7.DataSource = DT;
        Repeater7.DataBind();
    }

    //private void Load_Data_Level()
    //{
    //    string query = "";
    //    DataTable DT = new DataTable();
    //    query = "Select * from LevelMaster where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
    //    DT = objdata.RptEmployeeMultipleDetails(query);
    //    Repeater8.DataSource = DT;
    //    Repeater8.DataBind();
    //}
    protected void GridEditEnquiryClick(object sender, CommandEventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from Department_Mst where DeptCode='" + e.CommandName.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            txtDeptCode.Text = DT.Rows[0]["DeptCodeSort"].ToString();
            txtDeptCodeHide.Value = DT.Rows[0]["DeptCode"].ToString();
            txtDeptName.Text = DT.Rows[0]["DeptName"].ToString();

            btnDeptSave.Text = "Update";
        }
        else
        {
            txtDeptCode.Text = "";
            txtDeptCodeHide.Value = "";
            txtDeptName.Text = "";
        }

    }

    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {
       string query = "";
        DataTable DT = new DataTable();
        query = "Select * from Department_Mst where DeptCode='" + e.CommandName.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            query = "Delete from Department_Mst where DeptCode='" + e.CommandName.ToString() + "'";
            objdata.RptEmployeeMultipleDetails(query);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Department Deleted Successfully');", true);
    
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Department Not Found');", true);
        }
        Load_Data_Dept();
        Load_Department();
    }

    protected void btnDeptSave_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        string SaveMode = "Insert";
        bool ErrFlag = false;

        if (btnDeptSave.Text == "Update")
        {
            SaveMode = "Update";
            query = "Update Department_Mst set DeptName='" + txtDeptName.Text.ToUpper() + "' where DeptCode='" + txtDeptCodeHide.Value + "' And DeptCodeSort='" + txtDeptCode.Text + "'";
            objdata.RptEmployeeMultipleDetails(query);

            query = "";
            query = "update Employee_Mst set DeptName='" + txtDeptName.Text + "' and DeptCode='" + txtDeptCode.Text + "' and CompCode='" + SessionCcode + "' and LocCOde='" + SessionLcode + "'";
            objdata.RptEmployeeMultipleDetails(query);
        }
        else if (btnDeptSave.Text == "Save")
        {
            query = "Select *from Department_Mst where DeptCodeSort='" + txtDeptCode.Text + "' And DeptName='" + txtDeptName.Text + "'";
            DT_Check = objdata.RptEmployeeMultipleDetails(query);

            if (DT_Check.Rows.Count == 0)
            {
                SaveMode = "Insert";
                query = "Insert Into Department_Mst(DeptCodeSort,DeptName,DeptCode)";
                query = query + " Values ('" + txtDeptCode.Text + "','" + txtDeptName.Text.ToUpper() + "','" + txtDeptCode.Text + "')";
                objdata.RptEmployeeMultipleDetails(query);
            }
            else
            {
                SaveMode = "Error";
            }
        }

        if (SaveMode == "Insert")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Department Saved Successfully');", true);
        }
        else if (SaveMode == "Update")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Department Updated Successfully');", true);
        }
        else if (SaveMode == "Error")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Department Details Already Exsits');", true);
        }
        Load_Data_Dept();
        Load_Department();
        Clear_All_Field();
    }

    protected void btnDeptClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    private void Clear_All_Field()
    {
        txtDeptCode.Text = "";
        txtDeptCodeHide.Value = "";
        txtDeptName.Text = "";

        btnDeptSave.Text = "Save";

        ddlDepartment.SelectedValue = "0";
        txtDesignation.Text = "";

        btnDesgnSave.Text = "Save";

        txtWagesID.Value = "";
        txxWagesType.Text = "";
        ddlCategory.SelectedValue = "-Select-";
        
        btnWagesSave.Text = "Save";

        //txtDivision.Text = "";
        //btnDivSave.Text = "Save";

        txtTaluk.Text = "";
        btnTalukSave.Text = "Save";

        txtDistrict.Text = "";
        btnDistrictSave.Text = "Save";

        txtState.Text = "";
        btnStateSave.Text = "Save";

        //txtLevel.Text = "";
        //btnLevelSave.Text = "Save";

        Load_Department();
    }

    protected void GridDeleteDesignClick(object sender, CommandEventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from Designation_Mst where DesignName='" + e.CommandName.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            query = "Delete from Designation_Mst where DesignName='" + e.CommandName.ToString() + "'";
            objdata.RptEmployeeMultipleDetails(query);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Designation Deleted Successfully');", true);

        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Designation Not Found');", true);
        }
        Load_Data_Designtion();
       
    }

    protected void btnDesgnSave_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        string SaveMode = "Insert";
        bool ErrFlag = false;

        if (btnDeptSave.Text == "Save")
        {
            query = "Select *from Designation_Mst where DeptName='" + ddlDepartment.SelectedItem.Text + "' And DesignName='" + txtDesignation.Text + "'";
            DT_Check = objdata.RptEmployeeMultipleDetails(query);

            if (DT_Check.Rows.Count == 0)
            {
                SaveMode = "Insert";
                query = "Insert Into Designation_Mst(DeptName,DesignName)";
                query = query + " Values ('" + ddlDepartment.SelectedItem.Text + "','" + txtDesignation.Text.ToUpper() + "')";
                objdata.RptEmployeeMultipleDetails(query);
            }
            else
            {
                SaveMode = "Error";
            }
        }

        if (SaveMode == "Insert")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Designation Saved Successfully');", true);
        }
        else if (SaveMode == "Error")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Designation Details Already Exsits');", true);
        }
        Load_Data_Designtion();
        Clear_All_Field();
    }

    protected void btnDesgnClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }
  
    protected void GridDeleteWagesClick(object sender, CommandEventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from MstEmployeeType where EmpType='" + e.CommandName.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            query = "Delete from MstEmployeeType where EmpType='" + e.CommandName.ToString() + "'";
            objdata.RptEmployeeMultipleDetails(query);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('WagesType Deleted Successfully');", true);

        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('WagesType Not Found');", true);
        }
        Load_Data_WagesType();
    }

    protected void btnWagesSave_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        string SaveMode = "Insert";
        bool ErrFlag = false;

        if (btnWagesSave.Text == "Update")
        {
            SaveMode = "Update";
            query = "Update MstEmployeeType set EmpType='" + txxWagesType.Text.ToUpper() + "',EmpCategory='" + ddlCategory.SelectedValue + "' where EmpTypeCd='" + txtWagesID.Value + "'";
            objdata.RptEmployeeMultipleDetails(query);
        }
        else if (btnWagesSave.Text == "Save")
        {
            query = "Select *from MstEmployeeType where EmpType='" + txxWagesType.Text.ToUpper() + "' And EmpCategory='" + ddlCategory.SelectedValue + "'";
            DT_Check = objdata.RptEmployeeMultipleDetails(query);

            if (DT_Check.Rows.Count == 0)
            {
                SaveMode = "Insert";
                query = "Insert Into MstEmployeeType(EmpType,EmpCategory)";
                query = query + " Values ('" + txxWagesType.Text.ToUpper() + "','" + ddlCategory.SelectedValue + "')";
                objdata.RptEmployeeMultipleDetails(query);
            }
            else
            {
                SaveMode = "Error";
            }
        }

        if (SaveMode == "Insert")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('WagesType Saved Successfully');", true);
        }
        else if (SaveMode == "Update")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('WagesType Updated Successfully');", true);
        }
        else if (SaveMode == "Error")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('WagesType Details Already Exsits');", true);
        }
        Load_Data_WagesType();
        Clear_All_Field();
    }

    protected void btnWagesClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    protected void GridDeleteDivisionClick(object sender, CommandEventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from Division_Master where Division='" + e.CommandName.ToString() + "'";
        query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            query = "Delete from Division_Master where Division='" + e.CommandName.ToString() + "'";
            query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            objdata.RptEmployeeMultipleDetails(query);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Division Deleted Successfully');", true);

        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Division Not Found');", true);
        }
        //Load_Data_Division();
    }

    //protected void btnDivSave_Click(object sender, EventArgs e)
    //{
    //    string query = "";
    //    DataTable DT_Check = new DataTable();
    //    string SaveMode = "Insert";
    //    bool ErrFlag = false;

    //    if (btnDivSave.Text == "Save")
    //    {
    //        query = "Select *from Division_Master where Division='" + txtDivision.Text.ToUpper() + "' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
    //        DT_Check = objdata.RptEmployeeMultipleDetails(query);

    //        if (DT_Check.Rows.Count == 0)
    //        {
    //            SaveMode = "Insert";
    //            query = "Insert Into Division_Master(CompCode,LocCode,Division)";
    //            query = query + " Values ('" + SessionCcode + "','" + SessionLcode + "','" + txtDivision.Text.ToUpper() + "')";
    //            objdata.RptEmployeeMultipleDetails(query);
    //        }
    //        else
    //        {
    //            SaveMode = "Error";
    //        }
    //    }

    //    if (SaveMode == "Insert")
    //    {
    //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Division Saved Successfully');", true);
    //    }
    //    else if (SaveMode == "Update")
    //    {
    //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Division Updated Successfully');", true);
    //    }
    //    else if (SaveMode == "Error")
    //    {
    //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Division Details Already Exsits');", true);
    //    }
    //    //Load_Data_Division();
    //    Clear_All_Field();
    //}

    protected void btnDivClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    protected void GridDeleteTalukClick(object sender, CommandEventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from MstTaluk where Taluk='" + e.CommandName.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            query = "Delete from MstTaluk where Taluk='" + e.CommandName.ToString() + "'";
            objdata.RptEmployeeMultipleDetails(query);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Taluk Deleted Successfully');", true);

        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Taluk Not Found');", true);
        }
        Load_Data_Taluk();
    }

    protected void btnTalukSave_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        string SaveMode = "Insert";
        bool ErrFlag = false;

        query = "Select *from MstTaluk where Taluk='" + txtTaluk.Text.ToUpper() + "'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);

        if (DT_Check.Rows.Count == 0)
        {
            SaveMode = "Insert";
            query = "Insert Into MstTaluk(Taluk)";
            query = query + " Values ('" + txtTaluk.Text.ToUpper() + "')";
            objdata.RptEmployeeMultipleDetails(query);
        }
        else
        {
            SaveMode = "Error";
        }

        if (SaveMode == "Insert")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Taluk Saved Successfully');", true);
        }
        else if (SaveMode == "Update")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Taluk Updated Successfully');", true);
        }
        else if (SaveMode == "Error")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Taluk Details Already Exsits');", true);
        }
        Load_Data_Taluk();
        Clear_All_Field();
    }

    protected void btnTalukClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    protected void GridDeleteDistrictClick(object sender, CommandEventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from MstDistrict where District='" + e.CommandName.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            query = "Delete from MstDistrict where District='" + e.CommandName.ToString() + "'";
            objdata.RptEmployeeMultipleDetails(query);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('District Deleted Successfully');", true);

        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('District Not Found');", true);
        }
        Load_Data_District();
    }

    protected void btnDistrictSave_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        string SaveMode = "Insert";
        bool ErrFlag = false;

        query = "Select *from MstDistrict where District='" + txtDistrict.Text.ToUpper() + "'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);

        if (DT_Check.Rows.Count == 0)
        {
            SaveMode = "Insert";
            query = "Insert Into MstDistrict(District)";
            query = query + " Values ('" + txtDistrict.Text.ToUpper() + "')";
            objdata.RptEmployeeMultipleDetails(query);
        }
        else
        {
            SaveMode = "Error";
        }

        if (SaveMode == "Insert")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('District Saved Successfully');", true);
        }
        else if (SaveMode == "Update")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('District Updated Successfully');", true);
        }
        else if (SaveMode == "Error")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('District Details Already Exsits');", true);
        }
        Load_Data_District();
        Clear_All_Field();
    }

    protected void btnDistrictClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    protected void GridDeleteStateClick(object sender, CommandEventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from MstState where State='" + e.CommandName.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            query = "Delete from MstState where State='" + e.CommandName.ToString() + "'";
            objdata.RptEmployeeMultipleDetails(query);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('State Deleted Successfully');", true);

        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('State Not Found');", true);
        }
        Load_Data_State();
    }

    protected void btnStateSave_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Check = new DataTable();
        string SaveMode = "Insert";
        bool ErrFlag = false;

        query = "Select *from MstState where State='" + txtState.Text.ToUpper() + "'";
        DT_Check = objdata.RptEmployeeMultipleDetails(query);

        if (DT_Check.Rows.Count == 0)
        {
            SaveMode = "Insert";
            query = "Insert Into MstState(State)";
            query = query + " Values ('" + txtState.Text.ToUpper() + "')";
            objdata.RptEmployeeMultipleDetails(query);
        }
        else
        {
            SaveMode = "Error";
        }
        if (SaveMode == "Insert")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('State Saved Successfully');", true);
        }
        else if (SaveMode == "Update")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('State Updated Successfully');", true);
        }
        else if (SaveMode == "Error")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('State Details Already Exsits');", true);
        }
        Load_Data_State();
        Clear_All_Field();
    }

    protected void btnStateClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    protected void GridDeleteLevelClick(object sender, CommandEventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from LevelMaster where LevelCode='" + e.CommandName.ToString() + "'";
        query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            query = "Delete from LevelMaster where LevelCode='" + e.CommandName.ToString() + "'";
            query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            objdata.RptEmployeeMultipleDetails(query);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Level Deleted Successfully');", true);

        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Level Not Found');", true);
        }
        //Load_Data_Level();
    }

    //protected void btnLevelSave_Click(object sender, EventArgs e)
    //{
    //    string query = "";
    //    DataTable DT_Check = new DataTable();
    //    string SaveMode = "Insert";
    //    bool ErrFlag = false;

    //    query = "Select *from LevelMaster where LevelCode='" + txtLevel.Text.ToUpper() + "' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
    //    DT_Check = objdata.RptEmployeeMultipleDetails(query);

    //    if (DT_Check.Rows.Count == 0)
    //    {
    //        SaveMode = "Insert";
    //        query = "Insert Into LevelMaster(CompCode,LocCode,LevelCode)";
    //        query = query + " Values ('" + SessionCcode + "','" + SessionLcode + "','" + txtLevel.Text.ToUpper() + "')";
    //        objdata.RptEmployeeMultipleDetails(query);
    //    }
    //    else
    //    {
    //        SaveMode = "Error";
    //    }

    //    if (SaveMode == "Insert")
    //    {
    //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Level Saved Successfully');", true);
    //    }
    //    else if (SaveMode == "Update")
    //    {
    //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Level Updated Successfully');", true);
    //    }
    //    else if (SaveMode == "Error")
    //    {
    //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Level Details Already Exsits');", true);
    //    }
    //    Load_Data_Level();
    //    Clear_All_Field();
    //}

    protected void btnLevelClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

}
