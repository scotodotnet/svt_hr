﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;
using System.Drawing;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using System.Runtime.InteropServices;


public partial class Form15 : System.Web.UI.Page
{
    DataTable dt_emp = new DataTable();
    string fromdate;
    string todate;
    string Wages;
    BALDataAccess objdata = new BALDataAccess();
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    DataTable AutoDTable = new DataTable();
    DataTable DataCells = new DataTable();
    string SessionUserType;
    string SSQL = "";
    string mIpAddress_IN;
    string mIpAddress_OUT;
    DataTable dsEmployee = new DataTable();
    DateTime date1 = new DateTime();
    DateTime date2 = new DateTime();
    string RegNo;

    string Address1;
    string Address2;
    string PF_DOJ_Get_str;
    DateTime PF_DOJ_Get;
    string SessionPayroll;

    string Stafflabour;

    string CBNO = "";
    string sum_pfsal;
    string sum_pf1;
    string sum_epf;
    string sum_eps;
    string Establishment;
    string sum_pf2;
    DataTable dtdnewTable = new DataTable();
    DataTable dt = new DataTable();
    string Year;
    string Year1;

    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            Load_DB();
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report-FORM 15";
            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            fromdate = Request.QueryString["FromDate"].ToString();
            todate = Request.QueryString["ToDate"].ToString();
            string TempWages = Request.QueryString["WagesType"].ToString();
            Wages = TempWages.Replace("_", "&");
            //Wages = Request.QueryString["WagesType"].ToString();
            Year1 = Request.QueryString["Year"].ToString();

            Form_15();
        }
    }
    public void Load_DB()
    {
        //Get Database Name
        string query = "";
        DataTable dt_DB = new DataTable();
        query = "select CMS,Rights,Sales,Stores,Weaving,Maintaince,Production,Payroll from [SVT_Rights]..MstDBname";
        dt_DB = objdata.RptEmployeeMultipleDetails(query);
        if (dt_DB.Rows.Count > 0)
        {
            SessionPayroll = dt_DB.Rows[0]["Payroll"].ToString();
            //SessionRights = dt_DB.Rows[0]["Rights"].ToString();
        }
    }

    public void Form_15()
    {

        //DataTable dtIPaddress = new DataTable();
        //dtIPaddress = objdata.BelowFourHours(SessionCcode.ToString(), SessionLcode.ToString());


        //if (dtIPaddress.Rows.Count > 0)
        //{
        //    for (int i = 0; i < dtIPaddress.Rows.Count; i++)
        //    {
        //        if (dtIPaddress.Rows[i]["IPMode"].ToString() == "IN")
        //        {
        //            mIpAddress_IN = dtIPaddress.Rows[i]["IPAddress"].ToString();
        //        }
        //        else if (dtIPaddress.Rows[i]["IPMode"].ToString() == "OUT")
        //        {
        //            mIpAddress_OUT = dtIPaddress.Rows[i]["IPAddress"].ToString();
        //        }
        //    }
        //}


        System.Web.UI.WebControls.DataGrid grid =
                             new System.Web.UI.WebControls.DataGrid();





        SSQL = "Select * from Location_Mst where CompCode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
        dt_emp = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt_emp.Rows.Count > 0)
        {
            RegNo = dt_emp.Rows[0]["Register_No"].ToString();
            Address1 = dt_emp.Rows[0]["Add1"].ToString();
            Address2 = dt_emp.Rows[0]["Add2"].ToString();
        }
        else
        {
            RegNo = "";
        }
        SSQL = "";

        SSQL = SSQL + "Select * from Employee_Mst where CompCode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "' And Wages='" + Wages.ToString() + "' And IsActive='Yes'";
        if (SessionUserType == "2")
        {
            SSQL = SSQL + " And Eligible_PF='1'";
        }
        SSQL = SSQL + "Order by ExistingCode Asc";

        dsEmployee = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dsEmployee.Rows.Count > 0)
        {
            int rowindex = 0;

            Microsoft.Office.Interop.Excel.Application xlApp;
            Excel.Application xlapp;
            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;
            Excel.Range chartrange;
            xlapp = new Excel.ApplicationClass();
            xlWorkBook = xlapp.Workbooks.Add(misValue);
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

            bool ErrFlag = false;
            bool download = false;


            for (int i = 0; i < dsEmployee.Rows.Count; i++)
            {
                string MachineID = dsEmployee.Rows[i]["MachineID"].ToString();

                date1 = Convert.ToDateTime(fromdate);
                date2 = Convert.ToDateTime(todate);
                int dayCount = (int)((date2 - date1).TotalDays);
                string monthname = date2.ToString("MMMM");
                //string[] Fyear = Year1.Split('-');

                //string First_Year = Fyear[0];
                //string Second_Year = Fyear[1];

                //if (monthname == "Janauary" || monthname == "February" || monthname == "March")
                //{
                //    Year = Fyear[1];
                //}
                //else
                //{
                //    Year = Fyear[0];
                //}

                Year = Year1;
                SSQL = "Select EM.EmpNo,EM.FirstName as EmpName,EM.FamilyDetails as FatherName,EM.DeptName as DepartmentNm,convert(varchar,EM.DOJ,105) as DOJ,";
                SSQL = SSQL + " Year(SD.FromDate) As Year_Str,left(SD.Month,3) + '-' + Right(Year(SD.FromDate),2) as Month_Str,";
                SSQL = SSQL + " Round(SD.BasicAndDANew,0) as Basic_Wages,SD.WorkedDays from Employee_Mst EM inner Join [" + SessionPayroll + "]..SalaryDetails SD on EM.EmpNo=SD.EmpNo";
                SSQL = SSQL + " where EM.CompCode='" + SessionCcode.ToString() + "' And EM.LocCode='" + SessionLcode.ToString() + "' And SD.Ccode='" + SessionCcode.ToString() + "' And SD.Lcode='" + SessionLcode.ToString() + "'";
                SSQL = SSQL + " And Year(SD.FromDate)='" + Year.ToString() + "' And EM.MachineID='" + MachineID + "'";

                dtdnewTable = objdata.RptEmployeeMultipleDetails(SSQL);


                //SSQL = "Select SD.EmpNo,ED.EmpName,ED.FatherName,MD.DepartmentNm,convert(varchar,OP.Dateofjoining,105) as DOJ,";
                //SSQL = SSQL + " Year(SD.FromDate) As Year_Str,left(SD.Month,3) + '-' + Right(Year(SD.FromDate),2) as Month_Str,";
                //SSQL = SSQL + " Round(SD.BasicAndDANew,0) as Basic_Wages,SD.WorkedDays from [TRK_Epay]..EmployeeDetails ED inner Join [TRK_Epay]..SalaryDetails SD on ED.EmpNo=SD.EmpNo";
                //SSQL = SSQL + " inner Join [TRK_Epay]..Officialprofile OP on ED.EmpNo=OP.EmpNo inner Join [TRK_Epay]..MstDepartment as MD on MD.DepartmentCd = ED.Department";
                //SSQL = SSQL + " where ED.Ccode='" + SessionCcode.ToString() + "' And ED.Lcode='" + SessionLcode.ToString() + "' And SD.Ccode='" + SessionCcode.ToString() + "' And SD.Lcode='" + SessionLcode.ToString() + "'";
                //SSQL = SSQL + " And Year(SD.FromDate)='" + Year.ToString() + "' And ED.BiometricID='" + MachineID + "'";

                //dtdnewTable = objdata.ReturnMultipleValue(SSQL);
                SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode.ToString() + "' ";
                dt = objdata.RptEmployeeMultipleDetails(SSQL);
                SessionCompanyName = dt.Rows[0]["CompName"].ToString();

                //CBNO = objdata.Establishment(SessionCcode, SessionLcode);
                if (dtdnewTable.Rows.Count > 0)
                {
                    if (!ErrFlag)
                    {

                        rowindex = rowindex + 1;

                        chartrange = xlWorkSheet.get_Range("A" + rowindex.ToString() + "", "R" + rowindex.ToString() + "");
                        xlWorkSheet.Cells[rowindex, 1] = "FORM No.15";
                        chartrange.Merge(true);
                        chartrange.Font.Bold = true;
                        chartrange.Font.Size = "14";
                        chartrange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                        rowindex = rowindex + 1;

                        //chartrange = xlWorkSheet.get_Range("A2", "R2");

                        chartrange = xlWorkSheet.get_Range("A" + rowindex.ToString() + "", "R" + rowindex.ToString() + "");
                        xlWorkSheet.Cells[rowindex, 1] = "(Prescribed under Rules 87 and 88)";
                        chartrange.Merge(true);
                        chartrange.Font.Bold = true;
                        chartrange.Font.Size = "14";
                        chartrange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;


                        rowindex = rowindex + 1;

                        //chartrange = xlWorkSheet.get_Range("A3", "R3");
                        chartrange = xlWorkSheet.get_Range("A" + rowindex.ToString() + "", "R" + rowindex.ToString() + "");
                        xlWorkSheet.Cells[rowindex, 1] = "Register of leave with wages";
                        chartrange.Merge(true);
                        chartrange.Font.Bold = true;
                        chartrange.Font.Size = "12";
                        chartrange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                        rowindex = rowindex + 1;

                        //chartrange = xlWorkSheet.get_Range("A4:E4", "A5:E5");

                        chartrange = xlWorkSheet.get_Range("A" + rowindex.ToString() + "", "E" + rowindex.ToString() + "");

                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        xlWorkSheet.Cells[rowindex, 1] = "Name of factory";
                        chartrange.Merge(true);
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        //chartrange.Font.Bold = true;
                        //chartrange.Font.Size = "12";
                        chartrange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                        //chartrange = xlWorkSheet.get_Range("F4", "L4");
                        chartrange = xlWorkSheet.get_Range("F" + rowindex.ToString() + "", "L" + rowindex.ToString() + "");

                        xlWorkSheet.Cells[rowindex, 7] = SessionCompanyName.ToString() + SessionLcode.ToString();
                        chartrange.Merge(true);
                        //chartrange.Font.Bold = true;
                        //chartrange.Font.Size = "12";
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        chartrange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                        //chartrange = xlWorkSheet.get_Range("M4", "O4");
                        chartrange = xlWorkSheet.get_Range("M" + rowindex.ToString() + "", "O" + rowindex.ToString() + "");
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        xlWorkSheet.Cells[rowindex, 13] = "Adult /Child";
                        chartrange.Merge(true);
                        //chartrange.Font.Bold = true;
                        //chartrange.Font.Size = "12";
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        chartrange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                        //chartrange = xlWorkSheet.get_Range("P4", "R4");
                        chartrange = xlWorkSheet.get_Range("P" + rowindex.ToString() + "", "R" + rowindex.ToString() + "");
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        xlWorkSheet.Cells[rowindex, 18] = "ADULT";
                        chartrange.Merge(true);
                        //chartrange.Font.Bold = true;
                        //chartrange.Font.Size = "12";
                        chartrange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                        rowindex = rowindex + 1;
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        //chartrange = xlWorkSheet.get_Range("F5", "L5");
                        chartrange = xlWorkSheet.get_Range("F" + rowindex.ToString() + "", "L" + rowindex.ToString() + "");

                        xlWorkSheet.Cells[rowindex, 6] = Address1.ToString();
                        chartrange.Merge(true);
                        //chartrange.Font.Bold = true;
                        //chartrange.Font.Size = "12";
                        chartrange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;


                        rowindex = rowindex + 1;

                        //chartrange = xlWorkSheet.get_Range("A6", "E6");
                        chartrange = xlWorkSheet.get_Range("A" + rowindex.ToString() + "", "E" + rowindex.ToString() + "");
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        xlWorkSheet.Cells[rowindex, 1] = "Serial No";
                        chartrange.Merge(true);
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        //chartrange.Font.Bold = true;
                        //chartrange.Font.Size = "12";
                        chartrange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                        //chartrange = xlWorkSheet.get_Range("F6", "J6");
                        chartrange = xlWorkSheet.get_Range("F" + rowindex.ToString() + "", "J" + rowindex.ToString() + "");

                        xlWorkSheet.Cells[rowindex, 10] = dsEmployee.Rows[i]["ExistingCode"].ToString();
                        chartrange.Merge(true);
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        //chartrange.Font.Bold = true;
                        //chartrange.Font.Size = "12";
                        chartrange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;


                        //chartrange = xlWorkSheet.get_Range("K6", "O6");
                        chartrange = xlWorkSheet.get_Range("K" + rowindex.ToString() + "", "O" + rowindex.ToString() + "");
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        xlWorkSheet.Cells[rowindex, 15] = "Name";
                        chartrange.Merge(true);
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        //chartrange.Font.Bold = true;
                        //chartrange.Font.Size = "12";
                        chartrange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                        //chartrange = xlWorkSheet.get_Range("P6", "R6");
                        chartrange = xlWorkSheet.get_Range("P" + rowindex.ToString() + "", "R" + rowindex.ToString() + "");

                        xlWorkSheet.Cells[rowindex, 18] = dsEmployee.Rows[i]["FirstName"].ToString();
                        chartrange.Merge(true);
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        //chartrange.Font.Bold = true;
                        //chartrange.Font.Size = "12";
                        chartrange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                        rowindex = rowindex + 1;

                        //chartrange = xlWorkSheet.get_Range("A7", "E7");
                        chartrange = xlWorkSheet.get_Range("A" + rowindex.ToString() + "", "E" + rowindex.ToString() + "");
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        xlWorkSheet.Cells[rowindex, 1] = "Department";
                        chartrange.Merge(true);
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        //chartrange.Font.Bold = true;
                        //chartrange.Font.Size = "12";
                        chartrange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        //chartrange = xlWorkSheet.get_Range("F7", "J7");
                        chartrange = xlWorkSheet.get_Range("F" + rowindex.ToString() + "", "J" + rowindex.ToString() + "");
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        xlWorkSheet.Cells[rowindex, 10] = dsEmployee.Rows[i]["DeptName"].ToString();
                        chartrange.Merge(true);
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        //chartrange.Font.Bold = true;
                        //chartrange.Font.Size = "12";
                        chartrange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                        //chartrange = xlWorkSheet.get_Range("K7", "O7");
                        chartrange = xlWorkSheet.get_Range("K" + rowindex.ToString() + "", "O" + rowindex.ToString() + "");
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        xlWorkSheet.Cells[rowindex, 15] = "Father's Name";
                        chartrange.Merge(true);
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        //chartrange.Font.Bold = true;
                        //chartrange.Font.Size = "12";
                        chartrange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        //chartrange = xlWorkSheet.get_Range("P7", "R7");
                        chartrange = xlWorkSheet.get_Range("P" + rowindex.ToString() + "", "R" + rowindex.ToString() + "");

                        xlWorkSheet.Cells[rowindex, 18] = dsEmployee.Rows[i]["LastName"].ToString();
                        chartrange.Merge(true);
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        //chartrange.Font.Bold = true;
                        //chartrange.Font.Size = "12";
                        chartrange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                        rowindex = rowindex + 1;

                        //chartrange = xlWorkSheet.get_Range("A8", "E8");
                        chartrange = xlWorkSheet.get_Range("A" + rowindex.ToString() + "", "E" + rowindex.ToString() + "");
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        xlWorkSheet.Cells[rowindex, 1] = "Serial No. in the Register of Adult/Child workers";
                        chartrange.Merge(true);
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        //chartrange.Font.Bold = true;
                        //chartrange.Font.Size = "12";
                        chartrange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                        //chartrange = xlWorkSheet.get_Range("F8", "J8");
                        chartrange = xlWorkSheet.get_Range("F" + rowindex.ToString() + "", "J" + rowindex.ToString() + "");
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        xlWorkSheet.Cells[rowindex, 10] = "";
                        chartrange.Merge(true);
                        //chartrange.Font.Bold = true;
                        //chartrange.Font.Size = "12";
                        chartrange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;


                        //chartrange = xlWorkSheet.get_Range("K8", "O8");
                        chartrange = xlWorkSheet.get_Range("K" + rowindex.ToString() + "", "O" + rowindex.ToString() + "");
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        xlWorkSheet.Cells[rowindex, 15] = "Date of discharge";
                        chartrange.Merge(true);
                        //chartrange.Font.Bold = true;
                        //chartrange.Font.Size = "12";
                        chartrange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                        //chartrange = xlWorkSheet.get_Range("P8", "R8");
                        chartrange = xlWorkSheet.get_Range("P" + rowindex.ToString() + "", "R" + rowindex.ToString() + "");
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        xlWorkSheet.Cells[rowindex, 18] = "";
                        chartrange.Merge(true);
                        //chartrange.Font.Bold = true;
                        //chartrange.Font.Size = "12";
                        chartrange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                        rowindex = rowindex + 1;
                        //chartrange = xlWorkSheet.get_Range("A9", "E9");
                        chartrange = xlWorkSheet.get_Range("A" + rowindex.ToString() + "", "E" + rowindex.ToString() + "");
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        xlWorkSheet.Cells[rowindex, 1] = "Date of entry into service";
                        chartrange.Merge(true);
                        //chartrange.Font.Bold = true;
                        //chartrange.Font.Size = "12";
                        chartrange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                        //chartrange = xlWorkSheet.get_Range("F9", "J9");
                        chartrange = xlWorkSheet.get_Range("F" + rowindex.ToString() + "", "J" + rowindex.ToString() + "");
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        xlWorkSheet.Cells[rowindex, 10] = Convert.ToDateTime(dsEmployee.Rows[i]["DOJ"].ToString()).ToString("dd/MM/yyyy");
                        chartrange.Merge(true);
                        //chartrange.Font.Bold = true;
                        //chartrange.Font.Size = "12";
                        chartrange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;


                        //chartrange = xlWorkSheet.get_Range("K9", "O9");
                        chartrange = xlWorkSheet.get_Range("K" + rowindex.ToString() + "", "O" + rowindex.ToString() + "");
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        xlWorkSheet.Cells[rowindex, 15] = "Date of amount of payment made in lieu leave due";
                        chartrange.Merge(true);
                        //chartrange.Font.Bold = true;
                        //chartrange.Font.Size = "12";
                        chartrange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                        //chartrange = xlWorkSheet.get_Range("P9", "R9");
                        chartrange = xlWorkSheet.get_Range("P" + rowindex.ToString() + "", "R" + rowindex.ToString() + "");
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        xlWorkSheet.Cells[rowindex, 18] = "";
                        chartrange.Merge(true);
                        //chartrange.Font.Bold = true;
                        //chartrange.Font.Size = "12";
                        chartrange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;


                        rowindex = rowindex + 1;

                        //chartrange = xlWorkSheet.get_Range("A10", "A10");
                        chartrange = xlWorkSheet.get_Range("A" + rowindex.ToString() + "", "A" + rowindex.ToString() + "");
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        xlWorkSheet.Cells[rowindex, 1] = "Calender Year of Service";
                        chartrange.Merge(true);
                        chartrange.Orientation = 90;
                        //chartrange.Font.Bold = true;
                        //chartrange.Font.Size = "12";
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                        //chartrange = xlWorkSheet.get_Range("B10", "B10");
                        chartrange = xlWorkSheet.get_Range("B" + rowindex.ToString() + "", "B" + rowindex.ToString() + "");

                        xlWorkSheet.Cells[rowindex, 2] = "Wage period from to";
                        chartrange.Merge(true);
                        chartrange.Orientation = 90;
                        //chartrange.Font.Bold = true;
                        //chartrange.Font.Size = "12";
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                        //chartrange = xlWorkSheet.get_Range("C10", "C10");
                        chartrange = xlWorkSheet.get_Range("C" + rowindex.ToString() + "", "C" + rowindex.ToString() + "");

                        xlWorkSheet.Cells[rowindex, 3] = "Wage earned during the wage period";
                        chartrange.WrapText = true;
                        chartrange.Orientation = 90;
                        //chartrange.Font.Bold = true;
                        //chartrange.Font.Size = "12";
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;


                        //chartrange = xlWorkSheet.get_Range("D10", "D10");
                        chartrange = xlWorkSheet.get_Range("D" + rowindex.ToString() + "", "D" + rowindex.ToString() + "");
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                        xlWorkSheet.Cells[rowindex, 4] = "No of days work performed";
                        chartrange.WrapText = true;
                        chartrange.Orientation = 90;
                        //chartrange.Font.Bold = true;
                        //chartrange.Font.Size = "12";
                        //chartrange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                        //chartrange.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

                        //chartrange.Font.Bold = true;
                        //chartrange.Font.Size = "12";
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;


                        //chartrange = xlWorkSheet.get_Range("E10", "E10");
                        chartrange = xlWorkSheet.get_Range("E" + rowindex.ToString() + "", "E" + rowindex.ToString() + "");
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                        xlWorkSheet.Cells[rowindex, 5] = "No of days of lay-off";
                        chartrange.WrapText = true;
                        chartrange.Orientation = 90;
                        //chartrange.Font.Bold = true;
                        //chartrange.Font.Size = "12";
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;


                        //chartrange = xlWorkSheet.get_Range("F10", "F10");
                        chartrange = xlWorkSheet.get_Range("F" + rowindex.ToString() + "", "F" + rowindex.ToString() + "");

                        xlWorkSheet.Cells[rowindex, 6] = "No of days of maernity leave";
                        chartrange.WrapText = true;
                        chartrange.Orientation = 90;
                        //chartrange.Font.Bold = true;
                        //chartrange.Font.Size = "12";
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;


                        //chartrange = xlWorkSheet.get_Range("G10", "G10");
                        chartrange = xlWorkSheet.get_Range("G" + rowindex.ToString() + "", "G" + rowindex.ToString() + "");

                        xlWorkSheet.Cells[rowindex, 7] = "No of days of leave enjoyed";
                        chartrange.WrapText = true;
                        chartrange.Orientation = 90;
                        //chartrange.Font.Bold = true;
                        //chartrange.Font.Size = "12";
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;


                        //chartrange = xlWorkSheet.get_Range("H10", "H10");
                        chartrange = xlWorkSheet.get_Range("H" + rowindex.ToString() + "", "H" + rowindex.ToString() + "");

                        xlWorkSheet.Cells[rowindex, 8] = "Total of columns   (4) - (7)";
                        chartrange.WrapText = true;
                        chartrange.Orientation = 90;
                        //chartrange.Font.Bold = true;
                        //chartrange.Font.Size = "12";
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;


                        //chartrange = xlWorkSheet.get_Range("I10", "I10");
                        chartrange = xlWorkSheet.get_Range("I" + rowindex.ToString() + "", "I" + rowindex.ToString() + "");

                        xlWorkSheet.Cells[rowindex, 9] = "Balance of leave from preceingyear";
                        chartrange.WrapText = true;
                        chartrange.Orientation = 90;
                        //chartrange.Font.Bold = true;
                        //chartrange.Font.Size = "12";
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                        //chartrange = xlWorkSheet.get_Range("J10", "j10");
                        chartrange = xlWorkSheet.get_Range("J" + rowindex.ToString() + "", "J" + rowindex.ToString() + "");

                        xlWorkSheet.Cells[rowindex, 10] = "Leave earned during the year mentioned in Col(1)";
                        chartrange.WrapText = true;
                        chartrange.Orientation = 90;
                        //chartrange.Font.Bold = true;
                        //chartrange.Font.Size = "12";
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;


                        //chartrange = xlWorkSheet.get_Range("K10", "K10");
                        chartrange = xlWorkSheet.get_Range("K" + rowindex.ToString() + "", "K" + rowindex.ToString() + "");

                        xlWorkSheet.Cells[rowindex, 11] = "Total of columns (9) and (10)";
                        chartrange.WrapText = true;
                        chartrange.Orientation = 90;
                        //chartrange.Font.Bold = true;
                        //chartrange.Font.Size = "12";
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;


                        //chartrange = xlWorkSheet.get_Range("L10", "L10");
                        chartrange = xlWorkSheet.get_Range("L" + rowindex.ToString() + "", "L" + rowindex.ToString() + "");

                        xlWorkSheet.Cells[rowindex, 12] = "Whether leave in accordance with scheme under Sec.79(8)was refuesed";
                        chartrange.WrapText = true;
                        chartrange.Orientation = 90;
                        //chartrange.Font.Bold = true;
                        //chartrange.Font.Size = "12";
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                        //chartrange = xlWorkSheet.get_Range("M10", "M10");
                        chartrange = xlWorkSheet.get_Range("M" + rowindex.ToString() + "", "M" + rowindex.ToString() + "");

                        xlWorkSheet.Cells[rowindex, 13] = "Leave enjoyed from to";
                        chartrange.WrapText = true;
                        chartrange.Orientation = 90;
                        //chartrange.Font.Bold = true;
                        //chartrange.Font.Size = "12";
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;


                        //chartrange = xlWorkSheet.get_Range("N10", "N10");
                        chartrange = xlWorkSheet.get_Range("N" + rowindex.ToString() + "", "N" + rowindex.ToString() + "");

                        xlWorkSheet.Cells[rowindex, 14] = "Balance of leave to credit";
                        chartrange.WrapText = true;
                        chartrange.Orientation = 90;
                        //chartrange.Font.Bold = true;
                        //chartrange.Font.Size = "12";
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                        //chartrange = xlWorkSheet.get_Range("O10", "O10");
                        chartrange = xlWorkSheet.get_Range("O" + rowindex.ToString() + "", "O" + rowindex.ToString() + "");

                        xlWorkSheet.Cells[rowindex, 15] = "Normal rate of wages";
                        chartrange.WrapText = true;
                        chartrange.Orientation = 90;
                        //chartrange.Font.Bold = true;
                        //chartrange.Font.Size = "12";
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                        //chartrange = xlWorkSheet.get_Range("P10", "P10");
                        chartrange = xlWorkSheet.get_Range("P" + rowindex.ToString() + "", "P" + rowindex.ToString() + "");

                        xlWorkSheet.Cells[rowindex, 16] = "Cash equivalent of advantage accruing through consessional sale of foodgrains and other articles";
                        chartrange.WrapText = true;
                        chartrange.Orientation = 90;
                        //chartrange.Font.Bold = true;
                        //chartrange.Font.Size = "12";
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                        //chartrange = xlWorkSheet.get_Range("Q10", "Q10");
                        chartrange = xlWorkSheet.get_Range("Q" + rowindex.ToString() + "", "Q" + rowindex.ToString() + "");

                        xlWorkSheet.Cells[rowindex, 17] = "Rate of wages for the leave period total of col(15) and (16)";
                        chartrange.WrapText = true;
                        chartrange.Orientation = 90;
                        //chartrange.Font.Bold = true;
                        //chartrange.Font.Size = "12";
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                        //chartrange = xlWorkSheet.get_Range("R10", "R10");
                        chartrange = xlWorkSheet.get_Range("R" + rowindex.ToString() + "", "R" + rowindex.ToString() + "");

                        xlWorkSheet.Cells[rowindex, 18] = "Remarks";
                        chartrange.WrapText = true;
                        chartrange.Orientation = 90;
                        //chartrange.Font.Bold = true;
                        //chartrange.Font.Size = "12";
                        chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                        ////chartrange = xlWorkSheet.get_Range("S10", "S10");
                        //chartrange = xlWorkSheet.get_Range("S" + rowindex.ToString() + "", "S" + rowindex.ToString() + "");

                        //xlWorkSheet.Cells[10, 19] = "";
                        //chartrange.WrapText = true;
                        //chartrange.Orientation = 90;
                        ////chartrange.Font.Bold = true;
                        ////chartrange.Font.Size = "12";
                        //chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                        ////chartrange = xlWorkSheet.get_Range("T10", "T10");
                        //chartrange = xlWorkSheet.get_Range("T" + rowindex.ToString() + "", "T" + rowindex.ToString() + "");

                        //xlWorkSheet.Cells[10, 20] = "";
                        //chartrange.WrapText = true;
                        //chartrange.Orientation = 90;
                        ////chartrange.Font.Bold = true;
                        ////chartrange.Font.Size = "12";
                        //chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                        if (dtdnewTable.Rows.Count > 0)
                        {
                            for (int j = 0; j < dtdnewTable.Rows.Count; j++)
                            {
                                rowindex = rowindex + 1;

                                //chartrange = xlWorkSheet.get_Range("A11", "A11");
                                chartrange = xlWorkSheet.get_Range("A" + rowindex.ToString() + "", "A" + rowindex.ToString() + "");

                                xlWorkSheet.Cells[rowindex, 1] = Year.ToString();
                                chartrange.WrapText = true;
                                //chartrange.Orientation = 90;
                                //chartrange.Font.Bold = true;
                                //chartrange.Font.Size = "12";
                                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                                //chartrange = xlWorkSheet.get_Range("B11", "B11");
                                chartrange = xlWorkSheet.get_Range("B" + rowindex.ToString() + "", "B" + rowindex.ToString() + "");

                                xlWorkSheet.Cells[rowindex, 2] = dtdnewTable.Rows[j]["Month_Str"].ToString();
                                chartrange.WrapText = true;
                                //chartrange.Orientation = 90;
                                //chartrange.Font.Bold = true;
                                //chartrange.Font.Size = "12";
                                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;


                                //chartrange = xlWorkSheet.get_Range("C11", "C11");
                                chartrange = xlWorkSheet.get_Range("C" + rowindex.ToString() + "", "C" + rowindex.ToString() + "");

                                xlWorkSheet.Cells[rowindex, 3] = dtdnewTable.Rows[j]["Basic_Wages"].ToString();
                                chartrange.WrapText = true;
                                //chartrange.Orientation = 90;
                                //chartrange.Font.Bold = true;
                                //chartrange.Font.Size = "12";
                                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;


                                //chartrange = xlWorkSheet.get_Range("D11", "D11");
                                chartrange = xlWorkSheet.get_Range("D" + rowindex.ToString() + "", "D" + rowindex.ToString() + "");

                                xlWorkSheet.Cells[rowindex, 4] = dtdnewTable.Rows[j]["WorkedDays"].ToString();
                                chartrange.WrapText = true;
                                //chartrange.Orientation = 90;
                                //chartrange.Font.Bold = true;
                                //chartrange.Font.Size = "12";
                                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;


                                //chartrange = xlWorkSheet.get_Range("E11", "E11");
                                chartrange = xlWorkSheet.get_Range("E" + rowindex.ToString() + "", "E" + rowindex.ToString() + "");

                                xlWorkSheet.Cells[rowindex, 5] = "0";
                                chartrange.WrapText = true;
                                //chartrange.Orientation = 90;
                                //chartrange.Font.Bold = true;
                                //chartrange.Font.Size = "12";
                                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                                //chartrange = xlWorkSheet.get_Range("F11", "F11");
                                chartrange = xlWorkSheet.get_Range("F" + rowindex.ToString() + "", "F" + rowindex.ToString() + "");

                                xlWorkSheet.Cells[rowindex, 6] = "0";
                                chartrange.WrapText = true;
                                //chartrange.Orientation = 90;
                                //chartrange.Font.Bold = true;
                                //chartrange.Font.Size = "12";
                                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;


                                //chartrange = xlWorkSheet.get_Range("G11", "G11");
                                chartrange = xlWorkSheet.get_Range("G" + rowindex.ToString() + "", "G" + rowindex.ToString() + "");

                                xlWorkSheet.Cells[rowindex, 7] = "0";
                                chartrange.WrapText = true;
                                //chartrange.Orientation = 90;
                                //chartrange.Font.Bold = true;
                                //chartrange.Font.Size = "12";
                                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                                //chartrange = xlWorkSheet.get_Range("H11", "H11");
                                chartrange = xlWorkSheet.get_Range("H" + rowindex.ToString() + "", "H" + rowindex.ToString() + "");

                                xlWorkSheet.Cells[rowindex, 8] = dtdnewTable.Rows[j]["WorkedDays"].ToString();
                                chartrange.WrapText = true;
                                //chartrange.Orientation = 90;
                                //chartrange.Font.Bold = true;
                                //chartrange.Font.Size = "12";
                                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                                //chartrange = xlWorkSheet.get_Range("I11", "I11");
                                chartrange = xlWorkSheet.get_Range("I" + rowindex.ToString() + "", "I" + rowindex.ToString() + "");

                                xlWorkSheet.Cells[rowindex, 9] = "";
                                chartrange.WrapText = true;
                                //chartrange.Orientation = 90;
                                //chartrange.Font.Bold = true;
                                //chartrange.Font.Size = "12";
                                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                                //chartrange = xlWorkSheet.get_Range("J11", "J11");
                                chartrange = xlWorkSheet.get_Range("J" + rowindex.ToString() + "", "J" + rowindex.ToString() + "");

                                xlWorkSheet.Cells[rowindex, 10] = "";
                                chartrange.WrapText = true;
                                //chartrange.Orientation = 90;
                                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                                //chartrange = xlWorkSheet.get_Range("K11", "K11");
                                chartrange = xlWorkSheet.get_Range("K" + rowindex.ToString() + "", "K" + rowindex.ToString() + "");

                                xlWorkSheet.Cells[rowindex, 11] = "";
                                chartrange.WrapText = true;
                                //chartrange.Orientation = 90;
                                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                                //chartrange = xlWorkSheet.get_Range("L11", "L11");
                                chartrange = xlWorkSheet.get_Range("L" + rowindex.ToString() + "", "L" + rowindex.ToString() + "");

                                xlWorkSheet.Cells[rowindex, 12] = "";
                                chartrange.WrapText = true;
                                //chartrange.Orientation = 90;
                                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;


                                //chartrange = xlWorkSheet.get_Range("M11", "M11");
                                chartrange = xlWorkSheet.get_Range("M" + rowindex.ToString() + "", "M" + rowindex.ToString() + "");

                                xlWorkSheet.Cells[rowindex, 13] = "";
                                chartrange.WrapText = true;
                                //chartrange.Orientation = 90;
                                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                                //chartrange = xlWorkSheet.get_Range("N11", "N11");
                                chartrange = xlWorkSheet.get_Range("N" + rowindex.ToString() + "", "N" + rowindex.ToString() + "");

                                xlWorkSheet.Cells[rowindex, 14] = "";
                                chartrange.WrapText = true;
                                chartrange.Orientation = 90;
                                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                                //chartrange = xlWorkSheet.get_Range("O11", "O11");
                                chartrange = xlWorkSheet.get_Range("O" + rowindex.ToString() + "", "O" + rowindex.ToString() + "");

                                xlWorkSheet.Cells[rowindex, 15] = "";
                                chartrange.WrapText = true;
                                //chartrange.Orientation = 90;
                                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                                //chartrange = xlWorkSheet.get_Range("P11", "P11");
                                chartrange = xlWorkSheet.get_Range("P" + rowindex.ToString() + "", "P" + rowindex.ToString() + "");

                                xlWorkSheet.Cells[rowindex, 16] = "";
                                chartrange.WrapText = true;
                                chartrange.Orientation = 90;
                                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;


                                //chartrange = xlWorkSheet.get_Range("Q11", "Q11");
                                chartrange = xlWorkSheet.get_Range("Q" + rowindex.ToString() + "", "Q" + rowindex.ToString() + "");

                                xlWorkSheet.Cells[rowindex, 17] = "";
                                chartrange.WrapText = true;
                                //chartrange.Orientation = 90;
                                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                                //chartrange = xlWorkSheet.get_Range("R11", "R11");
                                chartrange = xlWorkSheet.get_Range("R" + rowindex.ToString() + "", "R" + rowindex.ToString() + "");

                                xlWorkSheet.Cells[rowindex, 18] = "";
                                chartrange.WrapText = true;
                                //chartrange.Orientation = 90;
                                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                                //chartrange = xlWorkSheet.get_Range("S11", "S11");
                                chartrange = xlWorkSheet.get_Range("S" + rowindex.ToString() + "", "S" + rowindex.ToString() + "");

                                xlWorkSheet.Cells[rowindex, 19] = "";
                                chartrange.WrapText = true;
                                //chartrange.Orientation = 90;
                                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;


                                //chartrange = xlWorkSheet.get_Range("T11", "T11");
                                chartrange = xlWorkSheet.get_Range("T" + rowindex.ToString() + "", "T" + rowindex.ToString() + "");

                                xlWorkSheet.Cells[rowindex, 20] = "";
                                chartrange.WrapText = true;
                                //chartrange.Orientation = 90;
                                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                                //chartrange = xlWorkSheet.get_Range("U11", "U11");
                                chartrange = xlWorkSheet.get_Range("U" + rowindex.ToString() + "", "U" + rowindex.ToString() + "");

                                xlWorkSheet.Cells[rowindex, 21] = "";
                                chartrange.WrapText = true;
                                //chartrange.Orientation = 90;
                                chartrange.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                            }
                        }


                    }
                }


            }
            //string newpath = Server.MapPath("C:");
            string fileName = Server.MapPath("~/FormNo15.xls");
            xlapp.DisplayAlerts = false; //Supress overwrite request
            xlWorkBook.SaveAs(fileName, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
            xlWorkBook.Close(true, misValue, misValue);
            xlapp.Quit();

            //Release objects
            releaseObject(xlWorkSheet);
            releaseObject(xlWorkBook);
            releaseObject(xlapp);

            //Give the user the option to save the copy of the file anywhere they desire
            String FilePath = Server.MapPath("~/FormNo15.xls");
            Response.ContentType = ContentType;
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + Path.GetFileName(FilePath));
            Response.WriteFile(FilePath);
            Response.End();


            //System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
            //response.ClearContent();
            //response.Clear();
            //response.ContentType = "text/plain";
            //response.AddHeader("Content-Disposition", "attachment; filename=FormNo15.xls;");
            //response.TransmitFile(FilePath);
            //response.Flush();
            //response.Close();




            //Delete the temporary file
            DeleteFile(fileName);
            download = true;

            Marshal.ReleaseComObject(xlapp);
            Marshal.ReleaseComObject(xlWorkBook);
            Marshal.ReleaseComObject(xlWorkSheet);

            if (download == true)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully in Documents in the name of Form15.xls');", true);
            }

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No records matched');", true);
        }

    }

    private void DeleteFile(string fileName)
    {
        if (File.Exists(fileName))
        {
            try
            {
                File.Delete(fileName);
            }
            catch (Exception ex)
            {
                //Could not delete the file, wait and try again
                try
                {
                    System.GC.Collect();
                    System.GC.WaitForPendingFinalizers();
                    File.Delete(fileName);
                }
                catch
                {
                    //Could not delete the file still
                }
            }
        }
    }

    private void releaseObject(object obj)
    {
        try
        {
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
            obj = null;
        }
        catch (Exception ex)
        {
            obj = null;
            Response.Write("Exception Occured while releasing object " + ex.ToString());
        }
        finally
        {
            GC.Collect();
        }
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        CrystalReportViewer1.Dispose();
    }

}







