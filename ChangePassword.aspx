﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="ChangePassword.aspx.cs" Inherits="ChangePassword" Title="" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.select2').select2();
                $('#example').dataTable();
            }
        });
    };
</script>


<asp:UpdatePanel>
<ContentTemplate>
<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Master</a></li>
				<li class="active">Change Password</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Change Password</h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">Change Password</h4>
                        </div>
                        <div class="panel-body">
                        <!-- begin row -->
                          <div class="row">
                           <!-- begin col-4 -->
                              <div class="form-group col-md-3">
					           <label for="exampleInputName">New Password<span class="mandatory">*</span></label>
                               <asp:TextBox ID="txtNewPassword" class="form-control" runat="server" TextMode="Password"></asp:TextBox>
	                           <asp:RequiredFieldValidator ControlToValidate="txtNewPassword" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                               </asp:RequiredFieldValidator>
	                               </div>
                              <!-- end col-4 -->
                              <!-- begin col-4 -->
                              <div class="form-group col-md-4">
						                 <label for="exampleInputName">Conform Password<span class="mandatory">*</span></label>
		                                 <asp:TextBox ID="txtConformPassword" class="form-control" runat="server" TextMode="Password"></asp:TextBox>
		                                 <asp:RequiredFieldValidator ControlToValidate="txtConformPassword" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                          </asp:RequiredFieldValidator>
					                </div>
                              <!-- end col-4 -->
                              
                            
                          
                              </div>
                        <!-- end row -->
                      
                    <div class="col-md-12">
                        <div class="row">
                            <div class="txtcenter">
                            <asp:Button ID="btnSave" class="btn btn-success"  runat="server" Text="Save" 
                               ValidationGroup="Validate_Field" onclick="btnSave_Click" />
                               <asp:Button ID="btnCancel" class="btn btn-danger" runat="server" Text="Cancel" 
                                 onclick="btnCancel_Click" />   </div>
                        </div>
                    </div>                    
                    <!-- Button end -->       
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
       </div>
<!-- end #content -->
</ContentTemplate>
</asp:UpdatePanel>



</asp:Content>
