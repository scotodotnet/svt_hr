﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Globalization;


public partial class LunchReportBWDates : System.Web.UI.Page
{

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string Division;
    string Status;
    double Final_Count;
    string TokenNo;

    double Present_Count;
    double Absent_Count;
    decimal Fixed_Work_Days;
    decimal Month_WH_Count;
    double Present_WH_Count;
    double NFH_Days_Count;
    double NFH_Days_Present_Count;

    BALDataAccess objdata = new BALDataAccess();

    string SSQL = "";
    DataTable AutoDTable = new DataTable();
    DataTable mEmployeeDT = new DataTable();
    DataTable MEmployeeDS = new DataTable();
    DateTime date1;
    DateTime Date2 = new DateTime();
    int intK;
    string FromDate;
    string ToDate;
    string Date_Value_Str;
    string Date_value_str1;
    string[] Time_Minus_Value_Check;
    string WagesType;
    double leaveCount;

    DataTable Lunchin_dt = new DataTable();
    DataTable Lunchout_dt = new DataTable();
    DataTable dt = new DataTable();
    DataTable DataCell = new DataTable();
    string Time_IN_Str;
    string Time_OUT_Str;
    int time_Check_dbl;
    string Total_Time_get;
  
    System.Web.UI.WebControls.DataGrid grid =
                            new System.Web.UI.WebControls.DataGrid();
    public string Left_Val(string Value, int Length)
    {
        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {

            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report- Lunch Report Between Dates";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("ManualEntry"));
                //li.Attributes.Add("class", "droplink active open");
            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            //SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            Status = Request.QueryString["Status"].ToString();
            Division = Request.QueryString["Division"].ToString();
            string TempWages = Request.QueryString["Wages"].ToString();
            WagesType = TempWages.Replace("_", "&");
            //WagesType = Request.QueryString["Wages"].ToString();
            TokenNo = Request.QueryString["TokenNo"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            //if (SessionUserType == "2")
            //{
            //    NonAdminBetweenDates();
            //}

            //else
            //{

            string TableName = "";

            if (Status == "Pending")
            {
                TableName = "Employee_Mst_New_Emp";
            }

            else
            {
                TableName = "Employee_Mst";
            }

           
            DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();
            DataTable mLocalDS = new DataTable();

            AutoDTable.Columns.Add("SNo");
            AutoDTable.Columns.Add("DeptName");
            AutoDTable.Columns.Add("Designation");
            AutoDTable.Columns.Add("MachineID");
            AutoDTable.Columns.Add("ExistingCode");
            AutoDTable.Columns.Add("FirstName");
            AutoDTable.Columns.Add("LunchHours");
            //AutoDTable.Columns.Add("Category");
            //AutoDTable.Columns.Add("Source");
            //AutoDTable.Columns.Add("Grade");

            date1 = Convert.ToDateTime(FromDate);
            string dat = ToDate;
            Date2 = Convert.ToDateTime(dat);

            int daycount = (int)((Date2 - date1).TotalDays);
            int daysAdded = 0;

            while (daycount >= 0)
            {
                DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded).ToShortDateString());
                //string day1=string.Format("{MM/dd/yyyy}",date1.AddDays(daysAdded));
                AutoDTable.Columns.Add(Convert.ToString(dayy.ToShortDateString()));

                daycount -= 1;
                daysAdded += 1;
            }

            AutoDTable.Columns.Add("Late Days");
            AutoDTable.Columns.Add("Exact Days");
            AutoDTable.Columns.Add("Improper Punch");
            AutoDTable.Columns.Add("Total Days");
            SSQL = "select MachineID,isnull(FirstName,'') as FirstName,ExistingCode,DeptName,Designation,MachineID_Encrypt";
            SSQL = SSQL + "  from " + TableName + " ";
            //   SSQL = SSQL + " inner join Department_Mst DM on EM.DeptName = DM.DeptCode";
            //SSQL = SSQL + " inner join MstCategory MC on EM.WageCategoty = MC.CateID ";
            //SSQL = SSQL + " inner join AgentMst AM on EM.BrokerName = AM.AgentID ";
            //SSQL = SSQL + " inner join MstGrade MG on EM.Grade = MG.GradeID ";
            SSQL = SSQL + " Where CompCode='" + Session["Ccode"].ToString() + "' And LocCode='" + Session["Lcode"].ToString() + "' and IsActive='Yes' ";
            // SSQL = SSQL + " And DM.CompCode='" + Session["Ccode"].ToString() + "' And DM.LocCode='" + Session["Lcode"].ToString() + "'";
            //  SSQL = SSQL + " And AM.CompCode='" + Session["Ccode"].ToString() + "' And AM.LocCode='" + Session["Lcode"].ToString() + "'";
            // SSQL = SSQL + " And MG.CompCode='" + Session["Ccode"].ToString() + "' And MG.LocCode='" + Session["Lcode"].ToString() + "'";
            if (TokenNo != "")
            {
                SSQL = SSQL + " And ExistingCode='" + TokenNo + "'";
            }

            if (WagesType != "-Select-")
            {
                SSQL = SSQL + " And Wages='" + WagesType + "'";
            }
            if (Division != "-Select-")
            {
                SSQL = SSQL + " And Division = '" + Division + "'";
            }

            SSQL = SSQL + " Group by MachineID,FirstName,ExistingCode,DeptName,Designation,MachineID_Encrypt order by ExistingCode";
            dt = objdata.RptEmployeeMultipleDetails(SSQL);



            intK = 0;

            Present_WH_Count = 0;

            int SNo = 1;

            for (int i = 0; i < dt.Rows.Count; i++)
            {

                AutoDTable.NewRow();
                AutoDTable.Rows.Add();

                string MachineID = dt.Rows[i]["MachineID"].ToString();

                AutoDTable.Rows[intK]["SNo"] = SNo;
                AutoDTable.Rows[intK]["DeptName"] = dt.Rows[i]["DeptName"].ToString();
                AutoDTable.Rows[intK]["Designation"] = dt.Rows[i]["Designation"].ToString();
                AutoDTable.Rows[intK]["MachineID"] = dt.Rows[i]["MachineID"].ToString();
                AutoDTable.Rows[intK]["ExistingCode"] = dt.Rows[i]["ExistingCode"].ToString();
                AutoDTable.Rows[intK]["FirstName"] = dt.Rows[i]["FirstName"].ToString();
                
                string DOJ_Date_Str = "";
              
               
                string OTEllible = "";
              

                int count = 7;
                decimal count1 = 0;
                decimal count2 = 0;
                decimal count3 = 0;
                decimal count4 = 0;

                if (dt.Rows[i]["MachineID"].ToString() == "142")
                {
                    string Val;
                    Val = "1";
                }
                if (dt.Rows[i]["MachineID"].ToString() == "7406")
                {
                    string Val;
                    Val = "1";
                }
                string machineencrypt = "";
                machineencrypt = dt.Rows[i]["MachineID_Encrypt"].ToString();
                DateTime Query_Date_Check = new DateTime();
                DateTime DOJ_Date_Check_Emp = new DateTime();


                for (int j = 0; j < daysAdded; j++)
                {
                    DateTime dayy = Convert.ToDateTime(date1.AddDays(j).ToShortDateString());

                    string Date1 = dayy.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);

                    Date_Value_Str = dayy.ToString("yyyy/MM/dd", CultureInfo.InvariantCulture);

                    DateTime date2 = Convert.ToDateTime(Date1);
                  



                    Time_IN_Str = "";
                    Time_OUT_Str = "";
                    //SSQL = "Select TimeOUT from  LogTimeLunch_OUT where MachineID= '" + machineencrypt + "' and ";
                    //SSQL = SSQL + " CompCode='" + Session["Ccode"].ToString() + "' and LocCode='" + Session["Lcode"].ToString() + "' ";

                    SSQL = "Select TimeOUT from LogTimeLunch_OUT where MachineID='" + machineencrypt + "'";
                    SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                    SSQL = SSQL + " And TimeOUT >='" + date2.AddDays(0).ToString("yyyy/MM/dd") + " " + "07:00' And TimeOUT <='" + date2.AddDays(1).ToString("yyyy/MM/dd") + " " + "07:00' Order by TimeOUT ASC";

                    Lunchout_dt = objdata.RptEmployeeMultipleDetails(SSQL);
                    //if (Lunchout_dt.Rows.Count != 0)
                    //{

                    //    if (Lunchout_dt.Rows[0]["TimeOUT"] == "" || Lunchout_dt.Rows[0]["TimeOUT"] == "00:00")
                    //    {
                    //        DataCell.Rows[i]["LOUT"] = "";

                    //    }
                    //    else
                    //    {

                    //        DataCell.Rows[i]["LOUT"] = String.Format("{0:hh:mm tt}", Lunchout_dt.Rows[0]["TimeOUT"]);
                    //    }
                    //}
                    //else
                    //{
                    //    DataCell.Rows[i]["LOUT"] = "";

                    //}
                    SSQL = "Select TimeIN from LogTimeLunch_IN where MachineID='" + machineencrypt + "'";
                    SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                    SSQL = SSQL + " and TimeIN >='" + date2.AddDays(0).ToString("yyyy/MM/dd") + " " + "07:00" + "'";
                    SSQL = SSQL + " and TimeIN <='" + date2.AddDays(1).ToString("yyyy/MM/dd") + " " + "07:00" + "' Order by TimeIN desc";


                    //SSQL = "Select TimeIN from  LogTimeLunch_IN where MachineID= '" + machineencrypt + "' and ";
                    //SSQL = SSQL + " CompCode='" + Session["Ccode"].ToString() + "' and LocCode='" + Session["Lcode"].ToString() + "' ";
                    Lunchin_dt = objdata.RptEmployeeMultipleDetails(SSQL);

                    //if (Lunchin_dt.Rows.Count != 0)
                    //{
                    //    if (Lunchin_dt.Rows[0]["TimeIN"] == "" || Lunchin_dt.Rows[0]["TimeIN"] == "00:00")
                    //    {
                    //        DataCell.Rows[i]["LIN"] = "";


                    //    }
                    //    else
                    //    {

                    //        DataCell.Rows[i]["LIN"] = String.Format("{0:hh:mm tt}", Lunchin_dt.Rows[0]["TimeIN"]);
                    //    }

                    //}
                    //else
                    //{
                    //    DataCell.Rows[i]["LIN"] = "";
                    //}




                    //Lunch Calculation

                    String Emp_Total_Work_Time_1 = "00:00";
                    if (Lunchout_dt.Rows.Count > 1)
                    {
                        for (int tin = 0; tin < Lunchout_dt.Rows.Count; tin++)
                        {
                            Time_IN_Str = Lunchout_dt.Rows[tin][0].ToString();

                            if (Lunchin_dt.Rows.Count > tin)
                            {
                                Time_OUT_Str = Lunchin_dt.Rows[tin][0].ToString();
                            }
                            else if (Lunchin_dt.Rows.Count > Lunchout_dt.Rows.Count)
                            {
                                Time_OUT_Str = Lunchin_dt.Rows[Lunchin_dt.Rows.Count - 1][0].ToString();
                            }
                            else
                            {
                                Time_OUT_Str = "";
                            }

                            TimeSpan ts4;
                            ts4 = Convert.ToDateTime(String.Format("{0:hh:mm tt}", Emp_Total_Work_Time_1)).TimeOfDay;
                            if (Lunchin_dt.Rows.Count <= 0)
                            {
                                Time_IN_Str = "";
                            }
                            else
                            {
                                Time_IN_Str = Lunchout_dt.Rows[tin][0].ToString();
                            }

                            if (Time_IN_Str == "" || Time_OUT_Str == "")
                            {
                                time_Check_dbl = time_Check_dbl;
                            }
                            else
                            {
                                DateTime date3 = Convert.ToDateTime(Time_IN_Str);
                                DateTime date4 = Convert.ToDateTime(Time_OUT_Str);
                                TimeSpan ts1;
                                ts1 = date4.Subtract(date3);
                                Total_Time_get = ts1.Hours.ToString();
                                Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;

                                string[] Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }
                                if (Left_Val(Total_Time_get, 1) == "-")
                                {
                                    date4 = System.Convert.ToDateTime(Time_OUT_Str).AddDays(1);
                                    ts1 = date4.Subtract(date3);
                                    ts1 = date4.Subtract(date3);
                                    ts4 = ts4.Add(ts1);
                                    Total_Time_get = ts1.Hours.ToString();
                                    time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                    Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                    Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');


                                    if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    {

                                        Emp_Total_Work_Time_1 = "00:00";
                                    }
                                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                    {

                                        Emp_Total_Work_Time_1 = "00:00";
                                    }

                                }
                                else
                                {
                                    ts4 = ts4.Add(ts1);
                                    Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                    Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                    if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    {
                                        Emp_Total_Work_Time_1 = "00:00";
                                    }
                                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                    {
                                        Emp_Total_Work_Time_1 = "00:00";
                                        time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                    }
                                }
                            }

                        



                        }
                    }
                    else
                    {
                        TimeSpan ts4;
                        ts4 = Convert.ToDateTime(String.Format("{0:hh:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
                        if (Lunchout_dt.Rows.Count <= 0)
                        {
                            Time_IN_Str = "";
                        }
                        else
                        {
                            Time_IN_Str = Lunchout_dt.Rows[0][0].ToString();
                        }
                        for (int tout = 0; tout <= Lunchin_dt.Rows.Count; tout++)
                        {
                            if (Lunchin_dt.Rows.Count <= 0)
                            {
                                Time_OUT_Str = "";
                            }
                            else
                            {
                                Time_OUT_Str = Lunchin_dt.Rows[0][0].ToString();
                            }

                        }
                        if (Time_IN_Str == "" || Time_OUT_Str == "")
                        {
                            time_Check_dbl = 0;
                        }
                        else
                        {
                            DateTime date3 = System.Convert.ToDateTime(Time_IN_Str);
                            DateTime date4 = System.Convert.ToDateTime(Time_OUT_Str);
                            TimeSpan ts1;
                            ts1 = date4.Subtract(date3);
                            Total_Time_get = ts1.Hours.ToString();

                            if (Left_Val(Total_Time_get, 1) == "-")
                            {
                                date4 = System.Convert.ToDateTime(Time_OUT_Str).AddDays(1);
                                ts1 = date4.Subtract(date3);
                                ts1 = date4.Subtract(date3);
                                ts4 = ts4.Add(ts1);
                                Total_Time_get = ts1.Hours.ToString();
                                time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }
                            }
                            else
                            {
                                ts4 = ts4.Add(ts1);
                                string s1 = ts4.Minutes.ToString();
                                if (s1.Length == 1)
                                {
                                    string ts_str = "0" + ts4.Minutes;
                                    Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts_str;
                                }
                                else
                                {
                                    Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                }
                                Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }

                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }
                                time_Check_dbl = Convert.ToInt16(Total_Time_get);
                            }
                           




                        }

                    }

                    //LunchMaster
                    DataTable DT = new DataTable();
                    string MstLunchTime = "30";
                    string[] Lunch_Time_Value;
                    SSQL = "select * from LunchTiming_Mst where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And DeptName='" + dt.Rows[i]["DeptName"].ToString() + "' And Designation='" + dt.Rows[i]["Designation"].ToString() + "' And Wages='" + WagesType + "'";
                    DT = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (DT.Rows.Count != 0)
                    {
                        MstLunchTime = DT.Rows[0]["LunchTiming"].ToString();
                    }
                    AutoDTable.Rows[intK]["LunchHours"] = MstLunchTime;
                    if (Emp_Total_Work_Time_1 != "00:00")
                    {
                        string Value1 = "0";
                        string Values2 = "0";
                        string FinalValues = "0";
                        Lunch_Time_Value = Emp_Total_Work_Time_1.Split(':');
                        if (Convert.ToDecimal(Lunch_Time_Value[0]) > Convert.ToDecimal(00))
                        {
                            Value1 = (Convert.ToDecimal(Lunch_Time_Value[0]) * Convert.ToDecimal(60)).ToString();

                        }
                        Values2 = Lunch_Time_Value[1];
                        FinalValues = (Convert.ToDecimal(Value1) + Convert.ToDecimal(Values2)).ToString();

                        if (Convert.ToDecimal(MstLunchTime) < Convert.ToDecimal(FinalValues))
                        {
                            AutoDTable.Rows[intK][count] = "<span style=color:red><b>" + Emp_Total_Work_Time_1 + "</b></span>";
                            count1 = count1 + 1;
                        }
                        else if (Convert.ToDecimal(MstLunchTime) >= Convert.ToDecimal(FinalValues))
                        {
                            AutoDTable.Rows[intK][count] = "<span style=color:black><b>" + Emp_Total_Work_Time_1 + "</b></span>";
                            count2 = count2 + 1;
                        }
                    }
                    else
                    {
                        AutoDTable.Rows[intK][count] = "<span style=color:brown><b>" + "N" + "</b></span>";
                        count3 = count3 + 1;
                       
                    }

                    count = count + 1;
                    count4 = count4 + 1;
                }
               
                 AutoDTable.Rows[intK]["Late Days"]=count1;
                 AutoDTable.Rows[intK]["Exact Days"] = count2;
                 AutoDTable.Rows[intK]["Improper Punch"] = count3;

                      AutoDTable.Rows[intK]["Total Days"] = count4;
                              
                intK = intK + 1;

                     SNo = SNo + 1;
                      count1 = 0;
                    count2 = 0;
                    count3 = 0;
                    count4 = 0;
            }
            grid.DataSource = AutoDTable;
            grid.DataBind();
            string attachment = "attachment;filename=LUNCH REPORT BETWEEN DATES.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);
            Response.Write("<table>");
          
            Response.Write("<tr Font-Bold='true' align='left'>");
            Response.Write("<td colspan='10'>");
            Response.Write("<a style=\"font-weight:bold\">LUNCH REPORT BETWEEN DATES  -" + Session["Lcode"].ToString() + "-" + FromDate + "-" + ToDate + "</a>");

            Response.Write("</td>");
            Response.Write("</tr>");
          
            Response.Write("</table>");
            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();
        }
    }
}
