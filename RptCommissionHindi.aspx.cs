﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;
public partial class RptCommissionHindi : System.Web.UI.Page
{
    string ShiftType1 = "";
    string Date = "";
    string Status = "";
    string fmDate = "";
    string Todate = "";
    string Wages = "";
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string ss = "";


    ReportDocument report = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();

    string SSQL = "";
    
    DataTable AutoDTable = new DataTable();
    string Shift = "";

    DateTime date1;
    DateTime Date2 = new DateTime();
    

    System.Web.UI.WebControls.DataGrid grid =
                           new System.Web.UI.WebControls.DataGrid();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Report-Hindi Group Commission";
            }
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            Status = Request.QueryString["Status"].ToString();
            Shift = Request.QueryString["Shift"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            fmDate = Request.QueryString["FromDate"].ToString();
            Todate = Request.QueryString["ToDate"].ToString();
            Wages = Request.QueryString["Wages"].ToString();

            Report();
        }
    }

    private void Report()
    {
        string TableName = "";
        if (Status == "Pending")
        {
            TableName = "Employee_Mst_New_Emp";
        }
        else
        {
            TableName = "Employee_Mst";
        }
        
        SSQL = "";
        SSQL = "Select * from " + TableName + " where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and IsActive='Yes' and Hindi_Category!=''";
        //if (Wages.ToString() != "-Select-")
        //{
        //    SSQL = SSQL + " and Wages='" + Wages + "'";
        //}

        SSQL = SSQL + " and Wages='WEEKLY-HINDI' order by Hindi_Category ASC";

        DataTable EmpDt = new DataTable();
        EmpDt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (EmpDt.Rows.Count > 0)
        {
            AutoDTable.Columns.Add("MachineID");
            AutoDTable.Columns.Add("Name");
            AutoDTable.Columns.Add("Group Name");
            AutoDTable.Columns.Add("Commission Rate");
            AutoDTable.Columns.Add("Total Days");
            AutoDTable.Columns.Add("Total Amount");

            for (int EmpRw = 0; EmpRw < EmpDt.Rows.Count; EmpRw++)
            {
                decimal CommissionRate = 0;
                decimal Total_Days = 0;
                decimal Total_Hrs = 0;
                decimal Total_Amt = 0;

                AutoDTable.NewRow();
                AutoDTable.Rows.Add();

                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["MachineID"] = EmpDt.Rows[EmpRw]["MachineID"];
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Name"] = EmpDt.Rows[EmpRw]["FirstName"];

                date1 = Convert.ToDateTime(fmDate);
                string dat = Todate;
                Date2 = Convert.ToDateTime(dat);

                int daycount = (int)((Date2 - date1).TotalDays);
                int daysAdded = 0;

                while (daycount >= 0)
                {
                    DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded).ToShortDateString());

                    SSQL = "";
                    SSQL = "Select MachineID, FirstName,Total_Hrs from LogTime_Days where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and MachineID='"+EmpDt.Rows[EmpRw]["MachineID"] +"'";
                    SSQL = SSQL + " and Convert(datetime,Attn_Date_Str,103)=Convert(datetime,'"+dayy.ToString("dd/MM/yyyy")+ "',103) and Present='1'";

                    DataTable Log_DT = new DataTable();
                    Log_DT = objdata.RptEmployeeMultipleDetails(SSQL);

                    if (Log_DT.Rows.Count > 0)
                    {
                        Total_Hrs = Convert.ToDecimal(Log_DT.Rows[0]["Total_Hrs"]);

                        if (Total_Hrs >= 4)
                        {
                            Total_Days = Total_Days + 1;
                        }
                    }

                    daycount -= 1;
                    daysAdded += 1;
                }

                if (EmpDt.Rows[EmpRw]["EligibleCommissoin"].ToString()=="1")
                {
                    SSQL = "Select GroupName,Amt from MstCommissionHindi where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and GroupName='" + EmpDt.Rows[EmpRw]["Hindi_Category"] + "'";
                    DataTable Group_DT = new DataTable();

                    Group_DT = objdata.RptEmployeeMultipleDetails(SSQL);

                    if (Group_DT.Rows.Count > 0)
                    {
                        if (Group_DT.Rows[0]["Amt"].ToString() != "")
                        {
                            CommissionRate = Convert.ToDecimal(Group_DT.Rows[0]["Amt"].ToString());
                        }
                        else
                        {
                            CommissionRate = 0;
                        }
                    }
                    else
                    {
                        CommissionRate = 0;
                    }
                }


                if (CommissionRate > 0)
                {

                    Total_Amt = Convert.ToDecimal(CommissionRate * Total_Days);
                    Total_Amt = Math.Round(Convert.ToDecimal(Total_Amt),2,MidpointRounding.AwayFromZero);

                }

                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Group Name"] = EmpDt.Rows[EmpRw]["Hindi_Category"];

                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Commission Rate"] = CommissionRate;

                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Total Days"] = Total_Days;

                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Total Amount"] = Total_Amt;
            }
        }

        if (AutoDTable.Rows.Count > 0)
        {
            grid.DataSource = AutoDTable;
            grid.DataBind();
            string attachment = "attachment;filename=HINDI GROUP COMMISSION REPORT.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);
            Response.Write("<table>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='6'>");
            Response.Write("<a style=\"font-weight:bold\">COMMISSION REPORT -" + Session["Lcode"].ToString() + "-" + fmDate + "-" + Todate + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("</table>");
            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();
        }
    }
}