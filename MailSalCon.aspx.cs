﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;
using System.Net.Mail;

public partial class MailSalCon : System.Web.UI.Page
{
    string WitSalesOrdNo = "";
    string ShiftType1 = "";
    string Date = "";
    string Status = "";

    string AttachfileName_Invoice_Bill = "";

    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string ss = "";
    ReportDocument report = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();
    //  EmployeeDetailsClass objEmpDetails = new EmployeeDetailsClass();



    string SSQL = "";
    string Adolescent_Shift;
    DataSet ds = new DataSet();

    string Division = "";
    DataTable AutoDTable = new DataTable();
    DataTable DataCell = new DataTable();
    DataTable dt = new DataTable();
    string OTEligible = "";//By Selva
    string IsActive = "";//By Selva

    string Datestr = "";
    string Datestr1 = "";
    string ShiftType = "";
    DataTable mDataSet = new DataTable();

    DataTable mLocalDS_INTAB = new DataTable();
    DataTable mLocalDS_OUTTAB = new DataTable();
    string Date_Value_Str;
    string Time_IN_Str = "";
    string Time_Out_Str = "";
    int time_Check_dbl = 0;
    string Total_Time_get = "";

    DataRow dtRow;

    DataTable MLocal_Day = new DataTable();
    DataTable Payroll_DS = new DataTable();

    string Shift_Start_Time;
    string Shift_End_Time;
    string Employee_Time = "";
    DateTime ShiftdateStartIN = new DateTime();
    DateTime ShiftdateEndIN = new DateTime();
    DateTime EmpdateIN = new DateTime();
    string ColumnName = "";
    double Totol_Hours_Check = 0;
    double Totol_Hours_Check_1 = 8.2;
    string[] Time_Split_Str;
    string Time_IN_Get;
    string Time_Out_Update = "";
    long Random_No_Get;
    string[] Time_Minus_Value_Check;

    string todate;
    string fromdate;
    DateTime Date1;


    public string Left_Val(string Value, int Length)
    {
        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        SessionCcode = "THEN";
        SessionLcode = "UNIT I";
        SessionAdmin = "1";
        SessionCompanyName = "Thenpandi Spinning Mill";
        SessionLocationName = "UNIT I";
        SessionUserType = "1";
        Date1 = DateTime.Now.AddDays(-1);
        DateTime Date2 = DateTime.Now.AddDays(-1);

        DateTime dateGiven = DateTime.Now;
        dateGiven = dateGiven.AddMonths(-1);
        DateTime thisMonth = new DateTime(dateGiven.Year, dateGiven.Month, 1);
        //thisMonth.ToString("yyyy-MM-01");
        //fromdate = "01-" + dateGiven.Month + "-" + dateGiven.Year;
        //todate = thisMonth.AddMonths(1).AddDays(-1).Day + "-" + dateGiven.Month + "-" + dateGiven.Year;

        //fromdate = "07/10/2018";

        fromdate = Convert.ToString(Date1.ToString("dd/MM/yyyy"));
        todate = Convert.ToString(Date2.ToString("dd/MM/yyyy"));

        GetAttdDayWise_Change();

        InvoiceMail_AttachandSend("", "tpsmhrm@gmail.com", "padmanabhanscoto@gmail.com", "Daily Reports");

        ClientScript.RegisterStartupScript(typeof(Page), "closePage", "window.close();", true);
       

    }
    public void GetAttdDayWise_Change()
    {
        string TableName = "";

        string GrandTotal = "";
        string GrandTotalWages = "";

        int generalcount = 0;
        int shift1count = 0;
        int shift2count = 0;
        int shift3count = 0;

        int general = 0;
        int shift1 = 0;
        int shift2 = 0;
        int shift3 = 0;

        TableName = "Employee_Mst";

        decimal rounded = 0;
        decimal roundedsalary = 0;
        decimal staffonedaysalary;

        AutoDTable.Columns.Add("ExCode");
        AutoDTable.Columns.Add("Dept");
        AutoDTable.Columns.Add("Name");
        AutoDTable.Columns.Add("TimeIN");
        AutoDTable.Columns.Add("TimeOUT");
        AutoDTable.Columns.Add("DayWages");
        AutoDTable.Columns.Add("TotalMIN");
        AutoDTable.Columns.Add("Wages");
        AutoDTable.Columns.Add("Shift");
        AutoDTable.Columns.Add("SubCatName");
        AutoDTable.Columns.Add("GradeName");

        AutoDTable.Columns.Add("GradeTotalCount");
        AutoDTable.Columns.Add("GeneralCount");
        AutoDTable.Columns.Add("Shift1Count");
        AutoDTable.Columns.Add("Shift2Count");
        AutoDTable.Columns.Add("Shift3Count");

        AutoDTable.Columns.Add("ShiftWages");
        AutoDTable.Columns.Add("OTHours");

        DataTable Shift_DS = new DataTable();

        SSQL = "select EM.MachineID,DEP.DeptName,EM.Shift,isnull(EM.FirstName,'') as FirstName,EM.TimeIN,EM.TimeOUT,";
        SSQL = SSQL + "DEP.SubCatName,EM.Total_Hrs,EM.Total_Hrs1,DEP.CatName,DEP.BaseSalary,DEP.StdWrkHrs,DEP.OTEligible,DEP.IsActive from LogTime_Days EM  inner join Employee_Mst DEP on  EM.MachineID= DEP.MachineID ";
        SSQL = SSQL + " where EM.CompCode='" + SessionCcode + "' ANd EM.LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " And DEP.CompCode='" + SessionCcode + "' ANd DEP.LocCode='" + SessionLcode + "'";

        SSQL = SSQL + " And EM.Shift!='No Shift'";
        SSQL = SSQL + " And DEP.DeptName!='CONTRACT'";

        if (fromdate != "")
        {
            SSQL = SSQL + " And CONVERT(DATETIME,EM.Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(fromdate).ToString("dd/MM/yyyy") + "',103)";
        }
        SSQL = SSQL + " And  (Present_Absent='Present' or Present_Absent='Half Day') ";

        dt = objdata.RptEmployeeMultipleDetails(SSQL);

        string staffsalary = "";
        string staffwages = "";

        string TempWorkHours = "";

        string[] TempWorkHourSlip;
        string Machineid = "";
        string Deptname = "";
        string workinghours = "";
        string stdwrkhrs = "";
        decimal staffonehoursalary;
        decimal Totalwages;
        staffonedaysalary = 0;

        string OThours = ""; //Edit by Narmatha
        string OTSal = "0"; //Edit by Narmatha

        DataTable dt1 = new DataTable();

        if (dt.Rows.Count > 0)
        {
            for (int k = 0; k < dt.Rows.Count; k++)
            {
                Machineid = dt.Rows[k]["MachineID"].ToString();
                staffwages = dt.Rows[k]["CatName"].ToString();
                Deptname = dt.Rows[k]["DeptName"].ToString();
                stdwrkhrs = dt.Rows[k]["StdWrkHrs"].ToString();

                OTEligible = dt.Rows[k]["OTEligible"].ToString();//By Selva
                IsActive = dt.Rows[k]["IsActive"].ToString();//By Selva

                OTSal = "0";//Edit by Narmatha



                if (stdwrkhrs == "" || stdwrkhrs == "0")
                {
                    stdwrkhrs = "8";
                }

                //Check Above 8 hours conditions
                TempWorkHours = dt.Rows[k]["Total_Hrs1"].ToString();
                TempWorkHourSlip = TempWorkHours.Split(':');

                workinghours = dt.Rows[k]["Total_Hrs"].ToString();
                OThours = "0"; //Edit by Narmatha


                if (Machineid == "3166")
                {
                    Machineid = "3166";
                }
                if (staffwages == "STAFF" || Deptname == "FITTER & ELECTRICIANS" || Deptname == "SECURITY" || Deptname == "DRIVERS")
                {
                    if (dt.Rows[k]["OTEligible"].ToString().Trim() == "1")
                    {
                        staffsalary = dt.Rows[k]["BaseSalary"].ToString();
                        staffonedaysalary = Convert.ToDecimal(staffsalary.ToString()) / 26;
                        roundedsalary = (Math.Round(staffonedaysalary, 0, MidpointRounding.AwayFromZero));

                        //OT salary for Labour by Narmatha
                        string workinghours_New = (Convert.ToDecimal(workinghours) - Convert.ToDecimal(OThours)).ToString();


                        decimal check = 0;
                        check = Convert.ToDecimal(workinghours.ToString());
                        check = (Math.Round(check, 0, MidpointRounding.AwayFromZero));

                        if (Convert.ToDecimal(check) >= Convert.ToDecimal(stdwrkhrs))
                        {
                            rounded = (Convert.ToDecimal(roundedsalary) / Convert.ToDecimal(8));
                            rounded = (Convert.ToDecimal(check) * Convert.ToDecimal(rounded));
                            rounded = (Math.Round(rounded, 0, MidpointRounding.AwayFromZero));

                            OThours = (Convert.ToDecimal(workinghours_New) - Convert.ToDecimal(stdwrkhrs)).ToString();
                        }
                        else if (check >= 4 || check < Convert.ToDecimal(stdwrkhrs))
                        {
                            staffonehoursalary = staffonedaysalary / 2;
                            //Totalwages = staffonehoursalary * Convert.ToDecimal(workinghours.ToString());
                            rounded = (Math.Round(staffonehoursalary, 0, MidpointRounding.AwayFromZero));
                        }
                        else if (check <= 4)
                        {
                            rounded = 0;
                        }
                    }
                    else
                    {
                        staffsalary = dt.Rows[k]["BaseSalary"].ToString();
                        staffonedaysalary = Convert.ToDecimal(staffsalary.ToString()) / 26;
                        roundedsalary = (Math.Round(staffonedaysalary, 0, MidpointRounding.AwayFromZero));

                        //OT salary for Labour by Narmatha
                        string workinghours_New = (Convert.ToDecimal(workinghours)).ToString();


                        decimal check = 0;
                        check = Convert.ToDecimal(workinghours.ToString());
                        check = (Math.Round(check, 0, MidpointRounding.AwayFromZero));

                        if (Convert.ToDecimal(check) >= Convert.ToDecimal(stdwrkhrs))
                        {
                            rounded = (Convert.ToDecimal(roundedsalary) / Convert.ToDecimal(8));
                            rounded = (Convert.ToDecimal(8) * Convert.ToDecimal(rounded));
                            rounded = (Math.Round(rounded, 0, MidpointRounding.AwayFromZero));

                            OThours = "0";
                        }
                        else if (check >= 4 || check < Convert.ToDecimal(stdwrkhrs))
                        {
                            staffonehoursalary = staffonedaysalary / 2;
                            //Totalwages = staffonehoursalary * Convert.ToDecimal(workinghours.ToString());
                            rounded = (Math.Round(staffonehoursalary, 0, MidpointRounding.AwayFromZero));
                        }
                        else if (check <= 4)
                        {
                            rounded = 0;
                        }
                    }

                }
                else
                {
                    staffsalary = dt.Rows[k]["BaseSalary"].ToString();
                    staffonedaysalary = Convert.ToDecimal(staffsalary.ToString());
                    roundedsalary = (Math.Round(staffonedaysalary, 0, MidpointRounding.AwayFromZero));

                    //OT salary for Labour by Narmatha
                    string workinghours_New = (Convert.ToDecimal(workinghours) - Convert.ToDecimal(OThours)).ToString();

                    OTSal = (Convert.ToDecimal(OTSal) * Convert.ToDecimal(OThours.ToString())).ToString();

                    if (dt.Rows[k]["Shift"].ToString().Trim().ToUpper() == "SHIFT3")
                    {
                        staffonehoursalary = staffonedaysalary / Convert.ToDecimal(7);
                        Totalwages = staffonehoursalary * Convert.ToDecimal(workinghours_New.ToString());

                        //Stdworking hrs Salary plus OT Sal = TotalWages by Narmatha
                        Totalwages = Totalwages + Convert.ToDecimal(OTSal.ToString());

                        rounded = (Math.Round(Totalwages, 0, MidpointRounding.AwayFromZero));
                    }
                    else
                    {
                        staffonehoursalary = staffonedaysalary / Convert.ToDecimal(stdwrkhrs.ToString());
                        Totalwages = staffonehoursalary * Convert.ToDecimal(workinghours_New.ToString());

                        //Stdworking hrs Salary plus OT Sal = TotalWages by Narmatha
                        Totalwages = Totalwages + Convert.ToDecimal(OTSal.ToString());

                        rounded = (Math.Round(Totalwages, 0, MidpointRounding.AwayFromZero));
                    }

                    //By Selva v
                    if ((OTEligible == "Yes" && IsActive == "Yes") && (Convert.ToDecimal(workinghours_New) > 8))
                    {
                        OThours = (Convert.ToDecimal(workinghours_New) - Convert.ToDecimal(stdwrkhrs)).ToString();
                    }
                    else
                    {
                        OThours = "0";
                    }
                    //By Selva ^
                }


                AutoDTable.NewRow();
                AutoDTable.Rows.Add();
                AutoDTable.Rows[k]["ExCode"] = dt.Rows[k]["MachineID"].ToString();
                AutoDTable.Rows[k]["Dept"] = dt.Rows[k]["DeptName"].ToString();
                AutoDTable.Rows[k]["Name"] = dt.Rows[k]["FirstName"].ToString();
                AutoDTable.Rows[k]["TimeIN"] = dt.Rows[k]["TimeIN"].ToString();
                AutoDTable.Rows[k]["TimeOUT"] = dt.Rows[k]["TimeOUT"].ToString();
                AutoDTable.Rows[k]["DayWages"] = rounded;

                AutoDTable.Rows[k]["TotalMIN"] = workinghours;// dt.Rows[k]["Total_Hrs"].ToString();
                AutoDTable.Rows[k]["Wages"] = roundedsalary.ToString();
                AutoDTable.Rows[k]["Shift"] = dt.Rows[k]["Shift"].ToString();
                AutoDTable.Rows[k]["SubCatName"] = dt.Rows[k]["SubCatName"].ToString();
                AutoDTable.Rows[k]["GradeName"] = "A+";
                AutoDTable.Rows[k]["OTHours"] = OThours;


                if (dt.Rows[k]["Shift"].ToString() == "GENERAL")
                {
                    generalcount = generalcount + 1;
                    general = Convert.ToInt32(Convert.ToDecimal(general) + Convert.ToDecimal(rounded));
                }
                else if (dt.Rows[k]["Shift"].ToString() == "SHIFT1")
                {
                    shift1count = shift1count + 1;
                    shift1 = Convert.ToInt32(Convert.ToDecimal(shift1) + Convert.ToDecimal(rounded));
                }
                else if (dt.Rows[k]["Shift"].ToString() == "SHIFT2")
                {
                    shift2count = shift2count + 1;
                    shift2 = Convert.ToInt32(Convert.ToDecimal(shift2) + Convert.ToDecimal(rounded));
                }
                else if (dt.Rows[k]["Shift"].ToString() == "SHIFT3")
                {
                    shift3count = shift3count + 1;
                    shift3 = Convert.ToInt32(Convert.ToDecimal(shift3) + Convert.ToDecimal(rounded));
                }

                GrandTotal = (Convert.ToDecimal(generalcount) + Convert.ToDecimal(shift1count) + Convert.ToDecimal(shift2count) + Convert.ToDecimal(shift3count)).ToString();

                GrandTotalWages = (Convert.ToDecimal(general) + Convert.ToDecimal(shift1) + Convert.ToDecimal(shift2) + Convert.ToDecimal(shift3)).ToString();

                //AutoDTable.Rows[k]["GeneralCount"] = Convert.ToString(generalcount);
                //AutoDTable.Rows[k]["Shift1Count"] = Convert.ToString(shift1count);
                //AutoDTable.Rows[k]["Shift2Count"] = Convert.ToString(shift2count);
                //AutoDTable.Rows[k]["Shift3Count"] = Convert.ToString(shift3count);

            }
        }

        SSQL = "select ShiftDesc from Shift_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        Shift_DS = objdata.RptEmployeeMultipleDetails(SSQL);

        DataTable Depart = new DataTable();

        SSQL = "select distinct  DeptName from Department_Mst ";
        SSQL = SSQL + " where DeptName!='CONTRACT' order by DeptName Asc"; //Edit By Narmatha
        Depart = objdata.RptEmployeeMultipleDetails(SSQL);

        DataTable SubCat = new DataTable();

        SSQL = "select SubCatName from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' group by SubCatName ";
        SubCat = objdata.RptEmployeeMultipleDetails(SSQL);

        string Customer_Mail_ID = "tpsmhrm@gmail.com";
        string Agent_Mail_ID = "padmanabhanscoto@gmail.com";


        string[] Check_Val = fromdate.Split(new[] { "/", "_" }, StringSplitOptions.RemoveEmptyEntries);





        ds.Tables.Add(AutoDTable);
        //ReportDocument report = new ReportDocument();
        report.Load(Server.MapPath("crystal/SalaryConsolidateNew.rpt"));
        report.DataDefinition.FormulaFields["GradeTotalCount"].Text = "'" + GrandTotal + "'";
        report.DataDefinition.FormulaFields["ShiftDate"].Text = "'" + fromdate + "'";
        //report.DataDefinition.FormulaFields["ShiftDate"].Text = "'" + Date + "'";
        report.DataDefinition.FormulaFields["GeneralCount"].Text = "'" + generalcount + "'";
        report.DataDefinition.FormulaFields["Shift1Count"].Text = "'" + shift1count + "'";
        report.DataDefinition.FormulaFields["Shift2Count"].Text = "'" + shift2count + "'";
        report.DataDefinition.FormulaFields["Shift3Count"].Text = "'" + shift3count + "'";


        //Get All Salary
        report.DataDefinition.FormulaFields["GrandTotalWages"].Text = "'" + GrandTotalWages + "'";
        report.DataDefinition.FormulaFields["GenWages"].Text = "'" + general + "'";
        report.DataDefinition.FormulaFields["S1Wages"].Text = "'" + shift1 + "'";
        report.DataDefinition.FormulaFields["S2Wages"].Text = "'" + shift2 + "'";
        report.DataDefinition.FormulaFields["S3Wages"].Text = "'" + shift3 + "'";

        report.Database.Tables[0].SetDataSource(ds.Tables[0]);

        string Server_Path = Server.MapPath("~");

        string Invoice_No_Str = WitSalesOrdNo.Replace("/", "_");
        AttachfileName_Invoice_Bill = Server_Path + "/Daily_Report/Salary_" + Check_Val[0] + Check_Val[1] + Check_Val[2] + ".pdf";

        if (File.Exists(AttachfileName_Invoice_Bill))
        {
            File.Delete(AttachfileName_Invoice_Bill);
        }

        report.ExportToDisk(ExportFormatType.PortableDocFormat, AttachfileName_Invoice_Bill);



        
        //report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
        //CrystalReportViewer1.ReportSource = report;

    }

    public void InvoiceMail_AttachandSend(string Invoice_Bill_No, string Customer_Mail_Str, string Agent_Mail_ID_Str, string Mail_Subject)
    {
        string query = "";
        string To_Address = "";
        string CC_Mail_Address = "";
        string BCC_Mail_Address = "";
        DataTable Order_Mail_DT = new DataTable();
        DataTable DT_T = new DataTable();
        DataTable Party_DT = new DataTable();
        if (Customer_Mail_Str != "")
        {

            To_Address = Customer_Mail_Str;
            if (Agent_Mail_ID_Str != "") { To_Address = To_Address + "," + Agent_Mail_ID_Str; }

            //CC Address Add
            CC_Mail_Address = "aatm2005@gmail.com";// "kalyan.r@scoto.in";

            MailMessage mm = new MailMessage("THENPANDI SPINNING<aatm2005@gmail.com>", To_Address);
            if (Mail_Subject != "")
            {
                mm.Subject = Mail_Subject;
            }
            else
            {
                mm.Subject = "Daily Report";
            }
            mm.Body = "Find the attachment for Daily Details";
            mm.Attachments.Add(new Attachment(AttachfileName_Invoice_Bill));
            //mm.Attachments.Add(new Attachment(AttachfileName_Improper));
            //mm.Attachments.Add(new Attachment(AttachfileName_Miss));


            if (CC_Mail_Address != "") { mm.CC.Add(CC_Mail_Address); }
            //if (BCC_Mail_Address != "") { mm.Bcc.Add(BCC_Mail_Address); }

            mm.IsBodyHtml = false;
            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com";   // We use gmail as our smtp client
            smtp.Port = 587;
            smtp.EnableSsl = true;
            smtp.UseDefaultCredentials = true;
            smtp.Credentials = new System.Net.NetworkCredential("aatm2005@gmail.com", "Altius@2005");

            smtp.Send(mm);

            mm.Attachments.Dispose();
            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Mail Sent Successfully..');", true);

        }


    }
}
