﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using System.IO;
using AjaxControlToolkit;
using System.Collections.Generic;
using System.Drawing;

public partial class EmployeeDetails : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights; string SessionAdmin;
    string SSQL;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Employee Details";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");

            Load_Date();

            Initial_Data_Referesh();
            Initial_Data1_Referesh();
            rbtEligibleDayincentive_SelectedIndexChanged(sender, e);

            Load_Department();
            Load_Community();
            Load_Qualification();
            Load_HostelExp();
            Load_Designation();
            Load_RoomNo();
            Load_WagesType();
            Load_Bank();
            Load_Unit();
            Load_WorkingUnit();
            Load_Taluk();
            Load_District();
            Load_State();
            Load_Recruitment();
            Load_AgentName();
            Load_RoomName();
            Load_PFCode();
            Load_ESICode();
            Load_Grade();
            Load_Division();
            Load_Route();
            Load_BusNo();
            rbtnSalaryThrough_SelectedIndexChanged(sender, e);
            RdbPFEligible_SelectedIndexChanged(sender, e);
            RdbESIEligible_SelectedIndexChanged(sender, e);
            txtRecruitThrg_SelectedIndexChanged(sender, e);
            ddlWorkingUnit.SelectedValue = SessionLcode;
            ddlSalaryUnit.SelectedValue = SessionLcode;
            // LoadShiftType();
            LoadHindiCat();
            if (SessionAdmin == "2")
            {
                IF_PF_Eligible.Visible = false;
                IF_ESI_Eligible.Visible = false;
                RdbPFEligible.SelectedValue = "1";
                RdbPFEligible_SelectedIndexChanged(sender, e);
                RdbESIEligible.SelectedValue = "1";
                RdbESIEligible_SelectedIndexChanged(sender, e);
            }
            else
            {
                IF_PF_Eligible.Visible = true;
                IF_ESI_Eligible.Visible = true;
            }

            if (Session["MachineID"] != null)
            {
                txtMachineID.Text = Session["MachineID"].ToString();
                txtMachineID.Enabled = false;
                btnSearch_Click(sender, e);
            }

            if (Session["MachineID_Apprv"] != null)
            {
                txtMachineID.Text = Session["MachineID_Apprv"].ToString();
                txtMachineID.Enabled = false;
                btnSearch_Click(sender, e);
                btnEmpSave.Enabled = false;
                btnBack.Visible = true;
                btnApprove.Visible = true;
                btnCancel_Approve.Visible = true;
                Approve_Cancel_panel.Visible = true;
                btnEmpSave.Visible = false;
                btnEmpClear.Visible = false;
            }
        }
        Load_OLD_data();
        Load_OLD_data1();
    }

    private void LoadHindiCat()
    {
        SSQL = "";
        SSQL = "Select * from MstHindiCat where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        ddlHindiCategory.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlHindiCategory.DataTextField = "CatName";
        ddlHindiCategory.DataValueField = "CatCode";
        ddlHindiCategory.DataBind();
        ddlHindiCategory.Items.Insert(0, new ListItem("Select", "0"));
    }

    private void LoadShiftType()
    {
        SSQL = "";
        SSQL = "Select ShiftDesc from Shift_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' order by ShiftDesc ASC";
        ddlShiftType.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlShiftType.DataTextField = "ShiftDesc";
        ddlShiftType.DataValueField = "ShiftDesc";
        ddlShiftType.DataBind();
    }

    private void Load_RoomName()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlRoomName.Items.Clear();
        query = "Select *from MstRoomName";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlRoomName.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["RoomCode"] = "0";
        dr["RoomName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlRoomName.DataTextField = "RoomName";
        ddlRoomName.DataValueField = "RoomCode";
        ddlRoomName.DataBind();
    }

    protected void Upload(object sender, EventArgs e)
    {
        string token_Name = "";

        string UNIT_Folder = "";
        string Doc_Folder = "";
        if (SessionLcode.ToUpper() == "UNIT I") { UNIT_Folder = "Emp_Scan/UNIT_I/Photos/"; }
        if (SessionLcode.ToUpper() == "UNIT II") { UNIT_Folder = "Emp_Scan/UNIT_II/Photos/"; }
        if (SessionLcode.ToUpper() == "UNIT III") { UNIT_Folder = "Emp_Scan/UNIT_III/Photos/"; }
        if (SessionLcode.ToUpper() == "UNIT IV") { UNIT_Folder = "Emp_Scan/UNIT_IV/Photos/"; }

        token_Name = txtTokenID.Text;




        string path_1 = UNIT_Folder;

        //if (FileUpload1.HasFile)
        //{
        // string FileName = Path.GetFileName(FileUpload1.PostedFile.FileName);
        // string Exten = Path.GetExtension(FileUpload1.PostedFile.FileName);

        //FileUpload1.SaveAs(Server.MapPath("~/" + path_1 + token_Name + Exten));
        //}

    }

    public void Load_Date()
    {
        string query = "";
        DataTable dt = new DataTable();
        query = "Select GETDATE() as curDate";
        dt = objdata.RptEmployeeMultipleDetails(query);

        if (dt.Rows.Count != 0)
        {
            txtAge18Comp_Date.Text = Convert.ToDateTime(dt.Rows[0]["curDate"].ToString()).ToString("dd/MM/yyyy");
            txtAdoles_Due_Date.Text = Convert.ToDateTime(dt.Rows[0]["curDate"].ToString()).ToString("dd/MM/yyyy");
            txtCertificate_Date.Text = Convert.ToDateTime(dt.Rows[0]["curDate"].ToString()).ToString("dd/MM/yyyy");

        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        DataTable DT_Check = new DataTable();
        string path_3 = "";
        string UNIT_Folder = "";
        btnEmpSave.Text = "Update";
        SSQL = "Select * from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " And EmpNo='" + txtMachineID.Text + "'";
        DT_Check = objdata.RptEmployeeMultipleDetails(SSQL);

        if (DT_Check.Rows.Count == 0)
        {
            SSQL = "Select * from Employee_Mst_New_Emp where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And EmpNo='" + txtMachineID.Text + "'";
            DT_Check = objdata.RptEmployeeMultipleDetails(SSQL);
        }

        if (DT_Check.Rows.Count != 0)
        {

            txtExistingCode.Text = DT_Check.Rows[0]["ExistingCode"].ToString();
            txtTokenID.Text = DT_Check.Rows[0]["EmpNo"].ToString();

            DataTable DT_Photo = new DataTable();
            string SS = "Select * from Photo_Path_Det";
            DT_Photo = objdata.RptEmployeeMultipleDetails(SS);

            string PhotoDet1 = "";
            if (DT_Photo.Rows.Count != 0)
            {
                PhotoDet1 = DT_Photo.Rows[0]["Photo_Path"].ToString();
            }

            if (SessionLcode.ToUpper() == "UNIT I") { UNIT_Folder = PhotoDet1 + "/UNIT_I/"; }
            string path_1 = PhotoDet1 + "\\" + txtTokenID.Text + ".jpg";
            byte[] imageBytes = new byte[0];


            if (File.Exists((path_1)))
            {
                Image3.Visible = true;
                Image3.ImageUrl = "Handler.ashx?f=" + path_1 + "";
            }
            else
            {
                Image3.Visible = true;
                Image3.ImageUrl = "~/assets/images/No_Image.jpg";
            }

            string path_2 = UNIT_Folder + "/ParentPhoto/" + txtTokenID.Text + ".jpg";

            //File Check
            if (File.Exists((path_2)))
            {
                Image1.Visible = true;
                Image1.ImageUrl = "Handler.ashx?f=" + path_2 + "";
            }
            else
            {
                Image1.Visible = true;
                Image1.ImageUrl = "~/assets/images/No_Image.jpg";
            }


            txtFirstName.Text = DT_Check.Rows[0]["FirstName"].ToString();
            txtLastName.Text = DT_Check.Rows[0]["LastName"].ToString();
            ddlCategory.SelectedValue = DT_Check.Rows[0]["CatName"].ToString();
            ddlSubCategory.SelectedValue = DT_Check.Rows[0]["SubCatName"].ToString();
            ddlShift.SelectedValue = DT_Check.Rows[0]["ShiftType"].ToString();
            txtDOB.Text = Convert.ToDateTime(DT_Check.Rows[0]["BirthDate"]).ToString("dd/MM/yyyy");
            ddlShiftType.SelectedItem.Text = DT_Check.Rows[0]["ShiftType_New"].ToString();
            txtDOB_TextChanged(sender, e);

            txtVanNo.Text = DT_Check.Rows[0]["BusNo"].ToString();
            txtVanRoute.Text = DT_Check.Rows[0]["BusRoute"].ToString();

            //txtAge.Text = DT_Check.Rows[0]["Age"].ToString();

            ddlGender.SelectedValue = DT_Check.Rows[0]["Gender"].ToString();
            txtDOJ.Text = Convert.ToDateTime(DT_Check.Rows[0]["DOJ"]).ToString("dd/MM/yyyy");
            ddlDepartment.SelectedValue = DT_Check.Rows[0]["DeptCode"].ToString();
            ddlRoomName.SelectedValue = DT_Check.Rows[0]["RoomNameID"].ToString();
            ddlHindiCategory.SelectedItem.Text = DT_Check.Rows[0]["Hindi_Category"].ToString();
            string Val_Fixed = DT_Check.Rows[0]["FixedSalary"].ToString();
            string Val_tMasthiri = DT_Check.Rows[0]["EligibleMasthiri_Inc"].ToString();
            rbtHindiCommission.SelectedValue = DT_Check.Rows[0]["EligibleCommissoin"].ToString();
            txtmiltype.SelectedValue = DT_Check.Rows[0]["Unit_Type"].ToString();
            rbtEligibleDayincentive.SelectedValue = DT_Check.Rows[0]["EligibleDayIncentive"].ToString();
            txtSalary2.Text = DT_Check.Rows[0]["Salary2"].ToString();
            if (Val_Fixed == "Yes")
            {
                rbtnFixed_Salary.SelectedValue = "1";
            }
            else
            {
                rbtnFixed_Salary.SelectedValue = "2";
            }
            if (Val_tMasthiri == "Yes")
            {
                rbtMasthiri.SelectedValue = "1";
            }
            else
            {
                rbtMasthiri.SelectedValue = "2";
            }
            Load_Designation();

            Load_RoomNo();


            ddlDesignation.SelectedValue = DT_Check.Rows[0]["Designation"].ToString();
            txtQulification.SelectedValue = DT_Check.Rows[0]["Qualification"].ToString();
            if (DT_Check.Rows[0]["EmployeeMobile"].ToString() != "")
            {
                string[] MobileNo = DT_Check.Rows[0]["EmployeeMobile"].ToString().Split('-');
                if (MobileNo.Length == 2)
                {
                    txtEmpMobileNo.Text = MobileNo[1].ToString();
                }
                else
                {
                    txtEmpMobileNo.Text = "";
                }
            }
            ddlOTEligible.SelectedValue = DT_Check.Rows[0]["OTEligible"].ToString();
            ddlWagesType.SelectedValue = DT_Check.Rows[0]["Wages"].ToString();
            ddlWagesType_SelectedIndexChanged(sender, e);
            ddlEmpLevel.SelectedValue = DT_Check.Rows[0]["EmpLevel"].ToString();
            RdbPFEligible.SelectedValue = DT_Check.Rows[0]["Eligible_PF"].ToString();
            txtPFNo.Text = DT_Check.Rows[0]["PFNo_New"].ToString();
            if (DT_Check.Rows[0]["PFDOJ"].ToString() != "")
            {
                txtPFDate.Text = Convert.ToDateTime(DT_Check.Rows[0]["PFDOJ"].ToString()).ToString("dd/MM/yyyy");
            }
            else
            {
                txtPFDate.Text = "";
            }
            RdbESIEligible.SelectedValue = DT_Check.Rows[0]["Eligible_ESI"].ToString();
            txtESINo.Text = DT_Check.Rows[0]["ESINo"].ToString();
            if (DT_Check.Rows[0]["ESIDOJ"].ToString() != "")
            {
                txtESIDate.Text = Convert.ToDateTime(DT_Check.Rows[0]["ESIDOJ"].ToString()).ToString("dd/MM/yyyy");
            }
            else
            {
                txtESIDate.Text = "";
            }

            RdbPFEligible_SelectedIndexChanged(sender, e);
            RdbESIEligible_SelectedIndexChanged(sender, e);

            txtUAN.Text = DT_Check.Rows[0]["UAN"].ToString();
            ddlPFCode.SelectedValue = DT_Check.Rows[0]["PF_Code"].ToString();
            ddlESICode.SelectedValue = DT_Check.Rows[0]["ESICode"].ToString();
            txtHostelRoom.SelectedValue = DT_Check.Rows[0]["RoomNo"].ToString();
            ddlWorkType.SelectedValue = DT_Check.Rows[0]["WorkingType"].ToString();

            if (DT_Check.Rows[0]["IsActive"].ToString() == "Yes")
            {
                dbtnActive.SelectedValue = "1";
            }
            else
            {
                dbtnActive.SelectedValue = "2";
            }
            txtReliveDate.Text = DT_Check.Rows[0]["DOR"].ToString();
            txtReason.Text = DT_Check.Rows[0]["Reason"].ToString();
            txt480Days.Text = DT_Check.Rows[0]["Emp_Permn_Date"].ToString();
            txtCertificate.Text = DT_Check.Rows[0]["Certificate"].ToString();

            if (DT_Check.Rows[0]["ExemptedStaff"].ToString() == "1")
            {
                chkExment.Checked = true;
            }
            else
            {
                chkExment.Checked = false;
            }
            txtFormIDate.Text = DT_Check.Rows[0]["FormIObtained"].ToString();

            chkExment_CheckedChanged(sender, e);

            ddlWeekOff.SelectedValue = DT_Check.Rows[0]["WeekOff"].ToString();
            rbtnSalaryThrough.SelectedValue = DT_Check.Rows[0]["Salary_Through"].ToString();
            rbtnSalaryThrough_SelectedIndexChanged(sender, e);
            ddlBankName.SelectedValue = DT_Check.Rows[0]["BankName"].ToString();
            txtIFSC.Text = DT_Check.Rows[0]["IFSC_Code"].ToString();
            txtBranch.Text = DT_Check.Rows[0]["BranchCode"].ToString();
            txtAccNo.Text = DT_Check.Rows[0]["AccountNo"].ToString();
            txtBasic.Text = DT_Check.Rows[0]["BaseSalary"].ToString();
            if (DT_Check.Rows[0]["VPF"].ToString() != "")
            {
                txtVPF.Text = DT_Check.Rows[0]["VPF"].ToString();
            }
            else
            {
                txtVPF.Text = "0";
            }
            if (DT_Check.Rows[0]["Alllowance1"].ToString() != "")
            {
                txtAllowance1.Text = DT_Check.Rows[0]["Alllowance1"].ToString();
            }
            else
            {
                txtAllowance1.Text = "0.0";
            }
            if (DT_Check.Rows[0]["Alllowance2"].ToString() != "")
            {
                txtAllowance2.Text = DT_Check.Rows[0]["Alllowance2"].ToString();
            }
            else
            {
                txtAllowance2.Text = "0.0";
            }
            if (DT_Check.Rows[0]["Deduction1"].ToString() != "")
            {
                txtDeduction1.Text = DT_Check.Rows[0]["Deduction1"].ToString();
            }
            else
            {
                txtDeduction1.Text = "0.0";
            }
            if (DT_Check.Rows[0]["Deduction2"].ToString() != "")
            {
                txtDeduction2.Text = DT_Check.Rows[0]["Deduction2"].ToString();
            }
            else
            {
                txtDeduction2.Text = "0.0";
            }
            if (DT_Check.Rows[0]["OT_Salary"].ToString() != "")
            {
                txtOTSal.Text = DT_Check.Rows[0]["OT_Salary"].ToString();
            }
            else
            {
                txtOTSal.Text = "0.0";
            }

            ddlMartialStatus.SelectedValue = DT_Check.Rows[0]["MaritalStatus"].ToString();
            txtNationality.Text = DT_Check.Rows[0]["Nationality"].ToString();
            txtReligion.Text = DT_Check.Rows[0]["Religion"].ToString();
            txtHeight.Text = DT_Check.Rows[0]["Height"].ToString();
            txtWeight.Text = DT_Check.Rows[0]["Weight"].ToString();

            ddlCommunity.SelectedValue = DT_Check.Rows[0]["Community"].ToString();

            if (DT_Check.Rows[0]["StdWrkHrs"].ToString() == "")
            {
                txtStdWorkingHrs.Text = "0";
            }
            else
            {
                txtStdWorkingHrs.Text = DT_Check.Rows[0]["StdWrkHrs"].ToString();
            }

            if (DT_Check.Rows[0]["Handicapped"].ToString().ToUpper() == "Yes".ToUpper().ToString())
            {
                rbtnPhysically.SelectedValue = "1";
            }
            else
            {
                rbtnPhysically.SelectedValue = "2";
            }

            rbtnPhysically_SelectedIndexChanged(sender, e);
            txtPhyReason.Text = DT_Check.Rows[0]["Handicapped_Reason"].ToString();
            ddlBloodGrp.SelectedValue = DT_Check.Rows[0]["BloodGroup"].ToString();

            txtRecruitThrg.SelectedValue = DT_Check.Rows[0]["RecuritmentThro"].ToString();
            txtRecruitThrg_SelectedIndexChanged(sender, e);

            txtExistingEmpNo.Text = DT_Check.Rows[0]["ExistingEmpNo"].ToString();
            txtExistingEmpName.Text = DT_Check.Rows[0]["ExistingEmpName"].ToString();
            txtRecruitMobile.Text = DT_Check.Rows[0]["RecutersMob"].ToString();

            txtRefType.SelectedValue = DT_Check.Rows[0]["ReferalType"].ToString();
            txtRefType_SelectedIndexChanged(sender, e);

            txtAgentName.SelectedValue = DT_Check.Rows[0]["AgentName"].ToString();

            txtRefParentsName.Text = DT_Check.Rows[0]["RefParentsName"].ToString();
            txtRefMobileNo.Text = DT_Check.Rows[0]["RefParentsMobile"].ToString();

            txtCommissionAmt.Text = DT_Check.Rows[0]["CommAmount"].ToString();

            ddlGrade.SelectedValue = DT_Check.Rows[0]["Grade"].ToString();


            txtLeaveFrom.Text = DT_Check.Rows[0]["LeaveFrom"].ToString();
            txtLeaveTo.Text = DT_Check.Rows[0]["LeaveTo"].ToString();
            txtFestival1.Text = DT_Check.Rows[0]["Festival1"].ToString();
            txtLeaveDays1.Text = DT_Check.Rows[0]["Days1"].ToString();
            txtLeaveFrom2.Text = DT_Check.Rows[0]["LeaveFrom2"].ToString();
            txtLeaveTo2.Text = DT_Check.Rows[0]["LeaveTo2"].ToString();
            txtFestival2.Text = DT_Check.Rows[0]["Festival2"].ToString();
            txtLeaveDays2.Text = DT_Check.Rows[0]["Days2"].ToString();

            txtNominee.Text = DT_Check.Rows[0]["Nominee"].ToString();
            txtNomineeRelation.Text = DT_Check.Rows[0]["NomineeRelation"].ToString();
            txtFatherName.Text = DT_Check.Rows[0]["FamilyDetails"].ToString();
            txtParentMob1.Text = DT_Check.Rows[0]["ParentsPhone"].ToString();
            txtMotherName.Text = DT_Check.Rows[0]["MotnerName"].ToString();
            txtParentMob2.Text = DT_Check.Rows[0]["parentsMobile"].ToString();
            txtGuardianName.Text = DT_Check.Rows[0]["GuardianName"].ToString();
            txtGuardianMobile.Text = DT_Check.Rows[0]["GuardianMobile"].ToString();
            txtPermAddr.Text = DT_Check.Rows[0]["Address1"].ToString();
            txtPermTaluk.SelectedValue = DT_Check.Rows[0]["Taluk_Perm"].ToString();
            txtPermDist.SelectedValue = DT_Check.Rows[0]["Permanent_Dist"].ToString();
            ddlState.SelectedValue = DT_Check.Rows[0]["StateName"].ToString();

            if (DT_Check.Rows[0]["OtherState"].ToString() == "Yes")
            {
                chkOtherState.Checked = true;
            }
            else
            {
                chkOtherState.Checked = false;
            }

            txtTempTaluk.SelectedValue = DT_Check.Rows[0]["Taluk_Present"].ToString();
            txtTempDist.SelectedValue = DT_Check.Rows[0]["Present_Dist"].ToString();
            txtTempAddr.Text = DT_Check.Rows[0]["Address2"].ToString();

            if (DT_Check.Rows[0]["SamepresentAddress"].ToString() == "1")
            {
                chkSame.Checked = true;
            }
            else
            {
                chkSame.Checked = false;
            }

            txtIdenMark1.Text = DT_Check.Rows[0]["IDMark1"].ToString();
            txtIdenMark2.Text = DT_Check.Rows[0]["IDMark2"].ToString();

            DataTable dt = new DataTable();
            SSQL = "Select DocType,DocNo,'' as imgurl from Employee_Doc_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And ExistingCode='" + txtExistingCode.Text + "'";
            dt = objdata.RptEmployeeMultipleDetails(SSQL);

            for (int k = 0; k < dt.Rows.Count; k++)
            {

                string token_Name = "";

                string Doc_Folder = "";

                if (SessionLcode.ToUpper() == "UNIT I") { UNIT_Folder = PhotoDet1 + "/UNIT_I/"; }


                if (dt.Rows[k]["DocType"].ToString() == "Adhar Card")
                {
                    Doc_Folder = "/ID_Proof/A_Copy/";
                    token_Name = "A_" + txtTokenID.Text;
                }
                if (dt.Rows[k]["DocType"].ToString() == "Bank Pass Book")
                {
                    Doc_Folder = "/ID_Proof/B_PB_Copy/";
                    token_Name = "A_" + txtTokenID.Text;
                }
                if (dt.Rows[k]["DocType"].ToString() == "Others")
                {
                    Doc_Folder = "/ID_Proof/Other_Copy/";
                    token_Name = "A_" + txtTokenID.Text;
                }
                if (dt.Rows[k]["DocType"].ToString() == "Voter Card")
                {
                    Doc_Folder = "/ID_Proof/V_Copy/";
                    token_Name = "V_" + txtTokenID.Text;
                }
                if (dt.Rows[k]["DocType"].ToString() == "Ration Card")
                {
                    Doc_Folder = "/ID_Proof/R_Copy/";
                    token_Name = "R_" + txtTokenID.Text;
                }
                if (dt.Rows[k]["DocType"].ToString() == "Pan Card")
                {
                    Doc_Folder = "/ID_Proof/P_Copy/";
                    token_Name = "P_" + txtTokenID.Text;
                }
                if (dt.Rows[k]["DocType"].ToString() == "Driving Licence")
                {
                    Doc_Folder = "/ID_Proof/DL_Copy/";
                    token_Name = "DL_" + txtTokenID.Text;
                }
                if (dt.Rows[k]["DocType"].ToString() == "Smart Card")
                {
                    Doc_Folder = "/ID_Proof/SC_Copy/";
                    token_Name = "SC_" + txtTokenID.Text;
                }


                string imgurl_Final = "";
                path_3 = UNIT_Folder + Doc_Folder;
                string Exten;
                Exten = ".jpg";
                string impath = path_3 + token_Name + Exten;

                if (File.Exists(impath))
                {

                    imgurl_Final = impath;
                    dt.Rows[k]["imgurl"] = "Handler.ashx?f=" + imgurl_Final + ""; //imgurl_Final;
                }
                else
                {
                    imgurl_Final = "~/assets/images/No_Image.jpg";
                    dt.Rows[k]["imgurl"] = imgurl_Final;
                }
            }

            ViewState["ItemTable"] = dt;
            Repeater1.DataSource = dt;
            Repeater1.DataBind();

            DataTable dt1 = new DataTable();
            SSQL = "Select *from Adolcent_Emp_Det where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And MachineID='" + txtMachineID.Text + "'";
            dt1 = objdata.RptEmployeeMultipleDetails(SSQL);
            ViewState["CertTable"] = dt1;
            Repeater2.DataSource = dt1;
            Repeater2.DataBind();

            if (txtRecruitThrg.SelectedValue != "-Select-")
            {
                txtRecruitmentName.SelectedValue = DT_Check.Rows[0]["RecuriterName"].ToString();
                txtRecruitmentName_SelectedIndexChanged(sender, e);
                ddlUnit.SelectedValue = DT_Check.Rows[0]["RecuritmentUnit"].ToString().Trim();
            }
        }
        else
        {
            Clear_All_Field();
        }
    }

    private void Initial_Data_Referesh()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("DocType", typeof(string)));
        dt.Columns.Add(new DataColumn("DocNo", typeof(string)));
        dt.Columns.Add(new DataColumn("imgurl", typeof(string)));


        Repeater1.DataSource = dt;
        Repeater1.DataBind();
        ViewState["ItemTable"] = Repeater1.DataSource;

        //dt = Repeater1.DataSource;
    }

    private void Initial_Data1_Referesh()
    {
        DataTable dt = new DataTable();
        dt.Columns.Add(new DataColumn("Certificate_No", typeof(string)));
        dt.Columns.Add(new DataColumn("Certificate_Date_Str", typeof(string)));
        dt.Columns.Add(new DataColumn("Status", typeof(string)));
        dt.Columns.Add(new DataColumn("Next_Due_Date_Str", typeof(string)));
        dt.Columns.Add(new DataColumn("Certificate_Type", typeof(string)));
        dt.Columns.Add(new DataColumn("Remarks", typeof(string)));


        Repeater2.DataSource = dt;
        Repeater2.DataBind();
        ViewState["CertTable"] = Repeater2.DataSource;

        //dt = Repeater1.DataSource;
    }

    private void Load_OLD_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }

    private void Load_OLD_data1()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["CertTable"];
        Repeater2.DataSource = dt;
        Repeater2.DataBind();
    }
    private void Load_PFCode()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlPFCode.Items.Clear();
        query = "Select *from Location_Mst where LocCode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlPFCode.DataSource = dtdsupp;

        //DataRow dr = dtdsupp.NewRow();
        //dr["DeptCode"] = "0";
        //dr["DeptName"] = "-Select-";
        //dtdsupp.Rows.InsertAt(dr, 0);

        ddlPFCode.DataTextField = "PF_Code";
        ddlPFCode.DataValueField = "PF_Code";
        ddlPFCode.DataBind();
    }

    private void Load_ESICode()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlESICode.Items.Clear();
        query = "Select *from ESICode_Mst where LocCode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlESICode.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["ESICode"] = "0";
        dr["ESICode"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlESICode.DataTextField = "ESICode";
        ddlESICode.DataValueField = "ESICode";
        ddlESICode.DataBind();
    }

    private void Load_Grade()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlGrade.Items.Clear();
        query = "Select *from MstGrade";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlGrade.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["GradeName"] = "-Select-";
        dr["GradeName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlGrade.DataTextField = "GradeName";
        ddlGrade.DataValueField = "GradeName";
        ddlGrade.DataBind();
    }


    private void Load_Department()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlDepartment.Items.Clear();
        query = "Select *from Department_Mst";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlDepartment.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["DeptCode"] = "0";
        dr["DeptName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlDepartment.DataTextField = "DeptName";
        ddlDepartment.DataValueField = "DeptCode";
        ddlDepartment.DataBind();
    }

    private void Load_Community()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlCommunity.Items.Clear();
        query = "Select *from MstCommunity Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlCommunity.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["Community"] = "-Select-";
        dr["Community"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlCommunity.DataTextField = "Community";
        ddlCommunity.DataValueField = "Community";
        ddlCommunity.DataBind();
    }

    private void Load_Qualification()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtQulification.Items.Clear();
        query = "Select *from MstQualification";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtQulification.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["Qualification"] = "-Select-";
        dr["Qualification"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtQulification.DataTextField = "Qualification";
        txtQulification.DataValueField = "Qualification";
        txtQulification.DataBind();
    }

    private void Load_HostelExp()
    {

    }

    private void Load_Designation()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlDesignation.Items.Clear();
        query = "Select *from Designation_Mst where DeptName='" + ddlDepartment.SelectedItem.Text + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlDesignation.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["DesignName"] = "-Select-";
        dr["DesignName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlDesignation.DataTextField = "DesignName";
        ddlDesignation.DataValueField = "DesignName";
        ddlDesignation.DataBind();
    }

    private void Load_Route()
    {

        //string query = "";
        //DataTable dtdsupp = new DataTable();
        //txtVillage.Items.Clear();
        //query = "Select Distinct RouteName from RouteMst where Type='" + ddlVehicleType.SelectedItem.Text + "'";
        //dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        //txtVillage.DataSource = dtdsupp;
        //DataRow dr = dtdsupp.NewRow();
        //dr["RouteName"] = "-Select-";
        //dr["RouteName"] = "-Select-";
        //dtdsupp.Rows.InsertAt(dr, 0);
        //txtVillage.DataTextField = "RouteName";
        //txtVillage.DataValueField = "RouteName";
        //txtVillage.DataBind();

    }

    private void Load_BusNo()
    {

        //string query = "";
        //DataTable dtdsupp = new DataTable();
        //txtBusNo.Items.Clear();
        //query = "Select Distinct BusNo from RouteMst where Type='" + ddlVehicleType.SelectedItem.Text + "' And RouteName='" + txtVillage.SelectedItem.Text + "'";
        //dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        //txtBusNo.DataSource = dtdsupp;
        //DataRow dr = dtdsupp.NewRow();
        //dr["BusNo"] = "-Select-";
        //dr["BusNo"] = "-Select-";
        //dtdsupp.Rows.InsertAt(dr, 0);
        //txtBusNo.DataTextField = "BusNo";
        //txtBusNo.DataValueField = "BusNo";
        //txtBusNo.DataBind();

    }

    private void Load_WagesType()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlWagesType.Items.Clear();
        query = "Select *from MstEmployeeType";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlWagesType.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpType"] = "-Select-";
        dr["EmpType"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlWagesType.DataTextField = "EmpType";
        ddlWagesType.DataValueField = "EmpType";
        ddlWagesType.DataBind();
    }

    protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Designation();
    }

    private void Load_Bank()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        DataTable DT = new DataTable();
        ddlBankName.Items.Clear();
        query = "Select *from MstBank";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlBankName.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["BankName"] = "-Select-";
        dr["BankName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlBankName.DataTextField = "BankName";
        ddlBankName.DataValueField = "BankName";
        ddlBankName.DataBind();

        query = "Select * from MstBank where Default_Bank='Yes'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            ddlBankName.SelectedValue = DT.Rows[0]["BankName"].ToString();
            txtIFSC.Text = DT.Rows[0]["IFSCCode"].ToString();
            txtBranch.Text = DT.Rows[0]["Branch"].ToString();
        }
        else
        {
            ddlBankName.SelectedValue = "-Select-";
            txtIFSC.Text = "";
            txtBranch.Text = "";
        }
    }

    private void Load_Unit()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        DataTable DT = new DataTable();
        ddlUnit.Items.Clear();
        query = "Select LocCode from Location_Mst where CompCode='" + SessionCcode + "'";

        //query = query + " And LocCode!='" + SessionLcode + "'";

        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlUnit.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["LocCode"] = "-Select-";
        dr["LocCode"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlUnit.DataTextField = "LocCode";
        ddlUnit.DataValueField = "LocCode";
        ddlUnit.DataBind();
    }

    private void Load_Recruitment()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        DataTable DT = new DataTable();
        txtRecruitmentName.Items.Clear();
        if (txtRecruitThrg.SelectedItem.Text == "Agent")
        {
            query = "Select AgentName as ROName from MstAgent";
        }
        else
        {
            query = "Select ROName from MstRecruitOfficer";
        }

        //query = query + " And LocCode!='" + SessionLcode + "'";

        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtRecruitmentName.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["ROName"] = "-Select-";
        dr["ROName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtRecruitmentName.DataTextField = "ROName";
        txtRecruitmentName.DataValueField = "ROName";
        txtRecruitmentName.DataBind();

    }

    private void Load_AgentName()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        DataTable DT = new DataTable();
        txtAgentName.Items.Clear();

        query = "Select AgentName as ROName from MstAgent";

        //query = query + " And LocCode!='" + SessionLcode + "'";

        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtAgentName.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["ROName"] = "-Select-";
        dr["ROName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtAgentName.DataTextField = "ROName";
        txtAgentName.DataValueField = "ROName";
        txtAgentName.DataBind();

    }

    private void Load_WorkingUnit()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        DataTable DT = new DataTable();
        ddlWorkingUnit.Items.Clear();
        query = "Select LocCode from Location_Mst where CompCode='" + SessionCcode + "'";

        //query = query + " And LocCode!='" + SessionLcode + "'";

        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlWorkingUnit.DataSource = dtdsupp;
        ddlWorkingUnit.DataTextField = "LocCode";
        ddlWorkingUnit.DataValueField = "LocCode";
        ddlWorkingUnit.DataBind();


        ddlSalaryUnit.Items.Clear();
        ddlSalaryUnit.DataSource = dtdsupp;
        ddlSalaryUnit.DataTextField = "LocCode";
        ddlSalaryUnit.DataValueField = "LocCode";
        ddlSalaryUnit.DataBind();
    }

    private void Load_Taluk()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        DataTable DT = new DataTable();
        txtPermTaluk.Items.Clear();
        query = "Select *from MstTaluk";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtPermTaluk.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["Taluk"] = "-Select-";
        dr["Taluk"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtPermTaluk.DataTextField = "Taluk";
        txtPermTaluk.DataValueField = "Taluk";
        txtPermTaluk.DataBind();

        txtTempTaluk.Items.Clear();
        txtTempTaluk.DataSource = dtdsupp;
        txtTempTaluk.DataTextField = "Taluk";
        txtTempTaluk.DataValueField = "Taluk";
        txtTempTaluk.DataBind();
    }

    private void Load_District()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        DataTable DT = new DataTable();
        txtPermDist.Items.Clear();
        query = "Select *from MstDistrict";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtPermDist.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["District"] = "-Select-";
        dr["District"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtPermDist.DataTextField = "District";
        txtPermDist.DataValueField = "District";
        txtPermDist.DataBind();

        txtTempDist.Items.Clear();
        txtTempDist.DataSource = dtdsupp;
        txtTempDist.DataTextField = "District";
        txtTempDist.DataValueField = "District";
        txtTempDist.DataBind();
    }
    private void Load_State()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        DataTable DT = new DataTable();
        ddlState.Items.Clear();
        query = "Select *from MstState";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlState.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["State"] = "-Select-";
        dr["State"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlState.DataTextField = "State";
        ddlState.DataValueField = "State";
        ddlState.DataBind();

    }

    private void Load_Division()
    {
        //string query = "";
        //DataTable dtdsupp = new DataTable();
        //DataTable DT = new DataTable();
        //ddlDivision.Items.Clear();
        //query = "Select *from Division_Master where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        //dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        //ddlDivision.DataSource = dtdsupp;
        //DataRow dr = dtdsupp.NewRow();
        //dr["Division"] = "-Select-";
        //dr["Division"] = "-Select-";
        //dtdsupp.Rows.InsertAt(dr, 0);
        //ddlDivision.DataTextField = "Division";
        //ddlDivision.DataValueField = "Division";
        //ddlDivision.DataBind();

    }

    protected void RdbPFEligible_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (RdbPFEligible.SelectedValue == "1")
        {
            txtPFNo.Enabled = true;
            txtPFDate.Enabled = true;
        }
        else
        {
            txtPFNo.Enabled = false;
            txtPFDate.Enabled = false;
        }
    }
    protected void RdbESIEligible_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (RdbESIEligible.SelectedValue == "1")
        {
            txtESINo.Enabled = true;
            txtESIDate.Enabled = true;
        }
        else
        {
            txtESINo.Enabled = false;
            txtESIDate.Enabled = false;
        }
    }
    protected void rbtnSalaryThrough_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (rbtnSalaryThrough.SelectedValue == "1")
        {
            ddlBankName.Enabled = false;
            txtAccNo.Enabled = false;
        }
        else
        {
            ddlBankName.Enabled = true; ;
            txtAccNo.Enabled = true;
        }
    }
    public void AgeCalc()
    {
        try
        {
            if (txtDOB.Text != "")
            {
                int dt = System.DateTime.Now.Year;
                string date1Day = this.txtDOB.Text.Remove(2);
                string date1Month = this.txtDOB.Text.Substring(3, 2);
                string date1Year = this.txtDOB.Text.Substring(6);
                int date2year = Convert.ToInt32(date1Year.ToString());
                int datediff = dt - date2year;

                if (datediff >= 0)
                {
                    string age = Convert.ToString(datediff);
                    txtAge.Text = age.ToString();

                }
                else
                {
                    bool ErrFlag = false;
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Give Valid Date');", true);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Give Valid Date');", true);

                    ErrFlag = true;
                }
            }
            else
            {
                txtAge.Text = "";
            }
        }
        catch (Exception ex)
        {

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Give Valid Date');", true);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Give Valid Date');", true);

        }

    }
    protected void txtDOB_TextChanged(object sender, EventArgs e)
    {
        AgeCalc();
    }
    protected void txtRecruitThrg_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlUnit.SelectedValue = "-Select-";
        txtRecruitMobile.Text = "";
        if (txtRecruitThrg.SelectedItem.Text == "Recruitment Officer")
        {
            lblRecruit.Visible = true;
            lblAgent.Visible = false;
            txtRecruitmentName.Enabled = true;
            txtRecruitMobile.Enabled = true;
            ddlUnit.Enabled = true;
            txtExistingEmpNo.Enabled = false;
            txtExistingEmpName.Enabled = false;
            txtRefParentsName.Enabled = false;
            txtRefMobileNo.Enabled = false;
        }
        else if (txtRecruitThrg.SelectedItem.Text == "Agent")
        {
            lblAgent.Visible = true;
            lblRecruit.Visible = false;
            txtRecruitmentName.Enabled = true;
            txtRecruitMobile.Enabled = true;
            ddlUnit.Enabled = true;
            txtExistingEmpNo.Enabled = false;
            txtExistingEmpName.Enabled = false;
            txtRefParentsName.Enabled = false;
            txtRefMobileNo.Enabled = false;
        }
        else if (txtRecruitThrg.SelectedItem.Text == "Existing Employee")
        {
            lblAgent.Visible = false;
            lblRecruit.Visible = true;
            txtRecruitmentName.Enabled = false;
            txtRecruitMobile.Enabled = false;
            ddlUnit.Enabled = true;
            txtExistingEmpNo.Enabled = true;
            txtExistingEmpName.Enabled = true;
            txtRefParentsName.Enabled = false;
            txtRefMobileNo.Enabled = false;
        }
        else if (txtRecruitThrg.SelectedItem.Text == "Parents")
        {
            lblAgent.Visible = false;
            lblRecruit.Visible = true;
            txtRecruitmentName.Enabled = false;
            txtRecruitMobile.Enabled = false;
            ddlUnit.Enabled = true;
            txtExistingEmpNo.Enabled = false;
            txtExistingEmpName.Enabled = false;
            txtRefParentsName.Enabled = true;
            txtRefMobileNo.Enabled = true;
        }
        else if (txtRecruitThrg.SelectedItem.Text == "Transfer")
        {
            lblAgent.Visible = false;
            lblRecruit.Visible = true;
            txtRecruitmentName.Enabled = false;
            txtRecruitMobile.Enabled = false;
            ddlUnit.Enabled = true;
            txtExistingEmpNo.Enabled = false;
            txtExistingEmpName.Enabled = false;
            txtRefParentsName.Enabled = false;
            txtRefMobileNo.Enabled = false;
        }
        else
        {
            lblRecruit.Visible = true;
            lblAgent.Visible = false;
            txtRecruitmentName.Enabled = false;
            txtRecruitMobile.Enabled = false;
            ddlUnit.Enabled = false;
            txtExistingEmpNo.Enabled = false;
            txtExistingEmpName.Enabled = false;
            txtRefParentsName.Enabled = false;
            txtRefMobileNo.Enabled = false;
        }

        Load_Recruitment();
    }

    protected void btnEmpSave_Click(object sender, EventArgs e)
    {
        string query = "";
        string PFNO = "";
        DataTable DT_Check = new DataTable();
        string SaveMode = "Insert";

        bool ErrFlag = false;
        string Employee_Save_Table = "";



        if (txtMachineID.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the MachineID..!');", true);
        }

        if (txtExistingCode.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Existing Code..!');", true);
        }


        if (ddlCategory.SelectedValue == "0")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the Category..!');", true);
        }

        if (ddlSubCategory.SelectedValue == "0")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the Sub Category..!');", true);
        }


        if (ddlShift.SelectedValue == "0")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the Shift..!');", true);
        }

        if (txtFirstName.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the FirstName..!');", true);
        }

        if (txtLastName.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the LastName..!');", true);
        }

        if (txtDOB.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter Date Of Birth..!');", true);
        }


        if (ddlGender.SelectedValue == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the Gender..!');", true);
        }

        if (txtDOJ.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter Date Of Joining..!');", true);
        }

        if (ddlDepartment.SelectedValue == "0")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the Department..!');", true);
        }

        if (ddlDesignation.SelectedValue == "0")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the Designation..!');", true);
        }


        if (ddlOTEligible.SelectedValue == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the OT Eligible..!');", true);
        }


        if (ddlWagesType.SelectedValue == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the Wages..!');", true);
        }

        //if (ddlWeekOff.SelectedValue == "0")
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the Week OFF..!');", true);
        //}

        if (RdbPFEligible.SelectedValue == "1")
        {
            if (txtPFNo.Text == "")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the PF No..!');", true);
            }
            if (txtPFDate.Text == "")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the PF Date..!');", true);
            }
        }

        if (RdbESIEligible.SelectedValue == "1")
        {
            if (txtESINo.Text == "")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the ESI No..!');", true);
            }
            if (txtESIDate.Text == "")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the ESI Date..!');", true);
            }
        }

        //if (ddlShiftType.SelectedItem.Value == "0" || ddlShiftType.SelectedItem.Text == "Select")
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page,this.GetType(),"script", "SaveMsgAlert('Please');", true);
        //}
        //if(ddl)

        //if (ddlMartialStatus.SelectedValue == "0")
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the Martial Status..!');", true);
        //}

        //if (txtPermAddr.Text == "")
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Permanent Address..!');", true);
        //}

        //if (txtPermTaluk.SelectedValue == "-Select-")
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the Permanent Taluk..!');", true);
        //}

        //if (txtPermDist.SelectedValue == "-Select-")
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the Permanent District..!');", true);
        //}

        //if (ddlState.SelectedValue == "-Select-")
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the State..!');", true);
        //}

        //if (txtTempTaluk.SelectedValue == "-Select-")
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the Temp Taluk..!');", true);
        //}
        //if (txtTempDist.SelectedValue == "-Select-")
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the Temp District..!');", true);
        //}

        //if (txtTempAddr.Text == "")
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Temp Address..!');", true);
        //}

        if (!ErrFlag)
        {
            SSQL = "Select * from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And MachineID='" + txtMachineID.Text + "'";
            DT_Check = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT_Check.Rows.Count != 0)
            {
                Employee_Save_Table = "Employee_Mst";
            }
            else
            {
                Employee_Save_Table = "Employee_Mst_New_Emp";
            }
        }

        if (!ErrFlag)
        {

            if (btnEmpSave.Text != "Update")
            {
                //Check Token No
                SSQL = "";
                SSQL = "Select * from " + Employee_Save_Table + " Where CompCode='" + SessionCcode + "'";
                SSQL = SSQL + " And LocCode='" + SessionLcode + "'";
                SSQL = SSQL + " And MachineID = '" + txtMachineID.Text + "' And ExistingCode = '" + txtExistingCode.Text + "'";
                DT_Check = objdata.RptEmployeeMultipleDetails(SSQL);
                if (DT_Check.Rows.Count != 0)
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This TOKEN NO Already Assign to Another Person...');", true);
                    txtExistingCode.Focus();
                }
            }
        }

        if (!ErrFlag)
        {
            if (rbtnSalaryThrough.SelectedValue == "2")
            {
                //Check Bank Account No
                SSQL = "";
                SSQL = "Select * from " + Employee_Save_Table + " Where CompCode='" + SessionCcode + "'";
                SSQL = SSQL + " And LocCode='" + SessionLcode + "'";
                SSQL = SSQL + " And MachineID != '" + txtMachineID.Text + "' And ExistingCode != '" + txtExistingCode.Text + "'";
                SSQL = SSQL + " And AccountNo='" + txtAccNo.Text + "'";
                DT_Check = objdata.RptEmployeeMultipleDetails(SSQL);
                if (DT_Check.Rows.Count != 0)
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This ACCOUNT NO Already Assign to Another Person...');", true);
                    txtAccNo.Focus();
                }
            }
        }

        if (!ErrFlag)
        {
            if (RdbPFEligible.SelectedValue == "1")
            {
                PFNO = ddlPFCode.SelectedItem.Text + "/" + txtPFNo.Text;

                //Check PF NO

                SSQL = "";
                SSQL = "Select * from " + Employee_Save_Table + " Where CompCode='" + SessionCcode + "'";
                SSQL = SSQL + " And LocCode='" + SessionLcode + "'";
                SSQL = SSQL + " And MachineID != '" + txtMachineID.Text + "' And ExistingCode != '" + txtExistingCode.Text + "'";
                SSQL = SSQL + " And PFNo_New='" + txtPFNo.Text + "'";
                DT_Check = objdata.RptEmployeeMultipleDetails(SSQL);
                if (DT_Check.Rows.Count != 0)
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This PF NO Already Assign to Another Person...');", true);
                    txtPFNo.Focus();
                }
            }
        }

        if (!ErrFlag)
        {
            if (RdbESIEligible.SelectedValue == "1")
            {

                //Check ESI NO

                SSQL = "";
                SSQL = "Select * from " + Employee_Save_Table + " Where CompCode='" + SessionCcode + "'";
                SSQL = SSQL + " And LocCode='" + SessionLcode + "'";
                SSQL = SSQL + " And MachineID != '" + txtMachineID.Text + "' And ExistingCode != '" + txtExistingCode.Text + "'";
                SSQL = SSQL + " And ESINo='" + txtESINo.Text + "'";
                DT_Check = objdata.RptEmployeeMultipleDetails(SSQL);
                if (DT_Check.Rows.Count != 0)
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This ESI NO Already Assign to Another Person...');", true);
                    txtPFNo.Focus();
                }
            }
        }


        if (!ErrFlag)
        {
            //Machine ID Encrypt

            string StrMachine_ID_Encrypt = "";
            StrMachine_ID_Encrypt = UTF8Encryption(txtMachineID.Text);

            //check State 

            string Other_State = "";
            if (chkOtherState.Checked == true)
            {
                Other_State = "Yes";
            }
            else
            {
                Other_State = "No";
            }
            string Same_ASPermanent = "";
            if (chkSame.Checked == true)
            {
                Same_ASPermanent = "1";
            }
            else
            {
                Same_ASPermanent = "0";
            }

            //Adolescent

            string Adolescent_Val = "";
            if (chkAdolescent.Checked == true)
            {
                Adolescent_Val = "1";
            }
            else
            {
                Adolescent_Val = "0";
            }

            string Adolescent_Complete_Status = "";
            string Adolescent_Complete_Date = "";
            if (chkAdolescent.Checked == true)
            {
                if (chkAge18Complete.Checked == true)
                {
                    Adolescent_Complete_Status = "1";
                    Adolescent_Complete_Date = txtAge18Comp_Date.Text;
                }
                else
                {
                    Adolescent_Complete_Status = "";
                    Adolescent_Complete_Date = "";
                }
            }
            else
            {
                Adolescent_Complete_Status = "";
                Adolescent_Complete_Date = "";
            }

            string ExemptedStaff = "";
            if (chkExment.Checked == true)
            {
                ExemptedStaff = "1";
            }
            else
            {
                ExemptedStaff = "0";
            }

            string UNIT_Folder = "";
            string Doc_Folder = "";

            DataTable DT_Photo = new DataTable();
            string SS = "Select *from Photo_Path_Det";
            DT_Photo = objdata.RptEmployeeMultipleDetails(SS);

            string PhotoDet = "";
            if (DT_Photo.Rows.Count != 0)
            {
                PhotoDet = DT_Photo.Rows[0]["Photo_Path"].ToString();
            }

            //if (SessionLcode.ToUpper() == "UNIT I") { UNIT_Folder = "Emp_Scan/UNIT_I/"; }
            //if (SessionLcode.ToUpper() == "UNIT II") { UNIT_Folder = "Emp_Scan/UNIT_II/"; }
            //if (SessionLcode.ToUpper() == "UNIT III") { UNIT_Folder = "Emp_Scan/UNIT_III/"; }
            //if (SessionLcode.ToUpper() == "UNIT IV") { UNIT_Folder = "//SABA-PC/To Share/Narmatha/Emp_Scan/UNIT_IV/"; } //UNIT_Folder = "Emp_Scan/UNIT_IV/"; }

            if (SessionLcode.ToUpper() == "UNIT I") { UNIT_Folder = PhotoDet + "/UNIT_I/"; }
            if (SessionLcode.ToUpper() == "UNIT II") { UNIT_Folder = PhotoDet + "/UNIT_II/"; }
            if (SessionLcode.ToUpper() == "UNIT III") { UNIT_Folder = PhotoDet + "/UNIT_III/"; }
            if (SessionLcode.ToUpper() == "UNIT IV") { UNIT_Folder = PhotoDet + "/UNIT_IV/"; } //UNIT_Folder = "Emp_Scan/UNIT_IV/"; }


            string token_Name = txtTokenID.Text;

            string path_1 = UNIT_Folder;

            if (FileUpload1.HasFile)
            {
                Doc_Folder = "Photos/";
                string FileName = Path.GetFileName(FileUpload1.PostedFile.FileName);
                string Exten = Path.GetExtension(FileUpload1.PostedFile.FileName);

                //FileUpload1.SaveAs(Server.MapPath("~/" + path_1 + Doc_Folder + token_Name + Exten));

                FileUpload1.SaveAs((path_1 + Doc_Folder + token_Name + Exten));
            }

            if (FileUpload2.HasFile)
            {
                Doc_Folder = "ParentPhoto/";
                string FileName = Path.GetFileName(FileUpload2.PostedFile.FileName);
                string Exten = Path.GetExtension(FileUpload2.PostedFile.FileName);

                //FileUpload2.SaveAs(Server.MapPath("~/" + path_1 + Doc_Folder + token_Name + Exten));

                FileUpload2.SaveAs((path_1 + Doc_Folder + token_Name + Exten));
            }


            SSQL = "";
            SSQL = "Delete from " + Employee_Save_Table + " Where CompCode='" + SessionCcode + "'";
            SSQL = SSQL + " And LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And EmpNo='" + txtMachineID.Text + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "";
            SSQL = "Insert into " + Employee_Save_Table + " (PFS,CompCode,LocCode,TypeName,EmpPrefix,EmpNo";
            SSQL = SSQL + ",ExistingCode,MachineID,MachineID_Encrypt,FirstName,LastName,MiddleInitial";
            SSQL = SSQL + ",Gender,BirthDate,Age,MaritalStatus,ShiftType,CatName,SubCatName";
            SSQL = SSQL + " ,DOJ,DeptCode,DeptName,Designation,Qualification,EmployeeMobile";
            SSQL = SSQL + ",OTEligible,Wages,Eligible_PF,PFNo,PFDOJ,Eligible_ESI,ESINo,ESIDOJ";
            SSQL = SSQL + ",UAN,PF_Code,ESICode,RoomNo,RoomCode,RoomNameID,RoomName,IsActive,EmpStatus";
            SSQL = SSQL + ",DOR,Reason,Emp_Permn_Date,Certificate,WeekOff,Salary_Through";
            SSQL = SSQL + ",BankName,IFSC_Code,BranchCode,AccountNo";
            SSQL = SSQL + ",BaseSalary,VPF,Alllowance1,Alllowance2,Deduction1,Deduction2";
            SSQL = SSQL + ",OT_Salary,Nationality,Religion,Height,Weight,StdWrkHrs,Calculate_Work_Hours";
            SSQL = SSQL + ",Handicapped,Handicapped_Reason,BloodGroup,RecuritmentThro";
            SSQL = SSQL + ",RecuritmentUnit,RecuriterName,RecutersMob,ExistingEmpNo,ExistingEmpName";
            SSQL = SSQL + ",WorkingUnit,SalaryUnit,Grade,Nominee,FamilyDetails,ParentsPhone";
            SSQL = SSQL + ",MotnerName,parentsMobile,GuardianName,GuardianMobile";
            SSQL = SSQL + ",Address1,Taluk_Perm,Permanent_Dist,StateName,OtherState";
            SSQL = SSQL + ",SamepresentAddress,Taluk_Present,Present_Dist,Address2";
            SSQL = SSQL + ",IDMark1,IDMark2,RefParentsName,RefParentsMobile,LeaveFrom,LeaveTo,EmpLevel";
            SSQL = SSQL + ",Training_Status";
            SSQL = SSQL + ",Adolescent,Adolescent_Status,Adolescent_Completed,Adolescent_Comple_Date";
            SSQL = SSQL + ",Created_By,Created_Date";
            SSQL = SSQL + ",ReferalType,AgentName,ExemptedStaff,FormIObtained,Community,NomineeRelation";
            SSQL = SSQL + ",Festival1,Days1,LeaveFrom2,LeaveTo2,Festival2,Days2,CommAmount,WorkingType";
            SSQL = SSQL + ",PFNo_New,HourSalary,FixedSalary,EligibleMasthiri_Inc,ShiftType_New,Hindi_Category";
            SSQL = SSQL + ",EligibleCommissoin,Unit_Type,BusNo,BusRoute,EligibleDayIncentive,Salary2) Values ( ";
            SSQL = SSQL + "'" + txtVPF.Text + "','" + SessionCcode + "','" + SessionLcode + "','REGULAR','A',";
            SSQL = SSQL + "'" + txtTokenID.Text + "','" + txtExistingCode.Text + "','" + txtMachineID.Text + "','" + StrMachine_ID_Encrypt + "',";
            SSQL = SSQL + "'" + txtFirstName.Text + "','" + txtLastName.Text + "','" + txtLastName.Text + "',";
            SSQL = SSQL + "'" + ddlGender.SelectedItem.Text + "','" + Convert.ToDateTime(txtDOB.Text).ToString("yyyy/MM/dd") + "',";
            SSQL = SSQL + "'" + txtAge.Text + "','" + ddlMartialStatus.SelectedItem.Text + "','" + ddlShift.SelectedItem.Text + "',";
            SSQL = SSQL + "'" + ddlCategory.SelectedItem.Text + "','" + ddlSubCategory.SelectedItem.Text + "',";
            SSQL = SSQL + "'" + Convert.ToDateTime(txtDOJ.Text).ToString("yyyy/MM/dd") + "','" + ddlDepartment.SelectedValue + "','" + ddlDepartment.SelectedItem.Text + "','" + ddlDesignation.SelectedItem.Text + "',";
            SSQL = SSQL + "'" + txtQulification.SelectedItem.Text + "','+91-" + txtEmpMobileNo.Text + "','" + ddlOTEligible.SelectedValue + "',";
            SSQL = SSQL + "'" + ddlWagesType.SelectedItem.Text + "','" + RdbPFEligible.SelectedValue + "','" + txtPFNo.Text + "',";
            SSQL = SSQL + "'" + txtPFDate.Text + "','" + RdbESIEligible.SelectedValue + "','" + txtESINo.Text + "','" + txtESIDate.Text + "',";
            SSQL = SSQL + "'" + txtUAN.Text + "','" + ddlPFCode.SelectedItem.Text + "','" + ddlESICode.SelectedItem.Text + "',";
            SSQL = SSQL + "'" + txtHostelRoom.SelectedItem.Text + "','" + txtHostelRoom.SelectedValue + "','" + ddlRoomName.SelectedValue + "','" + ddlRoomName.SelectedItem.Text + "',";
            SSQL = SSQL + "'" + dbtnActive.SelectedItem.Text + "','ON ROLL','" + txtReliveDate.Text + "',";
            SSQL = SSQL + "'" + txtReason.Text + "','" + txt480Days.Text + "','" + txtCertificate.Text + "','" + ddlWeekOff.SelectedItem.Text + "',";
            SSQL = SSQL + "'" + rbtnSalaryThrough.SelectedValue + "',";

            if (rbtnSalaryThrough.SelectedValue == "1")
            {
                SSQL = SSQL + "'','','','',";
            }
            else
            {
                SSQL = SSQL + "'" + ddlBankName.SelectedItem.Text + "','" + txtIFSC.Text + "','" + txtBranch.Text + "','" + txtAccNo.Text + "',";
            }

            SSQL = SSQL + "'" + txtBasic.Text + "','" + txtVPF.Text + "','" + txtAllowance1.Text + "','" + txtAllowance2.Text + "',";
            SSQL = SSQL + "'" + txtDeduction1.Text + "','" + txtDeduction2.Text + "','" + txtOTSal.Text + "','" + txtNationality.Text + "',";
            SSQL = SSQL + "'" + txtReligion.Text + "','" + txtHeight.Text + "','" + txtWeight.Text + "','" + txtStdWorkingHrs.Text + "','" + txtStdWorkingHrs.Text + "',";
            SSQL = SSQL + "'" + rbtnPhysically.SelectedItem.Text + "','" + txtPhyReason.Text + "','" + ddlBloodGrp.SelectedItem.Text + "',";
            SSQL = SSQL + "'" + txtRecruitThrg.SelectedItem.Text + "','" + ddlUnit.SelectedItem.Text + "','" + txtRecruitmentName.SelectedItem.Text + "',";
            SSQL = SSQL + "'" + txtRecruitMobile.Text + "','" + txtExistingEmpNo.Text + "','" + txtExistingEmpName.Text + "',";
            SSQL = SSQL + "'" + ddlWorkingUnit.SelectedItem.Text + "','" + ddlSalaryUnit.SelectedItem.Text + "','" + ddlGrade.SelectedItem.Text + "',";
            SSQL = SSQL + "'" + txtNominee.Text + "','" + txtFatherName.Text + "','" + txtParentMob1.Text + "',";
            SSQL = SSQL + "'" + txtMotherName.Text + "','" + txtParentMob2.Text + "','" + txtGuardianName.Text + "','" + txtGuardianMobile.Text + "',";
            SSQL = SSQL + "'" + txtPermAddr.Text + "','" + txtPermTaluk.SelectedItem.Text + "','" + txtPermDist.SelectedItem.Text + "',";
            SSQL = SSQL + "'" + ddlState.SelectedItem.Text + "','" + Other_State + "','" + Same_ASPermanent + "',";
            SSQL = SSQL + "'" + txtTempTaluk.SelectedItem.Text + "','" + txtTempDist.SelectedItem.Text + "','" + txtTempAddr.Text + "',";
            SSQL = SSQL + "'" + txtIdenMark1.Text + "','" + txtIdenMark2.Text + "','" + txtRefParentsName.Text + "','" + txtRefMobileNo.Text + "',";
            SSQL = SSQL + "'" + txtLeaveFrom.Text + "','" + txtLeaveTo.Text + "','" + ddlEmpLevel.SelectedItem.Text + "',";

            if (ddlEmpLevel.SelectedItem.Text == "Trainee")
            {
                SSQL = SSQL + "'1',";
            }
            else if (ddlEmpLevel.SelectedItem.Text == "SemiSkilled")
            {
                SSQL = SSQL + "'2',";
            }
            else if (ddlEmpLevel.SelectedItem.Text == "Skilled")
            {
                SSQL = SSQL + "'3',";
            }
            else
            {
                SSQL = SSQL + "'',";
            }

            SSQL = SSQL + "'" + Adolescent_Val + "','','" + Adolescent_Complete_Status + "','" + Adolescent_Complete_Date + "',";
            SSQL = SSQL + "'" + SessionUserName + "',GetDate()";
            SSQL = SSQL + ",'" + txtRefType.SelectedItem.Text + "','" + txtAgentName.SelectedItem.Text + "'";
            SSQL = SSQL + ",'" + ExemptedStaff + "','" + txtFormIDate.Text + "','" + ddlCommunity.SelectedItem.Text + "'";
            SSQL = SSQL + ",'" + txtNomineeRelation.Text + "'";
            SSQL = SSQL + ",'" + txtFestival1.Text + "','" + txtLeaveDays1.Text + "','" + txtLeaveFrom2.Text + "'";
            SSQL = SSQL + ",'" + txtLeaveTo2.Text + "','" + txtFestival2.Text + "','" + txtLeaveDays2.Text + "','" + txtCommissionAmt.Text + "'";

            if (ddlWagesType.SelectedItem.Text.ToUpper() != "HOSTEL")
            {
                SSQL = SSQL + ",'0','" + txtPFNo.Text + "'";
            }
            else
            {
                SSQL = SSQL + ",'" + ddlWorkType.SelectedValue + "','" + txtPFNo.Text + "'";
            }

            SSQL = SSQL + ",'" + rbtnHourSalary.SelectedItem.Text + "','" + rbtnFixed_Salary.SelectedItem.Text + "','" + rbtMasthiri.SelectedItem.Text + "'";
            SSQL = SSQL + ",'" + ddlShiftType.SelectedItem.Text + "','" + ddlHindiCategory.SelectedItem.Text + "','" + rbtHindiCommission.SelectedValue + "'";
            SSQL = SSQL + ",'" + txtmiltype.SelectedValue + "','" + txtVanNo.Text + "','" + txtVanRoute.Text + "','" + rbtEligibleDayincentive.SelectedValue + "','" + txtSalary2.Text + "')";

            objdata.RptEmployeeMultipleDetails(SSQL);

            //Schedule Leave start

            DataTable DT_Schedule = new DataTable();

            SSQL = "Select *from ScheduleLeaveDet Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And MachineID='" + txtMachineID.Text + "'";
            DT_Schedule = objdata.RptEmployeeMultipleDetails(SSQL);

            if (DT_Schedule.Rows.Count != 0)
            {
                SSQL = "Delete from ScheduleLeaveDet Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                SSQL = SSQL + " And MachineID='" + txtMachineID.Text + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);
            }

            if (txtLeaveFrom.Text != "" && txtLeaveTo.Text != "")
            {
                SSQL = "";
                SSQL = "Insert into ScheduleLeaveDet(CompCode,LocCode,MachineID,LeaveFrom,LeaveTo,FestivalName,LeaveDays)Values ( ";
                SSQL = SSQL + "'" + SessionCcode + "','" + SessionLcode + "','" + txtMachineID.Text + "',";
                SSQL = SSQL + "'" + txtLeaveFrom.Text + "','" + txtLeaveTo.Text + "','" + txtFestival1.Text + "','" + txtLeaveDays1.Text + "')";
                objdata.RptEmployeeMultipleDetails(SSQL);
            }

            if (txtLeaveFrom2.Text != "" && txtLeaveTo2.Text != "")
            {
                SSQL = "";
                SSQL = "Insert into ScheduleLeaveDet(CompCode,LocCode,MachineID,LeaveFrom,LeaveTo,FestivalName,LeaveDays)Values ( ";
                SSQL = SSQL + "'" + SessionCcode + "','" + SessionLcode + "','" + txtMachineID.Text + "',";
                SSQL = SSQL + "'" + txtLeaveFrom2.Text + "','" + txtLeaveTo2.Text + "','" + txtFestival2.Text + "','" + txtLeaveDays2.Text + "')";
                objdata.RptEmployeeMultipleDetails(SSQL);
            }

            //Schedule Leave end

            //Commission 

            if (txtRecruitThrg.SelectedItem.Text == "Recruitment Officer")
            {
                DataTable DT_Comm = new DataTable();
                string CommAmt = "0";

                CommAmt = txtCommissionAmt.Text;

                SSQL = "Select *from Commission_Transaction_Ledger Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                SSQL = SSQL + " And Comm_Trans_No='" + txtMachineID.Text + "'";
                DT_Comm = objdata.RptEmployeeMultipleDetails(SSQL);
                if (DT_Comm.Rows.Count == 0)
                {
                    SSQL = "Insert into Commission_Transaction_Ledger(CompCode,LocCode,TransDate,ReferalType,";
                    SSQL = SSQL + "ReferalName,Credit,Debit,FormType,Comm_Trans_No,Token_No)Values ( ";
                    SSQL = SSQL + "'" + SessionCcode + "','" + SessionLcode + "',convert(varchar(10), getdate(), 103),'" + txtRefType.SelectedItem.Text + "',";
                    if (txtRefType.SelectedItem.Text == "Agent")
                    {
                        SSQL = SSQL + "'" + txtAgentName.SelectedItem.Text + "',";
                    }
                    else if (txtRefType.SelectedItem.Text == "Parent")
                    {
                        SSQL = SSQL + "'" + txtRefParentsName.Text + "',";
                    }
                    SSQL = SSQL + "'" + CommAmt + "','0','EmployeeMst','" + txtMachineID.Text + "','" + txtMachineID.Text + "')";
                    objdata.RptEmployeeMultipleDetails(SSQL);

                }
            }


            //Employee Level

            if (Employee_Save_Table == "Employee_Mst")
            {
                if (ddlEmpLevel.SelectedItem.Text == "SemiSkilled")
                {
                    DataTable DT_Level = new DataTable();

                    SSQL = "Select *from Training_Level_Change where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                    SSQL = SSQL + " And EmpNo='" + txtMachineID.Text + "'";
                    DT_Level = objdata.RptEmployeeMultipleDetails(SSQL);

                    if (DT_Level.Rows.Count == 0)
                    {
                        query = "insert into Training_Level_Change(Ccode,Lcode,EmpNo,MachineID,ExistingCode,";
                        query = query + "Training_Level,Level_Date)values('" + SessionCcode + "','" + SessionLcode + "',";
                        query = query + "'" + txtMachineID.Text + "','" + txtMachineID.Text + "',";
                        query = query + "'" + txtExistingCode.Text + "','SemiSkilled',convert(varchar,GETDATE(),103))";
                        objdata.RptEmployeeMultipleDetails(query);
                    }
                }
            }


            //Emp Status Update
            string Emp_Status = "";
            if (Employee_Save_Table == "Employee_Mst_New_Emp")
            {
                Emp_Status = "Pending";
            }
            else
            {
                Emp_Status = "Completed";
            }

            DataTable EmpStatus = new DataTable();
            SSQL = "Select * from Employee_Mst_Status where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And MachineID='" + txtMachineID.Text + "'";
            EmpStatus = objdata.RptEmployeeMultipleDetails(SSQL);
            if (EmpStatus.Rows.Count != 0)
            {
                //Update
                SSQL = "Update Employee_Mst_Status set Token_No='" + txtExistingCode.Text + "',Emp_Status='" + Emp_Status + "',Cancel_Reson='' where";
                SSQL = SSQL + " CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And MachineID='" + txtMachineID.Text + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);

            }
            else
            {
                //Insert
                SSQL = "Insert Into Employee_Mst_Status(CompCode,LocCode,Token_No,MachineID,Emp_Status,Cancel_Reson)";
                SSQL = SSQL + " Values('" + SessionCcode + "','" + SessionLcode + "','" + txtExistingCode.Text + "','" + txtMachineID.Text + "','" + Emp_Status + "','')";
                objdata.RptEmployeeMultipleDetails(SSQL);
                //InsertDeleteUpdate_Lunch_Server(SSQL)


            }

            DataTable doc_Dt = new DataTable();
            SSQL = "Select * from Employee_Doc_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And ExistingCode='" + txtExistingCode.Text + "'";
            doc_Dt = objdata.RptEmployeeMultipleDetails(SSQL);

            if (doc_Dt.Rows.Count != 0)
            {
                SSQL = "";
                SSQL = "Delete from Employee_Doc_Mst Where CompCode='" + SessionCcode + "'";
                SSQL = SSQL + " And LocCode='" + SessionLcode + "'";
                SSQL = SSQL + " And ExistingCode='" + txtExistingCode.Text + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);
            }

            DataTable dt = new DataTable();
            dt = (DataTable)ViewState["ItemTable"];
            for (int i = 0; i < dt.Rows.Count; i++)
            {

                query = "Insert Into Employee_Doc_Mst(CompCode,LocCode,EmpNo,ExistingCode,DocType,DocNo,";
                query = query + "Created_By,Created_Date) Values('" + SessionCcode + "',";
                query = query + " '" + SessionLcode + "','" + txtMachineID.Text + "','" + txtExistingCode.Text + "',";
                query = query + " '" + dt.Rows[i]["DocType"].ToString() + "','" + dt.Rows[i]["DocNo"].ToString() + "',";
                query = query + " '" + SessionUserName + "',GetDate())";
                objdata.RptEmployeeMultipleDetails(query);
            }


            query = "delete from Adolcent_Emp_Det where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            query = query + " And ExistingCode='" + txtExistingCode.Text + "'";
            objdata.RptEmployeeMultipleDetails(query);

            DataTable dt1 = new DataTable();
            dt1 = (DataTable)ViewState["CertTable"];
            for (int i = 0; i < dt1.Rows.Count; i++)
            {
                query = "Insert Into Adolcent_Emp_Det(CompCode,LocCode,ExistingCode,MachineID,BirthDate,BirthDate_Str,";
                query = query + "Certificate_No,Certificate_Date,Certificate_Date_Str,Next_Due_Date,Next_Due_Date_Str,";
                query = query + "Certificate_Type,Remarks) Values('" + SessionCcode + "',";
                query = query + " '" + SessionLcode + "','" + txtExistingCode.Text + "','" + txtMachineID.Text + "',";
                query = query + "convert(datetime,'" + Convert.ToDateTime(txtDOB.Text).ToString("dd/MM/yyyy") + "',103),'" + txtDOB.Text + "',";
                query = query + "'" + dt1.Rows[i]["Certificate_No"].ToString() + "',";
                query = query + "convert(datetime,'" + Convert.ToDateTime(dt1.Rows[i]["Certificate_Date_Str"].ToString()).ToString("dd/MM/yyyy") + "',103),";
                query = query + "'" + dt1.Rows[i]["Certificate_Date_Str"].ToString() + "',";
                query = query + "convert(datetime,'" + Convert.ToDateTime(dt1.Rows[i]["Next_Due_Date_Str"].ToString()).ToString("dd/MM/yyyy") + "',103),";
                query = query + "'" + dt1.Rows[i]["Next_Due_Date_Str"].ToString() + "',";
                query = query + " '" + dt1.Rows[i]["Certificate_Type"].ToString() + "','" + dt1.Rows[i]["Remarks"].ToString() + "')";
                objdata.RptEmployeeMultipleDetails(query);
            }


            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Employee Details Saved Successfully..!');", true);
            Session.Remove("MachineID");
            Clear_All_Field();
            //Response.Redirect("Employee_Main.aspx");

        }
    }

    protected void txtMachineID_TextChanged(object sender, EventArgs e)
    {

        txtTokenID.Text = txtMachineID.Text;

        //DataTable DT = new DataTable();

        //SSQL = "Select * from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        //SSQL = SSQL + " And EmpNo='" + txtMachineID.Text + "'";
        //DT = objdata.RptEmployeeMultipleDetails(SSQL);

        //if (DT.Rows.Count == 0)
        //{
        //    SSQL = "Select * from Employee_Mst_New_Emp where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        //    SSQL = SSQL + " And EmpNo='" + txtMachineID.Text + "'";
        //    DT = objdata.RptEmployeeMultipleDetails(SSQL);

        //    if (DT.Rows.Count != 0)
        //    {
        //        txtMachineID.Text = "";
        //        txtTokenID.Text = "";
        //        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Already Employee No Exists!');", true);
        //    }
        //    else
        //    {
        //        txtTokenID.Text = txtMachineID.Text;
        //    }

        //}
        //else
        //{
        //    txtTokenID.Text = "";
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Already Employee No Exists!');", true);
        //}

    }

    protected void chkSame_CheckedChanged(object sender, EventArgs e)
    {
        if (chkSame.Checked == true)
        {
            txtTempAddr.Text = txtPermAddr.Text;

            txtTempTaluk.SelectedValue = txtPermTaluk.SelectedValue;
            txtTempDist.SelectedValue = txtPermDist.SelectedValue;
        }
        else
        {
            txtTempAddr.Text = "";
            txtTempTaluk.SelectedValue = "-Select-";
            txtTempDist.SelectedValue = "-Select-";
        }
    }

    private static string UTF8Encryption_OLD(string password)
    {
        string strmsg = string.Empty;
        byte[] encode = new byte[password.Length];
        encode = Encoding.UTF8.GetBytes(password);
        strmsg = Convert.ToBase64String(encode);
        return strmsg;
    }

    public string UTF8Encryption(string mvarPlanText)
    {
        string cipherText = "";
        try
        {
            string passPhrase = "Altius";
            string saltValue = "info@altius.co.in";
            string hashAlgorithm = "SHA1";
            string initVector = "@1B2c3D4e5F6g7H8";
            int passwordIterations = 2;
            int keySize = 256;
            byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
            byte[] saltValueBytes = Encoding.ASCII.GetBytes(saltValue);
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(mvarPlanText);
            PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, saltValueBytes, hashAlgorithm, passwordIterations);
            byte[] keyBytes = password.GetBytes(keySize / 8);
            RijndaelManaged symmetricKey = new RijndaelManaged();
            symmetricKey.Mode = CipherMode.CBC;
            ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes);
            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
            cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
            cryptoStream.FlushFinalBlock();
            byte[] cipherTextBytes = memoryStream.ToArray();
            memoryStream.Close();
            cryptoStream.Close();
            cipherText = Convert.ToBase64String(cipherTextBytes);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }

        return cipherText;
    }


    private static string UTF8Decryption(string encryptpwd)
    {
        string decryptpwd = string.Empty;
        UTF8Encoding encodepwd = new UTF8Encoding();
        Decoder Decode = encodepwd.GetDecoder();
        byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
        int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
        char[] decoded_char = new char[charCount];
        Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
        decryptpwd = new String(decoded_char);
        return decryptpwd;
    }

    protected void btnEmpClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }
    protected void btnBack_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
        Response.Redirect("EmployeeApproval.aspx");
    }

    private void Clear_All_Field()
    {
        txtMachineID.Text = ""; txtExistingCode.Text = "";
        txtTokenID.Text = ""; ddlCategory.SelectedValue = "0";
        ddlSubCategory.SelectedValue = "0"; ddlShift.SelectedValue = "0";
        txtFirstName.Text = ""; txtLastName.Text = ""; txtDOB.Text = ""; txtAge.Text = "";
        ddlGender.SelectedValue = "0"; txtDOJ.Text = ""; ddlDepartment.SelectedValue = "0";
        ddlDesignation.SelectedValue = "-Select-"; txtQulification.SelectedValue = "-Select-"; txtEmpMobileNo.Text = "";
        ddlOTEligible.SelectedValue = "-Select-"; ddlWagesType.SelectedValue = "-Select-"; ddlEmpLevel.SelectedValue = "-Select-";
        RdbPFEligible.SelectedValue = "2"; chkExment.Checked = false; txtPFNo.Text = ""; txtPFDate.Text = "";
        RdbESIEligible.SelectedValue = "2"; txtESINo.Text = ""; txtESIDate.Text = "";
        txtUAN.Text = ""; //txtHostelRoom.Text = ""; 
        dbtnActive.SelectedValue = "1";
        txtReliveDate.Text = ""; txtReason.Text = ""; txt480Days.Text = "";
        txtCertificate.Text = ""; ddlWeekOff.SelectedValue = "-Select-";
        rbtnSalaryThrough.SelectedValue = "1"; txtAccNo.Text = "";
        txtBasic.Text = "0.0"; txtVPF.Text = "0.0"; txtAllowance1.Text = "0.0";
        txtAllowance2.Text = "0.0"; txtDeduction1.Text = "0.0"; txtDeduction2.Text = "0.0";
        txtOTSal.Text = "0.0"; ddlMartialStatus.SelectedValue = "0"; txtNationality.Text = "INDIAN";
        txtReligion.Text = ""; txtHeight.Text = ""; txtWeight.Text = ""; txtStdWorkingHrs.Text = "";
        rbtnPhysically.SelectedValue = "2"; txtPhyReason.Text = ""; ddlBloodGrp.SelectedValue = "0";
        txtRecruitThrg.SelectedValue = "-Select-"; txtRecruitMobile.Text = ""; txtExistingEmpNo.Text = "";
        txtExistingEmpName.Text = ""; ddlUnit.SelectedValue = "-Select-"; ddlWorkingUnit.SelectedValue = SessionLcode;
        ddlSalaryUnit.SelectedValue = SessionLcode; ddlGrade.SelectedValue = "-Select-";
        txtNominee.Text = ""; txtFatherName.Text = ""; txtParentMob1.Text = ""; txtMotherName.Text = ""; txtParentMob2.Text = "";
        txtGuardianName.Text = ""; txtGuardianMobile.Text = ""; txtPermAddr.Text = ""; txtPermDist.SelectedValue = "-Select-";
        txtPermTaluk.SelectedValue = "-Select-"; ddlState.SelectedValue = "-Select-"; chkOtherState.Checked = false;
        txtTempTaluk.SelectedValue = "-Select-"; txtTempDist.SelectedValue = "-Select-"; txtTempAddr.Text = "";
        chkSame.Checked = false; txtIdenMark1.Text = ""; txtIdenMark2.Text = "";
        ddlDocType.SelectedValue = "0"; txtDocNo.Text = ""; txtCommissionAmt.Text = "0"; ddlWorkType.SelectedValue = "0";
        ddlDocType.Enabled = true;
        txtFestival1.Text = ""; txtLeaveDays1.Text = "";
        txtLeaveFrom2.Text = ""; txtLeaveTo2.Text = ""; txtFestival2.Text = ""; txtLeaveDays2.Text = "";
        rbtEligibleDayincentive.ClearSelection();
        txtSalary2.Text = "0.0";

        txtRefType.SelectedValue = "-Select-"; txtAgentName.SelectedValue = "-Select-";
        txtNomineeRelation.Text = "";
        txtRefMobileNo.Enabled = false;
        txtAgentName.Enabled = false;
        txtRefParentsName.Enabled = false;

        txtFormIDate.Text = "";
        txtFormIDate.Enabled = false;

        Load_ESICode();
        Load_PFCode();
        Load_Bank();
        Load_Recruitment();
        Load_AgentName();

        Load_Department();
        Load_Community();
        Load_Qualification();
        Load_HostelExp();
        Load_Designation();
        Load_RoomName();
        Load_RoomNo();
        lblRecruit.Visible = true; lblAgent.Visible = false; txtRecruitmentName.Enabled = false;
        txtRecruitMobile.Enabled = false; ddlUnit.Enabled = false; txtExistingEmpNo.Enabled = false;
        txtExistingEmpName.Enabled = false;


        txtCertificate_No.Text = "";
        txtAdols_Type.SelectedValue = "-Select-";
        txtAdols_Remarks.Text = "";
        Load_Date();

        ddlDocType.SelectedValue = "0";
        txtDocNo.Text = "";
        Initial_Data_Referesh();
        Initial_Data1_Referesh();
        Load_OLD_data();
        Load_OLD_data1();
        txtReason.Enabled = false; txtPhyReason.Enabled = false;
        txtMachineID.Enabled = true;
        Session.Remove("MachineID");
        Session.Remove("MachineID_Apprv");
        btnEmpSave.Text = "Save";
        btnEmpSave.Enabled = true;
        btnBack.Visible = false;

        Image3.ImageUrl = "~/assets/img/login-bg/man-user-50.png";
        Image1.ImageUrl = "~/assets/img/login-bg/man-user-50.png";
    }

    protected void txtRecruitmentName_SelectedIndexChanged(object sender, EventArgs e)
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        if (txtRecruitThrg.SelectedItem.Text == "Agent")
        {
            query = "Select * from MstAgent";
            dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        }
        else if (txtRecruitThrg.SelectedItem.Text == "Recruitment Officer")
        {
            query = "Select * from MstRecruitOfficer where ROName='" + txtRecruitmentName.SelectedValue + "'";
            dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        }
        if (dtdsupp.Rows.Count != 0)
        {
            if (txtRecruitThrg.SelectedItem.Text == "Recruitment Officer")
            {
                txtRecruitmentName.SelectedValue = dtdsupp.Rows[0]["ROName"].ToString();
                ddlUnit.SelectedValue = dtdsupp.Rows[0]["Unit"].ToString();
                txtRecruitMobile.Text = dtdsupp.Rows[0]["Mobile"].ToString();
            }
            else if (txtRecruitThrg.SelectedItem.Text == "Agent")
            {
                txtRecruitmentName.SelectedValue = dtdsupp.Rows[0]["AgentName"].ToString();
                ddlUnit.SelectedValue = "-Select-";
                txtRecruitMobile.Text = dtdsupp.Rows[0]["Mobile"].ToString();
            }
        }
        else
        {
            txtRecruitmentName.SelectedValue = "-Select-";
            ddlUnit.SelectedValue = "-Select-";
            txtRecruitMobile.Text = "";
        }
    }

    protected void GridEditClick(object sender, CommandEventArgs e)
    {
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        string qry = "";

        DataTable dt = new DataTable();

        ddlDocType.SelectedValue = e.CommandArgument.ToString();

        txtDocNo.Text = e.CommandName.ToString();

        ddlDocType.Enabled = false;
    }


    protected void GridDeleteClick(object sender, CommandEventArgs e)
    {
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        string qry = "";

        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i]["DocNo"].ToString() == e.CommandName.ToString())
            {
                dt.Rows.RemoveAt(i);
                dt.AcceptChanges();
            }
        }
        ViewState["ItemTable"] = dt;
        Load_OLD_data();

    }

    protected void BtnAdolcent_Add_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        DataRow dr = null;
        string query = "";
        if (txtCertificate_No.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Certificate No..!');", true);
        }



        if (txtCertificate_Date.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Certificate Date..');", true);
        }
        if (txtAdoles_Due_Date.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Adolscent Next Due Date..');", true);
        }

        if (txtAdols_Type.SelectedValue == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the Type of certificate..!');", true);
        }

        if (!ErrFlag)
        {

            // check view state is not null  
            if (ViewState["CertTable"] != null)
            {
                //get datatable from view state   
                dt = (DataTable)ViewState["CertTable"];

                //check Item Already add or not
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if ((dt.Rows[i]["Certificate_No"].ToString().ToUpper() == txtCertificate_No.Text.ToUpper()))
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Certificate Already Added..');", true);
                    }
                }

                if (!ErrFlag)
                {
                    dr = dt.NewRow();
                    dr["Certificate_No"] = txtCertificate_No.Text;
                    dr["Certificate_Date_Str"] = txtCertificate_Date.Text;
                    dr["Next_Due_Date_Str"] = txtAdoles_Due_Date.Text;
                    dr["Certificate_Type"] = txtAdols_Type.SelectedItem.Text;
                    dr["Remarks"] = txtAdols_Remarks.Text;

                    dt.Rows.Add(dr);
                    ViewState["CertTable"] = dt;
                    Repeater2.DataSource = dt;
                    Repeater2.DataBind();


                    txtCertificate_No.Text = "";
                    txtAdols_Type.SelectedValue = "-Select-";
                    txtAdols_Remarks.Text = "";
                    Load_Date();
                }
            }
            else
            {
                dr = dt.NewRow();
                dr["Certificate_No"] = txtCertificate_No.Text;
                dr["Certificate_Date_Str"] = txtCertificate_Date.Text;
                dr["Next_Due_Date_Str"] = txtAdoles_Due_Date.Text;
                dr["Certificate_Type"] = txtAdols_Type.SelectedItem.Text;
                dr["Remarks"] = txtAdols_Remarks.Text;

                dt.Rows.Add(dr);
                ViewState["CertTable"] = dt;
                Repeater2.DataSource = dt;
                Repeater2.DataBind();

                txtCertificate_No.Text = "";
                txtAdols_Type.SelectedValue = "-Select-";
                txtAdols_Remarks.Text = "";
                Load_Date();
            }
        }
    }

    protected string UploadFolderPath = "";
    protected void FileUploadComplete(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        DataRow dr = null;

        if (ddlDocType.SelectedItem.Text == "-Select-")
        {
            ErrFlag = true;
            Response.Write("<script language=javascript>alert('Select the Document Type...');</script>");
            //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the Document Type...');", true);

        }
        if (!ErrFlag)
        {
            if (txtDocNo.Text == "")
            {
                ErrFlag = true;
                Response.Write("<script language=javascript>alert('Enter the Document No.');</script>");

                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Document No.');", true);
            }
        }
        if (!ErrFlag)
        {
            if (ddlDocType.SelectedItem.Text == "Adhar Card")
            {
                if ((txtDocNo.Text).Length != 12 || (txtDocNo.Text).Length > 12)
                {
                    ErrFlag = true;
                    Response.Write("<script language=javascript>alert('Check Your ADHAR NO...Only 12 digit Allowed for ADHAR NO.');</script>");

                    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check Your ADHAR NO...Only 12 digit Allowed for ADHAR NO.');", true);
                }
            }
        }
        if (!ErrFlag)
        {
            if (ddlDocType.SelectedItem.Text == "Voter Card")
            {
                if ((txtDocNo.Text).Length != 10 || (txtDocNo.Text).Length > 10)
                {
                    ErrFlag = true;
                    Response.Write("<script language=javascript>alert('Check Your VOTER ID NO...Only 10 digit Allowed for VOTER ID NO.');</script>");

                    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check Your VOTER ID NO...Only 10 digit Allowed for VOTER ID NO.');", true);
                }
            }
        }
        if (!ErrFlag)
        {
            if (ddlDocType.SelectedItem.Text == "Ration Card")
            {
                if ((txtDocNo.Text).Length != 12 || (txtDocNo.Text).Length > 12)
                {
                    ErrFlag = true;
                    Response.Write("<script language=javascript>alert('Check Your RATION CARD NO...Only 12 digit Allowed for RATION CARD NO.');</script>");

                    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check Your RATION CARD NO...Only 12 digit Allowed for RATION CARD NO.');", true);
                }
            }
        }
        if (!ErrFlag)
        {
            if (ddlDocType.SelectedItem.Text == "Pan Card")
            {
                if ((txtDocNo.Text).Length != 10 || (txtDocNo.Text).Length > 10)
                {
                    ErrFlag = true;
                    Response.Write("<script language=javascript>alert('Check Your PAN CARD NO...Only 10 digit Allowed for PAN CARD NO.');</script>");

                    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check Your PAN CARD NO...Only 10 digit Allowed for PAN CARD NO.');", true);
                }
            }
        }
        if (!ErrFlag)
        {
            if (ddlDocType.SelectedItem.Text == "Driving Licence")
            {
                if ((txtDocNo.Text).Length != 16 || (txtDocNo.Text).Length > 16)
                {
                    ErrFlag = true;
                    Response.Write("<script language=javascript>alert('Check Your Driving Licence No...Only 16 digit Allowed for Driving Licence No.');</script>");

                    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check Your Driving Licence No...Only 16 digit Allowed for Driving Licence No.');", true);
                }
            }
        }
        if (!ErrFlag)
        {
            if (ddlDocType.SelectedItem.Text == "Smart Card")
            {
                if ((txtDocNo.Text).Length != 19 || (txtDocNo.Text).Length > 19)
                {
                    ErrFlag = true;
                    Response.Write("<script language=javascript>alert('Check Your SMART CARD NO...Only 19 digit Allowed for SMART CARD NO.');</script>");

                    //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check Your SMART CARD NO...Only 19 digit Allowed for SMART CARD NO.');", true);
                }
            }
        }
        if (!ErrFlag)
        {
            string token_Name = "";

            //string UNIT_Folder = "";
            //string Doc_Folder = "";
            //if (SessionLcode.ToUpper() == "UNIT I") { UNIT_Folder = "Emp_Scan/UNIT_I"; }
            //if (SessionLcode.ToUpper() == "UNIT II") { UNIT_Folder = "Emp_Scan/UNIT_II"; }
            //if (SessionLcode.ToUpper() == "UNIT III") { UNIT_Folder = "Emp_Scan/UNIT_III"; }
            //if (SessionLcode.ToUpper() == "UNIT IV") { UNIT_Folder = "Emp_Scan/UNIT_IV"; }

            string UNIT_Folder = "";
            string Doc_Folder = "";

            DataTable DT_Photo = new DataTable();
            string SS = "Select * from Photo_Path_Det";
            DT_Photo = objdata.RptEmployeeMultipleDetails(SS);

            string PhotoDet = "";
            if (DT_Photo.Rows.Count != 0)
            {
                PhotoDet = DT_Photo.Rows[0]["Photo_Path"].ToString();
            }
            //if (SessionLcode.ToUpper() == "UNIT I") { UNIT_Folder = "Emp_Scan/UNIT_I/"; }
            //if (SessionLcode.ToUpper() == "UNIT II") { UNIT_Folder = "Emp_Scan/UNIT_II/"; }
            //if (SessionLcode.ToUpper() == "UNIT III") { UNIT_Folder = "Emp_Scan/UNIT_III/"; }
            //if (SessionLcode.ToUpper() == "UNIT IV") { UNIT_Folder = "//SABA-PC/To Share/Narmatha/Emp_Scan/UNIT_IV/"; } //UNIT_Folder = "Emp_Scan/UNIT_IV/"; }

            if (SessionLcode.ToUpper() == "UNIT I") { UNIT_Folder = PhotoDet + "/UNIT_I/"; }
            if (SessionLcode.ToUpper() == "UNIT II") { UNIT_Folder = PhotoDet + "/UNIT_II/"; }
            if (SessionLcode.ToUpper() == "UNIT III") { UNIT_Folder = PhotoDet + "/UNIT_III/"; }
            if (SessionLcode.ToUpper() == "UNIT IV") { UNIT_Folder = PhotoDet + "/UNIT_IV/"; } //UNIT_Folder = "Emp_Scan/UNIT_IV/"; }


            if (ddlDocType.SelectedItem.Text == "Adhar Card")
            {
                Doc_Folder = "/ID_Proof/A_Copy/";
                token_Name = "A_" + txtTokenID.Text;
            }
            if (ddlDocType.SelectedItem.Text == "Bank Pass Book")
            {
                Doc_Folder = "/ID_Proof/B_PB_Copy/";
                token_Name = "A_" + txtTokenID.Text;
            }
            if (ddlDocType.SelectedItem.Text == "Others")
            {
                Doc_Folder = "/ID_Proof/Other_Copy/";
                token_Name = "A_" + txtTokenID.Text;
            }
            if (ddlDocType.SelectedItem.Text == "Voter Card")
            {
                Doc_Folder = "/ID_Proof/V_Copy/";
                token_Name = "V_" + txtTokenID.Text;
            }
            if (ddlDocType.SelectedItem.Text == "Ration Card")
            {
                Doc_Folder = "/ID_Proof/R_Copy/";
                token_Name = "R_" + txtTokenID.Text;
            }
            if (ddlDocType.SelectedItem.Text == "Pan Card")
            {
                Doc_Folder = "/ID_Proof/P_Copy/";
                token_Name = "P_" + txtTokenID.Text;
            }
            if (ddlDocType.SelectedItem.Text == "Driving Licence")
            {
                Doc_Folder = "/ID_Proof/DL_Copy/";
                token_Name = "DL_" + txtTokenID.Text;
            }
            if (ddlDocType.SelectedItem.Text == "Smart Card")
            {
                Doc_Folder = "/ID_Proof/SC_Copy/";
                token_Name = "SC_" + txtTokenID.Text;
            }

            string path_1 = UNIT_Folder + Doc_Folder;

            if (filUpload.HasFile)
            {
                string FileName = Path.GetFileName(filUpload.PostedFile.FileName);
                string Exten = Path.GetExtension(filUpload.PostedFile.FileName);
                //UploadFolderPath = "~/" + path_1 + token_Name + Exten;

                //filUpload.SaveAs(Server.MapPath("~/" + path_1 + token_Name + Exten));
                filUpload.SaveAs((path_1 + token_Name + Exten));

                Response.Write("<script language=javascript>alert('Saved Successfully...');</script>");
                //filUpload.SaveAs(Server.MapPath(UploadFolderPath + FileName));
                //filUpload.SaveAs("D:/Images/" + Shade_Name.Trim() + Exten);

            }
            // btnDocAdd_Click(sender, e); ;
        }
        //btnDocAdd_Click(sender, e);;
    }

    //protected void FileUploadComplete1(object sender, EventArgs e)
    //{
    //    DataTable dt = new DataTable();
    //    DataTable qry_dt = new DataTable();
    //    bool ErrFlag = false;
    //    DataRow dr = null;
    //    string token_Name = "";

    //    string UNIT_Folder = "";
    //    string Doc_Folder = "";
    //    if (SessionLcode.ToUpper() == "UNIT I") { UNIT_Folder = "Emp_Scan/UNIT_I/Photos/"; }
    //    if (SessionLcode.ToUpper() == "UNIT II") { UNIT_Folder = "Emp_Scan/UNIT_II/Photos/"; }
    //    if (SessionLcode.ToUpper() == "UNIT III") { UNIT_Folder = "Emp_Scan/UNIT_III/Photos/"; }
    //    if (SessionLcode.ToUpper() == "UNIT IV") { UNIT_Folder = "Emp_Scan/UNIT_IV/Photos/"; }

    //    token_Name = txtTokenID.Text;




    //    string path_1 = UNIT_Folder;

    //    if (AsyncFileUpload1.HasFile)
    //    {
    //    string FileName = Path.GetFileName(AsyncFileUpload1.PostedFile.FileName);
    //    string Exten = Path.GetExtension(AsyncFileUpload1.PostedFile.FileName);

    //    AsyncFileUpload1.SaveAs(Server.MapPath("~/" + path_1 + token_Name + Exten));
    //    }
    //}

    public void Doc_Add()
    {
        DataTable dt = new DataTable();
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        DataRow dr = null;
        string query = "";

        if (!ErrFlag)
        {
            // check view state is not null  
            if (ViewState["ItemTable"] != null)
            {
                //get datatable from view state   
                dt = (DataTable)ViewState["ItemTable"];

                //check Item Already add or not
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if ((dt.Rows[i]["DocType"].ToString().ToUpper() == ddlDocType.SelectedItem.Text.ToUpper()))
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Document Already Added..');", true);
                    }
                }



                if (!ErrFlag)
                {

                    dr = dt.NewRow();
                    dr["DocType"] = ddlDocType.SelectedItem.Text;
                    dr["DocNo"] = txtDocNo.Text;



                    //if (filUpload.HasFile)
                    //{
                    //    string FileName = Path.GetFileName(filUpload.PostedFile.FileName);
                    //    string Exten = Path.GetExtension(filUpload.PostedFile.FileName);
                    //    if (Exten == ".jpg")
                    //    {
                    //        filUpload.SaveAs(Server.MapPath("~/Images/" + txtMachineID.Text + Exten));
                    //        //filUpload.SaveAs("D:/Images/" + Shade_Name.Trim() + Exten);
                    //    }
                    //}

                    //string token_Name = "";

                    //string UNIT_Folder = "";
                    //string Doc_Folder = "";
                    //if (SessionLcode.ToUpper() == "UNIT I") { UNIT_Folder = "Emp_Scan/UNIT_I"; }
                    //if (SessionLcode.ToUpper() == "UNIT II") { UNIT_Folder = "Emp_Scan/UNIT_II"; }
                    //if (SessionLcode.ToUpper() == "UNIT III") { UNIT_Folder = "Emp_Scan/UNIT_III"; }
                    //if (SessionLcode.ToUpper() == "UNIT IV") { UNIT_Folder = "Emp_Scan/UNIT_IV"; }

                    //if (ddlDocType.SelectedItem.Text == "Adhar Card")
                    //{
                    //    Doc_Folder = "/ID_Proof/A_Copy/";
                    //    token_Name = "A_" + txtTokenID.Text;
                    //}
                    //if (ddlDocType.SelectedItem.Text == "Voter Card")
                    //{
                    //    Doc_Folder = "/ID_Proof/V_Copy/";
                    //    token_Name = "V_" + txtTokenID.Text;
                    //}
                    //if (ddlDocType.SelectedItem.Text == "Ration Card")
                    //{
                    //    Doc_Folder = "/ID_Proof/R_Copy/";
                    //    token_Name = "R_" + txtTokenID.Text;
                    //}
                    //if (ddlDocType.SelectedItem.Text == "Pan Card")
                    //{
                    //    Doc_Folder = "/ID_Proof/P_Copy/";
                    //    token_Name = "P_" + txtTokenID.Text;
                    //}
                    //if (ddlDocType.SelectedItem.Text == "Driving Licence")
                    //{
                    //    Doc_Folder = "/ID_Proof/DL_Copy/";
                    //    token_Name = "DL_" + txtTokenID.Text;
                    //}
                    //if (ddlDocType.SelectedItem.Text == "Smart Card")
                    //{
                    //    Doc_Folder = "/ID_Proof/SC_Copy/";
                    //    token_Name = "SC_" + txtTokenID.Text;
                    //}



                    //string path_1 = UNIT_Folder + Doc_Folder;

                    //if (filUpload.HasFile)
                    //{
                    //    string FileName = Path.GetFileName(filUpload.PostedFile.FileName);
                    //    string Exten = Path.GetExtension(filUpload.PostedFile.FileName);
                    //    if (Exten == ".jpg")
                    //    {
                    //        filUpload.SaveAs(Server.MapPath("~/" + path_1 + token_Name + Exten));
                    //        //filUpload.SaveAs("D:/Images/" + Shade_Name.Trim() + Exten);
                    //    }
                    //}


                    dt.Rows.Add(dr);
                    ViewState["ItemTable"] = dt;
                    Repeater1.DataSource = dt;
                    Repeater1.DataBind();

                    //Totalsum();


                    ddlDocType.SelectedValue = "0";

                    txtDocNo.Text = "";

                }
            }
            else
            {
                dr = dt.NewRow();
                dr["DocType"] = ddlDocType.SelectedItem.Text;
                dr["DocNo"] = txtDocNo.Text;


                //string Shade_Name = ddlDocType.SelectedItem.Text + "_" + txtMachineID.Text;//txtShadeName.SelectedItem.Text + "-" + txtShadeNo.Text;


                //if (filUpload.HasFile)
                //{
                //    string FileName = Path.GetFileName(filUpload.PostedFile.FileName);
                //    string Exten = Path.GetExtension(filUpload.PostedFile.FileName);
                //    if (Exten == ".jpg")
                //    {
                //        //filUpload.SaveAs(Server.MapPath("~/Images/" + Shade_Name + Exten));
                //        filUpload.SaveAs("D:/Images/" + Shade_Name.Trim() + Exten);
                //    }
                //}


                //string token_Name = "";

                //string UNIT_Folder = "";
                //string Doc_Folder = "";
                //if (SessionLcode.ToUpper() == "UNIT I") { UNIT_Folder = "Emp_Scan/UNIT_I"; }
                //if (SessionLcode.ToUpper() == "UNIT II") { UNIT_Folder = "Emp_Scan/UNIT_II"; }
                //if (SessionLcode.ToUpper() == "UNIT III") { UNIT_Folder = "Emp_Scan/UNIT_III"; }
                //if (SessionLcode.ToUpper() == "UNIT IV") { UNIT_Folder = "Emp_Scan/UNIT_IV"; }

                //if (ddlDocType.SelectedItem.Text == "Adhar Card")
                //{
                //    Doc_Folder = "/ID_Proof/A_Copy/";
                //    token_Name = "A_" + txtTokenID.Text;
                //}
                //if (ddlDocType.SelectedItem.Text == "Voter Card")
                //{
                //    Doc_Folder = "/ID_Proof/V_Copy/";
                //    token_Name = "V_" + txtTokenID.Text;
                //}
                //if (ddlDocType.SelectedItem.Text == "Ration Card")
                //{
                //    Doc_Folder = "/ID_Proof/R_Copy/";
                //    token_Name = "R_" + txtTokenID.Text;
                //}
                //if (ddlDocType.SelectedItem.Text == "Pan Card")
                //{
                //    Doc_Folder = "/ID_Proof/P_Copy/";
                //    token_Name = "P_" + txtTokenID.Text;
                //}
                //if (ddlDocType.SelectedItem.Text == "Driving Licence")
                //{
                //    Doc_Folder = "/ID_Proof/DL_Copy/";
                //    token_Name = "DL_" + txtTokenID.Text;
                //}
                //if (ddlDocType.SelectedItem.Text == "Smart Card")
                //{
                //    Doc_Folder = "/ID_Proof/SC_Copy/";
                //    token_Name = "SC_" + txtTokenID.Text;
                //}



                //string path_1 = UNIT_Folder + Doc_Folder;

                //if (filUpload.HasFile)
                //{
                //    string FileName = Path.GetFileName(filUpload.PostedFile.FileName);
                //    string Exten = Path.GetExtension(filUpload.PostedFile.FileName);
                //    if (Exten == ".jpg")
                //    {
                //        filUpload.SaveAs(Server.MapPath("~/" + path_1 + token_Name + Exten));
                //        //filUpload.SaveAs("D:/Images/" + Shade_Name.Trim() + Exten);
                //    }
                //}


                dt.Rows.Add(dr);
                ViewState["ItemTable"] = dt;
                Repeater1.DataSource = dt;
                Repeater1.DataBind();

                //Totalsum();


                ddlDocType.SelectedValue = "0";

                txtDocNo.Text = "";

            }
        }
    }

    protected void btnDocAdd_Click(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        DataRow dr = null;
        string query = "";

        if (txtMachineID.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Machine ID...');", true);
        }
        if (ddlDocType.SelectedItem.Text == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the Document Type...');", true);
        }

        if (txtDocNo.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Document No.');", true);
        }

        if (ddlDocType.SelectedItem.Text == "Adhar Card")
        {
            if ((txtDocNo.Text).Length != 12 || (txtDocNo.Text).Length > 12)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check Your ADHAR NO...Only 12 digit Allowed for ADHAR NO.');", true);
            }
            else
            {
                if (txtRecruitThrg.SelectedItem.Text != "ReJoin")
                {
                    DataTable Doc_dt = new DataTable();
                    SSQL = "Select * from Employee_Doc_Mst where CompCode='" + SessionCcode + "' and DocNo='" + txtDocNo.Text + "'";
                    SSQL = SSQL + " And DocType='Adhar Card' And EmpNo!='" + txtMachineID.Text + "'";

                    Doc_dt = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (Doc_dt.Rows.Count != 0)
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('ADHAR NO Already Exists...');", true);

                    }
                }
            }
        }
        if (ddlDocType.SelectedItem.Text == "Voter Card")
        {
            if ((txtDocNo.Text).Length != 10 || (txtDocNo.Text).Length > 10)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check Your VOTER ID NO...Only 10 digit Allowed for VOTER ID NO.');", true);
            }
        }

        if (ddlDocType.SelectedItem.Text == "Ration Card")
        {
            if ((txtDocNo.Text).Length != 12 || (txtDocNo.Text).Length > 12)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check Your RATION CARD NO...Only 12 digit Allowed for RATION CARD NO.');", true);
            }
        }

        if (ddlDocType.SelectedItem.Text == "Pan Card")
        {
            if ((txtDocNo.Text).Length != 10 || (txtDocNo.Text).Length > 10)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check Your PAN CARD NO...Only 10 digit Allowed for PAN CARD NO.');", true);
            }
        }

        if (ddlDocType.SelectedItem.Text == "Driving Licence")
        {
            if ((txtDocNo.Text).Length != 16 || (txtDocNo.Text).Length > 16)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check Your Driving Licence No...Only 16 digit Allowed for Driving Licence No.');", true);
            }
        }
        if (ddlDocType.SelectedItem.Text == "Smart Card")
        {
            if ((txtDocNo.Text).Length != 19 || (txtDocNo.Text).Length > 19)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check Your SMART CARD NO...Only 19 digit Allowed for SMART CARD NO.');", true);
            }
        }

        if (!ErrFlag)
        {
            // check view state is not null  
            if (ViewState["ItemTable"] != null)
            {
                //get datatable from view state   
                dt = (DataTable)ViewState["ItemTable"];

                if (ddlDocType.Enabled == false)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if ((dt.Rows[i]["DocType"].ToString().ToUpper() == ddlDocType.SelectedItem.Text.ToUpper()))
                        {
                            dt.Rows.RemoveAt(i);
                            dt.AcceptChanges();
                        }
                    }
                }

                //check Item Already add or not
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if ((dt.Rows[i]["DocType"].ToString().ToUpper() == ddlDocType.SelectedItem.Text.ToUpper()))
                    {
                        ErrFlag = true;
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Document Already Added..');", true);
                    }
                }



                if (!ErrFlag)
                {

                    dr = dt.NewRow();
                    dr["DocType"] = ddlDocType.SelectedItem.Text;
                    dr["DocNo"] = txtDocNo.Text;





                    //if (filUpload.HasFile)
                    //{
                    //    string FileName = Path.GetFileName(filUpload.PostedFile.FileName);
                    //    string Exten = Path.GetExtension(filUpload.PostedFile.FileName);
                    //    if (Exten == ".jpg")
                    //    {
                    //        filUpload.SaveAs(Server.MapPath("~/Images/" + txtMachineID.Text + Exten));
                    //        //filUpload.SaveAs("D:/Images/" + Shade_Name.Trim() + Exten);
                    //    }
                    //}

                    string token_Name = "";

                    //string UNIT_Folder = "";
                    //string Doc_Folder = "";
                    //if (SessionLcode.ToUpper() == "UNIT I") { UNIT_Folder = "Emp_Scan/UNIT_I"; }
                    //if (SessionLcode.ToUpper() == "UNIT II") { UNIT_Folder = "Emp_Scan/UNIT_II"; }
                    //if (SessionLcode.ToUpper() == "UNIT III") { UNIT_Folder = "Emp_Scan/UNIT_III"; }
                    //if (SessionLcode.ToUpper() == "UNIT IV") { UNIT_Folder = "Emp_Scan/UNIT_IV"; }

                    string UNIT_Folder = "";
                    string Doc_Folder = "";

                    DataTable DT_Photo = new DataTable();
                    string SS = "Select *from Photo_Path_Det";
                    DT_Photo = objdata.RptEmployeeMultipleDetails(SS);

                    string PhotoDet = "";
                    if (DT_Photo.Rows.Count != 0)
                    {
                        PhotoDet = DT_Photo.Rows[0]["Photo_Path"].ToString();
                    }
                    //if (SessionLcode.ToUpper() == "UNIT I") { UNIT_Folder = "Emp_Scan/UNIT_I/"; }
                    //if (SessionLcode.ToUpper() == "UNIT II") { UNIT_Folder = "Emp_Scan/UNIT_II/"; }
                    //if (SessionLcode.ToUpper() == "UNIT III") { UNIT_Folder = "Emp_Scan/UNIT_III/"; }
                    //if (SessionLcode.ToUpper() == "UNIT IV") { UNIT_Folder = "//SABA-PC/To Share/Narmatha/Emp_Scan/UNIT_IV/"; } //UNIT_Folder = "Emp_Scan/UNIT_IV/"; }

                    if (SessionLcode.ToUpper() == "UNIT I") { UNIT_Folder = PhotoDet + "/UNIT_I/"; }
                    if (SessionLcode.ToUpper() == "UNIT II") { UNIT_Folder = PhotoDet + "/UNIT_II/"; }
                    if (SessionLcode.ToUpper() == "UNIT III") { UNIT_Folder = PhotoDet + "/UNIT_III/"; }
                    if (SessionLcode.ToUpper() == "UNIT IV") { UNIT_Folder = PhotoDet + "/UNIT_IV/"; } //UNIT_Folder = "Emp_Scan/UNIT_IV/"; }


                    if (ddlDocType.SelectedItem.Text == "Adhar Card")
                    {
                        Doc_Folder = "/ID_Proof/A_Copy/";
                        token_Name = "A_" + txtTokenID.Text;
                    }
                    if (ddlDocType.SelectedItem.Text == "Bank Pass Book")
                    {
                        Doc_Folder = "/ID_Proof/B_PB_Copy/";
                        token_Name = "A_" + txtTokenID.Text;
                    }
                    if (ddlDocType.SelectedItem.Text == "Others")
                    {
                        Doc_Folder = "/ID_Proof/Other_Copy/";
                        token_Name = "A_" + txtTokenID.Text;
                    }
                    if (ddlDocType.SelectedItem.Text == "Voter Card")
                    {
                        Doc_Folder = "/ID_Proof/V_Copy/";
                        token_Name = "V_" + txtTokenID.Text;
                    }
                    if (ddlDocType.SelectedItem.Text == "Ration Card")
                    {
                        Doc_Folder = "/ID_Proof/R_Copy/";
                        token_Name = "R_" + txtTokenID.Text;
                    }
                    if (ddlDocType.SelectedItem.Text == "Pan Card")
                    {
                        Doc_Folder = "/ID_Proof/P_Copy/";
                        token_Name = "P_" + txtTokenID.Text;
                    }
                    if (ddlDocType.SelectedItem.Text == "Driving Licence")
                    {
                        Doc_Folder = "/ID_Proof/DL_Copy/";
                        token_Name = "DL_" + txtTokenID.Text;
                    }
                    if (ddlDocType.SelectedItem.Text == "Smart Card")
                    {
                        Doc_Folder = "/ID_Proof/SC_Copy/";
                        token_Name = "SC_" + txtTokenID.Text;
                    }




                    //string filters = "*.jpg;*.png;*.gif";
                    string imgurl_Final = "";
                    string path_1 = UNIT_Folder + Doc_Folder;
                    string Exten;
                    Exten = ".jpg";
                    string impath = path_1 + token_Name + Exten;
                    //string FileName = Path.GetFileName(filUpload.PostedFile.FileName);
                    // string Exten = Path.GetExtension(filUpload.PostedFile.FileName);

                    if (File.Exists(impath))
                    {
                        //imgurl_Final ="~/"+ impath;
                        imgurl_Final = impath;
                        dr["imgurl"] = "Handler.ashx?f=" + imgurl_Final + ""; //imgurl_Final;
                    }
                    else
                    {
                        imgurl_Final = "~/assets/images/No_Image.jpg";
                        dr["imgurl"] = imgurl_Final;
                    }

                    //string[] filePaths = Directory.GetFiles(Server.MapPath("~/" + impath));
                    //   List<ListItem> files = new List<ListItem>();
                    //   foreach (string filePath in filePaths)
                    //   {
                    //       string fileName = Path.GetFileName(filePath);
                    //       files.Add(new ListItem(fileName, "~/Images/" + fileName));
                    //   }


                    //string Path = ConfigurationManager.AppSettings["path_1"].ToString();

                    //List<string> images = new List<string>();

                    //foreach (string filter in filters.Split(';'))
                    //{
                    //    FileInfo[] fit = new DirectoryInfo(this.Server.MapPath(Path)).GetFiles(filter);
                    //    foreach (FileInfo fi in fit)
                    //    {
                    //        images.Add(String.Format(Path + "/{0}", fi));
                    //    }
                    //}

                    //if (filUpload.HasFile)
                    //{
                    //    string FileName = Path.GetFileName(filUpload.PostedFile.FileName);

                    //    
                    //    {
                    //        filUpload.SaveAs(Server.MapPath("~/" + path_1 + token_Name + Exten));
                    //        //filUpload.SaveAs("D:/Images/" + Shade_Name.Trim() + Exten);
                    //    }
                    //}

                    //dr["imgurl"] = "Handler.ashx?f=" + imgurl_Final + ""; //imgurl_Final;
                    dt.Rows.Add(dr);
                    ViewState["ItemTable"] = dt;
                    Repeater1.DataSource = dt;
                    Repeater1.DataBind();

                    //Totalsum();


                    ddlDocType.SelectedValue = "0";
                    ddlDocType.Enabled = true;
                    txtDocNo.Text = "";
                    txtDigit.InnerText = "";
                }
            }
            else
            {
                dr = dt.NewRow();
                dr["DocType"] = ddlDocType.SelectedItem.Text;
                dr["DocNo"] = txtDocNo.Text;


                //string Shade_Name = ddlDocType.SelectedItem.Text + "_" + txtMachineID.Text;//txtShadeName.SelectedItem.Text + "-" + txtShadeNo.Text;


                //if (filUpload.HasFile)
                //{
                //    string FileName = Path.GetFileName(filUpload.PostedFile.FileName);
                //    string Exten = Path.GetExtension(filUpload.PostedFile.FileName);
                //    if (Exten == ".jpg")
                //    {
                //        //filUpload.SaveAs(Server.MapPath("~/Images/" + Shade_Name + Exten));
                //        filUpload.SaveAs("D:/Images/" + Shade_Name.Trim() + Exten);
                //    }
                //}


                string token_Name = "";

                string UNIT_Folder = "";
                string Doc_Folder = "";

                DataTable DT_Photo = new DataTable();
                string SS = "Select *from Photo_Path_Det";
                DT_Photo = objdata.RptEmployeeMultipleDetails(SS);

                string PhotoDet = "";
                if (DT_Photo.Rows.Count != 0)
                {
                    PhotoDet = DT_Photo.Rows[0]["Photo_Path"].ToString();
                }
                //if (SessionLcode.ToUpper() == "UNIT I") { UNIT_Folder = "Emp_Scan/UNIT_I/"; }
                //if (SessionLcode.ToUpper() == "UNIT II") { UNIT_Folder = "Emp_Scan/UNIT_II/"; }
                //if (SessionLcode.ToUpper() == "UNIT III") { UNIT_Folder = "Emp_Scan/UNIT_III/"; }
                //if (SessionLcode.ToUpper() == "UNIT IV") { UNIT_Folder = "//SABA-PC/To Share/Narmatha/Emp_Scan/UNIT_IV/"; } //UNIT_Folder = "Emp_Scan/UNIT_IV/"; }

                if (SessionLcode.ToUpper() == "UNIT I") { UNIT_Folder = PhotoDet + "/UNIT_I/"; }
                if (SessionLcode.ToUpper() == "UNIT II") { UNIT_Folder = PhotoDet + "/UNIT_II/"; }
                if (SessionLcode.ToUpper() == "UNIT III") { UNIT_Folder = PhotoDet + "/UNIT_III/"; }
                if (SessionLcode.ToUpper() == "UNIT IV") { UNIT_Folder = PhotoDet + "/UNIT_IV/"; } //UNIT_Folder = "Emp_Scan/UNIT_IV/"; }

                if (ddlDocType.SelectedItem.Text == "Adhar Card")
                {
                    Doc_Folder = "/ID_Proof/A_Copy/";
                    token_Name = "A_" + txtTokenID.Text;
                }
                if (ddlDocType.SelectedItem.Text == "Bank Pass Book")
                {
                    Doc_Folder = "/ID_Proof/B_PB_Copy/";
                    token_Name = "A_" + txtTokenID.Text;
                }
                if (ddlDocType.SelectedItem.Text == "Others")
                {
                    Doc_Folder = "/ID_Proof/Other_Copy/";
                    token_Name = "A_" + txtTokenID.Text;
                }
                if (ddlDocType.SelectedItem.Text == "Voter Card")
                {
                    Doc_Folder = "/ID_Proof/V_Copy/";
                    token_Name = "V_" + txtTokenID.Text;
                }
                if (ddlDocType.SelectedItem.Text == "Ration Card")
                {
                    Doc_Folder = "/ID_Proof/R_Copy/";
                    token_Name = "R_" + txtTokenID.Text;
                }
                if (ddlDocType.SelectedItem.Text == "Pan Card")
                {
                    Doc_Folder = "/ID_Proof/P_Copy/";
                    token_Name = "P_" + txtTokenID.Text;
                }
                if (ddlDocType.SelectedItem.Text == "Driving Licence")
                {
                    Doc_Folder = "/ID_Proof/DL_Copy/";
                    token_Name = "DL_" + txtTokenID.Text;
                }
                if (ddlDocType.SelectedItem.Text == "Smart Card")
                {
                    Doc_Folder = "/ID_Proof/SC_Copy/";
                    token_Name = "SC_" + txtTokenID.Text;
                }




                //string filters = "*.jpg;*.png;*.gif";
                string imgurl_Final = "";
                string path_1 = UNIT_Folder + Doc_Folder;
                string Exten;
                Exten = ".jpg";
                string impath = path_1 + token_Name + Exten;
                //string FileName = Path.GetFileName(filUpload.PostedFile.FileName);
                // string Exten = Path.GetExtension(filUpload.PostedFile.FileName);

                if (File.Exists(impath))
                {
                    //imgurl_Final = "~/" + impath;
                    imgurl_Final = impath;
                    dr["imgurl"] = "Handler.ashx?f=" + imgurl_Final + ""; //imgurl_Final;
                }
                else
                {
                    imgurl_Final = "~/assets/images/No_Image.jpg";
                    dr["imgurl"] = imgurl_Final;
                }

                //string[] filePaths = Directory.GetFiles(Server.MapPath("~/" + impath));
                //   List<ListItem> files = new List<ListItem>();
                //   foreach (string filePath in filePaths)
                //   {
                //       string fileName = Path.GetFileName(filePath);
                //       files.Add(new ListItem(fileName, "~/Images/" + fileName));
                //   }


                //string Path = ConfigurationManager.AppSettings["path_1"].ToString();

                //List<string> images = new List<string>();

                //foreach (string filter in filters.Split(';'))
                //{
                //    FileInfo[] fit = new DirectoryInfo(this.Server.MapPath(Path)).GetFiles(filter);
                //    foreach (FileInfo fi in fit)
                //    {
                //        images.Add(String.Format(Path + "/{0}", fi));
                //    }
                //}

                //if (filUpload.HasFile)
                //{
                //    string FileName = Path.GetFileName(filUpload.PostedFile.FileName);

                //    
                //    {
                //        filUpload.SaveAs(Server.MapPath("~/" + path_1 + token_Name + Exten));
                //        //filUpload.SaveAs("D:/Images/" + Shade_Name.Trim() + Exten);
                //    }
                //}

                //dr["imgurl"] = "Handler.ashx?f=" + imgurl_Final + ""; //imgurl_Final;
                dt.Rows.Add(dr);
                ViewState["ItemTable"] = dt;
                Repeater1.DataSource = dt;
                Repeater1.DataBind();

                //Totalsum();


                ddlDocType.SelectedValue = "0";
                ddlDocType.Enabled = true;
                txtDocNo.Text = "";
                txtDigit.InnerText = "";
            }
        }
    }
    protected void rbtnPhysically_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rbtnPhysically.SelectedValue == "1")
        {
            txtPhyReason.Enabled = true;
        }
        else
        {
            txtPhyReason.Enabled = false;
        }
    }
    protected void dbtnActive_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (dbtnActive.SelectedValue == "2")
        {
            txtReason.Enabled = true;
        }
        else
        {
            txtReason.Enabled = false;
        }
    }




    protected void GridDeleteClick_Certificate(object sender, CommandEventArgs e)
    {
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;
        string qry = "";

        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["CertTable"];
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i]["Certificate_No"].ToString() == e.CommandName.ToString())
            {
                dt.Rows.RemoveAt(i);
                dt.AcceptChanges();
            }
        }
        ViewState["CertTable"] = dt;
        Load_OLD_data1();
    }

    protected void chkAdolescent_CheckedChanged(object sender, EventArgs e)
    {
        if (chkAdolescent.Checked == true)
        {
            chkAge18Complete.Enabled = true;
        }
        else
        {
            chkAge18Complete.Checked = false;
            chkAge18Complete.Enabled = false;
            txtAge18Comp_Date.Enabled = false;
        }
    }

    protected void chkAge18Complete_CheckedChanged(object sender, EventArgs e)
    {
        if (chkAge18Complete.Checked == true)
        {
            txtAge18Comp_Date.Enabled = true;
        }
        else
        {
            txtAge18Comp_Date.Enabled = false;
        }
    }

    protected void txtRefType_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtRefMobileNo.Enabled = false;
        txtAgentName.Enabled = false;
        txtRefParentsName.Enabled = false;

        txtCommissionAmt.Enabled = false;
        txtCommissionAmt.Text = "0";

        txtAgentName.SelectedValue = "-Select-";
        txtRefParentsName.Text = "";
        txtRefMobileNo.Text = "";
        if (txtRefType.SelectedItem.Text == "Agent")
        {
            txtRefMobileNo.Enabled = true;
            txtAgentName.Enabled = true;
            txtRefParentsName.Enabled = false;
            txtCommissionAmt.Enabled = false;
        }
        else if (txtRefType.SelectedItem.Text == "Parent")
        {
            txtAgentName.Enabled = false;
            txtRefParentsName.Enabled = true;
            txtRefMobileNo.Enabled = true;
            txtCommissionAmt.Enabled = true;
        }
    }

    protected void txtAgentName_SelectedIndexChanged(object sender, EventArgs e)
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        if (txtAgentName.SelectedItem.Text != "-Select-")
        {
            query = "Select * from MstAgent where AgentName='" + txtAgentName.SelectedItem.Text + "'";
            dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        }

        if (dtdsupp.Rows.Count != 0)
        {
            txtRefMobileNo.Text = dtdsupp.Rows[0]["Mobile"].ToString();
            txtCommissionAmt.Text = dtdsupp.Rows[0]["Commission"].ToString();
        }
        else
        {
            txtRefMobileNo.Text = "";
            txtCommissionAmt.Text = "0";
        }
    }

    protected void ddlDocType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlDocType.SelectedItem.Text == "Adhar Card")
        {
            txtDigit.InnerText = "12-digit";
        }
        else if (ddlDocType.SelectedItem.Text == "Voter Card")
        {
            txtDigit.InnerText = "10-digit";
        }
        else if (ddlDocType.SelectedItem.Text == "Ration Card")
        {
            txtDigit.InnerText = "12-digit";
        }
        else if (ddlDocType.SelectedItem.Text == "Pan Card")
        {
            txtDigit.InnerText = "10-digit";
        }
        else if (ddlDocType.SelectedItem.Text == "Driving Licence")
        {
            txtDigit.InnerText = "16-digit";
        }
        else if (ddlDocType.SelectedItem.Text == "Smart Card")
        {
            txtDigit.InnerText = "19-digit";
        }
        else if (ddlDocType.SelectedItem.Text == "Bank Pass Book")
        {
            txtDigit.InnerText = "";
        }
        else if (ddlDocType.SelectedItem.Text == "Others")
        {
            txtDigit.InnerText = "";
        }
        else if (ddlDocType.SelectedItem.Text == "Passport")
        {
            txtDigit.InnerText = "8-digit";
        }
        else
        {
            txtDigit.InnerText = "";
        }

    }
    protected void chkExment_CheckedChanged(object sender, EventArgs e)
    {
        if (chkExment.Checked == true)
        {
            txtFormIDate.Enabled = true;
        }
        else
        {
            txtFormIDate.Text = "";
            txtFormIDate.Enabled = false;
        }
    }

    protected void ddlWagesType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlWagesType.SelectedItem.Text.ToUpper() == "HOSTEL")
        {
            ddlWorkType.Enabled = true;
            //ddlHostelExp.Enabled = true;
        }
        else
        {
            ddlWorkType.Enabled = false;
            //ddlHostelExp.Enabled = false;
        }
    }
    protected void ddlBankName_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        string query = "";
        query = "Select *from MstBank where BankName='" + ddlBankName.SelectedItem.Text + "'";
        dt = objdata.RptEmployeeMultipleDetails(query);

        if (dt.Rows.Count != 0)
        {
            txtIFSC.Text = dt.Rows[0]["IFSCCode"].ToString();
            txtBranch.Text = dt.Rows[0]["Branch"].ToString();
        }


    }

    protected void btnCancel_Approve_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        if (txtCanecel_Reason_Approve.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Reason To Cancel..!');", true);
            txtCanecel_Reason_Approve.Focus();
        }

        if (!ErrFlag)
        {
            //Update Employee Status
            SSQL = "Update Employee_Mst_Status set Emp_Status='Cancel',Cancel_Reson='" + txtCanecel_Reason_Approve.Text + "'";
            SSQL = SSQL + " Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And MachineID='" + txtMachineID.Text + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Employee Cancelled Successfully..!');", true);

            Clear_All_Field();
            Response.Redirect("EmployeeApproval.aspx");
        }
    }

    protected void btnApprove_Click(object sender, EventArgs e)
    {

        DataTable DT = new DataTable();
        DataTable DT_Check = new DataTable();
        SSQL = "Select * from Employee_Mst_New_Emp Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And MachineID='" + txtMachineID.Text.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT.Rows.Count != 0)
        {
            DataTable Cancel_DT = new DataTable();
            SSQL = "Select * from Employee_Mst_Status Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And MachineID='" + txtMachineID.Text.ToString() + "'";
            SSQL = SSQL + " And Emp_Status='Cancel'";
            Cancel_DT = objdata.RptEmployeeMultipleDetails(SSQL);

            if (Cancel_DT.Rows.Count == 0)
            {
                //Insert Employee Master Table
                SSQL = "Select * from Employee_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And MachineID='" + txtMachineID.Text.ToString() + "'";
                DT_Check = objdata.RptEmployeeMultipleDetails(SSQL);
                if (DT_Check.Rows.Count != 0)
                {
                    SSQL = "Delete from Employee_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And MachineID='" + txtMachineID.Text.ToString() + "'";
                    objdata.RptEmployeeMultipleDetails(SSQL);
                }

                SSQL = "INSERT INTO Employee_Mst Select * from Employee_Mst_New_Emp";
                SSQL = SSQL + " Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And MachineID='" + txtMachineID.Text.ToString() + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);



                //Employee Level
                DataTable DT_Chk = new DataTable();
                SSQL = "Select * from Employee_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And MachineID='" + txtMachineID.Text.ToString() + "'";
                DT_Check = objdata.RptEmployeeMultipleDetails(SSQL);
                string EmpLevel = ""; string ExistingCode = "";
                if (DT_Check.Rows.Count != 0)
                {
                    EmpLevel = DT_Check.Rows[0]["EmpLevel"].ToString();
                    ExistingCode = DT_Check.Rows[0]["ExistingCode"].ToString();
                    if (EmpLevel == "Semi-Exp")
                    {
                        DataTable DT_Level = new DataTable();

                        SSQL = "Select *from Training_Level_Change where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                        SSQL = SSQL + " And EmpNo='" + txtMachineID.Text.ToString() + "'";
                        DT_Level = objdata.RptEmployeeMultipleDetails(SSQL);

                        if (DT_Level.Rows.Count == 0)
                        {
                            SSQL = "insert into Training_Level_Change(Ccode,Lcode,EmpNo,MachineID,ExistingCode,";
                            SSQL = SSQL + "Training_Level,Level_Date)values('" + SessionCcode + "','" + SessionLcode + "',";
                            SSQL = SSQL + "'" + txtMachineID.Text.ToString() + "','" + txtMachineID.Text.ToString() + "',";
                            SSQL = SSQL + "'" + ExistingCode + "','Semi-Exp',convert(varchar,GETDATE(),103))";
                            objdata.RptEmployeeMultipleDetails(SSQL);
                        }
                    }
                }


                //Update Employee Status
                SSQL = "Update Employee_Mst_Status set Emp_Status='Completed',Cancel_Reson=''";
                SSQL = SSQL + " Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And MachineID='" + txtMachineID.Text.ToString() + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);

                SSQL = "Delete from Employee_Mst_New_Emp Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And MachineID='" + txtMachineID.Text.ToString() + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Employee Approved Successfully..!');", true);

                Clear_All_Field();
                Response.Redirect("EmployeeApproval.aspx");
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Employee get Cancelled.. Cannot Approve..!');", true);
            }
        }
    }
    public void Load_RoomNo()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtHostelRoom.Items.Clear();
        query = "Select RoomNoCode,RoomNumber from MstRoomNo where RoomName='" + ddlRoomName.SelectedValue + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtHostelRoom.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["RoomNoCode"] = "0";
        dr["RoomNumber"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtHostelRoom.DataTextField = "RoomNumber";
        txtHostelRoom.DataValueField = "RoomNoCode";
        txtHostelRoom.DataBind();
    }
    protected void ddlRoomName_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_RoomNo();
    }

    protected void rbtnHourSalary_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void rbtnFixed_Salary_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void rbtEligibleDayincentive_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rbtEligibleDayincentive.SelectedValue == "1")
        {
            txtAllowance1.Enabled = true;

        }
        else
        {
            txtAllowance1.Enabled = false;
        }
    }
}
