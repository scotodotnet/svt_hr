﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="HindiGroupCommision.aspx.cs" Inherits="Default2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:UpdatePanel ID="SalPay" runat="server">
                 <ContentTemplate>

    <%-- <!-- begin page-header -->
			<h1 class="page-header">Hindi Boys Commision Details</h1>
			<!-- end page-header -->
     <!-- begin row -->
                     
                            <div class="col-md-4">
                           <div class="form-group">
    <asp:Label ID="lblHBCD" runat="server" class="user-name" 
                                       Font-Bold="True" Font-Size="16" Font-Names="Aharoni" Text="Hindi Boys Commision Details" ></asp:Label>
        
                   </div>
                   </div>
                           <!-- end col-4 -->
        <asp:DropDownList runat="server" ID="ddl_HindiGroupName" class="form-control select2" 
                                   style="width: 100%;" AutoPostBack="true">
                           </asp:DropDownList>
                  <asp:TextBox runat="server" ID="txt_Commision_Days" ></asp:TextBox>
                  <asp:TextBox runat="server" ID="txt_Commision_Perday" ></asp:TextBox>
                  <asp:TextBox runat="server" ID="txt_Commision_Grand" ></asp:TextBox>
                  <asp:Button runat="server" id="btnSave" Text="Save" class="btn btn-success" />
                  <asp:Button runat="server" id="btnClear" Text="Clear" class="btn btn-danger"  />--%>
    <div id="content" class="content">	
    <div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">Hindi Group Commission Details</h4>
                        </div>
                        <div class="panel-body">
                        <div class="form-group">
                          
                         
                            <!-- begin row -->
                          <div class="row">
                           <!-- begin col-4 -->
                           <div class="col-md-4">
                           <div class="form-group">
                           <label>Group Name</label>
                           <asp:DropDownList runat="server" ID="ddl_GroupName" class="form-control select2" 
                                   style="width: 100%;" AutoPostBack="true" OnSelectedIndexChanged="ddl_GroupName_SelectedIndexChanged">
                           </asp:DropDownList>
                          <%-- <asp:HiddenField ID="txtMachineID" runat="server" />
                           <asp:RequiredFieldValidator ControlToValidate="txtExistingCode" InitialValue="-Select-" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                           </asp:RequiredFieldValidator>--%>
                            </div>
                           </div>
                           <!-- end col-4 -->
                           <!-- begin col-4 -->
                            <div class="col-md-4">
							   <div class="form-group">
								<label>Total Days </label>
								<%--<asp:Label runat="server" ID="txtTotalDays" class="form-control" BackColor="#F3F3F3"></asp:Label>--%>
                                     <asp:TextBox runat="server" ID="txt_Commision_Days"  Class="form-control" ></asp:TextBox>
								
								</div>
                             </div>
                           <!-- end col-4 -->
                           <!-- begin col-4 -->
                            <div class="col-md-4">
							   <div class="form-group">
								<label>Commission Per Day</label>
								<%--<asp:Label runat="server" ID="txtDepartment" class="form-control" BackColor="#F3F3F3"></asp:Label>--%>
                                   <asp:TextBox runat="server" ID="txt_Commision_Perday" class="form-control" AutoPostBack="True" OnTextChanged="txt_Commision_Perday_TextChanged"></asp:TextBox>
								</div>
                             </div>
                           <!-- end col-4 -->
                             <!-- begin col-4 -->
                            <div class="col-md-4">
							   <div class="form-group">
								<label>Grand Total</label>
								<%--<asp:Label runat="server" ID="txtDepartment" class="form-control" BackColor="#F3F3F3"></asp:Label>--%>
                                   <asp:TextBox runat="server" ID="txt_Grand_Total" class="form-control"></asp:TextBox>
								</div>
                             </div>
                          </div>
                        <!-- end row -->
                             <div class="row">
                         <div class="col-md-4"></div>
                           <!-- begin col-4 -->
                                <div class="col-md-4">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btnSave" Text="Save" class="btn btn-success" 
                                        ValidationGroup="Validate_Field" OnClick="btnSave_Click"/>
                                   <%--  <asp:Button runat="server" id="btnUpdate" Text="Update" class="btn btn-success" 
                                        ValidationGroup="Validate_Field" Onclick="btnUpdate_Click"/>--%>
                                    
									<asp:Button runat="server" id="btnClear" Text="Clear" class="btn btn-danger" OnClick="btnClear_Click" />
								
                               </div>
                              <!-- end col-4 -->
                              <div class="col-md-4"></div>
                         </div>
                        <!-- end row -->  
                        
    </div>
                            <!-- table start -->
					<div class="col-md-12">
					    <div class="row">
					        <asp:Repeater ID="Repeater1" runat="server">
			                    <HeaderTemplate>
                                    <table id="example" class="display table">
                                        <thead>
                                            <tr>
                                                
                                                <%--<th>Machine ID</th>--%>
                                                 <th visible="false" >Sno</th>
                                                <th>Group Name</th>
                                                <th>Total Days</th>
                                                <th>Per Day</th>
                                                <th>Grand Total</th>
                                              <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                      <%--<td><%# Eval("MachineID")%></td>--%>
                                        <td visible="false"><%# Eval("Sno")%></td>
                                        <td><%# Eval("GroupName")%></td>
                                     
                                        <td><%# Eval("TotalDays")%></td>
                                        
                                        <td><%# Eval("PerDay")%></td>
                                       
                                        <td><%# Eval("GrandTotal")%></td>
                                        
                                      <%--  <td><%# Eval("Dept_Name")%></td>
                                        <td><%# Eval("SPG_Inc")%></td>--%>
                                        
                                       <%--<td>
                                        <asp:LinkButton ID="btnEditIssueEntry_Grid" class="btn btn-primary btn-sm fa fa-pencil"  runat="server" 
                                                        Text="" OnCommand="GridEditEntryClick" CommandArgument='<%# Eval("OTDate_Str")+","+Eval("OTHrs")%>' CommandName='<%# Eval("MachineID")%>'>
                                        </asp:LinkButton>
                                       
                                        <asp:LinkButton ID="btnDeleteEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                          Text="" OnCommand="GridDeleteEnquiryClick" CommandArgument='<%# Eval("OTDate_Str")+","+Eval("OTHrs")%>' CommandName='<%# Eval("MachineID")%>' 
                                         CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this OT details?');">
                                        </asp:LinkButton>
                                        </td>--%>
                                         <td>
                                     <asp:LinkButton ID="btnEditEnquiry_Grid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                                           Text="" OnCommand="GridEditEnquiryClick" CommandArgument="Edit" CommandName='<%# Eval("Sno")%>'>
                                     </asp:LinkButton>
                                     <asp:LinkButton ID="btnDeleteEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                        Text="" OnCommand="GridDeleteEnquiryClick" CommandArgument="Delete" CommandName='<%# Eval("Sno")%>' 
                                       CausesValidation="true" OnClientClick="return confirm('Are you sure you want to Delete this Voucher details?');">
                                     </asp:LinkButton>
                                    
                                    </td>
                                       
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater>
					       <%-- <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:VPS_SpayConnectionString %>" SelectCommand="SELECT [GroupName], [TotalDays], [PerDay], [GrandTotal] FROM [HindiBoysCommission]"></asp:SqlDataSource>--%>
					    </div>
					</div>
					<!-- table End -->
    </div>
                        </div>
                    </div>
                    </div>
                    </div>

    </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSave" />
        </Triggers>
</asp:UpdatePanel>

</asp:Content>

