﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.Collections.Generic;
using System.IO;


public partial class SalaryCoverAbstract : System.Web.UI.Page
{

    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string mIpAddress_IN;
    string mIpAddress_OUT;
    string SSQL;
    DataTable mEmployeeDS = new DataTable();
    DataTable AutoDTable = new DataTable();
    DataTable mDataTable = new DataTable();
    DataSet ds = new DataSet();


    string Date;
    string Date2;
    string Payfromdate;
    string PayTodate;
    DataTable mLocalDS_INTAB = new DataTable();
    DataTable mLocalDS_OUTTAB = new DataTable();
    string Time_IN_Str = "";
    string Time_Out_Str = "";
    Int32 time_Check_dbl = 0;
    string Total_Time_get = "";

    DataTable Payroll_DS = new DataTable();
    DataTable DataCells = new DataTable();
    DataTable mDataSet = new DataTable();
    DataTable dt = new DataTable();

    string State;
    string Division;
    string WagesType;
    string SessionPayroll;

    DataRow dtRow;

    public string Left_Val(string Value, int Length)
    {
        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            Load_DB();
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report-Salary Cover Abstract";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("masterpage"));
                //li.Attributes.Add("class", "droplink active open");
            }

            string SessionCcode = Session["Ccode"].ToString();
            string SessionLcode = Session["Lcode"].ToString();
            string SessionUserType = Session["Isadmin"].ToString();
            Date = Request.QueryString["FromDate"].ToString();
            Date2 = Request.QueryString["ToDate"].ToString();
            Payfromdate = Request.QueryString["PayFromDate"].ToString();
            PayTodate = Request.QueryString["payToDate"].ToString();
            Division = Request.QueryString["Division"].ToString();
            //string TempWages = Request.QueryString["Wages"].ToString();
            //WagesType = TempWages.Replace("_", "&");



            DataTable New = new DataTable();
            string Time_IN_Str = "";
            string Time_Out_Str = "";
            int time_Check_dbl = 0;
            string Total_Time_get = "";
            DataTable Payroll_DS = new DataTable();


            AutoDTable.Columns.Add("SNo");
            AutoDTable.Columns.Add("Dept");
            AutoDTable.Columns.Add("Type");
            AutoDTable.Columns.Add("Shift");
            AutoDTable.Columns.Add("EmpCode");
            AutoDTable.Columns.Add("Ex.Code");
            AutoDTable.Columns.Add("Name");
            AutoDTable.Columns.Add("TimeIN");
            AutoDTable.Columns.Add("TimeOUT");
            AutoDTable.Columns.Add("MachineID");
            AutoDTable.Columns.Add("Category");
            AutoDTable.Columns.Add("SubCategory");
            AutoDTable.Columns.Add("CoverDate");
            AutoDTable.Columns.Add("PayType");
            AutoDTable.Columns.Add("PayAmount");
             AutoDTable.Columns.Add("BankAmount");

         

            DataTable mLocalDS = new DataTable();
            int mStartINRow = 0;
            int mStartOUTRow = 0;

            for (int iTabRow = 0; iTabRow < 1; iTabRow++)
            {
                if (AutoDTable.Rows.Count <= 1)
                {
                    mStartOUTRow = 1;
                }
                else
                {
                    mStartOUTRow = AutoDTable.Rows.Count - 1;
                }
                // 'Employee Master

                SSQL = "";
                SSQL = "Select * from Employee_Mst where Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                SSQL = SSQL + " And (Wages='CIVIL' or Wages='HOSTEL' or Wages='REGULAR' or Wages='SUB-STAFF')";
                // 'SSQL = SSQL & " And IsActive='Yes'";
                if (Division != "-Select-")
                {
                    SSQL = SSQL + " And Division = '" + Division + "'";
                }

                SSQL = SSQL + "Order by ExistingCode Asc";

                mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);

                if (mDataSet.Rows.Count > 0)
                {
                    for (int iRow = 0; iRow < mDataSet.Rows.Count; iRow++)
                    {
                        //'Check Payroll Salary Cover

                        DataTable Salary_DS = new DataTable();

                        //'Get Payroll EmpNo
                        string Payroll_EmpNo = "";
                        string Payroll_EmployeeType = "";
                        string Payroll_SalaryType = "";
                        string Bank_Query = "";
                        DataTable Bank_Ds = new DataTable();

                        SSQL = "Select EM.EmpNo,EM.Salary_Through as Salarythrough,ET.EmpTypeCd as EmployeeType from Employee_Mst EM inner join MstEmployeeType ET";
                        SSQL = SSQL + " on EM.Wages=ET.EmpType";
                        SSQL = SSQL + " where EM.MachineID='" + mDataSet.Rows[iRow]["MachineID"].ToString() + "'";
                        SSQL = SSQL + " And EM.CompCode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "'";
                        // SSQL = SSQL + " And OP.Ccode='" + SessionCcode  + "' And OP.Lcode='" + SessionLcode  + "'";
                        //  SSQL = SSQL + " And EM.EmpNo=OP.EmpNo";
                        Salary_DS = objdata.RptEmployeeMultipleDetails(SSQL);

                        if (Salary_DS.Rows.Count != 0)
                        {
                            Payroll_EmpNo = "";
                            Payroll_EmployeeType = "";
                            Payroll_SalaryType = "";
                            Bank_Query = "";
                            Payroll_EmpNo = Salary_DS.Rows[0]["EmpNo"].ToString();
                            Payroll_EmployeeType = Salary_DS.Rows[0]["EmployeeType"].ToString();
                            Payroll_SalaryType = Salary_DS.Rows[0]["Salarythrough"].ToString();

                            double Gross_Earnings = 0;
                            double Basic_Salary = 0;
                            double Extra_Allowence = 0;
                            double Total_Deduction = 0;
                            double Bank_Salary = 0;
                            double Bank_Emp_Salary = 0;
                            double NFH_Worked_Days = 0;
                            double NFH_Worked_Days_Statutory = 0;
                            double NFH_Worked_Amt = 0;
                            double NFH_Worked_Amt_Statutory = 0;
                            double Without_Round_off_Net_Amount = 0;
                            double With_Round_Off_Net_Amount = 0;
                            double Final_Display_Net_Pay = 0;
                            DataTable CalCulate_Amt_DS = new DataTable();

                        
                            DateTime payfromdate1 = Convert.ToDateTime(Date.ToString());
                            DateTime paytodate1 = Convert.ToDateTime(Date2.ToString());


                            DateTime payfromdate2 = Convert.ToDateTime(Payfromdate.ToString());
                            DateTime paytodate2 = Convert.ToDateTime(PayTodate.ToString());

                            // 'Get NFH_Worked Days
                            SSQL = "Select AD.NFH_Work_Days_Statutory,AD.NFH_Work_Days,EM.EmpNo from [" + SessionPayroll + "]..AttenanceDetails AD,Employee_Mst EM where EM.EmpNo=AD.EmpNo";
                            SSQL = SSQL + " And EM.MachineID='" + mDataSet.Rows[iRow]["MachineID"].ToString() + "' And EM.CompCode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "'";
                            SSQL = SSQL + " And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "'";
                            SSQL = SSQL + " And AD.FromDate ='" + payfromdate1.AddDays(0).ToString("yyyy/MM/dd") + "'";
                            SSQL = SSQL + " And AD.ToDate ='" + paytodate1.AddDays(0).ToString("yyyy/MM/dd") + "'";
                            CalCulate_Amt_DS = objdata.RptEmployeeMultipleDetails(SSQL);
                            if (CalCulate_Amt_DS.Rows.Count != 0)
                            {
                                if (CalCulate_Amt_DS.Rows[0]["NFH_Work_Days"] == "")
                                {
                                    NFH_Worked_Days = 0;
                                }
                                else
                                {
                                    NFH_Worked_Days = Convert.ToDouble(CalCulate_Amt_DS.Rows[0]["NFH_Work_Days"].ToString());
                                }
                                if (CalCulate_Amt_DS.Rows[0]["NFH_Work_Days_Statutory"] == "")
                                {
                                    NFH_Worked_Days_Statutory = 0;
                                }
                                else
                                {
                                    NFH_Worked_Days_Statutory = Convert.ToDouble(CalCulate_Amt_DS.Rows[0]["NFH_Work_Days_Statutory"].ToString());
                                }

                            }
                            else
                            {
                                 NFH_Worked_Days = 0;
                                 NFH_Worked_Days_Statutory = 0;
                            }
                            if (Payroll_EmployeeType == "1" || Payroll_EmployeeType == "6")
                            {
                                NFH_Worked_Days = 0;
                                NFH_Worked_Days_Statutory = 0;
                            }

                            SSQL = "Select SD.BasicAndDANew,SD.BasicHRA,SD.ConvAllow,SD.EduAllow,SD.MediAllow,SD.BasicRAI,SD.WashingAllow,";
                            SSQL = SSQL + " SD.allowances1,SD.allowances2,SD.allowances3,SD.allowances4,SD.allowances5,SD.FullNightAmt,";
                            SSQL = SSQL + " SD.ThreesidedAmt,SD.DayIncentive,SD.HalfNightAmt,SD.Basic_SM,SD.TotalDeductions,ED.EmpNo,";

                            SSQL = SSQL + " SD.Basic_Spl_Allowance,SD.Basic_Uniform,SD.Basic_Vehicle_Maintenance,";
                            SSQL = SSQL + " SD.Basic_Communication,SD.Basic_Journ_Perid_Paper,SD.Basic_Incentives";

                            SSQL = SSQL + " from [" + SessionPayroll + "]..SalaryDetails SD,Employee_Mst ED where ED.EmpNo=SD.EmpNo";
                            SSQL = SSQL + " And ED.MachineID='" + mDataSet.Rows[iRow]["MachineID"].ToString() + "' And ED.CompCode='" + SessionCcode + "' And ED.LocCode='" + SessionLcode + "'";
                            SSQL = SSQL + " And SD.Ccode='" + SessionCcode + "' And SD.Lcode='" + SessionLcode + "'";
                            SSQL = SSQL + " And SD.FromDate ='" + payfromdate1.AddDays(0).ToString("yyyy/MM/dd") + "'";
                            SSQL = SSQL + " And SD.ToDate ='" + paytodate1.AddDays(0).ToString("yyyy/MM/dd") + "'";

                            //  'Left Date Check
                            //if(chkLeftDate.Checked=true)
                            //{
                            //    SSQL = SSQL + " And (ED.DOR > '" & txtLeftDate.Value.AddDays(0).ToString("yyyy/MM/dd") & "' Or ED.ActivateMode='Y')"

                            //}

                            Salary_DS = objdata.RptEmployeeMultipleDetails(SSQL);
                            if (Salary_DS.Rows.Count != 0)
                            {
                                //'Get Total Aamount

                                Basic_Salary = 0;
                                if (Salary_DS.Rows[0]["BasicAndDANew"] == "") { Basic_Salary = Basic_Salary + 0; } else { Basic_Salary = Basic_Salary + Convert.ToDouble(Salary_DS.Rows[0]["BasicAndDANew"].ToString()); }
                                if (Salary_DS.Rows[0]["BasicHRA"] == "") { Basic_Salary = Basic_Salary + 0; } else { Basic_Salary = Basic_Salary + Convert.ToDouble(Salary_DS.Rows[0]["BasicHRA"].ToString()); }
                                if (Salary_DS.Rows[0]["ConvAllow"] == "") { Basic_Salary = Basic_Salary + 0; } else { Basic_Salary = Basic_Salary + Convert.ToDouble(Salary_DS.Rows[0]["ConvAllow"].ToString()); }
                                if (Salary_DS.Rows[0]["EduAllow"] == "") { Basic_Salary = Basic_Salary + 0; } else { Basic_Salary = Basic_Salary + Convert.ToDouble(Salary_DS.Rows[0]["EduAllow"].ToString()); }
                                if (Salary_DS.Rows[0]["MediAllow"] == "") { Basic_Salary = Basic_Salary + 0; } else { Basic_Salary = Basic_Salary + Convert.ToDouble(Salary_DS.Rows[0]["MediAllow"].ToString()); }
                                if (Salary_DS.Rows[0]["BasicRAI"] == "") { Basic_Salary = Basic_Salary + 0; } else { Basic_Salary = Basic_Salary + Convert.ToDouble(Salary_DS.Rows[0]["BasicRAI"].ToString()); }
                                if (Salary_DS.Rows[0]["WashingAllow"] == "") { Basic_Salary = Basic_Salary + 0; } else { Basic_Salary = Basic_Salary + Convert.ToDouble(Salary_DS.Rows[0]["WashingAllow"].ToString()); }

                                if (Salary_DS.Rows[0]["Basic_Spl_Allowance"] == "") { Basic_Salary = Basic_Salary + 0; } else { Basic_Salary = Basic_Salary + Convert.ToDouble(Salary_DS.Rows[0]["Basic_Spl_Allowance"].ToString()); }
                                if (Salary_DS.Rows[0]["Basic_Uniform"] == "") { Basic_Salary = Basic_Salary + 0; } else { Basic_Salary = Basic_Salary + Convert.ToDouble(Salary_DS.Rows[0]["Basic_Uniform"].ToString()); }
                                if (Salary_DS.Rows[0]["Basic_Vehicle_Maintenance"] == "") { Basic_Salary = Basic_Salary + 0; } else { Basic_Salary = Basic_Salary + Convert.ToDouble(Salary_DS.Rows[0]["Basic_Vehicle_Maintenance"].ToString()); }
                                if (Salary_DS.Rows[0]["Basic_Communication"] == "") { Basic_Salary = Basic_Salary + 0; } else { Basic_Salary = Basic_Salary + Convert.ToDouble(Salary_DS.Rows[0]["Basic_Communication"].ToString()); }
                                if (Salary_DS.Rows[0]["Basic_Journ_Perid_Paper"] == "") { Basic_Salary = Basic_Salary + 0; } else { Basic_Salary = Basic_Salary + Convert.ToDouble(Salary_DS.Rows[0]["Basic_Journ_Perid_Paper"].ToString()); }
                                if (Salary_DS.Rows[0]["Basic_Incentives"] == "") { Basic_Salary = Basic_Salary + 0; } else { Basic_Salary = Basic_Salary + Convert.ToDouble(Salary_DS.Rows[0]["Basic_Incentives"].ToString()); }


                                Extra_Allowence = 0;
                                if (Salary_DS.Rows[0]["allowances1"] == "") { Extra_Allowence = Extra_Allowence + 0; } else { Extra_Allowence = Extra_Allowence + Convert.ToDouble(Salary_DS.Rows[0]["allowances1"].ToString()); }
                                if (Salary_DS.Rows[0]["allowances2"] == "") { Extra_Allowence = Extra_Allowence + 0; } else { Extra_Allowence = Extra_Allowence + Convert.ToDouble(Salary_DS.Rows[0]["allowances2"].ToString()); }
                                if (Salary_DS.Rows[0]["allowances3"] == "") { Extra_Allowence = Extra_Allowence + 0; } else { Extra_Allowence = Extra_Allowence + Convert.ToDouble(Salary_DS.Rows[0]["allowances3"].ToString()); }
                                if (Salary_DS.Rows[0]["allowances4"] == "") { Extra_Allowence = Extra_Allowence + 0; } else { Extra_Allowence = Extra_Allowence + Convert.ToDouble(Salary_DS.Rows[0]["allowances4"].ToString()); }
                                if (Salary_DS.Rows[0]["allowances5"] == "") { Extra_Allowence = Extra_Allowence + 0; } else { Extra_Allowence = Extra_Allowence + Convert.ToDouble(Salary_DS.Rows[0]["allowances5"].ToString()); }
                                if (Salary_DS.Rows[0]["FullNightAmt"] == "") { Extra_Allowence = Extra_Allowence + 0; } else { Extra_Allowence = Extra_Allowence + Convert.ToDouble(Salary_DS.Rows[0]["ThreesidedAmt"].ToString()); }
                                if (Salary_DS.Rows[0]["ThreesidedAmt"] == "") { Extra_Allowence = Extra_Allowence + 0; } else { Extra_Allowence = Extra_Allowence + Convert.ToDouble(Salary_DS.Rows[0]["ThreesidedAmt"].ToString()); }
                                if (Salary_DS.Rows[0]["DayIncentive"] == "") { Extra_Allowence = Extra_Allowence + 0; } else { Extra_Allowence = Extra_Allowence + Convert.ToDouble(Salary_DS.Rows[0]["DayIncentive"].ToString()); }
                                if (Salary_DS.Rows[0]["HalfNightAmt"] == "") { Extra_Allowence = Extra_Allowence + 0; } else { Extra_Allowence = Extra_Allowence + Convert.ToDouble(Salary_DS.Rows[0]["HalfNightAmt"].ToString()); }


                                if (Salary_DS.Rows[0]["Basic_SM"] == "") { NFH_Worked_Amt = NFH_Worked_Days * 0; } else { NFH_Worked_Amt = NFH_Worked_Days * Convert.ToDouble(Salary_DS.Rows[0]["Basic_SM"].ToString()); }

                                if (Salary_DS.Rows[0]["Basic_SM"] == "") { NFH_Worked_Amt_Statutory = NFH_Worked_Days_Statutory * 0; } else { NFH_Worked_Amt_Statutory = NFH_Worked_Days_Statutory * Convert.ToDouble(Salary_DS.Rows[0]["Basic_SM"].ToString()); }

                                Gross_Earnings = Basic_Salary + Extra_Allowence + NFH_Worked_Amt;
                                if (Salary_DS.Rows[0]["TotalDeductions"] == "") { Total_Deduction = 0; } else { Total_Deduction = Convert.ToDouble(Salary_DS.Rows[0]["TotalDeductions"].ToString()); }



                                Basic_Salary = Basic_Salary + NFH_Worked_Amt_Statutory;
                                Bank_Salary = Basic_Salary - Total_Deduction;
                                Bank_Salary = Math.Truncate((Bank_Salary + 5) / 10) * 10;
                                // 'Bank_Salary = Bank_Salary + ((10 - (Bank_Salary Mod 10)) Mod 5)

                                Without_Round_off_Net_Amount = Gross_Earnings - Total_Deduction;
                                With_Round_Off_Net_Amount = Math.Truncate((Without_Round_off_Net_Amount + 5) / 10) * 10;
                                //'With_Round_Off_Net_Amount = Without_Round_off_Net_Amount + ((5 - (Without_Round_off_Net_Amount Mod 10)) Mod 10)
                                Bank_Emp_Salary = With_Round_Off_Net_Amount - Bank_Salary;


                                if (Payroll_EmployeeType == "1" || Payroll_EmployeeType == "6")
                                {
                                    Final_Display_Net_Pay = With_Round_Off_Net_Amount;
                                }
                                else
                                {
                                    if (Left_Val(Payroll_SalaryType, 1) == "2")
                                    {
                                        Final_Display_Net_Pay = Bank_Emp_Salary;
                                    }
                                    else
                                    {
                                        Final_Display_Net_Pay = With_Round_Off_Net_Amount;
                                    }
                                }




                                AutoDTable.NewRow();
                                AutoDTable.Rows.Add();

                                AutoDTable.Rows[mStartINRow][14] = Final_Display_Net_Pay;

                                if(Left_Val(Payroll_SalaryType,1) =="2")
                                {
                                    AutoDTable.Rows[mStartINRow][15]=Bank_Salary;
                                }
                                else
                                {
                                    AutoDTable.Rows[mStartINRow][15]="0.00";
                                }
                                


                                //'Get Cover Date Punch 
                                SSQL = "Select Min(Payout) as [TimeIN] from LogTime_SALARY Where Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                SSQL = SSQL + " And Payout >='" + payfromdate2.AddDays(0).ToString("yyyy/MM/dd") + " " + "00:01' ";
                                SSQL = SSQL + " And Payout <='" + paytodate2.AddDays(0).ToString("yyyy/MM/dd") + " " + "23:59' ";
                                SSQL = SSQL + " And MachineID='" + mDataSet.Rows[iRow]["MachineID_Encrypt"] + "'";
                                SSQL = SSQL + " Order By Min(Payout)";
                                Salary_DS = objdata.RptEmployeeMultipleDetails(SSQL);
                                if (Salary_DS.Rows.Count != 0)
                                {
                                    if (Salary_DS.Rows[0]["TimeIN"].ToString() != "")
                                    {
                                        AutoDTable.Rows[mStartINRow][7] = Salary_DS.Rows[0]["TimeIN"].ToString();
                                        AutoDTable.Rows[mStartINRow][12] = Salary_DS.Rows[0]["TimeIN"].ToString();
                                        AutoDTable.Rows[mStartINRow][13] = "PAYIN";
                                    }
                                    else
                                    {
                                        AutoDTable.Rows[mStartINRow][7] = "";
                                        AutoDTable.Rows[mStartINRow][12] = "";
                                        AutoDTable.Rows[mStartINRow][13] = "PAYOUT";
                                    }
                                }
                                else
                                {
                                    AutoDTable.Rows[mStartINRow][7] = "";
                                    AutoDTable.Rows[mStartINRow][12] = "";
                                    AutoDTable.Rows[mStartINRow][13] = "PAYOUT";
                                }

                                AutoDTable.Rows[mStartINRow][0] = iRow + 1;
                                AutoDTable.Rows[mStartINRow][3] = "Salary Cover";
                                AutoDTable.Rows[mStartINRow][9] = mDataSet.Rows[iRow]["MachineID"].ToString();
                                AutoDTable.Rows[mStartINRow][1] = mDataSet.Rows[iRow]["Designation"].ToString();
                                AutoDTable.Rows[mStartINRow][2] = mDataSet.Rows[iRow]["TypeName"].ToString();
                                AutoDTable.Rows[mStartINRow][4] = Convert.ToInt32(mDataSet.Rows[iRow]["EmpNo"].ToString());
                                AutoDTable.Rows[mStartINRow][5] = mDataSet.Rows[iRow]["ExistingCode"].ToString();
                                AutoDTable.Rows[mStartINRow][6] = mDataSet.Rows[iRow]["CatName"].ToString();
                                AutoDTable.Rows[mStartINRow][10] = mDataSet.Rows[iRow]["TypeName"].ToString();
                                AutoDTable.Rows[mStartINRow][11] = mDataSet.Rows[iRow]["SubCatName"].ToString();


                                mStartINRow += 1;

                            }

                        }
                        else
                        {
                            Payroll_EmpNo = "";
                            Payroll_EmployeeType = "";
                            Payroll_SalaryType = "";
                            Bank_Query = "";
                        }

                    }
                }


            }


            SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "' ";
            dt = objdata.RptEmployeeMultipleDetails(SSQL);


            string name = dt.Rows[0]["CompName"].ToString();

            DataTable table_DT = new DataTable();


            table_DT.Columns.Add("CompanyName");
            table_DT.Columns.Add("LocationName");
            table_DT.Columns.Add("ShiftDate");

            table_DT.Columns.Add("SNo");
            table_DT.Columns.Add("Dept");
            table_DT.Columns.Add("Type");
            table_DT.Columns.Add("Shift");
            table_DT.Columns.Add("Category");
            table_DT.Columns.Add("SubCategory");
            table_DT.Columns.Add("EmpCode");
            table_DT.Columns.Add("ExCode");
            table_DT.Columns.Add("Name");
            table_DT.Columns.Add("TimeIN");
            table_DT.Columns.Add("MachineID");
            table_DT.Columns.Add("PrepBy");
            table_DT.Columns.Add("PrepDate");
            table_DT.Columns.Add("CoverDate");
            table_DT.Columns.Add("ReportDate");
            table_DT.Columns.Add("PayType");
            table_DT.Columns.Add("PayAmount");
             table_DT.Columns.Add("BankAmount");


       

            int sno = 1;
            for (int iRow1 = 0; iRow1 < AutoDTable.Rows.Count; iRow1++)
            {

                dtRow = table_DT.NewRow();
                dtRow["CompanyName"] = name;
                dtRow["LocationName"] = SessionLcode;
                dtRow["ShiftDate"] = Date;
                dtRow["SNo"] = sno;
                dtRow["Dept"] = AutoDTable.Rows[iRow1][1].ToString();
                dtRow["Type"] = AutoDTable.Rows[iRow1][2].ToString();
                dtRow["Shift"] = AutoDTable.Rows[iRow1][3].ToString();
                dtRow["Category"] = AutoDTable.Rows[iRow1][10].ToString();
                dtRow["SubCategory"] = AutoDTable.Rows[iRow1][11].ToString();
                dtRow["EmpCode"] = AutoDTable.Rows[iRow1][4].ToString();
                dtRow["ExCode"] = AutoDTable.Rows[iRow1][5].ToString();
                dtRow["Name"] = AutoDTable.Rows[iRow1][6].ToString();

                if (AutoDTable.Rows[iRow1][7].ToString() != "")
                {
                    dtRow["TimeIN"] = String.Format("{0:hh:mm tt}", AutoDTable.Rows[iRow1][7].ToString());
                }
                else
                {
                    dtRow["TimeIN"] = String.Format("{0:hh:mm tt}", AutoDTable.Rows[iRow1][7].ToString());

                }



                dtRow["MachineID"] = AutoDTable.Rows[iRow1][9].ToString();
                dtRow["PrepBy"] = "User";
                dtRow["PrepDate"] = Date;

                dtRow["ReportDate"] = "SALARY COVER REPORT FROM : " + Payfromdate + " TO : " + PayTodate;

                if (AutoDTable.Rows[iRow1][12].ToString() != "")
                {
                    DateTime dtt = Convert.ToDateTime(AutoDTable.Rows[iRow1][12].ToString());
                    dtRow["CoverDate"] = dtt.AddDays(0).ToString("dd/MM/yyyy");
                }
                else
                {
                    dtRow["CoverDate"] = "";
                }
                dtRow["PayType"] = AutoDTable.Rows[iRow1][13].ToString();
                dtRow["PayAmount"] = AutoDTable.Rows[iRow1][14].ToString();
                dtRow["BankAmount"] = AutoDTable.Rows[iRow1][15].ToString();
                table_DT.Rows.Add(dtRow);
            }





            ds.Tables.Add(table_DT);
            ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("crystal/Salary_Cover_Report_Abstract.rpt"));
            
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;


        }
    }
    public void Load_DB()
    {
        //Get Database Name
        string query = "";
        DataTable dt_DB = new DataTable();
        query = "select CMS,Rights,Sales,Stores,Weaving,Maintaince,Production,Payroll from [SVT_Rights]..MstDBname";
        dt_DB = objdata.RptEmployeeMultipleDetails(query);
        if (dt_DB.Rows.Count > 0)
        {
            SessionPayroll = dt_DB.Rows[0]["Payroll"].ToString();
            //SessionRights = dt_DB.Rows[0]["Rights"].ToString();
        }
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        CrystalReportViewer1.Dispose();
    }
}
