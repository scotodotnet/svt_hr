﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;


public partial class RptLateINHrsBWD : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string Division;
    string Status;
    double Final_Count;
    string TokenNo;


    double Present_Count;
    double Absent_Count;
    decimal Fixed_Work_Days;
    decimal Month_WH_Count;
    double Present_WH_Count;
    double NFH_Days_Count;
    double NFH_Days_Present_Count;

    BALDataAccess objdata = new BALDataAccess();

    string SSQL = "";
    DataTable AutoDTable = new DataTable();
    DataTable mEmployeeDT = new DataTable();
    DataTable MEmployeeDS = new DataTable();
    DateTime date1;
    DateTime Date2 = new DateTime();
    int intK;
    string FromDate = null;
    string ToDate;
    string Shift;
    string Date_Value_Str;
    string Date_value_str1;
    string[] Time_Minus_Value_Check;
    string WagesType;
    double leaveCount;


    System.Web.UI.WebControls.DataGrid grid =
                            new System.Web.UI.WebControls.DataGrid();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report- Late IN Between Dates";
            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            Status = Request.QueryString["Status"].ToString();
            Division = Request.QueryString["Division"].ToString();

            string TempWages = Request.QueryString["Wages"].ToString();
            WagesType = TempWages.Replace("_", "&");
            Shift = Request.QueryString["Shift"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString(); DataTable dt = new DataTable();
            DataTable dt1 = new DataTable();
            DataTable TempDt = new DataTable();

            AutoDTable.Columns.Add("MachineID");
            AutoDTable.Columns.Add("FirstName");
            AutoDTable.Columns.Add("DeptName");

            date1 = Convert.ToDateTime(FromDate);
            string dat = ToDate;
            Date2 = Convert.ToDateTime(dat);

            int daycount = (int)((Date2 - date1).TotalDays);
            int daysAdded = 0;

            while (daycount >= 0)
            {
                DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded).ToShortDateString());

                AutoDTable.Columns.Add(Convert.ToString("TimeIN" + dayy.ToShortDateString()));
                AutoDTable.Columns.Add(Convert.ToString("TimeOUT" + dayy.ToShortDateString()));
                AutoDTable.Columns.Add(Convert.ToString("Total_Hrs" + dayy.ToShortDateString()));
                AutoDTable.Columns.Add(Convert.ToString("Deduction_Hrs" + dayy.ToShortDateString()));

                daycount -= 1;
                daysAdded += 1;
            }

           // AutoDTable.Columns.Add("Total Minutes");

            SSQL = "Select MachineID,FirstName,DeptName from Employee_mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
            if (WagesType != "-Select-")
            {
                SSQL = SSQL + " and Wages='" + WagesType + "'";
            }
            dt1 = objdata.RptEmployeeMultipleDetails(SSQL);


            for (int EmpNoRw = 0; EmpNoRw < dt1.Rows.Count; EmpNoRw++)
            {

                AutoDTable.NewRow();
                AutoDTable.Rows.Add();

                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["MachineID"] = dt1.Rows[EmpNoRw]["MachineID"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["FirstName"] = dt1.Rows[EmpNoRw]["FirstName"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["DeptName"] = dt1.Rows[EmpNoRw]["DeptName"].ToString();

                //Total calculation for Each Emp
                double Total_Deduction_Hrs = 0;
                double Total_Deduction_Min = 0;

                daycount = (int)((Date2 - date1).TotalDays);
                daysAdded = 0;

                while (daycount >= 0)
                {
                    DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded).ToShortDateString());

                    SSQL = "";
                    SSQL = "Select TimeIN,TimeOUT,Total_Hrs1,Late_Total_Hrs from LogTime_Days";
                    SSQL = SSQL + " where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                    SSQL = SSQL + " and Convert(datetime,Attn_Date,103)= Convert(datetime,'" + dayy.ToString("dd/MM/yyyy") + "',103) and MachineID='" + dt1.Rows[EmpNoRw]["MachineID"] + "'";
                    SSQL = SSQL + " and TypeName!='Leave' and Present_Absent!='Leave' and Present_Absent!='Absent' and Shift!='No Shift'";

                    dt = objdata.RptEmployeeMultipleDetails(SSQL);

                    if (dt.Rows.Count > 0)
                    {
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TimeIN" + dayy.ToShortDateString()] = dt.Rows[0]["TimeIN"].ToString();
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TimeOUT" + dayy.ToShortDateString()] = dt.Rows[0]["TimeOUT"].ToString();
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Total_Hrs" + dayy.ToShortDateString()] = dt.Rows[0]["Total_Hrs1"].ToString();
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Deduction_Hrs" + dayy.ToShortDateString()] = dt.Rows[0]["Late_Total_Hrs"].ToString();
                    }

                    daycount -= 1;
                    daysAdded += 1;
                }
            }

            //// for delete the row if the Total Minutes in 0
            //for (int rw = AutoDTable.Rows.Count - 1; rw >= 0; rw--)
            //{
            //    DataRow dr = AutoDTable.Rows[rw];
            //    if (dr["Deduction_Hrs"].ToString() == "" | dr["Deduction_Hrs"].ToString() == "0" | dr["Deduction_Hrs"].ToString() == string.Empty | dr["Deduction_Hrs"].ToString() == "0:00")
            //        dr.Delete();

            //}
            //AutoDTable.AcceptChanges();

            string attachment = "attachment;filename=LATE IN DEDUCTION HRS BETWEEN DATES.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            StringWriter stw = new StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);

            Response.Write("<table style='font-weight:bold;'>");
            Response.Write("<tr align='Center'>");
            Response.Write("<td colspan='10'>");
            Response.Write(SessionCcode + "-" + SessionLcode);
            Response.Write("</td>");
            Response.Write("</tr>");

           

            Response.Write("<tr align='Center'>");
            Response.Write("<td colspan='10'>");
            Response.Write(WagesType + " WAGES");
            Response.Write("</td>");
            Response.Write("</tr>");

            Response.Write("<tr align='Center'>");
            Response.Write("<td colspan='10'>");
            Response.Write("LATE IN DEDUCTION REPORT BETWEEN DATES FROM" + FromDate + "  TO  " + ToDate.ToString());
            Response.Write("</td>");
            Response.Write("</tr>");

            Response.Write("</table>");

            Response.Write("<table border='1'>");
            Response.Write("<tr style='font-weight: bold;' align='center'>");

            if (AutoDTable.Rows.Count > 0)
            {
                Response.Write("<td>MachineID</td><td>FirstName</td><td>DeptName</td>");

                daycount = (int)((Date2 - date1).TotalDays);
                daysAdded = 0;

                while (daycount >= 0)
                {
                    DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded).ToShortDateString());

                    Response.Write("<td>");
                    Response.Write("<table border='1'><tr  style='font-weight: bold;' align='center'><td colspan='4' align='center'>" + dayy.ToString("dd/MM/yyyy") + "</td></tr><tr  style='font-weight: bold;' align='center'><td>TimeIN</td><td>TimeOUT</td><td>Total_Hrs</td><td>Deduction_Hrs</td></tr></table>");
                    Response.Write("</td>");

                    daycount -= 1;
                    daysAdded += 1;
                }
                Response.Write("</tr>");

                grid.DataSource = AutoDTable;
                grid.DataBind();
                grid.ShowHeader = false;
                grid.RenderControl(htextw);
                Response.Write(stw.ToString());
                Response.Write("</table>");
                Response.End();
                Response.Clear();
            }
        }
    }
}