﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;

public partial class EmployeeIDCardFormat : System.Web.UI.Page
{


    string ShiftType1 = "";
    string Date = "";

    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string WagesType;
    string Dept;
    string FromDate;
    string ToDate;
    string ss = "";
    ReportDocument report = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();
    //  EmployeeDetailsClass objEmpDetails = new EmployeeDetailsClass();



    string SSQL = "";

    DataSet ds = new DataSet();

    string Division = "";
    DataTable AutoDTable = new DataTable();
    DataTable DataCell = new DataTable();
    DataTable dt = new DataTable();
    DataTable mDataSet = new DataTable();
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Report-Employee ID Card Format";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Dashboard"));
                //li.Attributes.Add("class", "droplink active open");
            }
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();

            //ModeType = Request.QueryString["ModeType"].ToString();
            Division = Request.QueryString["Division"].ToString();
            Dept = Request.QueryString["Dept"].ToString();
            string TempWages = Request.QueryString["Wages"].ToString();
            WagesType = TempWages.Replace("_", "&");
            //WagesType = Request.QueryString["Wages"].ToString();

            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            if (SessionUserType == "2")
            {
                //NonAdminEmployeeIDCard();
            }
            else
            {
               EmployeeIDCard();
            }
        }
    }

    public void EmployeeIDCard()
    {

        DataCell.Columns.Add("CompanyName");
        DataCell.Columns.Add("LocationName");
        DataCell.Columns.Add("EmpNo");
        DataCell.Columns.Add("FirstName");
        DataCell.Columns.Add("LastName");
        DataCell.Columns.Add("DOB");
        DataCell.Columns.Add("Address");
        DataCell.Columns.Add("DeptName");
        DataCell.Columns.Add("Designation");
        DataCell.Columns.Add("Blood_Group");
        DataCell.Columns.Add("Date_Of_Issue");
        DataCell.Columns.Add("img", System.Type.GetType("System.Byte[]"));
        DataCell.Columns.Add("Reg_No");
        DataCell.Columns.Add("Hindi_Category");
        

        SSQL = "Select * from Location_Mst where CompCode='" + SessionCcode + "' And LocCode='" +SessionLcode + "'";
        mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);
        string UNIT_REG_No="";
       if(mDataSet.Rows.Count !=0)
       {
           UNIT_REG_No=mDataSet .Rows[0]["Register_No"].ToString();
       }
      
         SSQL = "";
        SSQL = "select isnull(ExistingCode,'') as [ExistingCode]";
        SSQL += ",isnull(FirstName,'') as [EmpName]";
        SSQL += ",isnull(MiddleInitial,'') as [LastName]";
         SSQL += ",isnull(FamilyDetails,'') as[FatherName]";
        SSQL += ",isnull(BirthDate,'') as [DOB]";
        SSQL += ",isnull(Address1,'') as [Address]";
        SSQL += ",isnull(DeptName,'') as [DeptName]";
        SSQL += ",isnull(Designation,'') as [Designation]";
        SSQL += ",isnull(BloodGroup,'') as [BloodGroup]";
        SSQL += ",isnull(MachineID,'') as [MachineID]";
        SSQL += ",isnull(DOJ,'') as [DOJ]";
        SSQL += ",isnull(Hindi_Category,'') as [Hindi_Category]";
        SSQL += " from employee_mst";
        SSQL += " Where CompCode='" +SessionCcode+  "'";
        SSQL += " and LocCode='" +SessionLcode+ "' And IsActive='Yes'";
        if (WagesType != "-Select-")
        {
            SSQL += " and Wages='" + WagesType + "'";
        }
        //SSQL = SSQL + " And (CONVERT(DateTime,DOJ, 103) between CONVERT(DateTime, '" +FromDate+"', 103) and CONVERT(DateTime, '" +ToDate+ "', 103))";
        if (Dept != "-Select-")
        {
            SSQL += " and DeptName='" + Dept + "'";
        }
       if (Division != "-Select-")
        {
            SSQL += " and Division='" + Division + "'";
        }
       

        SSQL = SSQL + " Order by ExistingCode Asc";

        AutoDTable = objdata.RptEmployeeMultipleDetails(SSQL);

        if (AutoDTable.Rows.Count != 0)
        {
            for (int i = 0; i < AutoDTable.Rows.Count; i++)
            {
                SSQL = "Select * from Company_Mst ";
            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            string name = dt.Rows[0]["CompName"].ToString();



            DataTable DT_Photo = new DataTable();
            string SS = "Select *from Photo_Path_Det";
            DT_Photo = objdata.RptEmployeeMultipleDetails(SS);

            string PhotoDet = "";
            string UNIT_Folder = "";
            if (DT_Photo.Rows.Count != 0)
            {
                PhotoDet = DT_Photo.Rows[0]["Photo_Path"].ToString();
            }
            //if (SessionLcode.ToUpper() == "UNIT I") { UNIT_Folder = "Emp_Scan/UNIT_I/"; }
            //if (SessionLcode.ToUpper() == "UNIT II") { UNIT_Folder = "Emp_Scan/UNIT_II/"; }
            //if (SessionLcode.ToUpper() == "UNIT III") { UNIT_Folder = "Emp_Scan/UNIT_III/"; }
            //if (SessionLcode.ToUpper() == "UNIT IV") { UNIT_Folder = "//SABA-PC/To Share/Narmatha/Emp_Scan/UNIT_IV/"; } //UNIT_Folder = "Emp_Scan/UNIT_IV/"; }

            if (SessionLcode.ToUpper() == "UNIT I") { UNIT_Folder = PhotoDet + "/UNIT_I/"; }
            if (SessionLcode.ToUpper() == "UNIT II") { UNIT_Folder = PhotoDet + "/UNIT_II/"; }
            if (SessionLcode.ToUpper() == "UNIT III") { UNIT_Folder = PhotoDet + "/UNIT_III/"; }
            if (SessionLcode.ToUpper() == "UNIT IV") { UNIT_Folder = PhotoDet + "/UNIT_IV/"; } //UNIT_Folder = "Emp_Scan/UNIT_IV/"; }


            byte[] imageByteData = new byte[0];
            string mid = AutoDTable.Rows[i]["MachineID"].ToString();
            // string path = "D:/Photo/" + mid + ".jpg";

            string path = PhotoDet + "\\" + mid + ".jpg";

            if (System.IO.File.Exists(path))
            {
                imageByteData = System.IO.File.ReadAllBytes(path);

            }

            else
            {
                string filepath = HttpContext.Current.Request.MapPath("~/assets/images/No_Image.jpg");

                imageByteData = System.IO.File.ReadAllBytes(filepath);

            }




                string  Nowdate= DateTime.Now.ToString("dd/MM/yyyy");
                string[] Now=Nowdate.Split('/') ;

                string Current_MOnth_First_Date="01/" + Now[1] + "/" + Now[2];
                string Current_Date_Str=DateTime.Now.ToString("dd/MM/yyyy");
                 string Date_of_Issue="";
          
                DateTime System_Date=new DateTime();
                DateTime DOJ_Date_Format=new DateTime();
           

                System_Date = Convert.ToDateTime(Current_MOnth_First_Date.ToString());
                 DOJ_Date_Format=Convert.ToDateTime(AutoDTable.Rows[i]["DOJ"].ToString());

            if(DOJ_Date_Format.Date <System_Date.Date)
            {
                Date_of_Issue = Current_MOnth_First_Date;
            }
            else
            {
              Date_of_Issue=DateTime.Now.ToString("dd/MM/yyyy");

            }
                string Company_Name_Reg_No_Join=SessionLcode + "-" + UNIT_REG_No;
               string DOB_Convert_Format=Convert.ToDateTime(AutoDTable.Rows[i]["DOB"]).ToString("dd/MM/yyyy");
                
                DataCell.NewRow();
                DataCell.Rows.Add();


          
      
                  DataCell.Rows[i]["CompanyName"]=name;
                  DataCell.Rows[i]["LocationName"]=Company_Name_Reg_No_Join;
                  DataCell.Rows[i]["EmpNo"] = AutoDTable.Rows[i]["ExistingCode"].ToString();
                  DataCell.Rows[i]["FirstName"] = AutoDTable.Rows[i]["EmpName"].ToString();
                  DataCell.Rows[i]["LastName"]=AutoDTable.Rows[i]["FatherName"].ToString();
                  DataCell.Rows[i]["DOB"]=DOB_Convert_Format;
                  DataCell.Rows[i]["Address"]=AutoDTable.Rows[i]["Address"].ToString();
                  DataCell.Rows[i]["DeptName"]=AutoDTable.Rows[i]["DeptName"].ToString();
                  DataCell.Rows[i]["Designation"]=AutoDTable.Rows[i]["Designation"].ToString();
                  DataCell.Rows[i]["Blood_Group"]=AutoDTable.Rows[i]["BloodGroup"].ToString();
                  DataCell.Rows[i]["Date_Of_Issue"]=Date_of_Issue;
                  DataCell.Rows[i]["img"]=imageByteData;
                  DataCell.Rows[i]["Reg_No"]=UNIT_REG_No;
                  DataCell.Rows[i]["Hindi_Category"] = AutoDTable.Rows[i]["Hindi_Category"].ToString();
           
            }

            ds.Tables.Add(DataCell);
            ReportDocument report = new ReportDocument();
            if (WagesType == "WEEKLY-HINDI")
            {
                report.Load(Server.MapPath("crystal/EmployeeIDCard.rpt"));
            }
            else if (WagesType == "WEEKLY")
            {
                report.Load(Server.MapPath("crystal/EmployeeIDCard_Weekly.rpt"));
            }

            
            
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);

            if (Division != "-Select-")
            {
                report.DataDefinition.FormulaFields["Division_Name_Str"].Text = "'" + "Division:" + "'" + "'" + Division + "'";
            }
            else
            {
                report.DataDefinition.FormulaFields["Division_Name_Str"].Text = "'" + "Division:" + "'";
            }
            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;




        }
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        CrystalReportViewer1.Dispose();
    }
}
