﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Web.UI.DataVisualization.Charting;
using System.Globalization;

using Payroll;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Drawing;

public partial class AttendDashBoard : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionUserName = Session["Usernmdisplay"].ToString();
            SessionUserID = Session["UserId"].ToString();
            SessionRights = Session["Rights"].ToString();
        }
        if (!IsPostBack)
        {
            HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Menu-Dashboard"));

            Current_Employee_Strength_Departmentwise();
            Current_Employee_Attendance_Departmentwise();
        }

    }

    private void Current_Employee_Strength_Departmentwise()
    {
        DataTable dt = new DataTable();
        string query = "";

        query = "Select DeptName ,count(EmpNo) as present ";
        query = query + " from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        query = query + " And IsActive='Yes' ";
        query = query + " group by DeptName";

        dt = objdata.RptEmployeeMultipleDetails(query);
        Employee_Strength_DeptWise.DataSource = dt;
        Employee_Strength_DeptWise.Series["Series1"].XValueMember = "DeptName";
        Employee_Strength_DeptWise.Series["Series1"].YValueMembers = "present";

        this.Employee_Strength_DeptWise.Series[0]["PieLabelStyle"] = "Outside";
        this.Employee_Strength_DeptWise.ChartAreas[0].Area3DStyle.Enable3D = true;
        this.Employee_Strength_DeptWise.ChartAreas[0].Area3DStyle.Inclination = 1;
        this.Employee_Strength_DeptWise.Series[0].BorderWidth = 1;
        this.Employee_Strength_DeptWise.Series[0].BorderColor = System.Drawing.Color.FromArgb(26, 59, 105);
        this.Employee_Strength_DeptWise.Legends.Clear();
        this.Employee_Strength_DeptWise.Legends.Add("Legend1");
        this.Employee_Strength_DeptWise.Legends[0].Enabled = true;
        this.Employee_Strength_DeptWise.Legends[0].Docking = Docking.Bottom;
        this.Employee_Strength_DeptWise.Legends[0].Alignment = System.Drawing.StringAlignment.Center;
        this.Employee_Strength_DeptWise.Series[0].LegendText = "#PERCENT";
        //this.Current_Month_Sale_Chart.DataManipulator.Sort(PointSortOrder.Descending, Chart2.Series[0]);
        Employee_Strength_DeptWise.DataBind();
    }

    private void Current_Employee_Attendance_Departmentwise()
    {

        DataTable dt = new DataTable();
        string query = "";
        DataTable DT_Date = new DataTable();
        DateTime Curr_Date = new DateTime();

        query = "Select GetDate() as CurrDate";
        DT_Date = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Date.Rows.Count != 0)
        {
            Curr_Date = Convert.ToDateTime(DT_Date.Rows[0]["CurrDate"].ToString());
        }


        query = "Select DeptName ,count(MachineID) as present ";
        query = query + " from LogTime_Days where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        //query = query + " And Attn_Date_Str='01/12/2017' And Present!='0.0'";
        query = query + " And Attn_Date_Str='" + Curr_Date.ToString("dd/MM/yyyy") + "' And Present!='0.0'";
        query = query + " group by DeptName";

        dt = objdata.RptEmployeeMultipleDetails(query);
        Employee_Attendance_DeptWise.DataSource = dt;
        Employee_Attendance_DeptWise.Series["Series1"].XValueMember = "DeptName";
        Employee_Attendance_DeptWise.Series["Series1"].YValueMembers = "present";

        this.Employee_Attendance_DeptWise.Series[0]["PieLabelStyle"] = "Outside";
        this.Employee_Attendance_DeptWise.ChartAreas[0].Area3DStyle.Enable3D = true;
        this.Employee_Attendance_DeptWise.ChartAreas[0].Area3DStyle.Inclination = 1;
        this.Employee_Attendance_DeptWise.Series[0].BorderWidth = 1;
        this.Employee_Attendance_DeptWise.Series[0].BorderColor = System.Drawing.Color.FromArgb(26, 59, 105);
        this.Employee_Attendance_DeptWise.Legends.Clear();
        this.Employee_Attendance_DeptWise.Legends.Add("Legend1");
        this.Employee_Attendance_DeptWise.Legends[0].Enabled = true;
        this.Employee_Attendance_DeptWise.Legends[0].Docking = Docking.Bottom;
        this.Employee_Attendance_DeptWise.Legends[0].Alignment = System.Drawing.StringAlignment.Center;
        this.Employee_Attendance_DeptWise.Series[0].LegendText = "#PERCENT";
        //this.Current_Month_Sale_Chart.DataManipulator.Sort(PointSortOrder.Descending, Chart2.Series[0]);
        Employee_Attendance_DeptWise.DataBind();
    }
}
