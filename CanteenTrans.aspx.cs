﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

using Payroll;
using Payroll.Data;
using Payroll.Configuration;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using System.Data.SqlClient;

public partial class CanteenTrans : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        if (!IsPostBack)
        {
            Months_load();

            int currentYear = Utility.GetCurrentYearOnly;
            for (int i = 0; i < 10; i++)
            {
                ddlYear.Items.Add(new System.Web.UI.WebControls.ListItem(currentYear.ToString(), currentYear.ToString()));
                currentYear = currentYear - 1;
            }
        }
    }

    private void Months_load()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlMonths.Items.Clear();
        query = "Select ID,Months from MonthDetails order by ID ASC";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlMonths.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["ID"] = "0";
        dr["Months"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlMonths.DataTextField = "Months";
        ddlMonths.DataValueField = "ID";
        ddlMonths.DataBind();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SaveMode = "Insert";
        string query = "";
        DataTable dt1 = new DataTable();
        DataTable dt = new DataTable();
        bool ErrFlag = false;

        if (RdbOpt.SelectedValue == "2")
        {
            if (ddlMonths.SelectedValue == "0")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the Month..');", true);
            }
        }


        if (!ErrFlag)
        {
            query = "Select *from CanteenDetails where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
            query = query + " And TransID='" + txtTransID.Text + "'";
            dt1 = objdata.RptEmployeeMultipleDetails(query);

            if (dt1.Rows.Count != 0)
            {
                SaveMode = "Already";
                //query = "delete from CanteenDetails where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                //query = query + " And TransID='" + txtTransID.Text + "'";
                //objdata.RptEmployeeMultipleDetails(query);
            }
            else
            {

                query = "insert into CanteenDetails(Ccode,Lcode,TransID,TransDate,FoodType,Category,";
                query = query + "Month,Year,HostelAmt,RegularAmt)Values('" + SessionCcode + "','" + SessionLcode + "',";
                query = query + "'" + txtTransID.Text + "','" + txtDate.Text + "','" + txtType.SelectedItem.Text + "',";
                query = query + "'" + RdbOpt.SelectedValue + "',";
                if(ddlMonths.SelectedValue=="0")
                {
                    query = query + "'',";
                }
                else
                {
                    query = query + "'" + ddlMonths.SelectedItem.Text + "',";
                }
                query = query + "'" + ddlYear.SelectedItem.Text + "',";
                query = query + "'" + txtHstCantAmt.Text + "','" + txtStfRegCantAmt.Text + "')";
                objdata.RptEmployeeMultipleDetails(query);
                SaveMode = "Insert";
            }

            if (SaveMode == "Already")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Already Exsists..');", true);
            }
            else if (SaveMode == "Insert")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Saved Successfully..');", true);
            }
            Clear_All_Field();
        }

    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

    private void Clear_All_Field()
    {
        txtTransID.Text = ""; txtDate.Text = "";
        txtType.SelectedValue = "All";
        RdbOpt.SelectedValue = "1";
        ddlMonths.SelectedValue = "0";
        ddlYear.SelectedValue = Utility.GetCurrentYearOnly.ToString();
        txtHstCantAmt.Text = "0";
        txtStfRegCantAmt.Text = "0";

        btnSave.Text = "Save";
    }
}
