﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;

using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Text;

public partial class MainPage : System.Web.UI.MasterPage
{
    protected System.Resources.ResourceManager rm;
    protected string PayrollRegisterContentMeta, pageTitle;
    BALDataAccess objdata = new BALDataAccess();
  
    System.Globalization.CultureInfo culterInfo = new System.Globalization.CultureInfo("en-GB");
    // static string ExisitingNo = "";
    static string UserID = "", SesRoleCode = "", SessionAdmin = "";
    string SessionCcode;
    string SessionLcode;

    //Parthi Merge Start
    string Data;
    string SSQL;
    string SessionUserType;
    //BALDataAccess objdata = new BALDataAccess();
    DataTable dtd = new DataTable();

    string DashboardM;
    string masterpageM;
    string UserCreationM;
    string AdministrationRightM;
    string UserRightsM;
    string DepartmentDetailsM;
    string DesginationDetailsM;
    string EmployeeTypeDetailsM;
    string WagesTypeDetailsM;
    string BankDetailsM;
    string EmployeeDetailsM;
    string LeaveMasterM;

    string PermissionMasterM;
    string GraceTimeM;
    string EarlyOutMasterM;
    string LateINMasterM;
    string EmployeeM;
    string EmployeeApprovalM;
    string EmployeeStatusM;
    string LeaveManagementM;
    string TimeDeleteM;
    string LeaveDetailsM;

    string PermissionDetailM;
    string ManualAttendanceM;
    string ManualShiftM;
    string ManualShiftDetailsM;
    string UploadDownloadM;
    string DeductionM;
    string IncentiveM;
    string ReportM;
    string DownloadClearM;
    string CompanyMasterM = "0";
    string SuperAdminCreationM = "0";
    string SalaryProcessM;
    string SessionSpay;
    string SessionEpay;
    string SessionRights;
    string SessionUser;

    //Parthi Merge End

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        UserID = Session["UserId"].ToString();
        SesRoleCode = Session["RoleCode"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionRights = Session["Rights"].ToString();
        SessionUser = Session["Usernmdisplay"].ToString();
        //lblUserNameDisp.Text = SessionLcode + " - " + Session["Usernmdisplay"].ToString();

        lblCcode.Text = SessionCcode.ToString();
        lblLcode.Text = SessionLcode.ToString();
        lblUserID.Text = Session["Usernmdisplay"].ToString();

        if (!IsPostBack)
        {
            string SQL = "select ModuleLink from [SVT_Rights]..Module_List where ModuleID='1'";
            DataTable dt = new DataTable();
            dt = objdata.RptEmployeeMultipleDetails(SQL);
            if (dt.Rows.Count > 0)
            {
                //MainPage_Link.Attributes["href"] = dt.Rows[0]["ModuleLink"].ToString();
            }

            //Login_User_Photo_Load();
           
                if (SessionUser == "Scoto")
                {
                    
                }
                else if(SessionAdmin!="1")
                {
                    ALL_Menu_Header_Forms_Disable();
                    
                    //Admin_ModuleLink_Check();
                    Admin_User_Rights_Check();


                    string IF_Query = "";
                    DataTable IF_DT = new DataTable();
                    IF_Query = "Select * from [" + SessionRights + "]..Company_Module_User_Rights where ModuleID='8'";
                    IF_Query = IF_Query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And UserName='" + SessionUser + "'";
                    IF_Query = IF_Query + " And (AddRights='1')";
                    IF_DT = objdata.RptEmployeeMultipleDetails(IF_Query);
                    if (IF_DT.Rows.Count == 0)
                    {
                        Mnu_Payroll_Link_ID.Visible = false;
                    }
                }
                if (SessionAdmin == "1")
                {
                   // ALL_Menu_MDHeader_Forms_Disable();
                }
                if (SessionAdmin == "4")
                {
                    ALL_Menu_Header_Forms_Disable();
                    ALL_Menu_MDHeader_Forms_Enable();
                }
                if (SessionAdmin != "4")
                {
                    ALL_Menu_MDHeader_Forms_Disable();
                }
        }
        
    }
    //protected void btnModule_Home_Click(object sender, ImageClickEventArgs e)
    //{
    //    //Get Cotton Link
    //    DataTable DT_Link = new DataTable();
    //    string query = "Select * from [" + SessionRights + "]..Module_List where ModuleID='6'";
    //    DT_Link = objdata.RptEmployeeMultipleDetails(query);
    //    if (DT_Link.Rows.Count != 0)
    //    {
    //        string Link_Open = DT_Link.Rows[0]["ModuleLink"].ToString();
    //        query = "Delete from [" + SessionRights + "]..Module_Open_User";
    //        objdata.RptEmployeeMultipleDetails(query);
    //        //Get user Password
    //        query = "Select * from [" + SessionRights + "]..MstUsers where UserCode='" + SessionUser.Trim() + "' and IsAdmin='" + SessionAdmin + "' And LocationCode='" + SessionLcode + "' And CompCode='" + SessionCcode + "'";
    //        DT_Link = objdata.RptEmployeeMultipleDetails(query);
    //        if (DT_Link.Rows.Count != 0)
    //        {
    //            string Password_Str = DT_Link.Rows[0]["Password"].ToString();
    //            //Insert User Details
    //            query = "Insert Into [" + SessionRights + "]..Module_Open_User(CompCode,LocCode,UserName,Password) Values";
    //            query = query + " ('" + SessionCcode + "','" + SessionLcode + "','" + SessionUser + "','" + Password_Str + "')";
    //            objdata.RptEmployeeMultipleDetails(query);
    //            Response.Redirect(Link_Open);
    //        }

    //    }
    //}

    private void ALL_Menu_Header_Forms_Disable()
    {
        //Dashdoard
        this.FindControl("Menu_Dashboard").Visible = false;

        //Master
        this.FindControl("Menu_Master").Visible = false;
        this.FindControl("Menu_UserCreation").Visible = false;
        this.FindControl("Menu_UserRights").Visible = false;
        this.FindControl("Menu_ReportRights").Visible = false;
        this.FindControl("Menu_HolidayMaster").Visible = false;
        this.FindControl("Menu_RouteMaster").Visible = false;
        this.FindControl("Menu_OthersMaster").Visible = false;
        this.FindControl("Menu_LabourAllotment").Visible = false;
        this.FindControl("Menu_Bank").Visible = false;
        this.FindControl("Menu_Agent").Visible = false;
        this.FindControl("Menu_Grade").Visible = false;
        this.FindControl("Menu_RecruitOff").Visible = false;
        this.FindControl("Menu_Qualify").Visible = false;
        this.FindControl("Menu_DeActive").Visible = false;
        //this.FindControl("Menu_MessMst").Visible = false;
        this.FindControl("Menu_CommTypeMst").Visible = false;
        //this.FindControl("Menu_RecruitmentCommMst").Visible = false;
        this.FindControl("Menu_CommunityMst").Visible = false;
        //this.FindControl("Menu_MedicalMst").Visible = false;
        this.FindControl("Menu_AdminRightsMSt").Visible = false;
        this.FindControl("Menu_LunchTimingMst").Visible = false;
        this.FindControl("Menu_DeptIncMst").Visible = false;
        this.FindControl("Menu_FineMst").Visible = false;
        //this.FindControl("Menu_EducationMst").Visible = false;

        this.FindControl("MstLatePunchTime").Visible = false;
        this.FindControl("MstPacking").Visible = false;
        this.FindControl("MstWeekSalary").Visible = false;
     //   this.FindControl("MstHindiCategory").Visible = false;
        this.FindControl("MstShiftType").Visible = false;
        this.FindControl("MstGrassTime").Visible = false;
        this.FindControl("MstGrassOTTime").Visible = false;
        this.FindControl("MstLateIN").Visible = false;
        this.FindControl("MstCommissionHindi").Visible = false;
        this.FindControl("MstEarlyOUT").Visible = false;
       


        //BiometricMachine
        this.FindControl("Menu_BiometricMachine").Visible = false;
        this.FindControl("Menu_DownLoad").Visible = false;
        //this.FindControl("Menu_Finger_Download_Upload").Visible = false;
        
        //Wages
        this.FindControl("Menu_Wages").Visible = false;
        this.FindControl("Menu_SalaryUpdate").Visible = false;
        this.FindControl("Menu_SalaryApprove").Visible = false;
        this.FindControl("Menu_SalaryCorrectionList").Visible = false;
        this.FindControl("Menu_SalaryApprvList").Visible = false;

        //EmployeeProfile
        this.FindControl("Menu_EmployeeProfile").Visible = false;
        this.FindControl("Menu_EmployeeDetails").Visible = false;
        this.FindControl("Menu_EmployeeApproval").Visible = false;
        this.FindControl("Menu_EmployeeStatus").Visible = false;

        //this.FindControl("Menu_Education").Visible = false;
        //this.FindControl("Menu_EducationHistRpt").Visible = false;
        //this.FindControl("Menu_MedGatePAss").Visible = false;
        //this.FindControl("Menu_MedicalDet").Visible = false;
        //this.FindControl("Menu_MedGatePAssReport").Visible = false;
        //this.FindControl("Menu_MedicalDetReport").Visible = false;

        //ManualEntry
        this.FindControl("Menu_ManualEntry").Visible = false;
        this.FindControl("Menu_ManualAttendance").Visible = false;
        this.FindControl("Menu_ManualOT").Visible = false;
        this.FindControl("Menu_ManualLeave").Visible = false;
        this.FindControl("Menu_LeaveApprove").Visible = false;
        this.FindControl("Menu_ManualTimeDelete").Visible = false;
        this.FindControl("Menu_OnDuty").Visible = false;

        //SalaryProcess
        this.FindControl("Menu_SalaryProcess").Visible = false;
        this.FindControl("Menu_Allowance").Visible = false;
        this.FindControl("Menu_Epay").Visible = false;
        this.FindControl("Menu_Voucher").Visible = false;
        this.FindControl("Menu_DeptIncentive").Visible = false;
        this.FindControl("Menu_Canteen").Visible = false;

        //Report
        this.FindControl("Menu_Report").Visible = false;

        //MD Report
        this.FindControl("Menu_MDReport").Visible = false;
        this.FindControl("MD_Dashboard").Visible = false;
        this.FindControl("MD_DashBoard1").Visible = false;
        this.FindControl("MD_UnitwiseRpt").Visible = false;
        this.FindControl("MD_LeftNewRpt").Visible = false;
        this.FindControl("MD_SalaryCoverRpt").Visible = false;
        this.FindControl("MD_AllUnitConsRpt").Visible = false;
        this.FindControl("MD_UnitwiseFine").Visible = false;
        this.FindControl("MD_RecruitmentRpt").Visible = false;
        this.FindControl("MD_AllotmentRpt").Visible = false;
        this.FindControl("MD_CostRpt").Visible = false;
        this.FindControl("MD_UnitwiseCanteen").Visible = false;
        this.FindControl("MD_DayAttnManRpt").Visible = false;
        this.FindControl("MD_DayDeptRpt").Visible = false;
        this.FindControl("MD_EmpProfRpt").Visible = false;

        //Traing Module
        //this.FindControl("Menu_TrainingModule").Visible = false;
        //this.FindControl("Menu_TrainDays").Visible = false;
        //this.FindControl("Menu_TrainStatus").Visible = false;
        //this.FindControl("Menu_TrainingRpt").Visible = false;

        //Menu_Commision
        this.FindControl("Menu_Commision").Visible = false;
        this.FindControl("Menu_CommisionVoucher").Visible = false;

        //this.FindControl("Menu_CommisionRecovery").Visible = false;
        //this.FindControl("Menu_CommisionVoucherReport").Visible = false;
        //this.FindControl("Menu_CommisionRecoveryreport").Visible = false;
        //this.FindControl("Menu_CommissionTransactionLedgerreort").Visible = false;
        //this.FindControl("Menu_CommRecPendingRpt").Visible = false;

       
    }

    private void ALL_Menu_MDHeader_Forms_Disable()
    {
       

        //MD Report
        this.FindControl("Menu_MDReport").Visible = false;
        this.FindControl("MD_Dashboard").Visible = false;
        this.FindControl("MD_DashBoard1").Visible = false;
        this.FindControl("MD_UnitwiseRpt").Visible = false;
        this.FindControl("MD_LeftNewRpt").Visible = false;
        this.FindControl("MD_SalaryCoverRpt").Visible = false;
        this.FindControl("MD_AllUnitConsRpt").Visible = false;
        this.FindControl("MD_UnitwiseFine").Visible = false;
        this.FindControl("MD_RecruitmentRpt").Visible = false;
        this.FindControl("MD_AllotmentRpt").Visible = false;
        this.FindControl("MD_CostRpt").Visible = false;
        this.FindControl("MD_UnitwiseCanteen").Visible = false;
        this.FindControl("MD_DayAttnManRpt").Visible = false;
        this.FindControl("MD_DayDeptRpt").Visible = false;
        this.FindControl("MD_EmpProfRpt").Visible = false;

     
    }

    private void ALL_Menu_MDHeader_Forms_Enable()
    {
        //MD Report
        this.FindControl("Menu_MDReport").Visible = true;
        this.FindControl("MD_Dashboard").Visible = true;
        this.FindControl("MD_DashBoard1").Visible = true;
        this.FindControl("MD_UnitwiseRpt").Visible = true;
        this.FindControl("MD_LeftNewRpt").Visible = true;
        this.FindControl("MD_SalaryCoverRpt").Visible = true;
        this.FindControl("MD_AllUnitConsRpt").Visible = true;
        this.FindControl("MD_UnitwiseFine").Visible = true;
        this.FindControl("MD_RecruitmentRpt").Visible = true;
        this.FindControl("MD_AllotmentRpt").Visible = true;
        this.FindControl("MD_CostRpt").Visible = true;
        this.FindControl("MD_UnitwiseCanteen").Visible = true;
        this.FindControl("MD_DayAttnManRpt").Visible = true;
        this.FindControl("MD_DayDeptRpt").Visible = true;
        this.FindControl("MD_EmpProfRpt").Visible = true;
    }

    private void Admin_User_Rights_Check()
    {
       
        //string ModuleID = Encrypt("2").ToString();
        string MenuID = "";
        DataTable DT_Head = new DataTable();
        DataTable DT_Form = new DataTable();
        //Check with Header Menu
        string query = "Select Distinct Menu_LI_ID,MenuID from [" + SessionRights + "]..Company_Module_MenuHead_Rights where ModuleID='7'";
        query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        DT_Head = objdata.RptEmployeeMultipleDetails(query);
        for (int i = 0; i < DT_Head.Rows.Count; i++)
        {
           

            MenuID = DT_Head.Rows[i]["MenuID"].ToString();
            //query = "Select Distinct Form_LI_ID from [" + SessionRights + "]..Company_Module_Menu_Form_Rights where ModuleID='1' And MenuID='" + MenuID + "'";
            //query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";

            query = "Select Distinct Form_LI_ID from [" + SessionRights + "]..Company_Module_User_Rights where ModuleID='7' And MenuID='" + MenuID + "'";
            query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And UserName='" + SessionUser + "'";
            query = query + " And (AddRights='1')";
            
            DT_Form = objdata.RptEmployeeMultipleDetails(query);
            for (int J = 0; J < DT_Form.Rows.Count; J++)
            {
                this.FindControl(DT_Head.Rows[i]["Menu_LI_ID"].ToString()).Visible = true;
                this.FindControl(DT_Form.Rows[J]["Form_LI_ID"].ToString()).Visible = true;
            }
        }
    }
}
