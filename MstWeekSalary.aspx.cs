﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class MstWeekSalary : System.Web.UI.Page
{

    BALDataAccess objdata = new BALDataAccess();
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SSQL = "";
    DataTable dt = new DataTable();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();

        LoadGrid();

        if (!IsPostBack)
        {
            LoadEmpCat();
        }
    }
    private void LoadEmpCat()
    {
        SSQL = "";
        SSQL = "Select * from MstEmployeeType where (EmpTypeCd='7' or EmpTypeCd='8' or EmpTypeCd='10' or EmpTypeCd='11')";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlType.DataSource = dt;
        ddlType.DataTextField = "EmpType";
        ddlType.DataValueField = "EmpTypeCd";
        ddlType.DataBind();
        ddlType.Items.Insert(0, new ListItem("Select", "0"));
    }
    private void LoadGrid()
    {
        SSQL = "";
        SSQL = "Select * from Mst_WeekSalary";
        Repeater1.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater1.DataBind();
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if(FieldCheck("Check").ToString()=="S")
        {
            SSQL = "";
            SSQL = "Delete from Mst_WeekSalary where EmpCatCode='" + ddlType.SelectedItem.Value + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);
            SSQL = "";
            SSQL = "Insert into Mst_WeekSalary values('" + ddlType.SelectedItem.Value + "','" + ddlType.SelectedItem.Text + "','" + txtDay1.Text + "','" + txtDay2.Text + "',";
            SSQL = SSQL + " '"+txtDay3.Text+"','"+txtDay4.Text+"','"+txtDay5.Text+"','"+txtDay6.Text+"','"+txtDay7.Text+"')";
            objdata.RptEmployeeMultipleDetails(SSQL);

            if (btnSave.Text == "Save")
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "Alert", "alert('Successfully Added');", true);
                btnClear_Click(sender, e);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "Alert", "alert('Successfully Updated');", true);
                btnClear_Click(sender, e);
                btnSave.Text = "Save";
            }
            LoadGrid();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "Alert", "alert('Please Check the Fields');", true);
        }
    }

    private String FieldCheck(string Check)
    {
        string MSG = "";
        if(ddlType.SelectedIndex==0)
        {
            MSG = "F";
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "Alert", "alert('Please Select the Employee Category');", true);
            ddlType.Focus();
        }
        else if(txtDay1.Text==string.Empty)
        {
            MSG = "F";
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "Alert", "alert('Please Enter Day1');", true);
            txtDay1.Focus();
        }
        else if (txtDay2.Text == string.Empty)
        {
            MSG = "F";
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "Alert", "alert('Please Enter Day2');", true);
            txtDay2.Focus();
        }
        else if (txtDay3.Text == string.Empty)
        {
            MSG = "F";
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "Alert", "alert('Please Enter Day3');", true);
            txtDay3.Focus();
        }
        else if (txtDay4.Text == string.Empty)
        {
            MSG = "F";
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "Alert", "alert('Please Enter Day4');", true);
            txtDay4.Focus();
        }
        else if (txtDay5.Text == string.Empty)
        {
            MSG = "F";
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "Alert", "alert('Please Enter Day5');", true);
            txtDay5.Focus();
        }
        else if (txtDay6.Text == string.Empty)
        {
            MSG = "F";
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "Alert", "alert('Please Enter Day6');", true);
            txtDay6.Focus();
        }
        else if (txtDay7.Text == string.Empty)
        {
            MSG = "F";
            ScriptManager.RegisterStartupScript(this, Page.GetType(), "Alert", "alert('Please Enter Day7');", true);
            txtDay7.Focus();
        }else
        {
            MSG = "S";
        }
        return MSG;
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        ddlType.ClearSelection();
        txtDay1.Text = string.Empty;
        txtDay2.Text = string.Empty;
        txtDay3.Text = string.Empty;
        txtDay4.Text = string.Empty;
        txtDay5.Text = string.Empty;
        txtDay6.Text = string.Empty;
        txtDay7.Text = string.Empty;
    }
    protected void btnEditIssueEntry_Grid_Command(object sender, CommandEventArgs e)
    {
        if (e.CommandName == "Edit")
        {
            DataTable dt = new DataTable();
            dt = objdata.RptEmployeeMultipleDetails("Select * from Mst_WeekSalary where EmpCatName='" + e.CommandArgument.ToString() + "'");
            ddlType.SelectedValue = dt.Rows[0][0].ToString();
            txtDay1.Text = dt.Rows[0][2].ToString();
            txtDay2.Text = dt.Rows[0][3].ToString();
            txtDay3.Text = dt.Rows[0][4].ToString();
            txtDay4.Text = dt.Rows[0][5].ToString();
            txtDay5.Text = dt.Rows[0][6].ToString();
            txtDay6.Text = dt.Rows[0][7].ToString();
            txtDay7.Text = dt.Rows[0][8].ToString();
            btnSave.Text = "Update";
        }
    }
    protected void btnDeleteEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {
        if (e.CommandName == "Delete")
        {
            SSQL = "";
            SSQL = "Delete From Mst_WeekSalary where EmpCatName='" + e.CommandArgument + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "alert('Category Deleted Successfully')", true);
            LoadGrid();
        }
    }
    protected void ddlType_TextChanged(object sender, EventArgs e)
    {

    }
}