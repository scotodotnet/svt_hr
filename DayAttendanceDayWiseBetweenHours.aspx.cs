﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;

public partial class DayAttendanceDayWiseBetweenHours : System.Web.UI.Page
{
    string ShiftType1 = "";
    string Date = "";

    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string AboveHrs;
    string BelowHrs;
    string ss = "";
    ReportDocument report = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();
    //  EmployeeDetailsClass objEmpDetails = new EmployeeDetailsClass();



    string SSQL = "";

    DataSet ds = new DataSet();

    string Division = "";
    DataTable AutoDTable = new DataTable();
    DataTable DataCell = new DataTable();
    DataTable dt = new DataTable();
   
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Report-Day Attendance Day Wise-Between Hours";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Dashboard"));
                //li.Attributes.Add("class", "droplink active open");
            }
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();

            //ModeType = Request.QueryString["ModeType"].ToString();
            ShiftType1 = Request.QueryString["ShiftType1"].ToString();
            Date = Request.QueryString["Date"].ToString();
            Division = Request.QueryString["Division"].ToString();
            AboveHrs = Request.QueryString["Above"].ToString();
            BelowHrs = Request.QueryString["Below"].ToString();
            if (SessionUserType == "2")
            {
              NonAdminGetAttdDayWise_Change();
            }
            else
            {
                GetAttdDayWise_Change();
            }
        }
    }

    public void GetAttdDayWise_Change()
    {

        Decimal Ded1;
        Decimal Ded2;
        Ded1 = (Convert.ToDecimal(AboveHrs.ToString()));
        Decimal DAboveHours = (Math.Round(Ded1, 1, MidpointRounding.AwayFromZero));

        Ded2 = (Convert.ToDecimal(BelowHrs.ToString()));
        Decimal DBelowHours = (Math.Round(Ded2, 1, MidpointRounding.AwayFromZero));


        DataCell.Columns.Add("CompanyName");
        DataCell.Columns.Add("LocationName");
        DataCell.Columns.Add("ShiftDate");
        DataCell.Columns.Add("SNo");
        DataCell.Columns.Add("Dept");
        DataCell.Columns.Add("Type");
        DataCell.Columns.Add("Shift");


        DataCell.Columns.Add("EmpCode");
        DataCell.Columns.Add("ExCode");
        DataCell.Columns.Add("Name");
        DataCell.Columns.Add("TimeIN");
        DataCell.Columns.Add("TimeOUT");
        DataCell.Columns.Add("MachineID");
        DataCell.Columns.Add("Category");
        DataCell.Columns.Add("SubCategory");
        DataCell.Columns.Add("TotalMIN");
        DataCell.Columns.Add("GrandTOT");




        // EM.Eligible_PF='1'

        SSQL = " Select LD.ExistingCode,LD.MachineID,LD.TimeIN,LD.TimeOUT,EM.DeptName,Em.TypeName,LD.Shift,";
        SSQL = SSQL + " LD.FirstName,LD.Total_Hrs,EM.CatName,Em.SubCatName from LogTime_Days LD  inner join Employee_Mst EM on LD.MachineID=EM.MachineID ";
        SSQL = SSQL + "where LD.CompCode='" + SessionCcode + "' and LD.LocCode='" + SessionLcode + "' and EM.CompCode ='" + SessionCcode + "'and ";
        SSQL = SSQL + "EM.LocCode='" + SessionLcode + "' and  LD.Total_Hrs >='" + DBelowHours + "' and  LD.Total_Hrs <='" + DAboveHours  + "'  ";
        // SSQL = SSQL + " and EM.IsActive='Yes' or CONVERT(DATETIME,EM.DOR,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(Date).ToString("dd/MM/yyyy") + "',103)";

        if (Division != "-Select-")
        {
            SSQL = SSQL + " And EM.Division = '" + Division + "'";
        }
        if (ShiftType1 != "ALL")
        {
            SSQL = SSQL + " And LD.Shift='" + ShiftType1 + "'";
        }
        if (Date != "")
        {
            SSQL = SSQL + " And CONVERT(DATETIME,LD.Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(Date).ToString("dd/MM/yyyy") + "',103)";
        }

        SSQL = SSQL + "  And LD.Shift !='No Shift' And LD.TimeIN!='' and LD.Total_Hrs !='0.00'  ";



        // 
        AutoDTable = objdata.RptEmployeeMultipleDetails(SSQL);


        SSQL = "Select * from Company_Mst  where CompCode ='" + SessionCcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        string name = dt.Rows[0]["CompName"].ToString();
        if (AutoDTable.Rows.Count != 0)
        {
            int sno = 1;
            for (int i = 0; i < AutoDTable.Rows.Count; i++)
            {
                DataCell.NewRow();
                DataCell.Rows.Add();


                DataCell.Rows[i]["SNo"] = sno;
                DataCell.Rows[i]["Dept"] = AutoDTable.Rows[i]["DeptName"].ToString();
                DataCell.Rows[i]["Type"] = AutoDTable.Rows[i]["TypeName"].ToString();
                DataCell.Rows[i]["Shift"] = AutoDTable.Rows[i]["Shift"].ToString();
                DataCell.Rows[i]["EmpCode"] = AutoDTable.Rows[i]["MachineID"].ToString();
                DataCell.Rows[i]["ExCode"] = AutoDTable.Rows[i]["ExistingCode"].ToString();
                DataCell.Rows[i]["Name"] = AutoDTable.Rows[i]["FirstName"].ToString();
                DataCell.Rows[i]["TimeIN"] = AutoDTable.Rows[i]["TimeIN"].ToString();
                DataCell.Rows[i]["TimeOUT"] = AutoDTable.Rows[i]["TimeOUT"].ToString();
                DataCell.Rows[i]["MachineID"] = AutoDTable.Rows[i]["MachineID"].ToString();
                DataCell.Rows[i]["Category"] = AutoDTable.Rows[i]["CatName"].ToString();
                DataCell.Rows[i]["SubCategory"] = AutoDTable.Rows[i]["SubCatName"].ToString();
                DataCell.Rows[i]["TotalMIN"] = AutoDTable.Rows[i]["Total_Hrs"].ToString();
                DataCell.Rows[i]["GrandTOT"] = AutoDTable.Rows[i]["Total_Hrs"].ToString();
                DataCell.Rows[i]["ShiftDate"] = Date;
                DataCell.Rows[i]["CompanyName"] = name.ToString();
                DataCell.Rows[i]["LocationName"] = SessionLcode;


                sno += 1;

            }

            ds.Tables.Add(DataCell);
            //ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("crystal/BetweenHours.rpt"));
            
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);

            report.DataDefinition.FormulaFields["New_Formula"].Text = "'" + "Total Number of Between " + BelowHrs + " TO " +  AboveHrs +"Hours :" + "'";
            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;

        }


    }

    public void NonAdminGetAttdDayWise_Change()
    {

        Decimal Ded1;
        Decimal Ded2;
        Ded1 = (Convert.ToDecimal(AboveHrs.ToString()));
        Decimal DAboveHours = (Math.Round(Ded1, 1, MidpointRounding.AwayFromZero));

        Ded2 = (Convert.ToDecimal(BelowHrs.ToString()));
        Decimal DBelowHours = (Math.Round(Ded2, 1, MidpointRounding.AwayFromZero));


        DataCell.Columns.Add("CompanyName");
        DataCell.Columns.Add("LocationName");
        DataCell.Columns.Add("ShiftDate");
        DataCell.Columns.Add("SNo");
        DataCell.Columns.Add("Dept");
        DataCell.Columns.Add("Type");
        DataCell.Columns.Add("Shift");


        DataCell.Columns.Add("EmpCode");
        DataCell.Columns.Add("ExCode");
        DataCell.Columns.Add("Name");
        DataCell.Columns.Add("TimeIN");
        DataCell.Columns.Add("TimeOUT");
        DataCell.Columns.Add("MachineID");
        DataCell.Columns.Add("Category");
        DataCell.Columns.Add("SubCategory");
        DataCell.Columns.Add("TotalMIN");
        DataCell.Columns.Add("GrandTOT");




        // EM.Eligible_PF='1'

        SSQL = " Select LD.ExistingCode,LD.MachineID,LD.TimeIN,LD.TimeOUT,EM.DeptName,Em.TypeName,LD.Shift,";
        SSQL = SSQL + " LD.FirstName,LD.Total_Hrs,EM.CatName,Em.SubCatName from LogTime_Days LD  inner join Employee_Mst EM on LD.MachineID=EM.MachineID ";
        SSQL = SSQL + "where LD.CompCode='" + SessionCcode + "' and LD.LocCode='" + SessionLcode + "' and EM.CompCode ='" + SessionCcode + "'and ";
        SSQL = SSQL + "EM.LocCode='" + SessionLcode + "'and EM.Eligible_PF='1' and  LD.Total_Hrs <='" + DBelowHours + "' and  LD.Total_Hrs >='" + DAboveHours + "'  ";
        // SSQL = SSQL + " and EM.IsActive='Yes' or CONVERT(DATETIME,EM.DOR,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(Date).ToString("dd/MM/yyyy") + "',103)";

        if (Division != "-Select-")
        {
            SSQL = SSQL + " And EM.Division = '" + Division + "'";
        }
        if (ShiftType1 != "ALL")
        {
            SSQL = SSQL + " And LD.Shift='" + ShiftType1 + "'";
        }
        if (Date != "")
        {
            SSQL = SSQL + " And CONVERT(DATETIME,LD.Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(Date).ToString("dd/MM/yyyy") + "',103)";
        }

        SSQL = SSQL + "  And LD.Shift !='No Shift' And LD.TimeIN!='' and LD.Total_Hrs !='0.00'  ";



        // 
        AutoDTable = objdata.RptEmployeeMultipleDetails(SSQL);


        SSQL = "Select * from Company_Mst  where CompCode ='" + SessionCcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        string name = dt.Rows[0]["CompName"].ToString();
        if (AutoDTable.Rows.Count != 0)
        {
            int sno = 1;
            for (int i = 0; i < AutoDTable.Rows.Count; i++)
            {
                DataCell.NewRow();
                DataCell.Rows.Add();


                DataCell.Rows[i]["SNo"] = sno;
                DataCell.Rows[i]["Dept"] = AutoDTable.Rows[i]["DeptName"].ToString();
                DataCell.Rows[i]["Type"] = AutoDTable.Rows[i]["TypeName"].ToString();
                DataCell.Rows[i]["Shift"] = AutoDTable.Rows[i]["Shift"].ToString();
                DataCell.Rows[i]["EmpCode"] = AutoDTable.Rows[i]["MachineID"].ToString();
                DataCell.Rows[i]["ExCode"] = AutoDTable.Rows[i]["ExistingCode"].ToString();
                DataCell.Rows[i]["Name"] = AutoDTable.Rows[i]["FirstName"].ToString();
                DataCell.Rows[i]["TimeIN"] = AutoDTable.Rows[i]["TimeIN"].ToString();
                DataCell.Rows[i]["TimeOUT"] = AutoDTable.Rows[i]["TimeOUT"].ToString();
                DataCell.Rows[i]["MachineID"] = AutoDTable.Rows[i]["MachineID"].ToString();
                DataCell.Rows[i]["Category"] = AutoDTable.Rows[i]["CatName"].ToString();
                DataCell.Rows[i]["SubCategory"] = AutoDTable.Rows[i]["SubCatName"].ToString();
                DataCell.Rows[i]["TotalMIN"] = AutoDTable.Rows[i]["Total_Hrs"].ToString();
                DataCell.Rows[i]["GrandTOT"] = AutoDTable.Rows[i]["Total_Hrs"].ToString();
                DataCell.Rows[i]["ShiftDate"] = Date;
                DataCell.Rows[i]["CompanyName"] = name.ToString();
                DataCell.Rows[i]["LocationName"] = SessionLcode;


                sno += 1;

            }

            ds.Tables.Add(DataCell);
            //ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("crystal/BetweenHours.rpt"));
            
            report.Database.Tables[0].SetDataSource(ds.Tables[0]);

            report.DataDefinition.FormulaFields["New_Formula"].Text = "'" + "Total Number of Between " + BelowHrs + " TO " + AboveHrs + "Hours :" + "'";
            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;

        }


    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        CrystalReportViewer1.Dispose();
    }
}
