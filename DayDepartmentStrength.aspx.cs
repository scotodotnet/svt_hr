﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;

public partial class DayDepartmentStrength : System.Web.UI.Page
{


    string ShiftType1 = "";
    string Date = "";
    string Status = "";

    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string ss = "";

    ReportDocument report = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();

    string SSQL = "";
    string Adolescent_Shift;
    DataSet ds = new DataSet();

    string Division = "";
    DataTable AutoDTable = new DataTable();
    DataTable DataCell = new DataTable();
    DataTable dt = new DataTable();


    string Datestr = "";
    string Datestr1 = "";
    string ShiftType = "";
    DataTable mDataSet = new DataTable();

    DataTable mLocalDS_INTAB = new DataTable();
    DataTable mLocalDS_OUTTAB = new DataTable();
    string Date_Value_Str;
    string Time_IN_Str = "";
    string Time_Out_Str = "";
    int time_Check_dbl = 0;
    string Total_Time_get = "";

    DataRow dtRow;

    DataTable MLocal_Day = new DataTable();
    DataTable Payroll_DS = new DataTable();

    string Shift_Start_Time;
    string Shift_End_Time;
    string Employee_Time = "";
    DateTime ShiftdateStartIN = new DateTime();
    DateTime ShiftdateEndIN = new DateTime();
    DateTime EmpdateIN = new DateTime();
    string ColumnName = "";
    double Totol_Hours_Check = 0;
    double Totol_Hours_Check_1 = 8.2;
    string[] Time_Split_Str;
    string Time_IN_Get;
    string Time_Out_Update = "";
    long Random_No_Get;
    string[] Time_Minus_Value_Check;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Report-Day Attendance Day Wise";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Dashboard"));
                //li.Attributes.Add("class", "droplink active open");
            }
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();

            Status = Request.QueryString["Status"].ToString();
         //   ShiftType1 = Request.QueryString["ShiftType1"].ToString();
            Date = Request.QueryString["Date"].ToString();
            Division = Request.QueryString["Division"].ToString();
            //if (SessionUserType == "1")
            //{
            //    GetAttdDayWise_Change();
            //}

            GetAttdDayWise_Change();
        }
    }

    private void GetAttdDayWise_Change()
    {
        AutoDTable.Columns.Add("MachineID");
        AutoDTable.Columns.Add("Dept");
        AutoDTable.Columns.Add("Name");
        AutoDTable.Columns.Add("TimeIN");
        AutoDTable.Columns.Add("TimeOUT");
        AutoDTable.Columns.Add("Improper");
        AutoDTable.Columns.Add("ShiftMismatch");
        AutoDTable.Columns.Add("Absent");
        AutoDTable.Columns.Add("Present");
        AutoDTable.Columns.Add("WeekOff");
        AutoDTable.Columns.Add("TotalHrs");
        AutoDTable.Columns.Add("OTHrs");

        DataTable Shift_DS = new DataTable();

        SSQL = "select EM.MachineID,DEP.DeptName,EM.Wages,DEP.WeekOff,DEP.Wages,EM.Shift,isnull(EM.FirstName,'') as FirstName,EM.TimeIN,EM.TimeOUT,";
        SSQL = SSQL + "DEP.SubCatName,EM.Total_Hrs,EM.Present_Absent,DEP.VPF,EM.Wh_Present_Count,EM.TypeName,EM.Total_Hrs1,DEP.CatName,DEP.BaseSalary,DEP.StdWrkHrs,DEP.OTEligible,DEP.IsActive from LogTime_Days EM  inner join Employee_Mst DEP on  EM.MachineID= DEP.MachineID ";
        SSQL = SSQL + " where EM.CompCode='" + SessionCcode + "' ANd EM.LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " And DEP.CompCode='" + SessionCcode + "' ANd DEP.LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " And DEP.DeptName!='CONTRACT'";

        if (Date != "")
        {
            SSQL = SSQL + " And CONVERT(DATETIME,EM.Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(Date).ToString("dd/MM/yyyy") + "',103)";
        }

        dt = objdata.RptEmployeeMultipleDetails(SSQL);

        
        string staffwages = "";
        string Wages = "";

        
        string Machineid = "";
        string Deptname = "";
        string workinghours = "";
        string stdwrkhrs = "";

        string OThours = "0"; //Edit by Narmatha

        //For GURUVAYURAPPAN 
        string STAFFImproper_Count = "0";
        string STAFFMismatch_Count = "0";
        string STAFFAbsent_Count = "0";
        string STAFFPresent_Count = "0";
        string STAFFTotalOT = "0";

        string WEEKLYImproper_Count = "0";
        string WEEKLYMismatch_Count = "0";
        string WEEKLYAbsent_Count = "0";
        string WEEKLYPresent_Count = "0";
        string WEEKLYTotalOT = "0";

        string WEEKLYHINDIImproper_Count = "0";
        string WEEKLYHINDIMismatch_Count = "0";
        string WEEKLYHINDIAbsent_Count = "0";
        string WEEKLYHINDIPresent_Count = "0";
        string WEEKLYHINDITotalOT = "0";

        //string MONTHLYImproper_Count = "0";
        //string MONTHLYMismatch_Count = "0";
        //string MONTHLYAbsent_Count = "0";
        //string MONTHLYPresent_Count = "0";
        //string MONTHLYTotalOT = "0";



        int OTPRESENT = 0;
        // int TBOT = 0,TGOT=0,HBOT=0,HGOT=0;
        int STAFFOT = 0, MOT = 0, MIIOT = 0, MIOT = 0, FNNBOT = 0;

        int TAB = 0, TMis = 0, TOT = 0, Timp = 0, TP = 0;

        string OTEligible = "";//By Selva
        string IsActive = "";//By Selva

        if (dt.Rows.Count > 0)
        {
            for(int i=0; i < dt.Rows.Count; i++)
            {
                Machineid = dt.Rows[i]["MachineID"].ToString();
                staffwages = dt.Rows[i]["CatName"].ToString();
                Deptname = dt.Rows[i]["DeptName"].ToString();
                stdwrkhrs = dt.Rows[i]["StdWrkHrs"].ToString();
                Wages = dt.Rows[i]["Wages"].ToString();

                OTEligible = dt.Rows[i]["OTEligible"].ToString();//By Selva
                IsActive = dt.Rows[i]["IsActive"].ToString();//By Selva
                workinghours = dt.Rows[i]["Total_Hrs"].ToString();
                
                if (stdwrkhrs == "" || stdwrkhrs == "0")
                {
                    stdwrkhrs = "8";
                }

                if(Machineid== "15222")
                {
                    Machineid = "15222";
                }

                AutoDTable.NewRow();
                AutoDTable.Rows.Add();

                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["MachineID"] = dt.Rows[i]["MachineID"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Name"] = dt.Rows[i]["FirstName"].ToString();
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Dept"] = dt.Rows[i]["DeptName"].ToString();

                if (dt.Rows[i]["Present_Absent"].ToString() == "Leave")
                {
                    if (dt.Rows[i]["WeekOff"].ToString() == (Convert.ToDateTime(Date).DayOfWeek.ToString()))
                    {
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Absent"] = "WH";
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Present"] = "-";
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TimeIN"] = "-";
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TimeOUT"] = "-";
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Improper"] = "-";
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1]["ShiftMismatch"] = "-";
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1]["WeekOff"] = "-";
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TotalHrs"] = "0";
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1]["OTHrs"] = "0";

                        
                        if (dt.Rows[i]["Wages"].ToString() == "MONTHLY - STAFF")
                        {
                            STAFFAbsent_Count = (Convert.ToInt32(STAFFAbsent_Count) + Convert.ToInt32(1)).ToString();
                        }
                        if (dt.Rows[i]["Wages"].ToString() == "WEEKLY")
                        {
                            WEEKLYAbsent_Count = (Convert.ToInt32(WEEKLYAbsent_Count) + Convert.ToInt32(1)).ToString();
                        }
                        if (dt.Rows[i]["Wages"].ToString() == "WEEKLY-HINDI")
                        {
                            WEEKLYHINDIAbsent_Count = (Convert.ToInt32(WEEKLYHINDIAbsent_Count) + Convert.ToInt32(1)).ToString();
                        }
                    }
                    else
                    {
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Absent"] = "A";
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Present"] = "-";
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TimeIN"] = "-";
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TimeOUT"] = "-";
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Improper"] = "-";
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1]["ShiftMismatch"] = "-";
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1]["WeekOff"] = "-";
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TotalHrs"] = "0";
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1]["OTHrs"] = "0";

                     

                        if (dt.Rows[i]["Wages"].ToString() == "MONTHLY - STAFF")
                        {
                            STAFFAbsent_Count = (Convert.ToInt32(STAFFAbsent_Count) + Convert.ToInt32(1)).ToString();
                        }
                        if (dt.Rows[i]["Wages"].ToString() == "WEEKLY")
                        {
                            WEEKLYAbsent_Count = (Convert.ToInt32(WEEKLYAbsent_Count) + Convert.ToInt32(1)).ToString();
                        }
                        if (dt.Rows[i]["Wages"].ToString() == "WEEKLY-HINDI")
                        {
                            WEEKLYHINDIAbsent_Count = (Convert.ToInt32(WEEKLYHINDIAbsent_Count) + Convert.ToInt32(1)).ToString();
                        }
                    }

                } else
                {
                    if (dt.Rows[i]["TypeName"].ToString() == "Improper")
                    {
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Improper"] = "IMP-PUNCH";
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1]["ShiftMismatch"] = "-";
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Absent"] = "-";
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1]["WeekOff"] = dt.Rows[i]["Wh_Present_Count"].ToString();
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Present"] = "-";
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TimeIN"] = dt.Rows[i]["TimeIN"].ToString();
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TimeOUT"] = dt.Rows[i]["TimeOUT"].ToString();
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TotalHrs"] = "0";
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1]["OTHrs"] = "0";

                       


                        //For Ramalinga
                        if (dt.Rows[i]["Wages"].ToString() == "MONTHLY - STAFF")
                        {
                            STAFFImproper_Count = (Convert.ToInt32(STAFFImproper_Count) + Convert.ToInt32(1)).ToString();
                            STAFFPresent_Count = (Convert.ToInt32(STAFFPresent_Count) + Convert.ToInt32(1)).ToString();
                        }
                        if (dt.Rows[i]["Wages"].ToString() == "WEEKLY")
                        {
                            WEEKLYImproper_Count = (Convert.ToInt32(WEEKLYImproper_Count) + Convert.ToInt32(1)).ToString();
                            WEEKLYPresent_Count = (Convert.ToInt32(WEEKLYPresent_Count) + Convert.ToInt32(1)).ToString();
                        }
                        if (dt.Rows[i]["Wages"].ToString() == "WEEKLY-HINDI")
                        {
                            WEEKLYHINDIImproper_Count = (Convert.ToInt32(WEEKLYHINDIImproper_Count) + Convert.ToInt32(1)).ToString();
                            WEEKLYHINDIPresent_Count = (Convert.ToInt32(WEEKLYHINDIPresent_Count) + Convert.ToInt32(1)).ToString();
                        }
                        
                    }
                    else if (dt.Rows[i]["TypeName"].ToString() == "Mismatch")
                    {
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Improper"] = "MISSED-SHIFT";
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1]["ShiftMismatch"] = "1";
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Absent"] = "-";
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1]["WeekOff"] = dt.Rows[i]["Wh_Present_Count"].ToString();
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Present"] = dt.Rows[i]["Shift"].ToString();
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TimeIN"] = dt.Rows[i]["TimeIN"].ToString();
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TimeOUT"] = dt.Rows[i]["TimeOUT"].ToString();
                        

                        if (staffwages == "STAFF" || Wages == "FITTER & ELECTRICIANS" || Wages == "SECURITY" || Wages == "DRIVERS")
                        {
                            if (dt.Rows[i]["OTEligible"].ToString().Trim().ToUpper() == "1")
                            {
                                //OT salary for Labour by Narmatha
                                string workinghours_New = (Convert.ToDecimal(workinghours)).ToString();
                                decimal check = 0;
                                check = Convert.ToDecimal(workinghours.ToString());
                                check = (Math.Round(check, 0, MidpointRounding.AwayFromZero));
                                //By Selva v
                                if (Convert.ToDecimal(check) >= Convert.ToDecimal(stdwrkhrs))
                                {
                                    OThours = (Convert.ToDecimal(workinghours_New) - Convert.ToDecimal(stdwrkhrs)).ToString();
                                    workinghours = (Convert.ToDecimal(workinghours_New) - Convert.ToDecimal(OThours)).ToString();
                                    OThours = Math.Round(Convert.ToDecimal(OThours), 0, MidpointRounding.AwayFromZero).ToString();
                                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TotalHrs"] =  workinghours_New;
                                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["OTHrs"] = OThours;
                                   

                                }
                                else
                                {
                                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TotalHrs"] = workinghours_New;
                                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["OTHrs"] = "0";
                                }
                            }
                            else
                            {
                                //OT salary for Labour by Narmatha
                                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TotalHrs"] = workinghours;
                                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["OTHrs"] = "0";
                            }
                        }
                        else
                        {
                            string workinghours_New = "0";
                            workinghours_New = (Convert.ToDecimal(workinghours)).ToString();

                            //By Selva v
                            if ((OTEligible == "1" && IsActive == "Yes") && (Convert.ToDecimal(workinghours_New) >= 8))
                            {

                                OThours = (Convert.ToDecimal(workinghours_New) - Convert.ToDecimal(stdwrkhrs)).ToString();
                                workinghours = (Convert.ToDecimal(workinghours_New) - Convert.ToDecimal(OThours)).ToString();
                                OThours = Math.Round(Convert.ToDecimal(OThours), 0, MidpointRounding.AwayFromZero).ToString();
                                
                                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TotalHrs"] = workinghours_New;
                                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["OTHrs"] =OThours;
                                
                            }
                            //By Selva ^
                            else
                            {
                                OThours = "0";
                                workinghours = Convert.ToDecimal(workinghours_New).ToString();/// - Convert.ToDecimal(OThours)).ToString();
                                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TotalHrs"] = workinghours_New;
                                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["OTHrs"] = "0";
                            }

                            if (dt.Rows[i]["Shift"].ToString().Trim().ToUpper() == "SHIFT3")
                            {
                                //By Selva v
                                if ((OTEligible == "1" && IsActive == "Yes") && (Convert.ToDecimal(workinghours_New) >= 7))
                                {
                                    OThours = (Convert.ToDecimal(workinghours_New) - Convert.ToDecimal(7)).ToString();
                                    workinghours = (Convert.ToDecimal(workinghours_New) - Convert.ToDecimal(OThours)).ToString();
                                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TotalHrs"] = workinghours_New;
                                    OThours = Math.Round(Convert.ToDecimal(OThours), 0, MidpointRounding.AwayFromZero).ToString();
                                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["OTHrs"] = OThours;
                                    
                                }
                                //By Selva ^
                                else
                                {
                                    OThours = "0";
                                    workinghours = workinghours_New.ToString();
                                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TotalHrs"] = workinghours_New;
                                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["OTHrs"] = "0";
                                }
                            }
                        }
                        //For Ramalinga
                        if (dt.Rows[i]["Wages"].ToString() == "MONTHLY - STAFF")
                        {
                            STAFFMismatch_Count = (Convert.ToInt32(STAFFMismatch_Count) + Convert.ToInt32(1)).ToString();
                            STAFFPresent_Count = (Convert.ToInt32(STAFFPresent_Count) + Convert.ToInt32(1)).ToString();
                            OThours = Math.Round(Convert.ToDecimal(OThours), 0, MidpointRounding.AwayFromZero).ToString();
                            STAFFTotalOT = (Convert.ToInt32(STAFFTotalOT) + Convert.ToInt32(OThours)).ToString();
                            if (Convert.ToInt32(OThours) > 0)
                            {
                                STAFFOT = STAFFOT + 1;
                            }
                        }
                        if (dt.Rows[i]["Wages"].ToString() == "WEEKLY")
                        {
                            WEEKLYMismatch_Count = (Convert.ToInt32(WEEKLYMismatch_Count) + Convert.ToInt32(1)).ToString();
                            WEEKLYPresent_Count = (Convert.ToInt32(WEEKLYPresent_Count) + Convert.ToInt32(1)).ToString();
                            OThours = Math.Round(Convert.ToDecimal(OThours), 0, MidpointRounding.AwayFromZero).ToString();
                            WEEKLYTotalOT = (Convert.ToInt32(WEEKLYTotalOT) + Convert.ToInt32(OThours)).ToString();
                            if (Convert.ToInt32(OThours) > 0)
                            {
                                MOT = MOT + 1;
                            }
                        }
                        if (dt.Rows[i]["Wages"].ToString() == "WEEKLY-HINDI")
                        {
                            WEEKLYHINDIMismatch_Count = (Convert.ToInt32(WEEKLYHINDIMismatch_Count) + Convert.ToInt32(1)).ToString();
                            WEEKLYHINDIPresent_Count = (Convert.ToInt32(WEEKLYHINDIPresent_Count) + Convert.ToInt32(1)).ToString();
                            OThours = Math.Round(Convert.ToDecimal(OThours), 0, MidpointRounding.AwayFromZero).ToString();
                            WEEKLYHINDITotalOT = (Convert.ToInt32(WEEKLYHINDITotalOT) + Convert.ToInt32(OThours)).ToString();
                            if (Convert.ToInt32(OThours) > 0)
                            {
                                MIIOT = MIIOT + 1;
                            }
                        }
                       

                    }
                    else
                    {
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Improper"] = "-";
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1]["ShiftMismatch"] = "-";
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Absent"] = "-";
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1]["WeekOff"] = dt.Rows[i]["Wh_Present_Count"].ToString();
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Present"] = dt.Rows[i]["Shift"].ToString();
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TimeIN"] = dt.Rows[i]["TimeIN"].ToString();
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TimeOUT"] = dt.Rows[i]["TimeOUT"].ToString();

                        if (staffwages == "STAFF" || Wages == "FITTER & ELECTRICIANS" || Wages == "SECURITY" || Wages == "DRIVERS")
                        {
                            if (dt.Rows[i]["OTEligible"].ToString().Trim().ToUpper() == "1")
                            {
                                //OT salary for Labour by Narmatha
                                string workinghours_New = (Convert.ToDecimal(workinghours)).ToString();
                                decimal check = 0;
                                check = Convert.ToDecimal(workinghours.ToString());
                                check = (Math.Round(check, 0, MidpointRounding.AwayFromZero));
                                //By Selva v
                                if (Convert.ToDecimal(check) >= Convert.ToDecimal(stdwrkhrs))
                                {
                                    OThours = (Convert.ToDecimal(workinghours_New) - Convert.ToDecimal(stdwrkhrs)).ToString();
                                    workinghours = (Convert.ToDecimal(workinghours_New) - Convert.ToDecimal(OThours)).ToString();
                                    OThours = Math.Round(Convert.ToDecimal(OThours), 0, MidpointRounding.AwayFromZero).ToString();
                                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TotalHrs"] = workinghours_New;
                                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["OTHrs"] = OThours;
                                    
                                }
                                else
                                {
                                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TotalHrs"] = workinghours_New;
                                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["OTHrs"] = "0";
                                }
                            }
                            else
                            {
                                //OT salary for Labour by Narmatha
                                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TotalHrs"] = workinghours;
                                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["OTHrs"] = "0";
                            }
                        }
                        else
                        {
                           // if()
                            string workinghours_New = "0";
                            workinghours_New = (Convert.ToDecimal(workinghours)).ToString();

                            //By Selva v
                            if ((OTEligible == "1" && IsActive == "Yes") && (Convert.ToDecimal(workinghours_New) >= 8))
                            {

                                OThours = (Convert.ToDecimal(workinghours_New) - Convert.ToDecimal(stdwrkhrs)).ToString();
                                workinghours = (Convert.ToDecimal(workinghours_New) - Convert.ToDecimal(OThours)).ToString();
                                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TotalHrs"] = workinghours_New;
                                OThours = Math.Round(Convert.ToDecimal(OThours), 0, MidpointRounding.AwayFromZero).ToString();
                                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["OTHrs"] = OThours;
                                
                            }
                            //By Selva ^
                            else
                            {
                                OThours = "0";
                                workinghours = Convert.ToDecimal(workinghours_New).ToString();/// - Convert.ToDecimal(OThours)).ToString();
                                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TotalHrs"] = workinghours_New;
                                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["OTHrs"] = "0";
                            }

                            if (dt.Rows[i]["Shift"].ToString().Trim().ToUpper() == "SHIFT3")
                            {
                                //By Selva v
                                if ((OTEligible == "1" && IsActive == "Yes") && (Convert.ToDecimal(workinghours_New) >= 7))
                                {
                                    OThours = (Convert.ToDecimal(workinghours_New) - Convert.ToDecimal(7)).ToString();
                                    workinghours = (Convert.ToDecimal(workinghours_New) - Convert.ToDecimal(OThours)).ToString();
                                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TotalHrs"] = workinghours_New;
                                    OThours = Math.Round(Convert.ToDecimal(OThours), 0, MidpointRounding.AwayFromZero).ToString();
                                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["OTHrs"] = OThours;
                                   
                                }
                                //By Selva ^
                                else
                                {
                                    OThours = "0";
                                    workinghours = workinghours_New.ToString();
                                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TotalHrs"] = workinghours_New;
                                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["OTHrs"] = "0";
                                }
                            }
                        }
           

                        //For Ramalinga
                        if (dt.Rows[i]["Wages"].ToString() == "MONTHLY - STAFF")
                        {

                            STAFFPresent_Count = (Convert.ToInt32(STAFFPresent_Count) + Convert.ToInt32(1)).ToString();
                            OThours = Math.Round(Convert.ToDecimal(OThours), 0, MidpointRounding.AwayFromZero).ToString();
                            STAFFTotalOT = (Convert.ToInt32(STAFFTotalOT) + Convert.ToInt32(OThours)).ToString();
                            if (Convert.ToInt32(OThours) > 0)
                            {
                                STAFFOT = STAFFOT + 1;
                            }
                        }
                        if (dt.Rows[i]["Wages"].ToString() == "WEEKLY")
                        {

                            WEEKLYPresent_Count = (Convert.ToInt32(WEEKLYPresent_Count) + Convert.ToInt32(1)).ToString();
                            OThours = Math.Round(Convert.ToDecimal(OThours), 0, MidpointRounding.AwayFromZero).ToString();
                            WEEKLYTotalOT = (Convert.ToInt32(WEEKLYTotalOT) + Convert.ToInt32(OThours)).ToString();
                            if (Convert.ToInt32(OThours) > 0)
                            {
                                MOT = MOT + 1;
                            }
                        }
                        if (dt.Rows[i]["Wages"].ToString() == "WEEKLY-HINDI")
                        {

                            WEEKLYHINDIPresent_Count = (Convert.ToInt32(WEEKLYHINDIPresent_Count) + Convert.ToInt32(1)).ToString();
                            OThours = Math.Round(Convert.ToDecimal(OThours), 0, MidpointRounding.AwayFromZero).ToString();
                            WEEKLYHINDITotalOT = (Convert.ToInt32(WEEKLYHINDITotalOT) + Convert.ToInt32(OThours)).ToString();
                            if (Convert.ToInt32(OThours) > 0)
                            {
                                MIIOT = MIIOT + 1;
                            }
                        }
                    }
                }
            }

            TOT = Convert.ToInt32(STAFFOT) + Convert.ToInt32(WEEKLYTotalOT) + Convert.ToInt32(WEEKLYHINDITotalOT) ;
            TP = Convert.ToInt32(STAFFPresent_Count) + Convert.ToInt32(WEEKLYPresent_Count) + Convert.ToInt32(WEEKLYHINDIPresent_Count) ;
            TAB = Convert.ToInt32(STAFFAbsent_Count) + Convert.ToInt32(WEEKLYAbsent_Count) + Convert.ToInt32(WEEKLYHINDIAbsent_Count) ;
            Timp = Convert.ToInt32(STAFFImproper_Count) + Convert.ToInt32(WEEKLYImproper_Count) + Convert.ToInt32(WEEKLYHINDIImproper_Count) ;
            TMis = Convert.ToInt32(STAFFMismatch_Count) + Convert.ToInt32(WEEKLYMismatch_Count) + Convert.ToInt32(WEEKLYHINDIMismatch_Count) ;
            OTPRESENT = STAFFOT + MOT + MIIOT + MIOT + FNNBOT;

        }
        if (AutoDTable.Rows.Count > 0)
        {
            report.Load(Server.MapPath("crystal/DayDeptStrength.rpt"));

          

            //For Ramalinga
            report.DataDefinition.FormulaFields["STAFFImproper"].Text = "'" + STAFFImproper_Count + "'";
            report.DataDefinition.FormulaFields["STAFFMismatch"].Text = "'" + STAFFMismatch_Count + "'";
            report.DataDefinition.FormulaFields["STAFFAbsent"].Text = "'" + STAFFAbsent_Count + "'";
            report.DataDefinition.FormulaFields["STAFFPresent"].Text = "'" + STAFFOT + "'";
            report.DataDefinition.FormulaFields["STAFFTotalOT"].Text = "'" + STAFFTotalOT + "'";

            report.DataDefinition.FormulaFields["MONTHLYImproper"].Text = "'" + WEEKLYImproper_Count + "'";
            report.DataDefinition.FormulaFields["MONTHLYMismatch"].Text = "'" + WEEKLYMismatch_Count + "'";
            report.DataDefinition.FormulaFields["MONTHLYAbsent"].Text = "'" + WEEKLYAbsent_Count + "'";
            report.DataDefinition.FormulaFields["MONTHLYPresent"].Text = "'" + MOT + "'";
            report.DataDefinition.FormulaFields["MONTHLYTotalOT"].Text = "'" + WEEKLYTotalOT + "'";

            report.DataDefinition.FormulaFields["MONTHLYIIImproper"].Text = "'" + WEEKLYHINDIImproper_Count + "'";
            report.DataDefinition.FormulaFields["MONTHLYIIMismatch"].Text = "'" + WEEKLYHINDIMismatch_Count + "'";
            report.DataDefinition.FormulaFields["MONTHLYIIAbsent"].Text = "'" + WEEKLYHINDIAbsent_Count + "'";
            report.DataDefinition.FormulaFields["MONTHLYIIPresent"].Text = "'" + MIIOT + "'";
            report.DataDefinition.FormulaFields["MONTHLYIITotalOT"].Text = "'" + WEEKLYHINDITotalOT + "'";


            report.DataDefinition.FormulaFields["TImp"].Text = "'" + Timp + "'";
            report.DataDefinition.FormulaFields["TMis"].Text = "'" + TMis + "'";
            report.DataDefinition.FormulaFields["TAB"].Text = "'" + TAB + "'";
            report.DataDefinition.FormulaFields["TP"].Text = "'" + OTPRESENT + "'";
            report.DataDefinition.FormulaFields["TOT"].Text = "'" + TOT + "'";
            report.DataDefinition.FormulaFields["ReportDT"].Text = "'"+Convert.ToDateTime(Date).ToString("dd/MM/yyyy")+"'";

            report.Database.Tables[0].SetDataSource(AutoDTable);
            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;
        }
    }
}