﻿<%@ Page Language="C#" MasterPageFile="~/PayrollMaster.master" AutoEventWireup="true" CodeFile="RptUnpaidVoucher.aspx.cs" Inherits="RptUnpaidVoucher" Title=""%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script src="assets/js/master_list_jquery.min.js"></script>
<script src="assets/js/master_list_jquery-ui.min.js"></script>
<link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css"/>

<script>
    $(document).ready(function() {
        //alert('hi');
        $('#example').dataTable();
        $('.select2').select2();
        $('.datepicker').datepicker({
            format: "dd/mm/yyyy",
            autoclose: true
        });
    });
</script>
<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
        if (sender._postBackSettings.panelsToUpdate != null) {
            $('.select2').select2();
                $('.datepicker').datepicker({
                    format: "dd/mm/yyyy",
                    autoclose: true
                });
            }
            $('#example').dataTable();
        });
    };
</script>


 <!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Report</a></li>
				<li class="active">UnPaid Voucher</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">UnPaid Voucher</h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">UnPaid Voucher</h4>
                        </div>
                        <div class="panel-body">
                        <div class="form-group">
                    
                     <!-- begin row -->
                        <div class="row">
                        <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Token No</label>
								 <asp:DropDownList runat="server" ID="ddltokenNo" class="form-control select2" 
                                        style="width:100%;">
								 </asp:DropDownList>
								 <asp:RequiredFieldValidator ControlToValidate="ddltokenNo" InitialValue="-Select-" Display="Dynamic"  ValidationGroup="Validate_PayField" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
								</div>
                               </div>
                           <!-- end col-4 -->
                           
                            <div class="col-md-2">
								 <div class="form-group">
									<br />
									 <label>Salary Confirmed</label>
                               <asp:CheckBox ID="chksalry" runat="server" />
						    	 </div>
                               </div>
                       
                           <!-- begin col-4 -->
                           <div class="col-md-3">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btnReport" Text="Report" class="btn btn-success" ValidationGroup="Validate_PayField" onclick="btnReport_Click"/>
									
						    	 </div>
                               </div>
 </div>
                       <!-- end row -->
                       
                       <!-- table start -->
					                        <div class="col-md-12">
					    <div class="row">
					        <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
			                    <HeaderTemplate>
                                    <table id="example" class="display table">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th>TokenNo</th>
                                                <th>Month</th>
                                                <th>FinancialYear</th>
                                                <th>NetAmount</th>
                                                <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Container.ItemIndex + 1 %></td>
                                        <td><%# Eval("ExistingCode")%></td>
                                        <td><%# Eval("Month")%></td>
                                        <td><%# Eval("FinancialYear")%></td>
                                        <td><%# Eval("NetAmount")%></td>
                                       
                                        
                                        <td>
                                         <asp:LinkButton ID="LinkButton1" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                                                        Text="" OnCommand="GridEditEntryClick" CommandArgument='<%#Eval("Month")+","+ Eval("FinancialYear")%>' CommandName='<%# Eval("ExistingCode")%>' 
                                                        CausesValidation="true" OnClientClick="return confirm('Are you sure you want to Confirm this Salary details?');">
                                         </asp:LinkButton>
                                         <asp:LinkButton ID="LinkButton2" class="btn btn-success btn-sm fa fa-print"  runat="server" 
                                                        Text="" OnCommand="GridPrintEntryClick" CommandArgument='<%#Eval("Month")+","+ Eval("FinancialYear")%>' CommandName='<%# Eval("ExistingCode")%>'>
                                         </asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater>
					    </div>
					</div>
					                      <!-- table End -->
                       
                       </div> 
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
<!-- end #content -->



</asp:Content>