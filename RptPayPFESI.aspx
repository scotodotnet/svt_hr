﻿<%@ Page Language="C#" MasterPageFile="~/PayrollMaster.master" AutoEventWireup="true" CodeFile="RptPayPFESI.aspx.cs" Inherits="RptPayPFESI" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">



<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
        if (sender._postBackSettings.panelsToUpdate != null) {
            $('.select2').select2();
                $('.datepicker').datepicker({
                    format: "dd/mm/yyyy",
                    autoclose: true
                });
            }
        });
    };
</script>

  <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                  <ContentTemplate>

 <!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Report</a></li>
				<li class="active">PF/ESI</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">PF/ESI</h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">PF/ESI</h4>
                        </div>
                        <div class="panel-body">
                        <div class="form-group">
                    
                     <!-- begin row -->
                        <div class="row">
                         <!-- begin col-4 -->
                              <div class="col-md-5">
								<div class="form-group">
								 <label>Report Type</label>
								     <asp:RadioButtonList ID="rbsalary" runat="server" RepeatColumns="4" class="form-control">
                                        <asp:ListItem Text ="ESI" Value="1" style="padding-right:30px" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="ESI ONLINE" style="padding-right:30px" Value="2" ></asp:ListItem>                                                                                                                                       
                                        <asp:ListItem Text="PF" style="padding-right:30px" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="PF ONLINE" Value="4"></asp:ListItem>
                                     </asp:RadioButtonList>
								 </div>
                               </div>
                           <!-- end col-4 -->
                        <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Month</label>
								 <asp:DropDownList runat="server" ID="txtFrom_Month" class="form-control select2" style="width:100%;">
							 	 </asp:DropDownList>
								</div>
                               </div>
                           <!-- end col-4 -->
                          <!-- begin col-4 -->
                              <div class="col-md-3">
								<div class="form-group">
								 <label>Fin. Year</label>
								 <asp:DropDownList runat="server" ID="txtFinancial_Year" class="form-control select2" style="width:100%;">
							 	 </asp:DropDownList>
								</div>
                               </div>
                           <!-- end col-4 -->
                           
                        </div>
                       <!-- end row -->
                       <!-- begin row -->
                        <div class="row">
                        <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Division</label>
								 <asp:DropDownList runat="server" ID="txtDivision" class="form-control select2" style="width:100%;">
								 </asp:DropDownList>
								</div>
                               </div>
                           <!-- end col-4 -->
                            <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								  <asp:CheckBox ID="ChkLeft" runat="server" />LeftEmployee
								 <asp:TextBox ID="txtLeftDate" runat="server" class="form-control datepicker"></asp:TextBox>
								</div>
                               </div>
                           <!-- end col-4 -->
                            <!-- begin col-4 -->
                                <div class="col-md-4">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btnexport" Text="Report" class="btn btn-success" onclick="btnexport_Click"/>
									
						    	 </div>
                               </div>
                              <!-- end col-4 -->
                        </div>
                       <!-- end row -->
                       
                       <legend>PF / ESI ADDITION DELETION DETAILS</legend>
                       
                            <!-- begin row -->
                        <div class="row">
                           <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Category</label>
								 <asp:DropDownList runat="server" ID="ddlcategory" class="form-control select2" 
                                        style="width:100%;" AutoPostBack="true" 
                                        onselectedindexchanged="ddlcategory_SelectedIndexChanged">
								 <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
								 <asp:ListItem Value="STAFF">STAFF</asp:ListItem>
								 <asp:ListItem Value="LABOUR">LABOUR</asp:ListItem>
								</asp:DropDownList>
								</div>
                               </div>
                           <!-- end col-4 -->
                           <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Employee Type</label>
								 <asp:DropDownList runat="server" ID="txtEmployeeType" class="form-control select2" style="width:100%;">
								 </asp:DropDownList>
								</div>
                               </div>
                           <!-- end col-4 -->
                           <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Month</label>
								 <asp:DropDownList runat="server" ID="txtPFMonth" class="form-control select2" style="width:100%;">
								 </asp:DropDownList>
								</div>
                               </div>
                           <!-- end col-4 -->
                        </div>
                         <!-- end row -->
                          <!-- begin row -->  
                        <div class="row">
                         <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Year</label>
								 <asp:DropDownList runat="server" ID="txtYear" class="form-control select2" style="width:100%;">
								 </asp:DropDownList>
								</div>
                               </div>
                           <!-- end col-4 -->
                           <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>From Date</label>
								 <asp:TextBox runat="server" ID="txt3AFromDate" class="form-control datepicker">
								 </asp:TextBox>
								</div>
                               </div>
                           <!-- end col-4 -->
                           <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>To Date</label>
								 <asp:TextBox runat="server" ID="txt3AToDate" class="form-control datepicker" >
								 </asp:TextBox>
								</div>
                               </div>
                           <!-- end col-4 -->
                        </div>
                        <!-- end row --> 
                        
                        <div class="row">
                         <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Division</label>
								 <asp:DropDownList runat="server" ID="txtDivision_PF" class="form-control select2" style="width:100%;">
								 </asp:DropDownList>
								</div>
                               </div>
                               
                                 <div class="col-md-4">
								<div class="form-group">
								</br>
								<asp:CheckBox ID="Chkbox" Text ="Adolocent" runat="server" />
                      
                   </div>
                   </div>
                           <!-- end col-4 -->
                        </div>
                        
                         <!-- begin row -->  
                        <div class="row">
                        <div class="col-md-2"></div>
                           <!-- begin col-4 -->
                                <div class="col-md-8">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btnPFForm3A" Text="Form 3A" class="btn btn-success" onclick="btnPFForm3A_Click"/>
									<asp:Button runat="server" id="btnPFAddLeft" Text="PF Addition Deletion" class="btn btn-success" onclick="btnPFAddLeft_Click"/>
									<asp:Button runat="server" id="btnESIAddLeft" Text="ESI Addition Deletion" class="btn btn-success" onclick="btnESIAddLeft_Click"/>
									<asp:Button runat="server" id="btnEmpAddLeft" Text="Employee Addition Deletion" class="btn btn-success" onclick="btnEmpAddLeft_Click"/>
									
						    	 </div>
                               </div>
                              <!-- end col-4 -->
                             <div class="col-md-2"></div>
                         </div>
                        <!-- end row --> 
                        
                        
                        <asp:Panel runat="server" Visible="false" ID="PnlPFAddition">
                         <tr id="PFAddition" runat="server" visible="false">
                                                        <td colspan="4">
                                                            <%--PF Addition--%>
                                                            <asp:GridView ID="GVPFAddition" runat="server" AutoGenerateColumns="false">
                                                                <Columns>
                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                                        <HeaderTemplate>SL No</HeaderTemplate>
                                                                        <ItemTemplate><%# Container.DataItemIndex + 1 %></ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Token No</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                        <asp:Label ID="lblTokenNo" runat="server" Text= '<%# Eval("ExisistingCode") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Name</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblEmpName" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>F.Name/Husband Name</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblFName" runat="server" Text='<%# Eval("FatherName") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Department</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDepartment" runat="server" Text='<%# Eval("DepartmentNm") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>DOB</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDOB" runat="server" Text='<%# Eval("DOB") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>                                                                    
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>PF DOJ</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblPFDOJ" runat="server" Text='<%# Eval("PFDOJ") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>PF No</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblPFNo" runat="server" Text='<%# Eval("PFnumber") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>ESI NO</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblESINo" runat="server" Text='<%# Eval("ESICnumber") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>                                                                    
                                                                </Columns>
                                                            </asp:GridView>
                                                            <%--PF Deletion--%>
                                                            <asp:GridView ID="GVPFDeletion" runat="server" AutoGenerateColumns="false">
                                                                <Columns>
                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                                        <HeaderTemplate>SL No</HeaderTemplate>
                                                                        <ItemTemplate><%# Container.DataItemIndex + 1 %></ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Token No</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                        <asp:Label ID="lblTokenNo" runat="server" Text= '<%# Eval("ExisistingCode") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Name</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblEmpName" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Department</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDepartment" runat="server" Text='<%# Eval("DepartmentNm") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>PF No</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblPFNo" runat="server" Text='<%# Eval("PFnumber") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>ESI NO</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblESINo" runat="server" Text='<%# Eval("ESICnumber") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>                                                                     
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>PF DOJ</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblPFDOJ" runat="server" Text='<%# Eval("PFDOJ") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Left Date</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblLEFT_Date" runat="server" Text='<%# Eval("LEFT_Date") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                                                                                       
                                                                </Columns>
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                          
                            <tr id="ESIAddition" runat="server" visible="false">
                                                        <td colspan="4">
                                                            <%--ESI Addition--%>
                                                            <asp:GridView ID="GVESIAddition" runat="server" AutoGenerateColumns="false">
                                                                <Columns>
                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                                        <HeaderTemplate>SL No</HeaderTemplate>
                                                                        <ItemTemplate><%# Container.DataItemIndex + 1 %></ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Token No</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                        <asp:Label ID="lblTokenNo" runat="server" Text= '<%# Eval("ExisistingCode") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Name</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblEmpName" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>F.Name/Husband Name</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblFName" runat="server" Text='<%# Eval("FatherName") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Department</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDepartment" runat="server" Text='<%# Eval("DepartmentNm") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>DOB</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDOB" runat="server" Text='<%# Eval("DOB") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>                                                                    
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>ESI DOJ</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblPFDOJ" runat="server" Text='<%# Eval("ESIDOJ") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>PF No</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblPFNo" runat="server" Text='<%# Eval("PFnumber") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>ESI NO</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblESINo" runat="server" Text='<%# Eval("ESICnumber") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                            <%--ESI Deletion--%>
                                                            <asp:GridView ID="GVESIDeletion" runat="server" AutoGenerateColumns="false">
                                                                <Columns>
                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                                        <HeaderTemplate>SL No</HeaderTemplate>
                                                                        <ItemTemplate><%# Container.DataItemIndex + 1 %></ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Token No</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                        <asp:Label ID="lblTokenNo" runat="server" Text= '<%# Eval("ExisistingCode") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Name</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblEmpName" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Department</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDepartment" runat="server" Text='<%# Eval("DepartmentNm") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>PF No</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblPFNo" runat="server" Text='<%# Eval("PFnumber") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>ESI NO</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblESINo" runat="server" Text='<%# Eval("ESICnumber") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>                                                                     
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>ESI DOJ</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblPFDOJ" runat="server" Text='<%# Eval("ESIDOJ") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Left Date</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblLEFT_Date" runat="server" Text='<%# Eval("LEFT_Date") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                                                                                       
                                                                </Columns>
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                    
                                                    <tr id="EmpAddition" runat="server" visible="false">
                                                        <td colspan="4">
                                                            <%--Employee Addition--%>
                                                            <asp:GridView ID="GVEmpAddLeft" runat="server" AutoGenerateColumns="false">
                                                                <Columns>
                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                                        <HeaderTemplate>SL No</HeaderTemplate>
                                                                        <ItemTemplate><%# Container.DataItemIndex + 1 %></ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Token No</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                        <asp:Label ID="lblTokenNo" runat="server" Text= '<%# Eval("ExisistingCode") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Name</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblEmpName" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>F.Name/Husband Name</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblFName" runat="server" Text='<%# Eval("FatherName") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Department</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDepartment" runat="server" Text='<%# Eval("DepartmentNm") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Designation</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDesignation" runat="server" Text='<%# Eval("Designation") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                                        <HeaderTemplate>DOJ</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDOJ" runat="server" Text='<%# Eval("DOJ") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                                        <HeaderTemplate>PF DOJ</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblPFDOJ" runat="server" Text='<%# Eval("PFDOJ") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                                        <HeaderTemplate>PF No</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblPFNo" runat="server" Text='<%# Eval("PFnumber") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                                        <HeaderTemplate>ESI DOJ</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblESIDOJ" runat="server" Text='<%# Eval("ESIDOJ") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                                        <HeaderTemplate>ESI No</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblESINo" runat="server" Text='<%# Eval("ESICnumber") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Wages</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblPFNo" runat="server" Text='<%# Eval("Base") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                            <%--Employee Deletion--%>
                                                            <asp:GridView ID="GVEmpDeletion" runat="server" AutoGenerateColumns="false">
                                                                <Columns>
                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                                        <HeaderTemplate>SL No</HeaderTemplate>
                                                                        <ItemTemplate><%# Container.DataItemIndex + 1 %></ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Token No</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                        <asp:Label ID="lblTokenNo" runat="server" Text= '<%# Eval("ExisistingCode") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Name</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblEmpName" runat="server" Text='<%# Eval("EmpName") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>F.Name/Husband Name</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblFName" runat="server" Text='<%# Eval("FatherName") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Department</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDepartment" runat="server" Text='<%# Eval("DepartmentNm") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Designation</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDesignation" runat="server" Text='<%# Eval("Designation") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                                        <HeaderTemplate>DOJ</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblDOJ" runat="server" Text='<%# Eval("DOJ") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                                        <HeaderTemplate>PF DOJ</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblPFDOJ" runat="server" Text='<%# Eval("PFDOJ") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                                        <HeaderTemplate>PF No</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblPFNo" runat="server" Text='<%# Eval("PFnumber") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                                        <HeaderTemplate>ESI DOJ</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblESIDOJ" runat="server" Text='<%# Eval("ESIDOJ") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                                        <HeaderTemplate>ESI No</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblESINo" runat="server" Text='<%# Eval("ESICnumber") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <HeaderTemplate>Wages</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblPFNo" runat="server" Text='<%# Eval("Base") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                                                        <HeaderTemplate>Left Date</HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblLeft_Date" runat="server" Text='<%# Eval("Left_Date") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                        <HeaderStyle BackColor="#E7E7E7" BorderStyle="Solid" CssClass="grid_heading"/>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                          
                        </asp:Panel>
                        
                        </div>
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
<!-- end #content -->


</ContentTemplate>
                  <Triggers>
                     <asp:PostBackTrigger ControlID="btnEmpAddLeft" />
                     <asp:PostBackTrigger ControlID="btnESIAddLeft" />
                     <asp:PostBackTrigger ControlID="btnPFAddLeft" />
                     <asp:PostBackTrigger ControlID="btnPFForm3A" />
                  </Triggers>
                </asp:UpdatePanel>


</asp:Content>

