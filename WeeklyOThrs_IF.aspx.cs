﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Globalization;
using System.Threading;

public partial class WeeklyOThrs_IF : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SSQL = "";
    long Random_No_Get;
    DateTime TimeIN;
    DateTime TimeOUT;
    string SessionPayroll;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        Load_DB();
        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Monthly Salary OT";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");
            Initial_Data_Referesh();
            Load_WagesType();
            Load_Data_EmpDet();
            Fin_Year_Add();
        }
        Load_OLD_data();
    }
    public void Load_DB()
    {
        //Get Database Name
        string query = "";
        DataTable dt_DB = new DataTable();
        query = "select CMS,Rights,Sales,Stores,Weaving,Maintaince,Production,Payroll from [SVT_Rights]..MstDBname";
        dt_DB = objdata.RptEmployeeMultipleDetails(query);
        if (dt_DB.Rows.Count > 0)
        {
            SessionPayroll = dt_DB.Rows[0]["Payroll"].ToString();
            //SessionRights = dt_DB.Rows[0]["Rights"].ToString();
        }
    }

    public void Fin_Year_Add()
    {
        int CurrentYear;


        CurrentYear = System.DateTime.Now.Year;

        ddlYear.Items.Clear();
        ddlYear.Items.Add("-Select-");
        for (int i = 0; i < 11; i++)
        {
            ddlYear.Items.Add(Convert.ToString(CurrentYear));
            CurrentYear = CurrentYear - 1;
        }

    }

    private void Initial_Data_Referesh()
    {
        DataTable dt = new DataTable();

        dt.Columns.Add(new DataColumn("MachineID", typeof(string)));
        dt.Columns.Add(new DataColumn("ExistingCode", typeof(string)));
        dt.Columns.Add(new DataColumn("EmpName", typeof(string)));
        dt.Columns.Add(new DataColumn("OTDate_Str", typeof(string)));
        dt.Columns.Add(new DataColumn("OTHrs", typeof(string)));
        dt.Columns.Add(new DataColumn("Dept_Name", typeof(string)));
        dt.Columns.Add(new DataColumn("Designation", typeof(string)));
        dt.Columns.Add(new DataColumn("Wages", typeof(string)));
        dt.Columns.Add(new DataColumn("SPG_Inc", typeof(string)));

        Repeater1.DataSource = dt;
        Repeater1.DataBind();

        ViewState["ItemTable"] = dt;

        //dt = Repeater1.DataSource;
    }
    private void Load_OLD_data()
    {
        DataTable dt = new DataTable();
        dt = (DataTable)ViewState["ItemTable"];
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }

    private void Load_WagesType()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlWages.Items.Clear();
        query = "Select *from MstEmployeeType";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlWages.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpType"] = "-Select-";
        dr["EmpType"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlWages.DataTextField = "EmpType";
        ddlWages.DataValueField = "EmpType";
        ddlWages.DataBind();
    }

    private void Load_Data_EmpDet()
    {
        string query = "";
        DataTable DT = new DataTable();

        //Repeater1.DataSource = DT;
        //Repeater1.DataBind();

        txtMachineID.Items.Clear();
        query = "Select CONVERT(varchar(10), EmpNo) as EmpNo from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        query = query + " And IsActive='Yes' And Wages='" + ddlWages.SelectedItem.Text + "' And Eligible_PF='1'";
        query = query + " And FLOOR((CAST (GetDate() AS INTEGER) - CAST(CONVERT(datetime,BirthDate,103)  AS INTEGER)) / 365.25) > 18";
        DT = objdata.RptEmployeeMultipleDetails(query);
        txtMachineID.DataSource = DT;
        DataRow dr = DT.NewRow();
        dr["EmpNo"] = "-Select-";
        dr["EmpNo"] = "-Select-";
        DT.Rows.InsertAt(dr, 0);
        txtMachineID.DataTextField = "EmpNo";
        txtMachineID.DataValueField = "EmpNo";
        txtMachineID.DataBind();
    }
    protected void ddlWages_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Data_EmpDet();
        Load_OTHrs();
    }
    protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        //Load_Data_EmpDet();
        Load_OTHrs();
    }
    protected void ddlMonth_SelectedIndexChanged(object sender, EventArgs e)
    {
        //Load_Data_EmpDet();
        Load_OTHrs();
    }
    protected void txtTokenNo_TextChanged(object sender, EventArgs e)
    {
        string query;
        DataTable DT = new DataTable();
        txtWeekoff.Text = "";
        lblMonthOTHrs.Text = "0.0";
        txtWeekoff_Day_All.Text = "";
        txtAbsent_Day_All.Text = "";
        txtDay1.Text = ""; txtDay2.Text = ""; txtDay3.Text = ""; txtDay4.Text = ""; txtDay5.Text = "";
        txtDay6.Text = ""; txtDay7.Text = ""; txtDay8.Text = ""; txtDay9.Text = ""; txtDay10.Text = "";

        query = "Select * from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And Eligible_PF='1'";
        query = query + " And IsActive='Yes' And Wages='" + ddlWages.SelectedItem.Text + "' And ExistingCode='" + txtTokenNo.Text + "'";
        query = query + " And FLOOR((CAST (GetDate() AS INTEGER) - CAST(CONVERT(datetime,BirthDate,103)  AS INTEGER)) / 365.25) > 18";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            txtMachineID.SelectedValue = DT.Rows[0]["EmpNo"].ToString();
            txtEmpName.Text = DT.Rows[0]["FirstName"].ToString();
            txtDepartment.Text = DT.Rows[0]["DeptName"].ToString();
            txtDesignation.Text = DT.Rows[0]["Designation"].ToString();
            txtWeekoff.Text = DT.Rows[0]["WeekOff"].ToString();
            Get_Month_OTHrs();
        }
        else
        {
            txtMachineID.SelectedValue = "-Select-";
            txtEmpName.Text = "";
            txtDepartment.Text = "";
            txtDesignation.Text = "";
            txtWeekoff.Text = "";
            //txtTokenNo.Text = "";
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Token No not found...In the Database!');", true);
        }

        
    }

    protected void txtMachineID_SelectedIndexChanged(object sender, EventArgs e)
    {
        string query;
        DataTable DT = new DataTable();
        txtWeekoff.Text = "";
        lblMonthOTHrs.Text = "0.0";
        txtWeekoff_Day_All.Text = "";
        txtAbsent_Day_All.Text = "";
        txtDay1.Text = ""; txtDay2.Text = ""; txtDay3.Text = ""; txtDay4.Text = ""; txtDay5.Text = "";
        txtDay6.Text = ""; txtDay7.Text = ""; txtDay8.Text = ""; txtDay9.Text = ""; txtDay10.Text = "";

        if (txtMachineID.SelectedItem.Text != "-Select-")
        {
            query = "Select * from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            query = query + " And Wages='" + ddlWages.SelectedItem.Text + "' And EmpNo='" + txtMachineID.SelectedItem.Text + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);

            if (DT.Rows.Count != 0)
            {
                txtTokenNo.Text = DT.Rows[0]["ExistingCode"].ToString();
                txtEmpName.Text = DT.Rows[0]["FirstName"].ToString();
                txtDepartment.Text = DT.Rows[0]["DeptName"].ToString();
                txtDesignation.Text = DT.Rows[0]["Designation"].ToString();
                txtWeekoff.Text = DT.Rows[0]["Weekoff"].ToString();
                Get_Month_OTHrs();
            }
            else
            {
                txtMachineID.SelectedValue = "-Select-";
                txtEmpName.Text = "";
                txtDepartment.Text = "";
                txtDesignation.Text = "";
                txtTokenNo.Text = "";
                txtWeekoff.Text = "";
            }
        }
        else
        {
            txtMachineID.SelectedValue = "-Select-";
            txtEmpName.Text = "";
            txtDepartment.Text = "";
            txtDesignation.Text = "";
            txtTokenNo.Text = "";
            txtWeekoff.Text = "";
        }
    }
    protected void txtOTDate_Click(object sender, EventArgs e)
    {
        Load_OTHrs();

        Weekoff_Check();
    }
    protected void Weekoff_Check()
    {
        string query = "";
        DataTable DT = new DataTable();
        bool Errflag = false;
        if (txtMachineID.SelectedItem.Text == "-Select-")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Script", "SaveMsgAlert('Select the MachineID');", true);
            Errflag = true;
        }
        if (txtOTDate.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "Script", "SaveMsgAlert('Select the OTDate');", true);
            Errflag = true;
        }

        if (!Errflag)
        {
            DateTime Fromdate = Convert.ToDateTime(txtOTDate.Text);
            string Curr_Weekoff = Fromdate.DayOfWeek.ToString();
            string Employee_weekoff = "";

            SSQL = "select WeekOff  from Employee_Mst where  CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " and Wages='" + ddlWages.SelectedItem.Text + "' And EmpNo='" + txtMachineID.SelectedItem.Text + "'";
            DT = objdata.RptEmployeeMultipleDetails(SSQL);
            if (DT.Rows.Count != 0)
            {
                Employee_weekoff = DT.Rows[0]["WeekOff"].ToString();
                if (Employee_weekoff == Curr_Weekoff)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('weekoff set the date....');", true);
                }
             
            }
        }
        
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;


        if (ddlWages.SelectedValue == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the Wages...!');", true);
        }
        if (ddlYear.SelectedValue == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the Year...!');", true);
        }
        if (ddlMonth.SelectedValue == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the Month...!');", true);
        }
        if (txtTokenNo.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Token No...!');", true);
        }

        if (txtDay1.Text=="" && txtDay2.Text=="" && txtDay3.Text=="" && txtDay4.Text=="" && txtDay5.Text=="" && txtDay6.Text=="" && txtDay7.Text=="" && txtDay8.Text=="" && txtDay9.Text=="" && txtDay10.Text=="")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the OT Day...!');", true);
        }
        if (Convert.ToDecimal(lblMonthOTHrs.Text.ToString()) == 0)
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Employee not eligible because OT hrs zero...!');", true);
        }

        if (!ErrFlag)
        {
            if (Convert.ToDecimal(lblMonthOTHrs.Text.ToString()) >= 20) 
            {
                if (txtDay1.Text == "" || txtDay2.Text == "" || txtDay3.Text == "" || txtDay4.Text == "" || txtDay5.Text == "" || txtDay6.Text == "" || txtDay7.Text == "" || txtDay8.Text == "" || txtDay9.Text == "" || txtDay10.Text == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the OT Day...!');", true);
                }
            }
            else if (Convert.ToDecimal(lblMonthOTHrs.Text.ToString()) >= 18) 
            {
                if (txtDay1.Text == "" || txtDay2.Text == "" || txtDay3.Text == "" || txtDay4.Text == "" || txtDay5.Text == "" || txtDay6.Text == "" || txtDay7.Text == "" || txtDay8.Text == "" || txtDay9.Text == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the OT Day...!');", true);
                }
            }
            else if (Convert.ToDecimal(lblMonthOTHrs.Text.ToString()) >= 16) 
            {
                if (txtDay1.Text == "" || txtDay2.Text == "" || txtDay3.Text == "" || txtDay4.Text == "" || txtDay5.Text == "" || txtDay6.Text == "" || txtDay7.Text == "" || txtDay8.Text == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the OT Day...!');", true);
                }
            }
            else if (Convert.ToDecimal(lblMonthOTHrs.Text.ToString()) >= 14) 
            {
                if (txtDay1.Text == "" || txtDay2.Text == "" || txtDay3.Text == "" || txtDay4.Text == "" || txtDay5.Text == "" || txtDay6.Text == "" || txtDay7.Text == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the OT Day...!');", true);
                }
            }
            else if (Convert.ToDecimal(lblMonthOTHrs.Text.ToString()) >= 12) 
            {
                if (txtDay1.Text == "" || txtDay2.Text == "" || txtDay3.Text == "" || txtDay4.Text == "" || txtDay5.Text == "" || txtDay6.Text == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the OT Day...!');", true);
                }
            }
            else if (Convert.ToDecimal(lblMonthOTHrs.Text.ToString()) >= 10) 
            {
                if (txtDay1.Text == "" || txtDay2.Text == "" || txtDay3.Text == "" || txtDay4.Text == "" || txtDay5.Text == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the OT Day...!');", true);
                }
            }
            else if (Convert.ToDecimal(lblMonthOTHrs.Text.ToString()) >= 8) 
            {
                if (txtDay1.Text == "" || txtDay2.Text == "" || txtDay3.Text == "" || txtDay4.Text == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the OT Day...!');", true);
                }
            }
            else if (Convert.ToDecimal(lblMonthOTHrs.Text.ToString()) >= 6)
            {
                if (txtDay1.Text == "" || txtDay2.Text == "" || txtDay3.Text == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the OT Day...!');", true);
                }
            }
            else if (Convert.ToDecimal(lblMonthOTHrs.Text.ToString()) >= 4)
            {
                if (txtDay1.Text == "" || txtDay2.Text == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the OT Day...!');", true);
                }
            }
            else if (Convert.ToDecimal(lblMonthOTHrs.Text.ToString()) >= 2)
            {
                if (txtDay1.Text == "")
                {
                    ErrFlag = true;
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the OT Day...!');", true);
                }
            }
            else
            {
                //Skip
            }
        }

        if (!ErrFlag)
        {
            string OT_Date_Final = "";
            string OT_Hrs_Final = "2";
            string Day_Val_Str = "";
            int month = DateTime.ParseExact(ddlMonth.SelectedValue, "MMM", CultureInfo.CurrentCulture).Month;
            string Month_Str = "1";
            if (month.ToString().Length == 1) { Month_Str = "0" + month.ToString(); }
            else { Month_Str = month.ToString(); }

            string query = "";
            DataTable DT_S = new DataTable();
            query = "Select * from W_Monthly_Salary_OT_Main where Wages='" + ddlWages.SelectedValue + "'";
            query = query + " And OT_Year='" + ddlYear.SelectedValue + "' And OT_Month='" + ddlMonth.SelectedValue + "'";
            query = query + " And Token_No='" + txtTokenNo.Text + "' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            DT_S = objdata.RptEmployeeMultipleDetails(query);
            if (DT_S.Rows.Count != 0)
            {
                for (int i = 1; i < 11; i++)
                {
                    string Column_Name = "Day" + i.ToString();
                    string OT_Date1, OT_Date2 = "";
                    Day_Val_Str = DT_S.Rows[0][Column_Name].ToString();
                    if (Day_Val_Str != "")
                    {
                        if (Day_Val_Str.Length == 1) { Day_Val_Str = "0" + Day_Val_Str; }

                        OT_Date1 = Day_Val_Str + "/" + Month_Str + "/" + ddlYear.SelectedValue.ToString();
                        OT_Date2 = ddlYear.SelectedValue.ToString() + "/" + Month_Str + "/" + Day_Val_Str;
                        OLD_OT_Date_Removed(OT_Date1, OT_Date2, txtTokenNo.Text);
                    }

                }

                query = "Delete from W_Monthly_Salary_OT_Main where Wages='" + ddlWages.SelectedValue + "'";
                query = query + " And OT_Year='" + ddlYear.SelectedValue + "' And OT_Month='" + ddlMonth.SelectedValue + "'";
                query = query + " And Token_No='" + txtTokenNo.Text + "' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                objdata.RptEmployeeMultipleDetails(query);
            }
            //Insert Main Table
            //Get OT Date ALL
            string OT_Date_All = "";
            for (int i = 1; i < 11; i++)
            {
                ContentPlaceHolder MainContent1 = Page.Master.FindControl("ContentPlaceHolder1") as ContentPlaceHolder;
                TextBox Day_textbox1 = MainContent1.FindControl("txtDay" + i.ToString()) as TextBox;
                if (Day_textbox1.Text.ToString() != "")
                {
                    if (OT_Date_All == "")
                    {
                        OT_Date_All = Day_textbox1.Text;
                    }
                    else
                    {
                        OT_Date_All = OT_Date_All + ", " + Day_textbox1.Text.ToString();
                    }
                }
            }
            query = "Insert Into W_Monthly_Salary_OT_Main(CompCode,LocCode,Wages,OT_Year,OT_Month,Token_No,MachineID,";
            query = query + " EmpName,DeptName,Designation,WeekOff,Month_Tot_OTHrs,Weekoff_Day_All,Absent_Day_All,Day1,Day2,";
            query = query + " Day3,Day4,Day5,Day6,Day7,Day8,Day9,Day10,OT_Date_All) Values('" + SessionCcode + "','" + SessionLcode + "','" + ddlWages.SelectedValue + "',";
            query = query + " '" + ddlYear.SelectedValue + "','" + ddlMonth.SelectedValue + "','" + txtTokenNo.Text + "','" + txtMachineID.SelectedValue + "','" + txtEmpName.Text + "',";
            query = query + " '" + txtDepartment.Text + "','" + txtDesignation.Text + "','" + txtWeekoff.Text + "','" + lblMonthOTHrs.Text + "','" + txtWeekoff_Day_All.Text + "',";
            query = query + " '" + txtAbsent_Day_All.Text + "','" + txtDay1.Text + "','" + txtDay2.Text + "','" + txtDay3.Text + "','" + txtDay4.Text + "','" + txtDay5.Text + "',";
            query = query + " '" + txtDay6.Text + "','" + txtDay7.Text + "','" + txtDay8.Text + "','" + txtDay9.Text + "','" + txtDay10.Text + "','" + OT_Date_All + "')";
            objdata.RptEmployeeMultipleDetails(query);

            

            for (int i = 1; i < 11; i++)
            {
                ContentPlaceHolder MainContent = Page.Master.FindControl("ContentPlaceHolder1") as ContentPlaceHolder;
                TextBox Day_textbox = MainContent.FindControl("txtDay" + i.ToString()) as TextBox;
                
                if (Day_textbox.Text.ToString() != "")
                {
                    Day_Val_Str = Day_textbox.Text.ToString();
                    if (Day_Val_Str != "")
                    {
                        if (Day_Val_Str.Length == 1) { Day_Val_Str = "0" + Day_Val_Str; }
                        OT_Date_Final = Day_Val_Str + "/" + Month_Str + "/" + ddlYear.SelectedValue.ToString();
                        txtOTDate.Text = OT_Date_Final;
                        txtOTHours.Text = OT_Hrs_Final;
                        Single_date_OTHrs_Saved();
                    }
                }
            }
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Month Salary OT Saved Successfully...!');", true);
            txtTokenNo.Text = "";
            txtMachineID.SelectedValue = "-Select-";
            txtEmpName.Text = ""; txtDepartment.Text = ""; txtDesignation.Text = "";
            txtWeekoff.Text = ""; txtWeekoff_Day_All.Text = ""; lblMonthOTHrs.Text = "0.0";
            txtDay1.Text = ""; txtDay2.Text = ""; txtDay3.Text = ""; txtDay4.Text = ""; txtDay5.Text = "";
            txtDay6.Text = ""; txtDay7.Text = ""; txtDay8.Text = ""; txtDay9.Text = ""; txtDay10.Text = "";
            txtAbsent_Day_All.Text = "";
            Load_OTHrs();
        }
     
    }
    public static string GetRandom(int Min, int Max)
    {
        System.Random Generator = new System.Random();

        return Generator.Next(Min, Max).ToString();


    }

    protected void btnView_Click(object sender, EventArgs e)
    {
        //Load_Data();
    }
    public void Load_OTHrs()
    {
        string query = "";
        DataTable DT = new DataTable();
        if (ddlWages.SelectedItem.Text != "-Select-" && ddlYear.SelectedValue !="-Select-" && ddlMonth.SelectedValue !="-Select-")
        {
            query = "Select * from W_Monthly_Salary_OT_Main where Wages='" + ddlWages.SelectedValue + "'";
            query = query + " And OT_Year='" + ddlYear.SelectedValue + "' And OT_Month='" + ddlMonth.SelectedValue + "'";
            query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            query = query + " Order by Token_No Asc";
            
            DT = objdata.RptEmployeeMultipleDetails(query);

            ViewState["ItemTable"] = DT;
            Repeater1.DataSource = DT;
            Repeater1.DataBind();
        }
        else
        {
            Initial_Data_Referesh();
        }
    }
    private void Clear_All_Field()
    {
        ddlWages.SelectedValue = "-Select-";
        txtMachineID.SelectedValue = "-Select-";
        txtEmpName.Text = "";
        txtDepartment.Text = "";
        txtDesignation.Text = "";
        txtOTDate.Text = ""; txtOTHours.Text = "";
        txtTokenNo.Text = "";

        ddlYear.SelectedValue = "-Select-";
        ddlMonth.SelectedValue = "-Select-";
        txtWeekoff.Text = "";
        lblMonthOTHrs.Text = "0.0";
        txtWeekoff_Day_All.Text = "";
        txtAbsent_Day_All.Text = "";
        txtDay1.Text = ""; txtDay2.Text = ""; txtDay3.Text = ""; txtDay4.Text = ""; txtDay5.Text = "";
        txtDay6.Text = ""; txtDay7.Text = ""; txtDay8.Text = ""; txtDay9.Text = ""; txtDay10.Text = "";
        Day_Column_Disable();

        Initial_Data_Referesh();
        //Load_Data();
        //btnSave.Text = "Save";
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();

    }

    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {
        string Day_Val_Str = "";
        int month = DateTime.ParseExact(ddlMonth.SelectedValue, "MMM", CultureInfo.CurrentCulture).Month;
        string Month_Str = "1";
        if (month.ToString().Length == 1) { Month_Str = "0" + month.ToString(); }
        else { Month_Str = month.ToString(); }

        string query = "";
        DataTable DT_S = new DataTable();
        
        query = "Select * from W_Monthly_Salary_OT_Main where Wages='" + ddlWages.SelectedValue + "'";
        query = query + " And OT_Year='" + ddlYear.SelectedValue + "' And OT_Month='" + ddlMonth.SelectedValue + "'";
        query = query + " And Token_No='" + e.CommandName + "' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        DT_S = objdata.RptEmployeeMultipleDetails(query);
        if (DT_S.Rows.Count != 0)
        {
            for (int i = 1; i < 11; i++)
            {
                string Column_Name = "Day" + i.ToString();
                string OT_Date1, OT_Date2 = "";
                Day_Val_Str = DT_S.Rows[0][Column_Name].ToString();
                if (Day_Val_Str != "")
                {
                    if (Day_Val_Str.Length == 1) { Day_Val_Str = "0" + Day_Val_Str; }
                    OT_Date1 = Day_Val_Str + "/" + Month_Str + "/" + ddlYear.SelectedValue.ToString();
                    OT_Date2 = ddlYear.SelectedValue.ToString() + "/" + Month_Str + "/" + Day_Val_Str;
                    OLD_OT_Date_Removed(OT_Date1, OT_Date2, e.CommandName);
                }

            }

            query = "Delete from W_Monthly_Salary_OT_Main where Wages='" + ddlWages.SelectedValue + "'";
            query = query + " And OT_Year='" + ddlYear.SelectedValue + "' And OT_Month='" + ddlMonth.SelectedValue + "'";
            query = query + " And Token_No='" + e.CommandName + "' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            objdata.RptEmployeeMultipleDetails(query);
        }

        Load_OTHrs();
        Load_OLD_data();
    }

    protected void GridEditEntryClick(object sender, CommandEventArgs e)
    {
        DataTable DT_Check = new DataTable();

        SSQL = "Select * from W_Monthly_Salary_OT_Main Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " And Wages='" + ddlWages.SelectedValue + "' And OT_Year='" + ddlYear.SelectedValue + "' And OT_Month='" + ddlMonth.SelectedValue + "'";
        SSQL = SSQL + " And Token_No='" + e.CommandName.ToString() + "'";
        DT_Check = objdata.RptEmployeeMultipleDetails(SSQL);

        if (DT_Check.Rows.Count != 0)
        {
            txtTokenNo.Text = DT_Check.Rows[0]["Token_No"].ToString();
            txtMachineID.SelectedValue = DT_Check.Rows[0]["MachineID"].ToString();
            txtEmpName.Text = DT_Check.Rows[0]["EmpName"].ToString();
            txtDepartment.Text = DT_Check.Rows[0]["DeptName"].ToString();
            txtDesignation.Text = DT_Check.Rows[0]["Designation"].ToString();
            txtWeekoff.Text = DT_Check.Rows[0]["WeekOff"].ToString();
            lblMonthOTHrs.Text = DT_Check.Rows[0]["Month_Tot_OTHrs"].ToString();
            txtWeekoff_Day_All.Text = DT_Check.Rows[0]["Weekoff_Day_All"].ToString();
            txtAbsent_Day_All.Text = DT_Check.Rows[0]["Absent_Day_All"].ToString();
            Day_Column_Disable();
            txtDay1.Text = DT_Check.Rows[0]["Day1"].ToString();
            txtDay2.Text = DT_Check.Rows[0]["Day2"].ToString();
            txtDay3.Text = DT_Check.Rows[0]["Day3"].ToString();
            txtDay4.Text = DT_Check.Rows[0]["Day4"].ToString();
            txtDay5.Text = DT_Check.Rows[0]["Day5"].ToString();
            txtDay6.Text = DT_Check.Rows[0]["Day6"].ToString();
            txtDay7.Text = DT_Check.Rows[0]["Day7"].ToString();
            txtDay8.Text = DT_Check.Rows[0]["Day8"].ToString();
            txtDay9.Text = DT_Check.Rows[0]["Day9"].ToString();
            txtDay10.Text = DT_Check.Rows[0]["Day10"].ToString();
        }

        //Load_OLD_data();
    }
    protected void txtOTHours_TextChanged(object sender, EventArgs e)
    {
        double OThrs = Convert.ToDouble(txtOTHours.Text);
        double Othrs_Val = 2.0;
        if (Convert.ToDouble(OThrs) > Convert.ToDouble(Othrs_Val))
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('below 2 hours OT Only Accept ..');", true);
        }
    }

    private void Get_Month_OTHrs()
    {
        lblMonthOTHrs.Text = "0.0";
        if (ddlYear.SelectedValue != "-Select-" && ddlMonth.SelectedValue != "-Select-")
        {
            int month = DateTime.ParseExact(ddlMonth.SelectedValue, "MMM", CultureInfo.CurrentCulture).Month;
            string Last_Day = DateTime.DaysInMonth(Convert.ToInt32(ddlYear.SelectedValue), month).ToString();
            string Month_Str = "1";
            if (month.ToString().Length == 1) { Month_Str = "0" + month.ToString(); }
            else { Month_Str = month.ToString(); }
            string FromDate = "01/" + Month_Str + "/" + ddlYear.SelectedValue;
            string ToDate = Last_Day + "/" + Month_Str + "/" + ddlYear.SelectedValue;

            //Get Week off OT Hrs
            string query = "";
            DataTable DT_OT = new DataTable();
            query = "Select ThreeSided from [" + SessionPayroll + "]..AttenanceDetails where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ExistingCode='" + txtTokenNo.Text + "'";
            query = query + " And CONVERT(Datetime,FromDate,103)=CONVERT(Datetime,'" + FromDate + "',103)";
            query = query + " And CONVERT(Datetime,ToDate,103)=CONVERT(Datetime,'" + ToDate + "',103)";
            DT_OT = objdata.RptEmployeeMultipleDetails(query);
            if (DT_OT.Rows.Count != 0)
            {
                lblMonthOTHrs.Text = (Convert.ToDecimal(DT_OT.Rows[0]["ThreeSided"].ToString()) * Convert.ToDecimal(4)).ToString();
            }
            else
            {
                lblMonthOTHrs.Text = "0.0";
            }

            //Get Absent Days All in the Month
            query = "Select Present,Attn_Date,DATENAME(dd, Attn_Date) as Day_V from LogTime_Days where CompCode='" + SessionCcode + "'";
            query = query + " And LocCode='" + SessionLcode + "' And ExistingCode='" + txtTokenNo.Text + "'";
            query = query + " And CONVERT(Datetime,Attn_Date,103) >= CONVERT(Datetime,'" + FromDate + "',103)";
            query = query + " And CONVERT(Datetime,Attn_Date,103) <= CONVERT(Datetime,'" + ToDate + "',103)";
            query = query + " And Present = 0.0 Order by Attn_Date Asc";
            DT_OT = objdata.RptEmployeeMultipleDetails(query);
            if (DT_OT.Rows.Count != 0)
            {
                for (int i = 0; i < DT_OT.Rows.Count; i++)
                {
                    if (i == 0)
                    {
                        txtAbsent_Day_All.Text = DT_OT.Rows[i]["Day_V"].ToString();
                    }
                    else
                    {
                        txtAbsent_Day_All.Text = txtAbsent_Day_All.Text.ToString() + ", " + DT_OT.Rows[i]["Day_V"].ToString();
                    }
                }
            }
            else
            {
                txtAbsent_Day_All.Text = "";
            }
            All_Weekoff_Day_Display(month, Convert.ToInt32(ddlYear.SelectedValue), txtWeekoff.Text);
        }
        Day_Column_Disable();
        
        
    }

    private void Day_Column_Disable()
    {
        
        txtDay1.Enabled = true;
        txtDay2.Enabled = true; txtDay3.Enabled = true; txtDay4.Enabled = true; txtDay5.Enabled = true; txtDay6.Enabled = true; txtDay7.Enabled = true; txtDay8.Enabled = true; txtDay9.Enabled = true; txtDay10.Enabled = true;
        if (Convert.ToDecimal(lblMonthOTHrs.Text.ToString()) >= 20) {  }
        else if (Convert.ToDecimal(lblMonthOTHrs.Text.ToString()) >= 18) { txtDay10.Enabled = false; }
        else if (Convert.ToDecimal(lblMonthOTHrs.Text.ToString()) >= 16) { txtDay9.Enabled = false; txtDay10.Enabled = false; }
        else if (Convert.ToDecimal(lblMonthOTHrs.Text.ToString()) >= 14) { txtDay8.Enabled = false; txtDay9.Enabled = false; txtDay10.Enabled = false; }
        else if (Convert.ToDecimal(lblMonthOTHrs.Text.ToString()) >= 12) { txtDay7.Enabled = false; txtDay8.Enabled = false; txtDay9.Enabled = false; txtDay10.Enabled = false; }
        else if (Convert.ToDecimal(lblMonthOTHrs.Text.ToString()) >= 10) { txtDay6.Enabled = false; txtDay7.Enabled = false; txtDay8.Enabled = false; txtDay9.Enabled = false; txtDay10.Enabled = false; }
        else if (Convert.ToDecimal(lblMonthOTHrs.Text.ToString()) >= 8) { txtDay5.Enabled = false; txtDay6.Enabled = false; txtDay7.Enabled = false; txtDay8.Enabled = false; txtDay9.Enabled = false; txtDay10.Enabled = false; }
        else if (Convert.ToDecimal(lblMonthOTHrs.Text.ToString()) >= 6) { txtDay4.Enabled = false; txtDay5.Enabled = false; txtDay6.Enabled = false; txtDay7.Enabled = false; txtDay8.Enabled = false; txtDay9.Enabled = false; txtDay10.Enabled = false; }
        else if (Convert.ToDecimal(lblMonthOTHrs.Text.ToString()) >= 4) { txtDay3.Enabled = false; txtDay4.Enabled = false; txtDay5.Enabled = false; txtDay6.Enabled = false; txtDay7.Enabled = false; txtDay8.Enabled = false; txtDay9.Enabled = false; txtDay10.Enabled = false; }
        else if (Convert.ToDecimal(lblMonthOTHrs.Text.ToString()) >= 2) { txtDay2.Enabled = false; txtDay3.Enabled = false; txtDay4.Enabled = false; txtDay5.Enabled = false; txtDay6.Enabled = false; txtDay7.Enabled = false; txtDay8.Enabled = false; txtDay9.Enabled = false; txtDay10.Enabled = false; }
        else
        {
            txtDay1.Enabled = false;
            txtDay2.Enabled = false; txtDay3.Enabled = false; txtDay4.Enabled = false; txtDay5.Enabled = false; txtDay6.Enabled = false; txtDay7.Enabled = false; txtDay8.Enabled = false; txtDay9.Enabled = false; txtDay10.Enabled = false;
        }
    }

    public static bool Day_OT_WH_Check(string OT_Date,string Emp_Weekoff)
    {
        if (OT_Date != "")
        {
            //Weekoff Date Check
            DateTime Fromdate = Convert.ToDateTime(OT_Date);
            string Curr_Weekoff = Fromdate.DayOfWeek.ToString();
           
            if (Curr_Weekoff.ToUpper().Trim() == Emp_Weekoff.ToUpper().Trim())
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        else
        {
            return true;
        }
    }
    public bool Present_Day_Checking(string OT_Date)
    {
        string query = "";
        DataTable DT_P = new DataTable();
        query = "Select isnull(Present,0) as Present from LogTime_Days where ExistingCode='" + txtTokenNo.Text + "' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        query = query + " And Convert(Datetime,Attn_Date,103)=Convert(Datetime,'" + OT_Date + "',103)";
        DT_P = objdata.RptEmployeeMultipleDetails(query);
        if (DT_P.Rows.Count != 0)
        {
            if (Convert.ToDecimal(DT_P.Rows[0]["Present"].ToString()) == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        else
        {
            return false;
        }
    }
    protected void txtDay1_TextChanged(object sender, EventArgs e)
    {
        bool Check_OThrs_Val = true;
        bool Present_Day_Blb = true;        
        string OT_Date_Str = txtDay1.Text.ToString() + "/" + DateTime.ParseExact(ddlMonth.SelectedValue, "MMM", CultureInfo.CurrentCulture).Month.ToString() + "/" + ddlYear.SelectedValue.ToString();
        bool Date_Valid_Or_Not = CheckDate(OT_Date_Str);
        if (Date_Valid_Or_Not == false)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('enter the valid date...');", true);
            txtDay1.Text = "";
            txtDay1.Focus();
        }
        else
        {
            Check_OThrs_Val = Day_OT_WH_Check(OT_Date_Str, txtWeekoff.Text);
            Present_Day_Blb = Present_Day_Checking(OT_Date_Str);
            string Date_Value_Str = txtDay1.Text.Trim().ToString();
            bool Duplicate_Check = true;
            Duplicate_Check = Duplicate_Value_Check(Date_Value_Str, txtDay2.Text, txtDay3.Text, txtDay4.Text, txtDay5.Text, txtDay6.Text, txtDay7.Text, txtDay8.Text, txtDay9.Text, txtDay10.Text);
            if (Check_OThrs_Val == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You can not enter week off Date...');", true);
                txtDay1.Text = "";
                txtDay1.Focus();
            }
            else if (Present_Day_Blb == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You can not enter Absent Date...');", true);
                txtDay1.Text = "";
                txtDay1.Focus();
            }
            else if (Duplicate_Check == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Already Enter this Day...');", true);
                txtDay1.Text = "";
                txtDay1.Focus();
            }
            else
            {
                txtDay2.Focus();
            }
        }
    }

    protected void txtDay2_TextChanged(object sender, EventArgs e)
    {
        bool Check_OThrs_Val = true;
        bool Present_Day_Blb = true;
        string OT_Date_Str = txtDay2.Text.ToString() + "/" + DateTime.ParseExact(ddlMonth.SelectedValue, "MMM", CultureInfo.CurrentCulture).Month.ToString() + "/" + ddlYear.SelectedValue.ToString();
        bool Date_Valid_Or_Not = CheckDate(OT_Date_Str);
        if (Date_Valid_Or_Not == false)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('enter the valid date...');", true);
            txtDay2.Text = "";
            txtDay2.Focus();
        }
        else
        {
            Check_OThrs_Val = Day_OT_WH_Check(OT_Date_Str, txtWeekoff.Text);
            Present_Day_Blb = Present_Day_Checking(OT_Date_Str);
            string Date_Value_Str = txtDay2.Text.Trim().ToString();
            bool Duplicate_Check = true;
            Duplicate_Check = Duplicate_Value_Check(Date_Value_Str, txtDay1.Text, txtDay3.Text, txtDay4.Text, txtDay5.Text, txtDay6.Text, txtDay7.Text, txtDay8.Text, txtDay9.Text, txtDay10.Text);

            if (Check_OThrs_Val == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You can not enter week off Date...');", true);
                txtDay2.Text = "";
            }
            else if (Present_Day_Blb == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You can not enter Absent Date...');", true);
                txtDay2.Text = "";
                txtDay2.Focus();
            }
            else if (Duplicate_Check == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Already Enter this Day...');", true);
                txtDay2.Text = "";
                txtDay2.Focus();
            }
            else
            {
                txtDay3.Focus();
            }
        }
    }

    protected void txtDay3_TextChanged(object sender, EventArgs e)
    {
        bool Check_OThrs_Val = true;
        bool Present_Day_Blb = true;
        string OT_Date_Str = txtDay3.Text.ToString() + "/" + DateTime.ParseExact(ddlMonth.SelectedValue, "MMM", CultureInfo.CurrentCulture).Month.ToString() + "/" + ddlYear.SelectedValue.ToString();
        bool Date_Valid_Or_Not = CheckDate(OT_Date_Str);
        if (Date_Valid_Or_Not == false)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('enter the valid date...');", true);
            txtDay3.Text = "";
            txtDay3.Focus();
        }
        else
        {
            Check_OThrs_Val = Day_OT_WH_Check(OT_Date_Str, txtWeekoff.Text);
            Present_Day_Blb = Present_Day_Checking(OT_Date_Str);
            string Date_Value_Str = txtDay3.Text.Trim().ToString();
            bool Duplicate_Check = true;
            Duplicate_Check = Duplicate_Value_Check(Date_Value_Str, txtDay2.Text, txtDay1.Text, txtDay4.Text, txtDay5.Text, txtDay6.Text, txtDay7.Text, txtDay8.Text, txtDay9.Text, txtDay10.Text);

            if (Check_OThrs_Val == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You can not enter week off Date...');", true);
                txtDay3.Text = "";
            }
            else if (Present_Day_Blb == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You can not enter Absent Date...');", true);
                txtDay3.Text = "";
                txtDay3.Focus();
            }
            else if (Duplicate_Check == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Already Enter this Day...');", true);
                txtDay3.Text = "";
                txtDay3.Focus();
            }
            else
            {
                txtDay4.Focus();
            }
        }
    }

    protected void txtDay4_TextChanged(object sender, EventArgs e)
    {
        bool Check_OThrs_Val = true;
        bool Present_Day_Blb = true;
        string OT_Date_Str = txtDay4.Text.ToString() + "/" + DateTime.ParseExact(ddlMonth.SelectedValue, "MMM", CultureInfo.CurrentCulture).Month.ToString() + "/" + ddlYear.SelectedValue.ToString();
        bool Date_Valid_Or_Not = CheckDate(OT_Date_Str);
        if (Date_Valid_Or_Not == false)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('enter the valid date...');", true);
            txtDay4.Text = "";
            txtDay4.Focus();
        }
        else
        {
            Check_OThrs_Val = Day_OT_WH_Check(OT_Date_Str, txtWeekoff.Text);
            Present_Day_Blb = Present_Day_Checking(OT_Date_Str);
            string Date_Value_Str = txtDay4.Text.Trim().ToString();
            bool Duplicate_Check = true;
            Duplicate_Check = Duplicate_Value_Check(Date_Value_Str, txtDay2.Text, txtDay3.Text, txtDay1.Text, txtDay5.Text, txtDay6.Text, txtDay7.Text, txtDay8.Text, txtDay9.Text, txtDay10.Text);

            if (Check_OThrs_Val == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You can not enter week off Date...');", true);
                txtDay4.Text = "";
            }
            else if (Present_Day_Blb == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You can not enter Absent Date...');", true);
                txtDay4.Text = "";
                txtDay4.Focus();
            }
            else if (Duplicate_Check == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Already Enter this Day...');", true);
                txtDay4.Text = "";
                txtDay4.Focus();
            }
            else
            {
                txtDay5.Focus();
            }
        }
    }

    protected void txtDay5_TextChanged(object sender, EventArgs e)
    {
        bool Check_OThrs_Val = true;
        bool Present_Day_Blb = true;
        string OT_Date_Str = txtDay5.Text.ToString() + "/" + DateTime.ParseExact(ddlMonth.SelectedValue, "MMM", CultureInfo.CurrentCulture).Month.ToString() + "/" + ddlYear.SelectedValue.ToString();
        bool Date_Valid_Or_Not = CheckDate(OT_Date_Str);
        if (Date_Valid_Or_Not == false)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('enter the valid date...');", true);
            txtDay5.Text = "";
            txtDay5.Focus();
        }
        else
        {
            Check_OThrs_Val = Day_OT_WH_Check(OT_Date_Str, txtWeekoff.Text);
            Present_Day_Blb = Present_Day_Checking(OT_Date_Str);
            string Date_Value_Str = txtDay5.Text.Trim().ToString();
            bool Duplicate_Check = true;
            Duplicate_Check = Duplicate_Value_Check(Date_Value_Str, txtDay2.Text, txtDay3.Text, txtDay4.Text, txtDay1.Text, txtDay6.Text, txtDay7.Text, txtDay8.Text, txtDay9.Text, txtDay10.Text);

            if (Check_OThrs_Val == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You can not enter week off Date...');", true);
                txtDay5.Text = "";
            }
            else if (Present_Day_Blb == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You can not enter Absent Date...');", true);
                txtDay5.Text = "";
                txtDay5.Focus();
            }
            else if (Duplicate_Check == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Already Enter this Day...');", true);
                txtDay5.Text = "";
                txtDay5.Focus();
            }
            else
            {
                txtDay6.Focus();
            }
        }
    }

    protected void txtDay6_TextChanged(object sender, EventArgs e)
    {
        bool Check_OThrs_Val = true;
        bool Present_Day_Blb = true;
        string OT_Date_Str = txtDay6.Text.ToString() + "/" + DateTime.ParseExact(ddlMonth.SelectedValue, "MMM", CultureInfo.CurrentCulture).Month.ToString() + "/" + ddlYear.SelectedValue.ToString();
        bool Date_Valid_Or_Not = CheckDate(OT_Date_Str);
        if (Date_Valid_Or_Not == false)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('enter the valid date...');", true);
            txtDay6.Text = "";
            txtDay6.Focus();
        }
        else
        {
            Check_OThrs_Val = Day_OT_WH_Check(OT_Date_Str, txtWeekoff.Text);
            Present_Day_Blb = Present_Day_Checking(OT_Date_Str);
            string Date_Value_Str = txtDay6.Text.Trim().ToString();
            bool Duplicate_Check = true;
            Duplicate_Check = Duplicate_Value_Check(Date_Value_Str, txtDay2.Text, txtDay3.Text, txtDay4.Text, txtDay5.Text, txtDay1.Text, txtDay7.Text, txtDay8.Text, txtDay9.Text, txtDay10.Text);

            if (Check_OThrs_Val == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You can not enter week off Date...');", true);
                txtDay6.Text = "";
            }
            else if (Present_Day_Blb == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You can not enter Absent Date...');", true);
                txtDay6.Text = "";
                txtDay6.Focus();
            }
            else if (Duplicate_Check == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Already Enter this Day...');", true);
                txtDay6.Text = "";
                txtDay6.Focus();
            }
            else
            {
                txtDay7.Focus();
            }
        }
    }

    protected void txtDay7_TextChanged(object sender, EventArgs e)
    {
        bool Check_OThrs_Val = true;
        bool Present_Day_Blb = true;
        string OT_Date_Str = txtDay7.Text.ToString() + "/" + DateTime.ParseExact(ddlMonth.SelectedValue, "MMM", CultureInfo.CurrentCulture).Month.ToString() + "/" + ddlYear.SelectedValue.ToString();
        bool Date_Valid_Or_Not = CheckDate(OT_Date_Str);
        if (Date_Valid_Or_Not == false)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('enter the valid date...');", true);
            txtDay7.Text = "";
            txtDay7.Focus();
        }
        else
        {
            Check_OThrs_Val = Day_OT_WH_Check(OT_Date_Str, txtWeekoff.Text);
            Present_Day_Blb = Present_Day_Checking(OT_Date_Str);
            string Date_Value_Str = txtDay7.Text.Trim().ToString();
            bool Duplicate_Check = true;
            Duplicate_Check = Duplicate_Value_Check(Date_Value_Str, txtDay2.Text, txtDay3.Text, txtDay4.Text, txtDay5.Text, txtDay6.Text, txtDay1.Text, txtDay8.Text, txtDay9.Text, txtDay10.Text);

            if (Check_OThrs_Val == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You can not enter week off Date...');", true);
                txtDay7.Text = "";
            }
            else if (Present_Day_Blb == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You can not enter Absent Date...');", true);
                txtDay7.Text = "";
                txtDay7.Focus();
            }
            else if (Duplicate_Check == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Already Enter this Day...');", true);
                txtDay7.Text = "";
                txtDay7.Focus();
            }
            else
            {
                txtDay8.Focus();
            }
        }
    }

    protected void txtDay8_TextChanged(object sender, EventArgs e)
    {
        bool Check_OThrs_Val = true;
        bool Present_Day_Blb = true;
        string OT_Date_Str = txtDay8.Text.ToString() + "/" + DateTime.ParseExact(ddlMonth.SelectedValue, "MMM", CultureInfo.CurrentCulture).Month.ToString() + "/" + ddlYear.SelectedValue.ToString();
        bool Date_Valid_Or_Not = CheckDate(OT_Date_Str);
        if (Date_Valid_Or_Not == false)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('enter the valid date...');", true);
            txtDay8.Text = "";
            txtDay8.Focus();
        }
        else
        {
            Check_OThrs_Val = Day_OT_WH_Check(OT_Date_Str, txtWeekoff.Text);
            Present_Day_Blb = Present_Day_Checking(OT_Date_Str);
            string Date_Value_Str = txtDay8.Text.Trim().ToString();
            bool Duplicate_Check = true;
            Duplicate_Check = Duplicate_Value_Check(Date_Value_Str, txtDay2.Text, txtDay3.Text, txtDay4.Text, txtDay5.Text, txtDay6.Text, txtDay7.Text, txtDay1.Text, txtDay9.Text, txtDay10.Text);

            if (Check_OThrs_Val == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You can not enter week off Date...');", true);
                txtDay8.Text = "";
            }
            else if (Present_Day_Blb == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You can not enter Absent Date...');", true);
                txtDay8.Text = "";
                txtDay8.Focus();
            }
            else if (Duplicate_Check == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Already Enter this Day...');", true);
                txtDay8.Text = "";
                txtDay8.Focus();
            }
            else
            {
                txtDay9.Focus();
            }
        }
    }

    protected void txtDay9_TextChanged(object sender, EventArgs e)
    {
        bool Check_OThrs_Val = true;
        bool Present_Day_Blb = true;
        string OT_Date_Str = txtDay9.Text.ToString() + "/" + DateTime.ParseExact(ddlMonth.SelectedValue, "MMM", CultureInfo.CurrentCulture).Month.ToString() + "/" + ddlYear.SelectedValue.ToString();
        bool Date_Valid_Or_Not = CheckDate(OT_Date_Str);
        if (Date_Valid_Or_Not == false)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('enter the valid date...');", true);
            txtDay9.Text = "";
            txtDay9.Focus();
        }
        else
        {
            Check_OThrs_Val = Day_OT_WH_Check(OT_Date_Str, txtWeekoff.Text);
            Present_Day_Blb = Present_Day_Checking(OT_Date_Str);
            string Date_Value_Str = txtDay9.Text.Trim().ToString();
            bool Duplicate_Check = true;
            Duplicate_Check = Duplicate_Value_Check(Date_Value_Str, txtDay2.Text, txtDay3.Text, txtDay4.Text, txtDay5.Text, txtDay6.Text, txtDay7.Text, txtDay8.Text, txtDay1.Text, txtDay10.Text);

            if (Check_OThrs_Val == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You can not enter week off Date...');", true);
                txtDay9.Text = "";
            }
            else if (Present_Day_Blb == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You can not enter Absent Date...');", true);
                txtDay9.Text = "";
                txtDay9.Focus();
            }
            else if (Duplicate_Check == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Already Enter this Day...');", true);
                txtDay9.Text = "";
                txtDay9.Focus();
            }
            else
            {
                txtDay10.Focus();
            }
        }
    }

    protected void txtDay10_TextChanged(object sender, EventArgs e)
    {
        bool Check_OThrs_Val = true;
        bool Present_Day_Blb = true;
        string OT_Date_Str = txtDay10.Text.ToString() + "/" + DateTime.ParseExact(ddlMonth.SelectedValue, "MMM", CultureInfo.CurrentCulture).Month.ToString() + "/" + ddlYear.SelectedValue.ToString();
        bool Date_Valid_Or_Not = CheckDate(OT_Date_Str);
        if (Date_Valid_Or_Not == false)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('enter the valid date...');", true);
            txtDay10.Text = "";
            txtDay10.Focus();
        }
        else
        {
            Check_OThrs_Val = Day_OT_WH_Check(OT_Date_Str, txtWeekoff.Text);
            Present_Day_Blb = Present_Day_Checking(OT_Date_Str);
            string Date_Value_Str = txtDay10.Text.Trim().ToString();
            bool Duplicate_Check = true;
            Duplicate_Check = Duplicate_Value_Check(Date_Value_Str, txtDay2.Text, txtDay3.Text, txtDay4.Text, txtDay5.Text, txtDay6.Text, txtDay7.Text, txtDay8.Text, txtDay9.Text, txtDay1.Text);

            if (Check_OThrs_Val == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You can not enter week off Date...');", true);
                txtDay10.Text = "";
            }
            else if (Present_Day_Blb == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('You can not enter Absent Date...');", true);
                txtDay10.Text = "";
                txtDay10.Focus();
            }
            else if (Duplicate_Check == false)
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Already Enter this Day...');", true);
                txtDay10.Text = "";
                txtDay10.Focus();
            }
            else
            {
                //
            }
        }
    }

    protected bool CheckDate(String date)
    {
        try
        {
            DateTime dt = DateTime.Parse(date);
            return true;
        }
        catch
        {
            return false;
        }
    }

    public bool Duplicate_Value_Check(string Checking_Val, string Val1, string Val2, string Val3, string Val4, string Val5, string Val6, string Val7, string Val8, string Val9)
    {
        if (Checking_Val != "")
        {
            if (Checking_Val == Val1 || Checking_Val == Val2 || Checking_Val == Val3 || Checking_Val == Val4 || Checking_Val == Val5 || Checking_Val == Val6 || Checking_Val == Val7 || Checking_Val == Val8 || Checking_Val == Val9)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        else
        {
            return true;
        }
    }

    private void All_Weekoff_Day_Display(int Month_Str,int Year_Str,string Emp_Weekoff)
    {
        int year = Year_Str;
        int month = Month_Str;
        string day = Emp_Weekoff;
        System.Globalization.CultureInfo currentCulture = Thread.CurrentThread.CurrentCulture;
        string Week_off_Day_Display = "";
        for (int i = 1; i <= currentCulture.Calendar.GetDaysInMonth(year, month); i++)
        {
            DateTime d = new DateTime(year, month, i);
            string Curr_Weekoff = d.DayOfWeek.ToString();
            if (Curr_Weekoff == day)
            {
                if (Week_off_Day_Display=="")
                {
                    Week_off_Day_Display=d.Day.ToString();
                }
                else
                {
                    Week_off_Day_Display=Week_off_Day_Display + ", " + d.Day.ToString();
                }
            }
        }
        txtWeekoff_Day_All.Text = Week_off_Day_Display;
    }

    private void Single_date_OTHrs_Saved()
    {
        bool ErrFlag = false;
        bool Errflag_Adolescent = false;
        DataTable DT = new DataTable();

        string[] sp_Time_IN;
        string[] sp_time_OUT;
        string[] sp_hr;
        string[] sp_Hr_Out;
        TimeSpan ts1;
        string Final_TimeIN = "";
        string Final_TimeOUT = "";
        string Total_Hr = "";
        DataTable da_IFatt = new DataTable();
        DataTable da_Employee = new DataTable();
        string catName = "";
        string SubCatName = "";
        string Birthday = "";
        DataTable insert = new DataTable();
        DataTable Delete = new DataTable();
        DataTable da_DOB = new DataTable();
        string Shift = "";
        int Age;
        int Ado_Age = 18;


        if (txtOTHours.Text == "" || txtOTHours.Text == "0")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the OT Hours...!');", true);
        }

        if (!ErrFlag)
        {



            string datestr1 = Convert.ToDateTime(txtOTDate.Text).ToString("yyyy/MM/dd");
            //Logdays checking insert or Not
            SSQL = "select * from LogTime_Days where MachineID='" + txtMachineID.SelectedItem.Text + "' and Attn_Date_Str='" + txtOTDate.Text + "' and CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' and Present_Absent !='Leave' ";
            da_IFatt = objdata.RptEmployeeMultipleDetails(SSQL);
            if (da_IFatt.Rows.Count != 0)
            {
                //Employee Details Get
                SSQL = "select CatName,SubCatName,BirthDate from Employee_Mst where MachineID='" + txtMachineID.SelectedItem.Text + "' and CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' and IsActive='Yes'";
                da_Employee = objdata.RptEmployeeMultipleDetails(SSQL);
                if (da_Employee.Rows.Count != 0)
                {
                    catName = da_Employee.Rows[0]["CatName"].ToString();
                    SubCatName = da_Employee.Rows[0]["SubCatName"].ToString();
                    Birthday = da_Employee.Rows[0]["BirthDate"].ToString();
                }
                Shift = da_IFatt.Rows[0]["Shift"].ToString();

                SSQL = "SELECT FLOOR((CAST (GetDate() AS INTEGER) - CAST(CONVERT(datetime,'" + Birthday + "',103)  AS INTEGER)) / 365.25) AS Age";
                da_DOB = objdata.RptEmployeeMultipleDetails(SSQL);
                if (da_DOB.Rows.Count != 0)
                {
                    Age = Convert.ToInt32(da_DOB.Rows[0][0].ToString());

                    if (Age < 18)
                    {
                        Errflag_Adolescent = true;
                    }
                }

                decimal Employee_Punch_Time = Convert.ToDecimal(da_IFatt.Rows[0]["Total_Hrs"]);// Employee Total Hours
                decimal Defult_Time = Convert.ToDecimal(08.00);// check 9 hours working 
                DateTime TimeIN = Convert.ToDateTime(da_IFatt.Rows[0]["TimeIN"]);

                
                if (Employee_Punch_Time >= Defult_Time)
                {
                    long Random_No_Fixed = 1;
                    string GetRndpm = GetRandom(Convert.ToInt32(Random_No_Fixed), 15);
                    Random_No_Get = Convert.ToInt64(GetRndpm);


                    if (Errflag_Adolescent == true) //Adolescent checking 
                    {
                        string TimeIN_AD = "08" + ":" + Random_No_Get + " AM";
                        TimeIN = Convert.ToDateTime(TimeIN_AD);
                        Shift = "SHIFT1";
                    }

                    double Final_OT_Hrs = Convert.ToDouble(txtOTHours.Text) + Convert.ToDouble(Defult_Time);
                    DateTime date3 = Convert.ToDateTime(TimeIN);
                    DateTime date4 = System.Convert.ToDateTime(TimeIN).AddHours(Final_OT_Hrs);
                    DateTime Date5 = date4.AddMinutes(Random_No_Get);
                    ts1 = Date5.Subtract(date3);

                    //Time IN Split 
                    string TimeIN_Val = date3.ToString("hh:mm tt");
                    sp_Time_IN = TimeIN_Val.Split(' ');
                    string Hr_Val = sp_Time_IN[0];
                    sp_hr = Hr_Val.Split(':');
                    Final_TimeIN = sp_hr[0] + ":" + sp_hr[1] + " " + sp_Time_IN[1];
                    //string chck="";
                    //chck = date3.ToString("hh:mm tt");
                    //txtAbsent_Day_All.Text = "TimeIN_Val : " + chck;


                    //Time OUT Split
                    //string TimeOUT_Val = Convert.ToString(Date5);
                    string TimeOUT_Val = Date5.ToString("hh:mm tt");
                    sp_time_OUT = TimeOUT_Val.Split(' ');
                    string hr_Val_Ot = sp_time_OUT[0];
                    sp_Hr_Out = hr_Val_Ot.Split(':');
                    Final_TimeOUT = sp_Hr_Out[0] + ":" + sp_Hr_Out[1] + " " + sp_time_OUT[1];

                    //Total Hrs
                    Total_Hr = ts1.Hours + ":" + ts1.Minutes;

                    //delete the Weekly_OTHours
                    SSQL = "delete from Weekly_OTHours_IF where MachineID='" + txtMachineID.SelectedItem.Text + "' and OTDate_Str='" + txtOTDate.Text + "' and CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' ";
                    Delete = objdata.RptEmployeeMultipleDetails(SSQL);

                    SSQL = "delete from IFReport_DayAttendance where MachineID='" + txtMachineID.SelectedItem.Text + "' and AttendanceDate='" + datestr1 + "' and CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' ";
                    Delete = objdata.RptEmployeeMultipleDetails(SSQL);

                    SSQL = "Insert Into Weekly_OTHours_IF (CompCode,LocCode,Wages,MachineID,ExistingCode,EmpName,OTDate_Str,OTDate,OTHrs,Created_Date,Created_By,Dept_Name,Designation)";
                    SSQL = SSQL + " Values('" + SessionCcode + "','" + SessionLcode + "','" + ddlWages.SelectedItem.Text + "','" + txtMachineID.Text + "','" + txtTokenNo.Text + "',";
                    SSQL = SSQL + "'" + txtEmpName.Text + "','" + txtOTDate.Text + "',convert(datetime,'" + txtOTDate.Text + "',103),'" + txtOTHours.Text + "',";
                    SSQL = SSQL + "GetDate(),'" + SessionUserName + "','" + txtDepartment.Text + "','" + txtDesignation.Text + "')";
                    insert = objdata.RptEmployeeMultipleDetails(SSQL);

                    SSQL = "insert into IFReport_DayAttendance(CompCode,LocCode,MachineID,ExistingCode,FirstName,DeptName,Shift,TimeIN,TimeOUT,Category,subcategory,TotalMIN,GrandTOT,AttendanceDate,PrepBy,TypeName) ";
                    SSQL = SSQL + "values('" + SessionCcode + "','" + SessionLcode + "','" + txtMachineID.Text + "','" + txtTokenNo.Text + "','" + txtEmpName.Text + "','" + txtDepartment.Text + "','" + Shift + "','" + Final_TimeIN + "','" + Final_TimeOUT + "', ";
                    SSQL = SSQL + " '" + catName + "','" + SubCatName + "','" + Total_Hr + "','" + Total_Hr + "','" + datestr1 + "','" + SessionUserID + "','" + ddlWages.SelectedItem.Text + "')";
                    insert = objdata.RptEmployeeMultipleDetails(SSQL);

                }
                else
                {

                    long Random_No_Fixed = 1;
                    string GetRndpm = GetRandom(Convert.ToInt32(Random_No_Fixed), 15);
                    Random_No_Get = Convert.ToInt64(GetRndpm);


                    if (Errflag_Adolescent == true) //Adolescent checking 
                    {
                        string TimeIN_AD = "08" + ":" + Random_No_Get + " AM";
                        TimeIN = Convert.ToDateTime(TimeIN_AD);
                        Shift = "SHIFT1";
                    }
                    double Final_OT_Hrs = Convert.ToDouble(txtOTHours.Text) + Convert.ToDouble(Defult_Time);
                    DateTime date3 = Convert.ToDateTime(TimeIN);
                    DateTime date4 = System.Convert.ToDateTime(TimeIN).AddHours(Final_OT_Hrs);
                    DateTime Date5 = date4.AddMinutes(Random_No_Get);
                    ts1 = Date5.Subtract(date3);

                    //Time IN Split 
                    //string TimeIN_Val = Convert.ToString(date3);
                    string TimeIN_Val = date3.ToString("hh:mm tt");
                    sp_Time_IN = TimeIN_Val.Split(' ');
                    string Hr_Val = sp_Time_IN[0];
                    sp_hr = Hr_Val.Split(':');
                    Final_TimeIN = sp_hr[0] + ":" + sp_hr[1] + " " + sp_Time_IN[1];

                    //Time OUT Split
                    //string TimeOUT_Val = Convert.ToString(Date5);
                    string TimeOUT_Val = Date5.ToString("hh:mm tt");
                    sp_time_OUT = TimeOUT_Val.Split(' ');
                    string hr_Val_Ot = sp_time_OUT[0];
                    sp_Hr_Out = hr_Val_Ot.Split(':');
                    Final_TimeOUT = sp_Hr_Out[0] + ":" + sp_Hr_Out[1] + " " + sp_time_OUT[1];

                    //Total Hrs
                    Total_Hr = ts1.Hours + ":" + ts1.Minutes;

                    //delete the Weekly_OTHours
                    SSQL = "delete from Weekly_OTHours_IF where MachineID='" + txtMachineID.SelectedItem.Text + "' and OTDate_Str='" + txtOTDate.Text + "' and CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' ";
                    Delete = objdata.RptEmployeeMultipleDetails(SSQL);

                    SSQL = "delete from IFReport_DayAttendance where MachineID='" + txtMachineID.SelectedItem.Text + "' and AttendanceDate='" + datestr1 + "' and CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' ";
                    Delete = objdata.RptEmployeeMultipleDetails(SSQL);

                    SSQL = "Insert Into Weekly_OTHours_IF (CompCode,LocCode,Wages,MachineID,ExistingCode,EmpName,OTDate_Str,OTDate,OTHrs,Created_Date,Created_By,Dept_Name,Designation)";
                    SSQL = SSQL + " Values('" + SessionCcode + "','" + SessionLcode + "','" + ddlWages.SelectedItem.Text + "','" + txtMachineID.Text + "','" + txtTokenNo.Text + "',";
                    SSQL = SSQL + "'" + txtEmpName.Text + "','" + txtOTDate.Text + "',convert(datetime,'" + txtOTDate.Text + "',103),'" + txtOTHours.Text + "',";
                    SSQL = SSQL + "GetDate(),'" + SessionUserName + "','" + txtDepartment.Text + "','" + txtDesignation.Text + "')";
                    insert = objdata.RptEmployeeMultipleDetails(SSQL);

                    SSQL = "insert into IFReport_DayAttendance(CompCode,LocCode,MachineID,ExistingCode,FirstName,DeptName,Shift,TimeIN,TimeOUT,Category,subcategory,TotalMIN,GrandTOT,AttendanceDate,PrepBy,TypeName) ";
                    SSQL = SSQL + "values('" + SessionCcode + "','" + SessionLcode + "','" + txtMachineID.Text + "','" + txtTokenNo.Text + "','" + txtEmpName.Text + "','" + txtDepartment.Text + "','" + Shift + "','" + Final_TimeIN + "','" + Final_TimeOUT + "', ";
                    SSQL = SSQL + " '" + catName + "','" + SubCatName + "','" + Total_Hr + "','" + Total_Hr + "','" + datestr1 + "','" + SessionUserID + "','" + ddlWages.SelectedItem.Text + "')";
                    insert = objdata.RptEmployeeMultipleDetails(SSQL);
                }

            }
            else
            {
                //ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Attendance Data...!');", true);
            }
            //if (!ErrFlag)
            //{
            //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Saved...!');", true);
            //    Clear_All_Field();
            //}
        }
    }

    private void OLD_OT_Date_Removed(string OT_Date, string OT_Date_Format_Change, string Token_No)
    {
        string query = "";
        query = "";
        //delete the Weekly_OTHours
        SSQL = "delete from Weekly_OTHours_IF where ExistingCode='" + Token_No + "' and Convert(Datetime,OTDate,103)=Convert(Datetime,'" + OT_Date + "',103) and CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' ";
        objdata.RptEmployeeMultipleDetails(SSQL);

        SSQL = "delete from IFReport_DayAttendance where ExistingCode='" + Token_No + "' and AttendanceDate='" + OT_Date_Format_Change + "' and CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' ";
        objdata.RptEmployeeMultipleDetails(SSQL);
    }
}
