﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class MsthindiCat : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights; string SessionAdmin;
    string SSQL;
    bool ErrFlag = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();

        if (!IsPostBack)
        {
            Load_ID();
        }
        Load_OlD();
        txtCatCode.ReadOnly = true;
    }

    private void Load_ID()
    {
        Int32 ID = 0; 
        SSQL = "";
        SSQL = "Select Max(CatCode) from MstHindiCat where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        DataTable ID_dt = new DataTable();
        ID_dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (ID_dt.Rows.Count > 0)
        {
            if (ID_dt.Rows[0][0] == null|| ID_dt.Rows[0][0].ToString()==string.Empty)
            {
                ID = 1;
            }
            else
            {
                ID = 1 + Convert.ToInt32(ID_dt.Rows[0][0].ToString());
            }
        }
        else
        {
            ID = 1;
        }
        txtCatCode.Text = (ID).ToString();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (txtCatCode.Text == "" || txtCatName.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please Check the Fields....');", true);
        }
        if (!ErrFlag)
        {
            if (btnSave.Text == "Update")
            {
                SSQL = "";
                SSQL = "Delete From MstHindiCat where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and CatCode='"+txtCatCode.Text+"'";
                objdata.RptEmployeeMultipleDetails(SSQL);
                btnSave.Text = "Save";
            }
            if (btnSave.Text == "Save")
            {
                SSQL = "";
                SSQL = "Select * from MstHindiCat where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and CatName='" + txtCatName.Text.ToUpper() + "'";
                DataTable chk = new DataTable();
                chk = objdata.RptEmployeeMultipleDetails(SSQL);
                if (chk.Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Category Alredy Inserted!!!');", true);
                    btnclr_Click(sender, e);
                }
                else
                {
                    SSQL = "";
                    SSQL = "Insert Into MstHindiCat values('" + SessionCcode + "','" + SessionLcode + "','" + txtCatCode.Text + "','" + txtCatName.Text.ToUpper() + "')";
                    objdata.RptEmployeeMultipleDetails(SSQL);
                    Load_OlD();
                    btnclr_Click(sender, e);
                }
            }
            Load_ID();
        }
    }

    private void Load_OlD()
    {
        SSQL = "";
        SSQL = "Select * from MstHindiCat where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        Repeater1.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater1.DataBind();
    }

    protected void btnclr_Click(object sender, EventArgs e)
    {
        txtCatCode.Text = "";
        txtCatName.Text = "";
    }

    protected void btnApprvEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {
        SSQL = "";
        SSQL = "Select * from MstHindiCat where CompCode='"+SessionCcode+"' and LocCode='"+SessionLcode+"' and CatCode='"+e.CommandName.ToString()+"'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count > 0)
        {
            txtCatCode.Text = dt.Rows[0]["CatCode"].ToString();
            txtCatName.Text = dt.Rows[0]["CatName"].ToString();
            btnSave.Text = "Update";
        }
    }

    protected void btnCancelEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {
        SSQL = "";
        SSQL = "Delete from MstHindiCat where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and CatCode='" + e.CommandName.ToString() + "'";
        objdata.RptEmployeeMultipleDetails(SSQL);
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Details Deleted Succesfully!!!');", true);
        Load_OlD();
        Load_ID();
    }
}