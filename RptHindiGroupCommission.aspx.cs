﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;

using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Default2 : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string Stafflabour;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string SessionUserName;
    string SessionUserID;
    string SessionRights;
    string SessionPayroll;
    string fmDate = "";
    string Todate = "";
    string query = "";
    string GroupName = "";
    string PerDay = "";
    bool ErrFlag = false;
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;

    ReportDocument report = new ReportDocument();


    string SSQL = "";

    DataTable AutoDTable = new DataTable();
    string Shift = "";

    DateTime date1;
    DateTime Date2 = new DateTime();


    System.Web.UI.WebControls.DataGrid grid1 =
                           new System.Web.UI.WebControls.DataGrid();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        if (!IsPostBack)
        {
            LoadGroup();

        }
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
    public void Report()
    {
        string SSQL = "";
        SSQL = "Select * from HindiGroupCommission";
        DataTable EmpDt = new DataTable();
        EmpDt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (EmpDt.Rows.Count > 0)
        {
            AutoDTable.Columns.Add("GroupName");
            AutoDTable.Columns.Add("TotalDays");
            AutoDTable.Columns.Add("PerDay");
            AutoDTable.Columns.Add("GrandTotal");

            for (int EmpRw = 0; EmpRw < EmpDt.Rows.Count; EmpRw++)
            {


                AutoDTable.NewRow();
                AutoDTable.Rows.Add();

                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["GroupName"] = EmpDt.Rows[EmpRw]["GroupName"];
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["TotalDays"] = EmpDt.Rows[EmpRw]["TotalDays"];
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["PerDay"] = EmpDt.Rows[EmpRw]["PerDay"];
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["GrandTotal"] = EmpDt.Rows[EmpRw]["GrandTotal"];

            }
        }
    }
    private void LoadGroup()
    {

        {

            SSQL = "Select * from MstHindiCat where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
            ddl_GroupName.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
            ddl_GroupName.DataTextField = "CatName";
            ddl_GroupName.DataValueField = "CatCode";
            ddl_GroupName.DataBind();
            ddl_GroupName.Items.Insert(0, new ListItem("ALL", "0"));
        }


    }
    protected void ddl_GroupName_SelectedIndexChanged(object sender, EventArgs e)
    {
        {
            if (ddl_GroupName.SelectedItem.Text != "Select")
            {
                SSQL = "";
                SSQL = "Select * from MstCommissionHindi where GroupCD='" + ddl_GroupName.SelectedItem.Value + "'and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                DataTable dt = new DataTable();

                dt = objdata.RptEmployeeMultipleDetails(SSQL);

            }
        }
    }

    protected void btnAttnReport_Click(object sender, EventArgs e)
    {

        if (txtFromDate.Text == "" || txtToDate.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "SaveMsgAlert('Please Choose From date and To date Correctly')", true);
            return;
        }
        else
        {
            ResponseHelper.Redirect("RptHindi.aspx?FromDate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&GroupName=" + ddl_GroupName.SelectedItem.Text + "", "_blank", "");
        }

        //try
        //{
        //    string GroupName = "";
        //    decimal TotalDays = 0;
        //    decimal PerDay = 0;
        //    decimal GrandTotal = 0;

        //    bool ErrFlag = false;



        //    if (ddl_GroupName.SelectedItem.Text == "Select")
        //    {
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Group Name');", true);
        //        ErrFlag = true;
        //    }

        //    if (!ErrFlag)
        //    {
        //        if (!ErrFlag)
        //        {
        //            SSQL = "Select GroupName,PerDay,TotalDays from HindiGroupCommission";
        //            DataTable Group_DT = new DataTable();

        //            Group_DT = objdata.RptEmployeeMultipleDetails(SSQL);
        //            if (Group_DT.Rows.Count > 0)
        //            {
        //                if (Group_DT.Rows[0]["GroupName"].ToString() != "")
        //                {
        //                    GroupName = (Group_DT.Rows[0]["GroupName"].ToString());
        //                }
        //                else
        //                {
        //                    GroupName = "";
        //                }
        //            }
        //            else
        //            {
        //                GroupName = "";
        //            }
        //            if (Group_DT.Rows.Count > 0)
        //            {
        //                if (Group_DT.Rows[0]["PerDay"].ToString() != "")
        //                {
        //                    PerDay = Convert.ToDecimal(Group_DT.Rows[0]["PerDay"].ToString());
        //                }
        //                else
        //                {
        //                    PerDay = 0;
        //                }
        //            }
        //            else
        //            {
        //                PerDay = 0;
        //            }

        //            if (Group_DT.Rows.Count > 0)
        //            {
        //                TotalDays = Convert.ToDecimal(Group_DT.Rows[0]["TotalDays"]);

        //            }
        //        }
        //        if (PerDay > 0)
        //        {

        //            GrandTotal = Convert.ToDecimal(PerDay * TotalDays);
        //            GrandTotal = Math.Round(Convert.ToDecimal(GrandTotal), 2, MidpointRounding.AwayFromZero);

        //        }



        //    }
        //    // Report();




        //    //if (AutoDTable.Rows.Count > 0)
        //    //    {
        //    //         grid.DataSource = AutoDTable;
        //    //         grid.DataBind();

        //    //        string attachment = "attachment;filename=Hindi Group Commission Report.xls";
        //    //        Response.ClearContent();
        //    //        Response.AddHeader("content-disposition", attachment);
        //    //        Response.ContentType = "application/ms-excel";
        //    //        grid.HeaderStyle.Font.Bold = true;
        //    //        System.IO.StringWriter stw = new System.IO.StringWriter();
        //    //        HtmlTextWriter htextw = new HtmlTextWriter(stw);
        //    //        grid.RenderControl(htextw);
        //    //        Response.Write("<table>");
        //    //        Response.Write("<tr Font-Bold='true' align='center'>");
        //    //        Response.Write("<td colspan='6'>");
        //    //        Response.Write("<a style=\"font-weight:bold\">HINDI GROUP COMMISSION REPORT -" + Session["Lcode"].ToString() + "-" + fmDate + "-" + Todate + "</a>");
        //    //        Response.Write("</td>");
        //    //        Response.Write("</tr>");
        //    //        Response.Write("</table>");
        //    //        Response.Write(stw.ToString());
        //    //        Response.End();
        //    //        Response.Clear();
        //    //    }

        //    //string attachment = "attachment;filename=Hindi Group Commission Report.xls";
        //    //Response.ClearContent();
        //    //Response.AddHeader("content-disposition", attachment);
        //    //Response.ContentType = "application/ms-excel";
        //    //StringWriter stw = new StringWriter();
        //    //HtmlTextWriter htextw = new HtmlTextWriter(stw);
        //    //  DataTable dt = new DataTable();
        //    //query = "Select GroupName,TotalDays,PerDay,TotalAmount from HindiGroupCommission ";
        //    //  dt = objdata.RptEmployeeMultipleDetails(query);

        //    Report();
        //    if (AutoDTable.Rows.Count > 0)
        //    {
        //        grid1.DataSource = AutoDTable;
        //        grid1.DataBind();
        //        //StringWriter stw = new StringWriter();
        //        //HtmlTextWriter htextw = new HtmlTextWriter(stw);
        //        //grid1.RenderControl(htextw);
        //        //Response.Write(stw.ToString());



        //        string attachment = "attachment;filename=Hindi Group Commission Report.xls";
        //        Response.ClearContent();
        //        //Response.Buffer = true;
        //        Response.AddHeader("content-disposition", attachment);
        //        Response.ContentType = "application/ms-excel";
        //        StringWriter stw = new StringWriter();
        //        HtmlTextWriter htextw = new HtmlTextWriter(stw);
        //        grid1.RenderControl(htextw);


        //        Response.Write("<table style='font-weight:bold;'>");
        //        Response.Write("<tr>");
        //        Response.Write("<td>");
        //        Response.Write(GroupName);
        //        Response.Write("</td>");
        //        Response.Write("<td >");
        //        Response.Write("" + PerDay + "");
        //        Response.Write("</td>");
        //        Response.Write("<td >");
        //        Response.Write("" + TotalDays + "");
        //        Response.Write("</td>");
        //        Response.Write("<td >");
        //        Response.Write("" + GrandTotal + "");
        //        Response.Write("</td>");
        //        Response.Write("</tr>");
        //        Response.Write("</table>");
        //        Response.Write("<table border='1'>");
        //        Response.Write("<tr style='font-weight: bold;' align='center'>");
        //        Response.Write("</tr>");
        //        Response.Write("</table>");
        //        Response.Write(stw.ToString());
        //        // Response.Write(stw);
        //        // Response.End();
        //        Response.Clear();
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Downloaded Successfully');", true);
        //        Response.Clear();
        //        Response.End();
        //    }


        //}
        //catch (Exception ex)
        //{

        //}
    }

}

    

    
  