﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Globalization;

public partial class MusterReportBWDates : System.Web.UI.Page
{

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string Division;
    string Status;
    double Final_Count;
    string TokenNo;

    double Present_Count;
    double Absent_Count;
    decimal Fixed_Work_Days;
    decimal Month_WH_Count;
    double Present_WH_Count;
    double NFH_Days_Count;
    double NFH_Days_Present_Count;

    BALDataAccess objdata = new BALDataAccess();

    string SSQL = "";
    DataTable AutoDTable = new DataTable();
    DataTable mEmployeeDT = new DataTable();
    DataTable MEmployeeDS = new DataTable();
    DateTime date1;
    DateTime Date2 = new DateTime();
    int intK;
    string FromDate;
    string ToDate;
    string Date_Value_Str;
    string Date_value_str1;
    string[] Time_Minus_Value_Check;
    string WagesType;
    double leaveCount;

    System.Web.UI.WebControls.DataGrid grid =
                            new System.Web.UI.WebControls.DataGrid();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {

            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report- Muster Report Between Dates";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("ManualEntry"));
                //li.Attributes.Add("class", "droplink active open");
            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            //SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            Status = Request.QueryString["Status"].ToString();
           Division = Request.QueryString["Division"].ToString();
           string TempWages = Request.QueryString["Wages"].ToString();
           WagesType = TempWages.Replace("_", "&");
            //WagesType = Request.QueryString["Wages"].ToString();
           TokenNo = Request.QueryString["TokenNo"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();
            string SessionUserType_1 = SessionUserType;
            if (SessionUserType == "2")
            {
                NonAdminBetweenDates();
            }
            else if (SessionUserType_1 == SessionUserType)
            {
                Between_Date_Query_Output();
            }
            else
            {

                string TableName = "";

                if (Status == "Pending")
                {
                    TableName = "Employee_Mst_New_Emp";
                }

                else
                {
                    TableName = "Employee_Mst";
                }

                double Total_Time_get;
                DataTable dt = new DataTable();
                DataTable dt1 = new DataTable();
                DataTable mLocalDS = new DataTable();

                AutoDTable.Columns.Add("SNo");
                AutoDTable.Columns.Add("DeptName");
                AutoDTable.Columns.Add("MachineID");
                AutoDTable.Columns.Add("ExistingCode");
                AutoDTable.Columns.Add("FirstName");

                //AutoDTable.Columns.Add("Category");
                //AutoDTable.Columns.Add("Source");
                //AutoDTable.Columns.Add("Grade");

                date1 = Convert.ToDateTime(FromDate);
                string dat = ToDate;
                Date2 = Convert.ToDateTime(dat);

                int daycount = (int)((Date2 - date1).TotalDays);
                int daysAdded = 0;

                while (daycount >= 0)
                {
                    DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded).ToShortDateString());
                    //string day1=string.Format("{MM/dd/yyyy}",date1.AddDays(daysAdded));
                    AutoDTable.Columns.Add(Convert.ToString(dayy.ToShortDateString()));

                    daycount -= 1;
                    daysAdded += 1;
                }

                AutoDTable.Columns.Add("Total Days");


                SSQL = "select MachineID,isnull(FirstName,'') as FirstName,ExistingCode,DeptName,";
                SSQL = SSQL + " OTEligible,WeekOff,DOJ from " + TableName + " ";
                //   SSQL = SSQL + " inner join Department_Mst DM on EM.DeptName = DM.DeptCode";
                //SSQL = SSQL + " inner join MstCategory MC on EM.WageCategoty = MC.CateID ";
                //SSQL = SSQL + " inner join AgentMst AM on EM.BrokerName = AM.AgentID ";
                //SSQL = SSQL + " inner join MstGrade MG on EM.Grade = MG.GradeID ";
                SSQL = SSQL + " Where CompCode='" + Session["Ccode"].ToString() + "' And LocCode='" + Session["Lcode"].ToString() + "' and IsActive='Yes' ";
                // SSQL = SSQL + " And DM.CompCode='" + Session["Ccode"].ToString() + "' And DM.LocCode='" + Session["Lcode"].ToString() + "'";
                //  SSQL = SSQL + " And AM.CompCode='" + Session["Ccode"].ToString() + "' And AM.LocCode='" + Session["Lcode"].ToString() + "'";
                // SSQL = SSQL + " And MG.CompCode='" + Session["Ccode"].ToString() + "' And MG.LocCode='" + Session["Lcode"].ToString() + "'";
                if (TokenNo != "")
                {
                    SSQL = SSQL + " And ExistingCode='" + TokenNo + "'";
                }

                if (WagesType != "-Select-")
                {
                    SSQL = SSQL + " And Wages='" + WagesType + "'";
                }
                if (Division != "-Select-")
                {
                    SSQL = SSQL + " And Division = '" + Division + "'";
                }

                SSQL = SSQL + " Group by MachineID,FirstName,ExistingCode,DeptName,OTEligible,WeekOff,DOJ order by ExistingCode";
                dt = objdata.RptEmployeeMultipleDetails(SSQL);



                intK = 0;

                Present_WH_Count = 0;

                int SNo = 1;

                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    AutoDTable.NewRow();
                    AutoDTable.Rows.Add();

                    string MachineID = dt.Rows[i]["MachineID"].ToString();

                    if (MachineID == "10004") 
                    {
                        string CheckVal = "12";
                    }

                    AutoDTable.Rows[intK]["SNo"] = SNo;
                    AutoDTable.Rows[intK]["DeptName"] = dt.Rows[i]["DeptName"].ToString();
                    AutoDTable.Rows[intK]["MachineID"] = dt.Rows[i]["MachineID"].ToString();
                    AutoDTable.Rows[intK]["ExistingCode"] = dt.Rows[i]["ExistingCode"].ToString();
                    AutoDTable.Rows[intK]["FirstName"] = dt.Rows[i]["FirstName"].ToString();
                    string Emp_WH = dt.Rows[i]["WeekOff"].ToString();
                    string DOJ_Date_Str = "";
                    if (dt.Rows[i]["DOJ"].ToString() != "")
                    {
                        DOJ_Date_Str = dt.Rows[i]["DOJ"].ToString();
                    }
                    else
                    {
                        DOJ_Date_Str = "";
                    }
                    //AutoDTable.Rows[intK]["Source"] = dt.Rows[i]["AgentName"].ToString();
                    //AutoDTable.Rows[intK]["Grade"] = dt.Rows[i]["GradeName"].ToString();



                    string OTEllible = "";
                    OTEllible = dt.Rows[i]["OTEligible"].ToString();

                    int count = 5;
                    decimal count1 = 0;
                    decimal count2 = 0;
                    decimal count3 = 0;

                    if (dt.Rows[i]["MachineID"].ToString() == "305")
                    {
                        string Val;
                        Val = "1";
                    }
                    if (dt.Rows[i]["MachineID"].ToString() == "1010")
                    {
                        string Val;
                        Val = "1";
                    }
                    DateTime Query_Date_Check = new DateTime();
                    DateTime DOJ_Date_Check_Emp = new DateTime();


                    for (int j = 0; j < daysAdded; j++)
                    {
                        DateTime dayy = Convert.ToDateTime(date1.AddDays(j).ToShortDateString());

                        string Date1 = dayy.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);

                        Date_Value_Str = dayy.ToString("yyyy/MM/dd", CultureInfo.InvariantCulture);

                        if (DOJ_Date_Str != "")
                        {
                            
                            Query_Date_Check = Convert.ToDateTime(Date_Value_Str.ToString());
                            DOJ_Date_Check_Emp = Convert.ToDateTime(DOJ_Date_Str.ToString());
                            if (DOJ_Date_Check_Emp <= Query_Date_Check)
                            {

                                SSQL = "select Wh_Present_Count,Present from LogTime_Days where CompCode='" + Session["Ccode"].ToString() + "' And LocCode='" + Session["Lcode"].ToString() + "'";
                                SSQL = SSQL + " And MachineID='" + MachineID + "' And CONVERT(datetime,Attn_Date_Str,103) = CONVERT(datetime,'" + Date1 + "',103)";

                                dt1 = objdata.RptEmployeeMultipleDetails(SSQL);

                                if (dt1.Rows.Count > 0)
                                {
                                    Present_WH_Count = Convert.ToDouble(dt1.Rows[0]["Wh_Present_Count"].ToString());
                                    // NFH_Days_Present_Count = Convert.ToDouble(dt1.Rows[0]["NFH_Present_Count"].ToString());
                                    Present_Count = Convert.ToDouble(dt1.Rows[0]["Present"].ToString());
                                    //  Total_Time_get = Convert.ToDouble(dt1.Rows[0]["OTHours"].ToString());


                                    //NFh Days
                                    SSQL = "Select NFHDate from NFH_Mst where NFHDate=convert(varchar,'" + Date_Value_Str + "',103)";
                                    mLocalDS = objdata.RptEmployeeMultipleDetails(SSQL);
                                    if (mLocalDS.Rows.Count > 0)
                                    {

                                        if (Present_Count == 1)
                                        {
                                            AutoDTable.Rows[intK][count] = "<span style=color:green><b>" + "NH / X" + "</b></span>";
                                            count1 = count1 + 1;
                                            NFH_Days_Present_Count = 1;

                                        }
                                        else if (Present_Count == 0.5)
                                        {
                                            AutoDTable.Rows[intK][count] = "<span style=color:green><b>" + "NH / H" + "</b></span>";

                                            count1 = count1 + Convert.ToDecimal(0.5);
                                            NFH_Days_Present_Count = 0.5;
                                        }
                                        else
                                        {
                                            AutoDTable.Rows[intK][count] = "<span style=color:red><b>" + "NH / A" + "</b></span>";

                                        }

                                    }
                                    string Attn_Date_Day = DateTime.Parse(Date_Value_Str).DayOfWeek.ToString();
                                    if (Emp_WH == Attn_Date_Day)
                                    {
                                        if (Present_WH_Count == 1)
                                        {
                                            AutoDTable.Rows[intK][count] = "<span style=color:green><b>" + "WH/X" + "</b></span>";

                                            count1 = count1 + 1;
                                        }
                                        else if (Present_WH_Count == 0.5)
                                        {
                                            AutoDTable.Rows[intK][count] = "<span style=color:green><b>" + "WH/H" + "</b></span>";

                                            count1 = count1 + Convert.ToDecimal(0.5);
                                        }
                                        else
                                        {
                                            if (Present_Count == 0.0)
                                            {
                                                AutoDTable.Rows[intK][count] = "<span style=color:red><b>" + "WH/A" + "</b></span>";
                                            }

                                        }
                                    }

                                    if (Present_Count == 1)
                                    {
                                        if (NFH_Days_Present_Count != 1 && Present_WH_Count != 1.0)
                                        {

                                            AutoDTable.Rows[intK][count] = "<span style=color:green><b>" + "X" + "</b></span>";
                                            count1 = count1 + 1;
                                        }
                                    }


                                    else if (Present_Count == 0.5)
                                    {
                                        if (NFH_Days_Present_Count != 0.5 && Present_WH_Count != 0.5)
                                        {
                                            AutoDTable.Rows[intK][count] = "<span style=color:black><b>" + "H" + "</b></span>";

                                            count1 = count1 + Convert.ToDecimal(0.5);
                                        }
                                    }
                                    else
                                    {
                                        if (Emp_WH != Attn_Date_Day)
                                        {
                                            AutoDTable.Rows[intK][count] = "<span style=color:red><b>" + "A" + "</b></span>";
                                        }
                                    }

                                    NFH_Days_Present_Count = 0;
                                    Present_WH_Count = 0;
                                    Present_Count = 0;
                                }

                            }
                            else
                            {
                                AutoDTable.Rows[intK][count] = "";
                            }
                        }

                        count = count + 1;
                    }

                    AutoDTable.Rows[intK]["Total Days"] = count1;

                    intK = intK + 1;

                    SNo = SNo + 1;

                    count1 = 0;
                    //count2 = 0;

                }

                grid.DataSource = AutoDTable;
                grid.DataBind();
                string attachment = "attachment;filename=DAY ATTENDANCE BETWEEN DATES.xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";
                grid.HeaderStyle.Font.Bold = true;
                System.IO.StringWriter stw = new System.IO.StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);
                grid.RenderControl(htextw);
                Response.Write("<table>");
                //Response.Write("<tr Font-Bold='true' align='center'>");
                //Response.Write("<td colspan='10'>");
                //Response.Write("<a style=\"font-weight:bold\">" + Session["Ccode"].ToString() + "</a>");
                //Response.Write("--");
                //Response.Write("<a style=\"font-weight:bold\">" + Session["Lcode"].ToString() + "</a>");
                //Response.Write("</td>");
                //Response.Write("</tr>");
                Response.Write("<tr Font-Bold='true' align='left'>");
                Response.Write("<td colspan='10'>");
                Response.Write("<a style=\"font-weight:bold\">DAY ATTENANCE BETWEEN DATES REPORT -" + Session["Lcode"].ToString() + "-" + FromDate + "-" + ToDate + "</a>");

                Response.Write("</td>");
                Response.Write("</tr>");
                //Response.Write("<tr Font-Bold='true' align='center'>");
                //Response.Write("<td colspan='10'>");
                //Response.Write("<a style=\"font-weight:bold\"> FROM -" + FromDate + "</a>");
                //Response.Write("&nbsp;&nbsp;&nbsp;");
                //Response.Write("<a style=\"font-weight:bold\"> TO -" + ToDate + "</a>");
                //Response.Write("</td>");
                //Response.Write("</tr>");
                Response.Write("</table>");
                Response.Write(stw.ToString());
                Response.End();
                Response.Clear();

            }
        }
    }
    public void NonAdminBetweenDates()
    {

        double Total_Time_get;
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();
        DataTable mLocalDS = new DataTable();
        
        AutoDTable.Columns.Add("SNo");
        AutoDTable.Columns.Add("DeptName");
        AutoDTable.Columns.Add("MachineID");
        AutoDTable.Columns.Add("ExistingCode");
        AutoDTable.Columns.Add("FirstName");

        //AutoDTable.Columns.Add("Category");
        //AutoDTable.Columns.Add("Source");
        //AutoDTable.Columns.Add("Grade");

        date1 = Convert.ToDateTime(FromDate);
        string dat = ToDate;
        Date2 = Convert.ToDateTime(dat);

        int daycount = (int)((Date2 - date1).TotalDays);
        int daysAdded = 0;

        while (daycount >= 0)
        {
            DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded).ToShortDateString());
            //string day1=string.Format("{MM/dd/yyyy}",date1.AddDays(daysAdded));
            AutoDTable.Columns.Add(Convert.ToString(dayy.ToShortDateString()));

            daycount -= 1;
            daysAdded += 1;
        }

        AutoDTable.Columns.Add("Total Days");

        SSQL = "select MachineID,isnull(FirstName,'') as FirstName,ExistingCode,DeptName,";
        SSQL = SSQL + " OTEligible,WeekOff,DOJ from Employee_Mst ";
         SSQL = SSQL + " Where CompCode='" + Session["Ccode"].ToString() + "' And LocCode='" + Session["Lcode"].ToString() + "' and IsActive='Yes' ";
         SSQL = SSQL + " And Eligible_PF='1' ";

        if (WagesType != "-Select-")
        {
            SSQL = SSQL + " And Wages='" + WagesType + "'";
        }
        if (TokenNo != "")
        {
            SSQL = SSQL + " And ExistingCode='" + TokenNo + "'";
        }

        if (Division != "-Select-")
        {
            SSQL = SSQL + " And Division = '" + Division + "'";
        }

        SSQL = SSQL + " Group by MachineID,FirstName,ExistingCode,DeptName,OTEligible,WeekOff,DOJ order by MachineID";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);




        intK = 0;

        Present_WH_Count = 0;

        int SNo = 1;

        for (int i = 0; i < dt.Rows.Count; i++)
        {

            AutoDTable.NewRow();
            AutoDTable.Rows.Add();

            string MachineID = dt.Rows[i]["MachineID"].ToString();

            AutoDTable.Rows[intK]["SNo"] = SNo;
            AutoDTable.Rows[intK]["DeptName"] = dt.Rows[i]["DeptName"].ToString();
            AutoDTable.Rows[intK]["MachineID"] = dt.Rows[i]["MachineID"].ToString();
            AutoDTable.Rows[intK]["ExistingCode"] = dt.Rows[i]["ExistingCode"].ToString();
            AutoDTable.Rows[intK]["FirstName"] = dt.Rows[i]["FirstName"].ToString();
            string Emp_WH = dt.Rows[i]["WeekOff"].ToString();
            string DOJ_Date_Str = "";
            if (dt.Rows[i]["DOJ"].ToString() != "")
            {
                DOJ_Date_Str = dt.Rows[i]["DOJ"].ToString();
            }
            else
            {
                DOJ_Date_Str = "";
            }
            //AutoDTable.Rows[intK]["Source"] = dt.Rows[i]["AgentName"].ToString();
            //AutoDTable.Rows[intK]["Grade"] = dt.Rows[i]["GradeName"].ToString();



            string OTEllible = "";
            OTEllible = dt.Rows[i]["OTEligible"].ToString();

            int count = 5;
            decimal count1 = 0;
            decimal count2 = 0;
            decimal count3 = 0;

            if (dt.Rows[i]["MachineID"].ToString() == "7404")
            {
                string Val;
                Val = "1";
            }
            if (dt.Rows[i]["MachineID"].ToString() == "7406")
            {
                string Val;
                Val = "1";
            }
            DateTime Query_Date_Check = new DateTime();
            DateTime DOJ_Date_Check_Emp = new DateTime();


            for (int j = 0; j < daysAdded; j++)
            {
                DateTime dayy = Convert.ToDateTime(date1.AddDays(j).ToShortDateString());

                string Date1 = dayy.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);

                Date_Value_Str = dayy.ToString("yyyy/MM/dd", CultureInfo.InvariantCulture);

                if (DOJ_Date_Str != "")
                {

                    Query_Date_Check = Convert.ToDateTime(Date_Value_Str.ToString());
                    DOJ_Date_Check_Emp = Convert.ToDateTime(DOJ_Date_Str.ToString());
                    if (DOJ_Date_Check_Emp <= Query_Date_Check)
                    {

                        SSQL = "select Wh_Present_Count,Present from LogTime_Days where CompCode='" + Session["Ccode"].ToString() + "' And LocCode='" + Session["Lcode"].ToString() + "'";
                        SSQL = SSQL + " And MachineID='" + MachineID + "' And CONVERT(datetime,Attn_Date_Str,103) = CONVERT(datetime,'" + Date1 + "',103)";

                        dt1 = objdata.RptEmployeeMultipleDetails(SSQL);

                        if (dt1.Rows.Count > 0)
                        {
                            Present_WH_Count = Convert.ToDouble(dt1.Rows[0]["Wh_Present_Count"].ToString());
                            // NFH_Days_Present_Count = Convert.ToDouble(dt1.Rows[0]["NFH_Present_Count"].ToString());
                            Present_Count = Convert.ToDouble(dt1.Rows[0]["Present"].ToString());
                            //  Total_Time_get = Convert.ToDouble(dt1.Rows[0]["OTHours"].ToString());


                            //NFh Days
                            SSQL = "Select NFHDate from NFH_Mst where NFHDate=convert(varchar,'" + Date_Value_Str + "',103)";
                            mLocalDS = objdata.RptEmployeeMultipleDetails(SSQL);
                            if (mLocalDS.Rows.Count > 0)
                            {

                                if (Present_Count == 1)
                                {
                                    AutoDTable.Rows[intK][count] = "<span style=color:green><b>" + "NH / X" + "</b></span>";
                                    count1 = count1 + 1;
                                    NFH_Days_Present_Count = 1;

                                }
                                else if (Present_Count == 0.5)
                                {
                                    AutoDTable.Rows[intK][count] = "<span style=color:green><b>" + "NH / H" + "</b></span>";

                                    count1 = count1 + Convert.ToDecimal(0.5);
                                    NFH_Days_Present_Count = 0.5;
                                }
                                else
                                {
                                    AutoDTable.Rows[intK][count] = "<span style=color:red><b>" + "NH / A" + "</b></span>";

                                }
                            }
                            
                            string Attn_Date_Day = DateTime.Parse(Date_Value_Str).DayOfWeek.ToString();
                            if (Emp_WH == Attn_Date_Day)
                            {
                                if (Present_WH_Count == 1)
                                {
                                    AutoDTable.Rows[intK][count] = "<span style=color:green><b>" + "WH/X" + "</b></span>";

                                    count1 = count1 + 1;
                                }
                                else if (Present_WH_Count == 0.5)
                                {
                                    AutoDTable.Rows[intK][count] = "<span style=color:green><b>" + "WH/H" + "</b></span>";

                                    count1 = count1 + Convert.ToDecimal(0.5);
                                }
                                else
                                {
                                    if (Present_Count == 0.0)
                                    {
                                        AutoDTable.Rows[intK][count] = "<span style=color:red><b>" + "WH/A" + "</b></span>";
                                    }

                                }
                            }

                            if (Present_Count == 1)
                            {
                                if (NFH_Days_Present_Count != 1 && Present_WH_Count != 1.0)
                                {

                                    AutoDTable.Rows[intK][count] = "<span style=color:green><b>" + "X" + "</b></span>";
                                    count1 = count1 + 1;
                                }
                            }


                            else if (Present_Count == 0.5)
                            {
                                if (NFH_Days_Present_Count != 0.5 && Present_WH_Count != 0.5)
                                {
                                    AutoDTable.Rows[intK][count] = "<span style=color:black><b>" + "H" + "</b></span>";

                                    count1 = count1 + Convert.ToDecimal(0.5);
                                }
                            }
                            else
                            {
                                if (Emp_WH != Attn_Date_Day)
                                {
                                    AutoDTable.Rows[intK][count] = "<span style=color:red><b>" + "A" + "</b></span>";
                                }
                            }

                            NFH_Days_Present_Count = 0;
                            Present_WH_Count = 0;
                            Present_Count = 0;
                        }

                    }
                    else
                    {
                        AutoDTable.Rows[intK][count] = "";
                    }
                }

                count = count + 1;
            }

            AutoDTable.Rows[intK]["Total Days"] = count1;

            intK = intK + 1;

            SNo = SNo + 1;

            count1 = 0;
            //count2 = 0;

        }


        grid.DataSource = AutoDTable;
        grid.DataBind();
        string attachment = "attachment;filename=DAY ATTENDANCE BETWEEN DATES.xls";
        Response.ClearContent();
        Response.AddHeader("content-disposition", attachment);
        Response.ContentType = "application/ms-excel";
        grid.HeaderStyle.Font.Bold = true;
        System.IO.StringWriter stw = new System.IO.StringWriter();
        HtmlTextWriter htextw = new HtmlTextWriter(stw);
        grid.RenderControl(htextw);
        Response.Write("<table>");
        //Response.Write("<tr Font-Bold='true' align='center'>");
        //Response.Write("<td colspan='10'>");
        //Response.Write("<a style=\"font-weight:bold\">" + Session["Ccode"].ToString() + "</a>");
        //Response.Write("--");
        //Response.Write("<a style=\"font-weight:bold\">" + Session["Lcode"].ToString() + "</a>");
        //Response.Write("</td>");
        //Response.Write("</tr>");
        Response.Write("<tr Font-Bold='true' align='left'>");
        Response.Write("<td colspan='10'>");
        Response.Write("<a style=\"font-weight:bold\">DAY ATTENANCE BETWEEN DATES REPORT -" + Session["Lcode"].ToString() + "-" + FromDate + "-" + ToDate + "</a>");

        Response.Write("</td>");
        Response.Write("</tr>");
        //Response.Write("<tr Font-Bold='true' align='center'>");
        //Response.Write("<td colspan='10'>");
        //Response.Write("<a style=\"font-weight:bold\"> FROM -" + FromDate + "</a>");
        //Response.Write("&nbsp;&nbsp;&nbsp;");
        //Response.Write("<a style=\"font-weight:bold\"> TO -" + ToDate + "</a>");
        //Response.Write("</td>");
        //Response.Write("</tr>");
        Response.Write("</table>");
        Response.Write(stw.ToString());
        Response.End();
        Response.Clear();


    }

    private void Between_Date_Query_Output()
    {
        string TableName = "";

        if (Status == "Pending")
        {
            TableName = "Employee_Mst_New_Emp";
        }

        else
        {
            TableName = "Employee_Mst";
        }

        double Total_Time_get;
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();
        DataTable mLocalDS = new DataTable();

        AutoDTable.Columns.Add("SNo");
        AutoDTable.Columns.Add("DeptName");
        AutoDTable.Columns.Add("MachineID");
        AutoDTable.Columns.Add("ExistingCode");
        AutoDTable.Columns.Add("FirstName");

        date1 = Convert.ToDateTime(FromDate);
        string dat = ToDate;
        Date2 = Convert.ToDateTime(dat);

        DateTime Query_Date_Check = new DateTime();
        DateTime DOJ_Date_Check_Emp = new DateTime();

        int daycount = (int)((Date2 - date1).TotalDays);
        int daysAdded = 0;
        string Query_header = "";
        string Query_Pivot = "";
        while (daycount >= 0)
        {
            DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded).ToShortDateString());
            //string day1=string.Format("{MM/dd/yyyy}",date1.AddDays(daysAdded));
            AutoDTable.Columns.Add(Convert.ToString(dayy.Day.ToString()));

            if (daysAdded==0)
            {
                Query_header = "isnull([" + dayy.Day.ToString() + "],'A') as [" + dayy.Day.ToString() + "]";
                Query_Pivot = "[" + dayy.Day.ToString() + "]";
            }
            else
            {
                Query_header = Query_header + ",isnull([" + dayy.Day.ToString() + "],'A') as [" + dayy.Day.ToString() + "]";
                Query_Pivot = Query_Pivot + ",[" + dayy.Day.ToString() + "]";
            }
            daycount -= 1;
            daysAdded += 1;
        }

        AutoDTable.Columns.Add("Total_Days");

        string query = "";

        query = "Select DeptName,MachineID,ExistingCode,FirstName,DOJ,WeekOff," + Query_header + " from ( ";
        query = query + " Select EM.DeptName,EM.MachineID,EM.ExistingCode,EM.FirstName,EM.DOJ,EM.WeekOff,DATENAME(dd, Attn_Date) as Day_V, ";
        query = query + " (Case when EM.DOJ > LD.Attn_Date then '' when Datename(weekday,LD.Attn_Date)=EM.WeekOff then (CASE WHEN LD.Present = 1.0 THEN 'WH/X' WHEN LD.Present = 0.5 THEN 'WH/H' else 'WH/A' End) ";
        query = query + " else (CASE WHEN LD.Present = 1.0 THEN 'X' WHEN LD.Present = 0.5 THEN 'H' else 'A' End) end) as Presents ";
        query = query + " from LogTime_Days LD inner join " + TableName + " EM on EM.LocCode=LD.LocCode And EM.CompCode=LD.CompCode And EM.ExistingCode=LD.ExistingCode ";
        query = query + " where LD.LocCode='" + SessionLcode + "' ";
        query = query + " And CONVERT(datetime,LD.Attn_Date,103) >= CONVERT(datetime,'" + FromDate + "',103) ";
        query = query + " And CONVERT(datetime,LD.Attn_Date,103) <= CONVERT(datetime,'" + ToDate + "',103) ";
        if (TokenNo != "")
        {
            query = query + " And LD.ExistingCode='" + TokenNo + "' And EM.ExistingCode='" + TokenNo + "'";
        }
        if (WagesType != "-Select-")
        {
            query = query + " And EM.Wages='" + WagesType + "'";
            query = query + " And LD.Wages='" + WagesType + "'";
        }
        if (Division != "-Select-")
        {
            query = query + " And EM.Division = '" + Division + "'";
        }
        query = query + " And EM.LocCode='" + SessionLcode + "') as CV ";
        query = query + " pivot (max (Presents) for Day_V in (" + Query_Pivot + ")) as AvgIncomePerDay ";
        query = query + " order by ExistingCode Asc";
        dt = objdata.RptEmployeeMultipleDetails(query);


        //NFH Check start
        query = "Select NFHDate from NFH_Mst where convert(datetime,NFHDate,103) >=convert(datetime,'" + FromDate + "',103)";
        query = query + " And convert(datetime,NFHDate,103) <=convert(datetime,'" + ToDate + "',103) order by NFHDate Asc";
        DataTable DT_NH = new DataTable();
        DT_NH = objdata.RptEmployeeMultipleDetails(query);
        if (DT_NH.Rows.Count != 0)
        {
            for (int ih = 0; ih < DT_NH.Rows.Count; ih++)
            {
                DateTime dayy = Convert.ToDateTime(DT_NH.Rows[ih]["NFHDate"].ToString());
                
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string DOJ_Date_Str = "";
                    if (dt.Rows[i]["DOJ"].ToString() != "")
                    {
                        DOJ_Date_Str = dt.Rows[i]["DOJ"].ToString();
                    }
                    else
                    {
                        DOJ_Date_Str = "";
                    }
                    if (DOJ_Date_Str != "")
                    {
                        Query_Date_Check = Convert.ToDateTime(dayy.Date.ToString());
                        DOJ_Date_Check_Emp = Convert.ToDateTime(DOJ_Date_Str.ToString());
                        if (DOJ_Date_Check_Emp <= Query_Date_Check)
                        {
                            if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "X") { dt.Rows[i]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "NH/X" + "</b></span>"; }
                            if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "H") { dt.Rows[i]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "NH/H" + "</b></span>"; }
                            if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "A") { dt.Rows[i]["" + dayy.Day.ToString() + ""] = "<span style=color:red><b>" + "NH/A" + "</b></span>"; }
                            if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/X") { dt.Rows[i]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "NH/X" + "</b></span>"; }
                            if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/H") { dt.Rows[i]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "NH/H" + "</b></span>"; }
                            if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/A") { dt.Rows[i]["" + dayy.Day.ToString() + ""] = "<span style=color:red><b>" + "NH/A" + "</b></span>"; }
                        }
                        else
                        {
                            dt.Rows[i]["" + dayy.Day.ToString() + ""] = "";
                        }
                    }
                }
            }
        }
        //NFH Check End

        for (int i = 0; i < dt.Rows.Count; i++)
        {

            AutoDTable.NewRow();
            AutoDTable.Rows.Add();

            string MachineID = dt.Rows[i]["MachineID"].ToString();
            int DT_Row = AutoDTable.Rows.Count - 1;
            AutoDTable.Rows[DT_Row]["SNo"] = (Convert.ToDecimal(i) + Convert.ToDecimal(1)).ToString();
            AutoDTable.Rows[DT_Row]["DeptName"] = dt.Rows[i]["DeptName"].ToString();
            AutoDTable.Rows[DT_Row]["MachineID"] = dt.Rows[i]["MachineID"].ToString();
            AutoDTable.Rows[DT_Row]["ExistingCode"] = dt.Rows[i]["ExistingCode"].ToString();
            AutoDTable.Rows[DT_Row]["FirstName"] = dt.Rows[i]["FirstName"].ToString();

            string Emp_WH = dt.Rows[i]["WeekOff"].ToString();
            string DOJ_Date_Str = "";
            if (dt.Rows[i]["DOJ"].ToString() != "")
            {
                DOJ_Date_Str = dt.Rows[i]["DOJ"].ToString();
            }
            else
            {
                DOJ_Date_Str = "";
            }

            daycount = (int)((Date2 - date1).TotalDays);
            daysAdded = 0;
            
            while (daycount >= 0)
            {
                DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded).ToShortDateString());
                if (DOJ_Date_Str != "")
                {
                    Query_Date_Check = Convert.ToDateTime(dayy.Date.ToString());
                    DOJ_Date_Check_Emp = Convert.ToDateTime(DOJ_Date_Str.ToString());
                    if (DOJ_Date_Check_Emp <= Query_Date_Check)
                    {
                        AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString();
                        if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "X") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "X" + "</b></span>"; }
                        if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "H") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:black><b>" + "H" + "</b></span>"; }
                        if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "A") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:red><b>" + "A" + "</b></span>"; }
                        if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/X") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "WH/X" + "</b></span>"; }
                        if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/H") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "WH/H" + "</b></span>"; }
                        if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/A") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:red><b>" + "WH/A" + "</b></span>"; }
                        
                    }
                    else
                    {
                        AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "";
                    }
                }
                else
                {
                    AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString();
                    if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "X") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "X" + "</b></span>"; }
                    if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "H") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:black><b>" + "H" + "</b></span>"; }
                    if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "A") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:red><b>" + "A" + "</b></span>"; }
                    if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/X") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "WH/X" + "</b></span>"; }
                    if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/H") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "WH/H" + "</b></span>"; }
                    if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/A") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:red><b>" + "WH/A" + "</b></span>"; }
                }
                
                daycount -= 1;
                daysAdded += 1;
            }
            //Get Total Days
            query = "Select isnull(sum(present),0) as Total_Days from LogTime_Days where ExistingCode='" + dt.Rows[i]["ExistingCode"].ToString() + "'";
            query = query + " And LocCode='" + SessionLcode + "'";
            query = query + " And CONVERT(datetime,Attn_Date,103) >= CONVERT(datetime,'" + FromDate + "',103) ";
            query = query + " And CONVERT(datetime,Attn_Date,103) <= CONVERT(datetime,'" + ToDate + "',103) ";
            dt1 = objdata.RptEmployeeMultipleDetails(query);
            if (dt1.Rows.Count != 0)
            {
                AutoDTable.Rows[DT_Row]["Total_Days"] = dt1.Rows[0]["Total_Days"].ToString();
            }
            else
            {
                AutoDTable.Rows[DT_Row]["Total_Days"] = "0";
            }
        }

        if (AutoDTable.Rows.Count != 0)
        {
            grid.DataSource = AutoDTable;
            grid.DataBind();
            string attachment = "attachment;filename=DAY ATTENDANCE BETWEEN DATES.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);
            Response.Write("<table>");
            //Response.Write("<tr Font-Bold='true' align='center'>");
            //Response.Write("<td colspan='10'>");
            //Response.Write("<a style=\"font-weight:bold\">" + Session["Ccode"].ToString() + "</a>");
            //Response.Write("--");
            //Response.Write("<a style=\"font-weight:bold\">" + Session["Lcode"].ToString() + "</a>");
            //Response.Write("</td>");
            //Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='left'>");
            Response.Write("<td colspan='10'>");
            Response.Write("<a style=\"font-weight:bold\">DAY ATTENANCE BETWEEN DATES REPORT -" + Session["Lcode"].ToString() + "-" + FromDate + "-" + ToDate + "</a>");

            Response.Write("</td>");
            Response.Write("</tr>");
            //Response.Write("<tr Font-Bold='true' align='center'>");
            //Response.Write("<td colspan='10'>");
            //Response.Write("<a style=\"font-weight:bold\"> FROM -" + FromDate + "</a>");
            //Response.Write("&nbsp;&nbsp;&nbsp;");
            //Response.Write("<a style=\"font-weight:bold\"> TO -" + ToDate + "</a>");
            //Response.Write("</td>");
            //Response.Write("</tr>");
            Response.Write("</table>");
            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();
        }
        else
        {
        }
    }
}
