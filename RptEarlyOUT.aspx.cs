﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;

public partial class RptEarlyOUT : System.Web.UI.Page
{
    string ShiftType1 = "";
    string Date = "";
    string Status = "";
    string fmDate = "";
    string Todate = "";
    string Wages = "";
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string ss = "";


    ReportDocument report = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();

    string SSQL = "";

    DataTable AutoDTable = new DataTable();
    string Shift = "";

    DateTime date1;
    DateTime Date2 = new DateTime();


    System.Web.UI.WebControls.DataGrid grid =
                           new System.Web.UI.WebControls.DataGrid();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Report-Shift Hour Based";
            }
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            Status = Request.QueryString["Status"].ToString();
            Shift = Request.QueryString["Shift"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            fmDate = Request.QueryString["FromDate"].ToString();
            Todate = Request.QueryString["ToDate"].ToString();
            Wages = Request.QueryString["Wages"].ToString();

            Report();
        }
    }

    private void Report()
    {
        string TableName = "";
        if (Status == "Pending")
        {
            TableName = "Employee_Mst_New_Emp";
        }
        else
        {
            TableName = "Employee_Mst";
        }

        SSQL = "";
        SSQL = "Select * from " + TableName + " where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and IsActive='Yes'";
        if (Wages.ToString() != "-Select-")
        {
            SSQL = SSQL + " and Wages='" + Wages + "'";
        }
        DataTable EmpDt = new DataTable();
        EmpDt = objdata.RptEmployeeMultipleDetails(SSQL);

        if (EmpDt.Rows.Count > 0)
        {
            //AutoDTable.Columns.Add("S.No");
            AutoDTable.Columns.Add("MachineID");
            AutoDTable.Columns.Add("Name");
            AutoDTable.Columns.Add("DeptName");

            date1 = Convert.ToDateTime(fmDate);
            string dat = Todate;
            Date2 = Convert.ToDateTime(dat);

            int daycount = (int)((Date2 - date1).TotalDays);
            int daysAdded = 0;
            while (daycount >= 0)
            {
                DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded).ToShortDateString());
                AutoDTable.Columns.Add(dayy.ToString("dd/MM/yyyy"));

                daycount -= 1;
                daysAdded += 1;
            }

            AutoDTable.Columns.Add("Total Days");

            for (int EmpRw = 0; EmpRw < EmpDt.Rows.Count; EmpRw++)
            {
                DateTime TimeIN =Convert.ToDateTime("00:00");
                DateTime TimeOUT= Convert.ToDateTime("00:00");
                DateTime Shift_Start= Convert.ToDateTime("00:00");
                DateTime Shift_End= Convert.ToDateTime("00:00");
                DateTime Early_OUT_Start_Dt= Convert.ToDateTime("00:00");
                DateTime Early_OUT_End_Dt= Convert.ToDateTime("00:00");

                int Daycnt = 0;

                string Early_OUT_Start = "";
                string Early_OUT_End = "";
                string Shift_Name = "";

                TimeSpan Total_Hrs ;


                AutoDTable.NewRow();
                AutoDTable.Rows.Add();
                
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["MachineID"] = EmpDt.Rows[EmpRw]["MachineID"];
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Name"] = EmpDt.Rows[EmpRw]["FirstName"];
                AutoDTable.Rows[AutoDTable.Rows.Count - 1]["DeptName"] = EmpDt.Rows[EmpRw]["DeptName"];

                daycount = (int)((Date2 - date1).TotalDays);
                daysAdded = 0;

                while (daycount >= 0)
                {
                    bool Shift_Checked = false;

                    DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded).ToShortDateString());

                    SSQL = "Select SM.StartTime,SM.EndTime,SM.ShiftDesc,EO.ShiftCd,EO.EarlyStart,EO.EarlyEnd, ";
                    SSQL = SSQL + " LD.MachineID, LD.FirstName, LD.Total_Hrs1, LD.TimeIN, LD.TimeOUT, LD.Shift";
                    SSQL = SSQL + " from LogTime_Days LD inner join Shift_Mst SM on SM.ShiftDesc=LD.Shift and SM.CompCode=LD.CompCode and SM.LocCode=LD.LocCode Inner Join MstEarlyOUT EO on EO.ShiftCd=SM.ShiftDesc and EO.CompCode=LD.CompCode and EO.LocCode=LD.LocCode";
                    SSQL = SSQL + " where LD.CompCode='" + SessionCcode + "' and LD.LocCode='" + SessionLcode + "'";
                    SSQL = SSQL + " and LD.MachineID='" + EmpDt.Rows[EmpRw]["MachineID"] + "' and Convert(datetime,LD.Attn_Date_Str,103)=Convert(datetime,'" + dayy.ToString("dd/MM/yyyy") + "',103) and LD.Present='1' and LD.TimeIN!='' and LD.TimeOUT!='' and (LD.Shift!='No Shift' or LD.Shift!='Leave')";

                    if (Shift.ToString() != "ALL")
                    {
                        SSQL = SSQL + " and LD.Shift='" + Shift.ToString().ToUpper() + "'";
                    }

                    DataTable Shift_DT = new DataTable();
                    Shift_DT = objdata.RptEmployeeMultipleDetails(SSQL);

                    if (Shift_DT.Rows.Count > 0)
                    {
                        TimeIN = Convert.ToDateTime(Shift_DT.Rows[0]["TimeIN"].ToString());
                        TimeOUT = Convert.ToDateTime(Shift_DT.Rows[0]["TimeOUT"].ToString());

                        Shift_Start = Convert.ToDateTime(Shift_DT.Rows[0]["StartTime"]);
                        Shift_End = Convert.ToDateTime(Shift_DT.Rows[0]["EndTime"]);

                        Early_OUT_Start = Convert.ToString(Shift_DT.Rows[0]["EarlyStart"].ToString().Replace(".", ":"));
                        Early_OUT_End = Convert.ToString(Shift_DT.Rows[0]["EarlyEnd"].ToString().Replace(".", ":"));

                        Early_OUT_Start_Dt = Convert.ToDateTime(Early_OUT_Start.ToString());
                        Early_OUT_End_Dt = Convert.ToDateTime(Early_OUT_End.ToString());

                        if (TimeOUT >= Early_OUT_Start_Dt & TimeOUT < Shift_End)
                        {
                            Shift_Checked = true;
                            Daycnt += 1;
                            Shift_Name = Shift_DT.Rows[0]["Shift"].ToString();
                            Total_Hrs = Shift_End.Subtract(TimeOUT);
                        }
                    }

                    if (Shift_Checked)
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1][dayy.ToString("dd/MM/yyyy")] = Shift_Name + "/" + TimeOUT.ToString("hh:mm tt");
                    else
                        AutoDTable.Rows[AutoDTable.Rows.Count - 1][dayy.ToString("dd/MM/yyyy")] = "";

                    daycount -= 1;
                    daysAdded += 1;
                }

                if (Daycnt >= 5)
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Total Days"] = "<b style=color:red>" + Daycnt + "</b>";
                else
                    AutoDTable.Rows[AutoDTable.Rows.Count - 1]["Total Days"] =  Daycnt;
            }
        }

        for (int rw = AutoDTable.Rows.Count - 1; rw >= 0; rw--)
        {
            DataRow dr = AutoDTable.Rows[rw];
            if (dr["Total Days"].ToString() == "" | dr["Total Days"].ToString() == "0" | dr["Total Days"].ToString() == string.Empty)
                dr.Delete();
        }
        AutoDTable.AcceptChanges();

        if (AutoDTable.Rows.Count > 0)
        {
            grid.DataSource = AutoDTable;
            grid.DataBind();
            string attachment = "attachment;filename=EARLY OUT REPORT - BETWEEN DATES.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);
            Response.Write("<table>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='6'>");
            Response.Write("<a style=\"font-weight:bold\">EARLY OUT REPORT -" + SessionCcode + "-" + SessionLcode + "-" + fmDate + "-" + Todate + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("</table>");
            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();
        }
    }
}