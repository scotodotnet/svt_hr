﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class MstLateIN : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights; string SessionAdmin;
    string SSQL;
    bool ErrFlag = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        if (!IsPostBack)
        {
            Load_OlD();
        }
    }

    private void Load_OlD()
    {
        SSQL = "";
        SSQL = "Select * from MstLateIN where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        DataTable DT = new DataTable();
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count > 0)
        {

            txtStartTime1.Text = dt.Rows[0][2].ToString();
            txtStartTime2.Text = dt.Rows[1][2].ToString();
            txtStartTime3.Text = dt.Rows[2][2].ToString();
            txtEndTime1.Text = dt.Rows[0][3].ToString();
            txtEndTime2.Text = dt.Rows[1][3].ToString();
            txtEndTime3.Text = dt.Rows[2][3].ToString();
            txtDeductionHrs1.Text = dt.Rows[0][4].ToString();
            txtDeductionHrs2.Text = dt.Rows[1][4].ToString();
            txtDeductionHrs3.Text = dt.Rows[2][4].ToString();

        }
        else
        {
            txtStartTime1.Text = "0";
            txtStartTime2.Text = "0";
            txtStartTime3.Text = "0";
            txtEndTime1.Text = "0";
            txtEndTime2.Text = "0";
            txtEndTime3.Text = "0";
            txtDeductionHrs1.Text = "0";
            txtDeductionHrs2.Text = "0";
            txtDeductionHrs3.Text = "0";
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (txtStartTime1.Text == "" || txtStartTime2.Text == "" || txtStartTime3.Text == "" || txtEndTime1.Text == "" || txtEndTime2.Text == "" || txtEndTime3.Text == "" || txtDeductionHrs1.Text == "" || txtDeductionHrs2.Text == "" || txtDeductionHrs3.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please Check the Fields....');", true);
            return;
        }
        if (!ErrFlag)
        {
            SSQL = "";
            SSQL = "Delete From MstLateIN where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "";
            SSQL = "Insert Into MstLateIN values('" + SessionCcode + "','" + SessionLcode + "','" + txtStartTime1.Text + "','" + txtEndTime1.Text + "','" + txtDeductionHrs1.Text + "');";
            objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "";
            SSQL = "Insert Into MstLateIN values('" + SessionCcode + "','" + SessionLcode + "','" + txtStartTime2.Text + "','" + txtEndTime2.Text + "','" + txtDeductionHrs2.Text + "');";
            objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "";
            SSQL = "Insert Into MstLateIN values('" + SessionCcode + "','" + SessionLcode + "','" + txtStartTime3.Text + "','" + txtEndTime3.Text + "','" + txtDeductionHrs3.Text + "');";
            objdata.RptEmployeeMultipleDetails(SSQL);

            Load_OlD();

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Data Saved Successfully!!!');", true);
        }
    }

    protected void btnClr_Click(object sender, EventArgs e)
    {
        txtStartTime1.Text = "0";
        txtStartTime2.Text = "0";
        txtStartTime3.Text = "0";
        txtEndTime1.Text = "0";
        txtEndTime2.Text = "0";
        txtEndTime3.Text = "0";
        txtDeductionHrs1.Text = "0";
        txtDeductionHrs2.Text = "0";
        txtDeductionHrs3.Text = "0";
    }
}