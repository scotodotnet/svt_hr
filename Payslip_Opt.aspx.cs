﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

using Payroll;
using Payroll.Data;
using Payroll.Configuration;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Globalization;

public partial class Payslip_Opt : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    DateTime TransDate;
    string SessionAdmin;
    string Stafflabour;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string NetBase;
    string NetFDA;
    string NetVDA;
    string NetHRA;
    string Nettotal;
    string NetPFEarnings;
    string NetPF;
    string NetESI;
    string NetUnion;
    string NetAdvance;
    string NetAll1;
    string NetAll2;
    string NetAll3;
    string NetAll4;
    string NetAll5;
    string NetDed1;
    string NetDed2;
    string NetDed3;
    string NetDed4;
    string NetDed5;
    string HomeDays;
    string Halfnight;
    string FullNight;
    string DayIncentive;
    string Spinning;
    string ThreeSided;
    string NetLOP;
    string NetStamp;
    string NetTotalDeduction;
    string NetOT;
    string NetAmt;
    string Network;
    string totNFh;
    string totweekoff;
    string totCL;
    string totwork;
    string Roundoff;
    DateTime MyDate;
    DateTime MyDate1;
    DateTime MyDate2;
    static decimal AdvAmt;
    static string ID;
    static string Adv_id = "";
    static string Dec_mont = "0";
    static string Adv_BalanceAmt = "0";
    static string Adv_due = "0";
    static string Increment_mont = "0";
    static decimal val;
    static string EmployeeDays = "0";
    string MyMonth;
    static string cl = "0";
    string TempDate;
    static string Fixedsal = "0";
    static string FixedOT = "0";
    static string Tot_OThr = "0";
    static string Emp_ESI_Code = "";
    static string NetPay_Grand_Total = "0";
    static string NetPay_Grand_Total_Words = "";
    static bool isUK = false;
    string SessionPayroll;
    string SessionUserName;
    string SessionUserID;
    string SessionRights;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        Load_DB();
        if (!IsPostBack)
        {
            Load_WagesType();
            Load_Department();
            Load_BankName();
            Months_load();
            //ESICode_load();
            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddlFinance.Items.Add(new System.Web.UI.WebControls.ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }

            currentYear = Utility.GetCurrentYearOnly;
            for (int i = 0; i < 10; i++)
            {
                txtLCYear.Items.Add(new System.Web.UI.WebControls.ListItem(currentYear.ToString(), currentYear.ToString()));
                currentYear = currentYear - 1;
            }
            Load_Division_Name();
            //Master.Visible = false;
            if (SessionUserType == "2")
            {
                IFUser_Fields_Hide();
            }
        }


        //IFUser Work
        //IFUserType();
    }
    public void Load_DB()
    {
        //Get Database Name
        string query = "";
        DataTable dt_DB = new DataTable();
        query = "select CMS,Rights,Sales,Stores,Weaving,Maintaince,Production,Payroll from [SVT_Rights]..MstDBname";
        dt_DB = objdata.RptEmployeeMultipleDetails(query);
        if (dt_DB.Rows.Count > 0)
        {
            SessionPayroll = dt_DB.Rows[0]["Payroll"].ToString();
            //SessionRights = dt_DB.Rows[0]["Rights"].ToString();
        }
    }
    private void Load_WagesType()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtEmployeeType.Items.Clear();
        string Category_Str = "0";
        if (ddlcategory.SelectedItem.Text == "STAFF")
        {
            Category_Str = "1";
        }
        else if (ddlcategory.SelectedItem.Text == "LABOUR")
        {
            Category_Str = "2";
        }
        query = "Select *from MstEmployeeType where EmpCategory='" + Category_Str + "'";
        if (SessionUserType == "2")
        {
            query = query + " And EmpType <> 'SUB-STAFF' And EmpType <> 'CIVIL' And EmpType <> 'OTHERS'";
        }

        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtEmployeeType.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpTypeCd"] = "0";
        dr["EmpType"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtEmployeeType.DataTextField = "EmpType";
        txtEmployeeType.DataValueField = "EmpTypeCd";
        txtEmployeeType.DataBind();
    }

    private void Load_Division_Name()
    {
        string query = "";
        DataTable DIV_DT = new DataTable();
        query = "Select '0' as Row_Num,'-Select-' as Division union ";
        query = query + "Select '1' as Row_Num,Division from Division_Master";
        query = query + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        query = query + " Order by Row_Num,Division Asc";
        DIV_DT = objdata.RptEmployeeMultipleDetails(query);
        txtDivision.DataSource = DIV_DT;
        txtDivision.DataTextField = "Division";
        txtDivision.DataValueField = "Division";
        txtDivision.DataBind();

    }

    private void Load_Department()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddldept.Items.Clear();
        query = "Select *from Department_Mst";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddldept.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["DeptCode"] = "0";
        dr["DeptName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddldept.DataTextField = "DeptName";
        ddldept.DataValueField = "DeptCode";
        ddldept.DataBind();
    }

    private void Load_BankName()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtBankName.Items.Clear();
        query = "Select BankName from MstBank order by BankName Asc";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtBankName.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["BankName"] = "-Select-";
        dr["BankName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtBankName.DataTextField = "BankName";
        txtBankName.DataValueField = "BankName";
        txtBankName.DataBind();
    }

    private void Months_load()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlMonths.Items.Clear();
        query = "Select ID,Months from MonthDetails order by ID ASC";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlMonths.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["ID"] = "0";
        dr["Months"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlMonths.DataTextField = "Months";
        ddlMonths.DataValueField = "ID";
        ddlMonths.DataBind();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            string CmpName = "";
            string Cmpaddress = "";
            int YR = 0;
            if (ddlMonths.SelectedItem.Text == "January")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else if (ddlMonths.SelectedItem.Text == "February")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else if (ddlMonths.SelectedItem.Text == "March")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
            }

            if (ddlcategory.SelectedValue == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Category');", true);
                ErrFlag = true;
            }
            else if ((txtEmployeeType.SelectedValue == "") || (txtEmployeeType.SelectedValue == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Employee Type');", true);
                ErrFlag = true;
            }

            else if ((ddlMonths.SelectedValue == "0") || (ddlMonths.SelectedValue == ""))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Months');", true);
                ErrFlag = true;
            }


            string Other_State = "";
            string Non_Other_State = "";
            if (RdbOtherState.SelectedValue == "0")
            {
                Other_State = "";
                Non_Other_State = "";
            }
            else if (RdbOtherState.SelectedValue == "1")
            {
                Other_State = "Yes";
                Non_Other_State = "";
            }
            else if (RdbOtherState.SelectedValue == "2")
            {
                Other_State = "";
                Non_Other_State = "No";
            }
            else
            {
                Other_State = "";
                Non_Other_State = "";
            }
            //if (ChkOtherState.Checked == true)
            //{
            //    Other_State = "Yes";
            //}
            //else
            //{
            //    Other_State = "No";
            //}

            if (!ErrFlag)
            {
                //if (ddldept.SelectedValue == "0")
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Department Properly');", true);
                //    ErrFlag = true;
                //}
                if (!ErrFlag)
                {
                    if (ddlcategory.SelectedValue == "STAFF")
                    {
                        Stafflabour = "STAFF";
                    }
                    else if (ddlcategory.SelectedValue == "LABOUR")
                    {
                        Stafflabour = "LABOUR";
                    }
                    string constr = ConfigurationManager.AppSettings["ConnectionString"];
                    SqlConnection con = new SqlConnection(constr);
                    //foreach (GridViewRow gvsal in gvSalary.Rows)
                    //{
                    //    Label lblEmpNo = (Label)gvsal.FindControl("lblExist");
                    //    string qry = "Update SalaryDetails set Process_Mode='" + 1 + "' where Month='" + ddlMonths.SelectedValue + "' and FinancialYear='" + ddlFinance.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and ExisistingCode='" + lblEmpNo.Text + "'";
                    //    SqlCommand cmd = new SqlCommand(qry, con);
                    //    con.Open();
                    //    cmd.ExecuteNonQuery();
                    //    con.Close();
                    //}
                    string Emp_ESI_Code = "";//ddESI.SelectedValue.ToString();
                    string Payslip_Format_Type = "";
                    if (SessionUserType == "2")
                    {
                        Payslip_Format_Type = rdbPayslipIFFormat.SelectedValue.ToString();
                    }
                    else
                    {
                        Payslip_Format_Type = rdbPayslipFormat.SelectedValue.ToString();
                    }
                    string Str_PFType = "";
                    if (SessionUserType == "2")
                    {
                        Str_PFType = RdpIFPF.SelectedValue.ToString();
                    }
                    else
                    {
                        Str_PFType = RdbPFNonPF.SelectedValue.ToString();
                    }

                    string Str_ChkLeft = "";
                    //Str_ChkLeft = ChkLeft.SelectedValue.ToString();
                    if (ChkLeft.Checked == true)
                    {
                        Str_ChkLeft = "1";
                    }
                    else { Str_ChkLeft = "0"; }

                    string SalaryType = "";
                    //if (SessionUserType == "2")
                    //{
                    //    SalaryType = rbIFsalary.SelectedValue;
                    //}
                    //else
                    //{
                    //    SalaryType = rbsalary.SelectedValue;
                    //}

                    string ExemptedStaff = "";
                    if (chkExment.Checked == true)
                    {
                        ExemptedStaff = "1";
                    }

                    ResponseHelper.Redirect("ViewReport_Opt.aspx?Cate=" + Stafflabour + "&Depat=" + ddldept.SelectedValue + "&Months=" + ddlMonths.SelectedItem.Text + "&yr=" + ddlFinance.SelectedValue + "&fromdate=" + txtfrom.Text + "&ToDate=" + txtTo.Text + "&Salary=" + SalaryType + "&CashBank=" + RdbCashBank.SelectedValue + "&ESICode=" + Emp_ESI_Code + "&EmpTypeCd=" + txtEmployeeType.SelectedValue.ToString() + "&EmpType=" + txtEmployeeType.SelectedItem.Text.ToString() + "&PayslipType=" + Payslip_Format_Type.ToString() + "&PFTypePost=" + Str_PFType.ToString() + "&Left_Emp=" + Str_ChkLeft.ToString() + "&Leftdate=" + txtLeftDate.Text + "&Division=" + txtDivision.Text + "&Report_Type=Payslip" + "&OtherState=" + Other_State + "&ExemptedStaff=" + ExemptedStaff + "&NonOtherState=" + Non_Other_State, "_blank", "");

                }
            }
        }
        catch (Exception ex)
        {
        }
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        //if (rbsalary.SelectedValue == "2")
        //{
        if (ddlMonths.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Month Properly');", true);
            ErrFlag = true;
        }
        //}
        //else
        //{
        if (txtfrom.Text.Trim() == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Enter the FromDate Properly');", true);
            ErrFlag = true;
        }
        else if (txtTo.Text.Trim() == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Enter the ToDate Properly');", true);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            DateTime dfrom = Convert.ToDateTime(txtfrom.Text);
            DateTime dtto = Convert.ToDateTime(txtTo.Text);
            MyDate1 = DateTime.ParseExact(txtfrom.Text, "dd-MM-yyyy", null);
            MyDate2 = DateTime.ParseExact(txtTo.Text, "dd-MM-yyyy", null);
            if (dtto < dfrom)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Enter the From Date...');", true);
                txtfrom.Text = null;
                txtTo.Text = null;
                ErrFlag = true;
            }
        }
        //}




        if ((ddlcategory.SelectedValue == "") || (ddlcategory.SelectedValue == "-Select-"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Category Properly');", true);
            ErrFlag = true;
        }

        if ((txtEmployeeType.SelectedValue == "") || (txtEmployeeType.SelectedValue == "0"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Employee Type Properly');", true);
            ErrFlag = true;
        }

        //else if ((ddldept.SelectedValue == "") || (ddldept.SelectedValue == "0"))
        //{
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Type Properly');", true);
        //    ErrFlag = true;
        //}

        if (!ErrFlag)
        {
            string CmpName = "";
            string Cmpaddress = "";
            int YR = 0;
            string query = "";
            if (ddlMonths.SelectedItem.Text == "January")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else if (ddlMonths.SelectedItem.Text == "February")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else if (ddlMonths.SelectedItem.Text == "March")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
            }
            if (ddlcategory.SelectedValue == "STAFF")
            {
                Stafflabour = "STAFF";
            }
            else if (ddlcategory.SelectedValue == "LABOUR")
            {
                Stafflabour = "LABOUR";
            }

            Emp_ESI_Code = "";//ddESI.SelectedValue.ToString();

            if (Emp_ESI_Code == "--Select--") { Emp_ESI_Code = ""; }

            //if (rbsalary.SelectedValue == "2")
            //{
            //    query = "Select MstDpt.DepartmentNm,EmpDet.Designation,EmpDet.ExisistingCode,SalDet.EmpNo,EmpDet.EmpName,SalDet.WorkedDays, " +
            //            " SalDet.NFh,SalDet.CL,SalDet.Weekoff,SalDet.HomeDays,SalDet.Basic_SM as Base,SalDet.BasicandDA, " +
            //            " SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5," +
            //            " SalDet.GrossEarnings,SalDet.ProvidentFund,SalDet.ESI,SalDet.Advance,SalDet.Deduction1,SalDet.Deduction2, " +
            //            " SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.TotalDeductions,SalDet.OverTime,SalDet.RoundOffNetPay as NetPay, " +
            //            " (SalDet.WorkedDays + SalDet.NFh) as tot,SalDet.Words,SalDet.Month,SalDet.Year,SalDet.LeaveDays, " +
            //            " sum(SalDet.RoundOffNetPay - SalDet.NetPay) as Roundoff, " +
            //            " SalDet.DayIncentive,SalDet.BasicAndDANew,SalDet.BasicHRA,SalDet.ConvAllow,SalDet.EduAllow,SalDet.MediAllow,SalDet.BasicRAI,SalDet.WashingAllow," +
            //            " SalDet.FullNightAmt,AttnDet.ThreeSided,SalDet.ThreesidedAmt,SalDet.OTHoursNew,SalDet.OTHoursAmtNew," +
            //            " SalDet.LOPDays,SalDet.Losspay,SalDet.Leave_Credit_Days as LeaveCredit,SalDet.WH_Work_Days " +

            //            " from EmployeeDetails EmpDet inner Join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo " +
            //            " inner Join AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
            //            " inner Join MstDepartment as MstDpt on MstDpt.DepartmentCd = EmpDet.Department inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo inner Join SalaryMaster SM on SM.EmpNo=SalDet.EmpNo where " +
            //            " SalDet.Month='" + ddlMonths.SelectedValue + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.StafforLabor='" + Stafflabour + "' and " +
            //            " EmpDet.Ccode='" + SessionCcode + "' and EmpDet.Lcode='" + SessionLcode + "' and SalDet.WagesType='" + rbsalary.SelectedValue + "' And EmpDet.EmployeeType='" + txtEmployeeType.SelectedValue.ToString() + "'";

            //    if (RdbCashBank.SelectedValue.ToString() != "0") { query = query + " and SalDet.Salarythrough='" + RdbCashBank.SelectedValue + "'"; }
            //    if (ddldept.SelectedValue != "0") { query = query + " and EmpDet.Department='" + ddldept.SelectedValue + "'"; }

            //    //PF Check
            //    if (RdbPFNonPF.SelectedValue.ToString() != "0")
            //    {
            //        if (RdbPFNonPF.SelectedValue.ToString() == "1")
            //        {
            //            //PF Employee
            //            query = query + " and (OP.EligiblePF='1')";
            //        }
            //        else
            //        {
            //            //Non PF Employee
            //            query = query + " and (OP.EligiblePF='2')";
            //        }
            //    }




            //    if (txtDivision.Text.ToString() != "" && txtDivision.Text.ToString() != "-----Select----")
            //    {
            //        query = query + " and EmpDet.Division='" + txtDivision.Text.ToString() + "'";
            //    }


            //    query = query + " group by MstDpt.DepartmentNm,EmpDet.Designation,EmpDet.ExisistingCode,SalDet.EmpNo,EmpDet.EmpName,SalDet.WorkedDays, " +
            //            " SalDet.NFh,SalDet.CL,SalDet.Weekoff,SalDet.HomeDays,SalDet.Basic_SM,SalDet.BasicandDA, " +
            //            " SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5," +
            //            " SalDet.GrossEarnings,SalDet.ProvidentFund,SalDet.ESI,SalDet.Advance,SalDet.Deduction1,SalDet.Deduction2, " +
            //            " SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.TotalDeductions,SalDet.OverTime,SalDet.RoundOffNetPay, " +
            //            " SalDet.Words,SalDet.Month,SalDet.Year,SalDet.LeaveDays, " +
            //            " SalDet.DayIncentive,SalDet.BasicAndDANew,SalDet.BasicHRA,SalDet.ConvAllow,SalDet.EduAllow," +
            //            " SalDet.MediAllow,SalDet.BasicRAI,SalDet.WashingAllow,SalDet.FullNightAmt,AttnDet.ThreeSided,SalDet.ThreesidedAmt, " +
            //            " SalDet.OTHoursNew,SalDet.OTHoursAmtNew,SalDet.LOPDays,SalDet.Losspay,SalDet.Leave_Credit_Days,SalDet.WH_Work_Days" +
            //            " Order by EmpDet.ExisistingCode,MstDpt.DepartmentNm Asc";

            //    if (txtEmployeeType.SelectedValue == "5")
            //    {
            //        query = "Select SalDet.EmpNo,MstDpt.DepartmentNm,EmpDet.Designation,EmpDet.ExisistingCode,EmpDet.EmpName,SalDet.WorkedDays,EmpDet.EmployeeType, " +
            //            " SalDet.NFh,SalDet.CL,SalDet.Weekoff,SalDet.Basic_SM as Base,SalDet.BasicandDA,(SalDet.WorkedDays + SalDet.NFh) as tot, " +
            //            " SalDet.Words,SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.GrossEarnings, " +
            //            " SalDet.ProvidentFund,SalDet.ESI,SalDet.Advance,SalDet.Deduction1,SalDet.Deduction2,SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5, " +
            //            " SalDet.TotalDeductions,SalDet.RoundOffNetPay as NetPay,sum(SalDet.RoundOffNetPay - SalDet.NetPay) as Roundoff,SalDet.FullNightAmt," +
            //            " SalDet.DayIncentive,SalDet.Leave_Credit_Days as LeaveCredit,SalDet.OTHoursNew,SalDet.OTHoursAmtNew," +
            //            " cast((cast(SalDet.Basic_SM as decimal(18,2)) / 8) as decimal(18,2)) as OTFixed from EmployeeDetails EmpDet " +
            //            " inner Join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo " +
            //            " inner Join MstDepartment as MstDpt on MstDpt.DepartmentCd = EmpDet.Department " +
            //            " inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo " +
            //            " inner Join SalaryMaster SM on SM.EmpNo=SalDet.EmpNo where " +
            //            " SalDet.Month='" + ddlMonths.SelectedValue + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.StafforLabor='" + Stafflabour + "' and " +
            //            " EmpDet.Ccode='" + SessionCcode + "' and EmpDet.Lcode='" + SessionLcode + "' and SalDet.WagesType='" + rbsalary.SelectedValue + "' and EmpDet.EmployeeType='" + txtEmployeeType.SelectedValue + "'";

            //        if (RdbCashBank.SelectedValue.ToString() != "0") { query = query + " and SalDet.Salarythrough='" + RdbCashBank.SelectedValue + "'"; }
            //        if (ddldept.SelectedValue != "0") { query = query + " and EmpDet.Department='" + ddldept.SelectedValue + "'"; }

            //        //PF Check
            //        if (RdbPFNonPF.SelectedValue.ToString() != "0")
            //        {
            //            if (RdbPFNonPF.SelectedValue.ToString() == "1")
            //            {
            //                //PF Employee
            //                query = query + " and (OP.EligiblePF='1')";
            //            }
            //            else
            //            {
            //                //Non PF Employee
            //                query = query + " and (OP.EligiblePF='2')";
            //            }
            //        }




            //        if (txtDivision.Text.ToString() != "" && txtDivision.Text.ToString() != "-----Select----")
            //        {
            //            query = query + " and EmpDet.Division='" + txtDivision.Text.ToString() + "'";
            //        }

            //        query = query + " group by SalDet.EmpNo,MstDpt.DepartmentNm,EmpDet.Designation,EmpDet.ExisistingCode,EmpDet.EmpName,SalDet.WorkedDays,EmpDet.EmployeeType, " +
            //                " SalDet.NFh,SalDet.CL,SalDet.Weekoff,SalDet.Basic_SM,SalDet.BasicandDA, " +
            //                " SalDet.Words,SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.GrossEarnings, " +
            //                " SalDet.ProvidentFund,SalDet.ESI,SalDet.Advance,SalDet.Deduction1,SalDet.Deduction2,SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5, " +
            //                " SalDet.TotalDeductions,SalDet.RoundOffNetPay,SalDet.FullNightAmt,SalDet.DayIncentive,SalDet.Leave_Credit_Days,SalDet.OTHoursNew,SalDet.OTHoursAmtNew Order by EmpDet.ExisistingCode,MstDpt.DepartmentNm Asc";
            //    }

            //}
            //else
            //{
            query = "Select SalDet.EmpNo,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName,EmpDet.Designation,EmpDet.DeptName as Department,SalDet.Totalworkingdays, " +
                        " SalDet.NFh,SalDet.BasicandDA,(SalDet.BasicandDA - SalDet.Employee_ESI_IF) as NetSalary,SalDet.FDA,SalDet.VDA,SalDet.HRA, SalDet.Advance,SalDet.Stamp,SalDet.Unioncg,(SalDet.WorkedDays + SalDet.NFh) as tot, " +
                        " SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.LeaveDays,SalDet.SalaryDate,SalDet.allowances1," +
                        " SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.Deduction1,SalDet.Weekoff,SalDet.CL,SalDet.Deduction2, " +
                        " SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.Deduction5,SalDet.HomeDays,SalDet.Losspay,SalDet.RoundOffNetPay_IF as NetPay,SalDet.Total_Deduction_IF as TotalDeductions,SalDet.GrossEarnings,EmpDet.EmpNo as OldID,MstDpt.DeptName as DepartmentNm,EmpDet.BaseSalary as Base, " + //SM.PFS,  " +
                        " SalDet.OverTime,MstDpt.DeptName as DepartmentNm,SalDet.ProvidentFund,SalDet.Employee_ESI_IF as ESI,SalDet.HRA,SalDet.WorkedDays,sum(SalDet.RoundOffNetPay_IF - SalDet.NetPay) as Roundoff," +
                        " SalDet.PfSalary as PFEarnings,SalDet.HalfNightAmt,SalDet.FullNightAmt,SalDet.SpinningAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt,SalDet.FullNightAmt,SalDet.Leave_Credit_Days as LeaveCredit from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo " +
                        " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode where " +
                        " SalDet.Month='" + ddlMonths.SelectedItem.Text + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.CatName='" + Stafflabour + "' and " +
                //" EmpDet.Ccode='" + SessionCcode + "' and EmpDet.Lcode='" + SessionLcode + "' and SalDet.WagesType='" + rbsalary.SelectedValue + "' and SalDet.FromDate = convert(datetime,'" + MyDate1 + "', 105) and SalDet.ToDate = Convert(Datetime,'" + MyDate2 + "', 105)";
                        " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Wages='" + txtEmployeeType.SelectedItem.Text + "' and SalDet.FromDate = convert(datetime,'" + MyDate1 + "', 105) and SalDet.ToDate = Convert(Datetime,'" + MyDate2 + "', 105)";

            if (RdbCashBank.SelectedValue.ToString() != "0") { query = query + " and SalDet.Salarythrough='" + RdbCashBank.SelectedValue + "'"; }
            if (ddldept.SelectedValue != "0") { query = query + " and EmpDet.DeptCode='" + ddldept.SelectedValue + "'"; }

            //PF Check
            if (RdbPFNonPF.SelectedValue.ToString() != "0")
            {
                if (RdbPFNonPF.SelectedValue.ToString() == "1")
                {
                    //PF Employee
                    query = query + " and (EmpDet.Eligible_PF='1')";
                }
                else
                {
                    //Non PF Employee
                    query = query + " and (EmpDet.Eligible_PF='2')";
                }
            }




            if (txtDivision.Text.ToString() != "" && txtDivision.Text.ToString() != "-Select-")
            {
                query = query + " and EmpDet.Division='" + txtDivision.Text.ToString() + "'";
            }

            query = query + " group by SalDet.EmpNo,EmpDet.ExistingCode,EmpDet.FirstName,EmpDet.Designation,EmpDet.DeptName,SalDet.Totalworkingdays, " +
                    " SalDet.NFh,SalDet.BasicandDA,SalDet.FDA,SalDet.VDA,SalDet.HRA, SalDet.Advance,SalDet.Stamp,SalDet.Unioncg, " +
                    " SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.LeaveDays,SalDet.SalaryDate,SalDet.allowances1, " +
                    " SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.Deduction1,SalDet.Weekoff,SalDet.CL,SalDet.Deduction2, " +
                    " SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.Deduction5,SalDet.HomeDays,SalDet.Losspay,SalDet.RoundOffNetPay_IF as RoundOffNetPay,SalDet.Total_Deduction_IF,SalDet.GrossEarnings,EmpDet.EmpNo,MstDpt.DeptName,EmpDet.BaseSalary, " + //SM.PFS,  " +
                    " SalDet.OverTime,SalDet.ProvidentFund,SalDet.Employee_ESI_IF,SalDet.HRA,SalDet.WorkedDays,sum(SalDet.NetPay - (SalDet.GrossEarnings - SalDet.Total_Deduction_IF)) as Roundoff, " +
                    " SalDet.PfSalary as PFEarnings,SalDet.HalfNightAmt,SalDet.FullNightAmt,SalDet.SpinningAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt,SalDet.FullNightAmt,SalDet.Leave_Credit_Days " +
                    " Order by EmpDet.ExistingCode,MstDpt.DeptName Asc";

            if (txtEmployeeType.SelectedValue == "5")
            {
                query = "Select SalDet.EmpNo,MstDpt.DeptName as DepartmentNm,EmpDet.Designation,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName,SalDet.WorkedDays,EmpDet.Wages as EmployeeType, " +
                    " SalDet.NFh,SalDet.CL,SalDet.Weekoff,SalDet.Basic_SM as Base,SalDet.BasicandDA,(SalDet.WorkedDays + SalDet.NFh) as tot, " +
                    " SalDet.Words,SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.GrossEarnings, " +
                    " SalDet.ProvidentFund,SalDet.Employee_ESI_IF as ESI,SalDet.Advance,SalDet.Deduction1,SalDet.Deduction2,SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5, " +
                    " SalDet.Total_Deduction_IF as TotalDeductions,SalDet.RoundOffNetPay_IF as NetPay,sum(SalDet.RoundOffNetPay_IF - SalDet.NetPay) as Roundoff,SalDet.FullNightAmt," +
                    " SalDet.DayIncentive,SalDet.Leave_Credit_Days as LeaveCredit,SalDet.OTHoursNew,SalDet.OTHoursAmtNew," +
                    " cast((cast(SalDet.Basic_SM as decimal(18,2)) / 8) as decimal(18,2)) as OTFixed from Employee_Mst EmpDet " +
                    " inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo " +
                    " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode " +
                    //" inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo " +
                    //" inner Join SalaryMaster SM on SM.EmpNo=SalDet.EmpNo where " +
                    " where SalDet.Month='" + ddlMonths.SelectedItem.Text + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.CatName='" + Stafflabour + "' and " +
                    " SalDet.FromDate = convert(datetime,'" + MyDate1 + "', 105) and SalDet.ToDate = Convert(Datetime,'" + MyDate2 + "', 105) and " +
                    " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Wages='" + txtEmployeeType.SelectedItem.Text + "'";


                if (RdbCashBank.SelectedValue.ToString() != "0") { query = query + " and SalDet.Salarythrough='" + RdbCashBank.SelectedValue + "'"; }
                if (ddldept.SelectedValue != "0") { query = query + " and EmpDet.DeptCode='" + ddldept.SelectedValue + "'"; }

                //PF Check
                if (RdbPFNonPF.SelectedValue.ToString() != "0")
                {
                    if (RdbPFNonPF.SelectedValue.ToString() == "1")
                    {
                        //PF Employee
                        query = query + " and (EmpDet.Eligible_PF='1')";
                    }
                    else
                    {
                        //Non PF Employee
                        query = query + " and (EmpDet.Eligible_PF='2')";
                    }
                }



                if (txtDivision.Text.ToString() != "" && txtDivision.Text.ToString() != "-Select-")
                {
                    query = query + " and EmpDet.Division='" + txtDivision.Text.ToString() + "'";
                }

                query = query + " group by SalDet.EmpNo,MstDpt.DeptName,EmpDet.Designation,EmpDet.ExistingCode,EmpDet.FirstName,SalDet.WorkedDays,EmpDet.Wages, " +
                        " SalDet.NFh,SalDet.CL,SalDet.Weekoff,SalDet.Basic_SM,SalDet.BasicandDA, " +
                        " SalDet.Words,SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.GrossEarnings, " +
                        " SalDet.ProvidentFund,SalDet.Employee_ESI_IF,SalDet.Advance,SalDet.Deduction1,SalDet.Deduction2,SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5, " +
                        " SalDet.Total_Deduction_IF,SalDet.RoundOffNetPay_IF,SalDet.FullNightAmt,SalDet.DayIncentive,SalDet.Leave_Credit_Days,SalDet.OTHoursNew,SalDet.OTHoursAmtNew Order by EmpDet.ExistingCode,MstDpt.DeptName Asc";
            }

            //}
            DataTable dt_1 = new DataTable();
            dt_1 = objdata.RptEmployeeMultipleDetails(query);


            //SqlCommand cmd = new SqlCommand(query, con);
            //DataTable dt_1 = new DataTable();
            //SqlDataAdapter sda = new SqlDataAdapter(cmd);
            ////DataSet ds1 = new DataSet();
            //con.Open();
            //sda.Fill(dt_1);
            //con.Close();

            //Leave Credit Update
            for (int k = 0; k < dt_1.Rows.Count; k++)
            {
                //string EmpNo_LC = "";

                //DataTable LC_DT = new DataTable();
                //EmpNo_LC = dt_1.Rows[k]["EmpNo"].ToString();
                //query = "Select * from SalaryDetails where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And EmpNo='" + EmpNo_LC + "' And FinancialYear='" + ddlFinance.SelectedValue + "' And Month='" + + "'";
                //LC_DT = objdata.RptEmployeeMultipleDetails(query);
                //if (LC_DT.Rows.Count != 0)
                //{
                //    dt_1.Rows[k]["LeaveCredit"] = LC_DT.Rows[0]["LeaveCredit"].ToString();
                //}
                //else
                //{
                //    dt_1.Rows[k]["LeaveCredit"] = "0";
                //}

                //NetPayGrandTotal
                NetPay_Grand_Total = (Convert.ToDecimal(NetPay_Grand_Total) + Convert.ToDecimal(dt_1.Rows[k]["NetPay"].ToString())).ToString();
            }
            Decimal NetGrTot = Convert.ToDecimal(NetPay_Grand_Total.ToString());
            NetPay_Grand_Total_Words = NumerictoNumber(Convert.ToInt32(NetGrTot), isUK).ToString() + " " + "Only";

            if (txtEmployeeType.SelectedValue == "1" || txtEmployeeType.SelectedValue == "6")
            {
                GVStaff.DataSource = dt_1;
                GVStaff.DataBind();
            }
            else if (txtEmployeeType.SelectedValue == "2")
            {
                GVSub_Staff.DataSource = dt_1;
                GVSub_Staff.DataBind();
            }
            else if (txtEmployeeType.SelectedValue == "3")
            {
                GVRegular.DataSource = dt_1;
                GVRegular.DataBind();
            }
            else if (txtEmployeeType.SelectedValue == "4")
            {
                GVHostel.DataSource = dt_1;
                GVHostel.DataBind();
            }
            else if (txtEmployeeType.SelectedValue == "5")
            {
                GVCivil.DataSource = dt_1;
                GVCivil.DataBind();
            }
            else
            {
                gvSalary.DataSource = dt_1;
                gvSalary.DataBind();
            }
            //if (txtEmployeeType.SelectedValue == "4")
            //{
            //    foreach (GridViewRow gvsal in GVHostel.Rows)
            //    {
            //        Label lbl_EmpNo = (Label)gvsal.FindControl("lblEmpNo");
            //        Label lbl_OTHours = (Label)gvsal.FindControl("lblOThours");
            //        string qry_ot = "";
            //        if (rbsalary.SelectedValue == "2")
            //        {
            //            qry_ot = "Select NoHrs from OverTime where EmpNo='" + lbl_EmpNo.Text.Trim() + "' and Lcode='" + SessionLcode + "' and Ccode='" + SessionCcode + "' and Month='" + ddlMonths.SelectedValue + "' and Financialyr='" + ddlFinance.SelectedValue + "'";
            //        }
            //        else
            //        {
            //            qry_ot = "Select NoHrs from OverTime where EmpNo='" + lbl_EmpNo.Text.Trim() + "' and Lcode='" + SessionLcode + "' and Ccode='" + SessionCcode + "' and Month='" + ddlMonths.SelectedValue + "' and Financialyr='" + ddlFinance.SelectedValue + "'" +
            //                            " and convert(Datetime, FromDate, 105)=convert(Datetime,'" + MyDate1 + "', 105) and convert(Datetime, ToDate, 105)=convert(datetime,'" + MyDate2 + "', 105)";

            //        }
            //        SqlCommand cmd_ot = new SqlCommand(qry_ot, con);
            //        con.Open();
            //        string val_ot = Convert.ToString(cmd_ot.ExecuteScalar());
            //        con.Close();
            //        if (val_ot.Trim() == "")
            //        {
            //            lbl_OTHours.Text = "0";
            //        }
            //        else
            //        {
            //            lbl_OTHours.Text = val_ot;
            //        }
            //    }
            //}
            string attachment = "attachment;filename=Payslip.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            DataTable dt = new DataTable();
            query = "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dt = objdata.RptEmployeeMultipleDetails(query);
            if (dt.Rows.Count > 0)
            {
                CmpName = dt.Rows[0]["Cname"].ToString();
                Cmpaddress = (dt.Rows[0]["Address1"].ToString() + ", " + dt.Rows[0]["Address2"].ToString() + "-" + dt.Rows[0]["Pincode"].ToString());
            }
            NetBase = "0";
            NetFDA = "0";
            NetVDA = "0";
            Nettotal = "0";
            NetPFEarnings = "0";
            NetPF = "0";
            NetESI = "0";
            NetUnion = "0";
            NetAdvance = "0";
            NetAll1 = "0";
            NetAll2 = "0";
            NetAll3 = "0";
            NetAll4 = "0";
            NetDed1 = "0";
            NetDed2 = "0";
            NetDed3 = "0";
            NetDed4 = "0";
            NetAll5 = "0";
            NetDed5 = "0";
            NetLOP = "0";
            NetStamp = "0";
            NetTotalDeduction = "0";
            NetOT = "0";
            NetAmt = "0";
            Network = "0";
            totCL = "0";
            totNFh = "0";
            totweekoff = "0";
            Roundoff = "0";
            Fixedsal = "0";
            FixedOT = "0";
            Tot_OThr = "0";
            //HomeDays = "0";
            //Halfnight = "0";
            //FullNight = "0";
            //Spinning = "0";
            //DayIncentive = "0";
            //ThreeSided = "0";
            //totwork = "0";

            StringWriter stw = new StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);

            if (txtEmployeeType.SelectedValue == "1" || txtEmployeeType.SelectedValue == "6")
            {
                GVStaff.RenderControl(htextw);
            }
            else if (txtEmployeeType.SelectedValue == "2")
            {
                GVSub_Staff.RenderControl(htextw);
            }
            else if (txtEmployeeType.SelectedValue == "3")
            {
                GVRegular.RenderControl(htextw);
            }
            else if (txtEmployeeType.SelectedValue == "4")
            {
                GVHostel.RenderControl(htextw);
            }
            else if (txtEmployeeType.SelectedValue == "5")
            {
                GVCivil.RenderControl(htextw);
            }
            else
            {
                gvSalary.RenderControl(htextw);
            }

            Response.Write("<table>");
            Response.Write("<tr align='Center'>");
            Response.Write("<td colspan='19'>");
            Response.Write("" + CmpName + " - PAYSLIP REPORT");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr align='Center'>");
            Response.Write("<td colspan='19'>");
            Response.Write("" + SessionLcode + "");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr align='Center'>");
            Response.Write("<td colspan='19'>");
            Response.Write("" + Cmpaddress + "");
            Response.Write("</td>");
            Response.Write("</tr>");
            string Salary_Head = "";
            //Salary Head
            if (txtEmployeeType.SelectedValue == "1" || txtEmployeeType.SelectedValue == "2" || txtEmployeeType.SelectedValue == "6")
            {
                Salary_Head = "SALARY FOR THE MONTH OF " + ddlMonths.SelectedValue.ToUpper() + " - " + YR;
            }
            if (txtEmployeeType.SelectedValue == "3")
            {
                Salary_Head = "WAGES WORKERS CHECK REPORT FOR THE MONTH OF " + ddlMonths.SelectedValue.ToUpper() + " - " + YR;
            }
            if (txtEmployeeType.SelectedValue == "4")
            {
                Salary_Head = "WAGES / CASUAL WORKERS CHECK REPORT FOR THE MONTH OF " + ddlMonths.SelectedValue.ToUpper() + " - " + YR;
            }
            if (txtEmployeeType.SelectedValue == "5")
            {
                Salary_Head = "WAGES WORKERS CHECK REPORT FOR THE MONTH OF " + txtfrom.Text + " - " + txtTo.Text;
            }


            //if (rbsalary.SelectedValue == "2")
            //{
            //    Response.Write("<tr align='Center'>");
            //    Response.Write("<td colspan='19'>");
            //    //Response.Write("Salary Month of " + ddlMonths.SelectedValue + " - " + YR);
            //    Response.Write(Salary_Head);
            //    Response.Write("</td>");
            //    Response.Write("</tr>");
            //}
            //else
            //{
            Response.Write("<tr align='Center'>");
            Response.Write("<td colspan='19'>");
            //Response.Write("Salary Month of " + txtfrom.Text + " - " + txtTo.Text);
            Response.Write(Salary_Head);
            Response.Write("</td>");
            Response.Write("</tr>");
            //}
            Response.Write("</table>");

            //gvSalary.RenderControl(htextw);
            //Response.Write("Contract Details");
            Response.Write(stw.ToString());

            Response.Write("<table border='1'>");
            Response.Write("<tr Font-Bold='true'>");
            Response.Write("<td font-Bold='true' align='right' colspan='5'>");
            Response.Write("Grand Total");
            Response.Write("</td>");

            Int32 Grand_Tot_End;

            if (txtEmployeeType.SelectedValue == "1" || txtEmployeeType.SelectedValue == "6")
            {
                Grand_Tot_End = GVStaff.Rows.Count + 5;
            }
            else if (txtEmployeeType.SelectedValue == "2")
            {
                Grand_Tot_End = GVSub_Staff.Rows.Count + 5;
            }
            else if (txtEmployeeType.SelectedValue == "3")
            {
                Grand_Tot_End = GVRegular.Rows.Count + 5;
            }
            else if (txtEmployeeType.SelectedValue == "4")
            {
                Grand_Tot_End = GVHostel.Rows.Count + 5;
            }
            else if (txtEmployeeType.SelectedValue == "5")
            {
                Grand_Tot_End = GVCivil.Rows.Count + 5;
            }
            else
            {
                Grand_Tot_End = gvSalary.Rows.Count + 5;
            }

            Response.Write("<td>=sum(F6:F" + Grand_Tot_End.ToString() + ")</td>");
            Response.Write("<td>=sum(G6:G" + Grand_Tot_End.ToString() + ")</td>");
            Response.Write("<td>=sum(H6:H" + Grand_Tot_End.ToString() + ")</td>");
            Response.Write("<td>=sum(I6:I" + Grand_Tot_End.ToString() + ")</td>");
            Response.Write("<td>=sum(J6:J" + Grand_Tot_End.ToString() + ")</td>");
            Response.Write("<td>=sum(K6:K" + Grand_Tot_End.ToString() + ")</td>");
            Response.Write("<td>=sum(L6:L" + Grand_Tot_End.ToString() + ")</td>");
            Response.Write("<td>=sum(M6:M" + Grand_Tot_End.ToString() + ")</td>");
            Response.Write("<td>=sum(N6:N" + Grand_Tot_End.ToString() + ")</td>");
            Response.Write("<td>=sum(O6:O" + Grand_Tot_End.ToString() + ")</td>");
            Response.Write("<td>=sum(P6:P" + Grand_Tot_End.ToString() + ")</td>");
            Response.Write("<td>=sum(Q6:Q" + Grand_Tot_End.ToString() + ")</td>");
            Response.Write("<td>=sum(R6:R" + Grand_Tot_End.ToString() + ")</td>");
            Response.Write("<td>=sum(S6:S" + Grand_Tot_End.ToString() + ")</td>");
            Response.Write("<td>=sum(T6:T" + Grand_Tot_End.ToString() + ")</td>");
            Response.Write("<td>=sum(U6:U" + Grand_Tot_End.ToString() + ")</td>");
            Response.Write("<td>=sum(V6:V" + Grand_Tot_End.ToString() + ")</td>");
            Response.Write("<td>=sum(W6:W" + Grand_Tot_End.ToString() + ")</td>");
            Response.Write("<td>=sum(X6:X" + Grand_Tot_End.ToString() + ")</td>");
            Response.Write("<td>=sum(Y6:Y" + Grand_Tot_End.ToString() + ")</td>");
            Response.Write("<td>=sum(Z6:Z" + Grand_Tot_End.ToString() + ")</td>");
            Response.Write("<td>=sum(AA6:AA" + Grand_Tot_End.ToString() + ")</td>");
            Response.Write("<td>=sum(AB6:AB" + Grand_Tot_End.ToString() + ")</td>");
            if (txtEmployeeType.SelectedValue != "5")
            {
                Response.Write("<td>=sum(AC6:AC" + Grand_Tot_End.ToString() + ")</td>");
                Response.Write("<td>=sum(AD6:AD" + Grand_Tot_End.ToString() + ")</td>");
                Response.Write("<td>=sum(AE6:AE" + Grand_Tot_End.ToString() + ")</td>");
                Response.Write("<td>=sum(AF6:AF" + Grand_Tot_End.ToString() + ")</td>");
                Response.Write("<td>=sum(AG6:AG" + Grand_Tot_End.ToString() + ")</td>");
                Response.Write("<td>=sum(AH6:AH" + Grand_Tot_End.ToString() + ")</td>");
            }
            if (txtEmployeeType.SelectedValue == "3")
            {

                Response.Write("<td>=sum(AI6:AI" + Grand_Tot_End.ToString() + ")</td>");
                Response.Write("<td>=sum(AJ6:AJ" + Grand_Tot_End.ToString() + ")</td>");
                Response.Write("<td>=sum(AK6:AK" + Grand_Tot_End.ToString() + ")</td>");
            }
            if (txtEmployeeType.SelectedValue == "1" || txtEmployeeType.SelectedValue == "6")
            {

                Response.Write("<td>=sum(AI6:AI" + Grand_Tot_End.ToString() + ")</td>");
                Response.Write("<td>=sum(AJ6:AJ" + Grand_Tot_End.ToString() + ")</td>");
                Response.Write("<td>=sum(AK6:AK" + Grand_Tot_End.ToString() + ")</td>");
                Response.Write("<td>=sum(AL6:AL" + Grand_Tot_End.ToString() + ")</td>");

            }
            if (txtEmployeeType.SelectedValue == "2")
            {

                Response.Write("<td>=sum(AI6:AI" + Grand_Tot_End.ToString() + ")</td>");
                Response.Write("<td>=sum(AJ6:AJ" + Grand_Tot_End.ToString() + ")</td>");
                Response.Write("<td>=sum(AK6:AK" + Grand_Tot_End.ToString() + ")</td>");
                Response.Write("<td>=sum(AL6:AL" + Grand_Tot_End.ToString() + ")</td>");
                Response.Write("<td>=sum(AM6:AM" + Grand_Tot_End.ToString() + ")</td>");
            }
            if (txtEmployeeType.SelectedValue == "4")
            {

                Response.Write("<td>=sum(AI6:AI" + Grand_Tot_End.ToString() + ")</td>");
                Response.Write("<td>=sum(AJ6:AJ" + Grand_Tot_End.ToString() + ")</td>");
                //Response.Write("<td>=sum(AJ6:AJ" + Grand_Tot_End.ToString() + ")</td>");
            }
            Response.Write("</tr></table>");

            //NetPay Grand Total In Words Add
            Response.Write("<table style='font-weight:bold;'><tr><td></td></tr>");
            Response.Write("<tr style='font-weight:bold;'>");
            Response.Write("<td></td><td colspan='11' align='left'>Rupess : " + NetPay_Grand_Total_Words + "</td>");
            Response.Write("</tr>");
            Response.Write("<tr><td></td></tr>");
            Response.Write("<tr><td></td></tr>");

            //Management Sign Area
            Response.Write("<tr style='font-weight:bold;'>");

            Response.Write("<td colspan='3' align='center'>PREPARED BY</td>");
            Response.Write("<td colspan='3' align='center'>CHECKED BY</td>");
            Response.Write("<td colspan='3' align='center'>AM</td>");
            Response.Write("<td colspan='3' align='center'>HRM</td>");
            Response.Write("<td colspan='3' align='center'>UNIT HEAD</td>");
            Response.Write("<td colspan='3' align='center'>GM</td>");
            Response.Write("<td colspan='3' align='center'>CGM</td>");
            Response.Write("<td colspan='3' align='center'>VP</td>");

            Response.Write("</tr>");
            Response.Write("</table>");


            Response.End();
            Response.Clear();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Downloaded Successfully');", true);
        }
    }

    public string NumerictoNumber(int Number, bool isUK)
    {
        if (Number == 0)
            return "Zero";

        string and = isUK ? "and " : "";
        if (Number == -2147483648)
            return "Minus Two Billion One Hundred " + and +
        "Forty Seven Million Four Hundred " + and + "Eighty Three Thousand " +
        "Six Hundred " + and + "Forty Eight";

        int[] num = new int[4];
        int first = 0;
        int u, h, t = 0;
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        if (Number < 0)
        {
            sb.Append("Minus");
            Number = -Number;
        }

        string[] words0 = { "", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine" };
        string[] words1 = { "Ten ", "Eleven ", "Twelve ", "Thirteen ", "Fourteen ", "Fifteen ", "Sixteen ", "Seventeen ", "Eighteen ", "Nineteen " };
        string[] words2 = { "Twenty ", "Thirty ", "Forty ", "Fifty ", "Sixty ", "Seventy ", "Eighty ", "Ninety " };
        string[] words3 = { "Thousand ", "Lak", "Crore", "Million ", "Billion " };
        num[0] = Number % 1000;
        num[1] = Number / 1000;
        num[2] = Number / 1000000;
        num[1] = num[1] - 1000 * num[2];  // thousands
        num[2] = num[2] - 100 * num[3];//laks
        num[3] = Number / 10000000;     // billions
        num[2] = num[2] - 1000 * num[3];  // millions

        for (int i = 3; i > 0; i--)
        {
            if (num[i] != 0)
            {
                first = i;
                break;
            }
        }
        for (int i = first; i >= 0; i--)
        {
            if (num[i] == 0) continue;
            u = num[i] % 10;
            t = num[i] / 10;
            h = num[i] / 100;
            t = t - 10 * h;

            if (h > 0)
                sb.Append(words0[h] + " Hundred ");
            if (u > 0 || t > 0)
            {
                if (h > 0 || i < first)
                    sb.Append(and);

                if (t == 0)
                    sb.Append(words0[u]);
                else if (t == 1)
                    sb.Append(words1[u]);
                else
                    sb.Append(words2[t - 2] + words0[u]);

            }

            if (i != 0)
                sb.Append(words3[i - 1]);

        }

        return sb.ToString().TrimEnd();



    }
    protected void gvSalary_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void btnBankSalary_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        //if (SessionUserType == "2")
        //{
        //    //rbsalary.SelectedValue = rbIFsalary.SelectedValue;
        //    //if (rbIFsalary.SelectedValue == "2")
        //    //{
        //        if (ddlMonths.SelectedValue == "0")
        //        {
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Month Properly');", true);
        //            ErrFlag = true;
        //        }
        //    //}
        //}
        //else
        //{
        //    //if (rbsalary.SelectedValue == "2")
        //    //{
        //    //    if (ddlMonths.SelectedValue == "")
        //    //    {
        //    //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Month Properly');", true);
        //    //        ErrFlag = true;
        //    //    }
        //    //}
        //    //else
        //    //{
        //if (txtfrom.Text.Trim() == "")
        //{
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Month Properly');", true);
        //    ErrFlag = true;
        //}
        //else if (txtTo.Text.Trim() == "")
        //{
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Month Properly');", true);
        //    ErrFlag = true;
        //}
        //if (!ErrFlag)
        //{
        //    DateTime dfrom = Convert.ToDateTime(txtfrom.Text);
        //    DateTime dtto = Convert.ToDateTime(txtTo.Text);
        //    MyDate1 = DateTime.ParseExact(txtfrom.Text, "dd/MM/yyyy", null);
        //    MyDate2 = DateTime.ParseExact(txtTo.Text, "dd/MM/yyyy", null);
        //    if (dtto < dfrom)
        //    {
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Enter the From Date...');", true);
        //        txtfrom.Text = null;
        //        txtTo.Text = null;
        //        ErrFlag = true;
        //    }
        //}
        //    //}
        //}
        if (!ErrFlag)
        {
            string CmpName = "";
            string Cmpaddress = "";
            int YR = 0;
            string query = "";
            if (ddlMonths.SelectedItem.Text == "January")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else if (ddlMonths.SelectedItem.Text == "February")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else if (ddlMonths.SelectedItem.Text == "March")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
            }
            Stafflabour = "";
            if (ddlcategory.SelectedValue == "STAFF")
            {
                Stafflabour = "STAFF";
            }
            else if (ddlcategory.SelectedValue == "LABOUR")
            {
                Stafflabour = "LABOUR";
            }

            Emp_ESI_Code = "";//ddESI.SelectedValue.ToString();

            if (Emp_ESI_Code == "--Select--") { Emp_ESI_Code = ""; }
            string PFTypeGet = RdbPFNonPF.SelectedValue.ToString();
            string Left_Employee = ""; //ChkLeft.SelectedValue.ToString();

            if (ChkLeft.Checked == true)
            {
                Left_Employee = "1";
            }
            else
            {
                Left_Employee = "0";
            }






            string Left_Date = "";
            Left_Date = txtLeftDate.Text.ToString();
            //if (rbsalary.SelectedValue == "2" || SessionUserType == "2")
            //{
            //    if (SessionUserType == "2") { rbsalary.SelectedValue = "2"; }
            //    query = "Select SalDet.EmpNo,EmpDet.EmpName,OP.BankACCNo as AccountNo,EmpDet.Department,EmpDet.ExisistingCode,";
            //    if (Stafflabour == "S")
            //    {
            //        query = query + " Round((Sum(cast(SalDet.BasicAndDANew as decimal(18,2)) + cast(SalDet.BasicHRA as decimal(18,2)) + ";
            //        query = query + " cast(SalDet.ConvAllow as decimal(18,2)) + cast(SalDet.EduAllow as decimal(18,2)) +  ";
            //        query = query + " cast(SalDet.MediAllow as decimal(18,2)) + cast(SalDet.BasicRAI as decimal(18,2)) + ";

            //        query = query + " isnull(cast(SalDet.Basic_Spl_Allowance as decimal(18,2)),0) + isnull(cast(SalDet.Basic_Uniform as decimal(18,2)),0) + ";
            //        query = query + " isnull(cast(SalDet.Basic_Vehicle_Maintenance as decimal(18,2)),0) + isnull(cast(SalDet.Basic_Communication as decimal(18,2)),0) + ";
            //        query = query + " isnull(cast(SalDet.Basic_Journ_Perid_Paper as decimal(18,2)),0) + isnull(cast(SalDet.Basic_Incentives as decimal(18,2)),0) + ";

            //        query = query + " cast(SalDet.WashingAllow as decimal(18,2))) - (Sum(SalDet.TotalDeductions))),-1) as NetSalary,";
            //    }
            //    else
            //    {
            //        query = query + " Round((Sum(cast(SalDet.BasicAndDANew as decimal(18,2)) + cast(SalDet.BasicHRA as decimal(18,2)) + ";
            //        query = query + " cast(SalDet.ConvAllow as decimal(18,2)) + cast(SalDet.EduAllow as decimal(18,2)) +  ";
            //        query = query + " cast(SalDet.MediAllow as decimal(18,2)) + cast(SalDet.BasicRAI as decimal(18,2)) + ";

            //        query = query + " isnull(cast(SalDet.Basic_Spl_Allowance as decimal(18,2)),0) + isnull(cast(SalDet.Basic_Uniform as decimal(18,2)),0) + ";
            //        query = query + " isnull(cast(SalDet.Basic_Vehicle_Maintenance as decimal(18,2)),0) + isnull(cast(SalDet.Basic_Communication as decimal(18,2)),0) + ";
            //        query = query + " isnull(cast(SalDet.Basic_Journ_Perid_Paper as decimal(18,2)),0) + isnull(cast(SalDet.Basic_Incentives as decimal(18,2)),0) + ";

            //        query = query + " cast(SalDet.WashingAllow as decimal(18,2)) + (AD.NFH_Work_Days_Statutory * (cast(SalDet.Basic_SM as decimal(18,2))))) - (Sum(SalDet.TotalDeductions))),-1) as NetSalary,";
            //    }

            //    //" SalDet.BasicandDA,(SalDet.BasicandDA - SalDet.ESI) as NetSalary, " +
            //    query = query + " SalDet.BasicandDA,((SalDet.NetPay)-(SalDet.ThreesidedAmt + SalDet.OTHoursAmtNew)) as NetSalary_Dummy,SalDet.RoundOffNetPay as NetSalaryOLD, " +
            //            " SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.SalaryDate," +
            //            " SalDet.NetPay,MstDpt.DepartmentNm,SalDet.ESI from EmployeeDetails EmpDet" +
            //            " inner Join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo " +
            //            " inner Join AttenanceDetails AD on EmpDet.EmpNo=AD.EmpNo " +
            //            " inner Join MstDepartment as MstDpt on MstDpt.DepartmentCd = EmpDet.Department" +
            //            " inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo where " +
            //            " SalDet.Month='" + ddlMonths.SelectedValue + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "'" +
            //            " And AD.Months='" + ddlMonths.SelectedValue + "' AND AD.FinancialYear='" + ddlFinance.SelectedValue + "'";
            //    if (Stafflabour != "") { query = query + " and EmpDet.StafforLabor='" + Stafflabour + "'"; }
            //    if (ddldept.SelectedValue != "0" && ddldept.SelectedValue != "") { query = query + " and EmpDet.Department='" + ddldept.SelectedValue + "'"; }
            //    if (txtEmployeeType.SelectedValue != "0" && txtEmployeeType.SelectedValue != "") { query = query + " and EmpDet.EmployeeType='" + txtEmployeeType.SelectedValue + "'"; }

            //    //Activate Employee Check
            //    if (Left_Employee == "1") { query = query + " and EmpDet.ActivateMode='N' and Month(EmpDet.deactivedate)=Month(convert(datetime,'" + Left_Date + "', 105)) And Year(EmpDet.deactivedate)=Year(convert(datetime,'" + Left_Date + "', 105))"; }
            //    //if (Left_Employee == "2") { query = query + " and EmpDet.ActivateMode='N'"; }
            //    else
            //    {
            //        //if (Left_Date != "") { query = query + " and ED.deactivedate > convert(datetime,'" + Left_Date + "', 105)"; }
            //        if (Left_Date != "") { query = query + " and (EmpDet.deactivedate > convert(datetime,'" + Left_Date + "', 105) Or EmpDet.ActivateMode='Y')"; }
            //    }
            //    if (SessionUserType == "2")
            //    {
            //        query = query + " and (OP.EligiblePF='1')";
            //    }
            //    else
            //    {
            //        //PF Check
            //        if (PFTypeGet.ToString() != "0")
            //        {
            //            if (PFTypeGet.ToString() == "1")
            //            {
            //                //PF Employee
            //                query = query + " and (OP.EligiblePF='1')";
            //            }
            //            else
            //            {
            //                //Non PF Employee
            //                query = query + " and (OP.EligiblePF='2')";
            //            }
            //        }
            //    }


            //    if (txtDivision.Text.ToString() != "" && txtDivision.Text.ToString() != "-----Select----")
            //    {
            //        query = query + " and EmpDet.Division='" + txtDivision.Text.ToString() + "'";
            //    }

            //    query = query + " and EmpDet.Ccode='" + SessionCcode + "' and EmpDet.Lcode='" + SessionLcode + "'" +
            //            " and SalDet.WagesType='" + rbsalary.SelectedValue + "' and SalDet.Salarythrough='2'" +
            //            " group by SalDet.EmpNo,EmpDet.EmpName,OP.BankACCNo,EmpDet.Department,EmpDet.ExisistingCode," +
            //            " SalDet.BasicandDA,SalDet.ThreesidedAmt,SalDet.OTHoursAmtNew,SalDet.RoundOffNetPay,SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.SalaryDate," +
            //            " SalDet.NetPay,MstDpt.DepartmentNm,SalDet.ESI Order by EmpDet.ExisistingCode Asc";
            //}
            //else
            //{


            //Get Adolescent Date
            int month = DateTime.ParseExact(ddlMonths.SelectedItem.Text.ToString(), "MMMM", CultureInfo.CurrentCulture).Month;
            string Year_Get_Adol = "";
            string Adolescent_Date_Str = "";
            
            if (ddlMonths.SelectedItem.Text.ToUpper().ToString() == "January".ToUpper().ToString() || ddlMonths.SelectedItem.Text.ToUpper().ToString() == "February".ToUpper().ToString() || ddlMonths.SelectedItem.Text.ToUpper().ToString() == "March".ToUpper().ToString())
            {
                Year_Get_Adol = (Convert.ToDecimal(ddlFinance.SelectedValue.ToString()) + Convert.ToDecimal(1)).ToString();
            }
            else
            {
                Year_Get_Adol = ddlFinance.SelectedValue.ToString();
            }
            string Last_Day = DateTime.DaysInMonth(Convert.ToInt32(Year_Get_Adol), month).ToString();
            string Month_Str = "1";
            if (month.ToString().Length == 1) { Month_Str = "0" + month.ToString(); }
            else { Month_Str = month.ToString(); }
            Adolescent_Date_Str = Last_Day + "/" + Month_Str + "/" + Year_Get_Adol;

            if (txtEmployeeType.SelectedValue == "3")
            {
                query = "Select EmpDet.BirthDate,SalDet.EmpNo,EmpDet.FirstName as EmpName,EmpDet.AccountNo as AccountNo,EmpDet.DeptName as Department,EmpDet.ExistingCode as ExisistingCode, ";
                query = query + " (Select case when ";
                query = query + " (FLOOR((CAST (CONVERT(Datetime,'" + Adolescent_Date_Str + "',103) AS INTEGER) - CAST(CONVERT(datetime,EmpDet.BirthDate,103)  AS INTEGER)) / 365.25)) >=18 then";
                query = query + " Round((Sum(cast(SalDet.BasicAndDANew as decimal(18,2)) + cast(SalDet.BasicHRA as decimal(18,2)) + " +
                            " cast(SalDet.ConvAllow as decimal(18,2)) + cast(SalDet.EduAllow as decimal(18,2)) +  " +
                            " cast(SalDet.MediAllow as decimal(18,2)) + cast(SalDet.BasicRAI as decimal(18,2)) + " +

                            " isnull(cast(SalDet.Basic_Spl_Allowance as decimal(18,2)),0) + isnull(cast(SalDet.Basic_Uniform as decimal(18,2)),0) + " +
                            " isnull(cast(SalDet.Basic_Vehicle_Maintenance as decimal(18,2)),0) + isnull(cast(SalDet.Basic_Communication as decimal(18,2)),0) + " +
                            " isnull(cast(SalDet.Basic_Journ_Perid_Paper as decimal(18,2)),0) + isnull(cast(SalDet.Basic_Incentives as decimal(18,2)),0) + ";
                query = query + " cast(SalDet.ThreesidedAmt as decimal(18,2)) + cast(SalDet.WashingAllow as decimal(18,2)) + (AD.NFH_Work_Days_Statutory * (cast(SalDet.Basic_SM as decimal(18,2))))) - (Sum(SalDet.Total_Deduction_IF))),-1)";
                query = query + " else";
                query = query + " Round((Sum(cast(SalDet.BasicAndDANew as decimal(18,2)) + cast(SalDet.BasicHRA as decimal(18,2)) + " +
                            " cast(SalDet.ConvAllow as decimal(18,2)) + cast(SalDet.EduAllow as decimal(18,2)) +  " +
                            " cast(SalDet.MediAllow as decimal(18,2)) + cast(SalDet.BasicRAI as decimal(18,2)) + " +

                            " isnull(cast(SalDet.Basic_Spl_Allowance as decimal(18,2)),0) + isnull(cast(SalDet.Basic_Uniform as decimal(18,2)),0) + " +
                            " isnull(cast(SalDet.Basic_Vehicle_Maintenance as decimal(18,2)),0) + isnull(cast(SalDet.Basic_Communication as decimal(18,2)),0) + " +
                            " isnull(cast(SalDet.Basic_Journ_Perid_Paper as decimal(18,2)),0) + isnull(cast(SalDet.Basic_Incentives as decimal(18,2)),0) + ";
                query = query + " cast(SalDet.WashingAllow as decimal(18,2)) + (AD.NFH_Work_Days_Statutory * (cast(SalDet.Basic_SM as decimal(18,2))))) - (Sum(SalDet.Total_Deduction_IF))),-1)";
                query = query + " end ) as NetSalary,";

            }
            else
            {
                query = "Select EmpDet.BirthDate,SalDet.EmpNo,EmpDet.FirstName as EmpName,EmpDet.AccountNo as AccountNo,EmpDet.DeptName as Department,EmpDet.ExistingCode as ExisistingCode, " +
                    //" SalDet.BasicandDA,(SalDet.BasicandDA - SalDet.ESI) as NetSalary, " +
                            " Round((Sum(cast(SalDet.BasicAndDANew as decimal(18,2)) + cast(SalDet.BasicHRA as decimal(18,2)) + " +
                            " cast(SalDet.ConvAllow as decimal(18,2)) + cast(SalDet.EduAllow as decimal(18,2)) +  " +
                            " cast(SalDet.MediAllow as decimal(18,2)) + cast(SalDet.BasicRAI as decimal(18,2)) + " +

                            " isnull(cast(SalDet.Basic_Spl_Allowance as decimal(18,2)),0) + isnull(cast(SalDet.Basic_Uniform as decimal(18,2)),0) + " +
                            " isnull(cast(SalDet.Basic_Vehicle_Maintenance as decimal(18,2)),0) + isnull(cast(SalDet.Basic_Communication as decimal(18,2)),0) + " +
                            " isnull(cast(SalDet.Basic_Journ_Perid_Paper as decimal(18,2)),0) + isnull(cast(SalDet.Basic_Incentives as decimal(18,2)),0) + ";
                if (txtEmployeeType.SelectedValue == "1" || txtEmployeeType.SelectedValue == "7" || txtEmployeeType.SelectedValue == "6")
                {
                    query = query + " cast(SalDet.WashingAllow as decimal(18,2)) + (0 * (cast(SalDet.Basic_SM as decimal(18,2))))) - (Sum(SalDet.Total_Deduction_IF))),-1) as NetSalary,";
                }
                else if (txtEmployeeType.SelectedValue == "3")
                {
                    query = query + " cast(SalDet.ThreesidedAmt as decimal(18,2)) + cast(SalDet.WashingAllow as decimal(18,2)) + (AD.NFH_Work_Days_Statutory * (cast(SalDet.Basic_SM as decimal(18,2))))) - (Sum(SalDet.Total_Deduction_IF))),-1) as NetSalary,";
                }
                else
                {
                    query = query + " cast(SalDet.WashingAllow as decimal(18,2)) + (AD.NFH_Work_Days_Statutory * (cast(SalDet.Basic_SM as decimal(18,2))))) - (Sum(SalDet.Total_Deduction_IF))),-1) as NetSalary,";
                }
            }
            query = query + " SalDet.BasicandDA,((SalDet.NetPay)-(SalDet.ThreesidedAmt + SalDet.OTHoursAmtNew)) as NetSalary_Dummy,SalDet.RoundOffNetPay_IF as NetSalaryOLD, " +
                        " SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.SalaryDate," +
                        " SalDet.NetPay,MstDpt.DeptName,SalDet.Employee_ESI_IF as ESI from Employee_Mst EmpDet" +
                        " inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.ExistingCode COLLATE DATABASE_DEFAULT = SalDet.ExisistingCode COLLATE DATABASE_DEFAULT And EmpDet.LocCode COLLATE DATABASE_DEFAULT = SalDet.Lcode COLLATE DATABASE_DEFAULT " +
                        " inner Join [" + SessionPayroll + "]..AttenanceDetails AD on EmpDet.ExistingCode COLLATE DATABASE_DEFAULT = AD.ExistingCode COLLATE DATABASE_DEFAULT And EmpDet.LocCode COLLATE DATABASE_DEFAULT = AD.Lcode COLLATE DATABASE_DEFAULT " +
                        " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                //" inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo where " +
                        " where SalDet.Month='" + ddlMonths.SelectedItem.Text + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "'" +
                        " And EmpDet.Salary_Through='2'" +
                        " And AD.Months='" + ddlMonths.SelectedItem.Text + "' AND AD.FinancialYear='" + ddlFinance.SelectedValue + "'";
            if (Stafflabour != "") { query = query + " and EmpDet.CatName='" + Stafflabour + "'"; }
            if (ddldept.SelectedValue != "0" && ddldept.SelectedValue != "") { query = query + " and EmpDet.DeptCode='" + ddldept.SelectedValue + "'"; }
            if (txtEmployeeType.SelectedValue != "0" && txtEmployeeType.SelectedValue != "") { query = query + " and EmpDet.Wages='" + txtEmployeeType.SelectedItem.Text + "'"; }

            //Activate Employee Check
            if (Left_Employee == "1") { query = query + " and EmpDet.IsActive='No' and Month(convert(datetime,EmpDet.DOR,105))=Month(convert(datetime,'" + Left_Date + "', 105)) And Year(convert(datetime,EmpDet.DOR,105))=Year(convert(datetime,'" + Left_Date + "', 105))"; }
            //if (Left_Employee == "2") { query = query + " and EmpDet.ActivateMode='N'"; }
            else
            {
                //if (Left_Date != "") { query = query + " and ED.deactivedate > convert(datetime,'" + Left_Date + "', 105)"; }
                if (Left_Date != "") { query = query + " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + Left_Date + "', 105) Or EmpDet.IsActive='Yes')"; }
            }


            if (SessionUserType == "2")
            {
                //PF Employee
                query = query + " and (EmpDet.Eligible_PF='1')";
            }
            else
            {
                //PF Check
                if (PFTypeGet.ToString() != "0")
                {
                    if (PFTypeGet.ToString() == "1")
                    {
                        //PF Employee
                        query = query + " and (EmpDet.Eligible_PF='1')";
                    }
                    else
                    {
                        //Non PF Employee
                        query = query + " and (EmpDet.Eligible_PF='2')";
                    }
                }
            }

            if (txtDivision.Text.ToString() != "" && txtDivision.Text.ToString() != "-Select-")
            {
                query = query + " and EmpDet.Division='" + txtDivision.Text.ToString() + "'";
            }

            if (txtBankName.SelectedValue != "-Select-")
            {
                query = query + " and EmpDet.BankName='" + txtBankName.SelectedValue.ToString() + "'";
            }

            //query = query + "and SalDet.FromDate = convert(datetime,'" + MyDate1 + "', 105) and SalDet.ToDate = Convert(Datetime,'" + MyDate2 + "', 105)";
            query = query + " and EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "'" +
            " and SalDet.Ccode='" + SessionCcode + "' and SalDet.Lcode='" + SessionLcode + "'" +
            " and AD.Ccode='" + SessionCcode + "' and AD.Lcode='" + SessionLcode + "'" +
                //" and SalDet.WagesType='" + rbsalary.SelectedValue + "' and SalDet.Salarythrough='2'" +
                //Check here
                //" and SalDet.Salarythrough='2'" +
                    " group by EmpDet.BirthDate,SalDet.EmpNo,EmpDet.FirstName,EmpDet.AccountNo,EmpDet.DeptName,EmpDet.ExistingCode," +
                    " SalDet.BasicandDA,SalDet.ThreesidedAmt,SalDet.OTHoursAmtNew,SalDet.RoundOffNetPay_IF,SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.SalaryDate," +
                    " SalDet.NetPay,MstDpt.DeptName,SalDet.Employee_ESI_IF Order by EmpDet.ExistingCode Asc";
            //}
            DataTable dt_1 = new DataTable();
            dt_1 = objdata.RptEmployeeMultipleDetails(query);
            //SqlCommand cmd = new SqlCommand(query, con);
            //DataTable dt_1 = new DataTable();
            //SqlDataAdapter sda = new SqlDataAdapter(cmd);
            ////DataSet ds1 = new DataSet();
            //con.Open();
            //sda.Fill(dt_1);
            //con.Close();
            BankSalaryGridView.DataSource = dt_1;
            BankSalaryGridView.DataBind();

            string attachment = "attachment;filename=BankSalaryDetails.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            DataTable dt = new DataTable();
            query = "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dt = objdata.RptEmployeeMultipleDetails(query);
            //dt = objdata.Company_retrive(SessionCcode, SessionLcode);
            if (dt.Rows.Count > 0)
            {
                CmpName = dt.Rows[0]["Cname"].ToString();
                Cmpaddress = (dt.Rows[0]["Address1"].ToString() + ", " + dt.Rows[0]["Address2"].ToString() + ", " + dt.Rows[0]["Location"].ToString() + "-" + dt.Rows[0]["Pincode"].ToString());
            }

            StringWriter stw = new StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            BankSalaryGridView.RenderControl(htextw);
            Response.Write("<table>");
            Response.Write("<tr align='Center'>");
            Response.Write("<td colspan='6' style='font-size:20.0pt;font-weight:bold;text-decoration:underline;''>");
            Response.Write("EVEREADY SPINING MILL");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr align='Center'>");
            Response.Write("<td colspan='6' style='font-size:13.0pt;font-weight:bold;text-decoration:underline;''>");
            Response.Write("" + SessionLcode + "");
            Response.Write("</td>");
            Response.Write("</tr>");
            if (txtBankName.SelectedValue != "-Select-")
            {
                Response.Write("<tr align='Center'>");
                Response.Write("<td colspan='6' style='font-size:12.0pt;font-weight:bold;text-decoration:underline;''>");
                Response.Write("BANK NAME : " + txtBankName.SelectedValue + "");
                Response.Write("</td>");
                Response.Write("</tr>");
            }
            else
            {
                Response.Write("<tr align='Center'>");
                Response.Write("<td colspan='6' style='font-size:12.0pt;font-weight:bold;text-decoration:underline;''>");
                //Response.Write("" + Cmpaddress + "");
                Response.Write("");
                Response.Write("</td>");
                Response.Write("</tr>");
            }

            //if (rbsalary.SelectedValue == "2")
            //{
            //    Response.Write("<tr align='center'>");
            //    Response.Write("<td colspan='6' style='font-size:11.0pt;font-weight:bold;text-decoration:underline;''>");
            //    Response.Write("BANK A/C SALARY DETAILS FOR THE MONTH OF " + ddlMonths.SelectedValue.ToUpper().ToString() + " - " + YR);
            //    Response.Write("</td>");
            //    Response.Write("</tr>");
            //}
            //else
            //{
            Response.Write("<tr align='center'>");
            Response.Write("<td colspan='6' style='font-size:11.0pt;font-weight:bold;text-decoration:underline;''>");
            Response.Write("BANK A/C SALARY DETAILS FOR THE MONTH OF " + ddlMonths.SelectedItem.Text + " - " + ddlFinance.SelectedValue);
            Response.Write("</td>");
            Response.Write("</tr>");
            //}
            Response.Write("</table>");

            Response.Write(stw.ToString());
            //Get Row Count
            string bank_row_count = "";
            bank_row_count = (Convert.ToDecimal(5) + Convert.ToDecimal(BankSalaryGridView.Rows.Count)).ToString();

            if (BankSalaryGridView.Rows.Count.ToString() != "0")
            {
                Response.Write("<table border='1'>");
                Response.Write("<tr style='font-size:12.0pt;font-weight:bold;'>");
                Response.Write("<td colspan='4' align='right'>Grand Total</td>");
                Response.Write("<td>=sum(E6:E" + bank_row_count.ToString() + ")</td>");
                Response.Write("<td></td>");
                Response.Write("</tr></table>");
            }
            Response.End();
            Response.Clear();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Downloaded Successfully');", true);
        }
    }


    protected void btnCivilAbstract_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            string CmpName = "";
            string Cmpaddress = "";
            int YR = 0;

            //if (rbsalary.SelectedValue != "1")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select Weekly Wages Type');", true);
            //    ErrFlag = true;
            //}

            if (ddlMonths.SelectedItem.Text == "January")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else if (ddlMonths.SelectedItem.Text == "February")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else if (ddlMonths.SelectedItem.Text == "March")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
            }

            if (ddlcategory.SelectedValue == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Category');", true);
                ErrFlag = true;
            }
            else if ((txtEmployeeType.SelectedValue == "") || (txtEmployeeType.SelectedValue == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Employee Type');", true);
                ErrFlag = true;
            }

            else if ((ddlMonths.SelectedValue == "0") || (ddlMonths.SelectedValue == ""))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Months');", true);
                ErrFlag = true;
            }


            if (!ErrFlag)
            {
                //if (ddldept.SelectedValue == "0")
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Department Properly');", true);
                //    ErrFlag = true;
                //}
                if (!ErrFlag)
                {
                    if (ddlcategory.SelectedValue == "STAFF")
                    {
                        Stafflabour = "STAFF";
                    }
                    else if (ddlcategory.SelectedValue == "LABOUR")
                    {
                        Stafflabour = "LABOUR";
                    }
                    string constr = ConfigurationManager.AppSettings["ConnectionString"];
                    SqlConnection con = new SqlConnection(constr);
                    //foreach (GridViewRow gvsal in gvSalary.Rows)
                    //{
                    //    Label lblEmpNo = (Label)gvsal.FindControl("lblExist");
                    //    string qry = "Update SalaryDetails set Process_Mode='" + 1 + "' where Month='" + ddlMonths.SelectedValue + "' and FinancialYear='" + ddlFinance.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and ExisistingCode='" + lblEmpNo.Text + "'";
                    //    SqlCommand cmd = new SqlCommand(qry, con);
                    //    con.Open();
                    //    cmd.ExecuteNonQuery();
                    //    con.Close();
                    //}
                    string Emp_ESI_Code = "";//ddESI.SelectedValue.ToString();

                    string Payslip_Format_Type = rdbPayslipFormat.SelectedValue.ToString();

                    string Str_PFType = RdbPFNonPF.SelectedValue.ToString();
                    string Str_ChkLeft = "";// ChkLeft.SelectedValue.ToString();
                    if (ChkLeft.Checked == true)
                    {
                        Str_ChkLeft = "1";
                    }
                    else
                    {
                        Str_ChkLeft = "0";
                    }


                    ResponseHelper.Redirect("ViewCivilAbstract.aspx?Cate=" + Stafflabour + "&Depat=" + ddldept.SelectedValue + "&Months=" + ddlMonths.SelectedItem.Text + "&yr=" + ddlFinance.SelectedValue + "&fromdate=" + txtfrom.Text + "&ToDate=" + txtTo.Text + "&CashBank=" + RdbCashBank.SelectedValue + "&ESICode=" + Emp_ESI_Code + "&EmpType=" + txtEmployeeType.SelectedValue.ToString() + "&PayslipType=" + rdbPayslipFormat.SelectedValue.ToString() + "&PFTypePost=" + RdbPFNonPF.SelectedValue.ToString() + "&Left_Emp=" + Str_ChkLeft.ToString() + "&Division=" + txtDivision.Text.ToString() + "&Leftdate=" + txtLeftDate.Text.ToString(), "_blank", "");

                }
            }
        }
        catch (Exception ex)
        {
        }
    }

    protected void btnLeaveWages_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            string CmpName = "";
            string Cmpaddress = "";
            int YR = 0;
            if (ddlMonths.SelectedItem.Text == "January")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else if (ddlMonths.SelectedItem.Text == "February")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else if (ddlMonths.SelectedItem.Text == "March")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
            }

            if (ddlcategory.SelectedValue == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Category');", true);
                ErrFlag = true;
            }
            else if ((txtEmployeeType.SelectedValue == "") || (txtEmployeeType.SelectedValue == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Employee Type');", true);
                ErrFlag = true;
            }

            else if ((ddlMonths.SelectedValue == "0") || (ddlMonths.SelectedValue == ""))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Months');", true);
                ErrFlag = true;
            }


            if (!ErrFlag)
            {
                //if (ddldept.SelectedValue == "0")
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Department Properly');", true);
                //    ErrFlag = true;
                //}
                if (!ErrFlag)
                {
                    if (ddlcategory.SelectedValue == "STAFF")
                    {
                        Stafflabour = "STAFF";
                    }
                    else if (ddlcategory.SelectedValue == "LABOUR")
                    {
                        Stafflabour = "LABOUR";
                    }
                    string constr = ConfigurationManager.AppSettings["ConnectionString"];
                    SqlConnection con = new SqlConnection(constr);
                    //foreach (GridViewRow gvsal in gvSalary.Rows)
                    //{
                    //    Label lblEmpNo = (Label)gvsal.FindControl("lblExist");
                    //    string qry = "Update SalaryDetails set Process_Mode='" + 1 + "' where Month='" + ddlMonths.SelectedValue + "' and FinancialYear='" + ddlFinance.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and ExisistingCode='" + lblEmpNo.Text + "'";
                    //    SqlCommand cmd = new SqlCommand(qry, con);
                    //    con.Open();
                    //    cmd.ExecuteNonQuery();
                    //    con.Close();
                    //}
                    string Emp_ESI_Code = "";//ddESI.SelectedValue.ToString();

                    string Payslip_Format_Type = rdbPayslipFormat.SelectedValue.ToString();

                    string Str_PFType = RdbPFNonPF.SelectedValue.ToString();
                    string Str_ChkLeft = "";// ChkLeft.SelectedValue.ToString();
                    if (ChkLeft.Checked == true)
                    {
                        Str_ChkLeft = "1";
                    }
                    else { Str_ChkLeft = "0"; }
                    string SalaryType = "";

                    ResponseHelper.Redirect("ViewReport_Opt.aspx?Cate=" + Stafflabour + "&Depat=" + ddldept.SelectedValue + "&Months=" + ddlMonths.SelectedItem.Text + "&yr=" + ddlFinance.SelectedValue + "&fromdate=" + txtfrom.Text + "&ToDate=" + txtTo.Text + "&Salary=" + SalaryType + "&CashBank=" + RdbCashBank.SelectedValue + "&ESICode=" + Emp_ESI_Code + "&EmpTypeCd=" + txtEmployeeType.SelectedValue.ToString() + "&EmpType=" + txtEmployeeType.SelectedItem.Text.ToString() + "&PayslipType=" + rdbPayslipFormat.SelectedValue.ToString() + "&PFTypePost=" + RdbPFNonPF.SelectedValue.ToString() + "&Left_Emp=" + Str_ChkLeft.ToString() + "&Leftdate=" + txtLeftDate.Text + "&Division=" + txtDivision.Text + "&Report_Type=NFHWages", "_blank", "");
                }
            }
        }
        catch (Exception ex)
        {
        }
    }

    //protected void btnAllSalary_Click(object sender, EventArgs e)
    //{
    //    bool ErrFlag = false;
    //    //if (rbsalary.SelectedValue == "2")
    //    //{
    //        if (ddlMonths.SelectedValue == "0")
    //        {
    //            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Month Properly');", true);
    //            ErrFlag = true;
    //        }
    //    //}
    //    //else
    //    //{
    //        if (txtfrom.Text.Trim() == "")
    //        {
    //            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Enter the From Date');", true);
    //            ErrFlag = true;
    //        }
    //        else if (txtTo.Text.Trim() == "")
    //        {
    //            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Enter the To Date');", true);
    //            ErrFlag = true;
    //        }
    //        if (!ErrFlag)
    //        {
    //            DateTime dfrom = Convert.ToDateTime(txtfrom.Text);
    //            DateTime dtto = Convert.ToDateTime(txtTo.Text);
    //            MyDate1 = DateTime.ParseExact(txtfrom.Text, "dd-MM-yyyy", null);
    //            MyDate2 = DateTime.ParseExact(txtTo.Text, "dd-MM-yyyy", null);
    //            if (dtto < dfrom)
    //            {
    //                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Enter the Date properly...');", true);
    //                txtfrom.Text = null;
    //                txtTo.Text = null;
    //                ErrFlag = true;
    //            }
    //        }
    //    //}

    //    if (!ErrFlag)
    //    {
    //        string CmpName = "";
    //        string Cmpaddress = "";
    //        int YR = 0;
    //        string query = "";
    //        string Staff_Bank_Query = "";
    //        string Staff_Cash_Query = "";
    //        string Labour_Bank_Query = "";
    //        string Labour_Cash_Query = "";
    //        string Department_Name_Check = "";
    //        Int32 SI_Count = 0;

    //        if (ddlMonths.SelectedItem.Text == "January")
    //        {
    //            YR = Convert.ToInt32(ddlFinance.SelectedValue);
    //            YR = YR + 1;
    //        }
    //        else if (ddlMonths.SelectedItem.Text == "February")
    //        {
    //            YR = Convert.ToInt32(ddlFinance.SelectedValue);
    //            YR = YR + 1;
    //        }
    //        else if (ddlMonths.SelectedItem.Text == "March")
    //        {
    //            YR = Convert.ToInt32(ddlFinance.SelectedValue);
    //            YR = YR + 1;
    //        }
    //        else
    //        {
    //            YR = Convert.ToInt32(ddlFinance.SelectedValue);
    //        }
    //        if (ddlcategory.SelectedValue == "STAFF")
    //        {
    //            Stafflabour = "STAFF";
    //        }
    //        else if (ddlcategory.SelectedValue == "LABOUR")
    //        {
    //            Stafflabour = "LABOUR";
    //        }

    //        Emp_ESI_Code = "";//ddESI.SelectedValue.ToString();

    //        if (Emp_ESI_Code == "--Select--") { Emp_ESI_Code = ""; }

    //        if (rbsalary.SelectedValue == "2")
    //        {
    //            query = "Select SalDet.EmpNo,EmpDet.ExisistingCode,EmpDet.EmpName,EmpDet.OldID,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays, " +
    //                    " SalDet.NFh,SalDet.BasicandDA,(SalDet.BasicandDA - SalDet.ESI) as NetSalary,SalDet.FDA,SalDet.VDA,SalDet.HRA, SalDet.Advance,SalDet.Stamp,SalDet.Unioncg,(SalDet.WorkedDays + SalDet.NFh) as tot, " +
    //                    " CAST(EmpDet.ExisistingCode AS int) as OrderToken," +
    //                    " SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.LeaveDays,SalDet.SalaryDate,SalDet.allowances1," +
    //                    " SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.Deduction1,SalDet.Weekoff,SalDet.CL,SalDet.Deduction2, " +
    //                    " SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.Losspay,SalDet.HomeDays,SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings,MstDpt.DepartmentNm,SM.Base,SM.PFS, " +
    //                    " SalDet.OverTime,MstDpt.DepartmentNm,SalDet.ProvidentFund,SalDet.ESI,SalDet.HRA,SalDet.WorkedDays,sum(SalDet.NetPay - (SalDet.GrossEarnings - SalDet.TotalDeductions)) as Roundoff," +
    //                    " SalDet.PfSalary as PFEarnings, SalDet.HalfNightAmt,SalDet.FullNightAmt,SalDet.SpinningAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt from EmployeeDetails EmpDet inner Join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo " +
    //                    " inner Join MstDepartment as MstDpt on MstDpt.DepartmentCd = EmpDet.Department inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo inner Join SalaryMaster SM on SM.EmpNo=SalDet.EmpNo where " +
    //                    " SalDet.Month='" + ddlMonths.SelectedValue + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.StafforLabor='" + Stafflabour + "' and " +
    //                    " EmpDet.Ccode='" + SessionCcode + "' and EmpDet.Lcode='" + SessionLcode + "' and SalDet.WagesType='" + rbsalary.SelectedValue + "'";

    //            if (txtDivision.Text.ToString() != "" && txtDivision.Text.ToString() != "-----Select----")
    //            {
    //                query = query + " and EmpDet.Division='" + txtDivision.Text.ToString() + "'";
    //            }

    //            query = query + " and SalDet.Salarythrough='" + RdbCashBank.SelectedValue + "'" +
    //                    " group by SalDet.EmpNo," +
    //                    " EmpDet.ExisistingCode,EmpDet.EmpName,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays,SalDet.NFh,SalDet.BasicandDA,SalDet.FDA,SalDet.VDA,SalDet.HRA," +
    //                    " SalDet.Advance,SalDet.Stamp,SalDet.Unioncg,SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.LeaveDays,SalDet.SalaryDate,SalDet.allowances1, " +
    //                    " SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.HomeDays,SalDet.Deduction1,SalDet.Weekoff,SalDet.CL,SalDet.Deduction2," +
    //                    " SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.Losspay,SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings," +
    //                    " SalDet.OverTime,MstDpt.DepartmentNm,SalDet.ProvidentFund,SalDet.ESI,SalDet.HRA,SalDet.WorkedDays, SalDet.PfSalary, SalDet.HalfNightAmt," +
    //                    " SalDet.FullNightAmt,SalDet.SpinningAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt,EmpDet.OldID,MstDpt.DepartmentNm,SM.Base,SM.PFS Order by OrderToken Asc";

    //            Staff_Bank_Query = "Select '' as OTHours,SalDet.EmpNo,EmpDet.ExisistingCode,EmpDet.EmpName,EmpDet.OldID,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays, " +
    //                    " SalDet.NFh,SalDet.BasicandDA,(SalDet.BasicandDA - SalDet.ESI) as NetSalary,SalDet.FDA,SalDet.VDA,SalDet.HRA, SalDet.Advance,SalDet.Stamp,SalDet.Unioncg,(SalDet.WorkedDays + SalDet.NFh) as tot, " +
    //                    " CAST(EmpDet.ExisistingCode AS int) as OrderToken," +
    //                    " SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.LeaveDays,SalDet.SalaryDate,SalDet.allowances1," +
    //                    " SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.Deduction1,SalDet.Weekoff,SalDet.CL,SalDet.Deduction2, " +
    //                    " SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.Losspay,SalDet.HomeDays,SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings,MstDpt.DepartmentNm,SM.Base,SM.PFS, " +
    //                    " SalDet.OverTime,MstDpt.DepartmentNm,SalDet.ProvidentFund,SalDet.ESI,SalDet.HRA,SalDet.WorkedDays,sum(SalDet.NetPay - (SalDet.GrossEarnings - SalDet.TotalDeductions)) as Roundoff," +
    //                    " SalDet.PfSalary as PFEarnings, SalDet.HalfNightAmt,SalDet.FullNightAmt,SalDet.SpinningAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt from EmployeeDetails EmpDet inner Join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo " +
    //                    " inner Join MstDepartment as MstDpt on MstDpt.DepartmentCd = EmpDet.Department inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo inner Join SalaryMaster SM on SM.EmpNo=SalDet.EmpNo where " +
    //                    " SalDet.Month='" + ddlMonths.SelectedValue + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.StafforLabor='S' and " +
    //                    " EmpDet.Ccode='" + SessionCcode + "' and EmpDet.Lcode='" + SessionLcode + "' and SalDet.WagesType='" + rbsalary.SelectedValue + "'" +
    //                    " and SalDet.Salarythrough='2'";
    //            if (ddldept.SelectedValue != "0" && ddldept.SelectedValue != "")
    //            {
    //                Staff_Bank_Query = Staff_Bank_Query + " and EmpDet.Department='" + ddldept.SelectedValue + "'";
    //            }

    //            if (txtDivision.Text.ToString() != "" && txtDivision.Text.ToString() != "-----Select----")
    //            {
    //                Staff_Bank_Query = Staff_Bank_Query + " and EmpDet.Division='" + txtDivision.Text.ToString() + "'";
    //            }

    //            Staff_Bank_Query = Staff_Bank_Query + " group by SalDet.EmpNo," +
    //                    " EmpDet.ExisistingCode,EmpDet.EmpName,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays,SalDet.NFh,SalDet.BasicandDA,SalDet.FDA,SalDet.VDA,SalDet.HRA," +
    //                    " SalDet.Advance,SalDet.Stamp,SalDet.Unioncg,SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.LeaveDays,SalDet.SalaryDate,SalDet.allowances1, " +
    //                    " SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.HomeDays,SalDet.Deduction1,SalDet.Weekoff,SalDet.CL,SalDet.Deduction2," +
    //                    " SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.Losspay,SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings," +
    //                    " SalDet.OverTime,MstDpt.DepartmentNm,SalDet.ProvidentFund,SalDet.ESI,SalDet.HRA,SalDet.WorkedDays, SalDet.PfSalary, SalDet.HalfNightAmt," +
    //                    " SalDet.FullNightAmt,SalDet.SpinningAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt,EmpDet.OldID,MstDpt.DepartmentNm,SM.Base,SM.PFS Order by MstDpt.DepartmentNm,OrderToken Asc";

    //            Staff_Cash_Query = "Select '' as OTHours,SalDet.EmpNo,EmpDet.ExisistingCode,EmpDet.EmpName,EmpDet.OldID,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays, " +
    //                    " SalDet.NFh,SalDet.BasicandDA,(SalDet.BasicandDA - SalDet.ESI) as NetSalary,SalDet.FDA,SalDet.VDA,SalDet.HRA, SalDet.Advance,SalDet.Stamp,SalDet.Unioncg,(SalDet.WorkedDays + SalDet.NFh) as tot, " +
    //                    " CAST(EmpDet.ExisistingCode AS int) as OrderToken," +
    //                    " SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.LeaveDays,SalDet.SalaryDate,SalDet.allowances1," +
    //                    " SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.Deduction1,SalDet.Weekoff,SalDet.CL,SalDet.Deduction2, " +
    //                    " SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.Losspay,SalDet.HomeDays,SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings,MstDpt.DepartmentNm,SM.Base,SM.PFS, " +
    //                    " SalDet.OverTime,MstDpt.DepartmentNm,SalDet.ProvidentFund,SalDet.ESI,SalDet.HRA,SalDet.WorkedDays,sum(SalDet.NetPay - (SalDet.GrossEarnings - SalDet.TotalDeductions)) as Roundoff," +
    //                    " SalDet.PfSalary as PFEarnings, SalDet.HalfNightAmt,SalDet.FullNightAmt,SalDet.SpinningAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt from EmployeeDetails EmpDet inner Join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo " +
    //                    " inner Join MstDepartment as MstDpt on MstDpt.DepartmentCd = EmpDet.Department inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo inner Join SalaryMaster SM on SM.EmpNo=SalDet.EmpNo where " +
    //                    " SalDet.Month='" + ddlMonths.SelectedValue + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.StafforLabor='S' and " +
    //                    " EmpDet.Ccode='" + SessionCcode + "' and EmpDet.Lcode='" + SessionLcode + "' and SalDet.WagesType='" + rbsalary.SelectedValue + "'" +
    //                    " and SalDet.Salarythrough='1'";
    //            if (ddldept.SelectedValue != "0" && ddldept.SelectedValue != "")
    //            {
    //                Staff_Cash_Query = Staff_Cash_Query + " and EmpDet.Department='" + ddldept.SelectedValue + "'";
    //            }

    //            if (txtDivision.Text.ToString() != "" && txtDivision.Text.ToString() != "-----Select----")
    //            {
    //                Staff_Cash_Query = Staff_Cash_Query + " and EmpDet.Division='" + txtDivision.Text.ToString() + "'";
    //            }

    //            Staff_Cash_Query = Staff_Cash_Query + " group by SalDet.EmpNo," +
    //                    " EmpDet.ExisistingCode,EmpDet.EmpName,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays,SalDet.NFh,SalDet.BasicandDA,SalDet.FDA,SalDet.VDA,SalDet.HRA," +
    //                    " SalDet.Advance,SalDet.Stamp,SalDet.Unioncg,SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.LeaveDays,SalDet.SalaryDate,SalDet.allowances1, " +
    //                    " SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.HomeDays,SalDet.Deduction1,SalDet.Weekoff,SalDet.CL,SalDet.Deduction2," +
    //                    " SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.Losspay,SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings," +
    //                    " SalDet.OverTime,MstDpt.DepartmentNm,SalDet.ProvidentFund,SalDet.ESI,SalDet.HRA,SalDet.WorkedDays, SalDet.PfSalary, SalDet.HalfNightAmt," +
    //                    " SalDet.FullNightAmt,SalDet.SpinningAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt,EmpDet.OldID,MstDpt.DepartmentNm,SM.Base,SM.PFS Order by MstDpt.DepartmentNm,OrderToken Asc";

    //            Labour_Bank_Query = "Select '' as OTHours,SalDet.EmpNo,EmpDet.ExisistingCode,EmpDet.EmpName,EmpDet.OldID,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays, " +
    //                    " SalDet.NFh,SalDet.BasicandDA,(SalDet.BasicandDA - SalDet.ESI) as NetSalary,SalDet.FDA,SalDet.VDA,SalDet.HRA, SalDet.Advance,SalDet.Stamp,SalDet.Unioncg,(SalDet.WorkedDays + SalDet.NFh) as tot, " +
    //                    " CAST(EmpDet.ExisistingCode AS int) as OrderToken," +
    //                    " SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.LeaveDays,SalDet.SalaryDate,SalDet.allowances1," +
    //                    " SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.Deduction1,SalDet.Weekoff,SalDet.CL,SalDet.Deduction2, " +
    //                    " SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.Losspay,SalDet.HomeDays,SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings,MstDpt.DepartmentNm,SM.Base,SM.PFS, " +
    //                    " SalDet.OverTime,MstDpt.DepartmentNm,SalDet.ProvidentFund,SalDet.ESI,SalDet.HRA,SalDet.WorkedDays,sum(SalDet.NetPay - (SalDet.GrossEarnings - SalDet.TotalDeductions)) as Roundoff," +
    //                    " SalDet.PfSalary as PFEarnings, SalDet.HalfNightAmt,SalDet.FullNightAmt,SalDet.SpinningAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt from EmployeeDetails EmpDet inner Join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo " +
    //                    " inner Join MstDepartment as MstDpt on MstDpt.DepartmentCd = EmpDet.Department inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo inner Join SalaryMaster SM on SM.EmpNo=SalDet.EmpNo where " +
    //                    " SalDet.Month='" + ddlMonths.SelectedValue + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.StafforLabor='L' and " +
    //                    " EmpDet.Ccode='" + SessionCcode + "' and EmpDet.Lcode='" + SessionLcode + "' and SalDet.WagesType='" + rbsalary.SelectedValue + "'" +
    //                    " and SalDet.Salarythrough='2'";
    //            if (ddldept.SelectedValue != "0" && ddldept.SelectedValue != "")
    //            {
    //                Labour_Bank_Query = Labour_Bank_Query + " and EmpDet.Department='" + ddldept.SelectedValue + "'";
    //            }

    //            if (txtDivision.Text.ToString() != "" && txtDivision.Text.ToString() != "-----Select----")
    //            {
    //                Labour_Bank_Query = Labour_Bank_Query + " and EmpDet.Division='" + txtDivision.Text.ToString() + "'";
    //            }

    //            Labour_Bank_Query = Labour_Bank_Query + " group by SalDet.EmpNo," +
    //                    " EmpDet.ExisistingCode,EmpDet.EmpName,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays,SalDet.NFh,SalDet.BasicandDA,SalDet.FDA,SalDet.VDA,SalDet.HRA," +
    //                    " SalDet.Advance,SalDet.Stamp,SalDet.Unioncg,SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.LeaveDays,SalDet.SalaryDate,SalDet.allowances1, " +
    //                    " SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.HomeDays,SalDet.Deduction1,SalDet.Weekoff,SalDet.CL,SalDet.Deduction2," +
    //                    " SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.Losspay,SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings," +
    //                    " SalDet.OverTime,MstDpt.DepartmentNm,SalDet.ProvidentFund,SalDet.ESI,SalDet.HRA,SalDet.WorkedDays, SalDet.PfSalary, SalDet.HalfNightAmt," +
    //                    " SalDet.FullNightAmt,SalDet.SpinningAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt,EmpDet.OldID,MstDpt.DepartmentNm,SM.Base,SM.PFS Order by MstDpt.DepartmentNm,OrderToken Asc";

    //            Labour_Cash_Query = "Select '' as OTHours,SalDet.EmpNo,EmpDet.ExisistingCode,EmpDet.EmpName,EmpDet.OldID,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays, " +
    //                    " SalDet.NFh,SalDet.BasicandDA,(SalDet.BasicandDA - SalDet.ESI) as NetSalary,SalDet.FDA,SalDet.VDA,SalDet.HRA, SalDet.Advance,SalDet.Stamp,SalDet.Unioncg,(SalDet.WorkedDays + SalDet.NFh) as tot, " +
    //                    " CAST(EmpDet.ExisistingCode AS int) as OrderToken," +
    //                    " SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.LeaveDays,SalDet.SalaryDate,SalDet.allowances1," +
    //                    " SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.Deduction1,SalDet.Weekoff,SalDet.CL,SalDet.Deduction2, " +
    //                    " SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.Losspay,SalDet.HomeDays,SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings,MstDpt.DepartmentNm,SM.Base,SM.PFS, " +
    //                    " SalDet.OverTime,MstDpt.DepartmentNm,SalDet.ProvidentFund,SalDet.ESI,SalDet.HRA,SalDet.WorkedDays,sum(SalDet.NetPay - (SalDet.GrossEarnings - SalDet.TotalDeductions)) as Roundoff," +
    //                    " SalDet.PfSalary as PFEarnings, SalDet.HalfNightAmt,SalDet.FullNightAmt,SalDet.SpinningAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt from EmployeeDetails EmpDet inner Join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo " +
    //                    " inner Join MstDepartment as MstDpt on MstDpt.DepartmentCd = EmpDet.Department inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo inner Join SalaryMaster SM on SM.EmpNo=SalDet.EmpNo where " +
    //                    " SalDet.Month='" + ddlMonths.SelectedValue + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.StafforLabor='L' and " +
    //                    " EmpDet.Ccode='" + SessionCcode + "' and EmpDet.Lcode='" + SessionLcode + "' and SalDet.WagesType='" + rbsalary.SelectedValue + "'" +
    //                    " and SalDet.Salarythrough='1'";
    //            if (ddldept.SelectedValue != "0" && ddldept.SelectedValue != "")
    //            {
    //                Labour_Cash_Query = Labour_Cash_Query + " and EmpDet.Department='" + ddldept.SelectedValue + "'";
    //            }

    //            if (txtDivision.Text.ToString() != "" && txtDivision.Text.ToString() != "-----Select----")
    //            {
    //                Labour_Cash_Query = Labour_Cash_Query + " and EmpDet.Division='" + txtDivision.Text.ToString() + "'";
    //            }

    //            Labour_Cash_Query = Labour_Cash_Query + " group by SalDet.EmpNo," +
    //                    " EmpDet.ExisistingCode,EmpDet.EmpName,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays,SalDet.NFh,SalDet.BasicandDA,SalDet.FDA,SalDet.VDA,SalDet.HRA," +
    //                    " SalDet.Advance,SalDet.Stamp,SalDet.Unioncg,SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.LeaveDays,SalDet.SalaryDate,SalDet.allowances1, " +
    //                    " SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.HomeDays,SalDet.Deduction1,SalDet.Weekoff,SalDet.CL,SalDet.Deduction2," +
    //                    " SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.Losspay,SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings," +
    //                    " SalDet.OverTime,MstDpt.DepartmentNm,SalDet.ProvidentFund,SalDet.ESI,SalDet.HRA,SalDet.WorkedDays, SalDet.PfSalary, SalDet.HalfNightAmt," +
    //                    " SalDet.FullNightAmt,SalDet.SpinningAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt,EmpDet.OldID,MstDpt.DepartmentNm,SM.Base,SM.PFS Order by MstDpt.DepartmentNm,OrderToken Asc";


    //        }
    //        else//
    //        {
    //            query = "Select '' as OTHours,SalDet.EmpNo,EmpDet.ExisistingCode,EmpDet.EmpName,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays, " +
    //                    " SalDet.NFh,SalDet.BasicandDA,(SalDet.BasicandDA - SalDet.ESI) as NetSalary,SalDet.FDA,SalDet.VDA,SalDet.HRA, SalDet.Advance,SalDet.Stamp,SalDet.Unioncg,(SalDet.WorkedDays + SalDet.NFh) as tot, " +
    //                    " CAST(EmpDet.ExisistingCode AS int) as OrderToken," +
    //                    " SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.LeaveDays,SalDet.SalaryDate,SalDet.allowances1," +
    //                    " SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.Deduction1,SalDet.Weekoff,SalDet.CL,SalDet.Deduction2, " +
    //                    " SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.Deduction5,SalDet.HomeDays,SalDet.Losspay,SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings,EmpDet.OldID,MstDpt.DepartmentNm,SM.Base,SM.PFS,  " +
    //                    " SalDet.OverTime,MstDpt.DepartmentNm,SalDet.ProvidentFund,SalDet.ESI,SalDet.HRA,SalDet.WorkedDays,sum(SalDet.NetPay - (SalDet.GrossEarnings - SalDet.TotalDeductions)) as Roundoff," +
    //                    " SalDet.PfSalary as PFEarnings,SalDet.HalfNightAmt,SalDet.FullNightAmt,SalDet.SpinningAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt from EmployeeDetails EmpDet inner Join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo " +
    //                    " inner Join MstDepartment as MstDpt on MstDpt.DepartmentCd = EmpDet.Department inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo inner Join SalaryMaster SM on SM.EmpNo=SalDet.EmpNo where " +
    //                    " SalDet.Month='" + ddlMonths.SelectedValue + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.StafforLabor='" + Stafflabour + "' and " +
    //                    " EmpDet.Ccode='" + SessionCcode + "' and EmpDet.Lcode='" + SessionLcode + "' and SalDet.WagesType='" + rbsalary.SelectedValue + "' and SalDet.FromDate = convert(datetime,'" + MyDate1 + "', 105) and SalDet.ToDate = Convert(Datetime,'" + MyDate2 + "', 105)";

    //            if (txtDivision.Text.ToString() != "" && txtDivision.Text.ToString() != "-----Select----")
    //            {
    //                query = query + " and EmpDet.Division='" + txtDivision.Text.ToString() + "'";
    //            }

    //            query = query + " And SalDet.Salarythrough='" + RdbCashBank.SelectedValue + "'" +
    //                    " group by SalDet.EmpNo,EmpDet.ExisistingCode,EmpDet.EmpName,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays, " +
    //                    "SalDet.NFh,SalDet.BasicandDA,SalDet.FDA,SalDet.VDA,SalDet.HRA,SalDet.HomeDays, SalDet.Advance,SalDet.Stamp,SalDet.Unioncg,SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.LeaveDays,SalDet.SalaryDate,SalDet.allowances1, " +
    //                    " SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.Deduction1,SalDet.Weekoff,SalDet.CL,SalDet.Deduction2, SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.Losspay,SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings," +
    //                    "SalDet.OverTime,MstDpt.DepartmentNm,SalDet.ProvidentFund,SalDet.ESI,SalDet.HRA,SalDet.WorkedDays, SalDet.PfSalary, SalDet.HalfNightAmt,SalDet.FullNightAmt,SalDet.SpinningAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt,EmpDet.OldID,MstDpt.DepartmentNm,SM.Base,SM.PFS Order by MstDpt.DepartmentNm,OrderToken Asc";

    //            Staff_Bank_Query = "Select '' as OTHours,SalDet.EmpNo,EmpDet.ExisistingCode,EmpDet.EmpName,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays, " +
    //                    " SalDet.NFh,SalDet.BasicandDA,(SalDet.BasicandDA - SalDet.ESI) as NetSalary,SalDet.FDA,SalDet.VDA,SalDet.HRA, SalDet.Advance,SalDet.Stamp,SalDet.Unioncg,(SalDet.WorkedDays + SalDet.NFh) as tot, " +
    //                    " CAST(EmpDet.ExisistingCode AS int) as OrderToken," +
    //                    " SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.LeaveDays,SalDet.SalaryDate,SalDet.allowances1," +
    //                    " SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.Deduction1,SalDet.Weekoff,SalDet.CL,SalDet.Deduction2, " +
    //                    " SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.Deduction5,SalDet.HomeDays,SalDet.Losspay,SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings,EmpDet.OldID,MstDpt.DepartmentNm,SM.Base,SM.PFS,  " +
    //                    " SalDet.OverTime,MstDpt.DepartmentNm,SalDet.ProvidentFund,SalDet.ESI,SalDet.HRA,SalDet.WorkedDays,sum(SalDet.NetPay - (SalDet.GrossEarnings - SalDet.TotalDeductions)) as Roundoff," +
    //                    " SalDet.PfSalary as PFEarnings,SalDet.HalfNightAmt,SalDet.FullNightAmt,SalDet.SpinningAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt from EmployeeDetails EmpDet inner Join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo " +
    //                    " inner Join MstDepartment as MstDpt on MstDpt.DepartmentCd = EmpDet.Department inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo inner Join SalaryMaster SM on SM.EmpNo=SalDet.EmpNo where " +
    //                    " SalDet.Month='" + ddlMonths.SelectedValue + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.StafforLabor='S' and " +
    //                    " EmpDet.Ccode='" + SessionCcode + "' and EmpDet.Lcode='" + SessionLcode + "' and SalDet.WagesType='" + rbsalary.SelectedValue + "' and SalDet.FromDate = convert(datetime,'" + MyDate1 + "', 105) and SalDet.ToDate = Convert(Datetime,'" + MyDate2 + "', 105)" +
    //                    " And SalDet.Salarythrough='2'";
    //            if (ddldept.SelectedValue != "0" && ddldept.SelectedValue != "")
    //            {
    //                Staff_Bank_Query = Staff_Bank_Query + " and EmpDet.Department='" + ddldept.SelectedValue + "'";
    //            }

    //            if (txtDivision.Text.ToString() != "" && txtDivision.Text.ToString() != "-----Select----")
    //            {
    //                Staff_Bank_Query = Staff_Bank_Query + " and EmpDet.Division='" + txtDivision.Text.ToString() + "'";
    //            }

    //            Staff_Bank_Query = Staff_Bank_Query + " group by SalDet.EmpNo,EmpDet.ExisistingCode,EmpDet.EmpName,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays, " +
    //                    "SalDet.NFh,SalDet.BasicandDA,SalDet.FDA,SalDet.VDA,SalDet.HRA,SalDet.HomeDays, SalDet.Advance,SalDet.Stamp,SalDet.Unioncg,SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.LeaveDays,SalDet.SalaryDate,SalDet.allowances1, " +
    //                    " SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.Deduction1,SalDet.Weekoff,SalDet.CL,SalDet.Deduction2, SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.Losspay,SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings," +
    //                    "SalDet.OverTime,MstDpt.DepartmentNm,SalDet.ProvidentFund,SalDet.ESI,SalDet.HRA,SalDet.WorkedDays, SalDet.PfSalary, SalDet.HalfNightAmt,SalDet.FullNightAmt,SalDet.SpinningAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt,EmpDet.OldID,MstDpt.DepartmentNm,SM.Base,SM.PFS Order by MstDpt.DepartmentNm,OrderToken Asc";
    //            Staff_Cash_Query = "Select '' as OTHours,SalDet.EmpNo,EmpDet.ExisistingCode,EmpDet.EmpName,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays, " +
    //                    " SalDet.NFh,SalDet.BasicandDA,(SalDet.BasicandDA - SalDet.ESI) as NetSalary,SalDet.FDA,SalDet.VDA,SalDet.HRA, SalDet.Advance,SalDet.Stamp,SalDet.Unioncg,(SalDet.WorkedDays + SalDet.NFh) as tot, " +
    //                    " CAST(EmpDet.ExisistingCode AS int) as OrderToken," +
    //                    " SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.LeaveDays,SalDet.SalaryDate,SalDet.allowances1," +
    //                    " SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.Deduction1,SalDet.Weekoff,SalDet.CL,SalDet.Deduction2, " +
    //                    " SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.Deduction5,SalDet.HomeDays,SalDet.Losspay,SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings,EmpDet.OldID,MstDpt.DepartmentNm,SM.Base,SM.PFS,  " +
    //                    " SalDet.OverTime,MstDpt.DepartmentNm,SalDet.ProvidentFund,SalDet.ESI,SalDet.HRA,SalDet.WorkedDays,sum(SalDet.NetPay - (SalDet.GrossEarnings - SalDet.TotalDeductions)) as Roundoff," +
    //                    " SalDet.PfSalary as PFEarnings,SalDet.HalfNightAmt,SalDet.FullNightAmt,SalDet.SpinningAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt from EmployeeDetails EmpDet inner Join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo " +
    //                    " inner Join MstDepartment as MstDpt on MstDpt.DepartmentCd = EmpDet.Department inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo inner Join SalaryMaster SM on SM.EmpNo=SalDet.EmpNo where " +
    //                    " SalDet.Month='" + ddlMonths.SelectedValue + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.StafforLabor='S' and " +
    //                    " EmpDet.Ccode='" + SessionCcode + "' and EmpDet.Lcode='" + SessionLcode + "' and SalDet.WagesType='" + rbsalary.SelectedValue + "' and SalDet.FromDate = convert(datetime,'" + MyDate1 + "', 105) and SalDet.ToDate = Convert(Datetime,'" + MyDate2 + "', 105)" +
    //                    " And SalDet.Salarythrough='1'";
    //            if (ddldept.SelectedValue != "0" && ddldept.SelectedValue != "")
    //            {
    //                Staff_Cash_Query = Staff_Cash_Query + " and EmpDet.Department='" + ddldept.SelectedValue + "'";
    //            }
    //            if (txtDivision.Text.ToString() != "" && txtDivision.Text.ToString() != "-----Select----")
    //            {
    //                Staff_Cash_Query = Staff_Cash_Query + " and EmpDet.Division='" + txtDivision.Text.ToString() + "'";
    //            }
    //            Staff_Cash_Query = Staff_Cash_Query + " group by SalDet.EmpNo,EmpDet.ExisistingCode,EmpDet.EmpName,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays, " +
    //                    "SalDet.NFh,SalDet.BasicandDA,SalDet.FDA,SalDet.VDA,SalDet.HRA,SalDet.HomeDays, SalDet.Advance,SalDet.Stamp,SalDet.Unioncg,SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.LeaveDays,SalDet.SalaryDate,SalDet.allowances1, " +
    //                    " SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.Deduction1,SalDet.Weekoff,SalDet.CL,SalDet.Deduction2, SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.Losspay,SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings," +
    //                    "SalDet.OverTime,MstDpt.DepartmentNm,SalDet.ProvidentFund,SalDet.ESI,SalDet.HRA,SalDet.WorkedDays, SalDet.PfSalary, SalDet.HalfNightAmt,SalDet.FullNightAmt,SalDet.SpinningAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt,EmpDet.OldID,MstDpt.DepartmentNm,SM.Base,SM.PFS Order by MstDpt.DepartmentNm,OrderToken Asc";

    //            Labour_Bank_Query = "Select '' as OTHours,SalDet.EmpNo,EmpDet.ExisistingCode,EmpDet.EmpName,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays, " +
    //                    " SalDet.NFh,SalDet.BasicandDA,(SalDet.BasicandDA - SalDet.ESI) as NetSalary,SalDet.FDA,SalDet.VDA,SalDet.HRA, SalDet.Advance,SalDet.Stamp,SalDet.Unioncg,(SalDet.WorkedDays + SalDet.NFh) as tot, " +
    //                    " CAST(EmpDet.ExisistingCode AS int) as OrderToken," +
    //                    " SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.LeaveDays,SalDet.SalaryDate,SalDet.allowances1," +
    //                    " SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.Deduction1,SalDet.Weekoff,SalDet.CL,SalDet.Deduction2, " +
    //                    " SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.Deduction5,SalDet.HomeDays,SalDet.Losspay,SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings,EmpDet.OldID,MstDpt.DepartmentNm,SM.Base,SM.PFS,  " +
    //                    " SalDet.OverTime,MstDpt.DepartmentNm,SalDet.ProvidentFund,SalDet.ESI,SalDet.HRA,SalDet.WorkedDays,sum(SalDet.NetPay - (SalDet.GrossEarnings - SalDet.TotalDeductions)) as Roundoff," +
    //                    " SalDet.PfSalary as PFEarnings,SalDet.HalfNightAmt,SalDet.FullNightAmt,SalDet.SpinningAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt from EmployeeDetails EmpDet inner Join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo " +
    //                    " inner Join MstDepartment as MstDpt on MstDpt.DepartmentCd = EmpDet.Department inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo inner Join SalaryMaster SM on SM.EmpNo=SalDet.EmpNo where " +
    //                    " SalDet.Month='" + ddlMonths.SelectedValue + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.StafforLabor='L' and " +
    //                    " EmpDet.Ccode='" + SessionCcode + "' and EmpDet.Lcode='" + SessionLcode + "' and SalDet.WagesType='" + rbsalary.SelectedValue + "' and SalDet.FromDate = convert(datetime,'" + MyDate1 + "', 105) and SalDet.ToDate = Convert(Datetime,'" + MyDate2 + "', 105)" +
    //                    " And SalDet.Salarythrough='2'";
    //            if (ddldept.SelectedValue != "0" && ddldept.SelectedValue != "")
    //            {
    //                Labour_Bank_Query = Labour_Bank_Query + " and EmpDet.Department='" + ddldept.SelectedValue + "'";
    //            }
    //            if (txtDivision.Text.ToString() != "" && txtDivision.Text.ToString() != "-----Select----")
    //            {
    //                Labour_Bank_Query = Labour_Bank_Query + " and EmpDet.Division='" + txtDivision.Text.ToString() + "'";
    //            }
    //            Labour_Bank_Query = Labour_Bank_Query + " group by SalDet.EmpNo,EmpDet.ExisistingCode,EmpDet.EmpName,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays, " +
    //                    "SalDet.NFh,SalDet.BasicandDA,SalDet.FDA,SalDet.VDA,SalDet.HRA,SalDet.HomeDays, SalDet.Advance,SalDet.Stamp,SalDet.Unioncg,SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.LeaveDays,SalDet.SalaryDate,SalDet.allowances1, " +
    //                    " SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.Deduction1,SalDet.Weekoff,SalDet.CL,SalDet.Deduction2, SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.Losspay,SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings," +
    //                    "SalDet.OverTime,MstDpt.DepartmentNm,SalDet.ProvidentFund,SalDet.ESI,SalDet.HRA,SalDet.WorkedDays, SalDet.PfSalary, SalDet.HalfNightAmt,SalDet.FullNightAmt,SalDet.SpinningAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt,EmpDet.OldID,MstDpt.DepartmentNm,SM.Base,SM.PFS Order by MstDpt.DepartmentNm,OrderToken Asc";
    //            Labour_Cash_Query = "Select '' as OTHours,SalDet.EmpNo,EmpDet.ExisistingCode,EmpDet.EmpName,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays, " +
    //                    " SalDet.NFh,SalDet.BasicandDA,(SalDet.BasicandDA - SalDet.ESI) as NetSalary,SalDet.FDA,SalDet.VDA,SalDet.HRA, SalDet.Advance,SalDet.Stamp,SalDet.Unioncg,(SalDet.WorkedDays + SalDet.NFh) as tot, " +
    //                    " CAST(EmpDet.ExisistingCode AS int) as OrderToken," +
    //                    " SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.LeaveDays,SalDet.SalaryDate,SalDet.allowances1," +
    //                    " SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.Deduction1,SalDet.Weekoff,SalDet.CL,SalDet.Deduction2, " +
    //                    " SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.Deduction5,SalDet.HomeDays,SalDet.Losspay,SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings,EmpDet.OldID,MstDpt.DepartmentNm,SM.Base,SM.PFS,  " +
    //                    " SalDet.OverTime,MstDpt.DepartmentNm,SalDet.ProvidentFund,SalDet.ESI,SalDet.HRA,SalDet.WorkedDays,sum(SalDet.NetPay - (SalDet.GrossEarnings - SalDet.TotalDeductions)) as Roundoff," +
    //                    " SalDet.PfSalary as PFEarnings,SalDet.HalfNightAmt,SalDet.FullNightAmt,SalDet.SpinningAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt from EmployeeDetails EmpDet inner Join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo " +
    //                    " inner Join MstDepartment as MstDpt on MstDpt.DepartmentCd = EmpDet.Department inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo inner Join SalaryMaster SM on SM.EmpNo=SalDet.EmpNo where " +
    //                    " SalDet.Month='" + ddlMonths.SelectedValue + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.StafforLabor='L' and " +
    //                    " EmpDet.Ccode='" + SessionCcode + "' and EmpDet.Lcode='" + SessionLcode + "' and SalDet.WagesType='" + rbsalary.SelectedValue + "' and SalDet.FromDate = convert(datetime,'" + MyDate1 + "', 105) and SalDet.ToDate = Convert(Datetime,'" + MyDate2 + "', 105)" +
    //                    " And SalDet.Salarythrough='1'";
    //            if (ddldept.SelectedValue != "0" && ddldept.SelectedValue != "")
    //            {
    //                Labour_Cash_Query = Labour_Cash_Query + " and EmpDet.Department='" + ddldept.SelectedValue + "'";
    //            }
    //            if (txtDivision.Text.ToString() != "" && txtDivision.Text.ToString() != "-----Select----")
    //            {
    //                Labour_Cash_Query = Labour_Cash_Query + " and EmpDet.Division='" + txtDivision.Text.ToString() + "'";
    //            }
    //            Labour_Cash_Query = Labour_Cash_Query + " group by SalDet.EmpNo,EmpDet.ExisistingCode,EmpDet.EmpName,EmpDet.Designation,EmpDet.Department,SalDet.Totalworkingdays, " +
    //                    "SalDet.NFh,SalDet.BasicandDA,SalDet.FDA,SalDet.VDA,SalDet.HRA,SalDet.HomeDays, SalDet.Advance,SalDet.Stamp,SalDet.Unioncg,SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.LeaveDays,SalDet.SalaryDate,SalDet.allowances1, " +
    //                    " SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.Deduction1,SalDet.Weekoff,SalDet.CL,SalDet.Deduction2, SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.Losspay,SalDet.NetPay,SalDet.TotalDeductions,SalDet.GrossEarnings," +
    //                    "SalDet.OverTime,MstDpt.DepartmentNm,SalDet.ProvidentFund,SalDet.ESI,SalDet.HRA,SalDet.WorkedDays, SalDet.PfSalary, SalDet.HalfNightAmt,SalDet.FullNightAmt,SalDet.SpinningAmt,SalDet.DayIncentive,SalDet.ThreesidedAmt,EmpDet.OldID,MstDpt.DepartmentNm,SM.Base,SM.PFS Order by MstDpt.DepartmentNm,OrderToken Asc";
    //            //Staff_Bank_Query = ""; Staff_Cash_Query = "";

    //        }

    //        SqlCommand cmd = new SqlCommand(query, con);
    //        DataTable dt_1 = new DataTable();
    //        SqlDataAdapter sda = new SqlDataAdapter(cmd);
    //        //DataSet ds1 = new DataSet();
    //        con.Open();
    //        sda.Fill(dt_1);
    //        con.Close();
    //        gvSalary.DataSource = dt_1;
    //        gvSalary.DataBind();
    //        foreach (GridViewRow gvsal in gvSalary.Rows)
    //        {
    //            Label lbl_EmpNo = (Label)gvsal.FindControl("lblEmpNo");
    //            Label lbl_OTHours = (Label)gvsal.FindControl("lblOThours");
    //            string qry_ot = "";
    //            if (rbsalary.SelectedValue == "2")
    //            {
    //                qry_ot = "Select NoHrs from OverTime where EmpNo='" + lbl_EmpNo.Text.Trim() + "' and Lcode='" + SessionLcode + "' and Ccode='" + SessionCcode + "' and Month='" + ddlMonths.SelectedValue + "' and Financialyr='" + ddlFinance.SelectedValue + "'";
    //            }
    //            else
    //            {
    //                qry_ot = "Select NoHrs from OverTime where EmpNo='" + lbl_EmpNo.Text.Trim() + "' and Lcode='" + SessionLcode + "' and Ccode='" + SessionCcode + "' and Month='" + ddlMonths.SelectedValue + "' and Financialyr='" + ddlFinance.SelectedValue + "'" +
    //                                " and convert(Datetime, FromDate, 105)=convert(Datetime,'" + MyDate1 + "', 105) and convert(Datetime, ToDate, 105)=convert(datetime,'" + MyDate2 + "', 105)";

    //            }
    //            SqlCommand cmd_ot = new SqlCommand(qry_ot, con);
    //            con.Open();
    //            string val_ot = Convert.ToString(cmd_ot.ExecuteScalar());
    //            con.Close();
    //            if (val_ot.Trim() == "")
    //            {
    //                lbl_OTHours.Text = "0";
    //            }
    //            else
    //            {
    //                lbl_OTHours.Text = val_ot;
    //            }
    //        }
    //        string attachment = "attachment;filename=SalaryDetails.xls";
    //        Response.ClearContent();
    //        Response.AddHeader("content-disposition", attachment);
    //        Response.ContentType = "application/ms-excel";
    //        DataTable dt = new DataTable();
    //        dt = objdata.Company_retrive(SessionCcode, SessionLcode);
    //        if (dt.Rows.Count > 0)
    //        {
    //            CmpName = dt.Rows[0]["Cname"].ToString();
    //            Cmpaddress = (dt.Rows[0]["Address1"].ToString() + ", " + dt.Rows[0]["Address2"].ToString() + ", " + dt.Rows[0]["Location"].ToString() + "-" + dt.Rows[0]["Pincode"].ToString());
    //        }

    //        StringWriter stw = new StringWriter();
    //        HtmlTextWriter htextw = new HtmlTextWriter(stw);
    //        gvSalary.RenderControl(htextw);
    //        Response.Write("<table style='font-weight:bold;'>");
    //        Response.Write("<tr align='Center' style='font-size:18.0pt; text-decoration:underline;'>");
    //        Response.Write("<td colspan='31'>");
    //        //Response.Write("" + CmpName + " - PAYSLIP REPORT");
    //        Response.Write("MURUGAN TEXTILES");
    //        Response.Write("</td>");
    //        Response.Write("</tr>");
    //        Response.Write("<tr align='Center' style='font-size:12.0pt; text-decoration:underline;'>");
    //        Response.Write("<td colspan='31'>");
    //        Response.Write("" + SessionLcode + "");
    //        Response.Write("</td>");
    //        Response.Write("</tr>");
    //        Response.Write("<tr align='Center' style='font-size:12.0pt; text-decoration:underline;'>");
    //        Response.Write("<td colspan='31'>");
    //        Response.Write("" + Cmpaddress + "");
    //        Response.Write("</td>");
    //        Response.Write("</tr>");
    //        if (rbsalary.SelectedValue == "2")
    //        {
    //            Response.Write("<tr align='Center' style='font-size:12.0pt; text-decoration:underline;'>");
    //            Response.Write("<td colspan='31'>");
    //            Response.Write("Salary Month of " + ddlMonths.SelectedValue + " - " + YR);
    //            Response.Write("</td>");
    //            Response.Write("</tr>");
    //        }
    //        else
    //        {
    //            Response.Write("<tr align='Center' style='font-size:12.0pt; text-decoration:underline;'>");
    //            Response.Write("<td colspan='31'>");
    //            Response.Write("Salary Month of " + txtfrom.Text + " - " + txtTo.Text);
    //            Response.Write("</td>");
    //            Response.Write("</tr>");
    //        }
    //        Response.Write("</table>");


    //        //PAYSLIP DESIGN IN EXCEL
    //        string Check_Heading_Add_OR_Not = "";
    //        Int32 Grand_Total_Count = 6;
    //        Int32 Grand_From_Count = 0;
    //        Int32 Grand_To_Count = 0;

    //        //BANK STAFF Payslip Design
    //        cmd = new SqlCommand(Staff_Bank_Query, con);
    //        dt_1 = new DataTable();
    //        sda = new SqlDataAdapter(cmd);
    //        //DataSet ds1 = new DataSet();
    //        con.Open();
    //        sda.Fill(dt_1);
    //        con.Close();
    //        //Fill OT Hours            
    //        for (int i = 0; i < dt_1.Rows.Count; i++)
    //        {
    //            string EmpNo = dt_1.Rows[i]["EmpNo"].ToString();
    //            string OTHours = "";
    //            string qry_ot = "";
    //            if (rbsalary.SelectedValue == "2")
    //            {
    //                qry_ot = "Select NoHrs from OverTime where EmpNo='" + EmpNo + "' and Lcode='" + SessionLcode + "' and Ccode='" + SessionCcode + "' and Month='" + ddlMonths.SelectedValue + "' and Financialyr='" + ddlFinance.SelectedValue + "'";
    //            }
    //            else
    //            {
    //                qry_ot = "Select NoHrs from OverTime where EmpNo='" + EmpNo + "' and Lcode='" + SessionLcode + "' and Ccode='" + SessionCcode + "' and Month='" + ddlMonths.SelectedValue + "' and Financialyr='" + ddlFinance.SelectedValue + "'" +
    //                                " and convert(Datetime, FromDate, 105)=convert(Datetime,'" + MyDate1 + "', 105) and convert(Datetime, ToDate, 105)=convert(datetime,'" + MyDate2 + "', 105)";

    //            }
    //            SqlCommand cmd_ot = new SqlCommand(qry_ot, con);
    //            con.Open();
    //            string val_ot = Convert.ToString(cmd_ot.ExecuteScalar());
    //            con.Close();
    //            if (val_ot.Trim() == "")
    //            {
    //                OTHours = "0".ToString();
    //            }
    //            else
    //            {
    //                OTHours = val_ot.ToString();
    //            }
    //            dt_1.Rows[i]["OTHours"] = OTHours.ToString();
    //        }

    //        for (int i = 0; i < dt_1.Rows.Count; i++) //Staff Bank Payslip Add
    //        {
    //            Response.Write("<table border='1'>");
    //            Response.Write("<tr>");
    //            if (i == 0)
    //            {
    //                Check_Heading_Add_OR_Not = "1";
    //                SI_Count = 1;
    //                Grand_From_Count = 7;
    //                Department_Name_Check = dt_1.Rows[i]["DepartmentNm"].ToString();
    //                Response.Write("<td colspan='5' align='left' style='font-weight:bold;'>STAFFS/LABOUR      : STAFFS</td><td colspan='6' align='center' style='font-weight:bold;'>DEPARTMENT            : " + dt_1.Rows[i]["DepartmentNm"].ToString() + "</td><td colspan='20' align='left' style='font-weight:bold;'>SALARY THROUGH       : BANK</td>");
    //                Response.Write("<tr align='center' valign='center' style='font-weight:bold;'>");
    //                Response.Write("<td>Si. No</td><td>Emp No</td><td>Ticket No</td><td>OldID</td><td>Name</td><td>Working Days</td><td>CL</td><td>Week Off</td><td>OT Hours</td><td>Fixed Salary</td><td>Fixed OT Salary</td><td>Base</td><td>P.F</td><td>ESI</td><td>Advance</td><td>Allowance 1</td><td>Allowance 2</td><td>Allowance 3</td><td>Allowance 4</td><td>Allowance 5</td><td>Deduction 1</td><td>Deduction 2</td><td>Deduction 3</td><td>Deduction 4</td><td>Deduction 5</td><td>LOP</td><td>Total Deduction</td><td>Net Salary</td><td>OT</td><td>Net Amount</td><td style='width: 10.5px;' width='400'>Signature     </td>");
    //                Response.Write("</tr><tr>");
    //                Response.Write("<td>" + SI_Count.ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["EmpNo"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["ExisistingCode"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["OldID"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["EmpName"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["WorkedDays"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["CL"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["Weekoff"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["OTHours"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["Base"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["PFS"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["BasicandDA"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["ProvidentFund"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["ESI"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["Advance"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["allowances1"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["allowances2"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["allowances3"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["allowances4"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["allowances5"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["Deduction1"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["Deduction2"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["Deduction3"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["Deduction4"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["Deduction5"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["Losspay"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["TotalDeductions"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["NetSalary"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["OverTime"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["NetPay"].ToString() + "</td>");
    //            }
    //            else
    //            {
    //                if (Department_Name_Check == dt_1.Rows[i]["DepartmentNm"].ToString())
    //                {
    //                    Response.Write("<td>" + SI_Count.ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["EmpNo"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["ExisistingCode"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["OldID"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["EmpName"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["WorkedDays"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["CL"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Weekoff"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["OTHours"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Base"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["PFS"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["BasicandDA"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["ProvidentFund"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["ESI"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Advance"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["allowances1"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["allowances2"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["allowances3"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["allowances4"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["allowances5"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Deduction1"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Deduction2"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Deduction3"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Deduction4"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Deduction5"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Losspay"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["TotalDeductions"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["NetSalary"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["OverTime"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["NetPay"].ToString() + "</td>");
    //                    Response.Write("<td></td>");
    //                }
    //                else
    //                {
    //                    if (SI_Count == 2)
    //                    {
    //                        Grand_To_Count = Grand_From_Count;
    //                    }
    //                    else
    //                    {
    //                        Grand_To_Count = SI_Count - 1;
    //                        Grand_To_Count = Grand_From_Count + Grand_To_Count;
    //                        Grand_To_Count = Grand_To_Count - 1;
    //                    }
    //                    Response.Write("<td colspan='5' align='right' style='font-size:12.0pt;font-weight:bold;'>Grand Total</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(F" + Grand_From_Count.ToString() + ":F" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(G" + Grand_From_Count.ToString() + ":G" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(H" + Grand_From_Count.ToString() + ":H" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(I" + Grand_From_Count.ToString() + ":I" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(J" + Grand_From_Count.ToString() + ":J" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(K" + Grand_From_Count.ToString() + ":K" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(L" + Grand_From_Count.ToString() + ":L" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(M" + Grand_From_Count.ToString() + ":M" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(N" + Grand_From_Count.ToString() + ":N" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(O" + Grand_From_Count.ToString() + ":O" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(P" + Grand_From_Count.ToString() + ":P" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(Q" + Grand_From_Count.ToString() + ":Q" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(R" + Grand_From_Count.ToString() + ":R" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(S" + Grand_From_Count.ToString() + ":S" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(T" + Grand_From_Count.ToString() + ":T" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(U" + Grand_From_Count.ToString() + ":U" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(V" + Grand_From_Count.ToString() + ":V" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(W" + Grand_From_Count.ToString() + ":W" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(X" + Grand_From_Count.ToString() + ":X" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(Y" + Grand_From_Count.ToString() + ":Y" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(Z" + Grand_From_Count.ToString() + ":Z" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(AA" + Grand_From_Count.ToString() + ":AA" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(AB" + Grand_From_Count.ToString() + ":AB" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(AC" + Grand_From_Count.ToString() + ":AC" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(AD" + Grand_From_Count.ToString() + ":AD" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td></td></tr></table><table border='1'><tr>");


    //                    Grand_From_Count = Grand_To_Count + 4;
    //                    SI_Count = 1;
    //                    Department_Name_Check = dt_1.Rows[i]["DepartmentNm"].ToString();
    //                    Response.Write("<td colspan='31'></td></tr></table><table border='1'><tr style='font-weight:bold;'>");
    //                    Response.Write("<td colspan='5' align='left' style='font-weight:bold;'>STAFFS/LABOUR      : STAFFS</td><td colspan='6' align='center' style='font-weight:bold;'>DEPARTMENT            : " + dt_1.Rows[i]["DepartmentNm"].ToString() + "</td><td colspan='20' align='left' style='font-weight:bold;'>SALARY THROUGH       : BANK</td></tr><tr>");
    //                    Response.Write("<td>" + SI_Count.ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["EmpNo"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["ExisistingCode"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["OldID"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["EmpName"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["WorkedDays"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["CL"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Weekoff"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["OTHours"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Base"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["PFS"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["BasicandDA"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["ProvidentFund"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["ESI"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Advance"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["allowances1"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["allowances2"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["allowances3"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["allowances4"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["allowances5"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Deduction1"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Deduction2"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Deduction3"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Deduction4"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Deduction5"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Losspay"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["TotalDeductions"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["NetSalary"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["OverTime"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["NetPay"].ToString() + "</td>");
    //                    Response.Write("<td></td>");
    //                }
    //            }
    //            Response.Write("</tr>");
    //            Response.Write("</table>");
    //            SI_Count++;
    //        }
    //        //GRAND Total
    //        if (dt_1.Rows.Count.ToString() != "0")
    //        {
    //            if (SI_Count == 2)
    //            {
    //                Grand_To_Count = Grand_From_Count;
    //            }
    //            else
    //            {
    //                Grand_To_Count = SI_Count - 1;
    //                Grand_To_Count = Grand_From_Count + Grand_To_Count;
    //                Grand_To_Count = Grand_To_Count - 1;
    //            }
    //            Response.Write("<table border='1'><tr style='font-size:12.0pt;font-weight:bold;'>");
    //            Response.Write("<td colspan='5' align='right'>Grand Total</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(F" + Grand_From_Count.ToString() + ":F" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(G" + Grand_From_Count.ToString() + ":G" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(H" + Grand_From_Count.ToString() + ":H" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(I" + Grand_From_Count.ToString() + ":I" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(J" + Grand_From_Count.ToString() + ":J" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(K" + Grand_From_Count.ToString() + ":K" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(L" + Grand_From_Count.ToString() + ":L" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(M" + Grand_From_Count.ToString() + ":M" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(N" + Grand_From_Count.ToString() + ":N" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(O" + Grand_From_Count.ToString() + ":O" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(P" + Grand_From_Count.ToString() + ":P" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(Q" + Grand_From_Count.ToString() + ":Q" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(R" + Grand_From_Count.ToString() + ":R" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(S" + Grand_From_Count.ToString() + ":S" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(T" + Grand_From_Count.ToString() + ":T" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(U" + Grand_From_Count.ToString() + ":U" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(V" + Grand_From_Count.ToString() + ":V" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(W" + Grand_From_Count.ToString() + ":W" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(X" + Grand_From_Count.ToString() + ":X" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(Y" + Grand_From_Count.ToString() + ":Y" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(Z" + Grand_From_Count.ToString() + ":Z" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(AA" + Grand_From_Count.ToString() + ":AA" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(AB" + Grand_From_Count.ToString() + ":AB" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(AC" + Grand_From_Count.ToString() + ":AC" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(AD" + Grand_From_Count.ToString() + ":AD" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td></td></tr></table>");
    //            Grand_From_Count = Grand_To_Count + 4;
    //        }

    //        //CASH STAFF Payslip Design
    //        cmd = new SqlCommand(Staff_Cash_Query, con);
    //        dt_1 = new DataTable();
    //        sda = new SqlDataAdapter(cmd);
    //        //DataSet ds1 = new DataSet();
    //        con.Open();
    //        sda.Fill(dt_1);
    //        con.Close();
    //        //Fill OT Hours            
    //        for (int i = 0; i < dt_1.Rows.Count; i++)
    //        {
    //            string EmpNo = dt_1.Rows[i]["EmpNo"].ToString();
    //            string OTHours = "";
    //            string qry_ot = "";
    //            if (rbsalary.SelectedValue == "2")
    //            {
    //                qry_ot = "Select NoHrs from OverTime where EmpNo='" + EmpNo + "' and Lcode='" + SessionLcode + "' and Ccode='" + SessionCcode + "' and Month='" + ddlMonths.SelectedValue + "' and Financialyr='" + ddlFinance.SelectedValue + "'";
    //            }
    //            else
    //            {
    //                qry_ot = "Select NoHrs from OverTime where EmpNo='" + EmpNo + "' and Lcode='" + SessionLcode + "' and Ccode='" + SessionCcode + "' and Month='" + ddlMonths.SelectedValue + "' and Financialyr='" + ddlFinance.SelectedValue + "'" +
    //                                " and convert(Datetime, FromDate, 105)=convert(Datetime,'" + MyDate1 + "', 105) and convert(Datetime, ToDate, 105)=convert(datetime,'" + MyDate2 + "', 105)";

    //            }
    //            SqlCommand cmd_ot = new SqlCommand(qry_ot, con);
    //            con.Open();
    //            string val_ot = Convert.ToString(cmd_ot.ExecuteScalar());
    //            con.Close();
    //            if (val_ot.Trim() == "")
    //            {
    //                OTHours = "0".ToString();
    //            }
    //            else
    //            {
    //                OTHours = val_ot.ToString();
    //            }
    //            dt_1.Rows[i]["OTHours"] = OTHours.ToString();
    //        }

    //        for (int i = 0; i < dt_1.Rows.Count; i++) //Staff CASH Payslip Add
    //        {
    //            Response.Write("<table border='1'>");
    //            Response.Write("<tr>");
    //            if (i == 0)
    //            {
    //                Department_Name_Check = dt_1.Rows[i]["DepartmentNm"].ToString();
    //                if (Check_Heading_Add_OR_Not == "")
    //                {
    //                    Check_Heading_Add_OR_Not = "1";
    //                    Grand_From_Count = 7;
    //                    Response.Write("<td colspan='5' align='left' style='font-weight:bold;'>STAFFS/LABOUR      : STAFFS</td><td colspan='6' align='center' style='font-weight:bold;'>DEPARTMENT            : " + dt_1.Rows[i]["DepartmentNm"].ToString() + "</td><td colspan='20' align='left' style='font-weight:bold;'>SALARY THROUGH       : CASH</td>");
    //                    Response.Write("<tr align='center' valign='center' style='font-weight:bold;'>");
    //                    Response.Write("<td>Si. No</td><td>Emp No</td><td>Ticket No</td><td>OldID</td><td>Name</td><td>Working Days</td><td>CL</td><td>Week Off</td><td>OT Hours</td><td>Fixed Salary</td><td>Fixed OT Salary</td><td>Base</td><td>P.F</td><td>ESI</td><td>Advance</td><td>Allowance 1</td><td>Allowance 2</td><td>Allowance 3</td><td>Allowance 4</td><td>Allowance 5</td><td>Deduction 1</td><td>Deduction 2</td><td>Deduction 3</td><td>Deduction 4</td><td>Deduction 5</td><td>LOP</td><td>Total Deduction</td><td>Net Salary</td><td>OT</td><td>Net Amount</td><td>Signature     </td>");
    //                    Response.Write("</tr><tr>");
    //                }
    //                else
    //                {
    //                    Response.Write("<td colspan='31'></td></tr></table>");
    //                    Response.Write("<table border='1'><tr style='font-weight:bold;'><td colspan='5' align='left' style='font-weight:bold;'>STAFFS/LABOUR      : STAFFS</td><td colspan='6' align='center' style='font-weight:bold;'>DEPARTMENT            : " + dt_1.Rows[i]["DepartmentNm"].ToString() + "</td><td colspan='20' align='left' style='font-weight:bold;'>SALARY THROUGH       : CASH</td>");
    //                    Response.Write("</tr><tr>");
    //                }
    //                SI_Count = 1;
    //                Response.Write("<td>" + SI_Count.ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["EmpNo"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["ExisistingCode"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["OldID"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["EmpName"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["WorkedDays"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["CL"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["Weekoff"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["OTHours"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["Base"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["PFS"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["BasicandDA"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["ProvidentFund"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["ESI"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["Advance"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["allowances1"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["allowances2"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["allowances3"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["allowances4"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["allowances5"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["Deduction1"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["Deduction2"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["Deduction3"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["Deduction4"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["Deduction5"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["Losspay"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["TotalDeductions"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["NetSalary"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["OverTime"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["NetPay"].ToString() + "</td>");
    //            }
    //            else
    //            {
    //                if (Department_Name_Check == dt_1.Rows[i]["DepartmentNm"].ToString())
    //                {
    //                    Response.Write("<td>" + SI_Count.ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["EmpNo"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["ExisistingCode"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["OldID"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["EmpName"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["WorkedDays"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["CL"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Weekoff"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["OTHours"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Base"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["PFS"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["BasicandDA"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["ProvidentFund"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["ESI"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Advance"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["allowances1"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["allowances2"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["allowances3"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["allowances4"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["allowances5"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Deduction1"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Deduction2"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Deduction3"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Deduction4"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Deduction5"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Losspay"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["TotalDeductions"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["NetSalary"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["OverTime"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["NetPay"].ToString() + "</td>");
    //                    Response.Write("<td></td>");
    //                }
    //                else
    //                {
    //                    if (SI_Count == 2)
    //                    {
    //                        Grand_To_Count = Grand_From_Count;
    //                    }
    //                    else
    //                    {
    //                        Grand_To_Count = SI_Count - 1;
    //                        Grand_To_Count = Grand_From_Count + Grand_To_Count;
    //                        Grand_To_Count = Grand_To_Count - 1;
    //                    }
    //                    Response.Write("<td colspan='5' align='right' style='font-size:12.0pt;font-weight:bold;'>Grand Total</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(F" + Grand_From_Count.ToString() + ":F" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(G" + Grand_From_Count.ToString() + ":G" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(H" + Grand_From_Count.ToString() + ":H" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(I" + Grand_From_Count.ToString() + ":I" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(J" + Grand_From_Count.ToString() + ":J" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(K" + Grand_From_Count.ToString() + ":K" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(L" + Grand_From_Count.ToString() + ":L" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(M" + Grand_From_Count.ToString() + ":M" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(N" + Grand_From_Count.ToString() + ":N" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(O" + Grand_From_Count.ToString() + ":O" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(P" + Grand_From_Count.ToString() + ":P" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(Q" + Grand_From_Count.ToString() + ":Q" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(R" + Grand_From_Count.ToString() + ":R" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(S" + Grand_From_Count.ToString() + ":S" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(T" + Grand_From_Count.ToString() + ":T" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(U" + Grand_From_Count.ToString() + ":U" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(V" + Grand_From_Count.ToString() + ":V" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(W" + Grand_From_Count.ToString() + ":W" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(X" + Grand_From_Count.ToString() + ":X" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(Y" + Grand_From_Count.ToString() + ":Y" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(Z" + Grand_From_Count.ToString() + ":Z" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(AA" + Grand_From_Count.ToString() + ":AA" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(AB" + Grand_From_Count.ToString() + ":AB" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(AC" + Grand_From_Count.ToString() + ":AC" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(AD" + Grand_From_Count.ToString() + ":AD" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td></td></tr></table><table border='1'><tr>");
    //                    Grand_From_Count = Grand_To_Count + 4;

    //                    SI_Count = 1;
    //                    Department_Name_Check = dt_1.Rows[i]["DepartmentNm"].ToString();
    //                    Response.Write("<td colspan='31'></td></tr></table><table border='1'><tr style='font-weight:bold;'>");
    //                    Response.Write("<td colspan='5' align='left' style='font-weight:bold;'>STAFFS/LABOUR      : STAFFS</td><td colspan='6' align='center' style='font-weight:bold;'>DEPARTMENT            : " + dt_1.Rows[i]["DepartmentNm"].ToString() + "</td><td colspan='20' align='left' style='font-weight:bold;'>SALARY THROUGH       : CASH</td></tr><tr>");
    //                    Response.Write("<td>" + SI_Count.ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["EmpNo"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["ExisistingCode"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["OldID"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["EmpName"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["WorkedDays"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["CL"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Weekoff"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["OTHours"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Base"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["PFS"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["BasicandDA"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["ProvidentFund"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["ESI"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Advance"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["allowances1"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["allowances2"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["allowances3"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["allowances4"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["allowances5"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Deduction1"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Deduction2"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Deduction3"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Deduction4"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Deduction5"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Losspay"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["TotalDeductions"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["NetSalary"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["OverTime"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["NetPay"].ToString() + "</td>");
    //                    Response.Write("<td></td>");
    //                }
    //            }
    //            Response.Write("</tr>");
    //            Response.Write("</table>");
    //            SI_Count++;
    //        }
    //        //GRAND Total
    //        if (dt_1.Rows.Count.ToString() != "0")
    //        {
    //            if (SI_Count == 2)
    //            {
    //                Grand_To_Count = Grand_From_Count;
    //            }
    //            else
    //            {
    //                Grand_To_Count = SI_Count - 1;
    //                Grand_To_Count = Grand_From_Count + Grand_To_Count;
    //                Grand_To_Count = Grand_To_Count - 1;
    //            }
    //            Response.Write("<table border='1'><tr style='font-size:12.0pt;font-weight:bold;'>");
    //            Response.Write("<td colspan='5' align='right'>Grand Total</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(F" + Grand_From_Count.ToString() + ":F" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(G" + Grand_From_Count.ToString() + ":G" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(H" + Grand_From_Count.ToString() + ":H" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(I" + Grand_From_Count.ToString() + ":I" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(J" + Grand_From_Count.ToString() + ":J" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(K" + Grand_From_Count.ToString() + ":K" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(L" + Grand_From_Count.ToString() + ":L" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(M" + Grand_From_Count.ToString() + ":M" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(N" + Grand_From_Count.ToString() + ":N" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(O" + Grand_From_Count.ToString() + ":O" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(P" + Grand_From_Count.ToString() + ":P" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(Q" + Grand_From_Count.ToString() + ":Q" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(R" + Grand_From_Count.ToString() + ":R" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(S" + Grand_From_Count.ToString() + ":S" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(T" + Grand_From_Count.ToString() + ":T" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(U" + Grand_From_Count.ToString() + ":U" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(V" + Grand_From_Count.ToString() + ":V" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(W" + Grand_From_Count.ToString() + ":W" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(X" + Grand_From_Count.ToString() + ":X" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(Y" + Grand_From_Count.ToString() + ":Y" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(Z" + Grand_From_Count.ToString() + ":Z" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(AA" + Grand_From_Count.ToString() + ":AA" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(AB" + Grand_From_Count.ToString() + ":AB" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(AC" + Grand_From_Count.ToString() + ":AC" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(AD" + Grand_From_Count.ToString() + ":AD" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td></td></tr></table>");
    //            Grand_From_Count = Grand_To_Count + 4;
    //        }


    //        //ALL LABOUR PAYSLIP
    //        //BANK LABOUR Payslip Design
    //        cmd = new SqlCommand(Labour_Bank_Query, con);
    //        dt_1 = new DataTable();
    //        sda = new SqlDataAdapter(cmd);
    //        //DataSet ds1 = new DataSet();
    //        con.Open();
    //        sda.Fill(dt_1);
    //        con.Close();
    //        //Fill OT Hours            
    //        for (int i = 0; i < dt_1.Rows.Count; i++)
    //        {
    //            string EmpNo = dt_1.Rows[i]["EmpNo"].ToString();
    //            string OTHours = "";
    //            string qry_ot = "";
    //            if (rbsalary.SelectedValue == "2")
    //            {
    //                qry_ot = "Select NoHrs from OverTime where EmpNo='" + EmpNo + "' and Lcode='" + SessionLcode + "' and Ccode='" + SessionCcode + "' and Month='" + ddlMonths.SelectedValue + "' and Financialyr='" + ddlFinance.SelectedValue + "'";
    //            }
    //            else
    //            {
    //                qry_ot = "Select NoHrs from OverTime where EmpNo='" + EmpNo + "' and Lcode='" + SessionLcode + "' and Ccode='" + SessionCcode + "' and Month='" + ddlMonths.SelectedValue + "' and Financialyr='" + ddlFinance.SelectedValue + "'" +
    //                                " and convert(Datetime, FromDate, 105)=convert(Datetime,'" + MyDate1 + "', 105) and convert(Datetime, ToDate, 105)=convert(datetime,'" + MyDate2 + "', 105)";

    //            }
    //            SqlCommand cmd_ot = new SqlCommand(qry_ot, con);
    //            con.Open();
    //            string val_ot = Convert.ToString(cmd_ot.ExecuteScalar());
    //            con.Close();
    //            if (val_ot.Trim() == "")
    //            {
    //                OTHours = "0".ToString();
    //            }
    //            else
    //            {
    //                OTHours = val_ot.ToString();
    //            }
    //            dt_1.Rows[i]["OTHours"] = OTHours.ToString();
    //        }

    //        for (int i = 0; i < dt_1.Rows.Count; i++) //LABOUR BANK Payslip Add
    //        {
    //            Response.Write("<table border='1'>");
    //            Response.Write("<tr>");
    //            if (i == 0)
    //            {
    //                Department_Name_Check = dt_1.Rows[i]["DepartmentNm"].ToString();
    //                if (Check_Heading_Add_OR_Not == "")
    //                {
    //                    Check_Heading_Add_OR_Not = "1";
    //                    Grand_From_Count = 7;
    //                    Response.Write("<td colspan='5' align='left' style='font-weight:bold;'>STAFFS/LABOUR      : LABOUR</td><td colspan='6' align='center' style='font-weight:bold;'>DEPARTMENT            : " + dt_1.Rows[i]["DepartmentNm"].ToString() + "</td><td colspan='20' align='left' style='font-weight:bold;'>SALARY THROUGH       : BANK</td>");
    //                    Response.Write("<tr align='center' valign='center' style='font-weight:bold;'>");
    //                    Response.Write("<td>Si. No</td><td>Emp No</td><td>Ticket No</td><td>OldID</td><td>Name</td><td>Working Days</td><td>CL</td><td>Week Off</td><td>OT Hours</td><td>Fixed Salary</td><td>Fixed OT Salary</td><td>Base</td><td>P.F</td><td>ESI</td><td>Advance</td><td>Allowance 1</td><td>Allowance 2</td><td>Allowance 3</td><td>Allowance 4</td><td>Allowance 5</td><td>Deduction 1</td><td>Deduction 2</td><td>Deduction 3</td><td>Deduction 4</td><td>Deduction 5</td><td>LOP</td><td>Total Deduction</td><td>Net Salary</td><td>OT</td><td>Net Amount</td><td>Signature     </td>");
    //                    Response.Write("</tr><tr>");
    //                }
    //                else
    //                {
    //                    Response.Write("<td colspan='31'></td></tr></table>");
    //                    Response.Write("<table border='1'><tr style='font-weight:bold;'><td colspan='5' align='left' style='font-weight:bold;'>STAFFS/LABOUR      : LABOUR</td><td colspan='6' align='center' style='font-weight:bold;'>DEPARTMENT            : " + dt_1.Rows[i]["DepartmentNm"].ToString() + "</td><td colspan='20' align='left' style='font-weight:bold;'>SALARY THROUGH       : BANK</td>");
    //                    Response.Write("</tr><tr>");
    //                }
    //                SI_Count = 1;
    //                Response.Write("<td>" + SI_Count.ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["EmpNo"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["ExisistingCode"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["OldID"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["EmpName"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["WorkedDays"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["CL"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["Weekoff"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["OTHours"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["Base"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["PFS"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["BasicandDA"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["ProvidentFund"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["ESI"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["Advance"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["allowances1"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["allowances2"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["allowances3"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["allowances4"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["allowances5"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["Deduction1"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["Deduction2"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["Deduction3"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["Deduction4"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["Deduction5"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["Losspay"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["TotalDeductions"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["NetSalary"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["OverTime"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["NetPay"].ToString() + "</td>");
    //            }
    //            else
    //            {
    //                if (Department_Name_Check == dt_1.Rows[i]["DepartmentNm"].ToString())
    //                {
    //                    Response.Write("<td>" + SI_Count.ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["EmpNo"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["ExisistingCode"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["OldID"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["EmpName"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["WorkedDays"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["CL"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Weekoff"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["OTHours"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Base"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["PFS"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["BasicandDA"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["ProvidentFund"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["ESI"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Advance"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["allowances1"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["allowances2"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["allowances3"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["allowances4"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["allowances5"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Deduction1"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Deduction2"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Deduction3"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Deduction4"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Deduction5"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Losspay"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["TotalDeductions"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["NetSalary"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["OverTime"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["NetPay"].ToString() + "</td>");
    //                    Response.Write("<td></td>");
    //                }
    //                else
    //                {
    //                    if (SI_Count == 2)
    //                    {
    //                        Grand_To_Count = Grand_From_Count;
    //                    }
    //                    else
    //                    {
    //                        Grand_To_Count = SI_Count - 1;
    //                        Grand_To_Count = Grand_From_Count + Grand_To_Count;
    //                        Grand_To_Count = Grand_To_Count - 1;
    //                    }
    //                    Response.Write("<td colspan='5' align='right' style='font-size:12.0pt;font-weight:bold;'>Grand Total</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(F" + Grand_From_Count.ToString() + ":F" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(G" + Grand_From_Count.ToString() + ":G" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(H" + Grand_From_Count.ToString() + ":H" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(I" + Grand_From_Count.ToString() + ":I" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(J" + Grand_From_Count.ToString() + ":J" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(K" + Grand_From_Count.ToString() + ":K" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(L" + Grand_From_Count.ToString() + ":L" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(M" + Grand_From_Count.ToString() + ":M" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(N" + Grand_From_Count.ToString() + ":N" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(O" + Grand_From_Count.ToString() + ":O" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(P" + Grand_From_Count.ToString() + ":P" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(Q" + Grand_From_Count.ToString() + ":Q" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(R" + Grand_From_Count.ToString() + ":R" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(S" + Grand_From_Count.ToString() + ":S" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(T" + Grand_From_Count.ToString() + ":T" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(U" + Grand_From_Count.ToString() + ":U" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(V" + Grand_From_Count.ToString() + ":V" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(W" + Grand_From_Count.ToString() + ":W" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(X" + Grand_From_Count.ToString() + ":X" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(Y" + Grand_From_Count.ToString() + ":Y" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(Z" + Grand_From_Count.ToString() + ":Z" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(AA" + Grand_From_Count.ToString() + ":AA" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(AB" + Grand_From_Count.ToString() + ":AB" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(AC" + Grand_From_Count.ToString() + ":AC" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(AD" + Grand_From_Count.ToString() + ":AD" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td></td></tr></table><table border='1'><tr>");
    //                    Grand_From_Count = Grand_To_Count + 4;

    //                    SI_Count = 1;
    //                    Department_Name_Check = dt_1.Rows[i]["DepartmentNm"].ToString();
    //                    Response.Write("<td colspan='31'></td></tr></table><table border='1'><tr style='font-weight:bold;'>");
    //                    Response.Write("<td colspan='5' align='left' style='font-weight:bold;'>STAFFS/LABOUR      : LABOUR</td><td colspan='6' align='center' style='font-weight:bold;'>DEPARTMENT            : " + dt_1.Rows[i]["DepartmentNm"].ToString() + "</td><td colspan='20' align='left' style='font-weight:bold;'>SALARY THROUGH       : BANK</td></tr><tr>");
    //                    Response.Write("<td>" + SI_Count.ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["EmpNo"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["ExisistingCode"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["OldID"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["EmpName"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["WorkedDays"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["CL"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Weekoff"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["OTHours"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Base"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["PFS"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["BasicandDA"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["ProvidentFund"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["ESI"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Advance"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["allowances1"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["allowances2"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["allowances3"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["allowances4"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["allowances5"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Deduction1"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Deduction2"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Deduction3"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Deduction4"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Deduction5"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Losspay"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["TotalDeductions"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["NetSalary"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["OverTime"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["NetPay"].ToString() + "</td>");
    //                    Response.Write("<td></td>");
    //                }
    //            }
    //            Response.Write("</tr>");
    //            Response.Write("</table>");
    //            SI_Count++;
    //        }
    //        //GRAND Total
    //        if (dt_1.Rows.Count.ToString() != "0")
    //        {
    //            if (SI_Count == 2)
    //            {
    //                Grand_To_Count = Grand_From_Count;
    //            }
    //            else
    //            {
    //                Grand_To_Count = SI_Count - 1;
    //                Grand_To_Count = Grand_From_Count + Grand_To_Count;
    //                Grand_To_Count = Grand_To_Count - 1;
    //            }
    //            Response.Write("<table border='1'><tr style='font-size:12.0pt;font-weight:bold;'>");
    //            Response.Write("<td colspan='5' align='right'>Grand Total</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(F" + Grand_From_Count.ToString() + ":F" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(G" + Grand_From_Count.ToString() + ":G" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(H" + Grand_From_Count.ToString() + ":H" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(I" + Grand_From_Count.ToString() + ":I" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(J" + Grand_From_Count.ToString() + ":J" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(K" + Grand_From_Count.ToString() + ":K" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(L" + Grand_From_Count.ToString() + ":L" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(M" + Grand_From_Count.ToString() + ":M" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(N" + Grand_From_Count.ToString() + ":N" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(O" + Grand_From_Count.ToString() + ":O" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(P" + Grand_From_Count.ToString() + ":P" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(Q" + Grand_From_Count.ToString() + ":Q" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(R" + Grand_From_Count.ToString() + ":R" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(S" + Grand_From_Count.ToString() + ":S" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(T" + Grand_From_Count.ToString() + ":T" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(U" + Grand_From_Count.ToString() + ":U" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(V" + Grand_From_Count.ToString() + ":V" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(W" + Grand_From_Count.ToString() + ":W" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(X" + Grand_From_Count.ToString() + ":X" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(Y" + Grand_From_Count.ToString() + ":Y" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(Z" + Grand_From_Count.ToString() + ":Z" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(AA" + Grand_From_Count.ToString() + ":AA" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(AB" + Grand_From_Count.ToString() + ":AB" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(AC" + Grand_From_Count.ToString() + ":AC" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(AD" + Grand_From_Count.ToString() + ":AD" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td></td></tr></table>");
    //            Grand_From_Count = Grand_To_Count + 4;
    //        }


    //        //CASH LABOUR Payslip Design
    //        cmd = new SqlCommand(Labour_Cash_Query, con);
    //        dt_1 = new DataTable();
    //        sda = new SqlDataAdapter(cmd);
    //        //DataSet ds1 = new DataSet();
    //        con.Open();
    //        sda.Fill(dt_1);
    //        con.Close();
    //        //Fill OT Hours            
    //        for (int i = 0; i < dt_1.Rows.Count; i++)
    //        {
    //            string EmpNo = dt_1.Rows[i]["EmpNo"].ToString();
    //            string OTHours = "";
    //            string qry_ot = "";
    //            if (rbsalary.SelectedValue == "2")
    //            {
    //                qry_ot = "Select NoHrs from OverTime where EmpNo='" + EmpNo + "' and Lcode='" + SessionLcode + "' and Ccode='" + SessionCcode + "' and Month='" + ddlMonths.SelectedValue + "' and Financialyr='" + ddlFinance.SelectedValue + "'";
    //            }
    //            else
    //            {
    //                qry_ot = "Select NoHrs from OverTime where EmpNo='" + EmpNo + "' and Lcode='" + SessionLcode + "' and Ccode='" + SessionCcode + "' and Month='" + ddlMonths.SelectedValue + "' and Financialyr='" + ddlFinance.SelectedValue + "'" +
    //                                " and convert(Datetime, FromDate, 105)=convert(Datetime,'" + MyDate1 + "', 105) and convert(Datetime, ToDate, 105)=convert(datetime,'" + MyDate2 + "', 105)";

    //            }
    //            SqlCommand cmd_ot = new SqlCommand(qry_ot, con);
    //            con.Open();
    //            string val_ot = Convert.ToString(cmd_ot.ExecuteScalar());
    //            con.Close();
    //            if (val_ot.Trim() == "")
    //            {
    //                OTHours = "0".ToString();
    //            }
    //            else
    //            {
    //                OTHours = val_ot.ToString();
    //            }
    //            dt_1.Rows[i]["OTHours"] = OTHours.ToString();
    //        }

    //        for (int i = 0; i < dt_1.Rows.Count; i++) //LABOUR CASH Payslip Add
    //        {
    //            Response.Write("<table border='1'>");
    //            Response.Write("<tr>");
    //            if (i == 0)
    //            {
    //                Department_Name_Check = dt_1.Rows[i]["DepartmentNm"].ToString();
    //                if (Check_Heading_Add_OR_Not == "")
    //                {
    //                    Check_Heading_Add_OR_Not = "1";
    //                    Grand_From_Count = 7;
    //                    Response.Write("<td colspan='5' align='left' style='font-weight:bold;'>STAFFS/LABOUR      : LABOUR</td><td colspan='6' align='center' style='font-weight:bold;'>DEPARTMENT            : " + dt_1.Rows[i]["DepartmentNm"].ToString() + "</td><td colspan='20' align='left' style='font-weight:bold;'>SALARY THROUGH       : CASH</td>");
    //                    Response.Write("<tr align='center' valign='center' style='font-weight:bold;'>");
    //                    Response.Write("<td>Si. No</td><td>Emp No</td><td>Ticket No</td><td>OldID</td><td>Name</td><td>Working Days</td><td>CL</td><td>Week Off</td><td>OT Hours</td><td>Fixed Salary</td><td>Fixed OT Salary</td><td>Base</td><td>P.F</td><td>ESI</td><td>Advance</td><td>Allowance 1</td><td>Allowance 2</td><td>Allowance 3</td><td>Allowance 4</td><td>Allowance 5</td><td>Deduction 1</td><td>Deduction 2</td><td>Deduction 3</td><td>Deduction 4</td><td>Deduction 5</td><td>LOP</td><td>Total Deduction</td><td>Net Salary</td><td>OT</td><td>Net Amount</td><td>Signature     </td>");
    //                    Response.Write("</tr><tr>");
    //                }
    //                else
    //                {
    //                    Response.Write("<td colspan='31'></td></tr></table>");
    //                    Response.Write("<table border='1'><tr style='font-weight:bold;'><td colspan='5' align='left' style='font-weight:bold;'>STAFFS/LABOUR      : LABOUR</td><td colspan='6' align='center' style='font-weight:bold;'>DEPARTMENT            : " + dt_1.Rows[i]["DepartmentNm"].ToString() + "</td><td colspan='20' align='left' style='font-weight:bold;'>SALARY THROUGH       : CASH</td>");
    //                    Response.Write("</tr><tr>");
    //                }
    //                SI_Count = 1;
    //                Response.Write("<td>" + SI_Count.ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["EmpNo"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["ExisistingCode"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["OldID"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["EmpName"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["WorkedDays"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["CL"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["Weekoff"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["OTHours"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["Base"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["PFS"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["BasicandDA"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["ProvidentFund"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["ESI"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["Advance"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["allowances1"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["allowances2"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["allowances3"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["allowances4"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["allowances5"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["Deduction1"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["Deduction2"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["Deduction3"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["Deduction4"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["Deduction5"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["Losspay"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["TotalDeductions"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["NetSalary"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["OverTime"].ToString() + "</td>");
    //                Response.Write("<td>" + dt_1.Rows[i]["NetPay"].ToString() + "</td>");
    //            }
    //            else
    //            {
    //                if (Department_Name_Check == dt_1.Rows[i]["DepartmentNm"].ToString())
    //                {
    //                    Response.Write("<td>" + SI_Count.ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["EmpNo"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["ExisistingCode"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["OldID"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["EmpName"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["WorkedDays"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["CL"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Weekoff"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["OTHours"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Base"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["PFS"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["BasicandDA"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["ProvidentFund"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["ESI"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Advance"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["allowances1"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["allowances2"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["allowances3"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["allowances4"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["allowances5"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Deduction1"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Deduction2"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Deduction3"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Deduction4"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Deduction5"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Losspay"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["TotalDeductions"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["NetSalary"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["OverTime"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["NetPay"].ToString() + "</td>");
    //                    Response.Write("<td></td>");
    //                }
    //                else
    //                {
    //                    if (SI_Count == 2)
    //                    {
    //                        Grand_To_Count = Grand_From_Count;
    //                    }
    //                    else
    //                    {
    //                        Grand_To_Count = SI_Count - 1;
    //                        Grand_To_Count = Grand_From_Count + Grand_To_Count;
    //                        Grand_To_Count = Grand_To_Count - 1;
    //                    }
    //                    Response.Write("<td colspan='5' align='right' style='font-size:12.0pt;font-weight:bold;'>Grand Total</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(F" + Grand_From_Count.ToString() + ":F" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(G" + Grand_From_Count.ToString() + ":G" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(H" + Grand_From_Count.ToString() + ":H" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(I" + Grand_From_Count.ToString() + ":I" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(J" + Grand_From_Count.ToString() + ":J" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(K" + Grand_From_Count.ToString() + ":K" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(L" + Grand_From_Count.ToString() + ":L" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(M" + Grand_From_Count.ToString() + ":M" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(N" + Grand_From_Count.ToString() + ":N" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(O" + Grand_From_Count.ToString() + ":O" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(P" + Grand_From_Count.ToString() + ":P" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(Q" + Grand_From_Count.ToString() + ":Q" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(R" + Grand_From_Count.ToString() + ":R" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(S" + Grand_From_Count.ToString() + ":S" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(T" + Grand_From_Count.ToString() + ":T" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(U" + Grand_From_Count.ToString() + ":U" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(V" + Grand_From_Count.ToString() + ":V" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(W" + Grand_From_Count.ToString() + ":W" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(X" + Grand_From_Count.ToString() + ":X" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(Y" + Grand_From_Count.ToString() + ":Y" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(Z" + Grand_From_Count.ToString() + ":Z" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(AA" + Grand_From_Count.ToString() + ":AA" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(AB" + Grand_From_Count.ToString() + ":AB" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(AC" + Grand_From_Count.ToString() + ":AC" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(AD" + Grand_From_Count.ToString() + ":AD" + Grand_To_Count.ToString() + ")</td>");
    //                    Response.Write("<td></td></tr></table><table border='1'><tr>");
    //                    Grand_From_Count = Grand_To_Count + 4;

    //                    SI_Count = 1;
    //                    Department_Name_Check = dt_1.Rows[i]["DepartmentNm"].ToString();
    //                    Response.Write("<td colspan='31'></td></tr></table><table border='1'><tr style='font-weight:bold;'>");
    //                    Response.Write("<td colspan='5' align='left' style='font-weight:bold;'>STAFFS/LABOUR      : LABOUR</td><td colspan='6' align='center' style='font-weight:bold;'>DEPARTMENT            : " + dt_1.Rows[i]["DepartmentNm"].ToString() + "</td><td colspan='20' align='left' style='font-weight:bold;'>SALARY THROUGH       : CASH</td></tr><tr>");
    //                    Response.Write("<td>" + SI_Count.ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["EmpNo"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["ExisistingCode"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["OldID"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["EmpName"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["WorkedDays"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["CL"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Weekoff"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["OTHours"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Base"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["PFS"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["BasicandDA"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["ProvidentFund"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["ESI"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Advance"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["allowances1"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["allowances2"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["allowances3"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["allowances4"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["allowances5"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Deduction1"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Deduction2"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Deduction3"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Deduction4"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Deduction5"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["Losspay"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["TotalDeductions"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["NetSalary"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["OverTime"].ToString() + "</td>");
    //                    Response.Write("<td>" + dt_1.Rows[i]["NetPay"].ToString() + "</td>");
    //                    Response.Write("<td></td>");
    //                }
    //            }
    //            Response.Write("</tr>");
    //            Response.Write("</table>");
    //            SI_Count++;
    //        }
    //        //GRAND Total
    //        if (dt_1.Rows.Count.ToString() != "0")
    //        {
    //            if (SI_Count == 2)
    //            {
    //                Grand_To_Count = Grand_From_Count;
    //            }
    //            else
    //            {
    //                Grand_To_Count = SI_Count - 1;
    //                Grand_To_Count = Grand_From_Count + Grand_To_Count;
    //                Grand_To_Count = Grand_To_Count - 1;
    //            }
    //            Response.Write("<table border='1'><tr style='font-size:12.0pt;font-weight:bold;'>");
    //            Response.Write("<td colspan='5' align='right'>Grand Total</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(F" + Grand_From_Count.ToString() + ":F" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(G" + Grand_From_Count.ToString() + ":G" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(H" + Grand_From_Count.ToString() + ":H" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(I" + Grand_From_Count.ToString() + ":I" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(J" + Grand_From_Count.ToString() + ":J" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(K" + Grand_From_Count.ToString() + ":K" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(L" + Grand_From_Count.ToString() + ":L" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(M" + Grand_From_Count.ToString() + ":M" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(N" + Grand_From_Count.ToString() + ":N" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(O" + Grand_From_Count.ToString() + ":O" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(P" + Grand_From_Count.ToString() + ":P" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(Q" + Grand_From_Count.ToString() + ":Q" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(R" + Grand_From_Count.ToString() + ":R" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(S" + Grand_From_Count.ToString() + ":S" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(T" + Grand_From_Count.ToString() + ":T" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(U" + Grand_From_Count.ToString() + ":U" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(V" + Grand_From_Count.ToString() + ":V" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(W" + Grand_From_Count.ToString() + ":W" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(X" + Grand_From_Count.ToString() + ":X" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(Y" + Grand_From_Count.ToString() + ":Y" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(Z" + Grand_From_Count.ToString() + ":Z" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(AA" + Grand_From_Count.ToString() + ":AA" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(AB" + Grand_From_Count.ToString() + ":AB" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(AC" + Grand_From_Count.ToString() + ":AC" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td style='font-size:12.0pt;font-weight:bold;'>=sum(AD" + Grand_From_Count.ToString() + ":AD" + Grand_To_Count.ToString() + ")</td>");
    //            Response.Write("<td></td></tr></table>");
    //            Grand_From_Count = Grand_To_Count + 4;
    //        }




    //        Response.End();
    //        //Response.Clear();
    //        ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Downloaded Successfully');", true);
    //    }
    //}

    protected void BtnDeptManDays_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            string CmpName = "";
            string Cmpaddress = "";
            int YR = 0;
            if (ddlMonths.SelectedItem.Text == "January")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else if (ddlMonths.SelectedItem.Text == "February")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else if (ddlMonths.SelectedItem.Text == "March")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
            }

            if (ddlcategory.SelectedValue == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Category');", true);
                ErrFlag = true;
            }
            else if ((txtEmployeeType.SelectedValue == "") || (txtEmployeeType.SelectedValue == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Employee Type');", true);
                ErrFlag = true;
            }

            else if ((ddlMonths.SelectedValue == "0") || (ddlMonths.SelectedValue == ""))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Months');", true);
                ErrFlag = true;
            }


            if (!ErrFlag)
            {
                //if (ddldept.SelectedValue == "0")
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Department Properly');", true);
                //    ErrFlag = true;
                //}
                if (!ErrFlag)
                {
                    if (ddlcategory.SelectedValue == "STAFF")
                    {
                        Stafflabour = "STAFF";
                    }
                    else if (ddlcategory.SelectedValue == "LABOUR")
                    {
                        Stafflabour = "LABOUR";
                    }
                    string constr = ConfigurationManager.AppSettings["ConnectionString"];
                    SqlConnection con = new SqlConnection(constr);
                    //foreach (GridViewRow gvsal in gvSalary.Rows)
                    //{
                    //    Label lblEmpNo = (Label)gvsal.FindControl("lblExist");
                    //    string qry = "Update SalaryDetails set Process_Mode='" + 1 + "' where Month='" + ddlMonths.SelectedValue + "' and FinancialYear='" + ddlFinance.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and ExisistingCode='" + lblEmpNo.Text + "'";
                    //    SqlCommand cmd = new SqlCommand(qry, con);
                    //    con.Open();
                    //    cmd.ExecuteNonQuery();
                    //    con.Close();
                    //}
                    string Emp_ESI_Code = "";//ddESI.SelectedValue.ToString();

                    string Payslip_Format_Type = rdbPayslipFormat.SelectedValue.ToString();

                    string Str_PFType = RdbPFNonPF.SelectedValue.ToString();
                    string Str_ChkLeft = ""; // ChkLeft.SelectedValue.ToString();
                    if (ChkLeft.Checked == true)
                    {
                        Str_ChkLeft = "1";
                    }
                    else { Str_ChkLeft = "0"; }
                    string SalaryType = "";
                    ResponseHelper.Redirect("ViewReport_Opt.aspx?Cate=" + Stafflabour + "&Depat=" + ddldept.SelectedValue + "&Months=" + ddlMonths.SelectedItem.Text + "&yr=" + ddlFinance.SelectedValue + "&fromdate=" + txtfrom.Text + "&ToDate=" + txtTo.Text + "&Salary=" + SalaryType + "&CashBank=" + RdbCashBank.SelectedValue + "&ESICode=" + Emp_ESI_Code + "&EmpTypeCd=" + txtEmployeeType.SelectedValue.ToString() + "&EmpType=" + txtEmployeeType.SelectedItem.Text.ToString() + "&PayslipType=" + rdbPayslipFormat.SelectedValue.ToString() + "&PFTypePost=" + RdbPFNonPF.SelectedValue.ToString() + "&Left_Emp=" + Str_ChkLeft.ToString() + "&Leftdate=" + txtLeftDate.Text + "&Division=" + txtDivision.Text + "&Report_Type=DeptManDays", "_blank", "");
                }
            }
        }
        catch (Exception ex)
        {
        }
    }

    protected void btnLCReport_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            string CmpName = "";
            string Cmpaddress = "";
            int YR = 0;

            if (txtLCYear.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Leave Credit Year');", true);
                ErrFlag = true;
            }


            if (!ErrFlag)
            {
                Stafflabour = "STAFF";

                string Emp_ESI_Code = "";//ddESI.SelectedValue.ToString();

                string Payslip_Format_Type = rdbPayslipFormat.SelectedValue.ToString();

                string Str_PFType = RdbPFNonPF.SelectedValue.ToString();
                string Str_ChkLeft = ""; //ChkLeft.SelectedValue.ToString();
                if (ChkLeft.Checked == true)
                {
                    Str_ChkLeft = "1";
                }
                else { Str_ChkLeft = "0"; }
                string SalaryType = "";
                ResponseHelper.Redirect("ViewReport_Opt.aspx?Cate=" + Stafflabour + "&Depat=" + ddldept.SelectedValue + "&Months=" + ddlMonths.SelectedItem.Text + "&yr=" + txtLCYear.SelectedValue + "&fromdate=" + txtfrom.Text + "&ToDate=" + txtTo.Text + "&Salary=" + SalaryType + "&CashBank=" + RdbCashBank.SelectedValue + "&ESICode=" + Emp_ESI_Code + "&EmpTypeCd=" + txtEmployeeType.SelectedValue.ToString() + "&EmpType=" + txtEmployeeType.SelectedItem.Text.ToString() + "&PayslipType=" + rdbPayslipFormat.SelectedValue.ToString() + "&PFTypePost=" + RdbPFNonPF.SelectedValue.ToString() + "&Left_Emp=" + Str_ChkLeft.ToString() + "&Leftdate=" + txtLeftDate.Text + "&Division=" + txtDivision.Text + "&Report_Type=LC_ALL_REPORT", "_blank", "");
            }
        }
        catch (Exception ex)
        {
        }

    }

    protected void btnLCYearTotal_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            string CmpName = "";
            string Cmpaddress = "";
            int YR = 0;

            if (txtLCYear.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Leave Credit Year');", true);
                ErrFlag = true;
            }


            if (!ErrFlag)
            {
                Stafflabour = "STAFF";

                string Emp_ESI_Code = "";//ddESI.SelectedValue.ToString();

                string Payslip_Format_Type = rdbPayslipFormat.SelectedValue.ToString();

                string Str_PFType = RdbPFNonPF.SelectedValue.ToString();
                string Str_ChkLeft = "";// ChkLeft.SelectedValue.ToString();
                if (ChkLeft.Checked == true)
                {
                    Str_ChkLeft = "1";
                }
                else { Str_ChkLeft = "0"; }
                string SalaryType = "";
                ResponseHelper.Redirect("ViewReport_Opt.aspx?Cate=" + Stafflabour + "&Depat=" + ddldept.SelectedValue + "&Months=" + ddlMonths.SelectedItem.Text + "&yr=" + txtLCYear.SelectedValue + "&fromdate=" + txtfrom.Text + "&ToDate=" + txtTo.Text + "&Salary=" + SalaryType + "&CashBank=" + RdbCashBank.SelectedValue + "&ESICode=" + Emp_ESI_Code + "&EmpTypeCd=" + txtEmployeeType.SelectedValue.ToString() + "&EmpType=" + txtEmployeeType.SelectedItem.Text.ToString() + "&PayslipType=" + rdbPayslipFormat.SelectedValue.ToString() + "&PFTypePost=" + RdbPFNonPF.SelectedValue.ToString() + "&Left_Emp=" + Str_ChkLeft.ToString() + "&Leftdate=" + txtLeftDate.Text + "&Division=" + txtDivision.Text + "&Report_Type=LC_YearTotal_REPORT", "_blank", "");
            }
        }
        catch (Exception ex)
        {
        }

    }
    protected void btnLCDetReport_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            string CmpName = "";
            string Cmpaddress = "";
            int YR = 0;

            if (txtLCYear.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Leave Credit Year');", true);
                ErrFlag = true;
            }


            if (!ErrFlag)
            {
                Stafflabour = "STAFF";

                string Emp_ESI_Code = "";//ddESI.SelectedValue.ToString();

                string Payslip_Format_Type = rdbPayslipFormat.SelectedValue.ToString();

                string Str_PFType = RdbPFNonPF.SelectedValue.ToString();
                string Str_ChkLeft = "";// ChkLeft.SelectedValue.ToString();
                if (ChkLeft.Checked == true)
                {
                    Str_ChkLeft = "1";
                }
                else { Str_ChkLeft = "0"; }
                string SalaryType = "";
                ResponseHelper.Redirect("ViewReport_Opt.aspx?Cate=" + Stafflabour + "&Depat=" + ddldept.SelectedValue + "&Months=" + ddlMonths.SelectedItem.Text + "&yr=" + txtLCYear.SelectedValue + "&fromdate=" + txtfrom.Text + "&ToDate=" + txtTo.Text + "&Salary=" + SalaryType + "&CashBank=" + RdbCashBank.SelectedValue + "&ESICode=" + Emp_ESI_Code + "&EmpTypeCd=" + txtEmployeeType.SelectedValue.ToString() + "&EmpType=" + txtEmployeeType.SelectedItem.Text.ToString() + "&PayslipType=" + rdbPayslipFormat.SelectedValue.ToString() + "&PFTypePost=" + RdbPFNonPF.SelectedValue.ToString() + "&Left_Emp=" + Str_ChkLeft.ToString() + "&Leftdate=" + txtLeftDate.Text + "&Division=" + txtDivision.Text + "&Report_Type=LC_Details_REPORT", "_blank", "");
            }
        }
        catch (Exception ex)
        {
        }

    }
    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_WagesType();
    }

    //IFUser Work
    public void IFUser_Fields_Hide()
    {
        IF_Dept_Hide.Visible = false;
        IF_FromDate_Hide.Visible = false;
        IF_ToDate_Hide.Visible = false;
        IF_Left_Employee_Hide.Visible = false;
        IF_State_Hide.Visible = false;
        btnBankSalary.Visible = false;
        btnCivilAbstract.Visible = false;
        BtnDeptManDays.Visible = false;
        IF_Leave_Credit.Visible = false;
        rdbPayslipFormat.Visible = false;
        rdbPayslipIFFormat.Visible = true;
        RdbCashBank.SelectedValue = "2";
        RdbPFNonPF.SelectedValue = "1";
        IF_Salary_Through_Hide.Visible = false;
        IF_PF_NON_PF_Hide.Visible = false;
        IF_Report_Type.Visible = true;
        IF_rdbPayslipFormat_Hide.Visible = false;
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
          server control at run time. */
    }

    protected void btnOtherBankSalary_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        //if (SessionUserType == "2")
        //{
        //    //rbsalary.SelectedValue = rbIFsalary.SelectedValue;
        //    //if (rbIFsalary.SelectedValue == "2")
        //    //{
        //        if (ddlMonths.SelectedValue == "0")
        //        {
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Month Properly');", true);
        //            ErrFlag = true;
        //        }
        //    //}
        //}
        //else
        //{
        //    //if (rbsalary.SelectedValue == "2")
        //    //{
        //    //    if (ddlMonths.SelectedValue == "")
        //    //    {
        //    //        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Month Properly');", true);
        //    //        ErrFlag = true;
        //    //    }
        //    //}
        //    //else
        //    //{
        //if (txtfrom.Text.Trim() == "")
        //{
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Month Properly');", true);
        //    ErrFlag = true;
        //}
        //else if (txtTo.Text.Trim() == "")
        //{
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Month Properly');", true);
        //    ErrFlag = true;
        //}
        //if (!ErrFlag)
        //{
        //    DateTime dfrom = Convert.ToDateTime(txtfrom.Text);
        //    DateTime dtto = Convert.ToDateTime(txtTo.Text);
        //    MyDate1 = DateTime.ParseExact(txtfrom.Text, "dd/MM/yyyy", null);
        //    MyDate2 = DateTime.ParseExact(txtTo.Text, "dd/MM/yyyy", null);
        //    if (dtto < dfrom)
        //    {
        //        ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Enter the From Date...');", true);
        //        txtfrom.Text = null;
        //        txtTo.Text = null;
        //        ErrFlag = true;
        //    }
        //}
        //    //}
        //}
        if (!ErrFlag)
        {
            string CmpName = "";
            string Cmpaddress = "";
            int YR = 0;
            string query = "";
            if (ddlMonths.SelectedItem.Text == "January")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else if (ddlMonths.SelectedItem.Text == "February")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else if (ddlMonths.SelectedItem.Text == "March")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
            }
            Stafflabour = "";
            if (ddlcategory.SelectedValue == "STAFF")
            {
                Stafflabour = "STAFF";
            }
            else if (ddlcategory.SelectedValue == "LABOUR")
            {
                Stafflabour = "LABOUR";
            }

            Emp_ESI_Code = "";//ddESI.SelectedValue.ToString();

            if (Emp_ESI_Code == "--Select--") { Emp_ESI_Code = ""; }
            string PFTypeGet = RdbPFNonPF.SelectedValue.ToString();
            string Left_Employee = ""; //ChkLeft.SelectedValue.ToString();

            if (ChkLeft.Checked == true)
            {
                Left_Employee = "1";
            }
            else
            {
                Left_Employee = "0";
            }






            string Left_Date = "";
            Left_Date = txtLeftDate.Text.ToString();
            //if (rbsalary.SelectedValue == "2" || SessionUserType == "2")
            //{
            //    if (SessionUserType == "2") { rbsalary.SelectedValue = "2"; }
            //    query = "Select SalDet.EmpNo,EmpDet.EmpName,OP.BankACCNo as AccountNo,EmpDet.Department,EmpDet.ExisistingCode,";
            //    if (Stafflabour == "S")
            //    {
            //        query = query + " Round((Sum(cast(SalDet.BasicAndDANew as decimal(18,2)) + cast(SalDet.BasicHRA as decimal(18,2)) + ";
            //        query = query + " cast(SalDet.ConvAllow as decimal(18,2)) + cast(SalDet.EduAllow as decimal(18,2)) +  ";
            //        query = query + " cast(SalDet.MediAllow as decimal(18,2)) + cast(SalDet.BasicRAI as decimal(18,2)) + ";

            //        query = query + " isnull(cast(SalDet.Basic_Spl_Allowance as decimal(18,2)),0) + isnull(cast(SalDet.Basic_Uniform as decimal(18,2)),0) + ";
            //        query = query + " isnull(cast(SalDet.Basic_Vehicle_Maintenance as decimal(18,2)),0) + isnull(cast(SalDet.Basic_Communication as decimal(18,2)),0) + ";
            //        query = query + " isnull(cast(SalDet.Basic_Journ_Perid_Paper as decimal(18,2)),0) + isnull(cast(SalDet.Basic_Incentives as decimal(18,2)),0) + ";

            //        query = query + " cast(SalDet.WashingAllow as decimal(18,2))) - (Sum(SalDet.TotalDeductions))),-1) as NetSalary,";
            //    }
            //    else
            //    {
            //        query = query + " Round((Sum(cast(SalDet.BasicAndDANew as decimal(18,2)) + cast(SalDet.BasicHRA as decimal(18,2)) + ";
            //        query = query + " cast(SalDet.ConvAllow as decimal(18,2)) + cast(SalDet.EduAllow as decimal(18,2)) +  ";
            //        query = query + " cast(SalDet.MediAllow as decimal(18,2)) + cast(SalDet.BasicRAI as decimal(18,2)) + ";

            //        query = query + " isnull(cast(SalDet.Basic_Spl_Allowance as decimal(18,2)),0) + isnull(cast(SalDet.Basic_Uniform as decimal(18,2)),0) + ";
            //        query = query + " isnull(cast(SalDet.Basic_Vehicle_Maintenance as decimal(18,2)),0) + isnull(cast(SalDet.Basic_Communication as decimal(18,2)),0) + ";
            //        query = query + " isnull(cast(SalDet.Basic_Journ_Perid_Paper as decimal(18,2)),0) + isnull(cast(SalDet.Basic_Incentives as decimal(18,2)),0) + ";

            //        query = query + " cast(SalDet.WashingAllow as decimal(18,2)) + (AD.NFH_Work_Days_Statutory * (cast(SalDet.Basic_SM as decimal(18,2))))) - (Sum(SalDet.TotalDeductions))),-1) as NetSalary,";
            //    }

            //    //" SalDet.BasicandDA,(SalDet.BasicandDA - SalDet.ESI) as NetSalary, " +
            //    query = query + " SalDet.BasicandDA,((SalDet.NetPay)-(SalDet.ThreesidedAmt + SalDet.OTHoursAmtNew)) as NetSalary_Dummy,SalDet.RoundOffNetPay as NetSalaryOLD, " +
            //            " SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.SalaryDate," +
            //            " SalDet.NetPay,MstDpt.DepartmentNm,SalDet.ESI from EmployeeDetails EmpDet" +
            //            " inner Join SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo " +
            //            " inner Join AttenanceDetails AD on EmpDet.EmpNo=AD.EmpNo " +
            //            " inner Join MstDepartment as MstDpt on MstDpt.DepartmentCd = EmpDet.Department" +
            //            " inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo where " +
            //            " SalDet.Month='" + ddlMonths.SelectedValue + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "'" +
            //            " And AD.Months='" + ddlMonths.SelectedValue + "' AND AD.FinancialYear='" + ddlFinance.SelectedValue + "'";
            //    if (Stafflabour != "") { query = query + " and EmpDet.StafforLabor='" + Stafflabour + "'"; }
            //    if (ddldept.SelectedValue != "0" && ddldept.SelectedValue != "") { query = query + " and EmpDet.Department='" + ddldept.SelectedValue + "'"; }
            //    if (txtEmployeeType.SelectedValue != "0" && txtEmployeeType.SelectedValue != "") { query = query + " and EmpDet.EmployeeType='" + txtEmployeeType.SelectedValue + "'"; }

            //    //Activate Employee Check
            //    if (Left_Employee == "1") { query = query + " and EmpDet.ActivateMode='N' and Month(EmpDet.deactivedate)=Month(convert(datetime,'" + Left_Date + "', 105)) And Year(EmpDet.deactivedate)=Year(convert(datetime,'" + Left_Date + "', 105))"; }
            //    //if (Left_Employee == "2") { query = query + " and EmpDet.ActivateMode='N'"; }
            //    else
            //    {
            //        //if (Left_Date != "") { query = query + " and ED.deactivedate > convert(datetime,'" + Left_Date + "', 105)"; }
            //        if (Left_Date != "") { query = query + " and (EmpDet.deactivedate > convert(datetime,'" + Left_Date + "', 105) Or EmpDet.ActivateMode='Y')"; }
            //    }
            //    if (SessionUserType == "2")
            //    {
            //        query = query + " and (OP.EligiblePF='1')";
            //    }
            //    else
            //    {
            //        //PF Check
            //        if (PFTypeGet.ToString() != "0")
            //        {
            //            if (PFTypeGet.ToString() == "1")
            //            {
            //                //PF Employee
            //                query = query + " and (OP.EligiblePF='1')";
            //            }
            //            else
            //            {
            //                //Non PF Employee
            //                query = query + " and (OP.EligiblePF='2')";
            //            }
            //        }
            //    }


            //    if (txtDivision.Text.ToString() != "" && txtDivision.Text.ToString() != "-----Select----")
            //    {
            //        query = query + " and EmpDet.Division='" + txtDivision.Text.ToString() + "'";
            //    }

            //    query = query + " and EmpDet.Ccode='" + SessionCcode + "' and EmpDet.Lcode='" + SessionLcode + "'" +
            //            " and SalDet.WagesType='" + rbsalary.SelectedValue + "' and SalDet.Salarythrough='2'" +
            //            " group by SalDet.EmpNo,EmpDet.EmpName,OP.BankACCNo,EmpDet.Department,EmpDet.ExisistingCode," +
            //            " SalDet.BasicandDA,SalDet.ThreesidedAmt,SalDet.OTHoursAmtNew,SalDet.RoundOffNetPay,SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.SalaryDate," +
            //            " SalDet.NetPay,MstDpt.DepartmentNm,SalDet.ESI Order by EmpDet.ExisistingCode Asc";
            //}
            //else
            //{

            //Get Adolescent Date
            int month = DateTime.ParseExact(ddlMonths.SelectedItem.Text.ToString(), "MMMM", CultureInfo.CurrentCulture).Month;
            string Year_Get_Adol = "";
            string Adolescent_Date_Str = "";
            string[] Fin_Year_Spilit = ddlFinance.SelectedValue.Split('_');
            if (ddlMonths.SelectedItem.Text.ToUpper().ToString() == "January".ToUpper().ToString() || ddlMonths.SelectedItem.Text.ToUpper().ToString() == "February".ToUpper().ToString() || ddlMonths.SelectedItem.Text.ToUpper().ToString() == "March".ToUpper().ToString())
            {
                Year_Get_Adol = (Convert.ToDecimal(ddlFinance.SelectedValue.ToString()) + Convert.ToDecimal(1)).ToString();
            }
            else
            {
                Year_Get_Adol = ddlFinance.SelectedValue.ToString();
            }
            string Last_Day = DateTime.DaysInMonth(Convert.ToInt32(Year_Get_Adol), month).ToString();
            string Month_Str = "1";
            if (month.ToString().Length == 1) { Month_Str = "0" + month.ToString(); }
            else { Month_Str = month.ToString(); }
            Adolescent_Date_Str = Last_Day + "/" + Month_Str + "/" + Year_Get_Adol;

            query = "Select EmpDet.BirthDate,SalDet.EmpNo,EmpDet.FirstName as EmpName,EmpDet.AccountNo as AccountNo,EmpDet.DeptName as Department,EmpDet.ExistingCode as ExisistingCode, " +
                //" SalDet.BasicandDA,(SalDet.BasicandDA - SalDet.ESI) as NetSalary, " +
                        " Round((Sum(cast(SalDet.BasicAndDANew as decimal(18,2)) + cast(SalDet.BasicHRA as decimal(18,2)) + " +
                        " cast(SalDet.ConvAllow as decimal(18,2)) + cast(SalDet.EduAllow as decimal(18,2)) +  " +
                        " cast(SalDet.MediAllow as decimal(18,2)) + cast(SalDet.BasicRAI as decimal(18,2)) + " +

                        " isnull(cast(SalDet.Basic_Spl_Allowance as decimal(18,2)),0) + isnull(cast(SalDet.Basic_Uniform as decimal(18,2)),0) + " +
                        " isnull(cast(SalDet.Basic_Vehicle_Maintenance as decimal(18,2)),0) + isnull(cast(SalDet.Basic_Communication as decimal(18,2)),0) + " +
                        " isnull(cast(SalDet.Basic_Journ_Perid_Paper as decimal(18,2)),0) + isnull(cast(SalDet.Basic_Incentives as decimal(18,2)),0) + " +

                        " cast(SalDet.WashingAllow as decimal(18,2)) + (AD.NFH_Work_Days_Statutory * (cast(SalDet.Basic_SM as decimal(18,2))))) - (Sum(SalDet.Total_Deduction_IF))),-1) as NetSalary," +


                        " ((SalDet.RoundOffNetPay_IF) - Round((Sum(cast(SalDet.BasicAndDANew as decimal(18,2)) + cast(SalDet.BasicHRA as decimal(18,2)) + " +
                        " cast(SalDet.ConvAllow as decimal(18,2)) + cast(SalDet.EduAllow as decimal(18,2)) +  " +
                        " cast(SalDet.MediAllow as decimal(18,2)) + cast(SalDet.BasicRAI as decimal(18,2)) + " +

                        " isnull(cast(SalDet.Basic_Spl_Allowance as decimal(18,2)),0) + isnull(cast(SalDet.Basic_Uniform as decimal(18,2)),0) + " +
                        " isnull(cast(SalDet.Basic_Vehicle_Maintenance as decimal(18,2)),0) + isnull(cast(SalDet.Basic_Communication as decimal(18,2)),0) + " +
                        " isnull(cast(SalDet.Basic_Journ_Perid_Paper as decimal(18,2)),0) + isnull(cast(SalDet.Basic_Incentives as decimal(18,2)),0) + ";
            



            query = query + " cast(SalDet.WashingAllow as decimal(18,2)) + (AD.NFH_Work_Days_Statutory * (cast(SalDet.Basic_SM as decimal(18,2))))) - (Sum(SalDet.Total_Deduction_IF))),-1)) as Bank_Others1,";
            if (txtEmployeeType.SelectedValue == "3")
            {
                query = query + " (Select case when ";
                query = query + " (FLOOR((CAST (CONVERT(Datetime,'" + Adolescent_Date_Str + "',103) AS INTEGER) - CAST(CONVERT(datetime,EmpDet.BirthDate,103)  AS INTEGER)) / 365.25)) >=18 then";
                query = query + " Round((sum(Saldet.allowances1) + sum(Saldet.DayIncentive)),-1)";
                query = query + " else Round((sum(Saldet.allowances1) + sum(Saldet.DayIncentive) + sum(Saldet.ThreesidedAmt)),-1)";
                query = query + " end ) as Bank_Others,";
                //query = query + " (sum(Saldet.allowances1) + sum(Saldet.DayIncentive)) as Bank_Others,";
            }
            else
            {
                query = query + " (sum(Saldet.allowances1) + sum(Saldet.DayIncentive) + sum(Saldet.ThreesidedAmt)) as Bank_Others,";
            }
            query = query + " SalDet.BasicandDA,((SalDet.NetPay)-(SalDet.ThreesidedAmt + SalDet.OTHoursAmtNew)) as NetSalary_Dummy,SalDet.RoundOffNetPay_IF as NetSalaryOLD, " +
                        " SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.SalaryDate," +
                        " SalDet.NetPay,MstDpt.DeptName,SalDet.Employee_ESI_IF as ESI from Employee_Mst EmpDet" +
                        " inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.ExistingCode COLLATE DATABASE_DEFAULT = SalDet.ExisistingCode COLLATE DATABASE_DEFAULT And EmpDet.LocCode COLLATE DATABASE_DEFAULT = SalDet.Lcode COLLATE DATABASE_DEFAULT " +
                        " inner Join [" + SessionPayroll + "]..AttenanceDetails AD on EmpDet.ExistingCode COLLATE DATABASE_DEFAULT = AD.ExistingCode COLLATE DATABASE_DEFAULT And EmpDet.LocCode COLLATE DATABASE_DEFAULT = AD.Lcode COLLATE DATABASE_DEFAULT " +
                        " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                //" inner Join OfficialProfile OP on OP.EmpNo=SalDet.EmpNo where " +
                        " where SalDet.Month='" + ddlMonths.SelectedItem.Text + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "'" +
                        " And EmpDet.Salary_Through='2'" +
                        " And AD.Months='" + ddlMonths.SelectedItem.Text + "' AND AD.FinancialYear='" + ddlFinance.SelectedValue + "'";
            if (Stafflabour != "") { query = query + " and EmpDet.CatName='" + Stafflabour + "'"; }
            if (ddldept.SelectedValue != "0" && ddldept.SelectedValue != "") { query = query + " and EmpDet.DeptCode='" + ddldept.SelectedValue + "'"; }
            if (txtEmployeeType.SelectedValue != "0" && txtEmployeeType.SelectedValue != "") { query = query + " and EmpDet.Wages='" + txtEmployeeType.SelectedItem.Text + "'"; }

            //Activate Employee Check
            if (Left_Employee == "1") { query = query + " and EmpDet.IsActive='No' and Month(convert(datetime,EmpDet.DOR,105))=Month(convert(datetime,'" + Left_Date + "', 105)) And Year(convert(datetime,EmpDet.DOR,105))=Year(convert(datetime,'" + Left_Date + "', 105))"; }
            //if (Left_Employee == "2") { query = query + " and EmpDet.ActivateMode='N'"; }
            else
            {
                //if (Left_Date != "") { query = query + " and ED.deactivedate > convert(datetime,'" + Left_Date + "', 105)"; }
                if (Left_Date != "") { query = query + " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + Left_Date + "', 105) Or EmpDet.IsActive='Yes')"; }
            }


            if (SessionUserType == "2")
            {
                //PF Employee
                query = query + " and (EmpDet.Eligible_PF='1')";
            }
            else
            {
                //PF Check
                if (PFTypeGet.ToString() != "0")
                {
                    if (PFTypeGet.ToString() == "1")
                    {
                        //PF Employee
                        query = query + " and (EmpDet.Eligible_PF='1')";
                    }
                    else
                    {
                        //Non PF Employee
                        query = query + " and (EmpDet.Eligible_PF='2')";
                    }
                }
            }

            if (txtDivision.Text.ToString() != "" && txtDivision.Text.ToString() != "-Select-")
            {
                query = query + " and EmpDet.Division='" + txtDivision.Text.ToString() + "'";
            }

            if (txtBankName.SelectedValue != "-Select-")
            {
                query = query + " and EmpDet.BankName='" + txtBankName.SelectedValue.ToString() + "'";
            }

            //query = query + "and SalDet.FromDate = convert(datetime,'" + MyDate1 + "', 105) and SalDet.ToDate = Convert(Datetime,'" + MyDate2 + "', 105)";
            query = query + " and EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "'" +
            " and SalDet.Ccode='" + SessionCcode + "' and SalDet.Lcode='" + SessionLcode + "'" +
            " and AD.Ccode='" + SessionCcode + "' and AD.Lcode='" + SessionLcode + "'" +
                //" and SalDet.WagesType='" + rbsalary.SelectedValue + "' and SalDet.Salarythrough='2'" +
                //Check here
                //" and SalDet.Salarythrough='2'" +
                    " group by EmpDet.BirthDate,SalDet.EmpNo,EmpDet.FirstName,EmpDet.AccountNo,EmpDet.DeptName,EmpDet.ExistingCode," +
                    " SalDet.BasicandDA,SalDet.ThreesidedAmt,SalDet.OTHoursAmtNew,SalDet.RoundOffNetPay_IF,SalDet.Words,SalDet.WorkedDays,SalDet.Month,SalDet.Year,SalDet.SalaryDate," +
                    " SalDet.NetPay,MstDpt.DeptName,SalDet.Employee_ESI_IF Order by EmpDet.ExistingCode Asc";
            //}
            DataTable dt_1 = new DataTable();
            dt_1 = objdata.RptEmployeeMultipleDetails(query);
            //SqlCommand cmd = new SqlCommand(query, con);
            //DataTable dt_1 = new DataTable();
            //SqlDataAdapter sda = new SqlDataAdapter(cmd);
            ////DataSet ds1 = new DataSet();
            //con.Open();
            //sda.Fill(dt_1);
            //con.Close();
            OtherBankSalaryGridView.DataSource = dt_1;
            OtherBankSalaryGridView.DataBind();

            string attachment = "attachment;filename=OthersBankSalaryDetails.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            DataTable dt = new DataTable();
            query = "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dt = objdata.RptEmployeeMultipleDetails(query);
            //dt = objdata.Company_retrive(SessionCcode, SessionLcode);
            if (dt.Rows.Count > 0)
            {
                CmpName = dt.Rows[0]["Cname"].ToString();
                Cmpaddress = (dt.Rows[0]["Address1"].ToString() + ", " + dt.Rows[0]["Address2"].ToString() + ", " + dt.Rows[0]["Location"].ToString() + "-" + dt.Rows[0]["Pincode"].ToString());
            }

            StringWriter stw = new StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            OtherBankSalaryGridView.RenderControl(htextw);
            Response.Write("<table>");
            Response.Write("<tr align='Center'>");
            Response.Write("<td colspan='6' style='font-size:20.0pt;font-weight:bold;text-decoration:underline;''>");
            Response.Write("EVEREADY SPINING MILL");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr align='Center'>");
            Response.Write("<td colspan='6' style='font-size:13.0pt;font-weight:bold;text-decoration:underline;''>");
            Response.Write("" + SessionLcode + "");
            Response.Write("</td>");
            Response.Write("</tr>");
            if (txtBankName.SelectedValue != "-Select-")
            {
                Response.Write("<tr align='Center'>");
                Response.Write("<td colspan='6' style='font-size:12.0pt;font-weight:bold;text-decoration:underline;''>");
                Response.Write("BANK NAME : " + txtBankName.SelectedValue + "");
                Response.Write("</td>");
                Response.Write("</tr>");
            }
            else
            {
                Response.Write("<tr align='Center'>");
                Response.Write("<td colspan='6' style='font-size:12.0pt;font-weight:bold;text-decoration:underline;''>");
                //Response.Write("" + Cmpaddress + "");
                Response.Write("");
                Response.Write("</td>");
                Response.Write("</tr>");
            }
            //if (rbsalary.SelectedValue == "2")
            //{
            //    Response.Write("<tr align='center'>");
            //    Response.Write("<td colspan='6' style='font-size:11.0pt;font-weight:bold;text-decoration:underline;''>");
            //    Response.Write("BANK A/C SALARY DETAILS FOR THE MONTH OF " + ddlMonths.SelectedValue.ToUpper().ToString() + " - " + YR);
            //    Response.Write("</td>");
            //    Response.Write("</tr>");
            //}
            //else
            //{
            Response.Write("<tr align='center'>");
            Response.Write("<td colspan='6' style='font-size:11.0pt;font-weight:bold;text-decoration:underline;''>");
            Response.Write("BANK A/C SALARY DETAILS FOR THE MONTH OF " + ddlMonths.SelectedItem.Text + " - " + ddlFinance.SelectedValue);
            Response.Write("</td>");
            Response.Write("</tr>");
            //}
            Response.Write("</table>");

            Response.Write(stw.ToString());
            //Get Row Count
            string bank_row_count = "";
            bank_row_count = (Convert.ToDecimal(5) + Convert.ToDecimal(OtherBankSalaryGridView.Rows.Count)).ToString();

            if (OtherBankSalaryGridView.Rows.Count.ToString() != "0")
            {
                Response.Write("<table border='1'>");
                Response.Write("<tr style='font-size:12.0pt;font-weight:bold;'>");
                Response.Write("<td colspan='4' align='right'>Grand Total</td>");
                Response.Write("<td>=sum(E6:E" + bank_row_count.ToString() + ")</td>");
                Response.Write("<td></td>");
                Response.Write("</tr></table>");
            }
            Response.End();
            Response.Clear();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Downloaded Successfully');", true);
        }
    }
}

