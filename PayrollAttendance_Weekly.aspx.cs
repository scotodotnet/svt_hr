﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;
public partial class PayrollAttendance_New : System.Web.UI.Page
{

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string Division;
    DataTable Log_DS = new DataTable();

    BALDataAccess objdata = new BALDataAccess();


    DataTable AutoDTable = new DataTable();
    DataTable mEmployeeDT = new DataTable();
    DataTable MEmployeeDS = new DataTable();
    DataTable mDataSet = new DataTable();
    DataTable NFH_DS = new DataTable();
    DataTable NFH_Type_Ds = new DataTable();
    DataTable WH_DS = new DataTable();

    DateTime NFH_Date = new DateTime();
    DateTime DOJ_Date_Format = new DateTime();
    string qry_nfh = "";
    string SSQL = "";
    DateTime Week_Off_Date = new DateTime();
    DateTime WH_DOJ_Date_Format = new DateTime();
    Boolean Check_Week_Off_DOJ = false;
    double NFH_Present_Check = 0;
    decimal NFH_Days_Count = 0;
    decimal AEH_NFH_Days_Count = 0;
    decimal LBH_NFH_Days_Count = 0;
    decimal NFH_Days_Present_Count = 0;
    decimal WH_Count = 0;
    decimal WH_Present_Count = 0;
    decimal Present_Days_Count = 0;
    int Fixed_Work_Days = 0;
    decimal Spinning_Incentive_Days = 0;

    decimal NFH_Double_Wages_Checklist = 0;
    decimal NFH_Double_Wages_Statutory = 0;
    decimal NFH_Double_Wages_Manual = 0;
    decimal NFH_WH_Days_Mins = 0;
    decimal NFH_Single_Wages = 0;
    int Month_Mid_Total_Days_Count;
    string NFH_Type_Str = "";
    string NFH_Name_Get_Str = "";
    string NFH_Dbl_Wages_Statutory_Check = "";
    string Emp_WH_Day = "";
    string DOJ_Date_Str = "";


    string NFH_Date_P_Date = "";
    DateTime date1;
    DateTime date2;
    DateTime Date2 = new DateTime();
    int intK;
    string FromDate;
    string ToDate;
    string Date_Value_Str;
    string Date_value_str1;
    string[] Time_Minus_Value_Check;
    string WagesType;
    string WagesValue;

    System.Web.UI.WebControls.DataGrid grid =
                            new System.Web.UI.WebControls.DataGrid();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {

            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report- Payroll Attendance ";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("ManualEntry"));
                //li.Attributes.Add("class", "droplink active open");
            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();

            WagesValue = Request.QueryString["Wages"].ToString();
            Division = Request.QueryString["Division"].ToString();
            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["Todate"].ToString();


            if (WagesValue == "WEEKLY")
            {
                //if (SessionAdmin == "2")
                //{

                //    mEmployeeDT.Columns.Add("Machine ID");
                //    mEmployeeDT.Columns.Add("Token No");
                //    mEmployeeDT.Columns.Add("EmpName");
                //    mEmployeeDT.Columns.Add("Days");
                //    mEmployeeDT.Columns.Add("H.Allowed");
                //    mEmployeeDT.Columns.Add("N/FH");
                //    mEmployeeDT.Columns.Add("OT Days");
                //    mEmployeeDT.Columns.Add("SPG Allow");
                //    mEmployeeDT.Columns.Add("Canteen Days Minus");
                //    mEmployeeDT.Columns.Add("OT Hours");
                //    mEmployeeDT.Columns.Add("W.H");
                //    mEmployeeDT.Columns.Add("Fixed W.Days");
                //    mEmployeeDT.Columns.Add("NFH W.Days");
                //    mEmployeeDT.Columns.Add("Total Month Days");
                //    mEmployeeDT.Columns.Add("NFH Worked Days");
                //    mEmployeeDT.Columns.Add("NFH D.W Statutory");
                //    mEmployeeDT.Columns.Add("AEH");
                //    mEmployeeDT.Columns.Add("LBH");
                //    NonAdminPayrollAttn();
                //}else
                {
                    Weeklywages();
                }
            }
            if (WagesValue == "WEEKLY-HINDI")
            {
                Weeklywages();
            }
            if (WagesValue == "MONTHLY-BIO")
            {
                BioMonthly();
            }
        }
    }

    private void BioMonthly()
    {
        try
        {

            SSQL = "";
            SSQL = "Select ExistingCode,FirstName,OTEligible from Employee_Mst where Wages='" + WagesValue + "' and  IsActive='Yes' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
            DataTable Emp_MstDT = new DataTable();
            Emp_MstDT = objdata.RptEmployeeMultipleDetails(SSQL);

            DateTime fmDate = Convert.ToDateTime(FromDate);
            DateTime TDate = Convert.ToDateTime(ToDate);

            mEmployeeDT.Columns.Add("S.No");
            mEmployeeDT.Columns.Add("Emp.No");
            mEmployeeDT.Columns.Add("Name");

            int daycount = 0;
            int daysAdded = 0;

            // For total days Calculation for eacxh employee
            int TotalDays = (int)((TDate - fmDate).TotalDays);

            daycount = (int)((TDate - fmDate).TotalDays);
            daysAdded = 0;


            while (daycount >= 0)
            {
                DateTime dayy = Convert.ToDateTime(fmDate.AddDays(daysAdded).ToShortDateString());
                mEmployeeDT.Columns.Add("AT" + daysAdded);
                mEmployeeDT.Columns.Add("OT" + daysAdded);
                daycount -= 1;
                daysAdded += 1;
            }

            mEmployeeDT.Columns.Add("Days");

            if (Emp_MstDT.Rows.Count > 0)
            {
                for (int EmpRw = 0; EmpRw < Emp_MstDT.Rows.Count; EmpRw++)
                {
                    decimal Emp_WH = 0;
                    decimal Emp_OT = 0;
                    decimal Total_WH = 0;
                    decimal Total_OT = 0;

                    decimal Total_Wages = 0;

                    mEmployeeDT.NewRow();
                    mEmployeeDT.Rows.Add();
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["S.No"] = mEmployeeDT.Rows.Count;
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Emp.No"] = Emp_MstDT.Rows[EmpRw]["ExistingCode"];
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Name"] = Emp_MstDT.Rows[EmpRw]["FirstName"];

                    daycount = (int)((TDate - fmDate).TotalDays);
                    daysAdded = 0;
                    while (daycount >= 0)
                    {
                        DateTime dayy = Convert.ToDateTime(fmDate.AddDays(daysAdded).ToShortDateString());
                        string QUERY = "";
                        DataTable HROT_DT = new DataTable();
                        QUERY = "Select Total_Hrs from LogTime_Days where ExistingCode='" + Emp_MstDT.Rows[EmpRw]["ExistingCode"] + "' And CONVERT(datetime,Attn_Date,103) = CONVERT(datetime,'" + dayy.ToString("dd/MM/yyyy") + "',103) and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                        HROT_DT = objdata.RptEmployeeMultipleDetails(QUERY);

                        //if (Emp_MstDT.Rows[EmpRw]["ExistingCode"].ToString() == "597" || Emp_MstDT.Rows[EmpRw]["ExistingCode"].ToString() == "506")
                        //{
                        //    string s = "0";
                        //}

                        if (HROT_DT.Rows.Count > 0)
                        {
                            Emp_WH = Convert.ToDecimal(HROT_DT.Rows[0]["Total_Hrs"]);
                        }
                        else
                        {
                            Emp_WH = 0;
                        }

                        if (Emp_WH >= 8)
                        {
                            if (Emp_MstDT.Rows[EmpRw]["OTEligible"].ToString() == "1")
                            {
                                Emp_OT = Emp_WH - 8;
                                Emp_WH = 8;


                                mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["AT" + daysAdded] = "8";
                                mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["OT" + daysAdded] = Emp_OT;

                                Total_WH = Total_WH + Emp_WH;
                                Total_OT = Total_OT + Emp_OT;

                            }
                            else
                            {
                                Emp_WH = 8;
                                mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["AT" + daysAdded] = "8";
                                mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["OT" + daysAdded] = "0";
                                Total_WH = Total_WH + Emp_WH;
                            }
                        }
                        else if (Emp_WH < 8)
                        {
                            //if (Emp_MstDT.Rows[EmpRw]["OTEligible"].ToString() == "1")
                            //{
                            //    Emp_OT = Emp_WH - 4;
                            //    Emp_WH = 4;

                            //    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["AT" + daysAdded] = "4";
                            //    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["OT" + daysAdded] = Emp_WH - 4;

                            //    Total_WH = Total_WH + Emp_WH;
                            //    Total_OT = Total_OT + Emp_OT;

                            //}
                            //else
                            //{
                            //Emp_WH = 4;
                            mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["AT" + daysAdded] = Emp_WH;
                            mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["OT" + daysAdded] = "0";
                            Total_WH = Total_WH + Emp_WH;
                            //}
                        }
                        daycount -= 1;
                        daysAdded += 1;
                    }

                    decimal totalDays = Total_WH / 8;
                    totalDays = Math.Round(Convert.ToDecimal(totalDays), 1, MidpointRounding.AwayFromZero);
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Days"] = totalDays.ToString("00.00");

                    //For Salary Calculation Only
                    SSQL = "";
                    SSQL = "Select * from Wages_Amt where Wages='" + WagesValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                    DataTable dt = new DataTable();
                    dt = objdata.RptEmployeeMultipleDetails(SSQL);

                    decimal basesal = 0;
                    decimal perday = 0;
                    decimal otamt = 0;
                    decimal otsal = 0;
                    decimal bus_inc = 0;
                    decimal masthiri = 0;
                    decimal adv = 0;
                    decimal total_wages = 0;

                    //for weekly salary based on total present
                    if (dt.Rows.Count == 1)
                    {
                        if (totalDays < 5)
                        {
                            perday = Convert.ToDecimal(dt.Rows[0]["5"].ToString());
                        }
                        else if (totalDays >= 5 & totalDays < 7)
                        {
                            perday = Convert.ToDecimal(dt.Rows[0]["6"].ToString());
                        }
                        else if (totalDays >= 7)
                        {
                            perday = Convert.ToDecimal(dt.Rows[0]["7"].ToString());
                        }
                    }

                    //  mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Perday"] = perday.ToString();

                    //For Basic wages calculation
                    basesal = totalDays * perday;
                    //mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["BaseAmt"] = basesal.ToString();

                    //For OT calculation
                    otamt = Math.Round(Convert.ToDecimal(perday / 8), 0, MidpointRounding.AwayFromZero);
                    otsal = Math.Round(Convert.ToDecimal(otamt * Total_OT), 0, MidpointRounding.AwayFromZero);

                    // mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["OTHrs"] = Total_OT.ToString();
                    // mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["PerHr"] = otamt.ToString();
                    // mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["OTAmt"] = otsal.ToString();

                    //For Incentive and Advance calculation
                    bus_inc = 0;
                    masthiri = 0;
                    adv = 0;

                    //For NetPay
                    total_wages = Math.Round(Convert.ToDecimal((basesal + otsal + bus_inc + masthiri) - adv), 0, MidpointRounding.AwayFromZero);

                    //mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Bus_INC"] = bus_inc.ToString();
                    //mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Masthiri"] = masthiri.ToString();
                    //mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Adv"] = adv.ToString();
                    //mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Total_Wages"] = total_wages.ToString();


                }
            }

            if (mEmployeeDT.Rows.Count > 0)
            {
                UploadDataTableToExcel(mEmployeeDT);
            }

        }
        catch (Exception e)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('" + e.ToString() + "');", true);
        }
    }

    // for Weekly Wages
    private void Weeklywages()
    {
        decimal Emp_WH = 0;
        decimal Emp_OT = 0;
        decimal Total_WH = 0;
        decimal Total_OT = 0;
        decimal MID_Present = 0;
        string Empno = "";

        bool WH_Check = false;
        try
        {

            SSQL = "";
            SSQL = "Select ExistingCode,FirstName,OTEligible,WeekOff from Employee_Mst where Wages='" + WagesValue + "' and  IsActive='Yes' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
            if (SessionAdmin == "2")
            {
                SSQL = SSQL + " and Eligible_PF='1'";
            }
            DataTable Emp_MstDT = new DataTable();
            Emp_MstDT = objdata.RptEmployeeMultipleDetails(SSQL);

            DateTime fmDate = Convert.ToDateTime(FromDate);
            DateTime TDate = Convert.ToDateTime(ToDate);

            mEmployeeDT.Columns.Add("S.No");
            mEmployeeDT.Columns.Add("Emp.No");
            mEmployeeDT.Columns.Add("Name");

            int daycount = 0;
            int daysAdded = 0;

            // For total days Calculation for eacxh employee
            int TotalDays = (int)((TDate - fmDate).TotalDays);

            daycount = (int)((TDate - fmDate).TotalDays);
            daysAdded = 0;


            while (daycount >= 0)
            {
                DateTime dayy = Convert.ToDateTime(fmDate.AddDays(daysAdded).ToShortDateString());
                mEmployeeDT.Columns.Add("AT" + daysAdded);
                if (SessionUserType != "2")
                {
                    mEmployeeDT.Columns.Add("OT" + daysAdded);

                }
                daycount -= 1;
                daysAdded += 1;
            }

            mEmployeeDT.Columns.Add("Days");
            mEmployeeDT.Columns.Add("Total_OTHrs");
            mEmployeeDT.Columns.Add("MID Incentive Days");

            if (Emp_MstDT.Rows.Count > 0)
            {
                for (int EmpRw = 0; EmpRw < Emp_MstDT.Rows.Count; EmpRw++)
                {
                    Emp_WH = 0;
                    Emp_OT = 0;
                    Total_WH = 0;
                    Total_OT = 0;
                    MID_Present = 0;

                    WH_Check = false;



                    decimal Total_Wages = 0;

                    mEmployeeDT.NewRow();
                    mEmployeeDT.Rows.Add();
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["S.No"] = mEmployeeDT.Rows.Count;
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Emp.No"] = Emp_MstDT.Rows[EmpRw]["ExistingCode"];
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Name"] = Emp_MstDT.Rows[EmpRw]["FirstName"];

                    if (Emp_MstDT.Rows[EmpRw]["ExistingCode"].ToString() == "1001")
                    {
                        string str = "sdsd";
                    }

                    daycount = (int)((TDate - fmDate).TotalDays);
                    daysAdded = 0;
                    while (daycount >= 0)
                    {
                        DateTime ShiftdateStartIN_Check = new DateTime();
                        DateTime ShiftdateEndIN_Check = new DateTime();
                        DateTime EmpdateIN_Check = new DateTime();
                        string Start_IN = "";
                        string End_In = "";

                        string EmpINPunch = "";
                        string EmpOUTPunch = "";
                        string EmpPresent = "";
                        string EM = "";


                        DateTime dayy = Convert.ToDateTime(fmDate.AddDays(daysAdded).ToShortDateString());
                        string QUERY = "";
                        DataTable HROT_DT = new DataTable();
                        //QUERY = "Select isnull(Late_Total_Hrs,'0') as Late_Total_Hrs,Total_Hrs,Shift,Total_Hrs1,Attn_Date_Str,isnull(MID_Present,0) MID_Present from LogTime_Days where ExistingCode='" + Emp_MstDT.Rows[EmpRw]["ExistingCode"] + "' And CONVERT(datetime,Attn_Date,103) = CONVERT(datetime,'" + dayy.ToString("dd/MM/yyyy") + "',103) and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                        QUERY = "Select isnull(Late_Total_Hrs,'0') as Late_Total_Hrs,Total_Hrs,Shift,Total_Hrs1,Attn_Date_Str,isnull(MID_Present,0) MID_Present,TimeIN,TimeOUT,Present from LogTime_Days where ExistingCode='" + Emp_MstDT.Rows[EmpRw]["ExistingCode"] + "' And CONVERT(datetime,Attn_Date,103) = CONVERT(datetime,'" + dayy.ToString("dd/MM/yyyy") + "',103) and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                        HROT_DT = objdata.RptEmployeeMultipleDetails(QUERY);
                        string MID = Emp_MstDT.Rows[EmpRw]["ExistingCode"].ToString();
                        if (MID == "3803")
                        {
                            string stop = "";
                        }
                        string Shift = "";
                        Empno = MID;
                        if (HROT_DT.Rows.Count != 0)
                        {
                            Shift = HROT_DT.Rows[0]["Shift"].ToString();
                            MID_Present = MID_Present + Convert.ToDecimal(HROT_DT.Rows[0]["MID_Present"].ToString());
                            EmpINPunch = HROT_DT.Rows[0]["TimeIN"].ToString();
                            EmpOUTPunch = HROT_DT.Rows[0]["TimeOUT"].ToString();
                            EmpPresent = HROT_DT.Rows[0]["Present"].ToString();
                            EM = HROT_DT.Rows[0]["Total_Hrs1"].ToString();

                            EmpdateIN_Check = Convert.ToDateTime(EM);
                            string hrs = Convert.ToDecimal(TimeSpan.Parse("10:09").TotalHours).ToString();
                        }
                        else
                        {
                            Shift = "Leave";
                        }
                        DateTime today = DateTime.Today;

                        //shift2 Employee only 16.20 to 16.59 hours working employee round off concept
                        Start_IN = Convert.ToDateTime(today).AddDays(Convert.ToDouble(0)).ToString("dd/MM/yyyy") + " " + "03:20:00 PM";
                        End_In = Convert.ToDateTime(today).AddDays(Convert.ToDouble(0)).ToString("dd/MM/yyyy") + " " + "03:59:00 PM";
                        ShiftdateStartIN_Check = Convert.ToDateTime(Start_IN);
                        ShiftdateEndIN_Check = Convert.ToDateTime(End_In);


                        if (MID == "3705")
                        {
                            MID = "3705";
                        }


                        if (Shift == "SHIFT2")
                        {
                            if (HROT_DT.Rows.Count > 0)
                            {

                                Emp_WH = Convert.ToDecimal(TimeSpan.Parse(HROT_DT.Rows[0]["Total_Hrs1"].ToString()).TotalHours);
                                //if (Convert.ToDecimal(Emp_WH) >= Convert.ToDecimal(7.5) && Convert.ToDecimal(Emp_WH) <= Convert.ToDecimal(8.25))
                                //{
                                //    Emp_WH = 8;
                                //}
                                //else
                                //{
                                Emp_WH = Convert.ToDecimal(Emp_WH.ToString("00.00"));
                                // }
                            }
                            else
                            {
                                Emp_WH = 0;
                            }
                            if (EmpdateIN_Check >= ShiftdateStartIN_Check && EmpdateIN_Check <= ShiftdateEndIN_Check)
                            {
                                Emp_WH = 16;
                            }
                        }
                        else
                        {
                            if (HROT_DT.Rows.Count > 0)
                            {
                                Emp_WH = Convert.ToDecimal(TimeSpan.Parse(HROT_DT.Rows[0]["Total_Hrs1"].ToString()).TotalHours);
                                //if (Convert.ToDecimal(EmpPresent) == Convert.ToDecimal(1.0) && Emp_WH == 7)
                                //{
                                //    Emp_WH = 8;
                                ////}
                                //Emp_WH = Convert.ToDecimal(HROT_DT.Rows[0]["Total_Hrs1"]);
                                //if (Convert.ToDecimal(Emp_WH) >= Convert.ToDecimal(7.5) && Convert.ToDecimal(Emp_WH) <= Convert.ToDecimal(8.25))
                                //{
                                //    Emp_WH = 8;
                                //}
                                //else
                                //{
                                Emp_WH = Convert.ToDecimal(Emp_WH.ToString("00.00"));
                                // }
                            }
                            else
                            {
                                Emp_WH = 0;
                            }

                        }

                        if (SessionUserType == "2")
                        {
                            if (Emp_MstDT.Rows[EmpRw]["WeekOff"].ToString().ToUpper() == dayy.DayOfWeek.ToString().ToUpper())
                            {
                                mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["AT" + daysAdded] = "WH";
                                Emp_WH = 0;
                                WH_Check = true;
                            }
                            else
                            {
                                WH_Check = false;
                            }
                        }

                        if (!WH_Check)
                        {
                            if (Emp_WH >= 8)
                            {
                                if (Emp_MstDT.Rows[EmpRw]["OTEligible"].ToString() == "1")
                                {
                                    if (Convert.ToDecimal(Emp_WH) > Convert.ToDecimal(8.5))
                                    {
                                        Emp_OT = Emp_WH - 8;
                                        string[] split = Emp_OT.ToString().Split('.');
                                        if (split.Length > 1)
                                        {
                                            if (Convert.ToDecimal(split[1]) >= Convert.ToDecimal("30"))
                                            {
                                                Emp_OT = Convert.ToDecimal((Emp_OT + 1).ToString().Split('.').First());
                                            }
                                            else
                                            {
                                                Emp_OT = Convert.ToDecimal(Emp_OT.ToString().Split('.').First());
                                            }
                                        }
                                        else
                                        {
                                            Emp_OT = Convert.ToDecimal(Emp_OT.ToString());
                                        }
                                    }
                                    else
                                    {
                                        Emp_OT = 0;
                                    }

                                    Emp_WH = 8;

                                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["AT" + daysAdded] = "8";

                                    if (SessionUserType != "2")
                                    {
                                        mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["OT" + daysAdded] = Emp_OT;
                                    }

                                    Total_WH = Total_WH + Emp_WH;
                                    Total_OT = Total_OT + Emp_OT;
                                }
                                else
                                {
                                    Emp_WH = 8;
                                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["AT" + daysAdded] = "8";
                                    if (SessionUserType != "2")
                                    {
                                        mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["OT" + daysAdded] = "0";

                                    }
                                    Emp_WH = Convert.ToDecimal(Emp_WH.ToString("00"));
                                    Total_WH = Total_WH + Emp_WH;
                                }
                            }
                            else if (Emp_WH < 8)
                            {
                                if (Emp_WH == 0 && MID_Present != 0)
                                {
                                    if (MID_Present == Convert.ToDecimal(1))
                                    {
                                        Emp_WH = 4;
                                    }
                                    else if (MID_Present == Convert.ToDecimal(0.5))
                                    {
                                        Emp_WH = 4;
                                    }
                                }
                                Emp_WH = Convert.ToDecimal(Emp_WH.ToString("00"));
                                mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["AT" + daysAdded] = Emp_WH;
                                if (SessionUserType != "2")
                                {
                                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["OT" + daysAdded] = "0";
                                }
                                Total_WH = Total_WH + Emp_WH;
                            }
                        }
                        daycount -= 1;
                        daysAdded += 1;
                    }

                    decimal totalDays = Total_WH / 8;
                    totalDays = Math.Round(Convert.ToDecimal(totalDays), 1, MidpointRounding.AwayFromZero);
                    if (SessionUserType == "2")
                    {
                        if (totalDays >= 21)
                        {
                            totalDays = 21;
                        }
                    }

                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Days"] = totalDays.ToString("00.00");

                    if (SessionUserType != "2")
                    {
                        mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Total_OTHrs"] = Total_OT;
                    }

                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["MID Incentive Days"] = MID_Present;
                }

                if (mEmployeeDT.Rows.Count > 0)
                {
                    UploadDataTableToExcel(mEmployeeDT);
                }
            }
        }
        catch (Exception e)
        {
            Emp_WH = 0;
            Empno = "";
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "alert('" + e.ToString() + "');", true);
        }
    }

    public void NonAdminPayrollAttn()
    {

        try
        {
            int intI;
            SSQL = "";
            SSQL = "Select LTD.MachineID,LTD.ExistingCode,EM.FirstName,sum(LTD.Present) as Days,";
            SSQL = SSQL + " EM.DOJ,EM.Weekoff from LogTime_Days LTD inner join Employee_Mst EM on";
            SSQL = SSQL + " EM.CompCode=LTD.CompCode And EM.LocCode=LTD.LocCode And LTD.MachineID=EM.MachineID";
            SSQL = SSQL + " where LTD.CompCode='" + SessionCcode + "' And LTD.LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And EM.CompCode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And EM.Wages='" + WagesValue + "' And (EM.IsActive='Yes' or CONVERT(DATETIME,EM.DOR,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103))";
            SSQL = SSQL + " and CONVERT(DATETIME,LTD.Attn_Date_Str,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103)";
            SSQL = SSQL + " And  CONVERT(DATETIME,LTD.Attn_Date_Str,103)<= CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103) ";
            SSQL = SSQL + " And LTD.Present!='0.0' and EM.Eligible_PF='1' ";
            if (Division != "-Select-")
            {
                SSQL = SSQL + " And EM.Division = '" + Division + "'";
            }
            //If Trim(txtDivision.Text) <> "" Then SSQL = SSQL + " And EM.Division = '" + txtDivision.Text + "'";

            SSQL = SSQL + " Group by LTD.MachineID,LTD.ExistingCode,EM.FirstName,EM.DOJ,EM.Weekoff ";
            SSQL = SSQL + " Order by LTD.ExistingCode Asc";

            Log_DS = objdata.RptEmployeeMultipleDetails(SSQL);

            intI = 2;
            for (int i = 0; i < Log_DS.Rows.Count; i++)
            {

                // 'Final Output Variable Declaration
                NFH_Days_Count = 0;
                AEH_NFH_Days_Count = 0;
                LBH_NFH_Days_Count = 0;
                NFH_Days_Present_Count = 0;
                WH_Count = 0;
                WH_Present_Count = 0;
                Present_Days_Count = Convert.ToDecimal(Log_DS.Rows[i]["Days"]);
                if (Present_Days_Count >= 21)
                {
                    Present_Days_Count = 21;
                }
                Fixed_Work_Days = 0;
                Spinning_Incentive_Days = 0;

                NFH_Double_Wages_Checklist = 0;
                NFH_Double_Wages_Statutory = 0;
                NFH_Double_Wages_Manual = 0;
                NFH_WH_Days_Mins = 0;
                NFH_Single_Wages = 0;

                NFH_Type_Str = "";
                NFH_Name_Get_Str = "";
                NFH_Dbl_Wages_Statutory_Check = "";
                Emp_WH_Day = "";
                DOJ_Date_Str = "";

                DOJ_Date_Str = Log_DS.Rows[i]["DOJ"].ToString();
                Emp_WH_Day = Log_DS.Rows[i]["WeekOff"].ToString();

                //  'NFH Check Start
                string Machineid = Log_DS.Rows[i]["MachineID"].ToString();
                if (Machineid == "1886")
                {
                    Machineid = "1886";
                }

                qry_nfh = "Select NFHDate from NFH_Mst where  CONVERT(DATETIME,DateStr,103) >=  CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103) ";
                qry_nfh = qry_nfh + " And  CONVERT(DATETIME,DateStr,103) <=  CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103) ";
                NFH_DS = objdata.RptEmployeeMultipleDetails(qry_nfh);
                if (NFH_DS.Rows.Count > 0)
                {

                    //  'Date OF Joining Check Start
                    for (int k = 0; k < NFH_DS.Rows.Count; k++)
                    {
                        NFH_Date = Convert.ToDateTime(NFH_DS.Rows[k]["NFHDate"]);
                        // 'NFH Day Present Check

                        NFH_Date_P_Date = String.Format(Convert.ToString(NFH_Date), "yyyy/MM/dd");

                        SSQL = "";
                        SSQL = "Select * from LogTime_Days where Attn_Date=CONVERT(DATETIME,'" + Convert.ToDateTime(NFH_Date_P_Date).ToString("dd/MM/yyyy") + "',103)  ";
                        SSQL = SSQL + "  And MachineID='" + Log_DS.Rows[i]["MachineID"] + "' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";

                        mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (mDataSet.Rows.Count != 0)
                        {

                            NFH_Present_Check = Convert.ToDouble(mDataSet.Rows[0]["Present"]);
                        }
                        else
                        {

                            NFH_Present_Check = 0;

                        }
                        //'Get NFh Type

                        SSQL = "Select * from NFH_Mst where CONVERT(DATETIME,DateStr,103) >=  CONVERT(DATETIME,'" + Convert.ToDateTime(NFH_Date_P_Date).ToString("dd/MM/yyyy") + "',103) ";
                        NFH_Type_Ds = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (NFH_Type_Ds.Rows.Count != 0)
                        {

                            NFH_Type_Str = NFH_Type_Ds.Rows[0]["NFH_Type"].ToString();
                            NFH_Dbl_Wages_Statutory_Check = NFH_Type_Ds.Rows[0]["Dbl_Wages_Statutory"].ToString();
                            NFH_Name_Get_Str = NFH_Type_Ds.Rows[0]["Form25DisplayText"].ToString();

                        }
                        else
                        {
                            NFH_Type_Str = "";
                            NFH_Dbl_Wages_Statutory_Check = "";
                        }
                        if (DOJ_Date_Str != "")
                        {

                            DOJ_Date_Format = Convert.ToDateTime(DOJ_Date_Str);
                            if (DOJ_Date_Format.Date < NFH_Date.Date)
                            {
                                if (NFH_Type_Str == "WH Minus")
                                {
                                    NFH_WH_Days_Mins = NFH_WH_Days_Mins + 1;

                                }
                                else
                                {

                                    NFH_Days_Present_Count = NFH_Days_Present_Count + Convert.ToDecimal(NFH_Present_Check);
                                    if (NFH_Type_Str == "Double Wages Checklist")
                                    {

                                        NFH_Double_Wages_Checklist = NFH_Double_Wages_Checklist + Convert.ToDecimal(NFH_Present_Check);

                                        if (NFH_Dbl_Wages_Statutory_Check == "Yes")
                                        {
                                            NFH_Double_Wages_Statutory = NFH_Double_Wages_Statutory + Convert.ToDecimal(NFH_Present_Check);

                                        }
                                    }

                                    if (NFH_Type_Str == "Double Wages Manual")
                                    {
                                        NFH_Double_Wages_Manual = NFH_Double_Wages_Manual + Convert.ToDecimal(NFH_Present_Check);
                                    }

                                    NFH_Days_Count = NFH_Days_Count + 1;
                                    if (NFH_Name_Get_Str == "AEH")
                                    {
                                        AEH_NFH_Days_Count = AEH_NFH_Days_Count + 1;
                                    }
                                    if (NFH_Name_Get_Str == "LBH")
                                    {
                                        LBH_NFH_Days_Count = LBH_NFH_Days_Count + 1;
                                    }

                                }
                            }
                        }
                        else
                        {
                            NFH_Days_Count = NFH_Days_Count + 1;

                            if (NFH_Name_Get_Str == "AEH")
                            {
                                AEH_NFH_Days_Count = AEH_NFH_Days_Count + 1;

                            }
                            if (NFH_Name_Get_Str == "LBH")
                            {
                                LBH_NFH_Days_Count = LBH_NFH_Days_Count + 1;

                            }

                            if (NFH_Type_Str == "WH Minus")
                            {
                                NFH_WH_Days_Mins = NFH_WH_Days_Mins + 1;
                            }
                            else
                            {
                                NFH_Days_Present_Count = NFH_Days_Present_Count + Convert.ToDecimal(NFH_Present_Check);

                                if (NFH_Type_Str == "Double Wages Checklist")
                                {
                                    NFH_Double_Wages_Checklist = NFH_Double_Wages_Checklist + Convert.ToDecimal(NFH_Present_Check);
                                    if (NFH_Dbl_Wages_Statutory_Check == "Yes")
                                    {
                                        NFH_Double_Wages_Statutory = NFH_Double_Wages_Statutory + Convert.ToDecimal(NFH_Present_Check);
                                    }
                                }
                                if (NFH_Type_Str == "Double Wages Manual")
                                {
                                    NFH_Double_Wages_Manual = NFH_Double_Wages_Manual + Convert.ToDecimal(NFH_Present_Check);
                                }
                            }
                        }
                    }
                }// NFH Check END


                //'Week of Check

                Check_Week_Off_DOJ = false;

                SSQL = "Select Present,Attn_Date from LogTime_Days where MachineID='" + Log_DS.Rows[i]["MachineID"] + "' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                SSQL = SSQL + " And upper(DATENAME(weekday,Attn_Date))=upper('" + Emp_WH_Day + "')";
                SSQL = SSQL + " And Attn_Date >= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + " And Attn_Date <=CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103)";
                WH_DS = objdata.RptEmployeeMultipleDetails(SSQL);
                for (int j = 0; j < WH_DS.Rows.Count; j++)
                {

                    Week_Off_Date = Convert.ToDateTime(WH_DS.Rows[j]["Attn_Date"]);
                    if (WagesType.ToUpper() == "STAFF".ToUpper() || WagesType.ToUpper() == "Watch & Ward".ToUpper() || WagesType.ToUpper() == "Manager".ToUpper())
                    {
                        if (DOJ_Date_Str != "")
                        {

                            WH_DOJ_Date_Format = Convert.ToDateTime(DOJ_Date_Str);
                            if (WH_DOJ_Date_Format.Date <= Week_Off_Date.Date)
                            {

                                Check_Week_Off_DOJ = true;
                            }
                            else
                            {
                                Check_Week_Off_DOJ = false;
                            }
                        }
                        else
                        {
                            Check_Week_Off_DOJ = true;
                        }
                    }
                    else
                    {
                        Check_Week_Off_DOJ = true;
                    }

                    if (Check_Week_Off_DOJ == true)
                    {

                        WH_Count = WH_Count + 1;
                        //'NFH Day Check
                        // String.Format(Convert.ToString(dtime), "yyyy/MM/dd");

                        string NFH_Date_WH_Date = Convert.ToDateTime(Week_Off_Date).AddDays(0).ToString("yyyy/MM/dd");
                        SSQL = "Select * from NFH_Mst where CONVERT(DATETIME,DateStr,103) >=  CONVERT(DATETIME,'" + Convert.ToDateTime(NFH_Date_WH_Date).ToString("dd/MM/yyyy") + "',103) ";
                        mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (mDataSet.Rows.Count > 0)
                        {
                            if (mDataSet.Rows.Count != 0)
                            {
                                NFH_Type_Str = mDataSet.Rows[0]["NFH_Type"].ToString();
                                NFH_Dbl_Wages_Statutory_Check = NFH_Type_Ds.Rows[0]["Dbl_Wages_Statutory"].ToString();
                            }
                            else
                            {
                                NFH_Type_Str = "";
                                NFH_Dbl_Wages_Statutory_Check = "";
                            }
                            if (NFH_Type_Str == "WH Minus")
                            {
                                WH_Present_Count = WH_Present_Count + Convert.ToDecimal(WH_DS.Rows[j]["Present"].ToString());
                            }

                        }
                        else
                        {
                            WH_Present_Count = WH_Present_Count + Convert.ToDecimal(WH_DS.Rows[j]["Present"].ToString());
                        }
                    }
                }


                //'Spinning Incentive Check
                if (WagesValue.ToUpper() == "REGULAR".ToUpper() || WagesValue.ToUpper() == "HOSTEL".ToUpper())
                {
                    //'Check Spinning Days Start
                    Boolean Check_Spinning_Eligible = false;
                    DataTable DS_Spinning_Incv = new DataTable();
                    // 'Spinning Wages Check
                    int Month_Int = 1;
                    string Months_Full_Str = "";
                    string Fin_Year = "";
                    string Spin_Wages = WagesType;
                    string Spin_Machine_ID_Str = Log_DS.Rows[i]["MachineID"].ToString();

                    string[] date = FromDate.Split('/');
                    string mon = date[1];
                    Month_Int = Convert.ToInt32(mon.ToString());
                    if (Month_Int >= 4)
                    {
                        Fin_Year = date[2] + "-" + (Convert.ToInt32(date[2]) + 1);
                    }
                    else
                    {
                        Fin_Year = (Convert.ToInt32(date[2]) - 1) + "-" + date[2];
                    }


                    if (Month_Int == 01)
                    {
                        Months_Full_Str = "January";
                    }

                    else if (Month_Int == 02)
                    {
                        Months_Full_Str = "February";
                    }
                    else if (Month_Int == 03)
                    {
                        Months_Full_Str = "March";
                    }
                    else if (Month_Int == 04)
                    {
                        Months_Full_Str = "April";
                    }
                    else if (Month_Int == 05)
                    {
                        Months_Full_Str = "May";
                    }
                    else if (Month_Int == 06)
                    {
                        Months_Full_Str = "June";
                    }
                    else if (Month_Int == 07)
                    {
                        Months_Full_Str = "July";
                    }
                    else if (Month_Int == 08)
                    {
                        Months_Full_Str = "August";
                    }
                    else if (Month_Int == 09)
                    {
                        Months_Full_Str = "September";
                    }
                    else if (Month_Int == 10)
                    {
                        Months_Full_Str = "October";
                    }
                    else if (Month_Int == 11)
                    {
                        Months_Full_Str = "November";
                    }
                    else if (Month_Int == 12)
                    {
                        Months_Full_Str = "December";
                    }

                    Months_Full_Str = Left_Val(Months_Full_Str, 3);
                    SSQL = "Select * from SpinIncentiveDetPart where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And Months='" + Months_Full_Str + "' And FinYear='" + Fin_Year + "' And Wages='" + Spin_Wages + "' And MachineID='" + Spin_Machine_ID_Str + "'";
                    DS_Spinning_Incv = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (DS_Spinning_Incv.Rows.Count != 0)
                    {
                        Check_Spinning_Eligible = true;
                    }
                    else
                    {
                        Check_Spinning_Eligible = false;
                    }
                    if (Check_Spinning_Eligible == true)
                    {
                        if (WagesType.ToUpper() == "HOSTEL".ToUpper())
                        {
                            Spinning_Incentive_Days = Convert.ToDecimal(Log_DS.Rows[i]["Days"].ToString());
                        }
                        else
                        {
                            if (WagesType.ToUpper() == "REGULAR".ToUpper())
                            {
                                SSQL = "Select isnull(sum(Present),0) as Days from LogTime_Days where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                SSQL = SSQL + " And MachineID='" + Log_DS.Rows[i]["MachineID"] + "'";
                                SSQL = SSQL + " And Attn_Date >= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103)";
                                SSQL = SSQL + " And Attn_Date <= CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103)";
                                SSQL = SSQL + " And (Shift='SHIFT2' or Shift='SHIFT3')";
                                mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);

                                if (mDataSet.Rows.Count != 0)
                                {
                                    Spinning_Incentive_Days = Convert.ToDecimal(mDataSet.Rows[0]["Days"].ToString());
                                }
                                else
                                {
                                    Spinning_Incentive_Days = 0;
                                }
                            }
                            else
                            {
                                Spinning_Incentive_Days = 0;
                            }

                        }

                    }

                    else
                    {
                        Spinning_Incentive_Days = 0;

                    }
                }
                else
                {
                    Spinning_Incentive_Days = 0;

                }



                //'Total Days Get

                date1 = Convert.ToDateTime(FromDate);
                // string dd =string.Format("{dd/MM/yyyy}",TxtTo.Text);
                date2 = Convert.ToDateTime(ToDate);
                int dayCount = (int)((date2 - date1).TotalDays);
                int Total_Days_Count = dayCount;
                Total_Days_Count = Total_Days_Count + 1;
                Month_Mid_Total_Days_Count = 0;

                // 'Check DOJ Date to Report Date
                DateTime Report_Date = new DateTime();
                DateTime DOJ_Date_Format_Check = new DateTime();
                if (WagesValue.ToUpper() == "STAFF".ToUpper() || WagesValue.ToUpper() == "Watch & Ward".ToUpper() || WagesValue.ToUpper() == "Manager".ToUpper())
                {
                    if (DOJ_Date_Str != "")
                    {
                        Report_Date = Convert.ToDateTime(FromDate.ToString()); //'Report Date
                        DOJ_Date_Format_Check = Convert.ToDateTime(DOJ_Date_Str);
                        if (DOJ_Date_Format_Check <= Report_Date)
                        {
                            Month_Mid_Total_Days_Count = Total_Days_Count;
                        }
                        else
                        {
                            Month_Mid_Total_Days_Count = dayCount;
                            Month_Mid_Total_Days_Count = Month_Mid_Total_Days_Count + 1;
                        }

                    }
                    else
                    {
                        Month_Mid_Total_Days_Count = Total_Days_Count;
                    }

                }
                else
                {

                }

                if (WagesValue.ToUpper() == "STAFF".ToUpper() || WagesValue.ToUpper() == "Watch & Ward".ToUpper() || WagesValue.ToUpper() == "Manager".ToUpper())
                {
                    Fixed_Work_Days = Month_Mid_Total_Days_Count - Convert.ToInt32(WH_Count);
                    Present_Days_Count = Present_Days_Count - WH_Present_Count;
                    Present_Days_Count = Present_Days_Count - NFH_Days_Present_Count;
                    WH_Present_Count = WH_Present_Count + NFH_Days_Present_Count;
                }
                else
                {
                    Fixed_Work_Days = 0;
                    Present_Days_Count = Present_Days_Count - WH_Present_Count;
                    Present_Days_Count = Present_Days_Count - NFH_Days_Present_Count;
                    WH_Present_Count = WH_Present_Count;
                }
                if (Present_Days_Count != 0 && NFH_Days_Count != 0 && NFH_WH_Days_Mins != 0)
                {
                    double NFH_Week_Off_Minus_Days = Convert.ToDouble(NFH_WH_Days_Mins);
                    double NFH_Final_Disp_Days = 0;
                    if (NFH_Week_Off_Minus_Days <= Convert.ToDouble(WH_Present_Count))
                    {
                        WH_Present_Count = WH_Present_Count - Convert.ToDecimal(NFH_Week_Off_Minus_Days);
                    }
                    else
                    {
                        // 'NFH Days Minus for Working Days
                        double Balance_NFH_Days_Minus_IN_WorkDays = 0;

                        Balance_NFH_Days_Minus_IN_WorkDays = NFH_Week_Off_Minus_Days - Convert.ToDouble(WH_Present_Count);
                        Present_Days_Count = Present_Days_Count - Convert.ToDecimal(Balance_NFH_Days_Minus_IN_WorkDays);
                        WH_Present_Count = 0;
                    }
                }



                string NFH_Doubale_Wages_Attn_Inct = (NFH_Double_Wages_Checklist + NFH_Double_Wages_Manual + NFH_WH_Days_Mins).ToString();

                if (Present_Days_Count != 0)
                {


                    mEmployeeDT.NewRow();
                    mEmployeeDT.Rows.Add();
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Machine ID"] = Log_DS.Rows[i]["MachineID"].ToString();
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Token No"] = Log_DS.Rows[i]["ExistingCode"].ToString();
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["EmpName"] = Log_DS.Rows[i]["FirstName"].ToString();
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Days"] = Present_Days_Count;
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["H.Allowed"] = "0";
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["N/FH"] = NFH_Days_Count;

                    if (WagesValue.ToUpper() == "STAFF".ToUpper() || WagesValue.ToUpper() == "Watch & Ward".ToUpper() || WagesValue.ToUpper() == "Manager".ToUpper())
                    {
                        mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["OT Days"] = "0";
                        mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["SPG Allow"] = "0";
                        mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Canteen Days Minus"] = "0";
                        mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["OT Hours"] = "0";
                        mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["W.H"] = WH_Present_Count;
                        mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Fixed W.Days"] = Fixed_Work_Days;
                    }
                    else
                    {
                        mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["OT Days"] = WH_Present_Count;
                        mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["SPG Allow"] = Spinning_Incentive_Days;
                        mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Canteen Days Minus"] = "0";
                        mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["OT Hours"] = "0";
                        mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["W.H"] = "0";
                        mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Fixed W.Days"] = "0";
                    }



                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["NFH W.Days"] = NFH_Double_Wages_Checklist;
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Total Month Days"] = Month_Mid_Total_Days_Count;
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["NFH Worked Days"] = NFH_Doubale_Wages_Attn_Inct;
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["NFH D.W Statutory"] = NFH_Double_Wages_Statutory;
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["AEH"] = AEH_NFH_Days_Count;
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["LBH"] = LBH_NFH_Days_Count;
                    intI = intI + 1;
                }



                NFH_Days_Count = 0;
                AEH_NFH_Days_Count = 0;
                LBH_NFH_Days_Count = 0;
                NFH_Double_Wages_Checklist = 0;
                NFH_Double_Wages_Statutory = 0;
                NFH_Double_Wages_Manual = 0;
                NFH_WH_Days_Mins = 0;


            }

        }
        catch (Exception ex)
        {

        }


        UploadDataTableToExcel(mEmployeeDT);

    }

    public void PayrollAttn()
    {

        try
        {
            int intI;
            SSQL = "";
            SSQL = "Select LTD.MachineID,LTD.ExistingCode,EM.FirstName,sum(LTD.Present) as Days,";
            SSQL = SSQL + " EM.DOJ,EM.Weekoff from LogTime_Days LTD inner join Employee_Mst EM on";
            SSQL = SSQL + " EM.CompCode=LTD.CompCode And EM.LocCode=LTD.LocCode And LTD.MachineID=EM.MachineID";
            SSQL = SSQL + " where LTD.CompCode='" + SessionCcode + "' And LTD.LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And EM.CompCode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And EM.Wages='" + WagesValue + "' And (EM.IsActive='Yes' or CONVERT(DATETIME,EM.DOR,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103))";
            SSQL = SSQL + " and CONVERT(DATETIME,LTD.Attn_Date_Str,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103)";
            SSQL = SSQL + " And  CONVERT(DATETIME,LTD.Attn_Date_Str,103)<= CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103) ";
            SSQL = SSQL + " And LTD.Present!='0.0'";
            if (Division != "-Select-")
            {
                SSQL = SSQL + " And EM.Division = '" + Division + "'";
            }
            //If Trim(txtDivision.Text) <> "" Then SSQL = SSQL + " And EM.Division = '" + txtDivision.Text + "'";

            SSQL = SSQL + " Group by LTD.MachineID,LTD.ExistingCode,EM.FirstName,EM.DOJ,EM.Weekoff ";
            SSQL = SSQL + " Order by LTD.ExistingCode Asc";

            Log_DS = objdata.RptEmployeeMultipleDetails(SSQL);




            intI = 2;
            for (int i = 0; i < Log_DS.Rows.Count; i++)
            {

                // 'Final Output Variable Declaration
                NFH_Days_Count = 0;
                AEH_NFH_Days_Count = 0;
                LBH_NFH_Days_Count = 0;
                NFH_Days_Present_Count = 0;
                WH_Count = 0;
                WH_Present_Count = 0;
                Present_Days_Count = Convert.ToDecimal(Log_DS.Rows[i]["Days"]);
                Fixed_Work_Days = 0;
                Spinning_Incentive_Days = 0;

                NFH_Double_Wages_Checklist = 0;
                NFH_Double_Wages_Statutory = 0;
                NFH_Double_Wages_Manual = 0;
                NFH_WH_Days_Mins = 0;
                NFH_Single_Wages = 0;

                NFH_Type_Str = "";
                NFH_Name_Get_Str = "";
                NFH_Dbl_Wages_Statutory_Check = "";
                Emp_WH_Day = "";
                DOJ_Date_Str = "";

                DOJ_Date_Str = Log_DS.Rows[i]["DOJ"].ToString();
                Emp_WH_Day = Log_DS.Rows[i]["WeekOff"].ToString();

                //  'NFH Check Start
                string Machineid = Log_DS.Rows[i]["MachineID"].ToString();
                if (Machineid == "1886")
                {
                    Machineid = "1886";
                }

                qry_nfh = "Select NFHDate from NFH_Mst where  CONVERT(DATETIME,DateStr,103) >=  CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103) ";
                qry_nfh = qry_nfh + " And  CONVERT(DATETIME,DateStr,103) <=  CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103) ";
                NFH_DS = objdata.RptEmployeeMultipleDetails(qry_nfh);
                if (NFH_DS.Rows.Count > 0)
                {

                    //  'Date OF Joining Check Start
                    for (int k = 0; k < NFH_DS.Rows.Count; k++)
                    {
                        NFH_Date = Convert.ToDateTime(NFH_DS.Rows[k]["NFHDate"]);
                        // 'NFH Day Present Check

                        NFH_Date_P_Date = String.Format(Convert.ToString(NFH_Date), "yyyy/MM/dd");

                        SSQL = "";
                        SSQL = "Select * from LogTime_Days where Attn_Date=CONVERT(DATETIME,'" + Convert.ToDateTime(NFH_Date_P_Date).ToString("dd/MM/yyyy") + "',103)  ";
                        SSQL = SSQL + "  And MachineID='" + Log_DS.Rows[i]["MachineID"] + "' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";

                        mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (mDataSet.Rows.Count != 0)
                        {

                            NFH_Present_Check = Convert.ToDouble(mDataSet.Rows[0]["Present"]);
                        }
                        else
                        {

                            NFH_Present_Check = 0;

                        }
                        //'Get NFh Type

                        SSQL = "Select * from NFH_Mst where CONVERT(DATETIME,DateStr,103) >=  CONVERT(DATETIME,'" + Convert.ToDateTime(NFH_Date_P_Date).ToString("dd/MM/yyyy") + "',103) ";
                        NFH_Type_Ds = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (NFH_Type_Ds.Rows.Count != 0)
                        {

                            NFH_Type_Str = NFH_Type_Ds.Rows[0]["NFH_Type"].ToString();
                            NFH_Dbl_Wages_Statutory_Check = NFH_Type_Ds.Rows[0]["Dbl_Wages_Statutory"].ToString();
                            NFH_Name_Get_Str = NFH_Type_Ds.Rows[0]["Form25DisplayText"].ToString();

                        }
                        else
                        {
                            NFH_Type_Str = "";
                            NFH_Dbl_Wages_Statutory_Check = "";
                        }
                        if (DOJ_Date_Str != "")
                        {

                            DOJ_Date_Format = Convert.ToDateTime(DOJ_Date_Str);
                            if (DOJ_Date_Format.Date < NFH_Date.Date)
                            {
                                if (NFH_Type_Str == "WH Minus")
                                {
                                    NFH_WH_Days_Mins = NFH_WH_Days_Mins + 1;

                                }
                                else
                                {

                                    NFH_Days_Present_Count = NFH_Days_Present_Count + Convert.ToDecimal(NFH_Present_Check);
                                    if (NFH_Type_Str == "Double Wages Checklist")
                                    {

                                        NFH_Double_Wages_Checklist = NFH_Double_Wages_Checklist + Convert.ToDecimal(NFH_Present_Check);

                                        if (NFH_Dbl_Wages_Statutory_Check == "Yes")
                                        {
                                            NFH_Double_Wages_Statutory = NFH_Double_Wages_Statutory + Convert.ToDecimal(NFH_Present_Check);

                                        }
                                    }

                                    if (NFH_Type_Str == "Double Wages Manual")
                                    {
                                        NFH_Double_Wages_Manual = NFH_Double_Wages_Manual + Convert.ToDecimal(NFH_Present_Check);
                                    }

                                    NFH_Days_Count = NFH_Days_Count + 1;
                                    if (NFH_Name_Get_Str == "AEH")
                                    {
                                        AEH_NFH_Days_Count = AEH_NFH_Days_Count + 1;
                                    }
                                    if (NFH_Name_Get_Str == "LBH")
                                    {
                                        LBH_NFH_Days_Count = LBH_NFH_Days_Count + 1;
                                    }

                                }
                            }
                        }




                        else
                        {
                            NFH_Days_Count = NFH_Days_Count + 1;

                            if (NFH_Name_Get_Str == "AEH")
                            {
                                AEH_NFH_Days_Count = AEH_NFH_Days_Count + 1;

                            }
                            if (NFH_Name_Get_Str == "LBH")
                            {
                                LBH_NFH_Days_Count = LBH_NFH_Days_Count + 1;

                            }

                            if (NFH_Type_Str == "WH Minus")
                            {
                                NFH_WH_Days_Mins = NFH_WH_Days_Mins + 1;
                            }
                            else
                            {
                                NFH_Days_Present_Count = NFH_Days_Present_Count + Convert.ToDecimal(NFH_Present_Check);

                                if (NFH_Type_Str == "Double Wages Checklist")
                                {
                                    NFH_Double_Wages_Checklist = NFH_Double_Wages_Checklist + Convert.ToDecimal(NFH_Present_Check);
                                    if (NFH_Dbl_Wages_Statutory_Check == "Yes")
                                    {
                                        NFH_Double_Wages_Statutory = NFH_Double_Wages_Statutory + Convert.ToDecimal(NFH_Present_Check);
                                    }
                                }
                                if (NFH_Type_Str == "Double Wages Manual")
                                {
                                    NFH_Double_Wages_Manual = NFH_Double_Wages_Manual + Convert.ToDecimal(NFH_Present_Check);
                                }
                            }



                        }
                    }
                }// NFH Check END


                //'Week of Check

                Check_Week_Off_DOJ = false;

                SSQL = "Select Present,Attn_Date from LogTime_Days where MachineID='" + Log_DS.Rows[i]["MachineID"] + "' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                SSQL = SSQL + " And upper(DATENAME(weekday,Attn_Date))=upper('" + Emp_WH_Day + "')";
                SSQL = SSQL + " And Attn_Date >= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103)";
                SSQL = SSQL + " And Attn_Date <=CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103)";
                WH_DS = objdata.RptEmployeeMultipleDetails(SSQL);
                for (int j = 0; j < WH_DS.Rows.Count; j++)
                {

                    Week_Off_Date = Convert.ToDateTime(WH_DS.Rows[j]["Attn_Date"]);
                    if (WagesType.ToUpper() == "STAFF".ToUpper() || WagesType.ToUpper() == "Watch & Ward".ToUpper() || WagesType.ToUpper() == "Manager".ToUpper())
                    {
                        if (DOJ_Date_Str != "")
                        {

                            WH_DOJ_Date_Format = Convert.ToDateTime(DOJ_Date_Str);
                            if (WH_DOJ_Date_Format.Date <= Week_Off_Date.Date)
                            {

                                Check_Week_Off_DOJ = true;
                            }
                            else
                            {
                                Check_Week_Off_DOJ = false;
                            }
                        }
                        else
                        {
                            Check_Week_Off_DOJ = true;
                        }
                    }
                    else
                    {
                        Check_Week_Off_DOJ = true;
                    }

                    if (Check_Week_Off_DOJ == true)
                    {

                        WH_Count = WH_Count + 1;
                        //'NFH Day Check
                        // String.Format(Convert.ToString(dtime), "yyyy/MM/dd");

                        string NFH_Date_WH_Date = Convert.ToDateTime(Week_Off_Date).AddDays(0).ToString("yyyy/MM/dd");
                        SSQL = "Select * from NFH_Mst where CONVERT(DATETIME,DateStr,103) >=  CONVERT(DATETIME,'" + Convert.ToDateTime(NFH_Date_WH_Date).ToString("dd/MM/yyyy") + "',103) ";
                        mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (mDataSet.Rows.Count > 0)
                        {
                            if (mDataSet.Rows.Count != 0)
                            {
                                NFH_Type_Str = mDataSet.Rows[0]["NFH_Type"].ToString();
                                NFH_Dbl_Wages_Statutory_Check = NFH_Type_Ds.Rows[0]["Dbl_Wages_Statutory"].ToString();
                            }
                            else
                            {
                                NFH_Type_Str = "";
                                NFH_Dbl_Wages_Statutory_Check = "";
                            }
                            if (NFH_Type_Str == "WH Minus")
                            {
                                WH_Present_Count = WH_Present_Count + Convert.ToDecimal(WH_DS.Rows[j]["Present"].ToString());
                            }
                        }
                        else
                        {
                            WH_Present_Count = WH_Present_Count + Convert.ToDecimal(WH_DS.Rows[j]["Present"].ToString());
                        }
                    }
                }




                //'Spinning Incentive Check
                if (WagesValue.ToUpper() == "REGULAR".ToUpper() || WagesValue.ToUpper() == "HOSTEL".ToUpper())
                {
                    //'Check Spinning Days Start
                    Boolean Check_Spinning_Eligible = false;
                    DataTable DS_Spinning_Incv = new DataTable();
                    // 'Spinning Wages Check
                    int Month_Int = 1;
                    string Months_Full_Str = "";
                    string Fin_Year = "";
                    string Spin_Wages = WagesType;
                    string Spin_Machine_ID_Str = Log_DS.Rows[i]["MachineID"].ToString();

                    string[] date = FromDate.Split('/');
                    string mon = date[1];
                    Month_Int = Convert.ToInt32(mon.ToString());
                    if (Month_Int >= 4)
                    {
                        Fin_Year = date[2] + "-" + (Convert.ToInt32(date[2]) + 1);
                    }
                    else
                    {
                        Fin_Year = (Convert.ToInt32(date[2]) - 1) + "-" + date[2];
                    }


                    if (Month_Int == 01)
                    {
                        Months_Full_Str = "January";
                    }

                    else if (Month_Int == 02)
                    {
                        Months_Full_Str = "February";
                    }
                    else if (Month_Int == 03)
                    {
                        Months_Full_Str = "March";
                    }
                    else if (Month_Int == 04)
                    {
                        Months_Full_Str = "April";
                    }
                    else if (Month_Int == 05)
                    {
                        Months_Full_Str = "May";
                    }
                    else if (Month_Int == 06)
                    {
                        Months_Full_Str = "June";
                    }
                    else if (Month_Int == 07)
                    {
                        Months_Full_Str = "July";
                    }
                    else if (Month_Int == 08)
                    {
                        Months_Full_Str = "August";
                    }
                    else if (Month_Int == 09)
                    {
                        Months_Full_Str = "September";
                    }
                    else if (Month_Int == 10)
                    {
                        Months_Full_Str = "October";
                    }
                    else if (Month_Int == 11)
                    {
                        Months_Full_Str = "November";
                    }
                    else if (Month_Int == 12)
                    {
                        Months_Full_Str = "December";
                    }

                    Months_Full_Str = Left_Val(Months_Full_Str, 3);
                    SSQL = "Select * from SpinIncentiveDetPart where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And Months='" + Months_Full_Str + "' And FinYear='" + Fin_Year + "' And Wages='" + Spin_Wages + "' And MachineID='" + Spin_Machine_ID_Str + "'";
                    DS_Spinning_Incv = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (DS_Spinning_Incv.Rows.Count != 0)
                    {
                        Check_Spinning_Eligible = true;
                    }
                    else
                    {
                        Check_Spinning_Eligible = false;
                    }
                    if (Check_Spinning_Eligible == true)
                    {
                        if (WagesType.ToUpper() == "HOSTEL".ToUpper())
                        {
                            Spinning_Incentive_Days = Convert.ToDecimal(Log_DS.Rows[i]["Days"].ToString());
                        }
                        else
                        {
                            if (WagesType.ToUpper() == "REGULAR".ToUpper())
                            {
                                SSQL = "Select isnull(sum(Present),0) as Days from LogTime_Days where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                SSQL = SSQL + " And MachineID='" + Log_DS.Rows[i]["MachineID"] + "'";
                                SSQL = SSQL + " And Attn_Date >= CONVERT(DATETIME,'" + Convert.ToDateTime(FromDate).ToString("dd/MM/yyyy") + "',103)";
                                SSQL = SSQL + " And Attn_Date <= CONVERT(DATETIME,'" + Convert.ToDateTime(ToDate).ToString("dd/MM/yyyy") + "',103)";
                                SSQL = SSQL + " And (Shift='SHIFT2' or Shift='SHIFT3')";
                                mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);

                                if (mDataSet.Rows.Count != 0)
                                {
                                    Spinning_Incentive_Days = Convert.ToDecimal(mDataSet.Rows[0]["Days"].ToString());
                                }
                                else
                                {
                                    Spinning_Incentive_Days = 0;
                                }
                            }
                            else
                            {
                                Spinning_Incentive_Days = 0;
                            }
                        }
                    }

                    else
                    {
                        Spinning_Incentive_Days = 0;

                    }
                }
                else
                {
                    Spinning_Incentive_Days = 0;

                }

                //'Total Days Get

                date1 = Convert.ToDateTime(FromDate);
                // string dd =string.Format("{dd/MM/yyyy}",TxtTo.Text);
                date2 = Convert.ToDateTime(ToDate);
                int dayCount = (int)((date2 - date1).TotalDays);
                int Total_Days_Count = dayCount;
                Total_Days_Count = Total_Days_Count + 1;
                Month_Mid_Total_Days_Count = 0;

                // 'Check DOJ Date to Report Date
                DateTime Report_Date = new DateTime();
                DateTime DOJ_Date_Format_Check = new DateTime();
                if (WagesValue.ToUpper() == "STAFF".ToUpper() || WagesValue.ToUpper() == "Watch & Ward".ToUpper() || WagesValue.ToUpper() == "Manager".ToUpper())
                {
                    if (DOJ_Date_Str != "")
                    {
                        Report_Date = Convert.ToDateTime(FromDate.ToString()); //'Report Date
                        DOJ_Date_Format_Check = Convert.ToDateTime(DOJ_Date_Str);
                        if (DOJ_Date_Format_Check <= Report_Date)
                        {
                            Month_Mid_Total_Days_Count = Total_Days_Count;
                        }
                        else
                        {
                            //Month_Mid_Total_Days_Count = dayCount;
                            Month_Mid_Total_Days_Count = (int)((date2 - DOJ_Date_Format_Check).TotalDays);
                            Month_Mid_Total_Days_Count = Month_Mid_Total_Days_Count + 1;
                        }

                    }
                    else
                    {
                        Month_Mid_Total_Days_Count = Total_Days_Count;
                    }

                }
                else
                {

                }

                if (WagesValue.ToUpper() == "STAFF".ToUpper() || WagesValue.ToUpper() == "Watch & Ward".ToUpper() || WagesValue.ToUpper() == "Manager".ToUpper())
                {
                    Fixed_Work_Days = Month_Mid_Total_Days_Count - Convert.ToInt32(WH_Count);
                    Present_Days_Count = Present_Days_Count - WH_Present_Count;
                    Present_Days_Count = Present_Days_Count - NFH_Days_Present_Count;
                    WH_Present_Count = WH_Present_Count + NFH_Days_Present_Count;
                }
                else
                {
                    Fixed_Work_Days = 0;
                    Present_Days_Count = Present_Days_Count - WH_Present_Count;
                    Present_Days_Count = Present_Days_Count - NFH_Days_Present_Count;
                    WH_Present_Count = WH_Present_Count;
                }
                if (Present_Days_Count != 0 && NFH_Days_Count != 0 && NFH_WH_Days_Mins != 0)
                {
                    double NFH_Week_Off_Minus_Days = Convert.ToDouble(NFH_WH_Days_Mins);
                    double NFH_Final_Disp_Days = 0;
                    if (NFH_Week_Off_Minus_Days <= Convert.ToDouble(WH_Present_Count))
                    {
                        WH_Present_Count = WH_Present_Count - Convert.ToDecimal(NFH_Week_Off_Minus_Days);
                    }
                    else
                    {
                        // 'NFH Days Minus for Working Days
                        double Balance_NFH_Days_Minus_IN_WorkDays = 0;

                        Balance_NFH_Days_Minus_IN_WorkDays = NFH_Week_Off_Minus_Days - Convert.ToDouble(WH_Present_Count);
                        Present_Days_Count = Present_Days_Count - Convert.ToDecimal(Balance_NFH_Days_Minus_IN_WorkDays);
                        WH_Present_Count = 0;
                    }
                }

                string NFH_Doubale_Wages_Attn_Inct = (NFH_Double_Wages_Checklist + NFH_Double_Wages_Manual + NFH_WH_Days_Mins).ToString();

                if (Present_Days_Count != 0)
                {

                    mEmployeeDT.NewRow();
                    mEmployeeDT.Rows.Add();
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Machine ID"] = Log_DS.Rows[i]["MachineID"].ToString();
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Token No"] = Log_DS.Rows[i]["ExistingCode"].ToString();
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["EmpName"] = Log_DS.Rows[i]["FirstName"].ToString();
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Days"] = Present_Days_Count;
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["H.Allowed"] = "0";
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["N/FH"] = NFH_Days_Count;

                    if (WagesValue.ToUpper() == "STAFF".ToUpper() || WagesValue.ToUpper() == "Watch & Ward".ToUpper() || WagesValue.ToUpper() == "Manager".ToUpper())
                    {
                        mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["OT Days"] = "0";
                        mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["SPG Allow"] = "0";
                        mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Canteen Days Minus"] = "0";
                        mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["OT Hours"] = "0";
                        mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["W.H"] = WH_Present_Count;
                        mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Fixed W.Days"] = Fixed_Work_Days;
                    }
                    else
                    {
                        mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["OT Days"] = WH_Present_Count;
                        mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["SPG Allow"] = Spinning_Incentive_Days;
                        mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Canteen Days Minus"] = "0";
                        mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["OT Hours"] = "0";
                        mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["W.H"] = "0";
                        mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Fixed W.Days"] = "0";
                    }

                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["NFH W.Days"] = NFH_Double_Wages_Checklist;
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["Total Month Days"] = Month_Mid_Total_Days_Count;
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["NFH Worked Days"] = NFH_Doubale_Wages_Attn_Inct;
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["NFH D.W Statutory"] = NFH_Double_Wages_Statutory;
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["AEH"] = AEH_NFH_Days_Count;
                    mEmployeeDT.Rows[mEmployeeDT.Rows.Count - 1]["LBH"] = LBH_NFH_Days_Count;
                    intI = intI + 1;
                }

                NFH_Days_Count = 0;
                AEH_NFH_Days_Count = 0;
                LBH_NFH_Days_Count = 0;
                NFH_Double_Wages_Checklist = 0;
                NFH_Double_Wages_Statutory = 0;
                NFH_Double_Wages_Manual = 0;
                NFH_WH_Days_Mins = 0;


            }

        }
        catch (Exception ex)
        {

        }
        UploadDataTableToExcel(mEmployeeDT);

    }
    protected void UploadDataTableToExcel(DataTable dtRecords)
    {
        string XlsPath = Server.MapPath(@"~/Add_data/EmployeePayroll.xls");
        string attachment = string.Empty;
        if (XlsPath.IndexOf("\\") != -1)
        {
            string[] strFileName = XlsPath.Split(new char[] { '\\' });
            attachment = "attachment; filename=" + strFileName[strFileName.Length - 1];
        }
        else
            attachment = "attachment; filename=" + XlsPath;
        try
        {
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/vnd.ms-excel";
            string tab = string.Empty;

            foreach (DataColumn datacol in dtRecords.Columns)
            {
                Response.Write(tab + datacol.ColumnName);
                tab = "\t";
            }
            Response.Write("\n");

            foreach (DataRow dr in dtRecords.Rows)
            {
                tab = "";
                for (int j = 0; j < dtRecords.Columns.Count; j++)
                {
                    Response.Write(tab + Convert.ToString(dr[j]));
                    tab = "\t";
                }

                Response.Write("\n");
            }
            Response.End();
        }
        catch (Exception ex)
        {
            //Response.Write(ex.Message);
        }
    }
    public string Left_Val(string Value, int Length)
    {
        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }
}