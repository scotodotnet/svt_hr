﻿<%@ Page Language="C#"  MasterPageFile="~/MainPage.master"  AutoEventWireup="true" CodeFile="LeftNewJoiningReport.aspx.cs" Inherits="LeftNewJoiningReport" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


<style type="text/css">
        .CalendarCSS
        {
            background-color:White;
            color:Black;
            border:1px solid #646464;
            font-size:xx-large;
            font-weight: bold;
            margin-bottom: 40px;
            margin-top: 20px;
        }
        </style>
<script type="text/javascript">
        $(document).ready(function () {
       
            $("ul[id*=myid] li").click(function () {
                alert($(this).html()); // gets innerHTML of clicked li
                alert($(this).text()); // gets text contents of clicked li
               
            });
        });
</script>

 <script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.date-picker').datepicker({ format: 'dd/mm/yyyy' });

                $('.select2').select2();
            }
        });
    };
</script>

<script src="assets/js/master_list_jquery.min.js"></script>
<script src="assets/js/master_list_jquery-ui.min.js"></script>
<link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css"/>
<%--<script src="assets/js/form-wizards.demo.min.js"></script>--%>

<asp:UpdatePanel ID="UpdatePanel5" runat="server">
 <ContentTemplate>
<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">MD Report</a></li>
				<li class="active">New Joinee and Left Employee Report</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">New Joinee and Left Employee Report </h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">New Joinee and Left Employee Report</h4>
                        </div>
                        <div class="panel-body">
                        <!-- begin row -->
                          <div class="row">
                           <!-- begin col-4 -->
                           
                            <div class="form-group col-md-4">
								 <%----%>	
									 <label>Unit</label>
								 <asp:DropDownList runat="server" ID="ddlunit" class="form-control select2">
								
								</asp:DropDownList>
								 </div>
                                <div class="form-group col-md-4">
												  <label>FromDate</label>
								  <asp:TextBox ID="txtFrmdate" runat="server" class="form-control datepicker" placeholder="dd/MM/YYYY"></asp:TextBox>
                                   <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                        TargetControlID="txtFrmdate" ValidChars="0123456789/">
                                   </cc1:FilteredTextBoxExtender>
												
												</div>
												
												    <div class="form-group col-md-4">
                                                     <label>ToDate</label>
								 <asp:TextBox ID="txtTodate" runat="server" class="form-control datepicker" placeholder="dd/MM/YYYY"></asp:TextBox>
                                   <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                        TargetControlID="txtTodate" ValidChars="0123456789/">
                                   </cc1:FilteredTextBoxExtender>
												</div>
                               </div>
                              <!-- end col-4 -->
                             <!-- begin col-4 -->
                             
                                                <div class="row">
                                                  <div class="form-group col-md-5"   >
                                                  <label>IsActive</label>
                                                   
                                                   <asp:RadioButtonList ID="RdbCashBank" runat="server" RepeatColumns="3" class="form-control">
                                              <asp:ListItem Selected="true" Text="New Joinee" style="padding-right:40px" Value="0"></asp:ListItem>
                                                 <asp:ListItem Selected="False" Text="Left Employee" style="padding-right:40px" Value="1"></asp:ListItem>
                                       
                                     </asp:RadioButtonList>
                                                 </div>
                                                 </div>
                                <div class="col-md-4">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btnReport" Text="Report" class="btn btn-info" 
                                          onclick="btnReport_Click"  />
									 
								 </div>
                               </div>
                              <!-- end col-4 -->
                          
                              </div>
                        <!-- end row -->
                    
                    
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
<!-- end #content -->

</ContentTemplate>

<Triggers>
<asp:PostBackTrigger ControlID="btnReport" /> 

</Triggers>
</asp:UpdatePanel>


</asp:Content>