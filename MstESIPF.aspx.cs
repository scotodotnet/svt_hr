﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Payroll;
using Payroll.Configuration;
using Payroll.Data;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class MstESIPF : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionPayroll;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        Load_DB();
        if (!IsPostBack)
        {
            DisplayESI();
        }
    }
    public void Load_DB()
    {
        //Get Database Name
        string query = "";
        DataTable dt_DB = new DataTable();
        query = "select CMS,Rights,Sales,Stores,Weaving,Maintaince,Production,Payroll from [SVT_Rights]..MstDBname";
        dt_DB = objdata.RptEmployeeMultipleDetails(query);
        if (dt_DB.Rows.Count > 0)
        {
            SessionPayroll = dt_DB.Rows[0]["Payroll"].ToString();
            //SessionRights = dt_DB.Rows[0]["Rights"].ToString();
        }
    }


    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            string MessFlag = "Insert";
            if ((txtPF.Text.Trim() == "") || (txtPF.Text.Trim() == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the PF %');", true);
                ErrFlag = true;
            }
            else if ((txtESI.Text.Trim() == "") || (txtESI.Text.Trim() == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the ESI %');", true);
                ErrFlag = true;
            }
            else if ((txtPFsalary.Text.Trim() == "") || (txtPFsalary.Text.Trim() == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Staff Salary');", true);
                ErrFlag = true;
            }

            else if ((txtEmpESI.Text.Trim() == "") || (txtEmpESI.Text.Trim() == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the EMPLOYEER ESI %');", true);
                ErrFlag = true;
            }
            else if ((txtEmpPF1.Text.Trim() == "") || (txtEmpPF1.Text.Trim() == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the EMPLOYEER PF A/C TWO %');", true);
                ErrFlag = true;
            }
            else if ((txtEmpPF2.Text.Trim() == "") || (txtEmpPF2.Text.Trim() == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the EMPLOYEER PF A/C ONE %');", true);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                DataTable dt = new DataTable();
                string Query = "Select * from [" + SessionPayroll + "]..MstESIPF where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                dt = objdata.RptEmployeeMultipleDetails(Query);
                if (dt.Rows.Count > 0)
                {
                    Query = "delete from [" + SessionPayroll + "]..MstESIPF where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                    dt = objdata.RptEmployeeMultipleDetails(Query);
                    MessFlag = "Update";
                }
                Query = "Insert into [" + SessionPayroll + "]..MstESIPF(Ccode,Lcode,PF_per,ESI_per,StaffSalary,EmployeerPFone,EmployeerPFTwo,EmployeerESI) values('" + SessionCcode + "','" + SessionLcode + "',";
                Query = Query + "'" + txtPF.Text + "','" + txtESI.Text + "','" + txtPFsalary.Text + "','" + txtEmpPF1.Text + "','" + txtEmpPF2.Text + "','" + txtEmpESI.Text + "')";
                objdata.RptEmployeeMultipleDetails(Query);
                //Clear();

                if (MessFlag == "Insert")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Update Successfully');", true);
                }


            }
        }
        catch (Exception ex)
        {
        }
    }
    public void DisplayESI()
    {
        DataTable dt = new DataTable();
        string Query = "Select * from [" + SessionPayroll + "]..MstESIPF where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(Query);
        if (dt.Rows.Count > 0)
        {
            txtPF.Text = dt.Rows[0]["PF_per"].ToString();
            txtESI.Text = dt.Rows[0]["ESI_per"].ToString();
            txtPFsalary.Text = dt.Rows[0]["StaffSalary"].ToString();
            txtEmpESI.Text = dt.Rows[0]["EmployeerESI"].ToString();
            txtEmpPF1.Text = dt.Rows[0]["EmployeerPFone"].ToString();
            txtEmpPF2.Text = dt.Rows[0]["EmployeerPFTwo"].ToString();
        }
        else
        {
            txtPF.Text = "0";
            txtESI.Text = "0";
            txtPFsalary.Text = "0";
            txtEmpESI.Text = "0";
            txtEmpPF1.Text = "0";
            txtEmpPF2.Text = "0";
        }

        Query = "Select * from MstCanteenAmtDet where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(Query);
        if (dt.Rows.Count != 0)
        {
            txtHostelCanteenAmt.Text = dt.Rows[0]["CanteenAmt"].ToString();
        }
        else
        {
            txtHostelCanteenAmt.Text = "0";
        }
    }

    protected void btnHostelCanteenSave_Click(object sender, EventArgs e)
    {
        string MessFlag = "Insert";
        if (txtHostelCanteenAmt.Text != "")
        {
            string query = "";
            DataTable DT_Q = new DataTable();
            query = "Select * from MstCanteenAmtDet where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            DT_Q = objdata.RptEmployeeMultipleDetails(query);
            if (DT_Q.Rows.Count != 0)
            {
                MessFlag = "Update";
                query = "Delete from MstCanteenAmtDet where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                objdata.RptEmployeeMultipleDetails(query);
            }
            //insert Record
            query = "insert into MstCanteenAmtDet(CompCode,LocCode,CanteenAmt) Values('" + SessionCcode + "','" + SessionLcode + "','" + txtHostelCanteenAmt.Text + "')";
            objdata.RptEmployeeMultipleDetails(query);
            if (MessFlag == "Insert")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Update Successfully');", true);
            }
        }
        else
        {

        }
    }
    
}
